Dear ART users,

I pushed several updates to the primary repository (cartamr/cart). The default version is still 1.9, but we should start switching to 2.0. That version is reasonably stable and I would strongly urge you to try using it.

I ran several tests (a cosmological run with RT and an isolated disk), but I was not, obviously, be able to test everything. To make sure we benefit from the large effort Doug put in before leaving, I would urge you to test your usual runs with it. If there are no complains, we should make it a default one.

There is also a version 2.1, that includes a hydro solver rewrite by Benedikt. This version was not properly tested and should not be used for production runs until further notice. It would be great, however, if people start testing it, so that we all can also benefit from Benedikt's effort.

One difference between 1.9 and 2.0 that you encounter up front is how star formation methods (and a new method for load balancing, LB_METHOD) are specified: in 1.9 those we put in defs.h, with Makefile editing (and, occasionally, corrupting) defs.h files, while in 2.0 the specifications are set in the Makefile and defs.h remains exclusively read-only. Also, SF_FORMSTAR has been renamed SF_ALGORITHM (to avoid such elegant names as starformation_formstar.form_star_particles). You can check tests or demos for examples.

n
