#include "config.h"

#include <math.h>
#include <stdio.h>

#include "auxiliary.h"
#include "cosmology.h"
#include "hydro.h"
#include "io.h"
#include "io_cart.h"
#include "parallel.h"
#include "sfc.h"
#include "times.h"
#include "tree.h"
#include "units.h"

#if defined(COSMOLOGY) && defined(HYDRO)

extern double auni_init;

void read_hart_gas_ic( char *filename ) {
	int i, j;
	FILE *input;
	int size;
	float boxh, ainit, astep;
	int ncells;
	int endian;
	int coords[nDim];
	sfc_t sfc;
	int proc, icell;
	int page_count;
	float *input_page;
	int var;
	float fracHII;
	const int num_gas_vars    = 6;
	const int var_index[] = {
		HVAR_GAS_DENSITY, HVAR_MOMENTUM,
		HVAR_MOMENTUM+1, HVAR_MOMENTUM+2,
		HVAR_GAS_ENERGY, HVAR_INTERNAL_ENERGY };

	input = fopen(filename, "r");
	if ( input == NULL ) {
		cart_error("Unable to open %s for reading!", filename );
	}

	fread( &size, sizeof(int), 1, input );
	if ( size != sizeof(float) ) {
		reorder( (char *)&size, sizeof(int) );
		endian = 1;
		if ( size != sizeof(float) ) {
			cart_error("Bad file-format in read_cell_ic");
		}
	} else {
		endian = 0;
	}

	fread( &boxh, sizeof(float), 1, input );
	fread( &size, sizeof(int), 1, input );

	fread( &size, sizeof(int), 1, input );
	fread( &ainit, sizeof(float), 1, input );
	fread( &astep, sizeof(float), 1, input );
	fread( &size, sizeof(int), 1, input );

	fread( &size, sizeof(int), 1, input );
	fread( &ncells, sizeof(int), 1, input );
	fread( &size, sizeof(int), 1, input );

	if ( endian ) {
		reorder( (char *)&boxh, sizeof(float) );
		reorder( (char *)&ainit, sizeof(float) );
		reorder( (char *)&astep, sizeof(float) );
		reorder( (char *)&ncells, sizeof(int) );
	}

	box_size = boxh;
	auni_init = ainit;

	if ( local_proc_id == MASTER_NODE ) {
		cart_debug("boxh = %f", boxh );
		cart_debug("ainit = %f", ainit );
		cart_debug("astep = %f", astep );
		cart_debug("ncells = %u", ncells );

		if ( ncells != num_root_cells ) {
			cart_error("ncells in %s does not match num_root_cells (%u vs %u)",
					filename, ncells, num_root_cells );
		}
	}

	input_page = cart_alloc(float, num_grid*num_grid );
	for ( var = 0; var < num_gas_vars; var++ ) {
		fread( &size, sizeof(int), 1, input );
		for ( coords[0] = 0; coords[0] < num_grid; coords[0]++ ) {
			size = fread( input_page, sizeof(float), num_grid*num_grid, input );
			if ( size != num_grid*num_grid ) {
				cart_error("Error reading from file %s", filename );
			}

			if ( endian ) {
				for ( i = 0; i < num_grid*num_grid; i++ ) {
					reorder( (char *)&input_page[i], sizeof(float) );
				}
			}

			page_count = 0;
			for ( coords[1] = 0; coords[1] < num_grid; coords[1]++ ) {
				for ( coords[2] = 0; coords[2] < num_grid; coords[2]++ ) {
					sfc = sfc_index( coords );

					if ( sfc_is_local(sfc) ) {
						icell = sfc_cell_location(sfc);
						cell_var(icell, var_index[var]) = input_page[page_count];
					}

					page_count++;
				}
			}
		}
		fread( &size, sizeof(int), 1, input );
	}

	fclose(input);
	cart_free( input_page );

	simulation_time_init();

	fracHII = 1.2e-5*sqrt(cosmology->Omh2)/cosmology->Obh2;

	/* set gas gamma on root level */
	for ( i = 0; i < num_cells_per_level[min_level]; i++ ) {
		cell_gas_gamma(i) =  constants->gamma;

#ifdef ELECTRON_ION_NONEQUILIBRIUM
		cell_electron_internal_energy(i) = cell_gas_internal_energy(i)*constants->wmu/constants->wmu_e;
#endif /* ELECTRON_ION_NONEQUILIBRIUM */

#ifdef ENRICHMENT
		cell_gas_metal_density_II(i) = 1e-30;

#ifdef ENRICHMENT_SNIa
		cell_gas_metal_density_Ia(i) = 1e-30;
#endif /* ENRICHMENT_SNIa */
#endif /* ENRICHMENT */

#ifdef RADIATIVE_TRANSFER
		cell_HI_density(i) = cell_gas_density(i)*constants->XH*(1.0-fracHII);
		cell_HII_density(i) = cell_gas_density(i)*constants->XH*fracHII;
		cell_HeI_density(i) = cell_gas_density(i)*constants->XHe;
		cell_HeII_density(i) = cell_gas_density(i)*0.0;
		cell_HeIII_density(i) = cell_gas_density(i)*0.0;
		cell_H2_density(i) = cell_gas_density(i)*constants->XH*2.0e-6;
#endif
#ifdef EXTRA_PRESSURE_SOURCE
		cell_extra_pressure_source(i) = 0;
#endif /* EXTRA_PRESSURE_SOURCE */
#ifdef SGS_TURBULENCE
		cell_gas_turbulent_energy(i) = 0.0;
#endif /* SGS_TURBULENCE */
#ifdef COSMIC_RAYS
      for( j = 0; j < cr_num_bins; j++ ) {
    	  cell_cosmic_rays_bin_density(i,j) = 0.0;
    	  cell_cosmic_rays_bin_energy(i,j) = 0.0;
      }
#endif /* COSMIC_RAYS */
	}
}

#endif  /* HYDRO && COSMOLOGY */
