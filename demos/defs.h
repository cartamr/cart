

#define HYDRO
#define GRAVITY
#define COOLING
#define COSMOLOGY
#define PARTICLES
#define REFINEMENT
#define STAR_FORMATION
#define RADIATIVE_TRANSFER
#define BLASTWAVE_FEEDBACK
#define SGS_TURBULENCE

#define ENRICHMENT
#define ENRICHMENT_SNIa

#define DEBUG_MEMORY_USE

#define num_root_grid_refinements	7
#define num_refinement_levels		9
#define num_octs			100000
#define num_particles		        1000000
#define num_star_particles              1000000

