#include "config.h"


#include <mpi.h>
#include <stdio.h>
#include <string.h>


#include "auxiliary.h"
#include "control_parameter.h"
#include "io.h"
#include "io_artio.h"


extern const char* executable_name;
extern char output_directory_d[]; 
extern char logfile_directory_d[]; 

extern int artio_buffer_size;


char apt_output_directory[CONTROL_PARAMETER_STRING_LENGTH] = "";
const char *apt_label = NULL;
int apt_save = 0;

void init()
{
  const char *str;
  char c, *tmp;
  int n;
  float v;

  if((!is_option_present("job-name","j",1) && !is_option_present("input","i",1)) || (is_option_present("job-name","j",1) && !is_option_present("label","l",1)))
    {
      cart_error("Usage: %s -i/--input=<filename> [options] -t/--test=<specs> [-t/--test=<specs> ...].\n"
		 "   or: %s -j/--job-name=<name> -l/--file-label=<label> [options] -t/--test=<specs> [-t/--test=<specs> ...].\n"
		 "One or more write tests will be made, where <specs> is a set of comma-separated numbers \n"
		 "io:artio-buffer-size,io:num-grid-files,io:num-particle-files. If the last\n"
		 "one is omitted, it defaults to io:num-grid-files.\n"
		 "Valid options:\n"
		 "  -j, --job-name=<name>              set the job name for the fileset\n" 
		 "  -d, --data-directory=<dir>         set the input data directory where files are\n" 
		 "                                     located (default is the current directory)\n"
		 "  -o, --output-directory=<dir>       set the data directory where converted output\n"
		 "                                     files are written (default is data-directory)\n"
		 "  -s, --save-outputs                 write outputs into separate sub-directories\n"
		 "  -bs, --buffer-size=<value>         set the buffer size for reading\n"
		 "  -l, --file-label=<label>           set the label for the fileset\n"
		 "  -i, --input=<filename>             set the input grid filename; this option\n"
		 "                                     combines -j, -d, and -l in one option, i.e.\n"
		 "                                     <filename> = <dir>/<name>_<label>.art\n"
		,executable_name,executable_name);
    }

  control_parameter_add2(control_parameter_string,(char *)jobname,"job-name","jobname","");

  str = extract_option1("input","i",NULL);
  if(str != NULL)
    {
      tmp = strrchr(str,'.');
      if(tmp != NULL)
	{
	  /*
	  //  Chop off the extension
	  */
	  *tmp = 0;
	  tmp++;
	}
      if(tmp==NULL || strcmp(tmp,"art")!=0)
	{
	  cart_error("Input ARTIO file must end with .art.");
	}

      tmp = strrchr(str,'/');
      if(tmp != NULL)
	{
	  /*
	  //  Detach the leading directory path
	  */
	  *tmp = 0;
	  strncpy(output_directory_d,str,CONTROL_PARAMETER_STRING_LENGTH);
	  output_directory_d[CONTROL_PARAMETER_STRING_LENGTH-1] = 0;
	  str = tmp + 1;
	  if(strlen(str) == 0)
	    {
	      cart_error("Missing the file name after the last /.");
	    }
	}

      tmp = strrchr(str,'_');
      if(tmp != NULL)
	{
	  if(sscanf(tmp,"_a%f%c",&v,&c)==1 || sscanf(tmp,"_%d%c",&n,&c)==1)
	    {
	      apt_label = tmp + 1;
	      *tmp = 0;
	    }
	}

      set_jobname(str);
    }

  str = extract_option1("job-name","j",NULL);
  if(str != NULL)
    {
      set_jobname(str);
    }

  str = extract_option1("data-directory","d",NULL);
  if(str != NULL)
    {
      strncpy(output_directory_d,str,CONTROL_PARAMETER_STRING_LENGTH-1);
      output_directory_d[CONTROL_PARAMETER_STRING_LENGTH-1] = 0;
    }

  str = extract_option1("output-directory","o",NULL);
  if(str != NULL)
    {
      strncpy(apt_output_directory,str,CONTROL_PARAMETER_STRING_LENGTH-1);
      apt_output_directory[CONTROL_PARAMETER_STRING_LENGTH-1] = 0;
    }

  str = extract_option1("file-label","l",NULL);
  if(str != NULL)
    {
      apt_label = str;
    }

  str = extract_option0("save-outputs","s");
  if(str != NULL)
    {
      apt_save = 1;
    }

  str = extract_option1("buffer-size","bs",NULL);
  if(str != NULL)
    {
      int bs;
      char c;
      if(sscanf(str,"%d%c",&bs,&c)!=1 || bs<1)
	{
	  cart_error("Invalid buffer size value %s.",str);
	}
      artio_buffer_size = bs;
    }
}


void read_file()
{
  double wtime = MPI_Wtime();
  int load_balance_after_io_flag = 0;

  read_artio_restart(apt_label, &load_balance_after_io_flag);

  cart_debug("Reading time: %lg sec",MPI_Wtime()-wtime);
}


void write_file()
{
  const char *str;
  char outdir[CONTROL_PARAMETER_STRING_LENGTH];
  int n = 0;

  if ( strcmp(apt_output_directory,"") != 0 ) {
      cart_debug("Setting output directory to %s", apt_output_directory);
      strncpy(output_directory_d,apt_output_directory,CONTROL_PARAMETER_STRING_LENGTH-1);
      output_directory_d[CONTROL_PARAMETER_STRING_LENGTH-1] = 0;
  }

  strncpy(outdir,output_directory_d,CONTROL_PARAMETER_STRING_LENGTH-5);
  outdir[CONTROL_PARAMETER_STRING_LENGTH-5] = 0;

  while((str = extract_option1("test","t",NULL)) != NULL)
    {
      double wtime;
      char c;
      int ng, np = 0;
      int bs;

      int ret = sscanf(str,"%d,%d,%d%c",&bs,&ng,&np,&c);
      if(ret!=3 && sscanf(str,"%dK,%d,%d%c",&bs,&ng,&np,&c)==3)
	{
	  bs *= 1024;
	  ret = 3;
	}
      if(ret!=3 && sscanf(str,"%dM,%d,%d%c",&bs,&ng,&np,&c)==3)
	{
	  bs *= (1024*1024);
	  ret = 3;
	}

      if(ret!=3 && sscanf(str,"%d,%d%c",&bs,&ng,&c)==2)
	{
	  np = ng;
	  ret = 3;
	}
      if(ret!=3 && sscanf(str,"%dK,%d%c",&bs,&ng,&c)==2)
	{
	  bs *= 1024;
	  np = ng;
	  ret = 3;
	}
      if(ret!=3 && sscanf(str,"%dM,%d%c",&bs,&ng,&c)==2)
	{
	  bs *= (1024*1024);
	  np = ng;
	  ret = 3;
	}

      if(ret!=3 || bs<0 || ng<1 || np<1)
	{
	  cart_error("Option --test must have 2 or 3 comma-separated positive integer numbers as its argument.");
	}

      if(bs == 0) bs = -1;

      artio_buffer_size = bs;
      num_artio_grid_files = ng;
#ifdef PARTICLES
      num_artio_particle_files = np;
#endif /* PARTICLES */

      cart_debug("io:artio-buffer-size = %d",bs);
      cart_debug("io:num-grid-files = %d",ng);
      cart_debug("io:num-particle-files = %d",np);

      if(apt_save)
	{
	  char tmp[CONTROL_PARAMETER_STRING_LENGTH];

	  sprintf(tmp,"%s/%03d",outdir,++n);
	  strncpy(output_directory_d,tmp,CONTROL_PARAMETER_STRING_LENGTH-1);
	  output_directory_d[CONTROL_PARAMETER_STRING_LENGTH-1] = 0;

	  if(system_mkdir(output_directory_d) != 0)
	    {
	      cart_error("Unable to create output directory %s",output_directory_d);
	    }
	}

      wtime = MPI_Wtime();
      write_artio_restart(WRITE_SAVE,WRITE_SAVE,WRITE_SAVE);
      cart_debug("Writing time: %lg sec",MPI_Wtime()-wtime);
    }

}
