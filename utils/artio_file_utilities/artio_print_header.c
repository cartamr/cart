#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <string.h>

#include "artio.h"
#include "artio_internal.h"

int main( int argc, char *argv[] ) {
	int i;
	int type, length, full_length;
	char key[64], dim[32];
	int *tmp_int;
	char **tmp_string;
	int64_t *tmp_long;
	double *tmp_double;
	float *tmp_float;
	int maxlen = 3;
	const char *ws = "                                                                                        ";
	const int tab = 4;

	if ( argc != 2 && argc != 3 ) {
		fprintf(stderr,"Usage: %s fileset_prefix [-l=<num>]\n",argv[0]);
		exit(1);
	}

	if ( argc == 3 && (sscanf(argv[2],"-l=%d%c",&maxlen,key)!=1 || maxlen<1 || maxlen>999999) ) {
		fprintf(stderr,"Option -l=<num> miust have a single psitive integer-valued argument\n",argv[2]);
		exit(1);
	}

	artio_fileset *handle = artio_fileset_open( argv[1], 0, NULL );
	if ( handle == NULL ) {
		fprintf(stderr,"Unable to open fileset %s\n", argv[1] );
		exit(1);
	}

	while ( artio_parameter_iterate( handle, key, &type, &full_length ) == ARTIO_SUCCESS ) {
		length = full_length;
		snprintf(dim,30,"%d",full_length);
		if ( length > maxlen ) length = maxlen;
		switch (type) {
			case ARTIO_TYPE_STRING :
				tmp_string = (char **)malloc( full_length*sizeof(char *) );
				for ( i = 0; i < full_length; i++ ) {
					tmp_string[i] = (char *)malloc( ARTIO_MAX_STRING_LENGTH*sizeof(char) );
				}
				artio_parameter_get_string_array(handle, key, full_length, tmp_string );

				if ( length > 1) printf("string[%s]%.*s | %-36s |",dim,tab-strlen(dim),ws,key); else printf("string%.*s | %-36s |",tab+2,ws,key);
				for ( i = 0; i < length; i++ ) {
					printf(" '%s'", tmp_string[i] );
					free( tmp_string[i] );
				}
				if ( length == maxlen ) printf("...\n"); else printf("\n");
				free(tmp_string);
				break;
			case ARTIO_TYPE_DOUBLE :
				tmp_double = (double *)malloc( full_length * sizeof(double) );
				artio_parameter_get_double_array(handle, key, full_length, tmp_double);

				if ( length > 1) printf("double[%s]%.*s | %-36s |",dim,tab-strlen(dim),ws,key); else printf("double%.*s | %-36s |",tab+2,ws,key); 
				for ( i = 0; i < length; i++ ) {
					printf(" %e", tmp_double[i] );
				}
				if ( length == maxlen ) printf("...\n"); else printf("\n");
				free(tmp_double);
				break;
			case ARTIO_TYPE_FLOAT :
				tmp_float = (float *)malloc( full_length * sizeof(float) );
				artio_parameter_get_float_array(handle, key, full_length, tmp_float);

				if ( length > 1) printf("float[%s]%.*s | %-36s |",dim,tab+1-strlen(dim),ws,key); else printf("float%.*s | %-36s |",tab+3,ws,key); 
				for ( i = 0; i < length; i++ ) {
					printf(" %e", tmp_float[i] );
				}
				if ( length == maxlen ) printf("...\n"); else printf("\n");
				free(tmp_float);
				break;
			case ARTIO_TYPE_LONG :
				tmp_long = (int64_t *)malloc( full_length * sizeof(int64_t) );
				artio_parameter_get_long_array(handle, key, full_length, tmp_long);

				if ( length > 1) printf("long[%s]%.*s | %-36s |",dim,tab+2-strlen(dim),ws,key); else printf("long%.*s | %-36s |",tab+4,ws,key); 
				for ( i = 0; i < length; i++ ) {
					printf(" %ld", tmp_long[i] );
				}
				if ( length == maxlen ) printf("...\n"); else printf("\n");
				free(tmp_long);
				break;
			case ARTIO_TYPE_INT :
				tmp_int = (int *)malloc( full_length * sizeof(int) );
				artio_parameter_get_int_array(handle, key, full_length, tmp_int);

				if ( length > 1) printf("int[%s]%.*s | %-36s |",dim,tab+3-strlen(dim),ws,key); else printf("int%.*s | %-36s |",tab+5,ws,key); 
				for ( i = 0; i < length; i++ ) {
					printf(" %d", tmp_int[i] );
				}
				if ( length == maxlen ) printf("...\n"); else printf("\n");
				free(tmp_int);
				break;
			default :
				fprintf(stderr, "ERROR: unknown ARTIO type %d\n", type );
				exit(1);
		}
	}

	artio_fileset_close(handle);
}
