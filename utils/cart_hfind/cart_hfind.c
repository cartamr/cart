#include "config.h"

#include <stdio.h>

#include "auxiliary.h"

#include "halos.h"
#include "halo_finder.h"

int init_analysis(int num_snapshots) {
	/* do nothing */
	return 0;
}

int main_analysis(int argc, const char **argv) {
	halo_list *list = find_halos();
	write_halo_list( list, halo_finder_output_directory );
	return 0;
}
