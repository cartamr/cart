
#define HYDRO
#define GRAVITY
#define COOLING
#define COSMOLOGY
#define PARTICLES
#define REFINEMENT
#define STAR_FORMATION
#define RADIATIVE_TRANSFER

#define SF_RECIPE			<hart>
#define SF_FEEDBACK                     <hart>
#define SF_FORMSTAR <hart>

#define num_root_grid_refinements	1
#define num_refinement_levels		0
#define num_octs			1
#define num_particles		        1
 
 
#define LB_RECIPE <sfc>
