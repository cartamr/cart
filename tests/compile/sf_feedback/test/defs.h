
#define HYDRO
#define GRAVITY
#define COOLING
#define COSMOLOGY
#define PARTICLES
#define REFINEMENT
#define STAR_FORMATION
#define STAR_PARTICLE_TYPES
#define DUST_EVOLUTION
#define RADIATIVE_TRANSFER

#define SGS_TURBULENCE
#define SGST_SN_SOURCE

#define ENRICHMENT
#define ENRICHMENT_SNIa

#define CLUSTER
#define CLUSTER_BOUND_FRACTION

#define num_root_grid_refinements   6
#define num_refinement_levels       9
#define num_octs                    1000000
#define num_particles               1000000
#define num_star_particles          1000000

#define LB_RECIPE <sfc>
