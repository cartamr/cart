#!/usr/bin/env python

import os
from glob import glob
import yt

for output in sorted(glob("dumps/outputs/kh_00*.art")):
    prefix = os.path.splitext(os.path.basename(output))[0]
    if not os.path.isfile("images/"+prefix+"_Slice_z_density.png"):
        ds = yt.load(output)
        yt.SlicePlot(ds, axis='z', fields="density").set_log("density",False).set_cmap("density","coolwarm").set_zlim("density",1.0,2.0).save("images/"+prefix)
