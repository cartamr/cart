#include "config.h"

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "tree.h"
#include "sfc.h"
#include "io.h"
#include "parallel.h"
#include "cell_buffer.h"
#include "lb.h"
#include "times.h"
#include "refinement.h"
#include "timing.h"
#include "units.h"
#include "hydro.h"
#include "iterators.h"
#include "auxiliary.h"
#include "cosmology.h"

#include "../run/step.h"
#include "../run/io_step.h"

#define SOFTENED
#define rho1      (2.0)
#define rho2      (1.0)
#define v1        (0.5)
#define v2        (-0.5)
#define P0        (2.5)
#define lambda    (0.5)
#define delta_vy  (0.1)
#define delta_y   (0.05)

extern double t_end;

void run_output() {
}

void init_run() {
	int i, j;
	double pos[nDim];
	int level, icell;
	int num_level_cells;
	int *level_cells;
	double y1, y2, R, vy;
	double tau_kh;

	lb_uniform_decomposition();

	/* set time variables */
	t_init = 0.0;
	simulation_time_init();

	/* set units */
	units_set( pow((double)num_grid,-3.0), 1.0, 1.0/(double)num_grid );
	units_init();

	/* create cell buffer */
	build_cell_buffer();

	for ( level = min_level; level <= max_level; level++ ) {
		select_level( level, CELL_TYPE_LOCAL, &num_level_cells, &level_cells );

		for ( i = 0; i < num_level_cells; i++ ) {
			icell = level_cells[i];

			cell_center_position(icell, pos);
			for ( j = 0; j < nDim; j++ ) {
				pos[j] *= units->length;
				cart_assert( pos[j] >= 0 && pos[j] < num_grid );
			}

			y1 = (pos[1]-0.25)/delta_y;
            y2 = (0.75-pos[1])/delta_y;
#ifdef SOFTENED
			/* softened initial conditions from Robertson et al (2010), Abel (2010) */
			R = pow((1.+exp(0.5/delta_y)),2)/((1.+exp(2*y1))*(1.+exp(2*y2)));
			vy = delta_vy*sin(M_PI*pos[0]/lambda);
#else
			/* instability seeded by grid-noise, Springel (2010) */
			R = (fabs(pos[1] - 0.5) < 0.25) ? 1.0: 0.0;
			vy = delta_vy*sin(M_PI*pos[0]/lambda)*(exp(-0.5*y1*y1) + exp(-0.5*y2*y2));
#endif

			cell_gas_density(icell) = (rho2 + R*(rho1-rho2))/units->density;
			cell_momentum(icell,0) = cell_gas_density(icell)*(v2 + R*(v1 - v2))/units->velocity;
			cell_momentum(icell,1) = cell_gas_density(icell)*vy/units->velocity;
			cell_momentum(icell,2) = 0.0;

			cell_gas_gamma(icell) = constants->gamma;
			cell_gas_pressure(icell) = P0/units->energy_density;

			cell_gas_internal_energy(icell) = cell_gas_pressure(icell)/(cell_gas_gamma(icell)-1.0);
			cell_gas_energy(icell) = cell_gas_internal_energy(icell)+cell_gas_kinetic_energy(icell);
#ifdef SGS_TURBULENCE
			cell_gas_turbulent_energy(icell) = 0.0;
#endif /* SGS_TURBULENCE */
		}

		cart_free( level_cells );

		update_buffer_level( level, all_hydro_vars, num_hydro_vars );
#ifdef REFINEMENT
		modify( level, OP_REFINE );
#endif
	}

#ifdef REFINEMENT
	for ( level = max_level-1; level >= min_level; level++ ) {
		hydro_split_update(level);
	}
#endif /* REFINEMENT */

#ifdef HYDRO_TRACERS
	cart_debug("setting hydro tracers");
	set_hydro_tracers( min_level );
#endif /* HYDRO_TRACERS */

	/* growth rate for lambda = 1 mode */
	tau_kh = (rho1+rho2)/sqrt(rho1*rho2)/fabs(v1-v2);
	t_end = 2.*tau_kh;

	write_restart(WRITE_SAVE, WRITE_SAVE, WRITE_SAVE);
}
