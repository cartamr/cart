#include "config.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include <mpi.h>

#include <sys/types.h>
#include <unistd.h>

#include "control_parameter.h"
#include "tree.h"
#include "cosmology.h"
#include "particle.h"
#include "sfc.h"
#include "parallel.h"
#include "cell_buffer.h"
#include "run/io_step.h"
#include "iterators.h"
#include "load_balance.h"
#include "run/step.h"
#include "run/starformation_step.h"
#include "run/hydro_step.h"
#include "refinement.h"
#include "refinement_indicators.h"
#include "refinement_operations.h"
#include "iterators.h"
#include "times.h"
#include "timing.h"
#include "units.h"
//#include "lb.h"

#include "hydro.h"
#include "hydro_tracer.h"
#include "gravity.h"
#include "density.h"
#include "io.h"
#include "auxiliary.h"
#include "starformation.h"
#include "plugin.h"


#include "rt_debug.h"

#include "disk.h"
#include "analysis.h"

//DEBUG
#include "cooling.h"

//#if (!defined(HYDRO) || !defined(PARTICLES) || !defined(STAR_FORMATION))
//#error "All of PARTICLES, HYDRO and STAR_FORMATION must be set."
//#endif

#ifdef COSMOLOGY
#error "COSMOLOGY should not be used."
#endif

#define NUM_VCIRC_POINTS_MAX 			10001
#define GAS_SWITCH_TO_AMBIENT_SCALE  	10.
#define INITIAL_REFINEMENT_SCALE  		2.
#define ICS_MASS_UNITS			  		(1e9*constants->Msun)
#define CODE_MASS_UNITS			  		((gas_mass_total+st_mass_total+dm_mass_total)*constants->Msun/(num_grid*num_grid*num_grid))

char ICs_dir[256] = ".";
char ICs_gas_fname[256] = "gas.dat";
char ICs_halo_fname[256] = "halo.dat";
char ICs_disk_fname[256] = "disk.dat";
char ICs_bulge_fname[256] = "bulge.dat";
char ICs_vcirc_fname[256] = "vcirc.dat";

double ICs_mass_units_Msun = 1e9;
double ICs_length_units_kpc = 1.;
double ICs_velocity_units_kms = 1.;

double ICs_Lbox = 2.62144;	//in Mpc (at 15th/16th/17th level the cell size would be 80/40/20 pc as required for LOW/MED/HIGH AGORA ICs)
int ICs_refine_to_level = MIN(6,max_level);

#ifdef PARTICLES
double disk_f_gas = 0.2;
#else
double disk_mass = 4.29661e10;
#endif
double disk_Z = 1.;

#ifdef SGS_TURBULENCE
double gas_vrms_disk = 1.;		//in km/s
double gas_vrms_ambient = 1.;	//in km/s
#endif
double gas_T_disk = 1e4;		//in K
double gas_T_ambient = 1e4;		//in K
double gas_n_ambient = 1e-6;	//in #/cc
double gas_r0 = 3.43218;		//in kpc
double gas_z0 = 0.343218;		//in kpc

double gas_mass_total;
double dm_mass_total;
double st_mass_total;
double stellar_disk_mass;
double gaseous_disk_mass;

double pos_disk_center[nDim]={num_grid/2.,num_grid/2.,num_grid/2.};

void init_disk_params() {
	// Initial conditions files:
	control_parameter_add2(control_parameter_string,ICs_dir,"ICs:dir","ICs_dir","Directory with ICs");
#ifdef PARTICLES
	control_parameter_add2(control_parameter_string,ICs_halo_fname,"ICs:halo","ICs_halo_fname","File with dark matter particles");
	control_parameter_add2(control_parameter_string,ICs_disk_fname,"ICs:disk","ICs_disk_fname","File with stellar disk particles");
	control_parameter_add2(control_parameter_string,ICs_bulge_fname,"ICs:bulge","ICs_bulge_fname","File with stellar bulge particles");
#endif
	control_parameter_add2(control_parameter_string,ICs_vcirc_fname,"ICs:vcirc","ICs_vcirc_fname","File with initial rotation curve");

	control_parameter_add2(control_parameter_double,&ICs_mass_units_Msun,"ICs:mass-units-Msun","ICs_mass_units_Msun","Mass units of ICs (in Msun).");
	control_parameter_add2(control_parameter_double,&ICs_length_units_kpc,"ICs:length-units-kpc","ICs_length_units_kpc","Length units of ICs (in kpc).");
	control_parameter_add2(control_parameter_double,&ICs_velocity_units_kms,"ICs:velocity-units-kms","ICs_velocity_units_kms","Velocity units of ICs (in km/s).");

	control_parameter_add2(control_parameter_double,&ICs_Lbox,"ICs:Lbox","ICs_Lbox","Box size. Default is 2.62144 Mpc. In this case at 15th/16th/17th level the cell size would be 80/40/20 pc as required for LOW/MED/HIGH AGORA ICs");
	control_parameter_add2(control_parameter_int,&ICs_refine_to_level,"ICs:level","ICs_refine_to_level","Initial refinement for ICs");

	// Disk parameters:
#ifdef PARTICLES
	control_parameter_add2(control_parameter_double,&disk_f_gas,"disk:f_gas","f_gas","Gas mass fraction of the disk");
#else
	control_parameter_add2(control_parameter_double,&disk_mass,"disk:mass","disk_mass","Gaseous disk mass in Msun");
#endif
	control_parameter_add2(control_parameter_double,&disk_Z,"disk:Z","disk_Z","Disk metallicity");

	// Gas parameters:
	control_parameter_add2(control_parameter_double,&gas_T_disk,"gas:T_disk","gas_T_disk","Gas temperature in disk (in K)");
	control_parameter_add2(control_parameter_double,&gas_T_ambient,"gas:T_ambient","gas_T_ambient","Ambient gas temperature (in K)");
	control_parameter_add2(control_parameter_double,&gas_n_ambient,"gas:n_ambient","gas_n_ambient","Ambient gas density (in #/cc)");
	control_parameter_add2(control_parameter_double,&gas_r0,"gas:r0","gas_r0","Gaseous disk scale radius (in kpc)");
	control_parameter_add2(control_parameter_double,&gas_z0,"gas:z0","gas_z0","Gaseous disk scale height (in kpc)");

#ifdef SGS_TURBULENCE
	control_parameter_add2(control_parameter_double,&gas_vrms_disk,"gas:vrms_disk","gas_vrms_disk","r.m.s. SGS turbulent velocity in disk (in km/s)");
	control_parameter_add2(control_parameter_double,&gas_vrms_ambient,"gas:vrms_ambient","gas_vrms_ambient","Ambient r.m.s. SGS turbulent velocity (in km/s)");
#endif
}

void verify_disk_params() {
	VERIFY(ICs:dir, ICs_dir[0] != '\0' );
#ifdef PARTICLES
	VERIFY(ICs:halo, ICs_halo_fname[0] != '\0' );
	VERIFY(ICs:disk, ICs_disk_fname[0] != '\0' );
	VERIFY(ICs:bulge, ICs_bulge_fname[0] != '\0' );
#endif
	VERIFY(ICs:vcirc, ICs_vcirc_fname[0] != '\0' );

	VERIFY(ICs:mass-units-Msun, ICs_mass_units_Msun > 0.0 );
	VERIFY(ICs:length-units-kpc, ICs_length_units_kpc > 0.0 );
	VERIFY(ICs:velocity-units-kms, ICs_velocity_units_kms > 0.0 );

	VERIFY(ICs:Lbox, ICs_Lbox >= 0.0 );
	VERIFY(ICs:level, ICs_refine_to_level >= min_level && ICs_refine_to_level <= max_level );

#ifdef PARTICLES
	VERIFY(disk:f_gas, disk_f_gas > 0.0 && disk_f_gas < 1.0 );
#else
	VERIFY(disk:mass, disk_mass > 0.0 );
#endif
	VERIFY(disk:Z, disk_Z > 0.0 );

#ifdef SGS_TURBULENCE
	VERIFY(gas:vrms_disk, gas_vrms_disk >= 0.0 );
	VERIFY(gas:vrms_ambient, gas_vrms_ambient >= 0.0 );
#endif

	VERIFY(gas:T_disk, gas_T_disk > 0.0 );
	VERIFY(gas:T_ambient, gas_T_ambient > 0.0 );
	VERIFY(gas:n_ambient, gas_n_ambient > 0.0 );
	VERIFY(gas:r0, gas_r0 > 0.0 );
	VERIFY(gas:z0, gas_z0 > 0.0 );
}

void set_particles_from_file();
void read_vcirc_file();
void refine_around_disk();
void set_hydro_constants();
void fill_grid();
void set_analytic_gas_model();
void rescale_mass();
void find_total_mass();


void init_run() {
    int i;
    int level;

	t_init = 0.0;
    units_set( ICS_MASS_UNITS, constants->Myr, ICs_Lbox*constants->Mpc/num_grid); /* mass time length */

	simulation_time_init();
	units_init();

	load_balance();

#ifdef PARTICLES
	set_particles_from_file();
#endif /* PARTICLES */

	for(i=0; i<num_particles; i++) if(particle_id[i]!=NULL_PARTICLE && particle_id_is_star(particle_id[i]))
	  {
	    if(particle_mass[i] > star_initial_mass[i]) cart_error("bug: wrong intial particle mass id=%d %g %g",particle_id[i],particle_mass[i],star_initial_mass[i]);
	  }


	//build_cell_buffer();
	repair_neighbors();

	read_vcirc_file();
	set_hydro_constants();

	fill_grid();
	refine_around_disk();

	set_analytic_gas_model();

	rescale_mass();
	units_set( CODE_MASS_UNITS, constants->Myr, ICs_Lbox*constants->Mpc/num_grid); /* mass time length */
	units_update(min_level);

	find_total_mass();

	for ( level = max_level; level >= min_level; level-- ) {
		hydro_split_update(level);
		modify( level, OP_DEREFINE );
		hydro_magic( level );
		hydro_eos(level);
	}
	for ( level = min_level; level <= max_level; level++ ) {
    	update_buffer_level( level, all_hydro_vars, num_hydro_vars );
    }

#ifdef PARTICLES
    build_particle_list();
#endif /* PARTICLES */

    load_balance();

    check_map();
	write_restart( WRITE_SAVE, WRITE_SAVE, WRITE_SAVE );

	cart_debug("done initialization");

	for(i=0; i<num_particles; i++) if(particle_id[i]!=NULL_PARTICLE && particle_id_is_star(particle_id[i]))
	  {
	    if(particle_mass[i] > star_initial_mass[i]) cart_error("bug: wrong intial particle mass id=%d %g %g",particle_id[i],particle_mass[i],star_initial_mass[i]);
	  }

}

#ifdef PARTICLES
void set_particles_from_file(){
	FILE *fd;
	char filename[256];

	particleid_t id = 0;
	int ipart, icell, dim;
	double xp[nDim];
	float vp[nDim];
	float mp, mp1;
	float mp_code;

	int num_part_node = 0;
	double mass_part_local;
	double stellar_disk_mass_local = 0.0;

	const double age_code = 1e9*constants->yr/units->time;

	num_particle_species = 2;
	particle_species_indices[num_particle_species-1] = num_particles;

	cart_debug("Reading particles from \'%s\'",ICs_dir);
	/*
	 * Reading dark matter particles:
	 */
	sprintf(filename,"%s/%s",ICs_dir,ICs_halo_fname);
	if ( !(fd = fopen(filename, "r")) ) cart_error("\tCannot open file with dark matter particles: \'%s\'",filename);
	cart_debug("\tReading dark matter particles from \'%s\'",filename);

	mass_part_local = 0.0;
	while ( fscanf(fd, "%le %le %le %e %e %e %e\n",&xp[0],&xp[1],&xp[2],&vp[0],&vp[1],&vp[2],&mp) == 7 ) {

		if( id == 0 ) {
			mp1 = mp;
			mp_code = mp*ICs_mass_units_Msun*constants->Msun/units->mass;
		} else {
			cart_assert( mp == mp1 );
		}

		for ( dim = 0; dim < nDim; dim++ ) {
			xp[dim] = xp[dim]*ICs_length_units_kpc*constants->kpc/units->length + pos_disk_center[dim];
		}

		icell = cell_find_position(xp);

		if ( icell >= 0 && cell_is_local(icell) ) {

			ipart = particle_alloc( id );

			for ( dim = 0; dim < nDim; dim++ ) {
				particle_x[ipart][dim] = xp[dim];
				particle_v[ipart][dim] = vp[dim]*ICs_velocity_units_kms*constants->kms/units->velocity;
			}

			particle_level[ipart] = min_level;
			particle_t[ipart] = tl[min_level];
			particle_dt[ipart] = 0.0;
			particle_mass[ipart] = mp_code;

			num_part_node++;
			mass_part_local += particle_mass[ipart];
		}

		id++;
	}

	particle_species_mass[0] = mp_code;
	num_particles_total += particle_species_num[0] = id;
	particle_species_indices[num_particle_species] = particle_species_indices[num_particle_species-1] = id;
	MPI_Allreduce( &mass_part_local, &dm_mass_total, 1, MPI_DOUBLE, MPI_SUM, mpi.comm.run );
	dm_mass_total *= units->mass/constants->Msun;

	cart_debug("\t\tTotal dm particles: %d",particle_species_num[0]);
	cart_debug("\t\tTotal particles on node: %d",num_part_node);
	cart_debug("\t\tParticle mass: %e Msun",particle_species_mass[0]*units->mass/constants->Msun);

	fclose(fd);

	/*
	 * Reading stellar particles:
	 */
	start_star_allocation();

	mass_part_local = 0.0;

	sprintf(filename,"%s/%s",ICs_dir,ICs_disk_fname);
	if ( !(fd = fopen(filename, "r")) ) cart_error("\tCannot open file with disk stellar particles: \'%s\'",filename);
	cart_debug("\tReading stellar particles (disk) from \'%s\'",filename);

	while ( fscanf(fd, "%le %le %le %e %e %e %e\n",&xp[0],&xp[1],&xp[2],&vp[0],&vp[1],&vp[2],&mp) == 7 ) {

		if( id == particle_species_num[0] ) {
			mp1 = mp;
			mp_code = mp*ICs_mass_units_Msun*constants->Msun/units->mass;
		} else {
			cart_assert( mp == mp1 );
		}

		for ( dim = 0; dim < nDim; dim++ ) {
			xp[dim] = xp[dim]*ICs_length_units_kpc*constants->kpc/units->length + pos_disk_center[dim];
		}

		icell = cell_find_position(xp);

		if ( icell >= 0 && cell_is_local(icell) ) {

			id = star_particle_alloc();
			ipart = particle_alloc( id );

			for ( dim = 0; dim < nDim; dim++ ) {
				particle_x[ipart][dim] = xp[dim];
				particle_v[ipart][dim] = vp[dim]*ICs_velocity_units_kms*constants->kms/units->velocity;
			}

			particle_level[ipart] = min_level;
			particle_t[ipart] = tl[min_level];
			particle_dt[ipart] = 0.0;
			particle_mass[ipart] = mp_code;

			star_tbirth[ipart] = particle_t[ipart] - age_code;
			star_initial_mass[ipart] = mp_code;
			/* don't count these stars as new star formation in logging */
			total_stellar_initial_mass += mp_code;
			total_stellar_mass += mp_code;
#ifdef ENRICHMENT
			star_metallicity_II[ipart] = disk_Z * constants->Zsun;
#ifdef ENRICHMENT_SNIa
			star_metallicity_Ia[ipart] = disk_Z * constants->Zsun;
#endif /* ENRICHMENT_SNIa */
#endif /* ENRICHMENT */

			num_part_node++;
			mass_part_local += particle_mass[ipart];
			stellar_disk_mass_local += particle_mass[ipart];
		}
	}
	MPI_Allreduce( &stellar_disk_mass_local, &stellar_disk_mass, 1, MPI_DOUBLE, MPI_SUM, mpi.comm.run );
	cart_debug("\t\tStellar disk mass: %e Msun",stellar_disk_mass*units->mass/constants->Msun);

	fclose(fd);

	sprintf(filename,"%s/%s",ICs_dir,ICs_bulge_fname);
	if ( !(fd = fopen(filename, "r")) ) cart_error("\tCannot open file with bulge stellar particles: \'%s\'",filename);
	cart_debug("\tReading stellar particles (bulge) from \'%s\'",filename);

	while ( fscanf(fd, "%le %le %le %e %e %e %e\n",&xp[0],&xp[1],&xp[2],&vp[0],&vp[1],&vp[2],&mp) == 7 ) {

		if( id == particle_species_num[0] ) {
			mp1 = mp;
			mp_code = mp*ICs_mass_units_Msun*constants->Msun/units->mass;
		} else {
			cart_assert( mp == mp1 );
		}

		for ( dim = 0; dim < nDim; dim++ ) {
			xp[dim] = xp[dim]*ICs_length_units_kpc*constants->kpc/units->length + pos_disk_center[dim];
		}

		icell = cell_find_position(xp);

		if ( icell >= 0 && cell_is_local(icell) ) {

			id = star_particle_alloc();
			ipart = particle_alloc( id );

			for ( dim = 0; dim < nDim; dim++ ) {
				particle_x[ipart][dim] = xp[dim];
				particle_v[ipart][dim] = vp[dim]*ICs_velocity_units_kms*constants->kms/units->velocity;
			}

			particle_level[ipart] = min_level;
			particle_t[ipart] = tl[min_level];
			particle_dt[ipart] = 0.0;
			particle_mass[ipart] = mp_code;

			star_tbirth[ipart] = particle_t[ipart] - age_code;
			star_initial_mass[ipart] = mp_code;
			/* don't count these stars as new star formation in logging */
			total_stellar_initial_mass += mp_code;
			total_stellar_mass += mp_code;
#ifdef ENRICHMENT
			star_metallicity_II[ipart] = disk_Z * constants->Zsun;
#ifdef ENRICHMENT_SNIa
			star_metallicity_Ia[ipart] = disk_Z * constants->Zsun;
#endif /* ENRICHMENT_SNIa */
#endif /* ENRICHMENT */

			num_part_node++;
			mass_part_local += particle_mass[ipart];
		}
	}


	end_star_allocation();

	particle_species_mass[num_particle_species-1] = 0.0;
	MPI_Allreduce( &mass_part_local, &st_mass_total, 1, MPI_DOUBLE, MPI_SUM, mpi.comm.run );
	st_mass_total *= units->mass/constants->Msun;

	cart_debug("\t\tStellar bulge mass: %e Msun",st_mass_total - stellar_disk_mass*units->mass/constants->Msun);
	cart_debug("\t\tTotal stellar particles: %d",particle_species_num[num_particle_species-1]);
	cart_debug("\t\tTotal particles on node: %d",num_part_node);
	cart_debug("done reading particles");
}
#endif /* PARTICLES */



double vcirc_dat[NUM_VCIRC_POINTS_MAX];
double rad_vcirc_dat[NUM_VCIRC_POINTS_MAX];
int num_vcirc_points = 1;

void read_vcirc_file() {
    FILE *fd;
    char filename[256];
    double rad_kpc, vcirc_kms;

    sprintf(filename,"%s/%s",ICs_dir,ICs_vcirc_fname);
    if ( !(fd = fopen(filename, "r")) ) cart_error("Cannot open vcirc file \'%s\'", filename);
    cart_debug("Reading vcirc from \'%s\'",filename);

    rad_vcirc_dat[0] = vcirc_dat[0] = 0.0;
    while ( fscanf(fd, "%lf %lf\n",&rad_kpc,&vcirc_kms) == 2 ) {
    	rad_vcirc_dat[num_vcirc_points] = rad_kpc*constants->kpc/units->length;
    	vcirc_dat[num_vcirc_points] = vcirc_kms*constants->kms/units->velocity;
    	num_vcirc_points++;
    }

    if ( feof(fd) ) {
    	fclose(fd);
    } else {
    	cart_error("vcirc file \'%s\' ended unexpectedly", filename);
    }

    cart_assert( num_vcirc_points <= NUM_VCIRC_POINTS_MAX);

    cart_debug("done reading vcirc");
}

double vcirc_from_file( double rad ) {
    double dr_bin, dr;
    int bin, bin2;

    int i;

    dr_bin = rad_vcirc_dat[2]-rad_vcirc_dat[1]; /* constant dr bins */
    bin = rad > rad_vcirc_dat[1] ? (int)((rad - rad_vcirc_dat[1]) / dr_bin) + 1 : 0 ;
    cart_assert( bin >= 0 && bin < num_vcirc_points );

    dr = (rad-rad_vcirc_dat[bin])/dr_bin ;
    bin2 = bin+1 < num_vcirc_points ? bin+1 : bin ;
    return vcirc_dat[bin]*(1-dr) + dr*vcirc_dat[bin2];
}

float ambient_density;
double r0_code, z0_code;
double rmax_code, zmax_code;
double rho0_code;

#define e_from_T(T,icell) (T/units->temperature/constants->wmu*cell_gas_density(icell)/(cell_gas_gamma(icell)-1.0))
#ifdef SGS_TURBULENCE
#define ambient_K(icell)  (ambient_vrms_factor*cell_gas_density(icell))
#define disk_K(icell)     (disk_vrms_factor*cell_gas_density(icell))
float ambient_vrms_factor;
float disk_vrms_factor;
#endif


void set_hydro_constants() {

	ambient_density = gas_n_ambient/constants->cc/units->number_density;

	r0_code = gas_r0*constants->kpc/units->length;
	z0_code = gas_z0*constants->kpc/units->length;

	rmax_code = GAS_SWITCH_TO_AMBIENT_SCALE*r0_code;
	zmax_code = GAS_SWITCH_TO_AMBIENT_SCALE*z0_code;

#ifdef PARTICLES
	gaseous_disk_mass = stellar_disk_mass*disk_f_gas/(1.0-disk_f_gas);
#else
	gaseous_disk_mass = disk_mass*constants->Msun/units->mass;
#endif
	rho0_code = gaseous_disk_mass/(4.*M_PI* r0_code*r0_code *z0_code );

#ifdef SGS_TURBULENCE
	ambient_vrms_factor = 0.5*pow(gas_vrms_disk*constants->kms/units->velocity , 2);
	disk_vrms_factor = 0.5*pow(gas_vrms_ambient*constants->kms/units->velocity , 2);
#endif
}

void refine_around_disk() {
	int i, icell, level, num_level_cells;
	int *level_cells;

	double pos[nDim];
	double z, r;
	double z_refine, r_refine;

	cart_debug("Refining cylindrical region ((r,z) = (%.3e,%.3e) code_length) to level %d",INITIAL_REFINEMENT_SCALE*zmax_code,INITIAL_REFINEMENT_SCALE*rmax_code,ICs_refine_to_level);

	for ( level = min_level; level < ICs_refine_to_level; level++ ) {

		z_refine = INITIAL_REFINEMENT_SCALE*MAX(cell_size[level],zmax_code);
		r_refine = INITIAL_REFINEMENT_SCALE*MAX(cell_size[level],rmax_code);

		select_level( level, CELL_TYPE_LOCAL | CELL_TYPE_LEAF, &num_level_cells, &level_cells );
		for ( i = 0; i < num_level_cells; i++ ) {
			icell = level_cells[i] ;

			cell_center_position(icell, pos);

			z = compute_distance_periodic_1d(pos[2], pos_disk_center[2]);

			pos[2] = pos_disk_center[2];

			r = compute_distance_periodic(pos, pos_disk_center);

			if ( z < z_refine && r < r_refine ) {
				refinement_indicator(icell,0) = 1.0;
			} else {
				refinement_indicator(icell,0) = 0.0;
			}
		}
		cart_free( level_cells );
		refine(level);
	}

}

void cell_ambient_conditions( int icell ){
    cell_gas_density(icell) = ambient_density;
    cell_momentum(icell,0) = 0.0;
    cell_momentum(icell,1) = 0.0;
    cell_momentum(icell,2) = 0.0;
    cell_gas_gamma(icell) = constants->gamma;
    cell_gas_internal_energy(icell) = e_from_T(gas_T_ambient,icell);
    cell_gas_pressure(icell) =  cell_gas_internal_energy(icell) * (cell_gas_gamma(icell)-1.0);
#ifdef SGS_TURBULENCE
    cell_gas_turbulent_energy(icell) = ambient_K(icell);
#endif /* SGS_TURBULENCE */
    cell_gas_energy(icell) = cell_gas_internal_energy(icell)+cell_gas_kinetic_energy(icell);

    #ifdef ENRICHMENT
    cell_gas_metal_density_II(icell) = 1e-20*constants->Zsun*cell_gas_density(icell);
#ifdef ENRICHMENT_SNIa
    cell_gas_metal_density_Ia(icell) = 1e-20*constants->Zsun*cell_gas_density(icell);
#endif /* ENRICHMENT_SNIa */
#endif /* ENRICHMENT */

#ifdef RADIATIVE_TRANSFER
    cell_HI_density(icell) = 0;
    cell_HII_density(icell) = cell_gas_density(icell)*constants->XH;
    cell_HeI_density(icell) = 0;
    cell_HeII_density(icell) = cell_gas_density(icell)*constants->XHe;
    cell_HeIII_density(icell) = 0;
    cell_H2_density(icell) = 0;
#endif
}

void fill_grid() {
	int i, level, num_level_cells;
	int *level_cells;

	for ( level = min_level; level <= max_level; level++ ) {
		select_level( level, CELL_TYPE_LOCAL, &num_level_cells, &level_cells );
		for ( i = 0; i < num_level_cells; i++ ) {
			cell_ambient_conditions(level_cells[i]);
		}
		cart_free( level_cells );
	}

}

void set_analytic_gas_model() {
	int i, icell, level, num_level_cells;
	int *level_cells;

	double z, r;
	double vcirc, ex, ey;
	double gas_mass_local = 0.0;

	double pos[nDim];

	float analytic_density;

	double Mtot = 0.;

	cart_debug("Initializing exponential gaseous disk: M_d = %.3e Msun, r_d = %.3e kpc, h_d = %.3e kpc",
			gaseous_disk_mass*units->mass/constants->Msun,
			gas_r0,gas_z0);

	for ( level = min_level; level <= max_level; level++ ) {

		select_level( level, CELL_TYPE_LOCAL | CELL_TYPE_LEAF, &num_level_cells, &level_cells );
		for ( i = 0; i < num_level_cells; i++ ) {
			icell = level_cells[i] ;

			cell_center_position(icell, pos);

			z = compute_distance_periodic_1d(pos[2], pos_disk_center[2]);

			pos[2] = pos_disk_center[2];

			r = compute_distance_periodic(pos, pos_disk_center);

			if ( z < zmax_code && r < rmax_code ) {

				analytic_density  = rho0_code*exp(-r/r0_code)*exp(-z/z0_code);
				Mtot += analytic_density*cell_volume[level];

				if ( analytic_density > ambient_density ) {

					cell_gas_density(icell) = analytic_density;

					vcirc = vcirc_from_file(r);
					ex = compute_displacement_periodic_1d(pos_disk_center[0],pos[0])/r;
					ey = compute_displacement_periodic_1d(pos_disk_center[1],pos[1])/r;

					cell_momentum(icell,0) = cell_gas_density(icell) * (-ey*vcirc);
					cell_momentum(icell,1) = cell_gas_density(icell) * ( ex*vcirc);
					cell_momentum(icell,2) = 0;

					cell_gas_gamma(icell) = constants->gamma;
					cell_gas_internal_energy(icell) =  e_from_T(gas_T_disk,icell);
					cell_gas_pressure(icell) =  cell_gas_internal_energy(icell) * (cell_gas_gamma(icell)-1.0);
#ifdef SGS_TURBULENCE
					cell_gas_turbulent_energy(icell) = disk_K(icell);
#endif /* SGS_TURBULENCE */
					cell_gas_energy(icell) = cell_gas_internal_energy(icell)+cell_gas_kinetic_energy(icell);

#ifdef ENRICHMENT
					cell_gas_metal_density_II(icell) = disk_Z * constants->Zsun*cell_gas_density(icell);
#ifdef ENRICHMENT_SNIa
					cell_gas_metal_density_Ia(icell) = 0;
#endif /* ENRICHMENT_SNIa */
#endif /* ENRICHMENT */

#ifdef RADIATIVE_TRANSFER
					/* start all singly ionized */
					cell_HI_density(icell) = 0;
					cell_HII_density(icell) = cell_gas_density(icell)*constants->XH;
					cell_HeI_density(icell) = 0;
					cell_HeII_density(icell) = cell_gas_density(icell)*constants->XHe;
					cell_HeIII_density(icell) = 0;
					cell_H2_density(icell) = 0;
#endif
				} else {
					cell_ambient_conditions(icell);
				}

			} else { /* ambient reset */
				cell_ambient_conditions(icell);
			}

			gas_mass_local += cell_gas_density(icell)*cell_volume[level];
		}
		cart_free( level_cells );
	}

	MPI_Allreduce( &gas_mass_local, &gas_mass_total, 1, MPI_DOUBLE, MPI_SUM, mpi.comm.run );
	gas_mass_total *= units->mass/constants->Msun;

	cart_debug("done initializing gaseous disk");
}


void rescale_mass() {
	int i, icell, level, num_level_cells;
	int *level_cells;

	const double mass_units_factor = ICS_MASS_UNITS/CODE_MASS_UNITS;

	cart_debug("Rescaling masses to code units");

	for ( level = min_level; level <= max_level; level++ ) {

		select_level( level, CELL_TYPE_LOCAL | CELL_TYPE_LEAF, &num_level_cells, &level_cells );
		for ( i = 0; i < num_level_cells; i++ ) {
			icell = level_cells[i] ;
			cell_gas_density(icell) *= mass_units_factor;
			cell_gas_internal_energy(icell) *= mass_units_factor;
#ifdef SGS_TURBULENCE
			cell_gas_turbulent_energy(icell) *= mass_units_factor;
#endif
			cell_gas_pressure(icell) *= mass_units_factor;
			cell_gas_energy(icell) *= mass_units_factor;
			cell_momentum(icell,0) *= mass_units_factor;
			cell_momentum(icell,1) *= mass_units_factor;
#ifdef ENRICHMENT
			cell_gas_metal_density_II(icell) *= mass_units_factor;
#ifdef ENRICHMENT_SNIa
			cell_gas_metal_density_Ia(icell) *= mass_units_factor;
#endif /* ENRICHMENT_SNIa */
#endif /* ENRICHMENT */

#ifdef RADIATIVE_TRANSFER
			cell_HI_density(icell) *= mass_units_factor;
			cell_HII_density(icell) *= mass_units_factor;
			cell_HeI_density(icell) *= mass_units_factor;
			cell_HeII_density(icell) *= mass_units_factor;
			cell_HeIII_density(icell) *= mass_units_factor;
			cell_H2_density(icell) *= mass_units_factor;
#endif
		}

		cart_free( level_cells );
	}

#ifdef PARTICLES
	for ( i = 0; i < num_particles; i++ ) {
		if ( particle_id[i] != NULL_PARTICLE ) {
			if ( particle_is_star(i) ) {
				particle_mass[i] *= mass_units_factor;
				star_initial_mass[i] *= mass_units_factor;
			} else {
				particle_mass[i] *= mass_units_factor;
			}
		}
	}

	particle_species_mass[0] *= mass_units_factor;
#endif /* PARTICLES */

	cart_debug("done rescaling masses");
}


void find_total_mass() {
	int i, level, icell;
	int num_level_cells;
	int *level_cells;

	double Total_gas_mass_local = 0.0, Total_gas_mass;
	double Total_dm_mass_local = 0.0, Total_dm_mass;
	double Total_st_mass_local = 0.0, Total_st_mass;

	double pos[3];

	for ( level = min_level; level <= max_level; level++ ) {
		select_level( level, CELL_TYPE_LOCAL | CELL_TYPE_LEAF, &num_level_cells, &level_cells );
		for ( i = 0; i < num_level_cells; i++ ) {
			icell = level_cells[i];
			Total_gas_mass_local += cell_gas_density(icell) * cell_volume[level];
		}
		cart_free(level_cells);
	}

#ifdef PARTICLES
	for ( i = 0; i < num_particles; i++ ) {
		if ( particle_id[i] != NULL_PARTICLE ) {
			if ( particle_is_star(i) ) {
				Total_st_mass_local += particle_mass[i];
			} else {
				Total_dm_mass_local += particle_mass[i];
			}

		}
	}
#endif

	MPI_Reduce( &Total_gas_mass_local, &Total_gas_mass, 1, MPI_DOUBLE, MPI_SUM, MASTER_NODE, mpi.comm.run );
	MPI_Reduce( &Total_st_mass_local, &Total_st_mass, 1, MPI_DOUBLE, MPI_SUM, MASTER_NODE, mpi.comm.run );
	MPI_Reduce( &Total_dm_mass_local, &Total_dm_mass, 1, MPI_DOUBLE, MPI_SUM, MASTER_NODE, mpi.comm.run );

	if ( local_proc_id == MASTER_NODE ) {
		cart_debug("");
		cart_debug("\t\tTotal gas mass \t\t%e \t(%e Msun)",Total_gas_mass,Total_gas_mass*units->mass/constants->Msun);
		cart_debug("\t\tTotal dark mass \t%e \t(%e Msun)",Total_dm_mass,Total_dm_mass*units->mass/constants->Msun);
		cart_debug("\t\tTotal stellar mass \t%e \t(%e Msun)",Total_st_mass,Total_st_mass*units->mass/constants->Msun);
		cart_debug("\t\tTotal part mass \t%e \t(%e Msun)",(Total_st_mass+Total_dm_mass),(Total_st_mass+Total_dm_mass)*units->mass/constants->Msun);
		cart_debug("\t\tTotal mass \t\t%e \t(%e Msun)",Total_gas_mass+Total_st_mass+Total_dm_mass,(Total_gas_mass+Total_st_mass+Total_dm_mass)*units->mass/constants->Msun);
		cart_debug("\t\tmean density \t\t%e", (Total_gas_mass+Total_st_mass+Total_dm_mass)/(num_grid*num_grid*num_grid));
		cart_debug("");
	}
}
