
#define HYDRO

/* SOD expected to run using single rank */
#define SFC_DOMAIN_DECOMPOSITION

#define num_root_grid_refinements   6
#define num_refinement_levels       0
#define num_octs                    (num_root_cells/num_children)
#define SFC                         SLAB


