#!/usr/bin/env python

try:
    import yt
except ImportError:
    import sys
    print "Error: yt is used to generate radial profiles and slices. See http://yt-project.org for installation instructions"
    sys.exit(1)

import os
from glob import glob
import sys

BASE_DIR="dumps/"
if len(sys.argv) > 1:
    BASE_DIR=sys.argv[1]

DATA_DIR=BASE_DIR+"outputs/"
SAVE_DIR=BASE_DIR+"images/"

fields = ["density", "temperature", "velocity_magnitude"]
y_log = { "density":False, "temperature":True, "velocity_magnitude":False }
cmap = { "density":"gist_heat", "temperature":"nipy_spectral", "velocity_magnitude":"bone" }

outputs = glob(DATA_DIR+"/sedov_00*.art")

# find variable bounds
ds = yt.load(outputs[-1])
fmin = { f:ds.find_min(f)[0] for f in fields }
fmax = { f:ds.find_max(f)[0] for f in fields }

for output in outputs:
    prefix = os.path.splitext(os.path.basename(output))[0]

    ds = yt.load(output)
    dx_min = ds.quan(2.**(-min(ds.max_level-2, 0)), 'code_length')

    # plot 1d profiles
    sph = ds.sphere(ds.domain_center, ds.domain_width[0]/2)
    profile = yt.create_profile(sph, "radius", fields,
                                extrema={'radius':(dx_min, ds.domain_width[0]/2.)},
                                n_bins=int(ds.domain_width[0]/2./dx_min),
                                logs={'radius':False},
                                units={'radius':'pc'})
    plot = yt.ProfilePlot.from_profiles(profile, y_log=y_log)
    for f in fields:
        plot.set_ylim(f, fmin[f], fmax[f])
    plot.save(SAVE_DIR+prefix)

    # plot 2d slices
    #plot = ds.slice(2, ds.domain_center[2]).to_pw(fields)
    #print plot.fields
    plot = yt.SlicePlot(ds, 2, fields)
    for f in fields:
        plot.set_log(f, y_log[f])
        plot.set_zlim(f, fmin[f], fmax[f])
        plot.set_cmap(f, cmap[f])
    plot.save(SAVE_DIR+prefix)
