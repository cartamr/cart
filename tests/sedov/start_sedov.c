#include "config.h"

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <mpi.h>

#include "auxiliary.h"
#include "tree.h"
#include "cell_buffer.h"
#include "cosmology.h"
#include "hydro.h"
#include "hydro_tracer.h"
#include "io.h"
#include "iterators.h"
#include "lb.h"
#include "particle.h"
#include "parallel.h"
#include "refinement.h"
#include "refinement_indicators.h"
#include "refinement_operations.h"
#include "sfc.h"
#include "times.h"
#include "timing.h"
#include "units.h"

#include "../run/io_step.h"

#ifdef VIEWDUMP
#include "../extras/viewdump.h"
#endif

#ifdef IFRIT
#include "../extra/cell_probe.h"
#include "../extra/ifrit.h"
#endif

#include "../run/step.h"


#define refine_radius   (4.5)  /* in cell lengths at each level */
#define sedov_radius    (cell_size[max_level])

/* ambient conditions in cgs */
#define rho0            (constants->mH)
#define T0              (1e6) /* K */
#define Lbox            (1.0)

/* energy of explosion */
#define E               (1e51) /* ergs */

void set_sedov_initial_conditions() {
	int i, j;
	int icell;
	int level;
	int num_level_cells;
	int *level_cells;
	double scale;
	double scale_energy = 0.0;
	double pos[nDim];
	double r;

	/* do initial refinements */
	for ( level = min_level; level < max_level; level++ ) {
		cart_debug("refining level %u", level );

		select_level( level, CELL_TYPE_LOCAL, &num_level_cells, &level_cells );
		for ( i = 0; i < num_level_cells; i++ ) {
			icell = level_cells[i];
			cell_center_position(icell, pos);

			r = 0.0;
			for ( j = 0; j < nDim; j++ ) {
				r += (pos[j]-0.5*num_grid)*(pos[j]-0.5*num_grid);
			}
			r = sqrt(r);

			if ( r < refine_radius*cell_size[level] ) {
				refinement_indicator(icell,0) = 1.0;
			} else {
				refinement_indicator(icell,0) = 0.0;
			}
		}
		cart_free( level_cells );
		refine(level);
	}

	/* set internal energy variable for all cells */
	for ( level = min_level; level <= max_level; level++ ) {
		select_level( level, CELL_TYPE_LOCAL, &num_level_cells, &level_cells );
		for ( i = 0; i < num_level_cells; i++ ) {
			icell = level_cells[i];
			cell_gas_internal_energy(icell) = 0.0;
			if ( cell_is_leaf(icell) ) {
				cell_center_position(icell, pos);

				r = 0.0;
				for ( j = 0; j < nDim; j++ ) {
					r += (pos[j]-0.5*num_grid)*(pos[j]-0.5*num_grid);
				}
				r = sqrt(r);

				if ( r <= 4.0*cell_size[min_level] ) {
					cell_gas_internal_energy(icell) = exp(-r*r / (2.0*sedov_radius*sedov_radius));
					scale_energy += cell_gas_internal_energy(icell)*cell_volume[level];
				}
			}
		}
		cart_free( level_cells );
	}

	scale = scale_energy;
	MPI_Allreduce( &scale, &scale_energy, 1, MPI_DOUBLE, MPI_SUM, mpi.comm.run );
	scale_energy = (E/units->energy) / scale_energy;

	/* rescale energy and set remaining variables */
	scale = 0;
	for ( level = min_level; level <= max_level; level++ ) {
		select_level( level, CELL_TYPE_LOCAL, &num_level_cells, &level_cells );
		for ( i = 0; i < num_level_cells; i++ ) {
			icell = level_cells[i];

			cell_gas_density(icell) = rho0/units->density;
			for ( j = 0; j < nDim; j++ ) {
				cell_momentum(icell,j) = 0.0;
			}
			cell_gas_gamma(icell) = constants->gamma;

			cell_gas_internal_energy(icell) = cell_gas_internal_energy(icell)*scale_energy +
				T0/units->temperature*cell_gas_density(icell)/(cell_gas_gamma(icell)-1.0)/constants->wmu;

			cell_gas_pressure(icell) = cell_gas_internal_energy(icell)*(cell_gas_gamma(icell)-1.0);
			cell_gas_energy(icell) = cell_gas_internal_energy(icell);
#ifdef SGS_TURBULENCE
			cell_gas_turbulent_energy(icell) = 0.0;
#endif /* SGS_TURBULENCE */
		}

		cart_free( level_cells );
	}

	/* set initial conditions for refined cells */
	for ( level = max_level - 1; level >= min_level; level-- ) {
		hydro_split_update(level);
	}
}

void run_output() {
	char filename[128];

#ifdef IFRIT
	char filelabel[128];
	const int nbin1 = 128;
	cell_probe_t probes[] = { cp_thermal_pressure, cp_gas_density, cp_temperature, cp_level, cp_task };
	int nbin[] = { nbin1, nbin1, nbin1 };
	double pos2[3];
	ifrit_config_t cfg = { mpi.comm.run, 0, 1, IFRIT_FLAG_WRITE_MESH };

	pos2[0] = pos2[1] = pos2[2] = 0.5*num_grid;

	sprintf(filename,"%s/out",output_directory);
	sprintf(filelabel,"%05d",step);
	ifritOutputRegion(filename,filelabel,max_level,nbin,pos2,sizeof(probes)/sizeof(cell_probe_t),probes,&cfg);
#endif /* IFRIT */

#ifdef VIEWDUMP
	sprintf(filename, "%s/%s_%04u.v", output_directory, jobname, step );
	viewdump( filename, max_level, ((float)(num_grid)/2.0)+0.5*cell_size[max_level], 2, DUMP_HVARS, CELL_TYPE_LOCAL );
#endif /* VIEWDUMP */

}

void init_run() {
	int level;
	double r0;

	/* distribute cells equally */
	lb_uniform_decomposition();

	/* set time variables */
	t_init = 0.0;
	simulation_time_init();

	/* 1 pc box at rho0 average density */
	r0 = Lbox*constants->pc/(double)num_grid;
 	units_set( rho0*pow(r0,3.0), constants->yr, r0 );
 	units_init();

	/* set simulation duration */
	t_end = constants->yr / units->time;

	/* build buffer */
	build_cell_buffer();

	cart_debug("setting initial conditions");
	set_sedov_initial_conditions();

#ifdef HYDRO_TRACERS
	cart_debug("setting hydro tracers");
	set_hydro_tracers( MIN(max_level, min_level+1) );
#endif /* HYDRO_TRACERS */

	cart_debug("set initial conditions");

	for ( level = min_level; level <= max_level; level++ ) {
		update_buffer_level( level, all_hydro_vars, num_hydro_vars );
	}

	cart_debug("done with initialization");

	write_restart(WRITE_SAVE, WRITE_SAVE, WRITE_SAVE);
}
