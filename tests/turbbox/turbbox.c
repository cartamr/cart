#include "config.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "plugin.h"
#include "control_parameter.h"
#include "auxiliary.h"
#include "parallel.h"
#include "tree.h"
#include "hydro.h"
#include "hydro_sgst.h"
#include "units.h"
#include "times.h"
#include "io.h"

char turbbox_ICs_fname[256] = "\0";
int ics_rgl;
int num_grid_input;

void turbbox_init();
void turbbox_verify();

plugin_t outPlugin = {NULL};

const plugin_t* add_plugin ( int id ) {
	if ( id == 0 ) {
		outPlugin.ConfigInit = turbbox_init;
		outPlugin.ConfigVerify = turbbox_verify;
		return &outPlugin;
	} else {
		return NULL;
	}
}

void turbbox_init() {
	control_parameter_add2(control_parameter_string,turbbox_ICs_fname,"turbbox:ICs","turbbox_ICs_fname","File with initial conditions");
}

void turbbox_verify() {
	VERIFY( turbbox:ICs, turbbox_ICs_fname[0] != '\0' );
}

#define 		L_BOX 					(1.0)
#define 		GAMMA 					(1.01)
#define 		SOUND_SPEED 			(1.0)

#define			TIME_UNITS				((SOUND_SPEED/L_BOX)/num_grid)
#define			LENGTH_UNITS			(L_BOX/num_grid)
#define			VOLUME_UNITS			(LENGTH_UNITS*LENGTH_UNITS*LENGTH_UNITS)
#define			MASS_UNITS				(1.0/(num_grid*num_grid*num_grid))


#define 		INITIAL_PROJECTION_NCELLS_1D 		(1<<(ics_rgl-num_root_grid_refinements))
#define 		INITIAL_PROJECTION_NCELLS_3D 		(INITIAL_PROJECTION_NCELLS_1D*INITIAL_PROJECTION_NCELLS_1D*INITIAL_PROJECTION_NCELLS_1D)
float *all_vals[4];


void average_vals(int x, int y, int z, float* rho, float* p, float* Ksgs);

float swap_bytes( const float init );

void init_run() {
	FILE *INPUT;

	int i, dir;
	int num_cells_input;

	int coords[3];
	int sfc;
	int icell;

	double rho_mean = 0.0;
	double rho_mean_reduced;

	int cur_val;

	float rho_f, p_f[nDim], Ksgs;
	float val;

	int do_not_convert_endian = 1;



	units_set( MASS_UNITS, TIME_UNITS, LENGTH_UNITS );
	build_cell_buffer();
	repair_neighbors();

	t_init /= TIME_UNITS;

	simulation_time_init();
	units_init();

	if( !(INPUT = fopen(turbbox_ICs_fname, "r")) ) cart_error("Cannot open ICs file \'%s\'", turbbox_ICs_fname);
	cart_debug("reading \'%s\'...",turbbox_ICs_fname);

	if ( fread(&ics_rgl,sizeof(int),1,INPUT) < 1 ) cart_error("ICs file ended unexpectedly.");

	if ( ics_rgl < 0 || ics_rgl > 12 ) {
		cart_debug("Invalid root grid level in ICs (%d). Switching endianness...", ics_rgl);
		do_not_convert_endian = 0;
		ics_rgl = swap_bytes(ics_rgl);
		if ( ics_rgl < 0 || ics_rgl > 12 ) {
			cart_error("Invalid root grid level in ICS (%d).",ics_rgl);
		}
	}

	if ( ics_rgl < num_root_grid_refinements ) {
		cart_error("Input grid level (%d) is smaller than num_root_grid_refinements (%d)",ics_rgl,num_root_grid_refinements);
	}

	num_grid_input = 1<<ics_rgl;
	num_cells_input = num_grid_input*num_grid_input*num_grid_input;

	cart_debug("Input root grid level: %d",ics_rgl);

	for ( cur_val = 0; cur_val < 4; cur_val++ ) {
		all_vals[cur_val] = cart_alloc(float, num_cells_input );
		for (i = 0; i < num_cells_input; i++) {
			if ( fread(&val,sizeof(float),1,INPUT) < 1 ) cart_error("ICs file ended unexpectedly. num_grid = %d   num_cells_input = %d", num_grid, num_cells_input);
			all_vals[cur_val][i] = do_not_convert_endian ? val : swap_bytes(val);
		}
		cart_debug("%d values of (%d) are read",i,cur_val);
	}

	getc(INPUT);
	if ( feof(INPUT) ) {
		cart_debug("ICs file is read. num_grid = %d, num_grid_input = %d", num_grid, num_grid_input);
		fclose(INPUT);
	} else {
		cart_error("ICs file contains more data then expected. num_grid = %d, num_grid_input = %d", num_grid, num_grid_input);
	}

	for ( coords[2] = 0; coords[2] < num_grid; coords[2]++ ) {
		for ( coords[1] = 0; coords[1] < num_grid; coords[1]++ ) {
			for ( coords[0] = 0; coords[0] < num_grid; coords[0]++ ) {

				sfc = sfc_index( coords );

				if ( root_cell_is_local(sfc) ) {
					icell = root_cell_location( sfc );
					average_vals(coords[0], coords[1], coords[2], &rho_f, p_f, &Ksgs);
					rho_mean += ( cell_gas_density(icell) = rho_f );
					cell_gas_gamma(icell) = GAMMA;
					cell_gas_pressure(icell) = GAMMA*SOUND_SPEED*SOUND_SPEED*cell_gas_density(icell);
					cell_gas_internal_energy(icell) = cell_gas_pressure(icell)/( (cell_gas_gamma(icell)-1.0) );
					for ( dir = 0; dir < nDim; dir++ ) {
						cell_momentum(icell,dir) = cell_gas_sound_speed(icell)/SOUND_SPEED * p_f[dir];
					}

#ifdef SGS_TURBULENCE
#ifdef NO_INITIAL_K
					cell_gas_internal_energy(icell) += Ksgs * cell_gas_sound_speed(icell)/SOUND_SPEED * cell_gas_sound_speed(icell)/SOUND_SPEED;
					cell_gas_turbulent_energy(icell) = 0.0;
#else
					cell_gas_turbulent_energy(icell) = Ksgs * cell_gas_sound_speed(icell)/SOUND_SPEED * cell_gas_sound_speed(icell)/SOUND_SPEED;
#endif /* NO_INITIAL_K */
					cell_gas_energy(icell) = cell_gas_kinetic_energy(icell) + cell_gas_internal_energy(icell) + cell_gas_turbulent_energy(icell);
#else
					cell_gas_internal_energy(icell) += Ksgs * cell_gas_sound_speed(icell)/SOUND_SPEED * cell_gas_sound_speed(icell)/SOUND_SPEED;
					cell_gas_energy(icell) = cell_gas_kinetic_energy(icell) + cell_gas_internal_energy(icell);
#endif


				}

			}
		}
	}

	for ( cur_val = 0; cur_val < 4; cur_val++ ) {
		cart_free( all_vals[cur_val] );
	}

	MPI_Reduce( &rho_mean, &rho_mean_reduced, 1, MPI_DOUBLE, MPI_SUM, MASTER_NODE, mpi.comm.run );

	if ( local_proc_id == MASTER_NODE ) {
		cart_debug("ICs are set. <rho> = %.10e",rho_mean_reduced/num_grid/num_grid/num_grid);
	}

	update_buffer_level( min_level, all_hydro_vars, num_hydro_vars );

}

float swap_bytes( const float init ) {
	float rev;
	char *init_bytes = (char*)&init;
	char *rev_bytes = (char*)&rev;

	rev_bytes[0] = init_bytes[3];
	rev_bytes[1] = init_bytes[2];
	rev_bytes[2] = init_bytes[1];
	rev_bytes[3] = init_bytes[0];

	return rev;
}


#define 	DEN			all_vals[0]
#define 	VEL(dir)	all_vals[dir+1]

#define ICELL_FILTER(x,y,z,i,j,k) 			(INITIAL_PROJECTION_NCELLS_1D*(num_grid_input*num_grid_input*z + num_grid_input*y + x) + num_grid_input*num_grid_input*k + num_grid_input*j + i)

void average_vals(int x, int y, int z, float* rho, float* p, float* Ksgs) {
	int i,j,k,dir;
	float rho_f = 0., p_f[3] = { 0. , 0. , 0. }, E_kin_tot = 0., E_kin_res = 0.;
	float cur_rho, cur_p;

	for ( i = 0; i < INITIAL_PROJECTION_NCELLS_1D; i++) {
		for ( j = 0; j < INITIAL_PROJECTION_NCELLS_1D; j++) {
			for ( k = 0; k < INITIAL_PROJECTION_NCELLS_1D; k++) {
				cur_rho = DEN[ ICELL_FILTER(x,y,z,i,j,k) ];
				rho_f += cur_rho;
				for ( dir = 0; dir < nDim; dir++ ) {
					cur_p = cur_rho * VEL(dir)[ ICELL_FILTER(x,y,z,i,j,k) ];
					p_f[dir] += cur_p;
					E_kin_tot += cur_p*cur_p/cur_rho;
				}
			}
		}
	}
	*rho = rho_f / INITIAL_PROJECTION_NCELLS_3D;
	E_kin_tot *= 0.5 / INITIAL_PROJECTION_NCELLS_3D;
	for ( dir = 0; dir < nDim; dir++ ) {
		*(p+dir) = p_f[dir] / INITIAL_PROJECTION_NCELLS_3D;
		E_kin_res += (*(p+dir)) * (*(p+dir));
	}
	E_kin_res *= 0.5 / (*rho);

	*Ksgs = E_kin_tot - E_kin_res;
}

void run_output() {

}

