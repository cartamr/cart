#include "config.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "auxiliary.h"
#include "lb.h"

#ifdef SFC_DOMAIN_DECOMPOSITION
#error "No-op load balancer is intended as an example only, and is incompatible with SFC_DOMAIN_DECOMPOSITION"
#endif

void lb_decompose( int num_local_root_cells,
		sfc_t *local_sfc_list, float *local_work,
		int num_constrained_quantities,
		int *local_constrained_quantities,
		int *per_proc_constraints,
		int *boundary_level,
		lb_decomposition *decomposition ) {

	/*
	//  All that needs to be called by a LB algorithm
	*/
	lb_set_sfc_list(decomposition, num_local_root_cells, local_sfc_list);
}
