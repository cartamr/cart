#include "config.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>

#include "auxiliary.h"
#include "lb.h"
#include "parallel.h"
#include "rand.h"
#include "sfc.h"


#define num_lb_procs 4  // number of procs used by lb algorithm

#if ( num_grid % num_lb_procs != 0 )
#error "invalid num_lb_procs"
#endif


// ---------------------------------------------------------------------------------
// global variables
// ---------------------------------------------------------------------------------
int lsz[3];         // grid size assigned to my rank
int ori[3];         // the origin (x, y, z) of my partition
int local_cells;    // number of local cells

const int maxneighbors = 10;

MPI_Comm comm_lb;

// data structures
struct list_node
{
    uintptr_t data;
    struct list_node * next;
    struct list_node * prev;
};

struct list
{
    struct list_node * first;
    struct list_node * last;
    int size;
};

struct cell
{
    int coords[nDim];

    char lv;             // floor level
    char bl[nDim * 2];   // boundary level

    double work;
};

struct proc_bin
{
    struct list * groups;   // groups belong to the bin
    int bid;                // bin id, start from 0
    double work;            // total work in the bin
};

struct g_neighbor
{
    int grp;       // index to the neighboring group
    double dist;   // distance to the group
};

// meta data for a group
struct group_meta
{
    int gid;       // group id
    int lv;        // level
    int size;      // group size
    int size_leaf; // # of leaf cells in the group

    double work;      // total work for all cells in the group
    double work_leaf; // total work for leaf cells in the group

    double sumx;      // the sum of all x-indicies of cells in the group
    double sumy;      // the sum of all y-indicies
    double sumz;      // the sum of all z-indicies
};

// array of leaf cell indexes of a group
struct group_cells
{
    int * cells;   // array of (leaf) cell indicies
    int size;      // # of leaf cells in the group
    int buf_size;  // buffer size (>=size)
};

// group tree related: how does the group connected with other groups
struct group_links
{
    int parent;
    int nsib;
    int psib;
    int child;
    int last_child;
};

struct group_info
{
    struct group_meta  * gms;
    struct group_cells * gcs;
    struct group_links * gls;

    int ngrp;
    int ngrp_slots;
};

struct group_meta_extra
{
    int proc;
    int gid;

    int size;
    double work;

    double x;
    double y;
    double z;
};

struct group_extra
{
    struct list ** gns;              // list of neighbors
    struct proc_bin ** gbs;          // belongs to which bin
    struct group_meta_extra * gms;  // meta 

    int ngrp;
};

struct group_dst
{
    int gid;
    int dst_proc;
};

struct gti_extract_aux
{
    struct cell * cells;
    sfc_t * buf;
    int count;
};


// ---------------------------------------------------------------------------------
// cell indexing methods
// ---------------------------------------------------------------------------------

// local cell indexing
//   x - y periodic, z non-periodic
inline int cell_idx(int coord[nDim])
{
    int i;

    if ( coord[nDim-1] < ori[nDim-1] || coord[nDim-1] >= lsz[nDim-1] + ori[nDim-1] )
        return -1;

    for ( i = 0; i < nDim - 1; ++i )
    {
        if ( coord[i] >= lsz[i] ) coord[i] -= lsz[i];
        if ( coord[i] < 0 )       coord[i] += lsz[i];
    }

#if nDim == 3
    return (coord[2] - ori[2]) * lsz[1] * lsz[0] + coord[1] * lsz[0] + coord[0];
#elif nDim == 2
    return (coord[1] - ori[1]) * lsz[0] + coord[0];
#endif
}


// reversed indexing for local cells
inline void idx_cell(int idx, int coord[nDim])
{
#if nDim == 3
    cart_assert ( idx >= 0 && idx < lsz[0] * lsz[1] * lsz[2] );

    coord[2] =  idx / (lsz[0] * lsz[1]) + ori[2];
    coord[1] = (idx - (coord[2] - ori[2]) * lsz[0] * lsz[1]) / lsz[0];
    coord[0] =  idx - (coord[2] - ori[2]) * lsz[0] * lsz[1] - coord[1] * lsz[0];
#elif nDim == 2
#error not implemented
#endif
}

int sfc_compare(const void * a, const void * b)
{
    return ( *(sfc_t *)a - *(sfc_t *)b );
}


// ---------------------------------------------------------------------------------
// list methods
// ---------------------------------------------------------------------------------

// create and init a list object
struct list * list_create()
{
    struct list * lst = cart_alloc(struct list, 1);

    lst->first = 0;
    lst->last  = 0;
    lst->size  = 0;

    return lst;
}

// reclaim the memory from a list
void free_list(struct list * l)
{
    if ( l == 0 ) return;

    struct list_node * n = l->first;
    while ( n != 0 )
    {
        struct list_node * nn = n->next;
        cart_free(n);
        n = nn;
    }

    cart_free(l);

    l = 0;
}

// push the pointer ptr(which is a payload) to the front of the list
void list_push_node_front(struct list * l, struct list_node * node)
{
    cart_assert(l    != 0);
    cart_assert(node != 0);

    if (l->size == 0)
    {
        node->prev = 0;
        node->next = 0;

        l->first = node;
        l->last  = node;

        l->size  = 1;
    }
    else
    {
        node->prev = 0;
        node->next = l->first;

        l->first->prev = node;
        l->first = node;

        ++(l->size);
    }
}

void list_push_front(struct list * l, uintptr_t data)
{
    cart_assert(l != 0);

    struct list_node * node = cart_alloc(struct list_node, 1);

    node->data = data;
    node->next = 0;
    node->prev = 0;

    list_push_node_front(l, node);
}

// push the pointer ptr(which is a payload) to the tail of the list
void list_push_node_end(struct list * l, struct list_node * node)
{
    cart_assert( l    != 0 );
    cart_assert( node != 0 );

    if (l->size == 0)
    {
        node->prev = 0;
        node->next = 0;

        l->first = node;
        l->last  = node;

        l->size  = 1;
    }
    else
    {
        node->next = 0;
        node->prev = l->last;

        node->prev->next = node;
        l->last = node;

        ++l->size;
    }
}

void list_push_end(struct list * l, uintptr_t data)
{
    cart_assert(l != 0);

    struct list_node * node = cart_alloc(struct list_node, 1);

    node->data = data;
    node->next = 0;
    node->prev = 0;

    list_push_node_end(l, node);
}

void list_insert_after(struct list * l, struct list_node * node, uintptr_t data)
{
    cart_assert(l != 0);
    cart_assert(node != 0);

    // append at tail
    if (node->next == 0)
    {
        list_push_end(l, data);
        return;
    }

    struct list_node * n = cart_alloc(struct list_node, 1);

    n->data = data;

    n->next = node->next;
    n->prev = node;
    n->next->prev = n;
    node->next = n;

    ++l->size;
}

void list_insert_node_before(struct list * l, struct list_node * pos, struct list_node * node)
{
    cart_assert( l    != 0 );
    cart_assert( pos  != 0 );
    cart_assert( node != 0 );
    cart_assert( node != pos );

    // insert at head
    if (pos->prev == 0)
    {
        list_push_node_front(l, node);
        return;
    }

    node->next = pos;
    node->prev = pos->prev;
    node->prev->next = node;
    pos->prev = node;

    ++l->size;
}

void list_insert_before(struct list * l, struct list_node * pos, uintptr_t data)
{
    struct list_node * node = cart_alloc(struct list_node, 1);

    node->data = data;
    node->prev = 0;
    node->next = 0;

    list_insert_node_before(l, pos, node);
}

// move node to before the pos
void list_move_node(struct list * l, struct list_node * node, struct list_node * pos)
{
    cart_assert( l    != 0 );
    cart_assert( node != 0 );
    cart_assert( node != pos );

    // do nothing if node->next = pos
    if (node->next == pos)
        return;

    // remove node from the list
    if (l->first == node)
    {
        node->next->prev = 0;
        l->first = node->next;
    }
    else
    {
        node->next->prev = node->prev;
        node->prev->next = node->next;
    }

    --(l->size);

    node->next = 0;
    node->prev = 0;

    // insert node at (before) pos
    if (pos != 0)
    {
        list_insert_node_before(l, pos, node);
    }
    else
    {
        list_push_node_end(l, node);
    }
}

#ifndef NDEBUG

void validate_list(struct list * l) { }
void validate_list_asc(struct list * l) { }

#else

void validate_list(struct list * l)
{
    cart_assert( l != 0 );

    if (l->size == 0)
    {
        cart_assert( l->first == 0 );
        cart_assert( l->last  == 0 );
        return;
    }

    if (l->size == 1)
    {
        cart_assert( l->first == l->last );
        cart_assert( l->first->prev == 0 );
        cart_assert( l->last ->next == 0 );
        return;
    }

    cart_assert( l->first != l->last );
    cart_assert( l->first->prev == 0 );
    cart_assert( l->last ->next == 0 );

    int count = 0, i;
    struct list_node ** nodes = cart_alloc(struct list_node *, l->size);

    struct list_node * node = l->first;
    while (node != 0)
    {
        nodes[count] = node;
        ++count;

        cart_assert(count <= l->size);
        
        for (i = 0; i<count-1; ++i)
            cart_assert(nodes[i] != node);

        node = node->next;
    }

    cart_assert( count == l->size );

    cart_free(nodes);
}

void validate_list_asc(struct list * l)
{
    cart_assert( l != 0 );

    if (l->size == 0)
    {
        cart_assert( l->first == 0 );
        cart_assert( l->last  == 0 );
        return;
    }

    if (l->size == 1)
    {
        cart_assert( l->first == l->last );
        cart_assert( l->first->prev == 0 );
        cart_assert( l->last ->next == 0 );
        return;
    }

    cart_assert( l->first != l->last );
    cart_assert( l->first->prev == 0 );
    cart_assert( l->last ->next == 0 );

    struct list_node * node = l->first;
    struct list_node * nnode = node->next;

    while (nnode != 0)
    {
        double wn  = ((struct proc_bin *)(node->data))->work;
        double wnn = ((struct proc_bin *)(nnode->data))->work;
        cart_assert( wn <= wnn );

        node = node->next;
        nnode = nnode->next;
    }
}

#endif

// get front without popping it
uintptr_t list_front(struct list * l)
{
    cart_assert(l != 0);
    cart_assert(l->size != 0);

    return l->first->data;
}

uintptr_t list_end(struct list *l)
{
    cart_assert(l != 0);
    cart_assert(l->size != 0);

    return l->last->data;
}

// get front and pop it
uintptr_t list_pop_front(struct list * l)
{
    cart_assert(l != 0);
    cart_assert(l->size != 0);

    uintptr_t data = l->first->data;

    if (l->size == 1)
    {
        cart_free(l->first);

        l->first = 0;
        l->last  = 0;
        l->size  = 0;
    }
    else
    {
        struct list_node * node = l->first->next;
        cart_free(l->first);

        node->prev = 0;
        l->first = node;
        --l->size;
    }

    return data;
}

uintptr_t list_pop_end(struct list * l)
{
    cart_assert(l != 0);
    cart_assert(l->size != 0);

    uintptr_t data = l->last->data;

    if (l->size == 1)
    {
        cart_free(l->last);

        l->first = 0;
        l->last  = 0;
        l->size  = 0;
    }
    else
    {
        struct list_node * node = l->last->prev;
        cart_free(l->last);

        node->next = 0;
        l->last = node;
        --l->size;
    }

    return data;
}

// ---------------------------------------------------------------------------------
// group methods
// ---------------------------------------------------------------------------------

// create and init a new group, return the gid
int group_create(struct group_info * ginfo, int lv)
{
    cart_assert(ginfo != NULL);

    int gid = ginfo->ngrp;

    // time to expand the buffer
    if (gid >= ginfo->ngrp_slots)
    {
        int increased = 0;

        if (ginfo->ngrp_slots == 0)
        {
            cart_assert(ginfo->gms == NULL);
            cart_assert(ginfo->gcs == NULL);
            cart_assert(ginfo->gls == NULL);

            ginfo->ngrp_slots = 1000;
            increased = 1000;
        }
        else
        {
            cart_assert(ginfo->gms != NULL);
            cart_assert(ginfo->gcs != NULL);
            cart_assert(ginfo->gls != NULL);

            ginfo->ngrp_slots *= 1.5;
            increased = ginfo->ngrp_slots / 3;
        }

        ginfo->gms = realloc(ginfo->gms, sizeof(struct group_meta)  * ginfo->ngrp_slots);
        ginfo->gcs = realloc(ginfo->gcs, sizeof(struct group_cells) * ginfo->ngrp_slots);
        ginfo->gls = realloc(ginfo->gls, sizeof(struct group_links) * ginfo->ngrp_slots);

    }

    cart_assert(gid < ginfo->ngrp_slots);

    // init the new group
    ginfo->gms[gid].gid        = gid;
    ginfo->gms[gid].lv         = lv;
    ginfo->gms[gid].size       = 0;
    ginfo->gms[gid].size_leaf  = 0;
    ginfo->gms[gid].work       = 0.0;
    ginfo->gms[gid].work_leaf  = 0.0;
    ginfo->gms[gid].sumx       = 0.0;
    ginfo->gms[gid].sumy       = 0.0;
    ginfo->gms[gid].sumz       = 0.0;

    ginfo->gcs[gid].size       = 0;
    ginfo->gcs[gid].buf_size   = 4;
    ginfo->gcs[gid].cells      = cart_alloc(int, ginfo->gcs[gid].buf_size);

    ginfo->gls[gid].parent     = -1;
    ginfo->gls[gid].nsib       = -1;
    ginfo->gls[gid].psib       = -1;
    ginfo->gls[gid].child      = -1;
    ginfo->gls[gid].last_child = -1;

    //grp->neighbors  = list_create();
    //grp->bin        = 0;

    // update the number of groups
    ginfo->ngrp = gid + 1;

    return gid;
}

// insert the grp into parent, sorted by group sizes
void group_insert_ordered(int parent, int grp, struct group_info * ginfo)
{
    cart_assert(parent != -1);
    cart_assert(parent < ginfo->ngrp);

    cart_assert(grp != -1);
    cart_assert(grp < ginfo->ngrp);

    // init
    ginfo->gls[grp].parent     = parent;
    ginfo->gls[grp].child      = -1;
    ginfo->gls[grp].last_child = -1;

    if (ginfo->gls[parent].child == -1)
    {
        // grp is the only child
        ginfo->gls[parent].child = grp;
        ginfo->gls[parent].last_child = grp;

        ginfo->gls[grp].nsib = -1;
        ginfo->gls[grp].psib = -1;
    }
    else
    {
        // sib is the first child of parent
        int sib = ginfo->gls[parent].child;

        if (ginfo->gms[grp].size >= ginfo->gms[sib].size)
        {
            // insert at head
            ginfo->gls[parent].child = grp;
            ginfo->gls[grp].nsib = sib;
            ginfo->gls[grp].psib = -1;
            ginfo->gls[sib].psib = grp;
        }
        else
        {
            // find the insert point
            while (ginfo->gms[grp].size < ginfo->gms[sib].size && ginfo->gls[sib].nsib != -1)
                sib = ginfo->gls[sib].nsib;

            if (ginfo->gms[grp].size < ginfo->gms[sib].size)
            {
                // insert at tail
                ginfo->gls[parent].last_child = grp;
                ginfo->gls[grp].nsib = -1;
                ginfo->gls[grp].psib = sib;
                ginfo->gls[sib].nsib = grp;
            }
            else
            {
                // insert before sib
                ginfo->gls[grp].nsib = sib;
                ginfo->gls[grp].psib = ginfo->gls[sib].psib;
                ginfo->gls[ginfo->gls[sib].psib].nsib = grp;
                ginfo->gls[sib].psib = grp;
            }
        }
    }
}

// actual insert a cell index to the group's leaf-cell list
void group_add_leaf_cell(int grp, struct group_info * ginfo, int cell)
{
    cart_assert( grp >= 0 && grp < ginfo->ngrp );

    if (ginfo->gcs[grp].size >= ginfo->gcs[grp].buf_size)
    {
        ginfo->gcs[grp].buf_size *= 1.5;
        ginfo->gcs[grp].cells = realloc(ginfo->gcs[grp].cells, ginfo->gcs[grp].buf_size * sizeof(int));
    }

    ginfo->gcs[grp].cells[ ginfo->gcs[grp].size ] = cell;
    ginfo->gcs[grp].size ++;
}

// add a new cell to the group
void group_add_cell(int grp, struct group_info * ginfo, int x, int y, int z, struct cell * cells)
{
    cart_assert( grp >= 0 && grp < ginfo->ngrp );

    // cell index
    int coord[3] = { x, y, z };
    int cell = cell_idx(coord);

    cart_assert( cell >= 0 && cell < local_cells );

    // increment the group size
    ginfo->gms[grp].size++;
 
    // and total work of the group
    double work = cells[cell].work;
    ginfo->gms[grp].work += work;

    // add x, y, and z of cell to sumx, sumy, and sumz
    ginfo->gms[grp].sumx += x;
    ginfo->gms[grp].sumy += y;
    ginfo->gms[grp].sumz += z;

    // is this cell a leaf at group lv?
    if (cells[cell].lv == ginfo->gms[grp].lv)
    {
        // leaf work/size
        ginfo->gms[grp].work_leaf += work;
        ginfo->gms[grp].size_leaf ++;

        // insert the cell to the list
        group_add_leaf_cell(grp, ginfo, cell);
    }
}

// transfer a leaf cell from source group (gs) to target group (gt)
void group_transfer_leaf(int gs, int gt, struct group_info * ginfo, struct cell * cells)
{
    cart_assert( gs >= 0 && gs < ginfo->ngrp );
    cart_assert( gt >= 0 && gt < ginfo->ngrp );

    cart_assert( ginfo->gms[gs].size_leaf == ginfo->gms[gs].size );
    cart_assert( ginfo->gms[gs].size_leaf == ginfo->gcs[gs].size );

    cart_assert( ginfo->gms[gt].size_leaf == ginfo->gms[gt].size );
    cart_assert( ginfo->gms[gt].size_leaf == ginfo->gcs[gt].size );

    cart_assert( ginfo->gms[gs].size_leaf > 0 );

    // identify the cell and work
    int cell = ginfo->gcs[gs].cells[ ginfo->gcs[gs].size - 1];
    double work = cells[cell].work;

    //cart_debug("cell = %d, work = %f", cell, work);

    int coord[3];
    idx_cell(cell, coord);

    // source group
    ginfo->gms[gs].size--;
    ginfo->gms[gs].size_leaf--;
    ginfo->gms[gs].work      -= work;
    ginfo->gms[gs].work_leaf -= work;
    ginfo->gms[gs].sumx      -= coord[0];
    ginfo->gms[gs].sumy      -= coord[1];
    ginfo->gms[gs].sumz      -= coord[2];

    // shrink the source leaf cell list by one
    ginfo->gcs[gs].size--;

    // target group
    ginfo->gms[gt].size++;
    ginfo->gms[gt].size_leaf++;
    ginfo->gms[gt].work      += work;
    ginfo->gms[gt].work_leaf += work;
    ginfo->gms[gt].sumx      += coord[0];
    ginfo->gms[gt].sumy      += coord[1];
    ginfo->gms[gt].sumz      += coord[2];

    // expand the target leaf cell list by one
    group_add_leaf_cell(gt, ginfo, cell);

    // post condition
    cart_assert( ginfo->gms[gs].size_leaf == ginfo->gms[gs].size );
    cart_assert( ginfo->gms[gs].size_leaf == ginfo->gcs[gs].size );

    cart_assert( ginfo->gms[gt].size_leaf == ginfo->gms[gt].size );
    cart_assert( ginfo->gms[gt].size_leaf == ginfo->gcs[gt].size );
}


int group_has_child(int grp, struct group_info * ginfo)
{
    cart_assert( grp >= 0 && grp < ginfo->ngrp );
    return ( ginfo->gls[grp].child != -1 );
}

int group_rip_first_child(int grp, struct group_info * ginfo)
{
    cart_assert(ginfo != NULL);
    cart_assert(grp >= 0 && grp < ginfo->ngrp);
    cart_assert(group_has_child(grp, ginfo));

    int g = ginfo->gls[grp].child;

    // adjust group size and work
    // note that leaf_work and leaf_cells will not be affected
    ginfo->gms[grp].work -= ginfo->gms[g].work;
    ginfo->gms[grp].size -= ginfo->gms[g].size;

    ginfo->gms[grp].sumx -= ginfo->gms[g].sumx;
    ginfo->gms[grp].sumy -= ginfo->gms[g].sumy;
    ginfo->gms[grp].sumz -= ginfo->gms[g].sumz;

    // only child?
    if (ginfo->gls[grp].child == ginfo->gls[grp].last_child)
    {
        ginfo->gls[grp].child      = -1;
        ginfo->gls[grp].last_child = -1;
    }
    else
    {
        ginfo->gls[grp].child = ginfo->gls[g].nsib;
        ginfo->gls[ ginfo->gls[grp].child ].psib = -1;
    }

    // cut loose ties from the parent and siblings
    ginfo->gls[g].parent = -1;
    ginfo->gls[g].nsib   = -1;
    ginfo->gls[g].psib   = -1;

    return g;
}

void free_group_info(struct group_info * ginfo)
{
    int i;

    for ( i = 0; i < ginfo->ngrp; ++i )
        cart_free(ginfo->gcs[i].cells);

    cart_free(ginfo->gms);
    cart_free(ginfo->gcs);
    cart_free(ginfo->gls);

    ginfo->gms = 0;
    ginfo->gcs = 0;
    ginfo->gls = 0;
    ginfo->ngrp = 0;
    ginfo->ngrp_slots = 0;
}

void free_group_extra(struct group_extra * gextra)
{
    int i;

    if ( gextra->gns != 0 )
        for ( i = 0; i < gextra->ngrp; ++i )
            free_list(gextra->gns[i]);

    cart_free(gextra->gms);
    cart_free(gextra->gbs);
    cart_free(gextra->gns);

    gextra->gms = 0;
    gextra->gbs = 0;
    gextra->gns = 0;
    gextra->ngrp = 0;
}

/* ------------------------------------------------------------------------
 * group tree iterate
 * ------------------------------------------------------------------------
 */

typedef void(*gti_cb)(int, struct group_info *, void *);

void gti_cb_leaf_count(int grp, struct group_info * ginfo, void * param)
{
    int * count = (int *)param;
    *count += ginfo->gms[grp].size_leaf;
}

void gti_cb_extract_cells(int grp, struct group_info * ginfo, void * param)
{
    int i;
    int nc = ginfo->gcs[grp].size;
    struct gti_extract_aux * aux = (struct gti_extract_aux *)param;
    struct cell * cells = aux->cells;
    sfc_t * buf = aux->buf;

    for ( i = 0; i < nc; ++i )
        buf[aux->count + i] = sfc_index(cells[ ginfo->gcs[grp].cells[i] ].coords);

    aux->count += nc;
}

void group_tree_iterate(int grp, struct group_info * ginfo, gti_cb cb, void * param)
{
    cart_assert( ginfo != NULL );
    cart_assert( grp >= 0 && grp < ginfo->ngrp );

    cb(grp, ginfo, param);

    int g = ginfo->gls[grp].child;

    while( g != -1 )
    {
        group_tree_iterate(g, ginfo, cb, param);
        g = ginfo->gls[g].nsib;
    }

    return;
}

/* ------------------------------------------------------------------------ */
      
struct cell * distribute_cells( int num_local_root_cells,
        sfc_t *local_sfc_list, float *local_work,
        int *boundary_level )
{
    int i, j;
    int coords[nDim];

    int slab_size = num_grid / num_lb_procs;

    /* partition geometry */
    for ( i = 0; i < nDim; ++i )
    {
        lsz[i] = num_grid;
        ori[i] = 0;
    }

    lsz[nDim-1] = slab_size;
    ori[nDim-1] = local_proc_id * slab_size;

#if nDim == 3
    local_cells = lsz[0] * lsz[1] * lsz[2];
#elif nDim == 2
    local_cells = lsz[0] * lsz[1];
#endif

    if ( local_proc_id >= num_lb_procs )
    {
        for ( i = 0; i < nDim; ++i )
        {
            lsz[i] = 0;
            ori[i] = 0;
        }

        local_cells = 0;
    }

    /* buffers */
    struct cell * send_buf = cart_alloc(struct cell, num_local_root_cells);
    struct cell * recv_buf = 0;

    if ( local_proc_id < num_lb_procs )
        recv_buf = cart_alloc(struct cell, slab_size * num_grid * num_grid);

    /* sizes and offsets */
    int * gsizes = cart_alloc(int, num_procs + 1);  // send buffer sizes for each recv rank
    int * gdspls = cart_alloc(int, num_procs + 1);  // send buffer offsets
    int * grsize = cart_alloc(int, num_procs + 1);  // recv buffer sizes for each send rank
    int * grdspl = cart_alloc(int, num_procs + 1);  // recv buffer offsets

    for ( i = 0; i < num_procs + 1; ++i )
    {
        gsizes[i] = 0;
        gdspls[i] = 0;
        grsize[i] = 0;
        grdspl[i] = 0;
    }

    for ( i = 0; i < num_local_root_cells; ++i )
    {
        sfc_coords( local_sfc_list[i], coords );

        int lb_rank = coords[nDim-1] / slab_size;
        gsizes[lb_rank]++;
    }

    for ( i = 0; i < num_lb_procs; ++i )
    {
        gdspls[i+1] = gdspls[i] + gsizes[i];
        gsizes[i] = 0;
    }

    double per_rank_work = 0.0;
    double total_work = 0.0;

    /* init send buffer */
    for ( i = 0; i < num_local_root_cells; ++i )
    {
        /* coords */
        sfc_coords( local_sfc_list[i], coords );
        int lb_rank = coords[nDim-1] / slab_size;

        /* index in the send buffer */
        int idx = gdspls[lb_rank] + gsizes[lb_rank];
        ++gsizes[lb_rank];

        /* assign coords */
        for ( j = 0; j < nDim; ++j )
            send_buf[idx].coords[j] = coords[j];

        /* level */
        char lv = 0;

        /* boundary lv */
        for ( j = 0; j < nDim * 2; ++j )
        {
            cart_assert(boundary_level[i*nDim*2+j] <= max_level);

            send_buf[idx].bl[j] = boundary_level[i*nDim*2 + j];
            lv = send_buf[idx].bl[j] > lv ? send_buf[idx].bl[j] : lv;
        }

        send_buf[idx].lv = lv;
        send_buf[idx].work = local_work[i];

        per_rank_work += local_work[i];
    }

    /* allreduce to get the total work */
    MPI_Allreduce(&per_rank_work, &total_work, 1, MPI_DOUBLE, MPI_SUM, mpi.comm.run);

    /* exchange gsizes */
    MPI_Alltoall( gsizes, 1, MPI_INT, grsize, 1, MPI_INT, mpi.comm.run );

    for ( i = 0; i < num_procs; ++i )
        grdspl[i+1] = grdspl[i] + grsize[i];

    /* buffer size check, local_cells = 0 when rank >= num_lb_procs */
    cart_assert( grdspl[num_procs] == local_cells );

    /* mpi_cell type */
    int blocklengths[4] = { nDim, 1, nDim * 2, 1 };
    MPI_Datatype types[4] = { MPI_INT, MPI_CHAR, MPI_CHAR, MPI_DOUBLE };
    MPI_Datatype mpi_cell;
    MPI_Aint offset[4] = { offsetof(struct cell, coords), 
        offsetof(struct cell, lv),
        offsetof(struct cell, bl),
        offsetof(struct cell, work) };

    MPI_Type_create_struct(4, blocklengths, offset, types, &mpi_cell);
    MPI_Type_commit(&mpi_cell);
      
    /* mpi_alltoallv */
    MPI_Alltoallv( send_buf, gsizes, gdspls, mpi_cell,
            recv_buf, grsize, grdspl, mpi_cell, mpi.comm.run );

    cart_free(send_buf);
    cart_free(gsizes);
    cart_free(gdspls);
    cart_free(grsize);
    cart_free(grdspl);

    MPI_Type_free(&mpi_cell);

    /* ordering the cells in the recv buffer */
    struct cell * rbuf = cart_alloc(struct cell, local_cells);
    for ( i = 0; i < local_cells; ++i )
    {
        int idx = cell_idx(recv_buf[i].coords);
        memcpy( &rbuf[idx], &recv_buf[i], sizeof(struct cell) );
        rbuf[idx].work /= total_work;
    }

    cart_free(recv_buf);

    return rbuf;
}



int ** init_cell_group_ids(struct cell * cells)
{
    int ** cellgids;
    int i, l;

    cellgids = cart_alloc(int *, local_cells);

    for (i = 0; i < local_cells; ++i)
    {
        int lv = cells[i].lv;

        cellgids[i] = cart_alloc(int, lv + 1);

        for (l = 0; l <= lv; ++l)
        {
            cellgids[i][l] = (l == 0) ? 0 : -1;  // all cells in lv 0 are assigned to group 0
        }
    }

    return cellgids;
}

inline void check_connect(int self, int coord[nDim], int f1, int f2, int lv, int gid, int * nr2, int * r2, struct cell * cells, int ** cellgids)
{
    int neib = cell_idx(coord);

    if (neib == -1) return;

    if (  cellgids[neib][lv] == -1
       && cells[self].bl[f1] >= lv       // self <-> neib connected
       && cells[neib].bl[f2] >= lv )
    {
        cellgids[neib][lv] = gid;
        r2[(*nr2)*3+0] = coord[0];
        r2[(*nr2)*3+1] = coord[1];
        r2[(*nr2)*3+2] = coord[2];
        ++(*nr2);
    }
}

void probe_group_iter(int seed, int lv, int gid, struct group_info * ginfo, int * red, int * r2, struct cell * cells, int ** cellgids)
{
    int  nred = 0;
    int  nr2  = 0;

    int coord[nDim];
    int r;

    idx_cell(seed, coord);
    red[0] = coord[0]; red[1] = coord[1]; red[2] = coord[2];
    nred = 1;

    while (nred)
    {
        nr2 = 0;

        for (r = 0; r < nred; ++r)
        {
            //int self = red[r];
            coord[0] = red[r*3+0];
            coord[1] = red[r*3+1];
            coord[2] = red[r*3+2];

            int self = cell_idx(coord);
            int neib = 0;

            // add the cell to the group
            group_add_cell(gid, ginfo, coord[0], coord[1], coord[2], cells);

            // check neighbors
            // x +
            coord[0]++;
            check_connect(self, coord, 1, 0, lv, gid, &nr2, r2, cells, cellgids);
            coord[0]--;

            // x -
            coord[0]--;
            check_connect(self, coord, 0, 1, lv, gid, &nr2, r2, cells, cellgids);
            coord[0]++;

            // y +
            coord[1]++;
            check_connect(self, coord, 3, 2, lv, gid, &nr2, r2, cells, cellgids);
            coord[1]--;

            // y -
            coord[1]--;
            check_connect(self, coord, 2, 3, lv, gid, &nr2, r2, cells, cellgids);
            coord[1]++;

            // z +
            coord[2]++;
            check_connect(self, coord, 5, 4, lv, gid, &nr2, r2, cells, cellgids);
            coord[2]--;

            // z -
            coord[2]--;
            check_connect(self, coord, 4, 5, lv, gid, &nr2, r2, cells, cellgids);
            coord[2]++;

        } // end of nred for loop

        int * temp = red;
        red = r2;
        r2  = temp;

        nred = nr2;

    } // end of while loop
}

void compute_groups_fast(struct cell * cells, int ** cellgids, struct group_info * ginfo)
{
    int l, i;
    int ngroups;
    int coord[nDim];

    int * red = cart_alloc(int, local_cells * 3);
    int * r2  = cart_alloc(int, local_cells * 3);

    // init group info
    ginfo->ngrp = 0;          // initial number of groups
    ginfo->ngrp_slots = 1000; // initial buffer size
    ginfo->gms = cart_alloc( struct group_meta,  ginfo->ngrp_slots );
    ginfo->gcs = cart_alloc( struct group_cells, ginfo->ngrp_slots );
    ginfo->gls = cart_alloc( struct group_links, ginfo->ngrp_slots );

    // only 1 group in level 0
    int root = group_create(ginfo, 0);

    for (i = 0; i < local_cells; ++i)
    {
        idx_cell(i, coord);

        ginfo->gms[root].work += cells[i].work;
        ginfo->gms[root].size ++;
        ginfo->gms[root].sumx += coord[0];
        ginfo->gms[root].sumy += coord[1];
        ginfo->gms[root].sumz += coord[2];

        if (cells[i].lv == 0) 
        {
            ginfo->gms[root].work_leaf += cells[i].work;
            ginfo->gms[root].size_leaf ++;

            group_add_leaf_cell(0, ginfo, i);
        }
    }

    cart_debug("lv = %d, ngroups = %d", 0, 1);

    for (l = 1; l <= max_level; ++l)
    {
        // number of groups at level l
        ngroups = 0;

        for (i = 0; i < local_cells; ++i)
        {
            // skip if a group has already assigned to the cell
            if (cells[i].lv < l || cellgids[i][l] != -1)
                continue;

            // create a new group
            int gid = group_create(ginfo, l);
            ++ngroups;

            // assign cell to the gid
            cellgids[i][l] = gid;

            // parent group and group id
            int pgid = cellgids[i][l-1];

            // probe the group using cell i as the seed
            probe_group_iter(i, l, gid, ginfo, red, r2, cells, cellgids);

            // insert into group tree (sorted by group sizes)
            group_insert_ordered(pgid, gid, ginfo);

            //cart_debug("lv = %d, gid = %d, size = %d\n", l, gid, ginfo->gms[gid].size);
        }

        cart_debug("%d: lv = %d, ngroups = %d", local_proc_id, l, ngroups);
    }

    int count = 0;
    group_tree_iterate(0, ginfo, gti_cb_leaf_count, &count);
    cart_debug("leaf count = %d", count);

    cart_free(red);
    cart_free(r2);
}

int * compute_leaf_cells(struct cell * cells)
{
    int * nleaf = cart_alloc(int, max_level + 1);

#if 0
    if (local_proc_id == 0)
    {
        int i;

        for (i = 0; i <= max_level; ++i)
            nleaf[i] = 0;

        for (i = 0; i < gx * gy * gz; ++i)
            ++ nleaf[ cells[i].lv ];
    }

    MPI_Bcast( nleaf, max_level + 1, MPI_INT, 0, MPI_COMM_WORLD );
#endif

    return nleaf;
}

int group_meet_condition(int gt, struct group_info * ginfo, int * nleaf, double coeff)
{
    // level
    int lv = ginfo->gms[gt].lv;

    // work and size
    double work = ginfo->gms[gt].work;
    double size = ginfo->gms[gt].size;

    // work limitations. total work is normalized to 1
    //double wmax = coeff * (1+tau) / num_procs;  
    double wmax = coeff * 0.01 / num_procs;  
    //double wlmax = coeff * fac * nleaf[lv] / num_procs;

    int accept = ( work <= wmax /*&& size <= wlmax*/);

    //cart_debug("[%d, wmax = %.3f, wlmax = %.3f]: gid = %d, work = %.3f, size = %.0f, accept = %d\n",
    //       lv, wmax, wlmax, gt, work, size, accept);

    return accept;
}

int cut_group(int gs, struct group_info * ginfo, int * nleaf, struct cell * cells)
{
    int lv = ginfo->gms[gs].lv;
    int gt = group_create(ginfo, lv);

    while (group_meet_condition(gt, ginfo, nleaf, 0.95))
    {
        group_transfer_leaf( gs, gt, ginfo, cells );
    }

    return gt;
}

struct list * split_groups(int gtree, struct group_info * ginfo, int * nleaf, struct cell * cells)
{
    struct list * groups_to_split = list_create();
    struct list * groups_to_cut   = list_create();
    struct list * groups_done     = list_create();

    // push the entire gtree to the "to be split" groups
    list_push_front(groups_to_split, gtree);

    // first rip sub-groups off the gtree
    while (groups_to_split->size)
    {
        int gt = list_front(groups_to_split);

        // gt meet condition?
        if (group_meet_condition(gt, ginfo, nleaf, 1.0))
        {
            // move gt to the g_done list
            list_push_end(groups_done, gt);
            list_pop_front(groups_to_split);
        }
        else
        {
            if (group_has_child(gt, ginfo))
            {
                // rip off the largest sub-group
                int gc = group_rip_first_child(gt, ginfo);
                list_push_end(groups_to_split, gc);
            }
            else
            {
                // no child -- push gt to g_cut and proceed to next in g_split
                list_push_end(groups_to_cut, gt);
                list_pop_front(groups_to_split);
            }
        }

    }

    cart_debug("done splitting. done = %d, split = %d, cut = %d\t", 
           groups_done->size, groups_to_split->size, groups_to_cut->size);

    // then cut all remaining groups
    while (groups_to_cut->size)
    {
        int gs = list_front(groups_to_cut);
        int gt = cut_group(gs, ginfo, nleaf, cells);

        // push gt to done
        list_push_end(groups_done, gt);

        // check if gs has met the condition
        if (group_meet_condition(gs, ginfo, nleaf, 1.0))
        {
            list_push_end(groups_done, gs);
            list_pop_front(groups_to_cut);
        }
    }

    cart_debug("done cutting. done = %d, split = %d, cut = %d\t", 
           groups_done->size, groups_to_split->size, groups_to_cut->size);

    // everything in the groups_done is good
    cart_assert(groups_to_split->size == 0);
    cart_assert(groups_to_cut->size == 0);

    cart_free(groups_to_split);
    cart_free(groups_to_cut);

    return groups_done;
}

// returns an array of per process group sizes, and accumulated sizes
void gather_groups(struct list * groups, struct group_info * ginfo, struct group_extra * gextra)
{
    int i, total_groups;

    // first half the group sizes from each process, second half is the accumulated size
    int * gsizes = cart_alloc(int, num_lb_procs * 2 + 1);

    // gather per process group sizes
    gsizes[local_proc_id] = groups->size;
    MPI_Allgather(&gsizes[local_proc_id], 1, MPI_INT, gsizes, 1, MPI_INT, comm_lb);

    // accumulated size
    gsizes[num_lb_procs] = 0;

    for ( i = 1; i <= num_lb_procs; ++i )
        gsizes[num_lb_procs + i] = gsizes[num_lb_procs + i - 1] + gsizes[i - 1];

    // total number of groups
    total_groups = gsizes[num_lb_procs * 2];

    // create the group_extra buffer
    if (local_proc_id == MASTER_NODE)
    {
        gextra->gms = cart_alloc(struct group_meta_extra, total_groups);
        gextra->gbs = cart_alloc(struct proc_bin *, total_groups);
        gextra->gns = cart_alloc(struct list *, total_groups);

        gextra->ngrp = total_groups;

        // init gns
        for ( i = 0; i < total_groups; ++i )
        {
            gextra->gns[i] = list_create();
            gextra->gbs[i] = 0;
        }
    }
    else
    {
        gextra->gms = cart_alloc(struct group_meta_extra, groups->size);
        gextra->gbs = 0;
        gextra->gns = 0;

        gextra->ngrp = groups->size;
    }

    // copy group meta data from ginfo to gextra
    struct list_node * node = groups->first;
    for ( i = 0; i < groups->size; ++i )
    {
        cart_assert( node != NULL );

        int gid = (int)(node->data);

        gextra->gms[i].proc = local_proc_id;
        gextra->gms[i].gid  = gid;

        gextra->gms[i].size = ginfo->gms[gid].size;
        gextra->gms[i].work = ginfo->gms[gid].work;

        gextra->gms[i].x = ginfo->gms[gid].sumx / ginfo->gms[gid].size;
        gextra->gms[i].y = ginfo->gms[gid].sumy / ginfo->gms[gid].size;
        gextra->gms[i].z = ginfo->gms[gid].sumz / ginfo->gms[gid].size;

        node = node->next;
    }

    // MPI types
    int          blocklengths[7] = { 1, 1, 1, 1, 1, 1, 1 };
    MPI_Datatype types[7] = { MPI_INT, MPI_INT, MPI_INT, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE };
    MPI_Datatype mpi_group_extra;
    MPI_Aint     offset[7];

    offset[0] = offsetof(struct group_meta_extra, proc);
    offset[1] = offsetof(struct group_meta_extra, gid );
    offset[2] = offsetof(struct group_meta_extra, size);
    offset[3] = offsetof(struct group_meta_extra, work);
    offset[4] = offsetof(struct group_meta_extra, x);
    offset[5] = offsetof(struct group_meta_extra, y);
    offset[6] = offsetof(struct group_meta_extra, z);

    MPI_Type_create_struct(7, blocklengths, offset, types, &mpi_group_extra);
    MPI_Type_commit(&mpi_group_extra);

    // gather group_meta array
    MPI_Gatherv(gextra->gms, groups->size, mpi_group_extra, 
        gextra->gms, &gsizes[0], &gsizes[num_lb_procs], mpi_group_extra, 0, comm_lb);

    // cart_free resources
    MPI_Type_free(&mpi_group_extra);

    if (local_proc_id != MASTER_NODE)
    {
        cart_free(gextra->gms);
        gextra->gms = 0;
        gextra->ngrp = 0;
    }

    cart_free(gsizes);
}


// try add gj to gi's neighbor list
void add_group_neighbor(int gi, int gj, struct group_extra * gextra)
{
    double xi = gextra->gms[gi].x;
    double yi = gextra->gms[gi].y;
    double zi = gextra->gms[gi].z;

    double xj = gextra->gms[gj].x;
    double yj = gextra->gms[gj].y;
    double zj = gextra->gms[gj].z;

    double dist2 = (xi - xj) * (xi - xj) + (yi - yj) * (yi - yj)
                 + (zi - zj) * (zi - zj);

    // get the neighbor list
    struct list * neighbors = gextra->gns[gi];

    // do nothing if the neighbor list is full, and last neighbor
    // is closer than gj
    if (neighbors->size >= maxneighbors &&
        ((struct g_neighbor *)(neighbors->last->data))->dist <= dist2)
    {
        return;
    }

    // create a new neighbor
    struct g_neighbor * neib = cart_alloc(struct g_neighbor, 1);

    neib->grp = gj;
    neib->dist = dist2;

    // find the insert position
    struct list_node * node = neighbors->first;
    while (node != 0 && ((struct g_neighbor *)(node->data))->dist < dist2)
        node = node->next;

    // append at tail
    if (node == 0)
    {
        list_push_end(neighbors, (uintptr_t)neib);
    }
    else
    {
        list_insert_before(neighbors, node, (uintptr_t)neib);
    }

    // pop and delete the last neighbor
    if (neighbors->size > maxneighbors)
    {
        struct g_neighbor * tmp = (struct g_neighbor *)list_pop_end(neighbors);
        cart_free(tmp);
    }
}

void find_group_neighbors(struct group_extra * gextra)
{
    int gi, gj;

    // at least two groups
    if (gextra->ngrp < 2) return;

    for ( gi = 0; gi < gextra->ngrp; ++gi )
    {
        //cart_debug("%d/%d\n", gi, gextra->ngrp);

        for ( gj = 0; gj < gextra->ngrp; ++gj )
        {
            if (gi != gj) add_group_neighbor(gi, gj, gextra);
        }
    }
}

// bin->work has been increased, so re-sort the list to its proper position
void sort_bins_asc(struct list * bins, struct proc_bin * bin)
{
    struct list_node * node = bins->first;

    // find the list node
    while (node != 0 && node->data != (uintptr_t)bin)
        node = node->next;

    // didnt find the node that contains bin
    if (node == 0)
    {
        cart_debug("error: didnt find bin in sort_bin_asc()");
        exit(1);
    }

    // already in order. do nothing
    if (node->next == 0)
        return;

    struct list_node * pos = node->next;

    // now find the insertion point
    while (pos !=0 && ((struct proc_bin *)(pos->data))->work < bin->work)
        pos = pos->next;

    // move node to before the pos
    list_move_node(bins, node, pos);

    // validate the list
    validate_list(bins);
    validate_list_asc(bins);
}

void assign_groups_to_bins(struct list * bins, struct group_extra * gextra)
{
    int grp;

    //double wmax = 1.0;
    double wmax = (1.0 / num_procs) * 1.2;

    for ( grp = 0; grp < gextra->ngrp; ++grp )
    {
        struct list * neighbors = gextra->gns[grp];

        // iterate through neighbors
        struct list_node * nnode = neighbors->first;
        struct proc_bin * bin = 0;

        while (nnode != 0)
        {
            struct g_neighbor * gneib = (struct g_neighbor *)(nnode->data);
            bin = gextra->gbs[gneib->grp];
         
            if (bin != 0 &&  // the neighbor group has been assigned to a bin
                bin->work + gextra->gms[grp].work <= wmax)  // bin has space available
            {
                // found the bin and jump out
                break;
            }

            nnode = nnode->next;
        }

        if (nnode == 0)
        {
            // meaning we didnt find a suitable bin in grp's neighbors
            // so just assign grp to the emptiest bin
            bin = (struct proc_bin *)(bins->first->data);
        }

        // assign grp to bin
        list_push_end(bin->groups, grp);
        bin->work += gextra->gms[grp].work;

        // associate bin to the group
        gextra->gbs[grp] = bin;

        // re-sort the bins according to the emptiness
        sort_bins_asc(bins, bin);
    }
}


struct list * binning_groups(struct group_extra * gextra)
{
    int i;

    // construct neighboring matrix
    find_group_neighbors(gextra);
    cart_debug("done calculating neighbors");

    // allocate a list of proc_bins
    struct list * bins = list_create();

    for (i = 0; i < num_procs; ++i)
    {
        struct proc_bin * bin = cart_alloc(struct proc_bin, 1);

        bin->groups = list_create();
        bin->bid  = i;
        bin->work = 0;

        list_push_end(bins, (uintptr_t)bin);
    }

    // allocate grups to bins
    assign_groups_to_bins(bins, gextra);
    cart_debug("done assigning groups to bins");

    return bins;
}

void verify_group_list_sizes(struct list * groups, struct group_info * ginfo)
{
    int total = 0;
    int leaf = 0;
    struct list_node * node = groups->first;

    while(node)
    {
        total += ginfo->gms[node->data].size;
        group_tree_iterate(node->data, ginfo, gti_cb_leaf_count, &leaf);

        node = node->next;
    }

    cart_debug("group sizes: total = %d, leaf = %d", total, leaf);
}

sfc_t * distribute_bin_groups(struct group_info * ginfo, struct group_extra * gextra, struct cell * cells, int * num_sfc)
{
    int grp;
    int i;

    int * gssizes = cart_alloc(int, num_lb_procs + 1);
    int * gsdspls = cart_alloc(int, num_lb_procs + 1);

    struct group_dst * gdbuf = 0;

    int gds = 0;

    for ( i = 0; i <= num_lb_procs;  ++i )
    {
        gssizes[i] = 0;
        gsdspls[i] = 0;
    }

    if (local_proc_id == MASTER_NODE)
    {
        for ( grp = 0; grp < gextra->ngrp; ++grp )
        {
            int src = gextra->gms[grp].proc;
            gssizes[src]++;
        }
    }

    if ( local_proc_id < num_lb_procs )
    {
        // broadcast the group source sizes
        MPI_Bcast(gssizes, num_lb_procs, MPI_INT, MASTER_NODE, comm_lb);

        // allocate buffer
        if ( local_proc_id == MASTER_NODE )
        {
            gdbuf = cart_alloc(struct group_dst, gextra->ngrp);
        }
        else
        {
            gdbuf = cart_alloc(struct group_dst, gssizes[local_proc_id]);
        }

        // number of group-dst pairs
        gds = gssizes[local_proc_id];
    }

    if (local_proc_id == MASTER_NODE)
    {
        for ( i = 0; i < num_lb_procs; ++i )
        {
            gsdspls[i+1] = gsdspls[i] + gssizes[i];
            gssizes[i] = 0;
        }

        for ( grp = 0; grp < gextra->ngrp; ++grp )
        {
            int src = gextra->gms[grp].proc;
            int gid = gextra->gms[grp].gid;
            int dst = gextra->gbs[grp]->bid;

            gdbuf[gsdspls[src] + gssizes[src]].gid = gid;
            gdbuf[gsdspls[src] + gssizes[src]].dst_proc = dst;

            gssizes[src]++;
        }
    }

    if ( local_proc_id < num_lb_procs )
    {
        int blocklengths[2] = { 1, 1 };
        MPI_Datatype types[2] = { MPI_INT, MPI_INT };
        MPI_Aint offset[2] = { offsetof(struct group_dst, gid), offsetof(struct group_dst, dst_proc) };
        MPI_Datatype mpi_group_dst;

        MPI_Type_create_struct(2, blocklengths, offset, types, &mpi_group_dst);
        MPI_Type_commit(&mpi_group_dst);
 
        // scatter the group - dest_proc buffer
        MPI_Scatterv( gdbuf, gssizes, gsdspls, mpi_group_dst,
                gdbuf, gssizes[local_proc_id], mpi_group_dst, 
                MASTER_NODE, comm_lb );

        MPI_Type_free(&mpi_group_dst);
    }

    cart_free(gssizes);
    cart_free(gsdspls);


    // -----------------------------
    // distribute cells
    // -----------------------------

    int * cdssizes = cart_alloc(int, num_procs + 1);  // cell-dst sizes (number of cells per dst proc)
    int * cdsdspls = cart_alloc(int, num_procs + 1);  // cell-dst displacements

    int * cdrsizes = cart_alloc(int, num_procs + 1);  // cell-dst sizes (number of cells per dst proc)
    int * cdrdspls = cart_alloc(int, num_procs + 1);  // cell-dst displacements

    for ( i = 0; i <= num_procs; ++i )
    {
        cdssizes[i] = 0;
        cdsdspls[i] = 0;

        cdrsizes[i] = 0;
        cdrdspls[i] = 0;
    }

    for ( i = 0; i < gds; ++i )
    {
        int gid = gdbuf[i].gid;
        int dst = gdbuf[i].dst_proc;

        cdssizes[dst] += ginfo->gms[gid].size;
    }

    MPI_Alltoall( cdssizes, 1, MPI_INT, cdrsizes, 1, MPI_INT, mpi.comm.run );

    for ( i = 0; i < num_procs; ++i )
    {
        cdsdspls[i+1] = cdsdspls[i] + cdssizes[i];
        cdssizes[i] = 0;

        cdrdspls[i+1] = cdrdspls[i] + cdrsizes[i];
    }

    *num_sfc = cdrdspls[num_procs];

    sfc_t * send_buf = cart_alloc(sfc_t, cdsdspls[num_procs]);
    sfc_t * recv_buf = cart_alloc(sfc_t, cdrdspls[num_procs]);

    for ( i = 0; i < gds; ++i )
    {
        int gid = gdbuf[i].gid;
        int dst = gdbuf[i].dst_proc;

        struct gti_extract_aux aux;
        aux.count = 0;
        aux.cells = cells;
        aux.buf   = &send_buf[cdsdspls[dst] + cdssizes[dst]];

        group_tree_iterate(gid, ginfo, gti_cb_extract_cells, &aux);

        cdssizes[dst] += aux.count;
    }

    MPI_Alltoallv( send_buf, cdssizes, cdsdspls, MPI_SFC,
            recv_buf, cdrsizes, cdrdspls, MPI_SFC, mpi.comm.run );

    cart_free(cdssizes);
    cart_free(cdsdspls);
    cart_free(cdrsizes);
    cart_free(cdrdspls);

    cart_free(gdbuf);
    cart_free(send_buf);

    // sort the recv buf
    qsort(recv_buf, *num_sfc, sizeof(sfc_t), sfc_compare);

    return recv_buf;
}

void validate_decomposition(int num_sfc, sfc_t * sfcs)
{
    int i;
    int nums[num_procs], dspls[num_procs];

    // gather sizes
    MPI_Gather(&num_sfc, 1, MPI_INT, nums, 1, MPI_INT, MASTER_NODE, mpi.comm.run);

    dspls[0] = 0;

    for ( i = 0; i < num_procs - 1; ++i ) 
        dspls[i+1] = dspls[i] + nums[i];

    sfc_t * total = cart_alloc(sfc_t, num_grid * num_grid * num_grid);

    // gather cells
    MPI_Gatherv(sfcs, num_sfc, MPI_SFC, total, nums, dspls, MPI_SFC, MASTER_NODE, mpi.comm.run);

    // validate
    if ( local_proc_id == MASTER_NODE )
    {
        int i;
        sfc_t s;
        int not_found = 0;

        for ( s = 0; s < num_grid * num_grid * num_grid; ++s )
        {
            int found = 0;

            for ( i = 0; i < num_grid * num_grid * num_grid; ++i )
            {
                if ( total[i] == s )
                {
                    found = 1;
                    break;
                }
            }

            if ( found == 0 )
                ++not_found;
        }

        cart_assert( not_found == 0 );
    }

    cart_free(total);
}

void lb_decompose( int num_local_root_cells,
		sfc_t *local_sfc_list, float *local_work,
		int num_constrained_quantities,
		int *local_constrained_quantities,
		int *per_proc_constraints,
		int *boundary_level,
		lb_decomposition *decomposition ) {

#ifdef SFC_DOMAIN_DECOMPOSITION
#error not implemented
#else

    int i;

    /* create load balance communicator */
    if ( local_proc_id < num_lb_procs )
        MPI_Comm_split(mpi.comm.run, 0, local_proc_id, &comm_lb);
    else
        MPI_Comm_split(mpi.comm.run, MPI_UNDEFINED, local_proc_id, &comm_lb);

    // distribute cells, for all ranks in comm.run
    struct cell * cells = distribute_cells( num_local_root_cells, local_sfc_list, local_work, boundary_level );

    struct group_info ginfo = { 0, 0, 0, 0, 0 };
    struct group_extra gextra = { 0, 0, 0, 0 };
    struct list * bins = 0;

    // rest of the work only for ranks in comm.lb
    if ( local_proc_id < num_lb_procs )
    {
        // create and init per cell per level group id
        int ** cellgids = init_cell_group_ids(cells);
        cart_debug("done assigning initial group ids for each cell.");

        // compute groups
        compute_groups_fast(cells, cellgids, &ginfo);
    
        // free the cellgids[]
        for( i = 0; i < local_cells; ++i ) 
            cart_free(cellgids[i]);

        cart_free(cellgids);

        cart_debug("done computing groups for each level.");

        // split groups from group 0
        int * nleaf = 0;
        struct list * groups = split_groups(0, &ginfo, nleaf, cells);

        // verify group sizes
        verify_group_list_sizes(groups, &ginfo);

        // validate
        //validate_list(groups);

        // gather the group lists from all ranks to root
        gather_groups(groups, &ginfo, &gextra);
        cart_debug("done gathering groups.");

        // free the groups list
        free_list(groups);

        // binning groups
        if (local_proc_id == MASTER_NODE)
        {
            bins = binning_groups(&gextra);

            /* print info */
            struct list_node * node = bins->first;
            double work = 0;
            for ( i=0; i<bins->size; ++i )
            {
                struct proc_bin * bin = (struct proc_bin *)(node->data);
                cart_debug("bin %d: size = %d, work = %f", bin->bid, bin->groups->size, bin->work);
                work += bin->work;
                node = node->next;
            }

            cart_debug("total work = %f", work);
        }

        // cart_free memory
        cart_free(nleaf);
    }

    int num_sfc;
    sfc_t * sfcs = distribute_bin_groups(&ginfo, &gextra, cells, &num_sfc);

    /* validate the final decomposition */
    //validate_decomposition(num_sfc, sfcs);

    /* submit the decomposition */
    cart_debug("num_sfc = %d", num_sfc);
    lb_set_sfc_list(decomposition, num_sfc, sfcs);

    /* free memory */
    if (local_proc_id == MASTER_NODE)
        free_list(bins);

    free_group_info(&ginfo);
    free_group_extra(&gextra);

    cart_free(cells);
    cart_free(sfcs);

#endif /* SFC_DOMAIN_DECOMPOSITION */
}
