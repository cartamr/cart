#include "config.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "auxiliary.h"
#include "lb.h"
#include "parallel.h"
#include "sfc.h"

int divide_list_linear( float *global_work,
		int num_constrained_quantities,
		int *constrained_quantities,
		sfc_t *per_proc_constraints,
		int num_root_cells_in_division, double total_work,
		int num_procs_in_division, int first_proc,
		sfc_t first_cell_index, sfc_t *proc_index );

int divide_list_recursive( float *global_work,
		int num_constrained_quantities,
		int *constrained_quantities,
		int *per_proc_constraints,
		sfc_t num_root_cells_in_division, double total_work,
		int num_procs_in_division, int first_proc,
		sfc_t first_cell_index, sfc_t *proc_index ) {

	int c;
	sfc_t i, j, k;
	float work_frac_left;
	double current_work, current_work_left;
	long long sum_constraints[num_constrained_quantities];
	int num_procs_left, num_procs_right;
	int left_failure, right_failure;

	cart_assert( num_root_cells_in_division >= num_procs_in_division );

	if ( num_procs_in_division == 1 ) {
		proc_index[first_proc] = first_cell_index;
		proc_index[first_proc+1] = first_cell_index+num_root_cells_in_division;
		return 0;
	}

	for ( c = 0; c < num_constrained_quantities; c++ ) {
		sum_constraints[c] = 0;
	}

	for ( i = 0; i < num_root_cells_in_division; i++ ) {
		for ( c = 0; c < num_constrained_quantities; c++ ) {
			cart_assert( constrained_quantities[num_constrained_quantities*i+c] >= 0 );
			sum_constraints[c] += constrained_quantities[num_constrained_quantities*i+c];
		}
	}

	/* unable to find decomposition due to constraints */
	for ( c = 0; c < num_constrained_quantities; c++ ) {
		if ( sum_constraints[c] > (long long)num_procs_in_division*(long long)per_proc_constraints[c] ) {
			cart_debug("Current segment contains more octs/particles than allowed by number of processes (constraint %d): %ld vs %ld",
					c, sum_constraints[c], (long long)num_procs_in_division*(long long)per_proc_constraints[c] );
			cart_debug("number of root trees in segment: %ld", num_root_cells_in_division);
			cart_debug("number of ranks in segment: %ld", num_procs_in_division);
			return -1;
		}
	}

	num_procs_left = num_procs_in_division/2;
	num_procs_right = num_procs_in_division - num_procs_left;

	cart_assert( num_procs_left >= 1 );
	cart_assert( num_procs_right >= 1 );

	work_frac_left = (float)(num_procs_left)/num_procs_in_division;

	/* find minimal splitting points based on constraints */
	for ( c = 0; c < num_constrained_quantities; c++ ) {
		sum_constraints[c] = 0;
	}

	/* use greedy algorithm to find max/min splitting points */
	j = 0;
	k = 0;
	while ( j < num_root_cells_in_division-num_procs_right && k < num_procs_left ) {
		for ( c = 0; c < num_constrained_quantities; c++ ) {
			sum_constraints[c] += constrained_quantities[num_constrained_quantities*j+c];

			if ( sum_constraints[c] >= per_proc_constraints[c] ) {
				break;
			}
		}

		if ( c < num_constrained_quantities ) {
			for ( c = 0; c < num_constrained_quantities; c++ ) {
				sum_constraints[c] = 0;
			}
			k++;
		} else {
			j++;
		}
	}

	for ( c = 0; c < num_constrained_quantities; c++ ) {
		sum_constraints[c] = 0;
	}

	i = num_root_cells_in_division - 1;
	k = 0;
	while ( i >= num_procs_left && k < num_procs_right ) {
		for ( c = 0; c < num_constrained_quantities; c++ ) {
			sum_constraints[c] += constrained_quantities[num_constrained_quantities*i+c];

			if ( sum_constraints[c] >= per_proc_constraints[c] ) {
				break;
			}
		}

		if ( c < num_constrained_quantities ) {
			for ( c = 0; c < num_constrained_quantities; c++ ) {
				sum_constraints[c] = 0;
			}
			k++;
		} else {
			i--;
		}
	}

	current_work_left = 0.0;

	for ( k = 0; k <= i; k++ ) {
		current_work_left += global_work[k];
	}

	proc_index[first_proc] = first_cell_index;

	/* k \in (i,j] (k is first cell to the right of division) */
	if ( i < j ) {
		/* pick the best splitting point using work as an estimate */
		current_work = 0.0;

		for ( k = i+1; k < j; k++ ) {
			if ( current_work + current_work_left + 0.5*global_work[k] >
					work_frac_left*total_work ) {
				break;
			} else {
				current_work += global_work[k];
			}
		}

		if ( num_procs_left > 1 ) {
			left_failure = divide_list_recursive( global_work,
					num_constrained_quantities,
					constrained_quantities,
					per_proc_constraints,
					k, current_work+current_work_left,
					num_procs_left, first_proc,
					first_cell_index, proc_index );
		} else {
			left_failure = 0;
			proc_index[first_proc+num_procs_left] = k+first_cell_index;
		}

		if ( num_procs_right > 1 ) {
			right_failure = divide_list_recursive( &global_work[k],
					num_constrained_quantities,
					&constrained_quantities[num_constrained_quantities*k],
					per_proc_constraints,
					num_root_cells_in_division - k,
					total_work - current_work - current_work_left,
					num_procs_right, first_proc+num_procs_left,
					first_cell_index+k, proc_index );
		} else {
			right_failure = 0;
			proc_index[first_proc+num_procs_in_division] =
					first_cell_index+num_root_cells_in_division;
		}

		/* if we're unable to divide recursively, try linear-greedy division */
		if ( left_failure || right_failure ) {
			return divide_list_linear( global_work, num_constrained_quantities,
					constrained_quantities,
					per_proc_constraints, num_root_cells_in_division,
					total_work, num_procs_in_division, first_proc,
					first_cell_index, proc_index );
		} else {
			return 0;
		}
	} else {
		cart_error("i = %ld, j = %ld", i, j );
	}

	return -1;
}

int divide_list_linear( float *global_work,
		int num_constrained_quantities,
		int *constrained_quantities,
		sfc_t *per_proc_constraints,
		int num_root_cells_in_division, double total_work,
		int num_procs_in_division, int first_proc,
		sfc_t first_cell_index, sfc_t *proc_index ) {

	int i, c, c2;
	int proc;
	sfc_t index;
	double local_work = 0.0;
	double ideal_work_per_proc;
	int num_procs_remaining;
	long long total_constraints[num_constrained_quantities];
	long long sum_constraints[num_constrained_quantities];

	local_work = 0.0;
	ideal_work_per_proc = total_work / (double)num_procs_in_division;

	num_procs_remaining = num_procs_in_division-1;
	proc_index[first_proc] = first_cell_index;

	for ( c = 0; c < num_constrained_quantities; c++ ) {
		total_constraints[c] = 0;
	}

	for ( index = 0; index < num_root_cells_in_division; index++ ) {
		for ( c = 0; c < num_constrained_quantities; c++ ) {
			total_constraints[c] += constrained_quantities[num_constrained_quantities*index+c];
		}
	}

	index = 0;
	for ( proc = first_proc+1; proc < num_procs_in_division + first_proc; proc++ ) {
		/* assign a minimum number of cells so that the remaining processors
		 * can satisfy the constraints (cells, particles, etc) */

		/* start by assigning 1 root cell */
		for ( c = 0; c < num_constrained_quantities; c++ ) {
			sum_constraints[c] = constrained_quantities[num_constrained_quantities*index+c];
			total_constraints[c] -= constrained_quantities[num_constrained_quantities*index+c];
		}
		index++;

		for ( c = 0; c < num_constrained_quantities; c++ ) {
			while ( total_constraints[c] >= num_procs_remaining*per_proc_constraints[c] ) {
				for ( c2 = 0; c2 < num_constrained_quantities; c2++ ) {
					total_constraints[c2] -= constrained_quantities[num_constrained_quantities*index+c2];
					sum_constraints[c2] += constrained_quantities[num_constrained_quantities*index+c2];

					if ( sum_constraints[c2] > per_proc_constraints[c2] ) {
						/* problem allocating! */
						cart_debug("failed allocating space linearly!");
						return -1;
					}
				}
				index++;
			}
		}

		/* ensure maximum of reduced_root_cells, leaving at least one root cell
		 * for each processor left to come, and not assigning more cells than processor
		 * can hold (max_cells_allowed), as well as allowing to go above ideal_work_per_proc
		 * if rest of procs can't hold the cells remaining
		 */
		while ( index < num_root_cells_in_division - num_procs_remaining &&
				local_work + 0.5*global_work[index] < ideal_work_per_proc ) {

			for ( c = 0; c < num_constrained_quantities; c++ ) {
				sum_constraints[c] += constrained_quantities[num_constrained_quantities*index+c];

				if ( sum_constraints[c] >= per_proc_constraints[c] ) {
					break;
				}
			}

			if ( c < num_constrained_quantities ) {
				break;
			} else {
				for ( c = 0; c < num_constrained_quantities; c++ ) {
					total_constraints[c] -= constrained_quantities[num_constrained_quantities*index+c];
				}

				local_work += global_work[index];
				index++;
			}
		}

		proc_index[proc] = index+first_cell_index;

		/* continually re-evaluate ideal work given what work is left */
		num_procs_remaining--;
		total_work -= local_work;

		if ( num_procs_remaining > 0 ) {
			ideal_work_per_proc = total_work / (double)(num_procs_remaining);
		}
	}

	proc_index[first_proc+num_procs_in_division] = first_cell_index + num_root_cells_in_division;

	return 0;
}

void lb_decompose( int num_local_root_cells,
		sfc_t *local_sfc_list, float *local_work,
		int num_constrained_quantities,
		int *local_constrained_quantities,
		int *per_proc_constraints,
		int *boundary_level,
		lb_decomposition *decomposition ) {

	int i, j, c, r;
	int ret;
	int *per_rank_count = NULL;
	float *global_work, *rank_work;
	int *global_constraints, *rank_constraints;
	sfc_t *rank_sfc_list;
	double total_work;
	int rank, num_ranks;
	sfc_t *new_proc_sfc_index;

	new_proc_sfc_index = cart_alloc(sfc_t, num_procs+1);

	/* revert to comm.world properties for now */
	rank = local_proc_id;
	num_ranks = num_procs;

	if ( rank == 0 ) {
		per_rank_count = cart_alloc(int, num_ranks);
	}

	MPI_Gather(&num_local_root_cells, 1, MPI_INT,
		per_rank_count, 1, MPI_INT, 0, mpi.comm.run);

	/* communicate all data to 0-rank for serial processing */
	if ( rank == 0 ) {
		global_work = cart_alloc(float, num_root_cells);
		global_constraints = cart_alloc(int, num_constrained_quantities*num_root_cells);

		for ( i = 0; i < num_local_root_cells; i++ ) {
			cart_assert( local_sfc_list[i] >= 0 && local_sfc_list[i] < num_root_cells );
			global_work[local_sfc_list[i]] = local_work[i];
		}

		for ( i = 0; i < num_local_root_cells; i++ ) {
			for ( c = 0; c < num_constrained_quantities; c++ ) {
				global_constraints[num_constrained_quantities*local_sfc_list[i]+c] = local_constrained_quantities[num_constrained_quantities*i+c];
			}
		}

		for ( r = 1; r < num_ranks; r++ ) {
			if ( per_rank_count[r] > 0 ) {
				rank_sfc_list = cart_alloc(sfc_t, per_rank_count[r]);
				MPI_Recv(rank_sfc_list, per_rank_count[r], MPI_SFC,
						r, 0, mpi.comm.run, MPI_STATUS_IGNORE);

				rank_work = cart_alloc(float, per_rank_count[r]);
				MPI_Recv(rank_work, per_rank_count[r], MPI_FLOAT,
						r, 1, mpi.comm.run, MPI_STATUS_IGNORE);
				for ( i = 0; i < per_rank_count[r]; i++ ) {
					global_work[rank_sfc_list[i]] = rank_work[i];
				}
				cart_free(rank_work);

				rank_constraints = cart_alloc(int, num_constrained_quantities*per_rank_count[r]);
				MPI_Recv(rank_constraints, num_constrained_quantities*per_rank_count[r], MPI_INT,
						r, 2, mpi.comm.run, MPI_STATUS_IGNORE);
				for ( i = 0; i < per_rank_count[r]; i++ ) {
					for ( c = 0; c < num_constrained_quantities; c++ ) {
						global_constraints[num_constrained_quantities*rank_sfc_list[i]+c] = rank_constraints[num_constrained_quantities*i+c];
					}
				}
				cart_free(rank_constraints);
				cart_free(rank_sfc_list);
			}
		}

		cart_free(per_rank_count);
	} else {
		if ( num_local_root_cells > 0 ) {
			MPI_Send(local_sfc_list, num_local_root_cells, MPI_SFC,
					0, 0, mpi.comm.run);
			MPI_Send(local_work, num_local_root_cells, MPI_FLOAT,
					0, 1, mpi.comm.run);

			for ( i = 0; i < num_local_root_cells*num_constrained_quantities; i++ ) {
				cart_assert( local_constrained_quantities[i] >= 0 );
			}
			MPI_Send(local_constrained_quantities, num_local_root_cells*num_constrained_quantities, MPI_INT,
					0, 2, mpi.comm.run);
		}
	}

	if ( rank == 0 ) {
		/* compute total work */
		total_work = 0.0;
		for ( i = 0; i < num_root_cells; i++ ) {
			total_work += global_work[i];
		}

		ret = divide_list_recursive( global_work,
				num_constrained_quantities,
				global_constraints,
				per_proc_constraints,
				num_root_cells, total_work,
				num_procs, 0, 0, new_proc_sfc_index );

		cart_free( global_work );
		cart_free( global_constraints );

		if ( ret == -1 ) {
			cart_error("Unable to find proper load balancing division; try increasing num_octs.");
		}

#ifdef DEBUG
		for ( i = 0; i <= num_procs; i++ ) {
			cart_debug("new_proc_sfc_index[%d] = %d", i, new_proc_sfc_index[i]);
		}
#endif
	}

	MPI_Bcast(new_proc_sfc_index, num_procs+1, MPI_SFC, 0, mpi.comm.run);
	lb_set_sfc_range(decomposition, new_proc_sfc_index[local_proc_id], new_proc_sfc_index[local_proc_id+1]-1);
	cart_free( new_proc_sfc_index );
}
