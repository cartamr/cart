#include "config.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "auxiliary.h"
#include "lb.h"
#include "parallel.h"
#include "rand.h"
#include "sfc.h"

void lb_decompose( int num_local_root_cells,
		sfc_t *local_sfc_list, float *local_work,
		int num_constrained_quantities,
		int *local_constrained_quantities,
		int *per_proc_constraints,
		int *boundary_level,
		lb_decomposition *decomposition ) {

#ifdef SFC_DOMAIN_DECOMPOSITION
	int i;
	sfc_t *new_proc_sfc_index = cart_alloc(sfc_t, num_procs+1);
	if ( local_proc_id == MASTER_NODE ) {
		/* for SFC decomposition the ranges must be continuous so count proc_map, draw
		 * from gaussian with 10% std, then normalize to num_root_cells
		 */
		double *draw = cart_alloc(double, num_procs);
		double sum = 0.;

		for ( i = 0; i < num_procs; i++ ) {
			draw[i] = cart_rand_gaussian(1.0, 0.1);
			draw[i] = MAX(0.1, draw[i]);
			sum += draw[i];
		}

		new_proc_sfc_index[0] = 0;
		for ( i = 1; i < num_procs; i++ ) {
			new_proc_sfc_index[i] = MAX((sfc_t)(draw[i-1]/sum*(double)num_root_cells+0.5), 1) + new_proc_sfc_index[i-1];
		}
		new_proc_sfc_index[num_procs] = num_root_cells;

		for ( i = 0; i <= num_procs; i++ ) {
			cart_debug("new_proc_sfc_index[%d] = %d", i, new_proc_sfc_index[i]);
		}
	}

	MPI_Bcast(new_proc_sfc_index, num_procs+1, MPI_SFC, 0, mpi.comm.run);
	lb_set_sfc_range(decomposition, new_proc_sfc_index[local_proc_id],
			new_proc_sfc_index[local_proc_id+1]-1);
	cart_free( new_proc_sfc_index );
#else
	sfc_t num_sfc;
	int *proc_count;
	sfc_t *proc_offset;
	sfc_t *send_sfc_list;
	sfc_t *sfc_list;

	if ( local_proc_id == MASTER_NODE ) {
		sfc_t il;
		int proc;
		int *proc_map = cart_alloc(int, num_root_cells);

		proc_count = cart_alloc(int, num_procs);
		for ( proc = 0; proc < num_procs; proc++ ) {
			proc_count[proc] = 0;
		}

		/* randomly assign rank to each root cell */
		for ( il = 0; il < num_root_cells; il++ ) {
			proc_map[il] = (int)cart_rand_ulong(num_procs);
			proc_count[proc_map[il]]++;
		}

		proc_offset = cart_alloc(sfc_t, num_procs);
		proc_offset[0] = 0;
		for ( proc = 1; proc < num_procs; proc++ ) {
			proc_offset[proc] = proc_offset[proc-1]+proc_count[proc-1];
		}

		send_sfc_list = cart_alloc(sfc_t, num_root_cells);
		for ( il = 0; il < num_root_cells; il++ ) {
			send_sfc_list[proc_offset[proc_map[il]]++] = il;
		}

		cart_free(proc_map);

		proc_offset[0] = 0;
		for ( proc = 1; proc < num_procs; proc++ ) {
			proc_offset[proc] = proc_offset[proc-1]+proc_count[proc-1];
		}
	}

	/* communicate new decomposition to all ranks */
	MPI_Scatter(proc_count, 1, MPI_INT,
			&num_sfc, 1, MPI_INT, MASTER_NODE, mpi.comm.run);
	sfc_list = cart_alloc(sfc_t, num_sfc);
	MPI_Scatterv(send_sfc_list, proc_count, proc_offset, MPI_SFC,
			sfc_list, num_sfc, MPI_SFC, MASTER_NODE, mpi.comm.run);

	/* set new local decomposition */
	lb_set_sfc_list(decomposition, num_sfc, sfc_list);
	cart_free(sfc_list);

	if ( local_proc_id == MASTER_NODE ) {
		cart_free(proc_count);
		cart_free(proc_offset);
		cart_free(send_sfc_list);
	}
#endif /* SFC_DOMAIN_DECOMPOSITION */
}
