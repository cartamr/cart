#include "config.h"
#if defined(HYDRO) && defined(STAR_FORMATION) && defined(CLUSTER)

#include <math.h>

#include "auxiliary.h"
#include "control_parameter.h"
#include "hydro.h"
#include "particle.h"
#include "starformation.h"
#include "starformation_algorithm.h"
#include "starformation_recipe.h"
#include "cosmology.h"
#include "rand.h"
#include "rt.h"
#include "tree.h"
#include "units.h"

#include "models/algorithm.starII.h"
#include "models/algorithm.stargrowth.h"

void GetCubeStencil(int level, int cell, int* nb);
//extern int CubeStencilSize;
double sfr_rate(int cell);

double sf_min_stellar_particle_mass = 0;     /* not used, but needs to be set for ifrit analysis */
extern double sf_sampling_timescale ;        /* in yrs; used to be called dtmin_SF, also in HART */
int find_lowv_cluster(int icell);
int find_active_cluster(int icell);
double sfr_cell(int icell);
double cell_virial_parameter(int cell);

double fast_growth_probability=0;            /* how often does efficient star formation happen? */
double fast_growth_multiplier=10;            /* how quickly does the efficient star formation happen? */
double cluster_age_spread=15.0e6;            /* in yrs; age above which "cluster" formation ceases */
double cluster_max_gap=1.0e6;                /* in yrs; cluster will be deactived if accretion gap is longer than this value */
double cluster_min_expected_mass=100;        /* do not form a cluster particle if it is expected to grow to less than this mass */

double cluster_cloud_radius = 10;            /* in pc;  draw mass from neighboring cells within cloud_radius*/
double cluster_cloud_radius_code;

double cluster_virial_threshold = 10;
double cell_size_cgs;                       // will be used in virial threshold calculation

double cluster_age_spread_code; 
double cluster_max_gap_code; 
double cluster_min_expected_mass_code; 
/* Please note: the current version of cluster formation is not compatible with starII creation */
int starII_indicator=0;

void star_form_config_init()
{
    control_parameter_add3(control_parameter_time, &cluster_age_spread,"cluster:age-spread","cluster_age_spread","cluster_age_spread","timescale over which star particles representing clusters are allowed to grow.");
    control_parameter_add3(control_parameter_time, &cluster_max_gap,"cluster:max-gap","cluster_max_gap","cluster_max_gap","cluster will be deactived if accretion gap is longer than this value");
    control_parameter_add2(control_parameter_double, &cluster_min_expected_mass,"cluster:min-expected-mass","cluster_min_expected_mass","the minimum mass expected from sfr*cluster_age_spread allowed to seed a cluster.");

    control_parameter_add2(control_parameter_double, &cluster_cloud_radius,"cluster:cloud-radius","cluster_cloud_radius","the radius of GMCs within which the cluster particles draw mass from");

    control_parameter_add2(control_parameter_double, &cluster_virial_threshold,"cluster:virial_threshold","cluster_virial_threshold","When cluster particles are seeded, we first check that the gas is self-gravitating. We calculate the virial parameter, then seed a particle if that virial parameter is lower than this threshold value. If not set, 10 will be used.");
    
#ifdef STAR_PARTICLE_TYPES
    control_parameter_add2(control_parameter_double, &fast_growth_multiplier, "cluster:fast-growth-multiplier", "fast_growth_multiplier", "multiplier for fast growth particle star formation rates");
    control_parameter_add2(control_parameter_double, &fast_growth_probability, "cluster:fast-growth-probability", "fast_growth_probability", "probability of a fast growth particle");
#endif

    control_parameter_add2(control_parameter_bool, &starII_indicator, "starII:indicator", "starII_indicator", "turn on starII star-formation");
    starII_config_init();
}


void star_form_config_verify()
{

#ifdef STAR_PARTICLE_TYPES
    VERIFY(cluster:fast-growth-multiplier, fast_growth_multiplier >= 0);//cluster:fast-growth-multiplier
    VERIFY(cluster:fast-growth-probability, fast_growth_probability >= 0 && fast_growth_probability <=1 );
#endif
    
    VERIFY(sf:sampling-timescale, sf_sampling_timescale == 0 );

    VERIFY(cluster:age-spread, cluster_age_spread >1.0e6 );
    VERIFY(cluster:max-gap, cluster_max_gap > 0.0 );
    VERIFY(cluster:max-gap, cluster_max_gap <= cluster_age_spread );
    VERIFY(cluster:min-expected-mass, cluster_min_expected_mass > 0.0 );

    VERIFY(cluster:cloud-radius, cluster_cloud_radius > 0.0 );

    VERIFY(cluster:virial_threshold, cluster_virial_threshold > 0.0);

    VERIFY(starII:indicator,starII_indicator==1 || starII_indicator==0);
    starII_config_verify();
}

void star_form_init()
{
    starII_init();
}

void star_form_setup(int level)
{
    cluster_age_spread_code = cluster_age_spread * constants->yr/units->time ; 
    cluster_max_gap_code = cluster_max_gap * constants->yr/units->time ; 
    cluster_min_expected_mass_code = cluster_min_expected_mass * constants->Msun/units->mass ; 
    
    cluster_cloud_radius_code = cluster_cloud_radius * constants->pc/units->length;

    // will be used in virial threshold calculation. 
    cell_size_cgs = cell_size[level] * units->length;
    
    starII_setup(level);
}

extern double mfrac_starII ; /* this is given by IMF */
extern int neighbor_side_child[num_neighbors][4];
void star_form_particles(int level, int icell, double dtl, double dt_eff, float sfr){
/* creates cluster and builds until cluster age spread, samples starII; */
    int ipart;
    double dmstar;
    double mstar_expect;
    int star_type = STAR_TYPE_NORMAL;
    
    /* calculate the fractions of neighboring cells occupied by sphere with radius cluster_cloud_radius
     * four different types of neighboring cells: 1 central (fo), 6 face (ff), 12 edge (fe), 8 corner (fc) */
    float x, fo, ff, fe, fc, fcrit;
    int CubeStencilSize = 26;
    int nb26[CubeStencilSize], j;
    double V_factor, sfr_nb;
    int level_nb, level_child;

#ifdef CLUSTER_INITIAL_BOUND
    double Mbaryon;
#endif /* CLUSTER_INITIAL_BOUND */

    int k, children[num_children];

    int i, icell_child, iside;
    int nb[num_neighbors];

    x = cluster_cloud_radius_code/cell_size[level];
    fo = 0.5 * tanh((x-0.4859)/0.1645) + 0.5;
    ff = 0.5 * tanh((x-1.0843)/0.3506) + 0.5;
    fe = 0.5 * tanh((x-1.4793)/0.3462) + 0.5;
    fc = 0.5 * tanh((x-1.7862)/0.3437) + 0.5;
    fcrit = 0.2; /* equivalent to x~0.84 */

    /* if ( sfr <= 0 ) return; // this condition has been moved to /src/run/starformation_step.c; */

    if ( ff < fcrit ) /* GMC radius is smaller than cell size */
    {
        dmstar = sfr * dt_eff * cell_volume[level] * fo;
        /* dmstar = sfr * dt_eff * MIN(4.19*cluster_cloud_radius_code*cluster_cloud_radius_code*cluster_cloud_radius_code, cell_volume[level]); */
        
        ipart = find_lowv_cluster(icell); /* seeding or growing? */
        if( ipart == -1 ){
            /* SEED -- there are no young clusters  -----------------*/ 
            
            /* Check the virial and molecular fraction criteria */
            if ( cell_virial_parameter(icell) > cluster_virial_threshold ) return;
            #ifdef RADIATIVE_TRANSFER
            if ( cell_H2_fraction(icell)<0.5 ) return;
            #endif /* RADIATIVE_TRANSFER */
            
            /* seed clusters that you expect to grow to some mass -- amounts to a density threshold */
            if ( sfr*cluster_age_spread_code*cell_volume[level]*fo < cluster_min_expected_mass_code )  return;
            /* if ( sfr*cluster_age_spread_code*MIN(4.19*cluster_cloud_radius_code*cluster_cloud_radius_code*cluster_cloud_radius_code, cell_volume[level]) < cluster_min_expected_mass_code )  return; */

#if 0
            /* check whether there exists any active particles in all
             * face-touching cells. If yes, seeding is not allowed. */
            cell_all_neighbors( icell, nb );
            for ( i=0; i<num_neighbors; i++)
            {
                if ( nb[i]!=-1 )
                {
                    if ( cell_is_refined(nb[i]) )
                    {
                        for ( j=0;j<4;j++ )
                        {
                            iside = neighbor_side_child[i][j];
                            icell_child = cell_child(nb[i], iside);
                            cart_assert( cell_is_leaf(icell_child) );
                            if ( find_active_cluster(icell_child)!=-1 ) return;
                        }
                    }
                    else
                    {
                        if ( find_active_cluster(nb[i])!=-1 ) return;
                    }
                }
            }
#endif
#if 0
            /* check in 26 neighbors 1. whether there exists any active particle
             * 2. whether the current cell is density peak */
            for( j=0; j<CubeStencilSize; j++) /* in face */
            {
                if ( nb26[j] != -1 )
                {
                    if ( find_active_cluster(nb26[j]) != -1 ) return;
    #ifdef RADIATIVE_TRANSFER
                    // if ( cell_H2_density(icell)<cell_H2_density(nb26[j]) ) return;
    #else /* RADIATIVE_TRANSFER */
                    // if ( cell_gas_density(icell)<cell_gas_density(nb26[j]) ) return;
    #endif /* RADIATIVE_TRANSFER */
                }
            }
#endif

    #ifdef STAR_PARTICLE_TYPES
            if(fast_growth_probability != 0 ){
                if(cart_rand()<fast_growth_probability){ 
                    dmstar *= fast_growth_multiplier; 
                    star_type = STAR_TYPE_FAST_GROWTH;
                }
            }
            if( starII_indicator ) { starII_creation( mfrac_starII*dmstar, icell, level, dtl );}
    #endif /* STAR_PARTICLE_TYPES */
            // ipart = create_star_particle3( icell, (1-mfrac_starII)*dmstar, dtl, star_type ); 
            ipart = create_star_particle3( icell, dmstar, dtl, dt_eff, star_type ); 
                
        }else{ 
            /* GROW -- there is a young cluster present ------------*/
           
    #ifdef STAR_PARTICLE_TYPES
            if(star_particle_type[ipart] == STAR_TYPE_FAST_GROWTH ){ dmstar *= fast_growth_multiplier;}
            if( starII_indicator ) { starII_creation( mfrac_starII*dmstar, icell, level, dtl );}
    #endif
            // grow_star_particle3( ipart, (1-mfrac_starII)*dmstar, icell, level, dt_eff);
            grow_star_particle3( ipart, dmstar, icell, level, dt_eff);

    #ifdef CLUSTER_INITIAL_BOUND
            // update the maximum gas+star mass to calculate initial bound fraction
            Mbaryon = star_initial_mass[ipart] + cell_gas_density(icell)*4.189*cluster_cloud_radius_code*cluster_cloud_radius_code*cluster_cloud_radius_code;
            if (Mbaryon>star_ibound[ipart]) star_ibound[ipart] = Mbaryon;
    #endif /* CLUSTER_INITIAL_BOUND */

        }
        // if(ipart != -1)
            // star_initial_mass[ipart] += mfrac_starII*dmstar; /* add back massII for feedback (mass loss is dealt with by starII) */
    }
    else /* GMC can cover 26 neighboring cells */
    {
        GetCubeStencil(level, icell, nb26);
        ipart = find_lowv_cluster(icell); /* seeding or growing? */

        if ( ipart != -1) /* active cluster exists in current cell, grow cluster particles using 27 cells */
        {
            /* center cell */
            dmstar = sfr * dt_eff * cell_volume[level] * fo;
            grow_star_particle3( ipart, dmstar, icell, level, dt_eff);

            for( j=0; j<6; j++) /* 6 face */ 
            {
                if ( nb26[j] != -1 )
                {
                    level_nb = cell_level(nb26[j]);
                    if ( cell_is_leaf(nb26[j]) )
                    {
                         /* sfr in nb neighboring cell should be revised by using free-fall timescale of central cell,
                         * instead of that of neighboring cell. */
    #ifdef RADIATIVE_TRANSFER
                        dmstar = sfr * cell_H2_density(nb26[j]) / cell_H2_density(icell) * dt_eff * cell_volume[level] * ff;
    #else /* RADIATIVE_TRANSFER */
                        sfr_nb = sfr_cell(nb26[j]);
                        dmstar = sfr_nb * dt_eff * cell_volume[level] * ff * sqrt( cell_gas_density(icell)/cell_gas_density(nb26[j]) );
    #endif /* RADIATIVE_TRANSFER */
                        grow_star_particle3( ipart, dmstar, nb26[j], level_nb, dt_eff);
                    }
                    else
                    {
                        /* if neighboring cell is not leaf,
                         * find all children and draw gas from them if they are leaves. */
                        if (level_nb==level) V_factor = 1.0;
                        else if (level_nb==level-1) V_factor = 0.125;
                        else cart_debug("Wrong NB calculation!");
                        cell_all_children( nb26[j], children);
                        for( k=0; k<num_children; k++ )
                        {
                            if ( cell_is_leaf(children[k]) )
                            {
                                level_child = cell_level(children[k]);
    #ifdef RADIATIVE_TRANSFER
                                dmstar = sfr * cell_H2_density(children[k]) / cell_H2_density(icell) * dt_eff * cell_volume[level_child] * ff * V_factor;
    #else /* RADIATIVE_TRANSFER */
                                sfr_nb = sfr_cell(children[k]);
                                dmstar = sfr_nb * dt_eff * cell_volume[level_child] * ff * sqrt( cell_gas_density(icell)/cell_gas_density(children[k]) ) * V_factor;
    #endif /* RADIATIVE_TRANSFER */
                                grow_star_particle3( ipart, dmstar, children[k], level_child, dt_eff);
                            }
                        }
                    }
                }
            }

            for( j=6; j<18; j++) /* 12 edge */ 
            {
                if ( nb26[j] != -1 )
                {
                    level_nb = cell_level(nb26[j]);
                    if ( cell_is_leaf(nb26[j]) )
                    {
    #ifdef RADIATIVE_TRANSFER
                        dmstar = sfr * cell_H2_density(nb26[j]) / cell_H2_density(icell) * dt_eff * cell_volume[level] * fe;
    #else /* RADIATIVE_TRANSFER */
                        sfr_nb = sfr_cell(nb26[j]);
                        dmstar = sfr_nb * dt_eff * cell_volume[level] * fe * sqrt( cell_gas_density(icell)/cell_gas_density(nb26[j]) );
    #endif /* RADIATIVE_TRANSFER */
                        grow_star_particle3( ipart, dmstar, nb26[j], level_nb, dt_eff);
                    }
                    else
                    {
                        /* if neighboring cell is not leaf,
                         * find all children and draw gas from them if they are leaves */
                        if (level_nb==level) V_factor = 1.0;
                        else if (level_nb==level-1) V_factor = 0.125;
                        else cart_debug("Wrong NB calculation!");
                        cell_all_children( nb26[j], children);
                        for( k=0; k<num_children; k++ )
                        {
                            if ( cell_is_leaf(children[k]) )
                            {
                                level_child = cell_level(children[k]);
    #ifdef RADIATIVE_TRANSFER
                                dmstar = sfr * cell_H2_density(children[k]) / cell_H2_density(icell) * dt_eff * cell_volume[level_child] * fe * V_factor;
    #else /* RADIATIVE_TRANSFER */
                                sfr_nb = sfr_cell(children[k]);
                                dmstar = sfr_nb * dt_eff * cell_volume[level_child] * fe * sqrt( cell_gas_density(icell)/cell_gas_density(children[k]) ) * V_factor;
    #endif /* RADIATIVE_TRANSFER */
                                grow_star_particle3( ipart, dmstar, children[k], level_child, dt_eff);
                            }
                        }
                    }
                }
            }

            for( j=18; j<26; j++) /* 8 corners */ 
            {
                if ( nb26[j] != -1 )
                {
                    level_nb = cell_level(nb26[j]);
                    if ( cell_is_leaf(nb26[j]) )
                    {
    #ifdef RADIATIVE_TRANSFER
                        dmstar = sfr * cell_H2_density(nb26[j]) / cell_H2_density(icell) * dt_eff * cell_volume[level] * fc;
    #else /* RADIATIVE_TRANSFER */
                        sfr_nb = sfr_cell(nb26[j]);
                        dmstar = sfr_nb * dt_eff * cell_volume[level] * fc * sqrt( cell_gas_density(icell)/cell_gas_density(nb26[j]) );
    #endif /* RADIATIVE_TRANSFER */
                        grow_star_particle3( ipart, dmstar, nb26[j], level_nb, dt_eff);
                    }
                    else
                    {
                        /* if neighboring cell is not leaf,
                         * find all children and draw gas from them if they are leaves */
                        if (level_nb==level) V_factor = 1.0;
                        else if (level_nb==level-1) V_factor = 0.125;
                        else cart_debug("Wrong NB calculation!");
                        cell_all_children( nb26[j], children);
                        for( k=0; k<num_children; k++ )
                        {
                            if ( cell_is_leaf(children[k]) )
                            {
                                level_child = cell_level(children[k]);
    #ifdef RADIATIVE_TRANSFER
                                dmstar = sfr * cell_H2_density(children[k]) / cell_H2_density(icell) * dt_eff * cell_volume[level_child] * fc * V_factor;
    #else /* RADIATIVE_TRANSFER */
                                sfr_nb = sfr_cell(children[k]);
                                dmstar = sfr_nb * dt_eff * cell_volume[level_child] * fc * sqrt( cell_gas_density(icell)/cell_gas_density(children[k]) ) * V_factor;
    #endif /* RADIATIVE_TRANSFER */
                                grow_star_particle3( ipart, dmstar, children[k], level_child, dt_eff);
                            }
                        }
                    }
                }
            }

    #ifdef CLUSTER_INITIAL_BOUND
            // update the maximum gas+star mass to calculate initial bound fraction
            Mbaryon = star_initial_mass[ipart] + cell_gas_density(icell)*4.189*cluster_cloud_radius_code*cluster_cloud_radius_code*cluster_cloud_radius_code;
            if (Mbaryon>star_ibound[ipart]) star_ibound[ipart] = Mbaryon;
    #endif /* CLUSTER_INITIAL_BOUND */
        }
        else /* no cluster particle at current cell */
        {
            /* Check the virial and molecular fraction criteria */
            if ( cell_virial_parameter(icell) > cluster_virial_threshold ) return;
            #ifdef RADIATIVE_TRANSFER
            if ( cell_H2_fraction(icell)<0.5 ) return;
            #endif /* RADIATIVE_TRANSFER */

            /* check whether there exists any active particles in all
             * face-touching cells. If yes, seeding is not allowed. */
            cell_all_neighbors( icell, nb );
            for ( i=0; i<num_neighbors; i++)
            {
                if ( nb[i]!=-1 )
                {
                    if ( cell_is_refined(nb[i]) )
                    {
                        for ( j=0;j<4;j++ )
                        {
                            iside = neighbor_side_child[i][j];
                            icell_child = cell_child(nb[i], iside);
                            cart_assert( cell_is_leaf(icell_child) );
                            if ( find_active_cluster(icell_child)!=-1 ) return;
                        }
                    }
                    else
                    {
                        if ( find_active_cluster(nb[i])!=-1 ) return;
                    }
                }
            }
#if 0
            /* check in 26 neighbors 1. whether there exists any active particle
             * 2. whether the current cell is density peak */
            for( j=0; j<CubeStencilSize; j++) /* in face */
            {
                if ( nb26[j] != -1 )
                {
                    if ( find_active_cluster(nb26[j]) != -1 ) return;
    #ifdef RADIATIVE_TRANSFER
                    // if ( cell_H2_density(icell)<cell_H2_density(nb26[j]) ) return;
    #else /* RADIATIVE_TRANSFER */
                    // if ( cell_gas_density(icell)<cell_gas_density(nb26[j]) ) return;
    #endif /* RADIATIVE_TRANSFER */
                }
            }
#endif

            /* Calculate expected cluster mass from 27 neighboring cells */
            mstar_expect = sfr * cluster_age_spread_code * cell_volume[level] * fo;

            for( j=0; j<6; j++) /* 6 face */ 
            {
                if ( nb26[j] != -1 )
                {
                    level_nb = cell_level(nb26[j]);
                    if ( cell_is_leaf(nb26[j]) )
                    {
    #ifdef RADIATIVE_TRANSFER
                        mstar_expect += sfr * cell_H2_density(nb26[j]) / cell_H2_density(icell) * cluster_age_spread_code * cell_volume[level] * ff;
    #else /* RADIATIVE_TRANSFER */
                        sfr_nb = sfr_cell(nb26[j]);
                        mstar_expect += sfr_nb * cluster_age_spread_code * cell_volume[level] * ff * sqrt( cell_gas_density(icell)/cell_gas_density(nb26[j]) );
    #endif /* RADIATIVE_TRANSFER */
                    }
                    else
                    {
                        /* if neighboring cell is not leaf,
                         * find all children and draw gas from them if they are leaves */
                        if (level_nb==level) V_factor = 1.0;
                        else if (level_nb==level-1) V_factor = 0.125;
                        else cart_debug("Wrong NB calculation!");
                        cell_all_children( nb26[j], children);
                        for( k=0; k<num_children; k++ )
                        {
                            if ( cell_is_leaf(children[k]) )
                            {
                                level_child = cell_level(children[k]);
    #ifdef RADIATIVE_TRANSFER
                                mstar_expect += sfr * cell_H2_density(children[k]) / cell_H2_density(icell) * cluster_age_spread_code * cell_volume[level_child] * ff * V_factor;
    #else /* RADIATIVE_TRANSFER */
                                sfr_nb = sfr_cell(children[k]);
                                mstar_expect += sfr_nb * cluster_age_spread_code * cell_volume[level_child] * ff * sqrt( cell_gas_density(icell)/cell_gas_density(children[k]) ) * V_factor;
    #endif /* RADIATIVE_TRANSFER */
                            }
                        }
                    }
                }
            }

            for( j=6; j<18; j++) /* 12 edge */ 
            {
                if ( nb26[j] != -1 )
                {
                    level_nb = cell_level(nb26[j]);
                    if ( cell_is_leaf(nb26[j]) )
                    {
    #ifdef RADIATIVE_TRANSFER
                        mstar_expect += sfr * cell_H2_density(nb26[j]) / cell_H2_density(icell) * cluster_age_spread_code * cell_volume[level] * fe;
    #else /* RADIATIVE_TRANSFER */
                        sfr_nb = sfr_cell(nb26[j]);
                        mstar_expect += sfr_nb * cluster_age_spread_code * cell_volume[level] * fe * sqrt( cell_gas_density(icell)/cell_gas_density(nb26[j]) );
    #endif /* RADIATIVE_TRANSFER */
                    }
                    else
                    {
                        /* if neighboring cell is not leaf,
                         * find all children and draw gas from them if they are leaves */
                        if (level_nb==level) V_factor = 1.0;
                        else if (level_nb==level-1) V_factor = 0.125;
                        else cart_debug("Wrong NB calculation!");
                        cell_all_children( nb26[j], children);
                        for( k=0; k<num_children; k++ )
                        {
                            if ( cell_is_leaf(children[k]) )
                            {
                                level_child = cell_level(children[k]);
    #ifdef RADIATIVE_TRANSFER
                                mstar_expect += sfr * cell_H2_density(children[k]) / cell_H2_density(icell) * cluster_age_spread_code * cell_volume[level_child] * fe * V_factor;
    #else /* RADIATIVE_TRANSFER */
                                sfr_nb = sfr_cell(children[k]);
                                mstar_expect += sfr_nb * cluster_age_spread_code * cell_volume[level_child] * fe * sqrt( cell_gas_density(icell)/cell_gas_density(children[k]) ) * V_factor;
    #endif /* RADIATIVE_TRANSFER */
                            }
                        }
                    }
                }
            }

            for( j=18; j<26; j++) /* 8 corners */ 
            {
                if ( nb26[j] != -1 )
                {
                    level_nb = cell_level(nb26[j]);
                    if ( cell_is_leaf(nb26[j]) )
                    {
    #ifdef RADIATIVE_TRANSFER
                        mstar_expect += sfr * cell_H2_density(nb26[j]) / cell_H2_density(icell) * cluster_age_spread_code * cell_volume[level] * fc;
    #else /* RADIATIVE_TRANSFER */
                        sfr_nb = sfr_cell(nb26[j]);
                        mstar_expect += sfr_nb * cluster_age_spread_code * cell_volume[level] * fc * sqrt( cell_gas_density(icell)/cell_gas_density(nb26[j]) );
    #endif /* RADIATIVE_TRANSFER */
                    }
                    else
                    {
                        /* if neighboring cell is not leaf,
                         * find all children and draw gas from them if they are leaves */
                        if (level_nb==level) V_factor = 1.0;
                        else if (level_nb==level-1) V_factor = 0.125;
                        else cart_debug("Wrong NB calculation!");
                        cell_all_children( nb26[j], children);
                        for( k=0; k<num_children; k++ )
                        {
                            if ( cell_is_leaf(children[k]) )
                            {
                                level_child = cell_level(children[k]);
    #ifdef RADIATIVE_TRANSFER
                                mstar_expect += sfr * cell_H2_density(children[k]) / cell_H2_density(icell) * cluster_age_spread_code * cell_volume[level_child] * fc * V_factor;
    #else /* RADIATIVE_TRANSFER */
                                sfr_nb = sfr_cell(children[k]);
                                mstar_expect += sfr_nb * cluster_age_spread_code * cell_volume[level_child] * fc * sqrt( cell_gas_density(icell)/cell_gas_density(children[k]) ) * V_factor;
    #endif /* RADIATIVE_TRANSFER */
                            }
                        }
                    }
                }
            }
            if ( mstar_expect < cluster_min_expected_mass_code )  return;

            /* if no active particles in nb cells and expected mass is larger than minimum cluster mass, create a new one at center cell */
    #ifdef STAR_PARTICLE_TYPES
            if(fast_growth_probability != 0 ){
                if(cart_rand()<fast_growth_probability){ 
                    dmstar *= fast_growth_multiplier; 
                    star_type = STAR_TYPE_FAST_GROWTH;
                }
            }
            if( starII_indicator ) { starII_creation( mfrac_starII*dmstar, icell, level, dtl );}
    #endif /* STAR_PARTICLE_TYPES */
            dmstar = sfr * dt_eff * cell_volume[level] * fo;
            ipart = create_star_particle3( icell, dmstar, dtl, dt_eff, star_type ); 

            /* then grow the cluster particle just created at the center cell by using 26 neighboring cells */
            for( j=0; j<6; j++) /* 6 face */ 
            {
                if ( nb26[j] != -1 )
                {
                    level_nb = cell_level(nb26[j]);
                    if ( cell_is_leaf(nb26[j]) )
                    {
    #ifdef RADIATIVE_TRANSFER
                        dmstar = sfr * cell_H2_density(nb26[j]) / cell_H2_density(icell) * dt_eff * cell_volume[level] * ff;
    #else /* RADIATIVE_TRANSFER */
                        sfr_nb = sfr_cell(nb26[j]);
                        dmstar = sfr_nb * dt_eff * cell_volume[level] * ff * sqrt( cell_gas_density(icell)/cell_gas_density(nb26[j]) );
    #endif /* RADIATIVE_TRANSFER */
                        grow_star_particle3( ipart, dmstar, nb26[j], level_nb, dt_eff);
                    }
                    else
                    {
                        /* if neighboring cell is not leaf,
                         * find all children and draw gas from them if they are leaves */
                        if (level_nb==level) V_factor = 1.0;
                        else if (level_nb==level-1) V_factor = 0.125;
                        else cart_debug("Wrong NB calculation!");
                        cell_all_children( nb26[j], children);
                        for( k=0; k<num_children; k++ )
                        {
                            if ( cell_is_leaf(children[k]) )
                            {
                                level_child = cell_level(children[k]);
    #ifdef RADIATIVE_TRANSFER
                                dmstar = sfr * cell_H2_density(children[k]) / cell_H2_density(icell) * dt_eff * cell_volume[level_child] * ff * V_factor;
    #else /* RADIATIVE_TRANSFER */
                                sfr_nb = sfr_cell(children[k]);
                                dmstar = sfr_nb * dt_eff * cell_volume[level_child] * ff * sqrt( cell_gas_density(icell)/cell_gas_density(children[k]) ) * V_factor;
    #endif /* RADIATIVE_TRANSFER */
                                grow_star_particle3( ipart, dmstar, children[k], level_child, dt_eff);
                            }
                        }
                    }
                }
            }

            for( j=6; j<18; j++) /* 12 edge */ 
            {
                if ( nb26[j] != -1 )
                {
                    level_nb = cell_level(nb26[j]);
                    if ( cell_is_leaf(nb26[j]) )
                    {
    #ifdef RADIATIVE_TRANSFER
                        dmstar = sfr * cell_H2_density(nb26[j]) / cell_H2_density(icell) * dt_eff * cell_volume[level] * fe;
    #else /* RADIATIVE_TRANSFER */
                        sfr_nb = sfr_cell(nb26[j]);
                        dmstar = sfr_nb * dt_eff * cell_volume[level] * fe * sqrt( cell_gas_density(icell)/cell_gas_density(nb26[j]) );
    #endif /* RADIATIVE_TRANSFER */
                        grow_star_particle3( ipart, dmstar, nb26[j], level_nb, dt_eff);
                    }
                    else
                    {
                        /* if neighboring cell is not leaf,
                         * find all children and draw gas from them if they are leaves */
                        if (level_nb==level) V_factor = 1.0;
                        else if (level_nb==level-1) V_factor = 0.125;
                        else cart_debug("Wrong NB calculation!");
                        cell_all_children( nb26[j], children);
                        for( k=0; k<num_children; k++ )
                        {
                            if ( cell_is_leaf(children[k]) )
                            {
                                level_child = cell_level(children[k]);
    #ifdef RADIATIVE_TRANSFER
                                dmstar = sfr * cell_H2_density(children[k]) / cell_H2_density(icell) * dt_eff * cell_volume[level_child] * fe * V_factor;
    #else /* RADIATIVE_TRANSFER */
                                sfr_nb = sfr_cell(children[k]);
                                dmstar = sfr_nb * dt_eff * cell_volume[level_child] * fe * sqrt( cell_gas_density(icell)/cell_gas_density(children[k]) ) * V_factor;
    #endif /* RADIATIVE_TRANSFER */
                                grow_star_particle3( ipart, dmstar, children[k], level_child, dt_eff);
                            }
                        }
                    }
                }
            }

            for( j=18; j<26; j++) /* 8 corners */ 
            {
                if ( nb26[j] != -1 )
                {
                    level_nb = cell_level(nb26[j]);
                    if ( cell_is_leaf(nb26[j]) )
                    {
    #ifdef RADIATIVE_TRANSFER
                        dmstar = sfr * cell_H2_density(nb26[j]) / cell_H2_density(icell) * dt_eff * cell_volume[level] * fc;
    #else /* RADIATIVE_TRANSFER */
                        sfr_nb = sfr_cell(nb26[j]);
                        dmstar = sfr_nb * dt_eff * cell_volume[level] * fc * sqrt( cell_gas_density(icell)/cell_gas_density(nb26[j]) );
    #endif /* RADIATIVE_TRANSFER */
                        grow_star_particle3( ipart, dmstar, nb26[j], level_nb, dt_eff);
                    }
                    else
                    {
                        /* if neighboring cell is not leaf,
                         * find all children and draw gas from them if they are leaves */
                        if (level_nb==level) V_factor = 1.0;
                        else if (level_nb==level-1) V_factor = 0.125;
                        else cart_debug("Wrong NB calculation!");
                        cell_all_children( nb26[j], children);
                        for( k=0; k<num_children; k++ )
                        {
                            if ( cell_is_leaf(children[k]) )
                            {
                                level_child = cell_level(children[k]);
    #ifdef RADIATIVE_TRANSFER
                                dmstar = sfr * cell_H2_density(children[k]) / cell_H2_density(icell) * dt_eff * cell_volume[level_child] * fc * V_factor;
    #else /* RADIATIVE_TRANSFER */
                                sfr_nb = sfr_cell(children[k]);
                                dmstar = sfr_nb * dt_eff * cell_volume[level_child] * fc * sqrt( cell_gas_density(icell)/cell_gas_density(children[k]) ) * V_factor;
    #endif /* RADIATIVE_TRANSFER */
                                grow_star_particle3( ipart, dmstar, children[k], level_child, dt_eff);
                            }
                        }
                    }
                }
            }
        }
    }
}

int find_lowv_cluster(int icell){
    int ipart, ipart_store;
    double min_dv, dv, sage, sgap; 
    ipart = cell_particle_list[icell]; 
    min_dv=1e30;
    ipart_store=-1;
    while ( ipart != NULL_PARTICLE )
    {
	    if(particle_is_star(ipart) 
    #ifdef STAR_PARTICLE_TYPES
	        && (star_particle_type[ipart] == STAR_TYPE_NORMAL 
	            || star_particle_type[ipart] == STAR_TYPE_FAST_GROWTH)
    #endif
	    )
	    {   /* if a cluster exists */
	        /* if theres more than one seed then assign to star with lowest relative velocity; */
	        sage = particle_t[ipart] - star_tbirth[ipart]; /* this can be negative (floats) */
            sgap = particle_t[ipart] - star_tfinal[ipart];
	        // if (sage < cluster_age_spread_code){
	        if ( sage < cluster_age_spread_code && sgap < cluster_max_gap_code ){
	            dv =pow(particle_v[ipart][0]-cell_momentum(icell,0)/cell_gas_density(icell) , 2)+
		        pow(particle_v[ipart][1]-cell_momentum(icell,1)/cell_gas_density(icell) , 2)+
		        pow(particle_v[ipart][2]-cell_momentum(icell,2)/cell_gas_density(icell) , 2);
	            if( dv < min_dv ){ 
		            min_dv = dv;
		            ipart_store = ipart;
	            }
	        }
	    }
	    ipart = particle_list_next[ipart];
    }
    return ipart_store;
}

#if 0
/* determine whether active cluster particle exists in icell
 * if no active, return -1, else return 0
 * this is a recursive version, in which the function walk
 * through the whole oct-tree structure if icell is not leaf */
int find_active_cluster(int icell){
    int i, ichild;
    int ipart;
    double sage; 

    if (icell<0) return -1; /* neighboring cell can be -1 */

    if ( cell_is_leaf(icell) )
    {
        ipart = cell_particle_list[icell]; 
        while ( ipart != NULL_PARTICLE )
        {
	        if(particle_is_star(ipart)
        #ifdef STAR_PARTICLE_TYPES
	            && (star_particle_type[ipart] == STAR_TYPE_NORMAL 
	                || star_particle_type[ipart] == STAR_TYPE_FAST_GROWTH)
        #endif
	          )
	        {
	            /* if any active particle exists, return */
	            sage = particle_t[ipart] - star_tbirth[ipart]; /* this can be negative (floats) */
	            if (sage < cluster_age_spread_code) return 0;
	        }
	        ipart = particle_list_next[ipart];
        }
        return -1;
    }
    else
    {
        for(i=0;i<num_children;i++)
        {
            ichild = cell_child(icell, i);
            if ( find_active_cluster(ichild)==0 ) return 0;
        }
    }
    return -1;
}
#endif

/* determine whether active cluster particle exists in icell
 * if no active, return -1, else return 0
 * Note that this function can only apply to leaf cells */
int find_active_cluster(int icell){
    int ipart;
    double sage, sgap; 

    if (icell<0) return -1; /* neighboring cell can be -1 */

    ipart = cell_particle_list[icell]; 
    while ( ipart != NULL_PARTICLE )
    {
	    if(particle_is_star(ipart)
    #ifdef STAR_PARTICLE_TYPES
	        && (star_particle_type[ipart] == STAR_TYPE_NORMAL 
	            || star_particle_type[ipart] == STAR_TYPE_FAST_GROWTH)
    #endif
	      )
	    {   /* if a cluster exists */
	        /* if any active particle exists, return */
	        sage = particle_t[ipart] - star_tbirth[ipart]; /* this can be negative (floats) */
            sgap = particle_t[ipart] - star_tfinal[ipart];
	        // if (sage < cluster_age_spread_code) return 0;
	        if ( sage < cluster_age_spread_code && sgap < cluster_max_gap_code ) return 0;
	    }
	    ipart = particle_list_next[ipart];
    }
    return -1;
}

extern double sf_min_gas_number_density;
extern double sf_min_overdensity;
extern double sf_max_gas_temperature;
double sfr_cell(int icell)
{
    double tem_max, rho_min;
	tem_max = sf_max_gas_temperature/(constants->wmu*units->temperature);
	rho_min = sf_min_gas_number_density/(constants->XH*units->number_density);
#ifdef COSMOLOGY
	rho_min = MAX(rho_min,sf_min_overdensity*cosmology->OmegaB/cosmology->OmegaM);
#endif

    if ( cell_gas_density(icell)>rho_min && cell_gas_temperature(icell) < tem_max )
        return sfr_rate(icell);
    else
        return 0.0;
    // return sfr_rate(icell);
}

double cell_virial_parameter(int cell)
{
    // calculate the alpha parameter here.
    double density_code, density_cgs, alpha;
    double v_turb_squared_code, c_s_squared_code;
    double sigma_squared_code, sigma_squared_cgs;

    // We want to calculate the virial parameter alpha. A convenient formula 
    // can briefly be derived as follows:
    // alpha = 5 sigma^2 R / (3GM)
    //     here the 3 is because sigma is the 3D velocity dispersion, rather 
    //     than the 1D version observers typically use
    // alpha = 5 sigma^2 / (pi * G * rho * l^2)
    //     where l is the cell size
    //     we assumed a constant density sphere M = 4/3 pi R^3
    //     and that R = l/2, i.e. the sphere has a diameter of the cell size
    // The needed quantities are easily accessible. We assume the velocity 
    // dispersion is the sum of the sound speed and turbulent velocity in 
    // quadrature: sigma^2 = v_turb^2 + c_s^2
    // The code to calculate these was borrowed from 
    // src/sf/sf_recipe.turbulent.c

    density_code = cell_gas_density(cell);
    sigma_squared_code = cell_gas_gamma(cell)*(cell_gas_gamma(cell)-1.0)*cell_gas_internal_energy(cell)/density_code;
    #ifdef SGS_TURBULENCE
    sigma_squared_code += 2.*cell_gas_turbulent_energy(cell)/density_code;
    #endif /* SGS_TURBULENCE */         

    // convert quantities to cgs to use our nice conversion factor
    sigma_squared_cgs = sigma_squared_code * units->velocity * units->velocity;
    // sigma^2 requires the conversion factor^2
    density_cgs = density_code * units->density;
    // cell size was calculated in the setup function

    alpha = 5 * sigma_squared_cgs / (M_PI * constants->G * density_cgs * cell_size_cgs * cell_size_cgs);
    // This works because G is in cgs

    // cart_debug("alpha=%f, density_cgs=%.7le, sigma_squared_cgs=%.7le, sigma_squared_code=%.7le, v_turb_squared_code=%.7le, c_s_squared_code=%.7le, cell_size_cgs=%.7le", 
    //            alpha, density_cgs, sigma_squared_cgs, sigma_squared_code,
    //            v_turb_squared_code, c_s_squared_code, cell_size_cgs);

    return alpha;

}

struct StarFormationAlgorithm sf_algorithm_internal = 
  {
    star_form_particles,
    star_form_config_init,
    star_form_config_verify,
    star_form_init,
    star_form_setup
  };

#endif /* HYDRO && STAR_FORMATION && CLUSTER */
