#include "config.h"
#if defined(HYDRO) && defined(STAR_FORMATION)

#include <math.h>

#include "auxiliary.h"
#include "control_parameter.h"
#include "cosmology.h"
#include "hydro.h"
#include "starformation_recipe.h"
#include "rand.h"
#include "rt.h"
#include "times.h"
#include "tree.h"
#include "units.h"


/*
//  Recipe based on Eq. (17) of GK10, with tau_{SF} = const = 1.5Gyr (a-la Genzel et al 2010 & Bigiel et al)
//  New H2 model, with line overlap
*/
#ifdef ENRICHMENT

struct
{
  double factor;              /* normalization constant; used to be called fmass */
  double efficiency;          /* defined as Sigma_SFR*1.5Gyr/(1.36*Sigma_H2) */
  double variability;
  double min_molecular_fraction;
  double max_formation_time_scale;
#ifndef RADIATIVE_TRANSFER
  double U_MW;                /* value to use without RT */
#endif /* RADIATIVE_TRANSFER */
}
  sfr = { 0.0, 1.0, 0.0, 0.0, 0.0
#ifndef RADIATIVE_TRANSFER
	  , 1.0
#endif /* RADIATIVE_TRANSFER */
  };


void sfr_config_init()
{
  control_parameter_add2(control_parameter_double,&sfr.efficiency,"sf:efficiency","sfr.efficiency","the relative efficiency of the star formation law (relative to the constant depletion time-scale of 1.5 Gyr).");

  control_parameter_add2(control_parameter_double,&sfr.variability,"sf:variability","sfr.variability","the variability of the efficiency. If <sf:variability> is greater than 0, then it serves as a dispersion of a lognormal distribution with which the actual SF efficiency fluctuates around <sf:efficiency> (i.e. <sf:efficiency> remains the mean of the distribution).");

  control_parameter_add2(control_parameter_double,&sfr.min_molecular_fraction,"sf:min-molecular-fraction","sfr.min_molecular_fraction","the minimum molecular (H2) fraction for star formation.");

  control_parameter_add2(control_parameter_double,&sfr.max_formation_time_scale,"sf:max-formation-time-scale","sfr.max_formation_time_scale","the maximum time-scale for H2 formation when RT_CHEMISTRY is unset; in that case fitting formulae of GD14 are used, and they assume rapid H2 formation, which will not be true for low metallciity environments. If that value is 0, then the age of the universe is assumed, if it is negative, then no limit is imposed (i.e. just GD14 is used).");

#ifndef RADIATIVE_TRANSFER
  control_parameter_add2(control_parameter_double,&sfr.U_MW,"sf:U_MW","sfr.U_MW","A constant value for the U_MW to use without RT.");
#endif /* RADIATIVE_TRANSFER */
}


void sfr_config_verify()
{
  VERIFY(sf:efficiency, sfr.efficiency > 0.0 );
  VERIFY(sf:variability, !(sfr.variability < 0.0) );
  VERIFY(sf:min-molecular-fraction, !(sfr.min_molecular_fraction < 0.0) );
  VERIFY(sf:max-formation-time-scale, 1 );
#ifndef RADIATIVE_TRANSFER
  VERIFY(sf:U_MW, sfr.U_MW > 0 );
#endif /* RADIATIVE_TRANSFER */
}


void sfr_setup(int level)
{
  sfr.factor = sfr.efficiency*units->time/(1.5*constants->Gyr);
}


double fracH2_GD14(int cell);

double sfr_rate(int cell)
{
#if defined(RADIATIVE_TRANSFER) && defined(RT_CHEMISTRY)
  /*
  //  Factor 1.36 is what observers add.
  */
  double fracH2 = 2*cell_H2_density(cell)/(0.76*cell_gas_density(cell));
#else

  double fracH2 = fracH2_GD14(cell);

#endif /* RADIATIVE_TRANSFER && RT_CHEMISTRY */
 
  if(fracH2 > sfr.min_molecular_fraction)
    {
      if(sfr.variability > 0.0)
	{
	  return sfr.factor*fracH2*cell_gas_density(cell)*cart_rand_lognormal(sfr.variability);
	}
      else
	{
	  return sfr.factor*fracH2*cell_gas_density(cell);
	}
    }
  else return 0.0;
}


struct StarFormationRecipe sf_recipe_internal =
{
  sfr_rate,
  sfr_config_init,
  sfr_config_verify,
  sfr_setup
};

#else

#error "SF Recipe line-in-H2 only works with ENRICHMENT activated."

#endif /* ENRICHMENT */

#endif /* HYDRO && STAR_FORMATION */


double fracH2_GD14(int cell)
{
#ifdef RADIATIVE_TRANSFER
  double U = rtUmwFS(cell);
  double D = rtDmwFL(cell);
#else
  double U = sfr.U_MW;
  double D = cell_gas_metal_density(cell)/(constants->Zsun*cell_gas_density(cell));
#endif /* RADIATIVE_TRANSFER */

  double S0 = units->length*cell_size[cell_level(cell)]/(100*constants->pc); /* as calibrated from 6 CHIMP runs */
  double S = (S0 > 0.3) ? S0 : 0.3;
  double D0 = 0.17*(1+1/(1+pow(S,5.0)));
  double U0 = 9*D0/S;
  double N0 = 14*sqrt(D0)/S;
  double g = sqrt(D*D+D0*D0);
  double q = log(1+pow(0.05/g+U,0.67)/U0*pow(g,0.33));
  double nth = N0*q/g;
  double w = 0.8 + sqrt(q)/pow(S,0.33);
  double nH = units->number_density*cell_gas_density(cell)*0.76;
  double x = w*log(nH/nth);
  double y = -x*(1-0.02*x+0.001*x*x);
 
  return 1/(1+exp(MIN(30,y)));
}
