#include "config.h"
#if defined(HYDRO) && defined(STAR_FORMATION)

#ifndef SGS_TURBULENCE
#error "SGS_TURBULENCE must be defined for turbulent SF_RECIPE"
#endif

#include <math.h>

#include "auxiliary.h"
#include "control_parameter.h"
#include "hydro.h"
#include "starformation_recipe.h"
#include "rand.h"
#include "rt.h"
#include "tree.h"
#include "units.h"


/*
 *  Recipe based on Eq. (2) of Padoan et al. 2012, ApJ, 759, L27
 */

struct {
  double efficiency;          /* Padoan et al. 2012: epsilon = efficiency*exp(-slope*(t_ff/t_dyn)); maximal efficiency of gas conversion into stars per t_ff (less than 1.0 due to loss by winds and etc.) */
  double slope;          	  /* Padoan et al. 2012: epsilon = efficiency*exp(-slope*(t_ff/t_dyn)); slope = 1.6 is given in Padoan et al. 2012 */

  double t_ff_factor;
  double t_dyn_factor;
}
  sfr = { 1.0, 1.6,
          0.0, 0.0 };


void sfr_config_init() {
	  control_parameter_add2(control_parameter_double,&sfr.efficiency,"sf:efficiency","sfr.efficiency","Padoan et al. 2012: epsilon = efficiency*exp(-slope*(t_ff/t_dyn)); maximal efficiency of gas conversion into stars per t_ff (less than 1.0 due to loss by winds and etc.)");

	  control_parameter_add2(control_parameter_double,&sfr.slope,"sf:slope","sfr.slope","Padoan et al. 2012: epsilon = efficiency*exp(-slope*(t_ff/t_dyn)); slope = 1.6 is given in Padoan et al. 2012");
}


void sfr_config_verify() {
  VERIFY(sf:efficiency, sfr.efficiency >= 0.0 );

  VERIFY(sf:slope, 1 );
}


void sfr_setup(int level) {
  sfr.t_ff_factor = sqrt(3.*M_PI/(32.*constants->G * units->density))/units->time;
  sfr.t_dyn_factor = 0.5*cell_size[level];
}


double sfr_rate(int cell)
{
  double density = cell_gas_density(cell);
  double rho_sigma_2 = 2.*cell_gas_turbulent_energy(cell);

  /*
   * Account for thermal pressure in virial parameter
   */
  rho_sigma_2 += cell_gas_gamma(cell)*(cell_gas_gamma(cell)-1.0)*cell_gas_internal_energy(cell);

  double t_ff_code = sfr.t_ff_factor/sqrt(density);
  double t_dyn_code = sfr.t_dyn_factor/sqrt( rho_sigma_2/density );

  return density/t_ff_code * sfr.efficiency * exp(-sfr.slope*t_ff_code/t_dyn_code);
}


struct StarFormationRecipe sf_recipe_internal =
{
  sfr_rate,
  sfr_config_init,
  sfr_config_verify,
  sfr_setup
};


#endif /* HYDRO && STAR_FORMATION */
