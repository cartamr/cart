#include "config.h"
#if defined(HYDRO) && defined(STAR_FORMATION)

#include <math.h>
#include <stdio.h>

#include "starformation_recipe.h"
#include "tree.h"

#include "models/algorithm.mcf.h"


/*
//  Recipe outputs 0 or 1 based on whether molecular gas fraction is above a specified threshold.
//  Details of evolution are handled in feedback. 
*/
#if defined(ENRICHMENT)

double sfr_rate(int cell)
{
    double f = mcf_fH2(cell,cell_gas_density(cell)) - mcf->fH2min;

	/*
	//  Correct for the left-over molecular gas  
	*/
	float den_left = (1-f)*cell_gas_density(cell);
	f = mcf_fH2(cell,den_left) - mcf->fH2min;
	
    if(f > 0)
    {
        return f;
    }
    else
    {
        return 0.0;
    }
}


struct StarFormationRecipe sf_recipe_internal =
{
    sfr_rate,
    NULL,
    NULL
};

#else

#error "SF Recipe <mcf> only works with ENRICHMENT activated."

#endif /* ENRICHMENT */

#endif /* HYDRO && STAR_FORMATION */

