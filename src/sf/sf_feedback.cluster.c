#include "config.h"

#include <stdlib.h>
#include "auxiliary.h"
#include "starformation_feedback.h"
#if defined(STAR_FORMATION) && defined(CLUSTER_FEEDBACK)

#include "models/feedback.snII.h"
#include "models/feedback.snIa.h"
#include "models/feedback.ml.h"
#include "models/feedback.winds.h"
#include "models/feedback.rapSR.h"

#include "models/feedback.irtrapping.h"
#include "models/feedback.kinetic.h"
#include "models/feedback.rad.h"

#include "starformation.h"
#include "particle.h"
#include "tree.h"

extern double rapSR_boost;
extern double wind_momentum_boost;
extern double tauIR_boost;
extern double cluster_age_spread_code;
extern double cluster_min_expected_mass_code;

void sfb_config_init()
{
  snII_config_init();
  snIa_config_init();
  ml_snl2012_config_init();
  wind_config_init();
  rapSR_config_init();

  trapIR_config_init();
  kfb_config_init();
}


void sfb_config_verify()
{
  snII_config_verify();
  snIa_config_verify();
  ml_snl2012_config_verify();
  wind_config_verify();
  rapSR_config_verify();

  trapIR_config_verify();
  kfb_config_verify();
}


void sfb_init()
{
  snII_init();
  snIa_init();
  ml_init();
  wind_init();
  rapSR_init();

/*   trapIR_init();  */
}

void sfb_setup(int level)
{
    snII_setup(level);
    snIa_setup(level);
    rad_setup(level);
    ml_setup(level);
    wind_setup(level);
    rapSR_setup(level);

    trapIR_setup(level);
/*     kfb_setup(level); */
}


void sfb_hydro_feedback(int level, int cell, int ipart, double t_next ) {
#if defined(HYDRO) && defined(PARTICLES)
	snII_thermal_feedback(level,cell,ipart,t_next);
	snII_kinetic_feedback(level,cell,ipart,t_next);
	ml_feedback(level,cell,ipart,t_next);

	if(rapSR_boost > 0){
		rapSR_kick(level,cell,ipart,t_next);
	}

	if(wind_momentum_boost > 0){
		stellar_wind_kick(level,cell,ipart,t_next);
	}

	snIa_thermal_feedback(level,cell,ipart,t_next);
#endif /* HYDRO && PARTICLES */
}

void sfb_hydro_feedback_cell(int level, int cell, double t_next, double dt )
{
#if defined(HYDRO) && defined(PARTICLES)
	cell_trapIR(level, cell, t_next, dt);
#endif /* HYDRO && PARTICLES */
}

struct StellarFeedbackParticle sf_feedback_particle_internal =
  {
    sfb_hydro_feedback,
    rad_luminosity_popM,
    NULL,
    sfb_config_init,
    sfb_config_verify,
    sfb_init,
    sfb_setup
  };

struct StellarFeedbackCell sf_feedback_cell_internal =
{
    sfb_hydro_feedback_cell,
    NULL
    //gather_kicks
};

#else

struct StellarFeedbackParticle sf_feedback_particle_internal;
struct StellarFeedbackCell sf_feedback_cell_internal;

#endif /* STAR_FORMATION && CLUSTER_FEEDBACK */
