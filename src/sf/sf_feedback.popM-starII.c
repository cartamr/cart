#include "config.h"
#ifdef STAR_FORMATION
#ifdef STAR_PARTICLE_TYPES

#include <math.h>
#include <string.h>

#include "auxiliary.h"
#include "tree.h"
#include "units.h"
#include "starformation_algorithm.h"
#include "starformation_feedback.h"
#include "models/onestarfits.h"

#include "models/feedback.snII.h"
#include "models/feedback.snIa.h"
#include "models/feedback.ml.h"
#include "models/feedback.winds.h"
#include "models/feedback.rapSR.h"
#include "models/feedback.rad.h"

#include "models/feedback.starII-snII.h"
#include "models/feedback.starII-winds.h"
#include "models/feedback.starII-rapSR.h"
#include "models/feedback.starII-rad.h"

#include "models/feedback.irtrapping.h"
#include "models/feedback.kinetic.h"

#include "times.h"
#include "timing.h"
#include "starformation.h"
#include "particle.h"

extern double starII_minimum_mass;
extern double tdelay_popM_feedback;

extern double rapSR_boost;
extern double wind_momentum_boost;
extern double starII_rapSR_boost;
extern double starII_wind_momentum_boost;

extern double tauIR_boost;

void sfb_config_init()
{
  snII_config_init();
  snIa_config_init();
  ml_snl2012_config_init();
  wind_config_init();
  rapSR_config_init();

  starII_wind_config_init();
  starII_rapSR_config_init();
  starII_explosion_config_init();

  trapIR_config_init();
  kfb_config_init();
}


void check_fsdefs_compatible()
{
    if(strcmp("continuous",sf_algorithm_name)!=0 &&
       strcmp("hart-starII",sf_algorithm_name)!=0
        ){
        cart_error("SF_ALGORITHM needs to be <continous> or <hart-starII> for SF_FEEDBACK -starII variants");
    }
}


void sfb_config_verify()
{
  check_fsdefs_compatible();

  snII_config_verify();
  snIa_config_verify();
  ml_snl2012_config_verify();
  wind_config_verify();
  rapSR_config_verify();

  starII_wind_config_verify();
  starII_rapSR_config_verify();
  starII_explosion_config_verify();

  trapIR_config_verify();
  kfb_config_verify();
}


void sfb_init()
{
  snII_init();
  snIa_init();
  ml_init();
  wind_init();
  rapSR_init();

/*   starII_wind_init(); */
/*   starII_rapSR_init(); */
  starII_explosion_feedback_init();

/*   trapIR_init();  */
}

void sfb_setup(int level)
{
    snII_setup(level);
    snIa_setup(level);
    rad_setup(level);
    ml_setup(level);
    wind_setup(level);
    rapSR_setup(level);

/*     starII_wind_setup(level); */
/*    starII_rapSR_setup(level); */
    starII_explosion_setup(level);

    trapIR_setup(level);
/*     kfb_setup(level); */
}


void sfb_hydro_feedback(int level, int cell, int ipart, double t_next )
{
#if defined(HYDRO) && defined(PARTICLES)
    double ini_mass_sol, Zsol ;
    float star_age = particle_t[ipart] - star_tbirth[ipart] ; /* this can be negative (float) */
    double age_yr = star_age * units->time/constants->yr;
    Zsol = star_metallicity_II[ipart]/constants->Zsun;

    if( star_particle_type[ipart] ==  STAR_TYPE_NORMAL || star_particle_type[ipart] == STAR_TYPE_FAST_GROWTH ){

 	if( star_age > tdelay_popM_feedback ){  /* turn on popM fb after starII feedback is done */
	    /* not exactly right -- big dt misses the start when t_next>lifetime */
	    snII_thermal_feedback(level,cell,ipart,t_next);
	    ml_feedback(level,cell,ipart,t_next);

 	    snII_kinetic_feedback(level,cell,ipart,t_next);
	    if(rapSR_boost > 0){
		rapSR_kick(level,cell,ipart,t_next);
	    }
	    if(wind_momentum_boost > 0){
		stellar_wind_kick(level,cell,ipart,t_next);
	    }
	}
	snIa_thermal_feedback(level,cell,ipart,t_next);

    } else if ( star_particle_type[ipart] == STAR_TYPE_STARII ){

	ini_mass_sol = star_initial_mass[ipart]*units->mass/constants->Msun;

	if( star_age > OneStar_stellar_lifetime(ini_mass_sol, Zsol) ){

	    starII_explosion_mass(level, cell, ipart);
 	    starII_explosion_kicks(level, cell, ipart);
	    starII_explosion_thermal(level, cell, ipart);
	}else{

	    if(starII_rapSR_boost > 0){
		starII_rapSR_kick(level, cell, ipart,ini_mass_sol,age_yr,Zsol, t_next);
	    }
	    if(starII_wind_momentum_boost > 0){
		starII_stellar_wind_kick(level, cell, ipart,ini_mass_sol,age_yr,Zsol, t_next);
	    }
	    /* RaP longrange is cell-by-cell */
	}
    }
#endif /* HYDRO && PARTICLES */
}

void sfb_hydro_feedback_cell(int level, int cell, double t_next, double dt )
{
#if defined(HYDRO) && defined(PARTICLES)
	cell_trapIR(level, cell, t_next, dt);
#endif /* HYDRO && PARTICLES */
}

struct StellarFeedbackParticle sf_feedback_particle_internal =
{
    sfb_hydro_feedback,
    rad_luminosity_popM_ionizingstarII0,
    NULL,
    sfb_config_init,
    sfb_config_verify,
    sfb_init,
    sfb_setup
};

struct StellarFeedbackCell sf_feedback_cell_internal =
{
    sfb_hydro_feedback_cell,
    //gather_kicks
    NULL
};

#else
#error "popM-starII feedback model requires STAR_PARTICLE_TYPES option"
#endif /* STAR_PARTICLE_TYPES */
#endif /* STAR_FORMATION */
