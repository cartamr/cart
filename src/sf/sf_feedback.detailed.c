#include "config.h"
#ifdef STAR_FORMATION

#include <math.h>
#include <string.h>
#include <stdlib.h>
#include "auxiliary.h"

#include "starformation_feedback.h"

#include "models/feedback.SNR-model.h"
#include "models/feedback.snIa-detailed.h"
#include "models/feedback.snII-detailed.h"
#include "models/feedback.AGB-detailed.h"
#include "models/feedback.winds-detailed.h"
#include "models/feedback.rad.h"
#include "models/feedback.kinetic.h"
#include "models/feedback.winds.h"
#include "models/feedback.rapSR.h"
#include "models/feedback.irtrapping.h"
#include "models/feedback.detailed_enrich.h"

#include "starformation.h"
#include "particle.h"
#include "tree.h"

extern double rapSR_boost;
extern double wind_momentum_boost;
extern double tauIR_boost;
extern double cluster_age_spread_code;
extern double cluster_max_gap_code;
extern double cluster_min_expected_mass_code;

void sfb_config_init() {
    snIa_detailed_config_init();
	snII_detailed_config_init();
	AGB_detailed_config_init();
	winds_detailed_config_init();
    wind_config_init();
    rapSR_config_init();
    trapIR_config_init();
	kfb_config_init();
	snr_model_config_init();
	detailed_enrichment_config_init();
}

void sfb_config_verify() {
    snIa_detailed_config_verify();
	snII_detailed_config_verify();
	AGB_detailed_config_verify();
	winds_detailed_config_verify();
    wind_config_verify();
    rapSR_config_verify();
    trapIR_config_verify();
	kfb_config_verify();
	snr_model_config_verify();
	detailed_enrichment_config_verify();
}

void sfb_init() {
    snIa_detailed_init();
	snII_detailed_init();
	AGB_detailed_init();
	winds_detailed_init();
    wind_init();
    rapSR_init();
	detailed_enrichment_init();
}

void sfb_setup(int level) {
    snIa_detailed_setup(level);
	snII_detailed_setup(level);
	AGB_detailed_setup(level);
	winds_detailed_setup(level);
	rad_setup(level);
    wind_setup(level);
    rapSR_setup(level);
    trapIR_setup(level);
	(*snr_model_setup)(level);
	detailed_enrichment_setup(level);
}


#if defined(HYDRO) && defined(PARTICLES)
void sfb_hydro_feedback(int level, int cell, int ipart, double t_next ) {

	if(rapSR_boost > 0){
		rapSR_kick(level,cell,ipart,t_next);
	}

	if(wind_momentum_boost > 0){
		stellar_wind_kick(level,cell,ipart,t_next);
	}

	snIa_thermal_feedback_detailed(level, cell, ipart, t_next);
	snII_remnant_feedback_detailed(level, cell, ipart, t_next);
	AGB_enrichment(level, cell, ipart, t_next);
	winds_enrichment(level, cell, ipart, t_next);

}
#endif /* HYDRO && PARTICLES */

void sfb_hydro_feedback_cell(int level, int cell, double t_next, double dt )
{
#if defined(HYDRO) && defined(PARTICLES)
	cell_trapIR(level, cell, t_next, dt);
#endif /* HYDRO && PARTICLES */
}

struct StellarFeedbackParticle sf_feedback_particle_internal = {
		sfb_hydro_feedback,
		rad_luminosity_hart,
		NULL,
		sfb_config_init,
		sfb_config_verify,
		sfb_init,
		sfb_setup
};

struct StellarFeedbackCell sf_feedback_cell_internal = {
		sfb_hydro_feedback_cell,
		NULL
};
#endif /* STAR_FORMATION */
