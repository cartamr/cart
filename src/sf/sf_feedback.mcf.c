#include "config.h"
#ifdef STAR_FORMATION

#include <stdlib.h>

#include "particle.h"
#include "starformation.h"
#include "starformation_feedback.h"

#include "models/algorithm.mcf.h"
#include "models/feedback.rad.h"


void sff_config_init()
{
    mcf_config_init();
}


void sff_config_verify()
{
    mcf_config_verify();
}


void sff_init()
{
    mcf_init();
}


void sff_setup(int level)
{
    mcf_setup(level);
}


#if defined(HYDRO) && defined(PARTICLES)
void sff_hydro_feedback(int level, int cell, int ipart, double t_next )
{
    mcf_feedback(level,cell,ipart,t_next);
}
#endif /* HYDRO && PARTICLES */


float sff_rad_feedback(int ipart)
{
    if(particle_mass[ipart]>0 && star_initial_mass[ipart]<=particle_mass[ipart])
    {
      return 0; //rad_luminosity_popM2(ipart,mcf->fRad)*(star_initial_mass[ipart]/particle_mass[ipart]);
    }
    else
    {
        return rad_luminosity_popM(ipart);
    }
}


struct StellarFeedbackParticle sf_feedback_particle_internal = 
{
    sff_hydro_feedback,
    sff_rad_feedback,
    NULL,
    sff_config_init,
    sff_config_verify,
    sff_init,
    sff_setup
};


struct StellarFeedbackCell sf_feedback_cell_internal =
{
    NULL,
    NULL
};


#endif /* STAR_FORMATION */
