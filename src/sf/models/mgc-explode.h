#ifndef __FEEDBACK_MGC_EXPLODE_H__
#define __FEEDBACK_MGC_EXPLODE_H__

#ifndef CONFIGURED
#error "Missing config.h include."
#endif


#ifdef STAR_FORMATION

void mgc_explode_config_init();
void mgc_explode_config_verify();
void mgc_explode_init();
void mgc_explode_setup(int level);

#if defined(HYDRO) && defined(PARTICLES)
void mgc_explode_thermal_feedback(int level, int cell, int ipart, double t_next );
void mgc_explode_kinetic_feedback(int level, int cell, int ipart, double t_next );
void mgc_explode_remnant_feedback(int level, int cell, int ipart, double t_next );
#endif /* HYDRO && PARTICLES */

struct MGC_EXPLODE_t
{
  double explosion_temperature;    /* Temperature to reinject remaining mg mass*/   
  double explosion_time;           /* time of mgc explosion */
  double mass_loss_time;           /* time to stretch out mass loss after explosion time*/
  double yield_factor;             /* fraction yield relative to the one coded in */
  double min_mass;                 /* used to be called aM_SNII */
  double max_mass;                 
};
struct MGC_EXPLODE_PROP_t /* physical counterparts*/
{
  double temperature;
  double metals;
  double teject;                    /* physical mgc explosion time */
  double tml;                       /* physical mgc mass loss time after explosion */
  double fmass;
};

extern struct MGC_EXPLODE_t mgc_explode;
extern struct MGC_EXPLODE_PROP_t mgc_explode_phys, mgc_explode_code;
#endif /* STAR_FORMATION */
#endif
