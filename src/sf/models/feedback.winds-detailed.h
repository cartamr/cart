#ifndef __FEEDBACK_WINDS_DETAILED__
#define __FEEDBACK_WINDS_DETAILED__

// functions used externally
#ifdef STAR_FORMATION
#ifndef PY_TESTING

void winds_detailed_config_init();
void winds_detailed_config_verify();
void winds_detailed_init();
void winds_detailed_setup(int);

void winds_enrichment(int, int, int, double);

#endif /* PY_TESTING */
#endif /* STAR_FORMATION */

#ifdef PY_TESTING
double get_ejecta_winds_py(double, double, double, double, double, double, double);
#endif /* PY_TESTING */

#endif  /* Header guard */