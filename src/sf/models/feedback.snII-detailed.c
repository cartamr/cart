/**
 * Note that the feedback.detailed-enrich.c file has much of the functionality
 * that this uses. For this to work correctly, make sure both the 
 * detailed_enrichment_init() and detailed_enrichment_setup() functions are 
 * used in the appropriate sections of the sf_feedback.*.c file being used.
 */
#ifndef PY_TESTING
#include "config.h"
#endif /* PY_TESTING */

#if defined(STAR_FORMATION) && defined(ENRICHMENT)

#ifndef PY_TESTING
#include "hydro.h"              // gas sound speed, temperature
#include "auxiliary.h"          // local_proc_id
#include "control_parameter.h"  // parameter parsing
#include "cosmology.h"          // tphys_from_tcode
#include "imf.h"                // stellar lifetimes
#include "parallel.h"           // MASTER_NODE
#include "starformation.h"      // star particle arrays
#include "tree.h"               // gas density functions
#include "units.h"              // units
#include "rand.h"               // random number generator
#include "times.h"              // abox (used in debugging)
#include "feedback.SNR-model.h" // SNR feedback model
#include "feedback.kinetic.h"   // distribute_momentum
#include "hydro_sgst.h"         // gas turbulence properties
#include "plugin.h"             // Plugins allowed in feedback

// Grab variables from other files
extern double dUfact;
extern double dKfact;
extern double feedback_temperature_ceiling;
extern double feedback_turbulence_temperature_ceiling;
extern float fixed_metallicity;

#endif /* PY_TESTING */

#include "feedback.detailed_enrich.h"

#include <math.h>               // log10

// When testing we need to initialize own own random number generator
#ifdef PY_TESTING
#include <gsl/gsl_rng.h>        // random numbers
#include <gsl/gsl_randist.h>    // random numbers
gsl_rng *r;
void init_rand(void) {
	r = gsl_rng_alloc(gsl_rng_mt19937);
}
#endif /* PY_TESTING */

void feedback_snII_core(double*, double, double, double, double, double);
void snr_feedback_detailed(int, int, int, double, double, double, double);
double hn_energy(double);
double hn_fraction(double);
int my_rand_binomial(double, unsigned int);

// Get the needed attributes from the detailed enrichment file.
extern int detail_n_fields_sn_ii;
extern int detail_field_idx_sn_ii_C;
extern int detail_field_idx_sn_ii_N;
extern int detail_field_idx_sn_ii_O;
extern int detail_field_idx_sn_ii_Mg;
extern int detail_field_idx_sn_ii_S;
extern int detail_field_idx_sn_ii_Ca;
extern int detail_field_idx_sn_ii_Fe;
extern int detail_field_idx_sn_ii_metals;
extern int detail_field_idx_sn_ii_total;


// Create the structure to hold configuration values. 
struct
{
    double min_mass;
    double max_mass;
    double hn_threshold;
    double hn_fraction_0;
}
snII_detailed = {8.0, 50.0, 20.0, 0.5};

// Also make a structure for code mass quantities. I initialize them with
// physical units for E51 and 1 for inv_vol so that they can be used like this
// in the python tests, where code units aren't actually defined. 
struct
{
    double inv_vol; // cell inverse volume
    double E51;  // 10^51 ergs
}
snII_detailed_code = {1, 1E51};

#ifndef PY_TESTING
void snII_detailed_config_init()
{
    control_parameter_add4(control_parameter_double,&snII_detailed.min_mass,"snII:min-mass","snII.min_max","imf:min-SNII-mass","am_snii","the minimum mass of stars that explode as type II supernovae. Note that this is not checked against the AGB maximum mass for consistency.");
    control_parameter_add2(control_parameter_double,&snII_detailed.max_mass,"snII:max-mass","snII.max_max","the maximum mass of stars that explode as type II supernovae.");
    control_parameter_add2(control_parameter_double,&snII_detailed.hn_fraction_0,"snII:hn-fraction", "snII.hn_fraction_0","The fraction of stars above `hn_threshold` that explode as hypernovae at low metallicity. This decreases at higher metallicity, following Grimmett et al 2020 Eq 1. Must be between zero and 1. Default value is 0.5, as recommended by Kobayashi+2006, 2011, Nomoto+2013, Grimmett et al 2020");
    control_parameter_add2(control_parameter_double,&snII_detailed.hn_threshold,"snII:hn-threshold", "snII.hn_threshold","The minimum mass of star that can explode as a hypernova. Default value is 20, which is the lowest mass hypernova model in Kobayashi+2006.");
}

void snII_detailed_config_verify()
{
    VERIFY(snII:min-mass, snII_detailed.min_mass > 1.0 );
    VERIFY(snII:max-mass, snII_detailed.max_mass > snII_detailed.min_mass );
    VERIFY(snII:max-mass, snII_detailed.max_mass <= imf->max_mass);
    VERIFY(snII:hn-threshold, snII_detailed.hn_threshold >= snII_detailed.min_mass);
    VERIFY(snII:hn-threshold, snII_detailed.hn_threshold <= snII_detailed.max_mass);
    VERIFY(snII:hn-fraction, snII_detailed.hn_fraction_0 >= 0.0);
    VERIFY(snII:hn-fraction, snII_detailed.hn_fraction_0 <= 1.0);
}

void snII_detailed_init()
{
    // diagnostics
    if(local_proc_id == MASTER_NODE) {
        cart_debug("SNII: mass range: %g to %g Msun, HN are %g%% of SN above %g Msun at Z=0", 
                   snII_detailed.min_mass, snII_detailed.max_mass,
                   snII_detailed.hn_fraction_0*100, snII_detailed.hn_threshold);
    }
}

void snII_detailed_setup(int level)
{
    snII_detailed_code.inv_vol = cell_volume_inverse[level];
    snII_detailed_code.E51 = 1E51 * constants->erg / units->energy;
}

void snII_remnant_feedback_detailed(int level, int cell, int ipart, 
                                    double t_next){
    int idx_field;
    double star_total_metallicity, m_initial;
    double t, tn, dt, age, tb;
    // Get time information. These are all in years.
    t = tphys_from_tcode(particle_t[ipart]);
    tn = tphys_from_tcode(t_next);
    dt = tn - t;

    tb = tphys_from_tcode(star_tbirth[ipart]);
    #ifdef CLUSTER
    double t_ave = tphys_from_tcode(star_tbirth[ipart] + star_ave_age[ipart]);
    age = t - t_ave;
    #else /* CLUSTER */
    age = t - tb;
    #endif /* CLUSTER  */

    // Get stellar metallicity
    #ifdef ENRICHMENT
    star_total_metallicity = star_metallicity_II[ipart];
    #ifdef ENRICHMENT_SNIa
    star_total_metallicity += star_metallicity_Ia[ipart];
    #endif /* ENRICHMENT_SNIa */
    #ifdef ENRICHMENT_ELEMENTS
    star_total_metallicity += star_metallicity_AGB[ipart];
    #endif /* ENRICHMENT_ELEMENTS */
    #else /* ENRICHMENT */
    star_total_metallicity = fixed_metallicity;
    #endif /* ENRICHMENT */

    // Turn these ages into their equivalent main sequence turnoff masses
    double log_Z = log10(star_total_metallicity);
    double m_turnoff_now, m_turnoff_next;
    // During the first timestep of a star's life, it's age will be negative, 
    // since t_ave is designated as being at t_now + 0.5 dt. We need to check
    // for that. 
    if (age < 0){
        m_turnoff_now = 120.0;
    }
    else{
        m_turnoff_now = pow(10, mlf_early_times(log10(age), log_Z));
    }
    // the age at the end of the timestep will always be positive, so we don't
    // need to do that check
    m_turnoff_next = pow(10, mlf_early_times(log10(age + dt), log_Z));

    m_initial = star_initial_mass[ipart];

    if (m_turnoff_now > snII_detailed.min_mass){ // can SN explode now?
        // We'll get the full yield set from the core detailed enrichment 
        // prescription, then only add the extra elements if they're needed
        double sn_yields[detail_n_fields_sn_ii+3];  //extra is for energy, n_sn, future n_sn

        #ifdef DISCRETE_SN
        feedback_snII_core(sn_yields, star_unexploded_sn[ipart], 
                           m_turnoff_now, m_turnoff_next, 
                           star_total_metallicity, m_initial);
        #else /* DISCRETE_SN */
        // here we never leave SN unexploded
        feedback_snII_core(sn_yields, 0, 
                           m_turnoff_now, m_turnoff_next, 
                           star_total_metallicity, m_initial);
        #endif /* DISCRETE_SN */

        // now we can drop in the SN
        if (sn_yields[detail_n_fields_sn_ii + 1] > 0){  // N_SN > 0
            snr_feedback_detailed(level, cell, ipart, t_next,
                                  sn_yields[detail_field_idx_sn_ii_total],
                                  sn_yields[detail_n_fields_sn_ii], // energy
                                  sn_yields[detail_n_fields_sn_ii + 1]); // n_sn
        }

        // Set the number of SN that didn't explode in this timestep. If SN
        // are still active, we use the value returned by the function. 
        // But if this is the last timestep for SN, we set this to zero, so 
        // it can be used by SNIa
        #ifdef DISCRETE_SN
        if (m_turnoff_next < snII_detailed.min_mass){ // last SN timestep
            star_unexploded_sn[ipart] = 0.0;
        }
        else{
            star_unexploded_sn[ipart] = sn_yields[detail_n_fields_sn_ii + 2];
        }
        #endif /* DISCRETE_SN */

        // Then scale the yields to be densities
        for (idx_field=0; idx_field < detail_n_fields_sn_ii; idx_field++) {
            sn_yields[idx_field] *= snII_detailed_code.inv_vol;            
        }        

        // then we can add them to the cell
        putback_stellar_mass_detailed(level, cell, ipart,
                                      sn_yields[detail_field_idx_sn_ii_total],
                                      sn_yields[detail_field_idx_sn_ii_metals],
                                      0, 0, // SNIa, AGB
                                      sn_yields[detail_field_idx_sn_ii_C],
                                      sn_yields[detail_field_idx_sn_ii_N],
                                      sn_yields[detail_field_idx_sn_ii_O],
                                      sn_yields[detail_field_idx_sn_ii_Mg],
                                      sn_yields[detail_field_idx_sn_ii_S],
                                      sn_yields[detail_field_idx_sn_ii_Ca],
                                      sn_yields[detail_field_idx_sn_ii_Fe]);
    }
}
#endif /* ifndef PY_TESTING */

void feedback_snII_core(double *yields, double unexploded_sn, 
                        double m_turnoff_now, double m_turnoff_next, 
                        double z, double m_star)
{
    #ifndef PY_TESTING
    cart_assert(m_turnoff_next <= m_turnoff_now);
    // equal sign comes from very early times when both are the max of the 
    // lifetime function
    #endif /* not PY_TESTING */
    
    double threshold, sn_mass, integral, needed_sn;
    int idx_field;
    // clean out yields before I use them. 
    // The +3 is used for energy, N_SN, and leftover SN, in that order
    for (idx_field = 0; idx_field < detail_n_fields_sn_ii+3; idx_field++) {
        yields[idx_field] = 0;
    }
    // make array that will be filled later. N_SN and energy aren't needed
    // in the temp yields, which will only be used for the elements
    double temp_yields[detail_n_fields_sn_ii]; 

    // Normally we launch a supernova if the integrated IMF gives one massive
    // star that has left the main sequence. This only is changed if we're in
    // the last timestep SN are active.
    threshold = 1.0;

    // The supernova model used will be the mean mass between the stars dying
    // at the beginning and end of this timestep
    sn_mass = 0.5 * (m_turnoff_next + m_turnoff_now);
    if (sn_mass < snII_detailed.min_mass){
        sn_mass = snII_detailed.min_mass;
    }
    else if (sn_mass > snII_detailed.max_mass){
        sn_mass = snII_detailed.max_mass;
    }

    // But we need to see if there are any SN. We integrate the IMF, taking 
    // the mass of stars that go SN into account. We'll weight by the stellar
    // mass of the particle later
    if (m_turnoff_next > snII_detailed.max_mass){ // to early to have SN
        integral = 0.0;
    }
    else if (m_turnoff_now < snII_detailed.min_mass){  // to late to have SN
        integral = 0.0;
    }
    else if (m_turnoff_now > snII_detailed.max_mass){
        // SN turn on in this timestep (since m_turnoff_next < snII_detailed.max_mass)
        integral = imf_integral(m_turnoff_next, snII_detailed.max_mass);
    }
    else if (m_turnoff_next < snII_detailed.min_mass){
        // SN turn off in this timestep (since m_turnoff_now > 8.0)
        integral = imf_integral(snII_detailed.min_mass, m_turnoff_now);
        // in the last timestep we want to explode a SN if more than half of
        // one is left, so we set 0.5 as the threshold for SN. We do this so 
        // that on average zero supernovae are left unexploded, so we don't 
        // systematically leave out SN.
        threshold = 0.5; 
    }
    else{ // SN active this whole timestep
        integral = imf_integral(m_turnoff_next, m_turnoff_now);
    }

    // weight this integral by the stellar mass, then add the leftover SN
    // from the previous timestep
    needed_sn = integral * m_star + unexploded_sn;
    
    #ifdef DISCRETE_SN
    unsigned int n_explosions = 0; // default value
    unsigned int n_sn, n_hn;
    // Figure out the number of explosions we have. To have any explosions at 
    // all, we must be above the threshold 
    if (needed_sn > threshold){
        // validate that we're in the right mass range
        #ifndef PY_TESTING
        cart_assert(sn_mass >= snII_detailed.min_mass);
        cart_assert(sn_mass <= snII_detailed.max_mass);
        #endif /* not PY_TESTING */
        // Then we determine exactly how many we have. The line of code here 
        // is a bit counter-intuitive. This method works with the flexible
        // threshold. For example, take needed_sn to be 1.6. Normally, when
        // threshold is 1.0, this produces 1 SN, with 0.6 left over, as 
        // expected. But in that last timestep, when we want to essentially 
        // round the number of supernovae to the nearest integer, we use the
        // threshold of 0.5 combined with this method to accomplish that. 
        // In our example a needed_sn of 1.6 will produce 2 SN here in that 
        // last timestep, as expected. 
        n_explosions = (unsigned int)ceil(needed_sn - threshold);
        yields[detail_n_fields_sn_ii+1] = n_explosions; 
        
        // Figure out how many SN and HN there are. This depends on our mass
        if (sn_mass < snII_detailed.hn_threshold){
            n_hn = 0;
        }
        else{ // we are in the mass range for HN
            n_hn = my_rand_binomial(hn_fraction(z), n_explosions);
        }
        n_sn = n_explosions - n_hn;

        // Energy: SN always have 1E51, while for HN We have to figure out 
        // what it's energy is.  We use the Kobayashi+2006 yield tables 
        // directly here to figure out what the energy should be. 
        yields[detail_n_fields_sn_ii]  = n_sn * snII_detailed_code.E51;
        yields[detail_n_fields_sn_ii] += n_hn * hn_energy(sn_mass);

        // Then we get the elemental yields. This is the yield from one SN...
        get_yields_sn_ii(z, sn_mass, temp_yields);
        // ... so we need to multiply by the number of supernovae.
        for (idx_field = 0; idx_field < detail_n_fields_sn_ii; idx_field++) {
            yields[idx_field] += n_sn * temp_yields[idx_field];
            temp_yields[idx_field] = 0;
        }
        // Do the same for hypernovae
        if (n_hn > 0){
            #ifndef PY_TESTING
            cart_assert(sn_mass >= snII_detailed.hn_threshold);
            cart_assert(sn_mass <= snII_detailed.max_mass);
            #endif /* not PY_TESTING */

            get_yields_hn_ii(z, sn_mass, temp_yields);
            for (idx_field = 0; idx_field < detail_n_fields_sn_ii; idx_field++) {
                yields[idx_field] += n_hn * temp_yields[idx_field];
                temp_yields[idx_field] = 0;
            }
        }
    }
    // then when we're done set the leftover SN
    yields[detail_n_fields_sn_ii+2] = needed_sn - n_explosions;
    #else /* DISCRETE_SN */
    // only do things if we have SN. The yields are initalized at zero, so they
    // will be correct even if we don't enter this if statement. 
    if (needed_sn > 0){
        // store the new SN
        yields[detail_n_fields_sn_ii+1] = needed_sn;
        // then figure out what the yields are. 
        // If we are below the hypernova threshold we only have SN
        if (sn_mass < snII_detailed.hn_threshold){
            // energy is always 1E51 ergs
            yields[detail_n_fields_sn_ii] = needed_sn * snII_detailed_code.E51;
            // Then get the elemental yields
            get_yields_sn_ii(z, sn_mass, temp_yields);
            // and scale them by the fractional number of SN as we store them.
            for (idx_field = 0; idx_field < detail_n_fields_sn_ii; idx_field++){
                yields[idx_field] = needed_sn * temp_yields[idx_field];
            }
            #ifndef PY_TESTING
            cart_assert(sn_mass >= snII_detailed.min_mass);
            cart_assert(sn_mass <= snII_detailed.hn_threshold);
            #endif /* not PY_TESTING */
        }
        else{ // we have hypernovae. 
            double n_hn = needed_sn * hn_fraction(z);
            double n_sn = needed_sn - n_hn; // equivalent to total*(1-hn_frac)
            // SN energy is always 1E51 ergs
            yields[detail_n_fields_sn_ii] = n_sn * snII_detailed_code.E51;
            // then add HN energy
            yields[detail_n_fields_sn_ii] += n_hn * hn_energy(sn_mass);

            // Then get the elemental yields for SN
            get_yields_sn_ii(z, sn_mass, temp_yields);
            // and scale them by the fractional number of SN as we store them.
            for (idx_field = 0; idx_field < detail_n_fields_sn_ii; idx_field++){
                yields[idx_field] = n_sn * temp_yields[idx_field];
            }

            // And do the same thing for HN
            get_yields_hn_ii(z, sn_mass, temp_yields);
            // and scale them by the fractional number of HN as we store them.
            for (idx_field = 0; idx_field < detail_n_fields_sn_ii; idx_field++){
                yields[idx_field] += n_hn * temp_yields[idx_field];
            }
            // double check that we should have had hypernovae
            #ifndef PY_TESTING
            cart_assert(sn_mass >= snII_detailed.hn_threshold);
            cart_assert(sn_mass <= snII_detailed.max_mass);
            #endif /* not PY_TESTING */
        }
    }
    #endif /* DISCRETE_SN */
}

double hn_energy(double sn_mass){
    if (sn_mass < 25){
        // masses less than 25 have 10E51 ergs
        return 10 * snII_detailed_code.E51;
    }
    else if (sn_mass < 30){ // between 25 and 30
        // interpolate between the energy of the 25 and 30 Msun models
        // which is 10 and 20 E51 ergs
        return interpolate(sn_mass, 25, 30, 10, 20) * snII_detailed_code.E51;
    }
    else if (sn_mass < 40){ // between 30 and 40
        // interpolate between the energy of the 30 and 40 Msun models
        // which is 20 and 30 E51 ergs
        return interpolate(sn_mass, 30, 40, 20, 30) * snII_detailed_code.E51;
    }
    else{  // sn_mass > 40
        // use the energy of the 40 Msun model
        return 30 * snII_detailed_code.E51;
    }
}

double hn_fraction(double metallicity){
    // Grimmet et al 2020: https://arxiv.org/pdf/1911.05901.pdf
    // Equation 1
    double min_frac = 0.001;
    double frac = snII_detailed.hn_fraction_0 * exp(metallicity/-0.001);
    if (frac < min_frac){
        return min_frac;
    }
    return frac;
}

/* 
 * ============================================================================
 * 
 * Feedback
 * 
 * ============================================================================
 * This function handles the SNII feedback. Most of this function is copied
 * from the snII_remnant_feedback function in the 
 * `src/sf/models/feedback.snII.c` file. I decided to move it here and
 * make it separate since all the parameters that can be modified in that file
 * are fixed in my scheme, and I didn't want any confusion about that. Since
 * I set the stellar population parameters when writing the tables, there are
 * no choices for the user.
 * 
 * Some portions of the old code were irrelevant here because of assumptions 
 * made by my code. I also take extra parameters, which are given to this
 * function by the main SN feedback function.
 */
#ifndef PY_TESTING
void snr_feedback_detailed(int level, int cell, int ipart, double t_next,
                           double M_0, double E_0, double N_SN) {
	double dp, dU, dK, P_0;
    double dt = t_next - particle_t[ipart];
	double params[9];

	const double r = cell_size[level];
	const double v_rms = cell_gas_sound_speed(cell); // cs^2 = sum(gamma*P)/rho ~ sigma^2

	// quantities in physical units:
	const double n_0 = cell_gas_density(cell) * units->number_density;
	const double T_0 = cell_gas_temperature(cell) * units->temperature;
    const double Z_0 = cell_gas_metal_density(cell)/(constants->Zsun*cell_gas_density(cell));

#ifdef STAR_PARTICLE_TYPES
	cart_assert(star_particle_type[ipart] == STAR_TYPE_NORMAL || star_particle_type[ipart] == STAR_TYPE_FAST_GROWTH );
#endif

    // Check that we aren't at any temperature ceilings before we actually go 
    // adding more energy
	if (T_0 < feedback_temperature_ceiling
#ifdef SGS_TURBULENCE
			&& cell_gas_turbulent_temperature(cell)*units->temperature < feedback_turbulence_temperature_ceiling
#endif /* SGS_TURBULENCE */
	) {
        // E_0 is given in code energy
        // M_0 is given in code masses
        P_0 = sqrt(2 * E_0 * M_0 ); // 2*0.5m^2v^2
        // don't need to account for remnant fraction like the old one does
        // because that's already baked in to the calculation of M_0 in the
        // tables. 

        params[0] = r;
        params[1] = E_0;
        params[2] = P_0;
        params[3] = n_0;
        params[4] = Z_0;
        params[5] = N_SN;
        (*snr_model)(params,&dp,&dU);  //this gets dp and dU from the SNR model

        dU += snr_momentum_to_heat * dp * v_rms;

#ifdef SGS_TURBULENCE
        dK = snr_momentum_to_turbulence * dp * v_rms;
        dK = MIN(snr_boost_turbulence*dK*cell_volume_inverse[level],dKfact*cell_gas_density(cell));
        cart_assert( !isnan(dK) && dK >= 0.0 && dK != INFINITY );
        // Uses of INFINITY trigger floating point overflows, but that's fine
        cell_gas_energy(cell) += dK;
        cell_gas_turbulent_energy(cell) += dK;
#endif /* SGS_TURBULENCE */

        /* limit energy release and don't allow to explode in hot bubble */
        dU = MIN(snr_boost_heat*dU*cell_volume_inverse[level],dUfact*cell_gas_density(cell));
        cart_assert( !isnan(dU) && dU >= 0.0 && dU != INFINITY );
        cell_gas_energy(cell) += dU;
        cell_gas_internal_energy(cell) += dU;
        cell_gas_pressure(cell) += dU*(cell_gas_gamma(cell)-1);

#ifdef SGS_TURBULENCE
        dp *= snr_boost_momentum*( 1. - snr_momentum_to_turbulence - snr_momentum_to_heat );
#else /* SGS_TURBULENCE */
        dp *= snr_boost_momentum*( 1. - snr_momentum_to_heat );
#endif /* SGS_TURBULENCE */

        cart_assert( !isnan(dp) && dp >= 0.0 && dp != INFINITY );
        distribute_momentum(dp, level, cell, dt);

        params[6] = dp;
        params[7] = dU*cell_volume[level];
        params[8] = dK*cell_volume[level];
        PLUGIN_POINT(GetLocalData,(level, cell, ipart, params, GetLocalDataId_SNIIRemnantFeedback ));
    }
}
#endif /* PY_TESTING */

int my_rand_binomial(double probability, unsigned int n_trials){
    // I have a version that works in ART and one that works with my python tests
    #ifdef PY_TESTING
    return gsl_ran_binomial(r, probability, n_trials);
    #else
    return cart_rand_binomial(probability, n_trials);
    #endif
}

#ifdef PY_TESTING
double *get_ejecta_sn_ii_py(double unexploded_sn, double m_turnoff_now, 
                            double m_turnoff_next, double m_star, double z){
    static double return_yields[20];
    // length doesn't matter, since python can't tell where it ends anyway
    feedback_snII_core(return_yields, unexploded_sn, m_turnoff_now, 
                       m_turnoff_next, z, m_star);
    return return_yields;
} 

double hn_energy_py(double mass){
    return hn_energy(mass);
}

double get_hn_fraction_py(double metallicity){
    return hn_fraction(metallicity);
}

#endif /* PY_TESTING */

#endif /* STAR_FORMATION */
