#include "config.h"
#if defined(HYDRO) && defined(STAR_FORMATION)

#include <math.h>
#include <string.h>

#include "auxiliary.h"
#include "control_parameter.h"
#include "cosmology.h"
#include "hydro.h"
#include "imf.h"
#include "rand.h"
#include "rt.h"
#include "starformation.h"
#include "starformation_recipe.h"
#include "times.h"
#include "tree.h"
#include "units.h"

#include "algorithm.mcf.h"
#include "algorithm.stargrowth.h"
#include "feedback.ml.h"
#include "feedback.rad.h"
#include "feedback.snII.h"
#include "feedback.snIa.h"


mcf_t mcf_form = { 0.1, 0.01, 1.0e4, 0.05, 0, 
#ifndef RADIATIVE_TRANSFER
		   1,
#endif
		   0
};
const mcf_t *mcf = &mcf_form;


extern double feedback_temperature_ceiling;


struct mcf_grow_type
{
    double delta; /*Power of time dependency */
    double tau_ff; /* years */
    double eps_ff; /*epsilon_ff at tau_ff*/
    double max_age; /* years */
    double alpha;   /* Lee2016 alpha in inverse yr */
}
  mcf_grow = { 2.0, 1.0e7, 0.03, 5.0e7, 0 };


struct mcf_kill_type
{
    double Tfin; /* Final temperature */
    double tDelay;  /* Myr */
    double tEmbed;  /* Myr */
    double epsEnd; /* the kill line */
    double epsVar; /* scatter around the kill line */
    double epsMax; /* ceiling for scatter */
    double fMetal;
}
  mcf_kill = { 1.0e6, 0, 0, 0.05, 0.7, 0.5, 0 };


void mcf_config_init()
{
    double total_mass = integrate( imf->fm, imf->min_mass, imf->max_mass, 1e-6, 1e-9 );
    cart_assert(total_mass > 0.0);
    mcf_kill.fMetal = integrate( imf->fmz, 8, 40, 1e-6, 1e-9 )/total_mass;

    snII_config_init();
    snIa_config_init();
    ml_snl2012_config_init();

    control_parameter_add(control_parameter_double,&mcf_form.fH2sf,"mcf_form.fH2sf","TBD");
    control_parameter_add(control_parameter_double,&mcf_form.fH2sfSlope,"mcf_form.fH2sfSlope","TBD");
    control_parameter_add(control_parameter_double,&mcf_form.fH2min,"mcf_form.fH2min","TBD");
    control_parameter_add(control_parameter_double,&mcf_form.TH2max,"mcf_form.TH2max","TBD");
    control_parameter_add(control_parameter_double,&mcf_form.Dbreak,"mcf_form.Dbreak","TBD");
#ifndef RADIATIVE_TRANSFER
    control_parameter_add(control_parameter_double,&mcf_form.Umw,"mcf_form.Umw","TBD");
#endif
    
    control_parameter_add(control_parameter_double,&mcf_grow.delta,"mcf_grow.delta","TBD");
    control_parameter_add(control_parameter_double,&mcf_grow.tau_ff,"mcf_grow.tau_ff","TBD");
    control_parameter_add(control_parameter_double,&mcf_grow.eps_ff,"mcf_grow.eps_ff","TBD");
    control_parameter_add(control_parameter_double,&mcf_grow.max_age,"mcf_grow.max_age","TBD");
    control_parameter_add(control_parameter_double,&mcf_grow.alpha,"mcf_grow.alpha","TBD");

    control_parameter_add(control_parameter_double,&mcf_kill.Tfin,"mcf_kill.Tfin","TBD");
    control_parameter_add(control_parameter_double,&mcf_kill.tDelay,"mcf_kill.tDelay","TBD");
    control_parameter_add(control_parameter_double,&mcf_kill.tEmbed,"mcf_kill.tEmbed","TBD");
    control_parameter_add(control_parameter_double,&mcf_kill.epsEnd,"mcf_kill.epsEnd","TBD");
    control_parameter_add(control_parameter_double,&mcf_kill.epsVar,"mcf_kill.epsVar","TBD");
}


void mcf_config_verify()
{
    snII_config_verify();
    snIa_config_verify();
    ml_snl2012_config_verify();

    VERIFY(mcf_form.fH2sf,mcf_form.fH2sf > 0);
    VERIFY(mcf_form.fH2sfSlope,1);
    VERIFY(mcf_form.fH2min,mcf_form.fH2min > 0);
    VERIFY(mcf_form.TH2max,mcf_form.TH2max > 0);
    VERIFY(mcf_form.Dbreak,mcf_form.Dbreak >= 0);
#ifndef RADIATIVE_TRANSFER
    VERIFY(mcf_form.Umw,mcf_form.Umw > 0);
#endif
    
    VERIFY(mcf_grow.delta,mcf_grow.delta >= 0);
    VERIFY(mcf_grow.tau_ff,mcf_grow.tau_ff > 0);
    VERIFY(mcf_grow.eps_ff,mcf_grow.eps_ff > 0);
    VERIFY(mcf_grow.max_age,mcf_grow.max_age > 0);
    VERIFY(mcf_grow.alpha,mcf_grow.alpha >= 0);

    VERIFY(mcf_kill.Tfin,mcf_kill.Tfin > 0);
    VERIFY(mcf_kill.tDelay,mcf_kill.tDelay >= 0);
    VERIFY(mcf_kill.tEmbed,mcf_kill.tEmbed >= 0);
    VERIFY(mcf_kill.epsEnd,mcf_kill.epsEnd>0 && mcf_kill.epsEnd<1);
    VERIFY(mcf_kill.epsVar,mcf_kill.epsVar >= 0);
}


void mcf_init()
{
    snII_init();
    snIa_init();
    ml_init();
}


void mcf_setup(int level)
{
    snII_setup(level);
    snIa_setup(level);
    rad_setup(level);
    ml_setup(level); 
#ifdef COSMOLOGY
    mcf_form.DBfac = mcf_form.Dbreak/pow(10*auni[level],3);
#endif
}


double mcf_fH2(int cell, float den)
{
#if defined(RADIATIVE_TRANSFER) && defined(RT_CHEMISTRY)
    return 2*cell_H2_density(cell)/(0.76*cell_gas_density(cell));
#else 
    if(units->temperature*cell_gas_temperature(cell) > mcf_form.TH2max)
    {
        return 0;
    }
    else
    {
#ifdef RADIATIVE_TRANSFER
        double U = rtUmwFS(cell);
        double D = rtDmwFL(cell);
#else
        double U = mcf_form.Umw;
#ifdef ENRICHMENT
        double D = cell_gas_metal_density(cell)/(constants->Zsun*cell_gas_density(cell));
#else /* ENRICHMENT */
        double D = fixed_metallicity;
#endif /* ENRICHMENT */
#endif /* RADIATIVE_TRANSFER */
        double D0 = 0.17;
        double g = sqrt(D*D+D0*D0);
        double s = pow(0.001+0.1*U,0.7);
        double alpha = 1 + 0.7*sqrt(s)/(1+s);
        double SigmaR1 = 40/g*s/(1+s);
        double SigmaGas = 2*units->density*units->length/constants->Msun*constants->pc*constants->pc*den*cell_sobolev_length(cell); /* 2 because Lsob is one-sided, Sigma = 2 rho Lsob */
        double q = pow(SigmaGas/SigmaR1,alpha);
        double R = q*(1+0.25*q)/1.25;

		if(mcf_form.DBfac > 0)
		  {
			/*
			//  Reduce the fraction in low D environments
			*/
			double q = sqrt(D/mcf_form.DBfac);
			R *= q/(1+q);
		  }
	
        return R/(1+R);
    }
#endif /* RADIATIVE_TRANSFER && RT_CHEMISTRY */
}


double mcf_fH2_old(int cell)
{
#ifdef RADIATIVE_TRANSFER
  double U = rtUmwFS(cell);
  double D = rtDmwFL(cell);
#else
  double U = mcf->Umw;
#ifdef ENRICHMENT
  double D = cell_gas_metal_density(cell)/(constants->Zsun*cell_gas_density(cell));
#else /* ENRICHMENT */
  double D = fixed_metallicity;
#endif /* ENRICHMENT */
#endif /* RADIATIVE_TRANSFER */
  double D0 = 0.17;
  double g = sqrt(D*D+D0*D0);
  double q = sqrt(0.01+U);
  double SigmaR1 = 50/g*q/(1+0.69*q);
  double alp = 0.5 + 1/(1+sqrt(U*D*D/600));
  double rhoH = 0.76*cell_gas_density(cell);
  double SigmaH = 2*units->density*units->length/constants->Msun*constants->pc*constants->pc*rhoH*cell_sobolev_length(cell); /* 2 because Lsob is one-sided, Sigma = 2 rho Lsob */
  double R = pow(SigmaH/SigmaR1,alp);

  return R/(1+R);
}


int mcf_find_local_cloud(int cell)
{
    int ipart, ipart_store;
    double min_age, age, smg; 
    /*Finds the youngest particle in the cell to accrete mass onto*/

    ipart = cell_particle_list[cell]; 
    min_age = 1e30;
    ipart_store = -1;
    while(ipart != NULL_PARTICLE)
    {
        if(particle_is_star(ipart)
#ifdef STAR_PARTICLE_TYPES
           && (star_particle_type[ipart] == STAR_TYPE_NORMAL)
#endif
           )
        { 
            /* if a cluster exists */
            /* if theres more than one seed then assign to star with lowest age; */
            smg = particle_mass[ipart] - star_initial_mass[ipart]; /* MC mass; MCs should be positive, normal stars are negative. */
            if(smg > 0.0)
            {
                age = tphys_from_tcode(particle_t[ipart]) - tphys_from_tcode(star_tbirth[ipart]);
                if(age < min_age)
                { 
                    min_age = age;
                    ipart_store = ipart;
                }
            }
        }
        ipart = particle_list_next[ipart];
    }

    if(min_age < mcf_grow.max_age) return ipart_store; else return -1;
}


float mcf_erfi(float x)
{
   float tt1, tt2, lnx, sgn;
   sgn = (x < 0) ? -1.0f : 1.0f;

   x = (1 - x)*(1 + x);        // x = 1 - x*x;
   lnx = logf(x);

   tt1 = 2/(M_PI*0.147) + 0.5f * lnx;
   tt2 = 1/(0.147) * lnx;

   return(sgn*sqrtf(-tt1 + sqrtf(tt1*tt1 - tt2)));
}


void mcf_feedback_mc(int level, int cell, int ipart, double t_next)
{
    double tage, dtage, Mgas1, Mgas2, q;
    double tn = tphys_from_tcode(t_next);
    double tb = tphys_from_tcode(star_tbirth[ipart]);
    double tp = tphys_from_tcode(particle_t[ipart]);

    tage = tp - tb - mcf_kill.tDelay*1.0e6;
    if(tage < 0) return;
	dtage = tn - tp;
    if(dtage<=0 || tage<0)
    {
        cart_error("ipart=%d sim=%lg pm=%lg dt=%lg tage=%lg %lg %lg)",ipart,star_initial_mass[ipart],particle_mass[ipart],dtage,tage,tp,tb);
    }

    q = mcf_grow.eps_ff/(1+mcf_grow.delta)*(pow((tage+dtage)/mcf_grow.tau_ff,1+mcf_grow.delta)-pow(tage/mcf_grow.tau_ff,1+mcf_grow.delta));
    if(q > 0.01) q = 1 - exp(-q); else q -= 0.5*q*q;

    Mgas1 = particle_mass[ipart] - star_initial_mass[ipart];
	
#ifdef DEBUG
    static int ipart_deb = -1;
    if(ipart_deb < 0)
    {
        ipart_deb = ipart;
    }
    if(ipart == ipart_deb)
    {
        cart_debug("DEB i=%d tp=%10.4lg tn=%10.4lg q=%9.3le M1=%9.3le M2=%9.3le",ipart,tp,tn,q,star_initial_mass[ipart]*units->mass/constants->Msun,(star_initial_mass[ipart]+Mgas1*q)*units->mass/constants->Msun);
    }
#endif
	
    star_initial_mass[ipart] += Mgas1*q;
    if(star_initial_mass[ipart] > particle_mass[ipart])
    {
        /* Don't allow stellar mass to exceed total mass*/
        star_initial_mass[ipart] = 0.999*particle_mass[ipart];
    }    

    float eps = star_initial_mass[ipart]/particle_mass[ipart];
    if(eps<0 || eps>1)
    {
        cart_error("ipart=%d sim=%lg pm=%lg eps=%lg",ipart,star_initial_mass[ipart],particle_mass[ipart],eps);
    }

    /*
    //  Lewis, Goodman, Miller random number generator
    */
    static const particleid_t a = 16807;
    static const particleid_t m = 2147483647;
    float rand_u = (float)((a*ipart) % m)/(m-1);
	
    /*
    //  Cut-off
    */
    float rand_n = 1.4142135623730951*mcf_erfi(2*rand_u-1);
    float eps_kill = mcf_kill.epsEnd*pow(10.0,-mcf_kill.epsVar*rand_n);
    if(eps_kill > mcf_kill.epsMax) eps_kill = mcf_kill.epsMax;
	
    int explode = (eps > eps_kill);

    Mgas2 = particle_mass[ipart] - star_initial_mass[ipart];
    if(mcf_grow.alpha > 0)
    {
        double Mout = star_initial_mass[ipart]*(1.0-exp(-mcf_grow.alpha*dtage));
		if(Mout > Mgas2)
		  {
			explode = 1;
		  }
		else
		  {
			cell_gas_density(cell) += Mout*cell_volume_inverse[level];
			particle_mass[ipart] -= Mout;
		  }
	}
	
    if(explode)
	{
        /*
        //  Explode
        */
        cell_gas_density(cell) += Mgas2*cell_volume_inverse[level];
        particle_mass[ipart] = 0.999*star_initial_mass[ipart];
        star_tbirth[ipart] = tcode_from_tphys(tphys_from_tcode(t_next)-mcf_kill.tEmbed*1.0e6);
  
#ifdef ENRICHMENT
        cell_gas_metal_density_II(cell) += mcf_kill.fMetal*star_initial_mass[ipart]*cell_volume_inverse[level];
#endif /* ENRICHMENT */

        /* limit energy release and don't allow to explode in hot bubble */
        if(units->temperature*cell_gas_temperature(cell) < feedback_temperature_ceiling)
        {
            double dU = mcf_kill.Tfin/units->temperature*cell_gas_density(cell);
            //if(dU > cell_gas_internal_energy(cell)) dU -= cell_gas_internal_energy(cell); else dU = 0;

#ifdef SGST_SN_SOURCE
            cart_error("NOT IMPLEMENTED.");
#endif /* SGST_SN_SOURCE */

            cell_gas_energy(cell) += dU;
            cell_gas_internal_energy(cell) += dU;
            cell_gas_pressure(cell) += dU*(cell_gas_gamma(cell)-1);
        }
    }
}

										  
void mcf_feedback(int level, int cell, int ipart, double t_next)
{
    cart_assert(particle_is_star(ipart));
#ifdef STAR_PARTICLE_TYPES
    cart_assert(star_particle_type[ipart] == STAR_TYPE_NORMAL);
#endif

    if(star_initial_mass[ipart] >= particle_mass[ipart])
    {
        /*
        // Normal star
        */
        snII_thermal_feedback(level,cell,ipart,t_next);
        snIa_thermal_feedback(level,cell,ipart,t_next);
        ml_feedback(level,cell,ipart,t_next); 
        return;
    }
	else
	{
        mcf_feedback_mc(level,cell,ipart,t_next);
    }	  
}

#endif /* HYDRO && STAR_FORMATION */

