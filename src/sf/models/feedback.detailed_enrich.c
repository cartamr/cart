/** 
 * feedback.detailed_enrich.c
 * 
 * This file is the core functionality for the enrichment prescription that 
 * adds AGB enrichment, and individual elements (C, N, O, Fe) to the overall
 * enrichment prescription. This functionality is enabled with the 
 * ENRICHMENT_ELEMENTS flag. In addition, some extra debugging output for this
 * is available with the ENRICHMENT_ELEMENTS_DEBUG flag. This prints all the 
 * information needed to verify the code is working manually.
 * 
 * Using this code
 * ---------------
 * TODO: fill this in once the actual use is written.
 * 
 */ 
#ifndef PY_TESTING
#include "config.h"
#endif /* PY_TESTING */

#ifdef STAR_FORMATION

#ifndef PY_TESTING
#include "auxiliary.h"          // local_proc_id
#include "cosmology.h"          // tphys_from_tcode
#include "parallel.h"           // MASTER_NODE
#include "starformation.h"      // star particle arrays
#include "tree.h"               // gas density functions
#include "units.h"              // units
#include "times.h"              // abox (used in debugging)
#include "imf.h"                // IMF, stellar lifetimes
#include "feedback.ml.h"        // putback stellar mass for winds

#endif /* PY_TESTING */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>     
#include <libgen.h>  

#include "feedback.detailed_enrich.h"


/* Define some constants used to help read the files. The values here come
 * from the tables. If they are ever remade, these values need to be updated
 * to ensure the tables are read correctly. The SNII, SNIa, and AGB tables
 * have the yields from individual stellar models, while the winds table has
 * the cumulative yield at many ages.
 * 
 * What these variables mean:
 * n_ages - How many age points are in the table. (Used for winds only.)
 * n_mass - How many mass models are present at each metallicity.
 * n_metallicity - How many metallicity points are in the table.
 * n_fields - How many columns with ejecta information the table has. This is 
 *            the number of columns in the file, ignoring age and metallicity 
 *            columns. Examples of this are C, N, total metals. 
 * 
 * Note that the SN Ia model doesn't contain any info about rates (that is 
 * taken care of in the SN Ia prescription), so the tables just holds the 
 * ejected mass for a single SN Ia. Therefore I don't need any age information 
 * for SN Ia.
 */
#define n_mass_models_agb       8
#define n_metallicity_agb       5
#define n_fields_agb            6  // C, N, O, Mg, metals, total ejecta 

#define n_mass_models_sn_ii     7
#define n_mass_models_hn_ii     4
#define n_metallicity_sn_ii     4
#define n_fields_sn_ii          9 // C, N, O, Mg, S, Ca, Fe, metals, total 

#define n_mass_points_winds     1000
#define n_metallicity_winds     4
#define n_fields_winds          1  // total ejecta

#define n_mass_models_sn_ia     1
#define n_metallicity_sn_ia     2
#define n_fields_sn_ia          8  // C, N, O, Mg, S, Ca, Fe, metals

// go manually look at the above to get the maximum values, which are 
// used when defining the arrays that hold the ejecta. 
#define max_n_mass_models       8
#define max_n_metallicities     5
#define max_n_fields            11


// Initialize the indices for each of the fields used above. These are not 
// #defined so that I can include them externally.
int detail_field_idx_agb_C        = 0;
int detail_field_idx_agb_N        = 1;
int detail_field_idx_agb_O        = 2;
int detail_field_idx_agb_Mg       = 3;
int detail_field_idx_agb_metals   = 4;
int detail_field_idx_agb_total    = 5;

int detail_field_idx_sn_ii_C           = 0;
int detail_field_idx_sn_ii_N           = 1;
int detail_field_idx_sn_ii_O           = 2;
int detail_field_idx_sn_ii_Mg          = 3;
int detail_field_idx_sn_ii_S           = 4;
int detail_field_idx_sn_ii_Ca          = 5;
int detail_field_idx_sn_ii_Fe          = 6;
int detail_field_idx_sn_ii_metals      = 7;
int detail_field_idx_sn_ii_total       = 8;

int detail_field_idx_winds_total  = 0;

int detail_field_idx_sn_ia_C      = 0;
int detail_field_idx_sn_ia_N      = 1;
int detail_field_idx_sn_ia_O      = 2;
int detail_field_idx_sn_ia_Mg     = 3;
int detail_field_idx_sn_ia_S      = 4;
int detail_field_idx_sn_ia_Ca     = 5;
int detail_field_idx_sn_ia_Fe     = 6;
int detail_field_idx_sn_ia_metals = 7;

// copy some of my above defines to be used externally. The defines are still
// needed to declare array lengths here.
int detail_n_fields_agb = n_fields_agb;
int detail_n_fields_sn_ii = n_fields_sn_ii;
int detail_n_fields_sn_ia = n_fields_sn_ia;
int detail_n_fields_winds = n_fields_winds;

/* Create a structure to hold the enrichment information for stellar winds. 
 * Unlike other sources, they need to be tabulated as a function of time. 
 * The other sources will use another structure. 
 * 
 * The variables contained here are described below, and will be filled in as
 * the tables are read in. It does not need to be initialized by hand. The only
 * things that must be set are the #defines above about each source.
 * 
 * The wind table holds the cumulative ejecta as a function of time, which I 
 * have turned into mass of the star leaving the main sequence at that time.
 * This is useful because it's independent of metallicity, unlike time.
 * 
 * Here are what the following variables represent:
 * n_mass_points - The number of turnoff mass points that were tabulated.
 * n_metallicity - As above, the number of metallicity points.
 * n_fields - As above, the number of columns with data about the enrichment.
 * masses - Array of turnoff masses at which the cumulative ejecta was 
 *          calculated.
 * mass_loss - Multidimensional array containing the cumulative fractional wind 
 *             ejecta. The first index into this array is the index 
 *             corresponding to the metallicity at which the ejecta is desired, 
 *             and the last corresponds to turnoff mass (i.e. time).
 * metallicities - Array containing the metallicities at which the ejecta were 
 *                 calculated.
 * min_mass - Minimum mass at which the ejecta was tabulated.
 * max_mass - Maximum mass at which the rates are tabulated
 * log_min_mass - log_10(min_mass)
 * log_max_mass - log_10(max_mass)
 * mass_spacing_log - The input table must have turnoff masses that are evenly
 *                    spaced in log space. This holds the spacing in log 
 *                    between two adjacent masses:
 *                    mass_spacing_log = log_10(masses[idx+1]) - 
 *                                       log_10(masses[idx])
 *                    where idx is arbitrary, since they are all the same.
 */
struct WindSource {
    int n_mass_points;  
    int n_metallicity;
    int n_fields;
    double masses[n_mass_points_winds];
    double mass_loss[n_metallicity_winds][n_mass_points_winds];
    double metallicities[n_metallicity_winds];
    double min_mass;
    double max_mass;
    double log_min_mass;
    double log_max_mass;
    double mass_spacing_log;
};


/* The structure to hold the AGB, SNII, and SNIa data is similar, but does not 
 * need to hold any of the time-resolved information, so it's simpler. This
 * holds the yields of individual stellar models, hence the name.
 * 
 * Here are what the variables represent:
 * n_mass_models - The number of masses at which models were calculated.
 * n_metallicity - The number of metallicity points.
 * n_fields - The number of of columns with data about the enrichment.
 * masses - The list of masses at which the models were made. In solar masses.
 * ejecta - A multidimensional array that holds the yield data. Ths first index
 *          into this array is to pick the field (i.e. which element to get
 *          the yield for), the second of for the metallicity, and the last
 *          is to determine which stellar mass model to return. The 'phys'
 *          version is in solar masses, the 'code' versions is in code masses.
 * metallicities - Array containing the metallicities at which the ejecta were 
 *                 calculated.
 */
struct IndividualSource {
    int n_mass_models;
    int n_metallicity;
    int n_fields;
    double masses[max_n_mass_models];
    double ejecta_phys[max_n_fields][max_n_metallicities][max_n_mass_models];
    double ejecta_code[max_n_fields][max_n_metallicities][max_n_mass_models];
    double metallicities[max_n_metallicities];
};


// Then initialize the structures. The data will be filled in as the files
// are read in later.
struct WindSource winds;
struct IndividualSource sn_ia, sn_ii, hn_ii, agb;

// Have another structure to hold the normalization for the IMF integration
// We want this IMF to be normalized to one unit mass, so that we can
// multiply by the mass of the particle to get the appropriate value. We want
// to be able to do this for both code and physical units. Note
// that when we normalize to one solar mass, that is not the same as 
// normalizing to one star. This variable corrects for that. This default value
// is the normalization for a Kroupa IMF with limits of 0.08 and 50 M_sun.
// Later we will fix this normalization if other limits are set. The default
// value is helpful for my Python tests.
struct{
    double norm;
}
IMF_norm_phys = {1.5346394435433723};

struct{
    double norm;
}
IMF_norm_code = {1.5346394435433723};


// Declare function prototypes that aren't needed outside this file. 
// Functions that are needed elsewhere are in the .h file
void file_read_in_sn_ia(char*);
void file_read_in_ejecta(char*, struct IndividualSource*);
void file_read_in_winds(char*);
FILE *open_file(char*);
void handle_header(FILE*, char*);
void fscanf_var_length(FILE*, double*, int);
void handle_wind_masses(void);
void handle_units(struct IndividualSource*);
void get_yields_arbitrary(double, double, struct IndividualSource*, double*);
void find_z_bound_idxs(double, double*, int, int*);
void find_mass_bound_idxs(double, double*, int, int, int*);
void find_mass_bound_idxs_descending(double, double*, int, int, int*);
int guess_mass_idx_winds(double);
void raise_error(char*);


/* 
 * ============================================================================
 * 
 * Functions used externally
 * 
 * ============================================================================
 * These functions are the interface of this portion of the code, so I'll place 
 * them first in the file. They call other internal functions which are 
 * defined below. The first three are analagous to functions in the SNIa and 
 * SN II prescriptions that handle initialization and setup. These handle the
 * ejecta for the sources that vary with time (SNII, winds, and AGB). The
 * SN Ia is handled in its own file, so we have one simple function for
 * it. 
 */
void detailed_enrichment_config_init(){
    ;
}

#ifndef PY_TESTING
void detailed_enrichment_config_verify(){
    // Our function to analytically integrate the IMF depends on a Kroupa IMF. 
    // Make sure that has been chosen. 
    // Note that this isn't strictly required here, since it's possible to use
    // this file without using the function, but better safe than sorry.
    if (strcmp("Kroupa", imf->name) != 0){
        cart_debug("You are including the file src/sf/models/feedback.detailed_enrich.c");
        cart_debug("which has a function that analytically integrates the IMF.");
        cart_debug("That assumes the Kroupa IMF.");
        cart_debug("You chose %s.", imf->name);
        cart_error("Choose the Kroupa IMF.");
    }
}
#endif /* not PY_TESTING */

void detailed_enrichment_init(void){
    /** 
     * Populate the structures containing the ejecta information by reading
     * in the appropriate files.
     * 
     * This function must be called before any other detailed enrichment
     * things can be done, since the data initialized here is required.
     */
    
    // First add the metadata that was defined up top and the mass boundaries
    winds.n_mass_points = n_mass_points_winds;
    winds.n_metallicity = n_metallicity_winds;
    winds.n_fields = n_fields_winds;

    sn_ii.n_mass_models = n_mass_models_sn_ii;
    sn_ii.n_metallicity = n_metallicity_sn_ii;
    sn_ii.n_fields = n_fields_sn_ii;

    // hypernovae are the same as SN, just with less mass points
    hn_ii.n_mass_models = n_mass_models_hn_ii;
    hn_ii.n_metallicity = n_metallicity_sn_ii;  
    hn_ii.n_fields = n_fields_sn_ii;

    agb.n_mass_models = n_mass_models_agb;
    agb.n_metallicity = n_metallicity_agb;
    agb.n_fields = n_fields_agb;

    sn_ia.n_mass_models = n_mass_models_sn_ia;
    sn_ia.n_fields = n_fields_sn_ia;
    sn_ia.n_metallicity = n_metallicity_sn_ia;

    // Read in each file. These functions populate the structures. 
    file_read_in_ejecta("detail_enrich_sn_ii_ejecta.txt", &sn_ii);
    file_read_in_ejecta("detail_enrich_hn_ii_ejecta.txt", &hn_ii);
    file_read_in_ejecta("detail_enrich_agb_ejecta.txt",   &agb);
    file_read_in_winds("detail_enrich_wind_ejecta.txt");
    file_read_in_sn_ia("detail_enrich_sn_ia_ejecta.txt");

    // Then set the normalization for our IMF integral. We want to normalize it
    // so that it integrates to the number of stars in a one solar mass SSP. 
    // We want a value A such that
    // A * analytic_integral = number of stars in a 1 solar mass SSP
    // Note that here our analytic_integral already has a normalization 
    // factor in it, so we really have
    // A * old_norm * analytic_integral = number of stars in a 1 solar mass SSP
    // old_norm is set to 1.0 at the beginning of this file, but is kept for 
    // generality. In the end, we want a new normalization, such that
    // new_norm = A * old_norm
    // To get the number of stars in a 1 solar mass SSP, we integrate the IMF
    // to get the number of stars, then divide that by the total mass in the 
    // SSP, obtained by integrating the IMF times the stellar mass. The 
    // internal IMF calculations do this in terms of solar masses, so we will 
    // be in those units.
    // So A = integral(phi(m) dm) / (integral(m * phi(m) dm) * old_norm * analytic_integral)
    // where all integrals are over the total mass range
    #ifndef PY_TESTING
    double total_mass, n_stars, A, old_norm;
    old_norm = IMF_norm_code.norm;
    total_mass = integrate( imf->fm, imf->min_mass, imf->max_mass, 1e-9, 1e-9);
    n_stars = integrate( imf->f, imf->min_mass, imf->max_mass, 1e-9, 1e-9);
    // here we don't multiply by old_norm, since its already used in imf_integral
    A = n_stars / (total_mass * imf_integral(imf->min_mass, imf->max_mass));

    IMF_norm_phys.norm = A * old_norm;
    IMF_norm_code.norm = A * old_norm;
    // This new normalization has units of 1 / solar masses.
    // the code mass value will be replaced in the setup function below
    #endif /* not PY_TESTING */
}

struct
{
    double inv_vol; // cell inverse volume
    double Msol_to_code_mass;
}
detail_struct;

#ifndef PY_TESTING
void detailed_enrichment_setup(int level){
    detail_struct.inv_vol = cell_volume_inverse[level];

    detail_struct.Msol_to_code_mass = constants->Msun / units->mass;

    // Then adjust the normalization used for the IMF integration. The 
    // normalization gives units of number of stars per solar mass, so it
    // has units of 1 / mass. We convert 1 / solar mass to 1 / code mass 
    IMF_norm_code.norm = IMF_norm_phys.norm / detail_struct.Msol_to_code_mass; 

    // then set the ejecta for all sources in code masses
    handle_units(&agb);
    handle_units(&sn_ii);
    handle_units(&hn_ii);
    handle_units(&sn_ia);
}

void putback_stellar_mass_detailed(int level, int cell, int ipart,
                                   double density_total, double density_z_ii, 
                                   double density_z_ia, double density_agb, 
                                   double density_c, double density_n, 
                                   double density_o, double density_mg, 
                                   double density_s, double density_ca, 
                                   double density_fe){
    // Essentially copied from feedback.ml.c. However, that 
    // putback_stellar_mass function has a slight bug, where the metals are not
    // considered when calculating the H and He injected. This does that
    // properly.
    // This uses densities, as my feedback functions use densities, as they 
    // allow for some slight optimizations. The function call here required
    // all the metal species, but those can be zeros if this is used without
    // the ENRICHMENT_ELEMENTS flag. The acutal addition is inside a check

    // decrease the mass of the star
    particle_mass[ipart] -= density_total / detail_struct.inv_vol;

    int i;
    double rhor, e_old, rhofact;
    float thermal_pressure;

    // account for momentum change 
    rhor = 1.0 / cell_gas_density(cell);
    e_old = cell_gas_energy(cell) -
            0.5 * ( cell_momentum(cell,0)*cell_momentum(cell,0) +
                    cell_momentum(cell,1)*cell_momentum(cell,1) +
                    cell_momentum(cell,2)*cell_momentum(cell,2) ) * rhor; 
    cell_gas_density(cell) += density_total;
    rhofact = rhor * cell_gas_density(cell);

    cell_momentum(cell,0) += density_total * particle_v[ipart][0];
    cell_momentum(cell,1) += density_total * particle_v[ipart][1];
    cell_momentum(cell,2) += density_total * particle_v[ipart][2];
                    
    cell_gas_energy(cell) = e_old + 
                            0.5 * ( cell_momentum(cell,0)*cell_momentum(cell,0) +
                                    cell_momentum(cell,1)*cell_momentum(cell,1) +
                                    cell_momentum(cell,2)*cell_momentum(cell,2) ) /
                            cell_gas_density(cell);

    // NG: this is to allow non-thermal pressure contribution
    thermal_pressure = MAX((cell_gas_gamma(cell)-1.0)*cell_gas_internal_energy(cell),0.0);
    cell_gas_pressure(cell) = MAX(0.0,cell_gas_pressure(cell)-thermal_pressure);

    cell_gas_internal_energy(cell) *= rhofact;
    cell_gas_pressure(cell) += thermal_pressure*rhofact;

    // then add the metals. As we go we subtract the metals from the total,
    // as we will turn what's left into H and He
    #ifdef ENRICHMENT
    cell_gas_metal_density_II(cell) += density_z_ii;
    density_total -= density_z_ii;
    #ifdef ENRICHMENT_SNIa
    cell_gas_metal_density_Ia(cell) += density_z_ia;
    density_total -= density_z_ia;
    #endif /* ENRICHMENT_SNIa */
    #ifdef ENRICHMENT_ELEMENTS
    cell_gas_metal_density_C(cell)   += density_c;
    cell_gas_metal_density_N(cell)   += density_n;
    cell_gas_metal_density_O(cell)   += density_o;
    cell_gas_metal_density_Mg(cell)  += density_mg;
    cell_gas_metal_density_S(cell)   += density_s;
    cell_gas_metal_density_Ca(cell)  += density_ca;
    cell_gas_metal_density_Fe(cell)  += density_fe;
    cell_gas_metal_density_AGB(cell) += density_agb;
    density_total -= density_agb;
    // Elements are a subset of the total metallicity, they do not need to
    // be subtracted individually
    #endif /* ENRICHMENT_ELEMENTS */
    #endif /* ENRICHMENT */

    // The rest is H and He. Those are both fully ionized when deposited.
    // We scale the non-metal amounts by the mass fractions for H and He
    #ifdef RADIATIVE_TRANSFER
    cell_HII_density(cell) += density_total * constants->XH;
    cell_HeIII_density(cell)  += density_total * constants->XHe;
    #endif /* RADIATIVE_TRANSFER */
}
#endif /* PY_TESTING */


void get_yields_sn_ia(double z, double *return_ejecta){
    /**
     * The yields of a single SN Ia explosion at a given metallicity.
     * 
     * Here we only linearly interpolate in metallicity between the available
     * models. If we are below/above the minimum/maximum metallicity of the 
     * models, we return the ejecta of the minimum/maximum metallicity model.
     * 
     * The returned values are the ejected masses of a single SN Ia for all
     * the fields available, and have units of solar masses.
     * 
     * Parameters
     * ----------
     * z: Metallicity of the progenitor stellar population.
     * return_ejecta: Array of doubles. The ejecta will be stored in this 
     *                variable rather than being directly returned.
     * 
     * Returns
     * -------
     * Nothing, but the ejected masses are stored in the `return_ejecta` 
     * variable and can be accessed in the original scope. The values have 
     * units of solar masses. 
     */

    // declare some variables used for intermediate calculations
    int z_bound_idxs[2];
    double rate_z0, rate_z1;
    double z_0, z_1;

    // Assuming the metallicity doesn't exactly match what's in the 
    // tables, we have to interpolate. We need to get the nearest metallicity 
    // points (z_0, z_1) in the table that are above and below the given 
    // metallicity:
    // z_0 <= z <= z_1
    
    // First get the indices of these values in the metallicity array... 
    find_z_bound_idxs(z, sn_ia.metallicities, sn_ia.n_metallicity, z_bound_idxs);
    // ...then get the actual values.
    z_0 = sn_ia.metallicities[z_bound_idxs[0]];
    z_1 = sn_ia.metallicities[z_bound_idxs[1]];

    // then interpolate each element individually.
    int idx_field;
    for (idx_field = 0; idx_field < sn_ia.n_fields; idx_field++) {
        // get the rates at the boundaries so we can interpolate.
        rate_z0 = sn_ia.ejecta_code[idx_field][z_bound_idxs[0]][0];
        rate_z1 = sn_ia.ejecta_code[idx_field][z_bound_idxs[1]][0];

        // interpolate in metallicity to get the final rate
        return_ejecta[idx_field] = interpolate(z, z_0, z_1, rate_z0, rate_z1);
    }
}


/* 
 * ============================================================================
 * 
 * Initialization functions
 * 
 * ============================================================================
 * In the initialization above the `detailed_enrichment_init`  
 * called other functions. These helper functions for reading the files and 
 * handling the age information are here.
 */
void file_read_in_sn_ia(char* file_name) {
    /**
     * Read in a file containing data of the SN Ia ejecta to populate the
     * associated parts of the SN Ia structure.
     * 
     * This assumes that the basic attributes of the sn_ia structure have 
     * already been set by the beginning of the `detailed_enrichment_init` 
     * function.
     * 
     * Parameters
     * ----------
     * file_name: Name of the file containing the SN Ia ejecta. Do not include
     *            the directory, since this will handle that.
     * 
     * Returns
     * -------
     * Nothing, but populates the sn_ia structure with the data in the file.
     */

    // open the file for reading
    FILE* table = open_file(file_name);
    
    // The first part of the file is the header, so take care of it
    handle_header(table, "# z      ");

    // Set up some variables to handle the read in of the yield data 
    // In each line, there are n_fields columns, plus metallicity.
    int n_in_line = sn_ia.n_fields + 1;
    double line[n_in_line];  // placeholder, will hold data in each line
    int idx_line; 

    // The file is structured so that each row is a separate metallicity, and 
    // the columns are the elements we care about. We iterate through the 
    // metallicities, which is equivalent to iterating through rows.
    int idx_met, idx_field;
    for (idx_met=0; idx_met < sn_ia.n_metallicity; idx_met++){
        // Use the helper function to get the data from the line and store
        // it in the array `line`.
        fscanf_var_length(table, line, n_in_line);
        
        // Store the metallicity of the line
        sn_ia.metallicities[idx_met] = line[0];

        // Then store the yield information in this row. We iterate over
        // all the fields this line holds and store then one by one 
        for (idx_field=0; idx_field < sn_ia.n_fields; idx_field++) {
            idx_line = idx_field + 1;  // metallicity is index zero
            // initially set both physical and code to the same, so it can 
            // be used in my python tests
            sn_ia.ejecta_phys[idx_field][idx_met][0] = line[idx_line];   
            sn_ia.ejecta_code[idx_field][idx_met][0] = line[idx_line];             
        }
    }
    fclose(table);
}

void file_read_in_ejecta(char* file_name, struct IndividualSource *source) {
    /**
     * Read in a file containing data of the ejecta of a given source to 
     * populate the structure of that source.
     * 
     * This assumes that the basic attributes of the structure have 
     * already been set by the beginning of the `detailed_enrichment_init` 
     * function.
     * 
     * Parameters
     * ----------
     * file_name: Name of the file containing the ejecta. Do not include
     *            the directory, since this will handle that.
     * source: Structure that will have the data filled in.
     * 
     * Returns
     * -------
     * Nothing, but populates the structure with the data in the file.
     */

    // open the file for reading
    FILE* table = open_file(file_name);
    
    // The first part of the file is the header, so take care of it
    handle_header(table, "# mass      ");

    // Set up some variables to handle the read in of the yield data 
    // In each line, there are n_fields columns, plus mass and metallicity.
    int n_in_line = source->n_fields + 2; 
    double line[n_in_line];  // placeholder, will hold data in each line

    // The file is structured so that each metallicity has all the ages for it, 
    // then the next metallicity begins. This controls how we iterate 
    // over the file 
    int idx_line, idx_met, idx_mass, idx_field; 
    for (idx_met=0; idx_met < source->n_metallicity; idx_met++){
        for (idx_mass=0; idx_mass < source->n_mass_models; idx_mass++){
            // Use the helper function to get the data from the line and store
            // it in the array `line`.
            fscanf_var_length(table, line, n_in_line);

            // Since the ages are assumed to be the same for all metallicities,
            // we only need to store the ages once. We choose to do this
            // during the first metallicity point.
            if (idx_met == 0){
                source->masses[idx_mass] = line[0];  
            } 

            // Similarly, we only need to store the metallicities once. We 
            // choose to do this on the first age. 
            if (idx_mass == 0){
                source->metallicities[idx_met] = line[1];
            }

            // Then store the yield information in this row. We iterate over
            // all the fields this line holds and store then one by one
            for (idx_field=0; idx_field < source->n_fields; idx_field++) {
                // mass and metallicity are indices 0 and 1, so skip them
                idx_line = idx_field + 2; 
                // initially set both physical and code to the same, so it can 
                // be used in my python tests
                source->ejecta_phys[idx_field][idx_met][idx_mass] = line[idx_line];  
                source->ejecta_code[idx_field][idx_met][idx_mass] = line[idx_line];                
            }
        }
    }
    fclose(table);
}

void file_read_in_winds(char* file_name){
    /**
     * Read in a file containing data of the ejecta of a given source to 
     * populate the structure of that source.
     * 
     * This assumes that the basic attributes of the structure have 
     * already been set by the beginning of the `detailed_enrichment_init` 
     * function. This is an exact copy of the function above, but it 
     * needs to have a different signature to work with winds. 
     * 
     * Parameters
     * ----------
     * file_name: Name of the file containing the ejecta. Do not include
     *            the directory, since this will handle that.
     * source: Structure that will have the data filled in.
     * 
     * Returns
     * -------
     * Nothing, but populates the structure with the data in the file.
     */
    
    // open the file for reading
    FILE* table = open_file(file_name);

    // The first part of the file is the header, so take care of it
    handle_header(table, "# mass    ");

    // Set up some variables to handle the read in of the yield data 
    // In each line, there are n_fields columns, plus mass and metallicity.
    int n_in_line = winds.n_fields + 2; 
    double line[n_in_line];  // placeholder, will hold data in each line

    // The file is structured so that each metallicity has all the ages for it, 
    // then the next metallicity begins. This controls how we iterate 
    // over the file 
    int idx_line, idx_met, idx_mass, idx_field; 
    for (idx_met=0; idx_met < winds.n_metallicity; idx_met++){
        for (idx_mass=0; idx_mass < winds.n_mass_points; idx_mass++){
            // Use the helper function to get the data from the line and store
            // it in the array `line`.
            fscanf_var_length(table, line, n_in_line);

            // Since the ages are assumed to be the same for all metallicities,
            // we only need to store the ages once. We choose to do this
            // during the first metallicity point.
            if (idx_met == 0){
                winds.masses[idx_mass] = line[0];  
            } 

            // Similarly, we only need to store the metallicities once. We 
            // choose to do this on the first age. 
            if (idx_mass == 0){
                winds.metallicities[idx_met] = line[1];
            }

            // Then store the yield information in this row. We iterate over
            // all the fields this line holds and store then one by one
            for (idx_field=0; idx_field < winds.n_fields; idx_field++) {
                // mass and metallicity are indices 0 and 1, so skip them
                idx_line = idx_field + 2; 
                winds.mass_loss[idx_met][idx_mass] = line[idx_line];                
            }
        }
    }
    fclose(table);

    // Then add some of the extra info into the wind structure
    handle_wind_masses(); 
}


FILE *open_file(char* file_name){
    /** 
     * Open a given file for reading.
     * 
     * Here I assume that the file is either in the working directory or in
     * a certain path on my local machine. This needs to be changed eventually. 
     * 
     * Parameters
     * ----------
     * file_name: Name of the file to be opened. Do not include the directory, 
     *            since this will handle that.
     * 
     * Returns
     * -------
     * file pointer set to the beginning of the opened file.
     * 
     * Error Checking
     * --------------
     * If the file does not exist an error will be raised.
     */

    // Start by making the file stream and parsing the current file location
    // to get the absolute path to the data files (which are assumed to be
    // in the same directory as this file.)
    FILE *table;
    char file_path[300] = "";
    char this_file[300] = __FILE__;
    strcat(file_path, dirname(this_file));
    strcat(file_path, "/");
    strcat(file_path, file_name);
    table = fopen(file_path, "r");

    // If that didn't work, warn the user.
    if (table == NULL ) {
        char msg[1000];
        sprintf(msg, "Unable to open %s file.\nEnsure this is in the "
                     "same directory as this file: \n%s.\n"
                     "The code needs to be in the same location "
                     "as it was when it was compiled,\nso try recompiling.", 
                     file_name, __FILE__);
        raise_error(msg);
    }
    return table;
}


void handle_header(FILE *table, char *end_flag){
    /** 
     * Read the file up to the end of the header. When done, the file will
     * be located to read the first data line of the file.
     * 
     * Parameters
     * ----------
     * table: file pointer that contains the data being read in
     * end_flag: The beginning of the line that indicates the last line of the
     *           header. In the context of these files, the column headers are
     *           the last line before the data starts, so the beginning of the
     *           column header line is what to pass in here.
     * 
     * Returns
     * -------
     * Nothing, but moves the file pointer to be at the beginning of the data
     */

    // Initialize the place we store the header lines. This must be 
    // initialized, not just declared, so that it doesn't reuse old memory
    char header[1000] = "";
    // We iterate through the file until `header` matches `end_flag`. 
    // We do this comparison with `strncmp`, which takes two C strings and 
    // returns 0 if they are equal. We only compare the first N characters, 
    // where N is the length of `end_flag`, calculated with`strlen`.
    while(strncmp(header, end_flag, strlen(end_flag)) != 0){
        // if not equal, keep going
        fscanf(table, "%[^\n]\n", header);
    }
}


void fscanf_var_length(FILE *table, double *line, int n_in_line){
    /** 
     * Read one line of the file, with a specified number of items.
     * 
     * This convenience function is used since `fscanf` is clunky and not 
     * flexible when you want to read a variable number of items into an 
     * array, since you have to manually specify all the places items to. This 
     * is my attempt to remedy that and make an easier to use `fscanf`. This 
     * code itself is very simple (and a little ugly), but it does make 
     * `fscanf` easier to use.
     * 
     * An array is passed into this function, and filled with the desired
     * amount of doubles read from the line the file is currently at.
     * 
     * Parameters
     * ----------
     * table: file pointer that contains the data being read in.
     * line: array of doubles. After read in, the first `n_in_line` values
     *       of this array will contain the first `n_in_line` values of the 
     *       row the file is currently at. 
     * n_in_line: how many doubles to read from the line.
     * 
     * Returns
     * -------
     * Nothing, but fills `line` with data from the line being read that can be
     * accessed in the calling scope.
     * 
     * Error Checking
     * --------------
     * This is currently build to read up to 10 items from a line. If more than
     * this are desired, an error will be raised and the user will be asked
     * to put their desired case into the code here. 
     */

    // make an error message for later use
    char err_msg[] = "The number of items in the line is too much "
                     "for the `fscanf_var_length` function.\n"
                     "Add the appropriate case to this function.\n"
                     "It is located at "
                     "`src/sf/models/feedback.detailed_enrich.c`.";
    // The core algorithm here is to just manually put different parameters
    // into `fscanf` depending on how many items are needed.
    switch(n_in_line){
        case 1:
            fscanf(table, "%lf\n", &line[0]);
            break;
        case 2:
            fscanf(table, "%lf %lf\n", &line[0], &line[1]);
            break;
        case 3:
            fscanf(table, "%lf %lf %lf\n", &line[0], &line[1], &line[2]);
            break;
        case 4:
            fscanf(table, "%lf %lf %lf %lf\n", 
                &line[0], &line[1], &line[2], &line[3]);
            break;
        case 5:
            fscanf(table, "%lf %lf %lf %lf %lf\n", 
                &line[0], &line[1], &line[2], &line[3], &line[4]);
            break;
        case 6:
            fscanf(table, "%lf %lf %lf %lf %lf %lf\n", 
                   &line[0], &line[1], &line[2], &line[3], &line[4], &line[5]);
            break;
        case 7:
            fscanf(table, "%lf %lf %lf %lf %lf %lf %lf\n", 
                   &line[0], &line[1], &line[2], &line[3], &line[4], &line[5],
                   &line[6]);
            break;
        case 8:
            fscanf(table, "%lf %lf %lf %lf %lf %lf %lf %lf\n", 
                   &line[0], &line[1], &line[2], &line[3], &line[4], &line[5],
                   &line[6], &line[7]);
            break;
        case 9:
            fscanf(table, "%lf %lf %lf %lf %lf %lf %lf %lf %lf\n", 
                   &line[0], &line[1], &line[2], &line[3], &line[4], &line[5],
                   &line[6], &line[7], &line[8]);
            break;
        case 10:
            fscanf(table, "%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf\n", 
                   &line[0], &line[1], &line[2], &line[3], &line[4], &line[5],
                   &line[6], &line[7], &line[8], &line[9]);
            break;
        case 11:
            fscanf(table, "%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf\n", 
                   &line[0], &line[1], &line[2], &line[3], &line[4], &line[5],
                   &line[6], &line[7], &line[8], &line[9], &line[10]);
            break;
        case 12:
            fscanf(table, "%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf\n", 
                   &line[0], &line[1], &line[2], &line[3], &line[4], &line[5],
                   &line[6], &line[7], &line[8], &line[9], &line[10], 
                   &line[11]);
            break;
        case 13:
            fscanf(table, "%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf\n", 
                   &line[0], &line[1], &line[2], &line[3], &line[4], &line[5],
                   &line[6], &line[7], &line[8], &line[9], &line[10], 
                   &line[11], &line[12]);
            break;
        default:
            // Can't handle all cases. If it's too long, tell the user to code
            // it in here themselves 
            raise_error(err_msg);
    }
}


void handle_wind_masses(void) {
    /** 
     * Initialize some of the mass variables that are present in the wind 
     * yields. 
     * 
     * This includes the minimum and maximum turnoff mass, which are 
     * directly present in the table and the logarithmic spacing between mass 
     * values. The spacing is assumed to be equally spaced in log, so we 
     * calculate the difference between two adjacent values and assume it 
     * applies to all.
     * 
     * Returns
     * -------
     * Nothing, but sets attributes of the wind source.
     */

    // The first value is the maximum mass 
    winds.max_mass = winds.masses[0];
    winds.min_mass = winds.masses[winds.n_mass_points-1];
    winds.log_min_mass = log10(winds.min_mass);
    winds.log_max_mass = log10(winds.max_mass);
    // get the spacing in log space between arbitrary mass values. For the 
    // spacing to be positive with decreasing values of masses, we take
    // an earlier value dividied by a later value
    winds.mass_spacing_log = log10(winds.masses[9] / winds.masses[10]);
}

#ifndef PY_TESTING
void handle_units(struct IndividualSource *source) {
    /** 
     * The yields for AGB, SNII, HN, and SNIa are given in solar masses. We
     * need these values in code masses, so we do that conversion here.
     * 
     * This sets the ejecta_code attribute for the structure passed in. Should 
     * be one of AGB, SNII, HN, or SNIa, although this is not checked.
     * 
     * The values in the winds table is fractional cumulative mass loss, which
     * is unitless, so those values do not need to be adjusted. 
     * 
     * Returns
     * -------
     * Nothing, but sets the ejecta_code attribute for this source
     */
    int idx_met, idx_field, idx_mass;

    // Then turn all sources from solar masses into code masses
    for (idx_met=0; idx_met < source->n_metallicity; idx_met++){
        for (idx_field=0; idx_field < source->n_fields; idx_field++) {
            for (idx_mass=0; idx_mass < source->n_mass_models; idx_mass++){
                source->ejecta_code[idx_field][idx_met][idx_mass] = source->ejecta_phys[idx_field][idx_met][idx_mass] * detail_struct.Msol_to_code_mass;
            }
        }
    }
}
#endif /* PY_TESTING */


/* 
 * ============================================================================
 * 
 * Functions for retrieving ejecta for all but SNIa
 * 
 * ============================================================================
 * The following functions are used to get the key aspect of this code: the 
 * ejected mass of a given element as a function of time and metallicity of the
 * progenitor. 
 */
double get_cumulative_mass_winds(double age, double m_turnoff,
                                 double z, double age_50){
    /**
     * Get the cumulative fractional mass lost in winds by a stellar population
     * 
     * Here fractional means that this returns the mass in winds lost by the 
     * population divided by its initial stellar mass, so to get the actual
     * ejected mass the result of this function needs to be multiplied by the
     * stellar mass of the star particle. Cumulative means that this gives the
     * fractional mass lost by winds from t=0 to today by this star particle.
     * 
     * I chose to have this return cumulative mass so that it would be easier
     * to make sure that the total amount of winds released is correct. 
     * Tabulating rates (with respect to time) is not a good idea, since those 
     * are dependent on stellar lifetimes. Doing cumulative ejecta as a 
     * function of mass leaving the main sequence is the most robust way to 
     * do this. To get the amount in a timestep, one can simply call this 
     * for the beginning and end times in that timestep, then subtract.
     * 
     * Note that I want this to be able to be called from Python, so I can't 
     * use any of the core ART stuff like lifetimes. That's why there are some
     * overlap in the parameters passed in.
     * 
     * Parameters
     * ----------
     * age: Age at which to get the cumulative ejecta
     * m_turnoff: Mass leaving the main sequence at this age and metallicity
     * z: metallicity of the star particle
     * age_50: Age at which the 50 solar mass model leaves the main sequence
     *         at this metallicity. 
     * 
     * Returns
     * --------
     * Double with the value of the cumulative fractional mass lost in winds
     * by the stellar population with the parametesr passed in.
     */ 
    int z_bound_idxs[2], mass_bound_idxs[2];
    double z_0, z_1, m_0, m_1;
    double cumulative_z0m0, cumulative_z0m1, cumulative_z1m0, cumulative_z1m1;
    double cumulative_z0, cumulative_z1;
    double fraction;

    // We start by getting the indices into the array that bound the masses
    // of interest
    find_z_bound_idxs(z, winds.metallicities, winds.n_metallicity, z_bound_idxs);
    // ...then get the actual values.
    z_0 = winds.metallicities[z_bound_idxs[0]];
    z_1 = winds.metallicities[z_bound_idxs[1]];

    // We need to get the values to interpolate. The way we do this depends on
    // whether we're before the first timestep in the table or not. First we'll
    // consider the next time point 
    if (m_turnoff > winds.max_mass){
        // Here we are before the first step of the table
        // We assume the rate of wind ejecta is constant in all time before 
        // that happened, since no stars have evolved off the main sequence, 
        // so we can use the lifetimes to see what fraction of the wind should 
        // be included. Here age_50 is the age of the 50 solar mass model at
        // the user's metallicity. This works because it gives the true 
        // fraction of the ejecta that we should eject. We don't want to use
        // the lifetimes at the metallicities of the points. We can think of it 
        // as interpolating to get the ejecta at the user's metallicity then 
        // multiply that by the true fraction to get the true ejecta. We don't 
        // want to include the incorrect lifetimes in the interpolation. This 
        // is mathematically equivalent to what's done here, where we multiply
        // by the fraction before the interpolation. 
        fraction = age / age_50;
        cumulative_z0 = fraction * winds.mass_loss[z_bound_idxs[0]][0];
        cumulative_z1 = fraction * winds.mass_loss[z_bound_idxs[1]][0];
    }
    else if (m_turnoff < winds.min_mass){
        // Here we're after the last timestep. We just grab the last point
        cumulative_z0 = winds.mass_loss[z_bound_idxs[0]][n_mass_points_winds-1];
        cumulative_z1 = winds.mass_loss[z_bound_idxs[1]][n_mass_points_winds-1];
    }
    else{
        // We aren't in the first or last timestep, so what we do is get two
        // bounding mass points, then interpolate between them
        find_mass_bound_idxs_descending(m_turnoff, winds.masses, 
                                        winds.n_mass_points, 
                                        guess_mass_idx_winds(m_turnoff),
                                        mass_bound_idxs);
                                               
        m_0 = winds.masses[mass_bound_idxs[0]];
        m_1 = winds.masses[mass_bound_idxs[1]];

        cumulative_z0m0 = winds.mass_loss[z_bound_idxs[0]][mass_bound_idxs[0]];
        cumulative_z0m1 = winds.mass_loss[z_bound_idxs[0]][mass_bound_idxs[1]];
        cumulative_z1m0 = winds.mass_loss[z_bound_idxs[1]][mass_bound_idxs[0]];
        cumulative_z1m1 = winds.mass_loss[z_bound_idxs[1]][mass_bound_idxs[1]];
        // interpolate in mass to get the proper ejecta at a given metallicity 
        cumulative_z0 = interpolate(m_turnoff, m_0, m_1,
                                    cumulative_z0m0, cumulative_z0m1);
        cumulative_z1 = interpolate(m_turnoff, m_0, m_1,
                                    cumulative_z1m0, cumulative_z1m1);
    }

    // Then interpolate in metallicity to get the cumulative ejecta at the
    // right metallicity for our two times
    return interpolate(z, z_0, z_1, cumulative_z0,  cumulative_z1);
}


void get_yields_arbitrary(double z, double m, struct IndividualSource *source, 
                          double *ejecta_return){
    int idx_field;
    // get the metallicity indicies
    int z_bound_idxs[2], m_bound_idxs[2];
    double z_0, z_1, m_0, m_1;
    find_z_bound_idxs(z, source->metallicities, source->n_metallicity, z_bound_idxs);
    find_mass_bound_idxs(m, source->masses, source->n_mass_models, 0, m_bound_idxs);
    
    z_0 = source->metallicities[z_bound_idxs[0]];
    z_1 = source->metallicities[z_bound_idxs[1]];
    m_0 = source->masses[m_bound_idxs[0]];
    m_1 = source->masses[m_bound_idxs[1]];

    double ejecta_z0[source->n_fields];
    double ejecta_z1[source->n_fields];
    // First we interpolate in mass. But if we're outside the limits and the
    // min and max are the same, we have to do something trickier to handle 
    // that properly. 
    if (m_bound_idxs[0] != m_bound_idxs[1]){
        // This is the regular case where we're inside the appropriate range
        for (idx_field = 0; idx_field < source->n_fields; idx_field++) {
            // Get the rates at the boundaries so we can interpolate
            double ejecta_z0m0 = source->ejecta_code[idx_field][z_bound_idxs[0]][m_bound_idxs[0]];
            double ejecta_z0m1 = source->ejecta_code[idx_field][z_bound_idxs[0]][m_bound_idxs[1]];
            double ejecta_z1m0 = source->ejecta_code[idx_field][z_bound_idxs[1]][m_bound_idxs[0]];
            double ejecta_z1m1 = source->ejecta_code[idx_field][z_bound_idxs[1]][m_bound_idxs[1]];

            // then interpolate in mass
            ejecta_z0[idx_field] = interpolate(m, m_0, m_1, ejecta_z0m0, ejecta_z0m1);
            ejecta_z1[idx_field] = interpolate(m, m_0, m_1, ejecta_z1m0, ejecta_z1m1);
        }
    }
    else if (m_bound_idxs[0] == 0 || m_bound_idxs[0] == source->n_mass_models-1){
        // Here we're outside of the model range, where we extrapolate
        for (idx_field = 0; idx_field < source->n_fields; idx_field++){
            // Get the rates at the boundaries so we can extrapolate
            double raw_ejecta_z0 = source->ejecta_code[idx_field][z_bound_idxs[0]][m_bound_idxs[0]];
            double raw_ejecta_z1 = source->ejecta_code[idx_field][z_bound_idxs[1]][m_bound_idxs[0]];

            // then extrapolate in mass
            ejecta_z0[idx_field] = extrapolate(m, m_0, raw_ejecta_z0);
            ejecta_z1[idx_field] = extrapolate(m, m_0, raw_ejecta_z1);
        }
    }
    else{
        // Here we're just exactly at a mass point
        for (idx_field = 0; idx_field < source->n_fields; idx_field++){
            ejecta_z0[idx_field] = source->ejecta_code[idx_field][z_bound_idxs[0]][m_bound_idxs[0]];
            ejecta_z1[idx_field] = source->ejecta_code[idx_field][z_bound_idxs[1]][m_bound_idxs[0]];
        }
    }

    // Then we can do the metallicity interpolation
    for (idx_field = 0; idx_field < source->n_fields; idx_field++) {
        ejecta_return[idx_field] = interpolate(z, z_0, z_1,
                                               ejecta_z0[idx_field], 
                                               ejecta_z1[idx_field]);
    }
}

void get_yields_agb(double z, double m, double *ejecta_return){
    get_yields_arbitrary(z, m, &agb, ejecta_return);
}

void get_yields_sn_ii(double z, double m, double *ejecta_return){
    get_yields_arbitrary(z, m, &sn_ii, ejecta_return);
}

void get_yields_hn_ii(double z, double m, double *ejecta_return){
    get_yields_arbitrary(z, m, &hn_ii, ejecta_return);
}


void find_z_bound_idxs(double z, double *metallicities, int n_metallicity,
                       int *bounds_idx){
    /** 
     * Find the model metallicities that bound a metallicity of interest, then
     * return the indices of those metallicities in the metallicity array.
     * 
     * In variables, this returns i and j such that
     * `metallicities[i] <= z <= metallicities[j]`
     * and i and j are either identical (if `z` matches one of the items 
     * available) or j - i = 1, meaning the indices returned are the ones
     * closest to the desired metallicity.
     * 
     * This is used when picking the yields to interpolate between. 
     * 
     * If `z` is lower than all values in `metallicities`, then 0 will be 
     * returned for both indicies, while if it is higher than all values,
     * `n_metallicity` - 1 is returned for both indices. In practice, this 
     * means that we use the yields at the lowest/highest metallicity if the
     * star particle has metallicity lower/higher than the min/max metallicity
     * available. 
     * 
     * Parameters
     * ----------
     * z: metallicity to find the bounding indices of
     * metallicities: array of metallicities that will be compared to `z` to 
     *                find the correct indices. Must be sorted.
     * n_metallicity: number of elements in the `metallicities` array.
     * 
     * Returns
     * --------
     * Two element integer array with elements as described above. 
     */
    int higher_idx, lower_idx;

    // if z is less than the lowest metallicity, use the lowest metallicity for 
    // both the upper and lower bounds, indicating no interpolation
    if (z <= metallicities[0]) {
        bounds_idx[0] = 0;
        bounds_idx[1] = 0;
    }
    // if it's higher than the highest, return both highest
    else if (z >= metallicities[n_metallicity-1]) {
        bounds_idx[0] = n_metallicity - 1;
        bounds_idx[1] = n_metallicity - 1;
    }
    else { // need to check metallicities in between
        for (lower_idx=0; lower_idx < n_metallicity - 1; lower_idx++){
            // if it exactly equals a metallicity, return that value
            if (z == metallicities[lower_idx]) {
                bounds_idx[0] = lower_idx;
                bounds_idx[1] = lower_idx;
                break;
            }
            // otherwise get the bounding values
            higher_idx = lower_idx + 1;
            if (z > metallicities[lower_idx] && z < metallicities[higher_idx]) {
                bounds_idx[0] = lower_idx;
                bounds_idx[1] = higher_idx;
                break;
            }
        }
    }
}

void find_mass_bound_idxs(double mass, double *masses, int n_masses, 
                          int start, int *bounds_idx){
    /**
     * Find the mass points that bound a mass of interest, then
     * return the indices of those ages in the mass array. This is analagous to
     * what is done in `find_z_bound_idxs`.
     * 
     * In variables, this returns i and j such that
     * `masses[i] <= mass <= masses[j]`
     * and i and j are either identical (if `mass` matches one of the items 
     * available) or j - i = 1, meaning the indices returned are the ones
     * closest to the desired mass.
     * 
     * We start by using a smart first guess to reduce computation time
     * making comparisons, but it does validate that the indexes do correspond
     * to masses that bound the age passed in.
     * 
     * The mass must be lower than the maximum mass in the wind table and
     * larger than the minimum mass in the wind table.
     * is higher than `source->max_age`, `source->n_ages-1` is returned for
     * both indices. In practice, this means that we use the yields at the 
     * earliest/latest age if the star particle has age lower/higher than the 
     * min/max age available. Accessing the yields at late times is fine for
     * SN and massive star winds, since they go to zero once they die. AGB can
     * be an issue, but if the table goes to later ages than the age of the
     * universe this is fine.
     * 
     * Parameters
     * ----------
     * mass: mass to find the bounding indices of
     * source: pointer to one of the structures holding the ejecta information
     *         for ejecta that depend on time. Should be one of the 
     *         following objects: `agb`, `sn_ii`, or `winds`. The age
     *         attributes should already be set, since this function accesses
     *         the `min_age`, `max_age`, `n_ages`, and `ages` attributes.
     *         This assumes that the `ages` attribute is sorted. 
     * start: What index to start at. This can often be zero, but if you have
     *        a smart guess, that is good too.
     * 
     * Returns
     * --------
     * Two element integer array with elements as described above. 
     */
    int idx;
    // before we start we do need to check boundary conditions
    // if z is less than the lowest metallicity, use the lowest metallicity for 
    // both the upper and lower bounds, indicating no interpolation
    if (mass <= masses[0]) {
        bounds_idx[0] = 0;
        bounds_idx[1] = 0;
        return;
    }
    // if it's higher than the highest, return both highest
    else if (mass >= masses[n_masses-1]) {
        bounds_idx[0] = n_masses - 1;
        bounds_idx[1] = n_masses - 1;
        return;
    }
    // get the first guess. This will be treated as the index of the lower
    // bounding age as we do this checking.
    idx = start;
    int done = 0;  // set flag variable to tell us when we're done
    while (!done){
        if (mass == masses[idx]){  
            // mass exactly matches the index
            // return the same for both so we don't need any interpolation
            bounds_idx[0] = idx;
            bounds_idx[1] = idx;
            done = 1;
        }
        else if (mass > masses[idx] &&
                 mass < masses[idx+1]) {
            // Here the age is bounded in the way we want. We return the 
            // two indices that bound it. 
            bounds_idx[0] = idx;
            bounds_idx[1] = idx + 1;
            done = 1;
        }
        else if (mass >= masses[idx+1]){ // guess was too small
            idx++;
            
        }
        else { // guess was too big
            idx--;
        }
    }
}


void find_mass_bound_idxs_descending(double mass, double *masses, int n_masses, 
                                     int start, int *bounds_idx){
    /**
     * Find the mass points that bound a mass of interest, then
     * return the indices of those ages in the mass array. This is analagous to
     * what is done in `find_z_bound_idxs`.
     * 
     * This assumes that the list of masses is sorted in descending order, as 
     * it the case in the winds table.
     * 
     * In variables, this returns i and j such that
     * `masses[i] >= mass >= masses[j]`
     * and i and j are either identical (if `mass` matches one of the items 
     * available) or j - i = 1, meaning the indices returned are the ones
     * closest to the desired mass.
     * 
     * We start by using a smart first guess to reduce computation time
     * making comparisons, but it does validate that the indexes do correspond
     * to masses that bound the age passed in.
     * 
     * If its outside the mass range of the table, i and j will both be the 
     * index of the boundary value (either 0 or the last index)
     * 
     * Parameters
     * ----------
     * mass: mass to find the bounding indices of
     * masses: array of masses that will be compared to `mass` to determine the
     *         correct indices. This function assumes that this array is 
     *         sorted in descending order!
     * n_masses: number of items in the `masses` array.
     * start: What index to start at. This can often be zero, but if you have
     *        a smart guess, that is good too.
     * 
     * Returns
     * --------
     * Two element integer array with elements as described above. 
     */
    int idx;
    // before we start we do need to check boundary conditions
    // if z is less than the lowest metallicity, use the lowest metallicity for 
    // both the upper and lower bounds, indicating no interpolation
    if (mass >= masses[0]) {
        bounds_idx[0] = 0;
        bounds_idx[1] = 0;
        return;
    }
    // if it's higher than the highest, return both highest
    else if (mass <= masses[n_masses-1]) {
        bounds_idx[0] = n_masses - 1;
        bounds_idx[1] = n_masses - 1;
        return;
    }

    // get the first guess. This will be treated as the index of the lower
    // bounding age as we do this checking.
    idx = start;
    int done = 0;  // set flag variable to tell us when we're done
    while (!done){
        if (mass == masses[idx]){  
            // mass exactly matches the index
            // return the same for both so we don't need any interpolation
            bounds_idx[0] = idx;
            bounds_idx[1] = idx;
            done = 1;
        }
        else if (mass < masses[idx] &&
                 mass > masses[idx+1]) {
            // Here the age is bounded in the way we want. We return the 
            // two indices that bound it. 
            bounds_idx[0] = idx;
            bounds_idx[1] = idx + 1;
            done = 1;
        }
        else if (mass <= masses[idx+1]){ // idx is too small (mass too high)
            idx++;
            
        }
        else { // guess was too big
            idx--;
        }
    }
}


int guess_mass_idx_winds(double mass) {
    /** 
     * Provide a first guess for the index into the mass array that corresponds 
     * to a given age.
     * 
     * This assumes that the masses equally spaced in logspace between
     * min_age and max_age. Here is how to derive this. Since the points are
     * evenly spaced in log space, we have 
     * 
     * log(mass) = log(max_mass) - n * spacing
     * 
     * This can then be solved for n. If we round this down, we get a lower
     * index that should provide the index of the age just below the age
     * of interest. 
     * 
     * Parameters
     * ----------
     * age: Mass at which we want an estimate of it's index.
     * 
     * Returns
     * -------
     * integer corresponding to the index into the mass array that should be
     * close to the desired age.
     */
    
    double n = (winds.log_max_mass - log10(mass)) / winds.mass_spacing_log;
    int return_idx = (int) floor(n);

    // check the bounds of the proposed value. It can't be the last value of 
    // the array, since we later need to check for the next value. This makes 
    // us restrict to 2 less than the length as the maximum (one for zero 
    // indexing, one for this effect). 
    if (return_idx > winds.n_mass_points-2) {
        return winds.n_mass_points-2;
    }
    // zero is of course the minimum
    else if (return_idx < 0) {
        return 0;
    }
    else{
        return return_idx;
    }
}

double imf_integral(double m_low, double m_high){
    // Calculate the integral of a Kroupa IMF, returning the number of stars
    // in the requested integral per unit stellar mass. This is so one can
    // multiply by the mass (in code units) to get the true number of stars 
    // in an SSP of that mass.
    // This integral is done analytically, separately from the integrands 
    // present in the imf.c file.
    // Normalization is stored in the IMF_norm_code structure, see that and 
    // the detailed_enrichment_init function that calculates this for
    // more details. This is normalized to integrate to the number of
    // stars in a 1 code_mass SSP. 
    double range;
    double half_03 = 1.231144413;  // 0.5**-0.3
    double half_13 = 2.462288827;  // 0.5**-1.3 

    if (m_high <= 0.5){
        range = (pow(m_low, -0.3) - pow(m_high, -0.3));
    }
    else if (m_low >= 0.5){
        range = (0.3 / 2.6) * (pow(m_low, -1.3) - pow(m_high, -1.3));
    }
    else{
        range =  (pow(m_low, -0.3) - half_03) + 
                 (0.3 / 2.6) * (half_13 - pow(m_high, -1.3));
    }
    return IMF_norm_code.norm * range;
}


double interpolate(double x, double x_0, double x_1, double y_0, double y_1) {
    /**
     * Perform single linear interpolation between two values.
     * 
     * The formula for linear interpolation is easy to derive. We have samples
     * at x_0 and x_1, with values of the function at those points y_0 and y_1.
     * We use the slope between those points to approximate the value at point 
     * of interest x: 
     * 
     * y = y_0 + m (x - x_0). 
     * 
     * where the slope is simply
     * 
     * m = (y_1 - y_0) / (x_1 - x_0)
     * 
     * Parameters
     * ----------
     * x: x value at which to get an interpolated value
     * x_0: x value of one sample
     * x_0: x value of the other sample
     * y_0: y value of the function at x_0
     * y_1: y value of the function at x_1
     * 
     * Returns
     * -------
     * The value at x obtained from linearly interpolating between the two
     * input points. 
     */
    if (y_0 == y_1){  // no interpolation needed
        return y_0;
    }
    double m = (y_1 - y_0) / (x_1 - x_0);
    return y_0 + m * (x - x_0);
}

double extrapolate(double mass, double model_mass, double boundary_ejecta){
    return boundary_ejecta * (mass / model_mass);
}

/* 
 * ============================================================================
 * 
 * Accessory functions
 * 
 * ============================================================================
 */
void raise_error(char *err_msg){
    /**
     * How we handle errors depends on whether we're running the Python tests
     * or doing a real run. For testing, printing a message is fine, since the
     * tests will fail anyway. For a real run we use the ART error function.
     * 
     * Parameters
     * ----------
     * err_msg: Error message to be passed to the respective error handling
     *          methods.
     * 
     * Returns
     * -------
     * Nothing
     */
    #ifdef PY_TESTING
    printf("%s\n", err_msg);
    #else  /* PY_TESTING */
    cart_error(err_msg); 
    #endif /* PY_TESTING */
}


#ifndef PY_TESTING
void debug_enrichment(int ipart, char *source, double time,
                      char *parameter, double value){
    /**
     * Print debugging information to check my detailed enrichment.
     * 
     * This prints the particle ID, the timestep, the type of enrichment
     * being checked here, and then a parameter and its value. The time is used
     * so we can check the same star particle at later outputs.
     * 
     * Parameters
     * ----------
     * ipart: Particle ID
     * source: What type of enrichment we're checking (SNII, AGB, etc)
     * time: The current time. 
     * parameter: The name of the parameter that will be held in `value`
     * value: The value of the named parameter.
     * 
     * Returns
     * -------
     * Nothing
     */
    int fractional_restriction = 1;
    if (ipart % fractional_restriction == 0)
    {
        cart_debug("detail_debug, %d, %s, t=%.17le, %s=%.17le",
                   ipart, source, time, parameter, value);
    }
}
#endif /* PY_TESTING */


/* 
 * ============================================================================
 * 
 * Python testing functions
 * 
 * ============================================================================
 * I wrote some test using a wrapper in Python. The interface with C arrays
 * and structures is clunky, so I wrote some wrapper functions that are used
 * in those tests to make it a lot easier to handle. These are exclusively 
 * used for those tests.
 */
#ifdef PY_TESTING
int *find_z_bound_idxs_agb_py(double z){
    static int bound_idxs[2];
    find_z_bound_idxs(z, agb.metallicities, agb.n_metallicity, bound_idxs);
    return bound_idxs;
}


int *find_z_bound_idxs_winds_py(double z){
    static int bound_idxs[2];
    find_z_bound_idxs(z, winds.metallicities, winds.n_metallicity, bound_idxs);
    return bound_idxs;
}


int *find_z_bound_idxs_sn_ii_py(double z){
    static int bound_idxs[2];
    find_z_bound_idxs(z, sn_ii.metallicities, sn_ii.n_metallicity, bound_idxs);
    return bound_idxs;
}


int *find_z_bound_idxs_sn_ia_py(double z){
    static int bound_idxs[2];
    find_z_bound_idxs(z, sn_ia.metallicities, sn_ia.n_metallicity, bound_idxs);
    return bound_idxs;
}


int guess_mass_idx_winds_py(double value) {
    return guess_mass_idx_winds(value);
}


int *find_mass_bound_idxs_agb_py(double mass){
    static int bound_idxs[2];
    find_mass_bound_idxs(mass, agb.masses, agb.n_mass_models, 0, bound_idxs);
    return bound_idxs;
}


int *find_mass_bound_idxs_sn_ii_py(double mass){
    static int bound_idxs[2];
    find_mass_bound_idxs(mass, sn_ii.masses, sn_ii.n_mass_models, 0, bound_idxs);
    return bound_idxs;
}


int *find_mass_bound_idxs_hn_ii_py(double mass){
    static int bound_idxs[2];
    find_mass_bound_idxs(mass, hn_ii.masses, hn_ii.n_mass_models, 0, bound_idxs);
    return bound_idxs;
}


int *find_mass_bound_idxs_winds_py(double mass){
    static int bound_idxs[2];
    find_mass_bound_idxs_descending(mass, winds.masses, winds.n_mass_points,
                                    guess_mass_idx_winds(mass), bound_idxs);
    return bound_idxs;
}


double *get_yields_sn_ia_py(double z){
    static double return_yields[n_fields_sn_ia];
    get_yields_sn_ia(z, return_yields);
    return return_yields;
}


double get_masses_agb(int idx) {
    return agb.masses[idx];
}


double get_masses_sn_ii(int idx) {
    return sn_ii.masses[idx];
}

double get_masses_hn_ii(int idx) {
    return hn_ii.masses[idx];
}


double get_masses_winds(int idx) {
    return winds.masses[idx];
}


double get_z_winds(int idx) {
    return winds.metallicities[idx];
}


double get_z_agb(int idx) {
    return agb.metallicities[idx];
}


double get_z_sn_ii(int idx) {
    return sn_ii.metallicities[idx];
}


double get_z_sn_ia(int idx) {
    return sn_ia.metallicities[idx];
}


double *get_yields_raw_sn_ii_py(double z, double mass){
    static double return_yields[n_fields_sn_ii];
    get_yields_sn_ii(z, mass, return_yields);
    return return_yields;
}


double *get_yields_raw_hn_ii_py(double z, double mass){
    static double return_yields[n_fields_sn_ii];
    get_yields_hn_ii(z, mass, return_yields);
    return return_yields;
}

double *get_yields_raw_agb_py(double z, double mass){
    static double return_yields[n_fields_agb];
    get_yields_agb(z, mass, return_yields);
    return return_yields;
}


double imf_integral_py(double m_low, double m_high){
    return imf_integral(m_low, m_high);
}


double extrapolate_py(double mass, double model_mass, double boundary_ejecta){
    return extrapolate(mass, model_mass, boundary_ejecta);
}


double interpolate_py(double x, double x_0, double x_1, double y_0, double y_1){
    return interpolate(x, x_0, x_1, y_0, y_1);
}


double get_cumulative_mass_winds_py(double age, double m_turnoff, 
                                    double z, double age_50){
    return get_cumulative_mass_winds(age, m_turnoff, z, age_50);
} 

#endif /* PY_TESTING */

#endif /* STAR_FORMATION */
