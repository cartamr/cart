#include "config.h"
#ifdef STAR_FORMATION

#include <math.h>
#include <string.h>

#include "auxiliary.h"
#include "control_parameter.h"
#include "cosmology.h"
#include "hydro.h"
#include "imf.h"
#include "parallel.h"
#include "particle.h"
#include "starformation.h"
#include "tree.h"
#include "units.h"
#include "times.h"

#include "algorithm.stargrowth.h"


void get_com_pos( int icell, double com_pos[nDim], int level ){
    int i, idir, isign;
    int neighbors[num_neighbors];
    double dcom_pos[nDim];
    double sum[nDim];
    
    cell_center_position(icell,com_pos); /* start at cell position */

    cell_all_neighbors(icell,neighbors);
    for(idir=0;idir<nDim; idir++){ dcom_pos[idir]=0; sum[idir]=cell_gas_density(icell); } 
    for(i=0;i<num_neighbors; i++){
	idir = i/2;
	isign = 1 - (i % 2)*2; // even is +
	dcom_pos[idir] += isign*cell_size[level]*cell_gas_density(neighbors[i]); 
	sum[idir] += cell_gas_density(neighbors[i]);
    }
    /* now assign COM position but stay within cell*/
    for(idir=0;idir<nDim; idir++){
	com_pos[idir] += (
	    dcom_pos[idir] > 0 ?
	    MIN( dcom_pos[idir]/sum[idir], cell_size[level]/2.01) :
	    MAX( dcom_pos[idir]/sum[idir],-cell_size[level]/2.01) );
    }
}

void grow_star_particle_base(int ipart, double delta_mass, int icell, int level, double delta_init_mass, double dt_eff) {
	int i;
	double add_mass, add_init_mass;
	double new_density;
	double density_fraction, thermal_pressure;
	double sum_mass, pmass_orig;
	
	cart_assert(ipart < num_star_particles);
    cart_assert(delta_mass>=0);
	add_mass = MIN( delta_mass, 0.667*cell_volume[cell_level(icell)]*cell_gas_density(icell));
	add_init_mass = MIN(delta_init_mass, 0.667*cell_volume[cell_level(icell)]*cell_gas_density(icell));
    /*     get_com_pos(icell, com_pos, level ); */
    /*     cart_assert( cell_contains_position(icell,com_pos) );  */
    /*     for ( i = 0; i < nDim; i++ ) { */
    /*      particle_x[ipart][i] = (com_pos[i]*add_mass + particle_x[ipart][i]*pmass_orig)/sum_mass; */
    /*     } */
    /* add mass [at COM of neighbors] with cell momentum; */
	pmass_orig = particle_mass[ipart];
	sum_mass = pmass_orig + add_mass;
    //cart_assert( cell_contains_position(icell,particle_x[ipart]) ); 
	for (i = 0; i < nDim; i++) {
		particle_v[ipart][i] = 
			(cell_momentum(icell,i) / cell_gas_density(icell) * add_mass + 
			particle_v[ipart][i]*pmass_orig) / sum_mass;
	}
    /* alternatively, just assign the current velocity of the gas cell
     * to the cluster particle so that it can follow the gas flow. 
     * NOTICE: the current code is wrong since icell can be the id of
     * neighboring cells rather than the central cell */
    /* particle_v[ipart][i] = cell_momentum(icell, i) / cell_gas_density(icell); */
	particle_mass[ipart] += add_mass;
	star_initial_mass[ipart] += add_init_mass;
#ifdef ENRICHMENT
    /* adding metals goes as follows:
    Final metallicity = final metals / final mass
                      = (accreted metals + existing metals) / finalmass
                      = (cell metallicity * accretted mass + existing metallicity * existing mass) / final mass
                      */
    star_metallicity_II[ipart] = 
	( cell_gas_metal_density_II(icell) / cell_gas_density(icell) * add_mass +
	  star_metallicity_II[ipart] * pmass_orig) / sum_mass ;
#ifdef ENRICHMENT_SNIa
    star_metallicity_Ia[ipart] = 
	( cell_gas_metal_density_Ia(icell) / cell_gas_density(icell) * add_mass +
	  star_metallicity_Ia[ipart] * pmass_orig) / sum_mass;
#endif /* ENRICHMENT_SNIa */
#ifdef ENRICHMENT_ELEMENTS
    star_metallicity_AGB[ipart] = 
      (cell_gas_metal_density_AGB(icell) / cell_gas_density(icell) * add_mass + 
       star_metallicity_AGB[ipart] * pmass_orig) / sum_mass;

    star_metallicity_C[ipart] = 
      (cell_gas_metal_density_C(icell) / cell_gas_density(icell) * add_mass + 
       star_metallicity_C[ipart] * pmass_orig) / sum_mass;

    star_metallicity_N[ipart] = 
      (cell_gas_metal_density_N(icell) / cell_gas_density(icell) * add_mass + 
       star_metallicity_N[ipart] * pmass_orig) / sum_mass;

    star_metallicity_O[ipart] = 
      (cell_gas_metal_density_O(icell) / cell_gas_density(icell) * add_mass + 
       star_metallicity_O[ipart] * pmass_orig) / sum_mass;

    star_metallicity_Mg[ipart] = 
      (cell_gas_metal_density_Mg(icell) / cell_gas_density(icell) * add_mass + 
       star_metallicity_Mg[ipart] * pmass_orig) / sum_mass;

    star_metallicity_S[ipart] = 
      (cell_gas_metal_density_S(icell) / cell_gas_density(icell) * add_mass + 
       star_metallicity_S[ipart] * pmass_orig) / sum_mass;

    star_metallicity_Ca[ipart] = 
      (cell_gas_metal_density_Ca(icell) / cell_gas_density(icell) * add_mass + 
       star_metallicity_Ca[ipart] * pmass_orig) / sum_mass;

    star_metallicity_Fe[ipart] = 
      (cell_gas_metal_density_Fe(icell) / cell_gas_density(icell) * add_mass + 
       star_metallicity_Fe[ipart] * pmass_orig) / sum_mass;
#endif /* ENRICHMENT_ELEMENTS */
#endif /* ENRICHMENT */
#ifdef CLUSTER
    star_tfinal[ipart] = particle_t[ipart];
    star_ave_age[ipart] = star_ave_age[ipart] + add_mass/star_initial_mass[ipart]*(particle_t[ipart]-star_tbirth[ipart]-star_ave_age[ipart]);
#ifdef ENRICHMENT_ELEMENTS    
    star_metal_dispersion[ipart] = star_metal_dispersion[ipart] + add_mass*(cell_gas_metal_density_Fe(icell)/cell_gas_density(icell))*(cell_gas_metal_density_Fe(icell)/cell_gas_density(icell));
#else
    star_metal_dispersion[ipart] = star_metal_dispersion[ipart] + add_mass*(cell_gas_metal_density_II(icell)/cell_gas_density(icell))*(cell_gas_metal_density_II(icell)/cell_gas_density(icell));
#endif /* ENRICHMENT_ELEMENTS */
    
#ifdef CLUSTER_DEBUG
    /* new definition of age spread. the factor sqrt(pi) is to normalize
     * the width of the SFH to 2 sigma for the Gaussian SFH case. */
    star_age_spread[ipart] = star_age_spread[ipart] + add_mass*add_mass/dt_eff;
#endif /* CLUSTER_DEBUG */
#endif /* CLUSTER */

 	   /* adjust cell values */
    	new_density = cell_gas_density(icell) - add_mass * cell_volume_inverse[level];
    	density_fraction = new_density / cell_gas_density(icell);
    
    	/*
    	// NG: this is to allow non-thermal pressure contribution
    	*/
    	thermal_pressure = MAX((cell_gas_gamma(icell)-1.0)*cell_gas_internal_energy(icell),0.0);
    	cell_gas_pressure(icell) = MAX(0.0,cell_gas_pressure(icell)-thermal_pressure);
    
    	cell_gas_density(icell) = new_density;
    	cell_gas_internal_energy(icell) *= density_fraction;
    	cell_momentum(icell,0) *= density_fraction;
    	cell_momentum(icell,1) *= density_fraction;
    	cell_momentum(icell,2) *= density_fraction;
    
    	cell_gas_pressure(icell) += thermal_pressure*density_fraction;

    	// We assume that extra energy remains constant per volume
    	for ( i = 0; i < num_extra_energy_variables; i++ ) {
    		cell_gas_energy(icell) -= cell_extra_energy_variables(icell,i);
    	}
    	cell_gas_energy(icell) *= density_fraction;
    	for ( i = 0; i < num_extra_energy_variables; i++ ) {
    		cell_gas_energy(icell) += cell_extra_energy_variables(icell,i);
    	}

    	for ( i = 0; i < num_chem_species; i++ ) {
		cell_advected_variable(icell,i) *= density_fraction;
    	}
}


void grow_star_particle( int ipart, float delta_mass, int icell, int level) {
    // This version doesn't distringuish between delta mass and delta_init_mass
    // and doesn't need the dt info, so we can pass 0 to that.
    grow_star_particle_base(ipart, delta_mass, icell, level, delta_mass, 0.0);
}

void grow_star_particle2(int ipart, float delta_mass, int icell, int level, float delta_init_mass ){
    // This is only called when CLUSTER is not defined, so we won't use dt_eff
    grow_star_particle_base(ipart, delta_mass, icell, level, delta_init_mass, 0.0);
}

void grow_star_particle3(int ipart, double delta_mass, int icell, int level, double dt_eff){
    // This will only be called when CLUSTER is used, where we don't distinguish
    // between delta_mass and delta_init_mass
    grow_star_particle_base(ipart, delta_mass, icell, level, delta_mass, dt_eff);
}
#endif /* STAR_FORMATION */
