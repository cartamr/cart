#ifndef __FEEDBACK_SNIa_DETAILED__
#define __FEEDBACK_SNIa_DETAILED__

#ifdef STAR_FORMATION
#ifndef PY_TESTING
void snIa_detailed_config_init();
void snIa_detailed_config_verify();
void snIa_detailed_init();
void snIa_detailed_setup(int level);

#if defined(HYDRO) && defined(PARTICLES)
void snIa_thermal_feedback_detailed(int, int, int, double);
#endif /* HYDRO && PARTICLES */
#endif /* PY_TESTING */
#endif /* STAR_FORMATION */

// functions used in my Python tests
#ifdef PY_TESTING
double *sn_ia_core_py(double, double, double, double, double, double);
double get_sn_ia_number_py(double, double, double);
#endif  /* PY TESTING */
#endif  /* Header guard */