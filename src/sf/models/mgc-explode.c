#include "config.h"
#ifdef STAR_FORMATION

#include <math.h>
#include <string.h>

#include "auxiliary.h"
#include "control_parameter.h"
#include "cosmology.h"
#include "hydro.h"
#include "hydro_sgst.h"
#include "imf.h"
#include "parallel.h"
#include "particle.h"
#include "starformation.h"
#include "tree.h"
#include "units.h"
#include "starformation_feedback.h"
#include "plugin.h"


#include "feedback.mgc-explode.h"
#include "feedback.kinetic.h"
#include "feedback.SNR-model.h"
#include "feedback.ml.h"
#include "feedback.rad.h"
#include "feedback.snII.h"
#include "feedback.snIa.h"
/*
//  Exploding MGCs after a period and injects particle's remaining gas mass at set 
//  Temperature. Also governs mgc evolution, as in Lee 2016. After the explosion, 
//  feedback is as in popM-thermal.  
*/

void evolve_mgc(int ipart);
void evolve_mgc_ml(int level, int cell, int ipart); /*Mass loss handled separately from stellar mass update*/

extern double dUfact;
extern double dKfact;
extern double feedback_temperature_ceiling;
extern double feedback_turbulence_temperature_ceiling;
struct MGC_EXPLODE_t mgc_explode = { 1.0e6, 8.0e6, 0.0, 1.0, 8.0, 100.0 };
struct MGC_EXPLODE_PROP_t mgc_explode_phys, mgc_explode_code;

double tau;

struct {
  double delta; /*Power of time dependency */
  double tau_ff; /* years */
  double eps_ff0; /*epsilon_ff at tau_ff*/
  double alpha; /*Relative to tau_ff */ } mgc = {2.0,6.7e6,0.1,0.0};


void mgc_explode_config_init()
{
  control_parameter_add(control_parameter_double,&mgc_explode.explosion_temperature,"mgc-explode:explode-temp","Temperature of Explosion");

  control_parameter_add(control_parameter_time,&mgc_explode.explosion_time,"mgc-explode:explode-time","time at which MGC particle explodes");

  control_parameter_add2(control_parameter_time,&mgc_explode.mass_loss_time,"mgc-explode:ml-time","mgc-explode:mass-loss-time","time over which molecular gas is lost back to the grid after the mgc explosion");

  control_parameter_add2(control_parameter_double,&mgc_explode.yield_factor,"mgc-explode:yield-factor","mgc-explode.yield_factor","fractional yield relative to the coded in model.");

  control_parameter_add4(control_parameter_double,&mgc_explode.min_mass,"mgc-explode:min-mass","mgc-explode.min_max","imf:min-SNII-mass","am_snii","the minimum mass of stars that explode as type II supernovae.");

  control_parameter_add2(control_parameter_double,&mgc_explode.max_mass,"mgc-explode:max-mass","mgc-explode.max_max","the maximum mass of stars that explode as type II supernovae.");
 control_parameter_add(control_parameter_time,&mgc.tau_ff,"sf:tau_ff","the freefall time for an MGC particle.");
  control_parameter_add(control_parameter_double,&mgc.delta,"sf:delta","time power law slope for star formation efficiency.");
  control_parameter_add(control_parameter_double,&mgc.eps_ff0,"sf:eps_ff0","baseline star formation efficiency.");
  control_parameter_add(control_parameter_double,&mgc.alpha,"sf:alpha","parameter for mgc mass loss by reionization of mg (normalized to tau_ff)");
  snII_config_init();
  snIa_config_init();
  ml_snl2012_config_init();
}


void mgc_explode_config_verify()
{
  /*
  //  MGC Explosion Feedback
  */
  VERIFY(mgc-explode:explode-temp, !(mgc_explode.explosion_temperature < 0.0) );

  VERIFY(mgc-explode:explode-time, mgc_explode.explosion_time > 0.0 );

  VERIFY(mgc-explode:ml-time, !(mgc_explode.mass_loss_time < 0.0));

  VERIFY(mgc-explode:yield-factor, mgc_explode.yield_factor > 0.0 );

  VERIFY(mgc-explode:min-mass, mgc_explode.min_mass > 1.0 );

  VERIFY(mgc-explode:max-mass, mgc_explode.max_mass > mgc_explode.min_mass );

  VERIFY(sf:tau_ff, mgc.tau_ff > 0.0);

  VERIFY(sf:delta, mgc.delta >= 0.0);

  VERIFY(sf:eps_ff0, mgc.eps_ff0 > 0.0);

  VERIFY(sf:alpha, mgc.alpha >= 0.0);

  if((mgc.alpha!=0)&&(mgc_explode.mass_loss_time!=0)) cart_error("alpha and t_ml both defined. Choose only one mass loss mechanism.");

  snIa_config_verify();
  snII_config_verify();
  ml_snl2012_config_verify();
}


int mgc_find_lowage_cluster(int icell){
    int ipart, ipart_store;
    double min_age, age, smg; 
    /*Finds the youngest particle in the cell to accrete mass onto*/

    ipart = cell_particle_list[icell]; 
    min_age=1e30;
    ipart_store=-1;
    while ( ipart != NULL_PARTICLE )
      {
	if(particle_is_star(ipart) 
#ifdef STAR_PARTICLE_TYPES
	   && (star_particle_type[ipart] == STAR_TYPE_NORMAL 
	       || star_particle_type[ipart] == STAR_TYPE_FAST_GROWTH)
#endif
	   )
	  { /* if a cluster exists */
	    /* if theres more than one seed then assign to star with lowest age; */
	    smg = particle_mass[ipart]- star_initial_mass[ipart]; /* MG mass; MGCs should be positive, Normal stars are negative. */
	    if (smg > 0.0){ /*Only MGCs */
	      age = particle_t[ipart]-star_tbirth[ipart];
	      if( age < min_age ){ 
		min_age = age;
		ipart_store = ipart;
	      }
	    }
	  }
	ipart = particle_list_next[ipart];
    }
    return ipart_store;
}


void mgc_explode_init()
{
  double total_mass, ejecta_mass;
  double number_SNII;

  /*
  //  All masses are in Msun
  */  
  total_mass = integrate( imf->fm, imf->min_mass, imf->max_mass, 1e-6, 1e-9 );
  cart_assert(total_mass > 0.0);

  ejecta_mass = integrate( imf->fm, mgc_explode.min_mass, mgc_explode.max_mass, 1e-6, 1e-9 );
  number_SNII = integrate( imf->f, mgc_explode.min_mass, mgc_explode.max_mass, 1e-6, 1e-9 );
  cart_assert(number_SNII > 0.0);

  mgc_explode_phys.teject = mgc_explode.explosion_time;
  mgc_explode_phys.tml = mgc_explode.mass_loss_time;
  mgc_explode_phys.temperature = constants->K*mgc_explode.explosion_temperature;
  mgc_explode_phys.metals = mgc_explode.yield_factor*integrate( imf->fmz, mgc_explode.min_mass, mgc_explode.max_mass, 1e-6, 1e-9 )/total_mass;
 
  mgc_explode_phys.fmass = ejecta_mass/total_mass;
  snII_init();
  snIa_init();
  ml_init(); 
}


void mgc_explode_setup(int level)
{
  mgc_explode_code.teject = mgc_explode_phys.teject*constants->yr/units->time;
  mgc_explode_code.tml = mgc_explode_phys.tml*constants->yr/units->time;
  mgc_explode_code.temperature = mgc_explode_phys.temperature*constants->K/units->temperature; 
  mgc_explode_code.metals = mgc_explode_phys.metals*cell_volume_inverse[level];  
  mgc_explode_code.fmass = mgc_explode_phys.fmass;
  tau = mgc.tau_ff * constants->yr / units->time;
  rad_setup(level);
  snIa_setup(level);
  snII_setup(level);
  ml_setup(level);
}


double mgc_fracH2(int cell)
{
#ifdef RADIATIVE_TRANSFER
  double U = rtUmwFS(cell);
  double D = rtDmwFL(cell);
#else
  double U = 1;
#ifdef ENRICHMENT
  double D = cell_gas_metal_density(cell)/(constants->Zsun*cell_gas_density(cell));
#else /* ENRICHMENT */
  double D = fixed_metallicity;
#endif /* ENRICHMENT */
#endif /* RADIATIVE_TRANSFER */
  double D0 = 0.17;
  double g = sqrt(D*D+D0*D0);
  double q = sqrt(0.01+U);
  double SigmaR1 = 50/g*q/(1+0.69*q);
  double alp = 0.5 + 1/(1+sqrt(U*D*D/600));
  double rhoH = 0.76*cell_gas_density(cell);
  double SigmaH = 2*units->density*units->length/constants->Msun*constants->pc*constants->pc*rhoH*cell_sobolev_length(cell); /* 2 because Lsob is one-sided, Sigma = 2 rho Lsob */
  double R = pow(SigmaH/SigmaR1,alp);

  return R/(1+R);
}

#if defined(HYDRO) && defined(PARTICLES)

void mgc_explode_thermal_feedback2(int level, int cell, int ipart, double t_next, double extra_factor )
{
  int j, ipartcheck;
  double  dU, dm;

  float dmstar,SFR;
  double dt = t_next - particle_t[ipart];
  double tml_end = mgc_explode_code.teject+mgc_explode_code.tml; /*Adds in delayed mass loss time*/
  double tage = particle_t[ipart] - star_tbirth[ipart];
  /* Evolve ODE  if age will be lower than explosion age at next time step */
  if((tage+dt)<mgc_explode_code.teject)
    {
      /*Mass accretion term handled here to be more accurate in ODE evolution*/
      ipartcheck = mgc_find_lowage_cluster(cell); 
      if ( ipart == ipartcheck ) {
	SFR = sfr_rate(cell);
	dmstar = SFR*mgc_fracH2(cell)*cell_gas_density(cell)*cell_volume[level]; /* sfr is 0 or 1 for this method, all molecular gas is collapsed to mgc particle */
	if(dmstar > 0.0){ 
	  grow_star_particle2(ipart,dmstar,cell,level,0.0);
	}
      }
      evolve_mgc(ipart); /* Time dependent component*/
      if(mgc.alpha>0.0) evolve_mgc_ml(level,cell,ipart); /*If mg mass loss is desired to be modeled*/
    }
  else if((tage<mgc_explode_code.teject)) /*Explode if next age will be above explosion age*/
    {
#ifdef ENRICHMENT
      /*unchecked*/
          cell_gas_metal_density_II(cell) += mgc_explode_code.metals*star_initial_mass[ipart]; 
#endif /* ENRICHMENT */
	  dm = particle_mass[ipart]-star_initial_mass[ipart];
          dU = MIN(1.5*dm*mgc_explode_code.temperature,dUfact*cell_gas_density(cell));
#ifdef SGST_SN_SOURCE
	  cart_error("Error, subgrid turbulence not currently supported in MGC particle mode.");
#endif /* SGST_SN_SOURCE */

          /* limit energy release and don't allow to explode in hot bubble */
          if(units->temperature*cell_gas_temperature(cell) < feedback_temperature_ceiling)
            {
              cell_gas_energy(cell) += dU;
              cell_gas_internal_energy(cell) += dU;
              cell_gas_pressure(cell) += dU*(cell_gas_gamma(cell)-1);
	      /* Explosion Mass Loss now handled in separate case to allow for distributed time gas loss (to match profiles) */
            }
    }
  else if(tage>mgc_explode_code.teject){ /*normal popM-thermal feedback after mgcs explode*/
    snII_thermal_feedback(level,cell,ipart,t_next);
    snIa_thermal_feedback(level,cell,ipart,t_next);
    ml_feedback(level, cell, ipart, t_next);
  }
  /*Mass loss of explosion handled here, linearly. First if tries to distribute roughly evenly. second (and last) ensures mass ends where it should. */
  if(((tage+dt)<tml_end) && ((tage+dt)>mgc_explode_code.teject)){ /* Start during explosion step */
    dm = (particle_mass[ipart]-star_initial_mass[ipart])*dt/(tml_end-tage); /*set slope to hit stellar mass at the end of mass loss*/
    dm = MIN(dm,particle_mass[ipart]-star_initial_mass[ipart]); /*Limit to avoid loss of particle mass accounted for in stellar mass*/
    cell_gas_density(cell) += dm*cell_volume_inverse[level];
    for ( j = 0; j<nDim;j++){
      cell_momentum(cell,j)+= dm*particle_v[ipart][j];
    }
    particle_mass[ipart] -= dm;
  }
  /*Second if statement to assure proper endpoint */
  if((tage<tml_end) && ((tage+dt)>tml_end)){ /*last step of mass loss, ensure all molecular gas expelled */
    dm = particle_mass[ipart]-star_initial_mass[ipart];
    cell_gas_density(cell) += dm*cell_volume_inverse[level];
    for ( j = 0; j<nDim;j++){
      cell_momentum(cell,j)+= dm*particle_v[ipart][j];
    }
    particle_mass[ipart] = star_initial_mass[ipart];
  }
}

void mgc_explode_thermal_feedback(int level, int cell, int ipart, double t_next )
{
  mgc_explode_thermal_feedback2(level,cell,ipart,t_next,1.0);
}

void mgc_explode_kinetic_feedback(int level, int cell, int ipart, double t_next )
{
}


void mgc_explode_remnant_feedback(int level, int cell, int ipart, double t_next )
{
}

void evolve_mgc(int ipart){
  double t;
/*
// This function evolves the mgc evolution equation from Lee 2016 without mass loss or 
// gain terms, which are handled by grow_star_particle2 and evolve_mgc_ml. 
//
*/
  if(particle_is_star(ipart)
#ifdef STAR_PARTICLE_TYPES
     && (star_particle_type[ipart] == STAR_TYPE_NORMAL
     || star_particle_type[ipart] == STAR_TYPE_FAST_GROWTH)
#endif
     )
    {
      t = (particle_t[ipart] - star_tbirth[ipart]);
      if(particle_mass[ipart] > star_initial_mass[ipart]){ /*Should restrict to mgcs*/
	cart_assert(t<mgc_explode_code.teject);
	if(particle_dt[ipart]>tau/10.0) cart_error("bug: particle timestep is too small for mgc evolution. dt = %g, tau_ff = %g",particle_dt[ipart],tau);
	star_initial_mass[ipart] += particle_dt[ipart]*mgc.eps_ff0*(particle_mass[ipart]-star_initial_mass[ipart])*pow(t/tau,mgc.delta)/tau;
	if(star_initial_mass[ipart]>particle_mass[ipart]){ /* Don't allow stellar mass to exceed total mass*/
	  star_initial_mass[ipart] = particle_mass[ipart];
	}
      }
    }
}

void evolve_mgc_ml(int level, int cell, int ipart){
/*
//  This function is designed to handle the mass loss by reionizing molecular gas
//  (alpha in the Lee 2016 model). Currently, it does not reinject energy, which 
//  will make the explosion energy smaller. Can be added in trivially. 
*/
  double t,dm, dU;
  int j;
  if(particle_is_star(ipart)
#ifdef STAR_PARTICLE_TYPES
     && (star_particle_type[ipart] == STAR_TYPE_NORMAL
     || star_particle_type[ipart] == STAR_TYPE_FAST_GROWTH)
#endif
     )
    {
      t = (particle_t[ipart] - star_tbirth[ipart]);
      if(particle_mass[ipart] > star_initial_mass[ipart]){
	dm = mgc.alpha/tau*star_initial_mass[ipart]*particle_dt[ipart]; /*alpha normalized to tau, not absolute rate*/
	dm = MIN(dm,particle_mass[ipart]-star_initial_mass[ipart]); /*Limit mg loss to total mg*/
	cell_gas_density(cell) += dm*cell_volume_inverse[level]; /*put back mass */
	for (j = 0; j<nDim;j++){
	  cell_momentum(cell,j)+= dm*particle_v[ipart][j]; /*put back momentum*/
	}
	particle_mass[ipart] -= dm; /* take mass from particle*/
      }
    }
}

#endif /* HYDRO && PARTICLES */

#endif /* STAR_FORMATION */
