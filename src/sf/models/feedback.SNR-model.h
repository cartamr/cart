#ifndef __SNR_MODEL_H__
#define __SNR_MODEL_H__

#ifdef HYDRO

void snr_model_config_init();
void snr_model_config_verify();
extern void (*snr_model_setup)(int);
extern void (*snr_model)(double*, double*, double*);

#ifdef SGS_TURBULENCE
extern double snr_momentum_to_turbulence;
#endif /* SGS_TURBULENCE */
extern double snr_momentum_to_heat;

extern double snr_boost_momentum;
#ifdef SGS_TURBULENCE
extern double snr_boost_turbulence;
#endif /* SGS_TURBULENCE */
extern double snr_boost_heat;

#endif /* HYDRO */
#endif /* __SNR_MODEL_H__ */
