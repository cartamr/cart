#ifndef __ALGORITHM_STARGROWTH_H__
#define __ALGORITHM_STARGROWTH_H__

#ifndef CONFIGURED
#error "Missing config.h include."
#endif

#ifdef STAR_FORMATION
void grow_star_particle( int ipart, float delta_mass, int icell, int level);
void grow_star_particle2( int ipart, float delta_mass, int icell, int level, float delta_init_mass);
void grow_star_particle3( int ipart, double delta_mass, int icell, int level, double dt_eff );
#endif /* STAR_FORMATION */
#endif
