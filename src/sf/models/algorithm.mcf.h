#ifndef __MCF_H__
#define __MCF_H__

#ifndef CONFIGURED
#error "Missing config.h include."
#endif


typedef struct mcf_type
{
    double fH2sf;  /* fraction of H2 that is star-forming */
    double fH2min; /* min H2 frac */
    double TH2max; /* max temp of H2 gas */
    double Dbreak;
    double fH2sfSlope;
#ifndef RADIATIVE_TRANSFER
    double Umw;    /* value to use w/out RT */
#endif
    /* internal values */
    double DBfac;
}
mcf_t;

extern const mcf_t* mcf;


void mcf_config_init();
void mcf_config_verify();

void mcf_init();
void mcf_setup(int level);

double mcf_fH2(int cell, float den);
int mcf_find_local_cloud(int cell);

void mcf_feedback(int level, int cell, int ipart, double t_next);


#endif /* __MCF_H__ */
