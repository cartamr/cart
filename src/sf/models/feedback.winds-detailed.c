/**
 * Note that the feedback.detailed-enrich.c file has much of the functionality
 * that this uses. For this to work correctly, make sure both the 
 * detailed_enrichment_init() and detailed_enrichment_setup() functions are 
 * used in the appropriate sections of the sf_feedback.*.c file being used.
 */
#ifndef PY_TESTING
#include "config.h"
#endif /* PY_TESTING */

#ifdef STAR_FORMATION

#ifndef PY_TESTING
#include "cosmology.h"          // tphys_from_tcode
#include "auxiliary.h"          // local_proc_id
#include "imf.h"                // stellar lifetimes
#include "parallel.h"           // MASTER_NODE
#include "starformation.h"      // star particle arrays
#include "tree.h"               // gas density functions
#include "units.h"              // units
#endif /* PY_TESTING */


#include "feedback.detailed_enrich.h"  // heavy lifting of wind ejecta

#include <stdlib.h>
#include <math.h>               // log10

// get the fixed metallicity if we aren't using enrichment
extern float fixed_metallicity;

double winds_ejecta_core(double, double, double, double, double, double, double);

// Also make a structure for code mass quantities. The initial value doesn't
// matter. 
struct
{
    double inv_vol; // cell inverse volume
}
wind_detailed_code = {1};

#ifndef PY_TESTING
void winds_detailed_config_init()
{
}

void winds_detailed_config_verify()
{
    // The table here depends on a Kroupa IMF with mass limits of
    // 0.1 to 50 M_sun. Check that those are chosen by the user, and warn them
    // if they are not.
    if (strcmp("Kroupa", imf->name) != 0 || imf->min_mass != 0.08 || imf->max_mass != 50.0){
        
        cart_debug("---------------------------------------------------------");
        cart_debug("Warning: IMF assumptions used to create the tabulated wind");
        cart_debug("         values used in src/sf/models/feedback.winds-detailed.c");
        cart_debug("         do not match the IMF values chosen by you!");
        cart_debug("         That used the following IMF paramaters:");
        cart_debug("         IMF = Kroupa");
        cart_debug("         IMF minimum mass = 0.08 M_sun");
        cart_debug("         IMF maximum mass = 50 M_sun");
        cart_debug("         You chose:");
        cart_debug("         IMF = %s", imf->name);
        cart_debug("         IMF minimum mass = %f M_sun", imf->min_mass);
        cart_debug("         IMF maximum mass = %f M_sun", imf->max_mass);
        cart_debug("         This is not a fatal error, but know that the tabulated");
        cart_debug("         enrichment from winds in inconsistent with your choices.");
        cart_debug("---------------------------------------------------------");
    }
}

void winds_detailed_init()
{
    // diagnostics
    if(local_proc_id == MASTER_NODE) {
        cart_debug("Winds: stars from 8 to 50 Msun have pre-SN mass loss");
    }
}

void winds_detailed_setup(int level)
{
    wind_detailed_code.inv_vol = cell_volume_inverse[level];
}

void winds_enrichment(int level, int cell, int ipart, double t_next){
    /**
     * Handle enrichment from winds
     */
    double star_total_metallicity, m_initial, log_Z, age_50;
    double ejecta;
    double m_turnoff_now, m_turnoff_next;
    double t, tn, dt, age, tb;
    // Get time information. These are all in years.
    t = tphys_from_tcode(particle_t[ipart]);
    tn = tphys_from_tcode(t_next);
    dt = tn - t;

    tb = tphys_from_tcode(star_tbirth[ipart]);
    #ifdef CLUSTER
    double t_ave = tphys_from_tcode(star_tbirth[ipart] + star_ave_age[ipart]);
    age = t - t_ave;
    #else /* CLUSTER */
    age = t - tb;
    #endif /* CLUSTER  */
    // Check for negative ages, which can happen in the first timstep of a 
    // star particle's life, when t_ave = t_now + 0.5 dt. Since winds are 
    // active in this first timestep, we also need to correct the timestep, 
    // since winds will only be active during half this timestep. See line
    // 327 of src/core/starformation.c. Another way to think of it is that we
    // will use age and dt to get the age at the beginning and end of this 
    // timestep. It's age at the beginning is zero, and the age at the end
    // (which is age + dt) is 0.5 dt
    if (age < 0.0){
        age = 0.0;
        dt *= 0.5;
    }

    // Winds end pretty quickly, so we can do a quick check to see if we can 
    // exit early
    if (age > 60E6){
        return;
    }

    // Get stellar metallicity
    #ifdef ENRICHMENT
    star_total_metallicity = star_metallicity_II[ipart];
    #ifdef ENRICHMENT_SNIa
    star_total_metallicity += star_metallicity_Ia[ipart];
    #endif /* ENRICHMENT_SNIa */
    #ifdef ENRICHMENT_ELEMENTS
    star_total_metallicity += star_metallicity_AGB[ipart];
    #endif /* ENRICHMENT_ELEMENTS */
    #else /* ENRICHMENT */
    star_total_metallicity = fixed_metallicity;
    #endif /* ENRICHMENT */

    // Turn these ages into their equivalent main sequence turnoff masses.
    log_Z = log10(star_total_metallicity);
    m_turnoff_now  = pow(10, mlf_early_times(log10(age), log_Z));
    m_turnoff_next = pow(10, mlf_early_times(log10(age + dt), log_Z));

    // the winds core function needs the age of the 50 solar mass model
    age_50 = pow(10, tlf(log10(50), log_Z));

    // get stellar mass in code masses
    m_initial = star_initial_mass[ipart];

    // Have the detailed enrichment core functionality do the heavy lifting
    // to turn the tables into the cumulative mass lost by winds at both 
    // the beginning and end at this timestep, so we can turn that into
    // the winds lost in this timestep
    ejecta = winds_ejecta_core(age, age + dt,
                               m_turnoff_now, m_turnoff_next,
                               star_total_metallicity, m_initial, age_50);

    // We then need to have the ejecta be a density, so it can be added
    // to the density fields
    ejecta *= wind_detailed_code.inv_vol;

    // We assume winds do no processing, so the fraction of each element or
    // source is the fraction present in the star for that element or source. 
    // We do have several ifdefs to handle the different elements
    #ifdef ENRICHMENT_ELEMENTS
    putback_stellar_mass_detailed(level, cell, ipart,
                                  ejecta,
                                  ejecta * star_metallicity_II[ipart], 
                                  ejecta * star_metallicity_Ia[ipart],
                                  ejecta * star_metallicity_AGB[ipart], 
                                  ejecta * star_metallicity_C[ipart],
                                  ejecta * star_metallicity_N[ipart], 
                                  ejecta * star_metallicity_O[ipart],
                                  ejecta * star_metallicity_Mg[ipart], 
                                  ejecta * star_metallicity_S[ipart],
                                  ejecta * star_metallicity_Ca[ipart], 
                                  ejecta * star_metallicity_Fe[ipart]);
    #else  /* ENRICHMENT_ELEMENTS */
    #ifdef ENRICHMENT_SNIa  
    // SNIa, SNII, no elements
    putback_stellar_mass_detailed(level, cell, ipart,
                                  ejecta,
                                  ejecta * star_metallicity_II[ipart], 
                                  ejecta * star_metallicity_Ia[ipart],
                                  0, 0, 0, 0, 0, 0, 0, 0);
    #else /* ENRICHMENT_SNIa */
    #ifdef ENRICHMENT 
    // SNII, nothing else
    putback_stellar_mass_detailed(level, cell, ipart,
                                  ejecta,
                                  ejecta * star_metallicity_II[ipart], 
                                  0, 0, 0, 0, 0, 0, 0, 0, 0);
    #else /* ENRICHMENT */
    putback_stellar_mass_detailed(level, cell, ipart,
                                  ejecta, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    #endif /* ENRICHMENT */
    #endif /* ENRICHMENT_SNIa */
    #endif /* ENRICHMENT_ELEMENTS */
}
#endif /* not PY_TESTING  */


double winds_ejecta_core(double age_now, double age_next, 
                         double m_turnoff_now, double m_turnoff_next,
                         double z, double m_star, double age_50){
    /**
     * Determine the wind ejecta in a given timestep. 
     * This calls one of the functions in the main detailed_enrich file, which
     * does the heavy lifting.
     * 
     * Separating it like this is weird, but it allows me to wrap this function
     * and test it in Python. Since we can't include the lifetime function in 
     * the compiled Python wrapper,  we can't call the lifetime functions in 
     * here, which is why we pass in both the age and mass leaving the 
     * main sequence.
     * 
     * Parameters
     * ----------
     * age_now: Age at the beginning of the current timestep, in years.
     * age_next: Age at the end of the current timestep, in years.
     * m_turnoff_now: Mass of the star leaving the main sequence at the 
     *                beginning of the timestep.
     * m_turnoff_next: Mass of the star leaving the main sequence at the end of
     *                 the timestep.
     * z: metallicity of the star particle.
     * m_star: Total initial mass of the star particle. The units on this 
     *         will be the units of the returned value.
     * age_50: Age of the 50 solar mass model.
     * 
     * Returns
     * --------
     * Double containing the ejected material by winds, in whatever units the
     * `m_star` parameter was in. 
     */
    double ejecta_now, ejecta_next;
    // We get the cumulative ejecta at the beginning and end of the timestep.
    // So the ejecta in this timestep is the difference between those two.
    // The cumulative mass is the fractional mass lost by winds in a given 
    // stellar population, so to get true masses we also need to multiply by
    // the mass of our stellar population.
    ejecta_now = get_cumulative_mass_winds(age_now, m_turnoff_now, z, age_50);
    ejecta_next = get_cumulative_mass_winds(age_next, m_turnoff_next, z, age_50);
    return (ejecta_next - ejecta_now) * m_star;
}

#ifdef PY_TESTING
double get_ejecta_winds_py(double age_now, double age_next,
                           double m_turnoff_now, double m_turnoff_next, 
                           double m_star, double z_tot, double age_50){
    return winds_ejecta_core(age_now, age_next, 
                             m_turnoff_now, m_turnoff_next,
                             z_tot, m_star, age_50);
} 
#endif /* PY_TESTING */

#endif /* STAR_FORMATION */