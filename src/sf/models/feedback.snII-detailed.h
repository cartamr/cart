#ifndef __FEEDBACK_SNII_DETAILED__
#define __FEEDBACK_SNII_DETAILED__

#ifdef STAR_FORMATION
#ifndef PY_TESTING

void snII_detailed_config_init();
void snII_detailed_config_verify();
void snII_detailed_init();
void snII_detailed_setup(int);
void snII_remnant_feedback_detailed(int, int, int, double);

#endif /* PY_TESTING */
#endif /* STAR_FORMATION */

// functions used in my Python tests
#ifdef PY_TESTING
double *get_ejecta_sn_ii_py(double, double, double, double, double);
void init_rand(void);
double hn_energy_py(double);
double get_hn_fraction_py(double);
#endif  /* PY TESTING */
#endif  /* Header guard */