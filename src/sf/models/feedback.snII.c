#include "config.h"
#ifdef STAR_FORMATION

#include <math.h>
#include <string.h>

#include "auxiliary.h"
#include "control_parameter.h"
#include "cosmology.h"
#include "hydro.h"
#include "hydro_sgst.h"
#include "imf.h"
#include "parallel.h"
#include "particle.h"
#include "starformation.h"
#include "tree.h"
#include "units.h"
#include "starformation_feedback.h"
#include "plugin.h"


#include "feedback.snII.h"
#include "feedback.kinetic.h"
#include "feedback.SNR-model.h"
/*
//  Type II supernova feedback
*/

extern double dUfact;
extern double dKfact;
extern double feedback_temperature_ceiling;
extern double feedback_turbulence_temperature_ceiling;
struct SNII_t snII = { 2.0, 1.0, 1.0e3, 0.0, 1.0, 8.0, 100.0 };
struct SNII_PROP_t snII_phys, snII_code;


void snII_config_init()
{
  control_parameter_add3(control_parameter_double,&snII.energy_per_explosion,"snII:energy-per-explosion","snII.energy_per_explosion","e_51","average energy per type II supernova explosion, in 1e51 ergs.");

  control_parameter_add3(control_parameter_time,&snII.time_duration,"snII:time-duration","snII.time_duration","t_fb","time duration over which type II supernova explosions occur.");

  control_parameter_add4(control_parameter_time,&snII.time_delay,"snII:time-delay","snII.time_delay","snII:time-before-start","snII.time_before_start","time delay between the formation of the stellar particle and type II supernova explosions.");

  control_parameter_add2(control_parameter_double,&snII.yield_factor,"snII:yield-factor","snII.yield_factor","fractional yield relative to the coded in model.");

  control_parameter_add4(control_parameter_double,&snII.min_mass,"snII:min-mass","snII.min_max","imf:min-SNII-mass","am_snii","the minimum mass of stars that explode as type II supernovae.");

  control_parameter_add2(control_parameter_double,&snII.max_mass,"snII:max-mass","snII.max_max","the maximum mass of stars that explode as type II supernovae.");

  control_parameter_add2(control_parameter_double,&snII.kinetic_energy_per_explosion,"snII:kinetic-energy-per-explosion","snII.kinetic_energy_per_explosion","average kinetic energy per type II supernova explosion, in 1e51 ergs.");
}


void snII_config_verify()
{
  /*
  //  type II supernova feedback
  */
  VERIFY(snII:energy-per-explosion, !(snII.energy_per_explosion < 0.0) );

  VERIFY(snII:time-duration, snII.time_duration > 0.0 );

  VERIFY(snII:time-delay, !(snII.time_delay < 0.0) );

  VERIFY(snII:yield-factor, snII.yield_factor > 0.0 );

  VERIFY(snII:min-mass, snII.min_mass > 1.0 );

  VERIFY(snII:max-mass, snII.max_mass > snII.min_mass );
  
  VERIFY(snII:kinetic-energy-per-explosion, !(snII.kinetic_energy_per_explosion < 0.0) );
}




void snII_init()
{
  double total_mass, ejecta_mass;
  double number_SNII;

  /*
  //  All masses are in Msun
  */  
  total_mass = integrate( imf->fm, imf->min_mass, imf->max_mass, 1e-6, 1e-9 );
  cart_assert(total_mass > 0.0);

  ejecta_mass = integrate( imf->fm, snII.min_mass, snII.max_mass, 1e-6, 1e-9 );
  number_SNII = integrate( imf->f, snII.min_mass, snII.max_mass, 1e-6, 1e-9 );
  cart_assert(number_SNII > 0.0);

  snII_phys.tdelay = snII.time_delay;
  snII_phys.teject = snII.time_duration;
  snII_phys.energy = 1e51*constants->erg*snII.energy_per_explosion*number_SNII/(constants->Msun*total_mass);
  snII_phys.metals = snII.yield_factor*integrate( imf->fmz, snII.min_mass, snII.max_mass, 1e-6, 1e-9 )/total_mass;

  snII_phys.kinetic_energy = 1e51*constants->erg*snII.kinetic_energy_per_explosion*number_SNII/(constants->Msun*total_mass); 
  snII_phys.fmass = ejecta_mass/total_mass;  
  snII_phys.number = number_SNII/(constants->Msun*total_mass);
  /*
  // The rest is for diagnostic only
  */
  if(local_proc_id == MASTER_NODE)
    {
      cart_debug("Number of SNII explosions per unit mass: %le per Msun",number_SNII/total_mass);
      cart_debug("SNII specific energy: %le erg/g = (%le km/s)^2",snII_phys.energy,sqrt(snII_phys.energy)/constants->kms);

#ifdef ENRICHMENT 
      cart_debug("SNII metal fraction : %le",snII_phys.metals);
#endif /* ENRICHMENT */
    }
}


void snII_setup(int level)
{
  snII_code.tdelay = snII_phys.tdelay*constants->yr/units->time;
  snII_code.teject = snII_phys.teject*constants->yr/units->time;
  snII_code.energy = snII_phys.energy*units->mass/units->energy*cell_volume_inverse[level]; 
  snII_code.metals = snII_phys.metals*cell_volume_inverse[level]; 

  snII_code.kinetic_energy = snII_phys.kinetic_energy * units->mass/units->energy*cell_volume_inverse[level]; 
  snII_code.fmass = snII_phys.fmass; 
  snII_code.number = snII_phys.number*units->mass;
}


#if defined(HYDRO) && defined(PARTICLES)

void snII_thermal_feedback2(int level, int cell, int ipart, double t_next, double extra_factor )
{
  double dteff, phi, dU, dU_turb;
  double tn = tphys_from_tcode(t_next);
  double tb = tphys_from_tcode(star_tbirth[ipart]);
  double t = tphys_from_tcode(particle_t[ipart]);

  double dt = tn - t;
  double tage = t - tb;

  /* do feedback, enrichment, etc */
  if(snII_phys.energy>0.0 || snII_phys.metals>0.0)
    {
      /* snII proceeds for fpb_snII_code.teject */
      dteff = tage - snII_phys.tdelay; 
      if(dteff<snII_phys.teject && dteff+dt>0) 
        {
#ifdef CLUSTER_FEEDBACK
          phi = dt/tage;
#else /* CLUSTER_FEEDBACK */
          if(dteff+dt>0 && dteff<0)
            {
              phi = MIN((dteff+dt)/snII_phys.teject,1.0);
            }
          else
            {
              phi = MIN(dt,snII_phys.teject-dteff)/snII_phys.teject;
            }
#endif /* CLUSTER_FEEDBACK */

#ifdef ENRICHMENT
          cell_gas_metal_density_II(cell) += phi*snII_code.metals*star_initial_mass[ipart];
#endif /* ENRICHMENT */

          dU = extra_factor*MIN(phi*snII_code.energy*star_initial_mass[ipart],dUfact*cell_gas_density(cell));
#ifdef SGST_SN_SOURCE
	  dU_turb = sgst_model.fraction_from_SN*dU;
	  dU_turb = MIN(  dU_turb, 
			MAX( 0.0, 
			     (feedback_turbulence_temperature_ceiling / units->temperature)
			     * cell_gas_density(cell)
			     / ((sgs_turbulence_gamma-1)*constants->wmu)
			     - cell_gas_turbulent_energy(cell)
			    )  );

	  cell_gas_turbulent_energy(cell) += dU_turb;
	  cell_gas_energy(cell) += dU_turb;
	  dU -= dU_turb; 
#endif /* SGST_SN_SOURCE */

          /* limit energy release and don't allow to explode in hot bubble */
          if(units->temperature*cell_gas_temperature(cell) < feedback_temperature_ceiling)
            {
              cell_gas_energy(cell) += dU;
              cell_gas_internal_energy(cell) += dU;
              cell_gas_pressure(cell) += dU*(cell_gas_gamma(cell)-1);
            }
        }
    }
}

void snII_thermal_feedback(int level, int cell, int ipart, double t_next )
{
  snII_thermal_feedback2(level,cell,ipart,t_next,1.0);
}

void snII_kinetic_feedback(int level, int cell, int ipart, double t_next ){
    double dteff, phi;
    double dt = t_next - particle_t[ipart];
    double tage = particle_t[ipart] - star_tbirth[ipart];
    double dp, dE, dmass;
    
#ifdef STAR_PARTICLE_TYPES
    cart_assert(star_particle_type[ipart] == STAR_TYPE_NORMAL || star_particle_type[ipart] == STAR_TYPE_FAST_GROWTH );
#endif
    if(snII_code.kinetic_energy >0.0){
	dteff = tage - snII_code.tdelay;
	if(dteff<snII_code.teject && dteff+dt>0){

#ifdef CLUSTER_FEEDBACK
          phi = dt/tage;
#else /* CLUSTER_FEEDBACK */
	    if(dteff+dt>0 && dteff<0)   {
		phi = MIN((dteff+dt)/snII_code.teject,1.0);
	    }else{
		phi = MIN(dt,snII_code.teject-dteff)/snII_code.teject;
	    }
#endif /* CLUSTER_FEEDBACK */
	    
	    dE    = phi*star_initial_mass[ipart]* snII_code.kinetic_energy*cell_volume[level];
	    /* really less mass per SNe with time, but this is fine */
	    dmass = phi*star_initial_mass[ipart]* snII_code.fmass;
	    dp = sqrt( 2*dE * 0.9*dmass ); /* 2*0.5m^2v^2  ; 0.9 = remnant fraction*/
	    distribute_momentum(dp, level, cell, dt);
	}
    }
}


void snII_remnant_feedback(int level, int cell, int ipart, double t_next )
{
	double dteff, phi;
	double dt = t_next - particle_t[ipart];
	double tage = particle_t[ipart] - star_tbirth[ipart];
	double P_0, E_0, M_0, N_SN;
	double dp, dU, dK = 0;
	double params[9];

	const double r = cell_size[level];
	const double v_rms = cell_gas_sound_speed(cell); // cs^2 = sum(gamma*P)/rho ~ sigma^2

	// quantities in physical units:
	const double n_0 = cell_gas_density(cell) * units->number_density;
	const double T_0 = cell_gas_temperature(cell) * units->temperature;
#ifdef ENRICHMENT
	const double Z_0 = cell_gas_metal_density(cell)/(constants->Zsun*cell_gas_density(cell));
#else
	const double Z_0 = fixed_metallicity;
#endif

#ifdef STAR_PARTICLE_TYPES
	cart_assert(star_particle_type[ipart] == STAR_TYPE_NORMAL || star_particle_type[ipart] == STAR_TYPE_FAST_GROWTH );
#endif
	if ( (snII_code.energy > 0.0 || snII_code.metals > 0.0)
			&& T_0 < feedback_temperature_ceiling
#ifdef SGS_TURBULENCE
			&& cell_gas_turbulent_temperature(cell)*units->temperature < feedback_turbulence_temperature_ceiling
#endif /* SGS_TURBULENCE */
	) {
		dteff = tage - snII_code.tdelay;

		if ( dteff < snII_code.teject && dteff+dt > 0 ) {
#ifdef CLUSTER_FEEDBACK
			phi = dt/tage;
#else /* CLUSTER_FEEDBACK */
			if ( dteff+dt > 0 && dteff < 0 ) {
				phi = MIN((dteff+dt)/snII_code.teject,1.0);
			} else {
				phi = MIN(dt,snII_code.teject-dteff)/snII_code.teject;
			}
#endif /* CLUSTER_FEEDBACK */

#ifdef ENRICHMENT
			cell_gas_metal_density_II(cell) += phi*snII_code.metals*star_initial_mass[ipart];
#endif /* ENRICHMENT */

			E_0 = phi*star_initial_mass[ipart] * snII_code.energy*cell_volume[level];
			M_0 = phi*star_initial_mass[ipart] * snII_code.fmass;
			N_SN = phi*star_initial_mass[ipart] * snII_code.number;
			P_0 = sqrt( 2*E_0 * 0.9*M_0 ); /* 2*0.5m^2v^2  ; 0.9 = remnant fraction */

			params[0] = r;
			params[1] = E_0;
			params[2] = P_0;
			params[3] = n_0;
			params[4] = Z_0;
			params[5] = N_SN;
			(*snr_model)(params,&dp,&dU);

			dU += snr_momentum_to_heat * dp * v_rms;

#ifdef SGS_TURBULENCE
			dK = snr_momentum_to_turbulence * dp * v_rms;
			dK = MIN(snr_boost_turbulence*dK*cell_volume_inverse[level],dKfact*cell_gas_density(cell));
			cart_assert( !isnan(dK) && dK >= 0.0 && dK != INFINITY );
			cell_gas_energy(cell) += dK;
			cell_gas_turbulent_energy(cell) += dK;
#endif /* SGS_TURBULENCE */

			/* limit energy release and don't allow to explode in hot bubble */
			dU = MIN(snr_boost_heat*dU*cell_volume_inverse[level],dUfact*cell_gas_density(cell));
			cart_assert( !isnan(dU) && dU >= 0.0 && dU != INFINITY );
			cell_gas_energy(cell) += dU;
			cell_gas_internal_energy(cell) += dU;
			cell_gas_pressure(cell) += dU*(cell_gas_gamma(cell)-1);

#ifdef SGS_TURBULENCE
			dp *= snr_boost_momentum*( 1. - snr_momentum_to_turbulence - snr_momentum_to_heat );
#else /* SGS_TURBULENCE */
			dp *= snr_boost_momentum*( 1. - snr_momentum_to_heat );
#endif /* SGS_TURBULENCE */

			cart_assert( !isnan(dp) && dp >= 0.0 && dp != INFINITY );
			distribute_momentum(dp, level, cell, dt);

			params[6] = dp;
			params[7] = dU*cell_volume[level];
			params[8] = dK*cell_volume[level];
			PLUGIN_POINT(GetLocalData,(level, cell, ipart, params, GetLocalDataId_SNIIRemnantFeedback ));

		}
    }
}

#endif /* HYDRO && PARTICLES */

#endif /* STAR_FORMATION */
