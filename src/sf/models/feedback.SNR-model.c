#include "config.h"
#ifdef STAR_FORMATION

#include <math.h>
#include <string.h>

#include "auxiliary.h"
#include "control_parameter.h"
#include "units.h"

#include "feedback.SNR-model.h"


#ifdef SGS_TURBULENCE
double snr_momentum_to_turbulence = 0.3;
double snr_momentum_to_heat = 0.05;
#else /* SGS_TURBULENCE */
double snr_momentum_to_heat = 0.35;
#endif /* SGS_TURBULENCE */

double snr_boost_momentum = 1.0;
#ifdef SGS_TURBULENCE
double snr_boost_turbulence = 1.0;
#endif /* SGS_TURBULENCE */
double snr_boost_heat = 1.0;

char snr_model_string[256];
char snr_model_help_string[2048];
int snr_model_id = -1;
void (*snr_model_setup)(int) = NULL;
void (*snr_model)(double*, double*, double*) = NULL;

#define SNR_MODEL_CONST       	0
void snr_model_const_setup(int level);
void snr_model_const(double *params, double *dp, double *dU);
double snr_momentum = 1e5;
double snr_thermal_energy = 1e49;
double snr_momentum_code;
double snr_thermal_energy_code;

#define SNR_MODEL_FULL			1
void snr_model_full_setup(int level);
void snr_model_full(double *params, double *dp, double *dU);
double snr_radius = 1.;
double alpha_mfq2015;
#define Rc_mfq2015_pc			6.3
#define Rr_mfq2015_pc			9.2
#define R0_mfq2015_pc			2.4
#define Rb_mfq2015_pc			8.0
double Rc_mfq2015;
double Rr_mfq2015;
double R0_mfq2015;
double Rb_mfq2015;

#define SNR_MODEL_FINAL        	2
void snr_model_final_setup(int level);
void snr_model_final(double *params, double *dp, double *dU);
double snr_thermal_fraction_ceiling = 0.1;
double snr_momentum_boost_ceiling = 50.;

#define AVAILIBLE_MODELS_STRING "Available models: \n\t'const' \t constant momentum (snr:p-SN) and thermal energy (snr:U-SN) injected per 1 SN \n\t'full'  \t fit to results by Martizzi et al. 2015 (MNRAS 450, 504)\n\t'final' \t Martizzi et al. 2015 fit at snowplow stage"


void control_parameter_set_snr_model(const char *value, void *ptr, int ind) {
	if ( strcmp(value,"const") == 0 ) {
		snr_model = &snr_model_const;
		snr_model_setup = &snr_model_const_setup;
		snr_model_id = SNR_MODEL_CONST;
		control_parameter_add3(control_parameter_double,&snr_momentum,"snr:momentum","snr-momentum","snr_momentum","constant momentum injected per SN in Msun*km/s.");
		control_parameter_add3(control_parameter_double,&snr_thermal_energy,"snr:thermal-energy","snr-thermal-energy","snr_thermal_energy","constant thermal energy injected per SN in ergs.");
	} else if ( strcmp(value,"full") == 0 ) {
		snr_model = &snr_model_full;
		snr_model_setup = &snr_model_full_setup;
		snr_model_id = SNR_MODEL_FULL;
		control_parameter_add3(control_parameter_double,&snr_radius,"snr:radius","snr-radius","snr_radius","radius of the SN remnant at the moment of momentum/energy injection (in units of SN hosting cell size).");
	} else if ( strcmp(value,"final") == 0 ) {
		snr_model = &snr_model_final;
		snr_model_setup = &snr_model_final_setup;
		snr_model_id = SNR_MODEL_FINAL;
		control_parameter_add3(control_parameter_double,&snr_thermal_fraction_ceiling,"snr:thermal-fraction-ceiling","snr-thermal-fraction-ceiling","snr_thermal_fraction_ceiling","maximal fraction of total SN energy left as thermal energy at snowplow stage.");
		control_parameter_add3(control_parameter_double,&snr_momentum_boost_ceiling,"snr:momentum-boost-ceiling","snr-momentum-boost-ceiling","snr_momentum_boost_ceiling","maximal boost of initial SN momentum.");
	} else {
		sprintf(snr_model_help_string,"ERROR: string '%s' is not a valid name of SN remnant model. %s",value,AVAILIBLE_MODELS_STRING);
		cart_error(snr_model_help_string);
	}
	strcpy(snr_model_string,value);
}



void control_parameter_list_snr_model(FILE *stream, const void *ptr) {
	control_parameter_list_string(stream,snr_model_string);
}


void snr_model_config_init() {
	strcpy(snr_model_string,"must be specified");
	sprintf(snr_model_help_string,"SN remnant model. %s",AVAILIBLE_MODELS_STRING);
	ControlParameterOps control_parameter_snr_model = { control_parameter_set_snr_model, control_parameter_list_snr_model };

	control_parameter_add3(control_parameter_snr_model,&snr_model_id,"snr:model","snr-model","snr_model",snr_model_help_string);

#ifdef SGS_TURBULENCE
	control_parameter_add3(control_parameter_double,&snr_momentum_to_turbulence,"snr:momentum-to-turbulence","snr-momentum-to-turbulence","snr_momentum_to_turbulence","fraction of injected momentum converted to SGS turbulence due to multiple SNR interactions.");
#endif /* SGS_TURBULENCE */
	control_parameter_add3(control_parameter_double,&snr_momentum_to_heat,"snr:momentum-to-heat","snr-momentum-to-heat","snr_momentum_to_heat","fraction of injected momentum converted to thermal energy due to multiple SNR interactions.");

	control_parameter_add3(control_parameter_double,&snr_boost_momentum,"snr:boost-momentum","snr-boost-momentum","snr_boost_momentum","boosting factor for the resulting momentum.");
#ifdef SGS_TURBULENCE
	control_parameter_add3(control_parameter_double,&snr_boost_turbulence,"snr:boost-turbulence","snr-boost-turbulence","snr_boost_turbulence","boosting factor for the resulting turbulent energy.");
#endif /* SGS_TURBULENCE */
	control_parameter_add3(control_parameter_double,&snr_boost_heat,"snr:boost-heat","snr-boost-heat","snr_boost_heat","boosting factor for the resulting thermal energy.");
}

void snr_model_config_verify() {

	switch ( snr_model_id ) {
	case SNR_MODEL_CONST:
		VERIFY( snr:momentum, snr_momentum >= 0.0 );
		VERIFY( snr:thermal-energy, snr_thermal_energy >= 0.0 );
		break;
	case SNR_MODEL_FULL:
		VERIFY( snr:radius, snr_radius > 0.0 );
		break;
	case SNR_MODEL_FINAL:
		VERIFY( snr:thermal-fraction-ceiling, snr_thermal_fraction_ceiling >= 0.0 && snr_thermal_fraction_ceiling < 1.0 );
		VERIFY( snr:momentum-boost-ceiling, snr_momentum_boost_ceiling > 1.0 );
		break;
	default:
		sprintf(snr_model_help_string,"ERROR: snr:model was not set in config file. %s",AVAILIBLE_MODELS_STRING);
		cart_error(snr_model_help_string);
	}

	VERIFY( snr:model, (*snr_model) != NULL && (*snr_model_setup) != NULL );

#ifdef SGS_TURBULENCE
	VERIFY( snr:momentum-to-turbulence, snr_momentum_to_turbulence >= 0.0 && snr_momentum_to_turbulence + snr_momentum_to_heat <= 1.0 );
	VERIFY( snr:momentum-to-heat, snr_momentum_to_heat >= 0.0 && snr_momentum_to_turbulence + snr_momentum_to_heat <= 1.0 );
#else /* SGS_TURBULENCE */
	VERIFY( snr:momentum-to-heat, snr_momentum_to_heat >= 0.0 && snr_momentum_to_heat <= 1.0 );
#endif /* SGS_TURBULENCE */

	VERIFY( snr:boost-momentum, snr_boost_momentum >= 0.0 );
#ifdef SGS_TURBULENCE
	VERIFY( snr:boost-turbulence, snr_boost_turbulence >= 0.0 );
#endif /* SGS_TURBULENCE */
	VERIFY( snr:boost-heat, snr_boost_heat >= 0.0 );
}

/*
 * SN remnant models:
 *
 * 'const' -> constant momentum and thermal energy injected per 1 SN
 */
void snr_model_const_setup(int level) {
	snr_momentum_code = snr_momentum * constants->Msun*constants->kms / (units->mass*units->velocity);
	snr_thermal_energy_code = snr_thermal_energy * constants->erg / (units->energy);
}
void snr_model_const(double *params, double *dp, double *dU) {
	*dp = snr_momentum_code * params[5];
	*dU = snr_thermal_energy_code * params[5];
}

/*
 * 'full' -> fit to results by Martizzi et al. 2015 (MNRAS 450, 504)
 */
void snr_model_full_setup(int level) {
	Rc_mfq2015 = Rc_mfq2015_pc*constants->pc/units->length;
	Rr_mfq2015 = Rr_mfq2015_pc*constants->pc/units->length;
	R0_mfq2015 = R0_mfq2015_pc*constants->pc/units->length;
	Rb_mfq2015 = Rb_mfq2015_pc*constants->pc/units->length;
}
void snr_model_full(double *params, double *dp, double *dU) {
	const double r = params[0]*snr_radius;
	const double E_0 = params[1];
	const double P_0 = params[2];
	const double n_2 = params[3]/100.;
	const double Z_0 = params[4];
	const double alpha =    -11. * pow(Z_0, 0.070)*pow(n_2,0.114);
	const double Rc = Rc_mfq2015 * pow(Z_0,-0.050)*pow(n_2,-0.42);
	const double Rr = Rr_mfq2015 * pow(Z_0,-0.067)*pow(n_2,-0.44);
	const double R0 = R0_mfq2015 * pow(Z_0, 0.021)*pow(n_2,-0.35);
	const double Rb = Rb_mfq2015 * pow(Z_0,-0.058)*pow(n_2,-0.46);

	*dp = P_0 * (( r > Rb ) ? pow( Rb/R0, 1.5 ) : pow( r/R0, 1.5 ));
	*dp = MAX(*dp,P_0);

	*dU = 0.717*E_0 * (( r > Rr ) ?
			pow( Rr/Rc, alpha ) :
			(( r > Rc ) ? pow( r/Rc, alpha ) : 1.0));
	*dU = MIN(*dU,0.717*E_0);

	/*
	cart_debug("DEBUG SNR: n0 = %g #/cc, Z0 = %g Zsun, E0 = %g erg, P0 = %g Msun*km/s \t r = %g Rc = %g Rr = %g pc -> %d %d %d \t -> \t  alpha = %g \t U/U0 = %g \t P/P0 = %g",
				n_2*100.,Z_0,
				E_0*units->energy,
				P_0*units->mass*units->velocity/(constants->Msun*constants->kms),
				r*units->length/constants->pc,
				Rc*units->length/constants->pc,
				Rr*units->length/constants->pc,
				r<Rc,(r>=Rc)&&(r<Rr),r>=Rr,
				alpha,
				*dU/(0.717*E_0),
				*dp/P_0);
	cart_debug("DEBUG SNR: mom per SN: %e",(*dp)*units->mass*units->velocity/(constants->Msun*constants->kms)/params[5]);
	*/
}

/*
 * 'final' -> Martizzi et al. 2015 (MNRAS 450, 504) fit at snowplow stage
 */
void snr_model_final_setup(int level) {

}
void snr_model_final(double *params, double *dp, double *dU) {
	const double E_0 = params[1];
	const double P_0 = params[2];
	const double n_2 = params[3]/100.;
	const double Z_0 = params[4];
	const double alpha = -11. * pow(Z_0, 0.070)*pow(n_2,0.114);

	*dp = P_0 * pow( Rb_mfq2015_pc/R0_mfq2015_pc, 1.5 ) * pow(Z_0,-0.119)*pow(n_2,-0.165);
	*dp = CLIP(*dp,P_0,snr_momentum_boost_ceiling*P_0);

	*dU = 0.717*E_0 * pow( Rr_mfq2015_pc/Rc_mfq2015_pc * pow(Z_0, -0.017)*pow(n_2,-0.02), alpha );
	*dU = MIN(*dU,snr_thermal_fraction_ceiling*E_0);

	/*
	cart_debug("DEBUG SNR: n0 = %g #/cc, Z0 = %g Zsun, E0 = %g erg, P0 = %g Msun*km/s \t -> \t  alpha = %g \t U/U0 = %g \t P/P0 = %g",
			n_2*100.,Z_0,
			E_0*units->energy,
			P_0*units->mass*units->velocity/(constants->Msun*constants->kms),
			alpha,
			*dU/(0.717*E_0),
			*dp/P_0);
	cart_debug("DEBUG SNR: mom per SN: %e",(*dp)*units->mass*units->velocity/(constants->Msun*constants->kms)/params[5]);
	*/
}

#endif /* STAR_FORMATION */
