#ifndef __FEEDBACK_STARII_RAD_H__
#define __FEEDBACK_STARII_RAD_H__

#ifndef CONFIGURED
#error "Missing config.h include."
#endif

#if defined(STAR_FORMATION) && defined(STAR_PARTICLE_TYPES)
float rad_luminosity_popM_ionizingstarII0(int ipart);
#endif /* STAR_FORMATION && STAR_PARTICLE_TYPES */

#endif /* FEEDBACK_STARII_RAD_H__ */
