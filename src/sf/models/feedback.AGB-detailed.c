/**
 * Note that the feedback.detailed-enrich.c file has much of the functionality
 * that this uses. For this to work correctly, make sure both the 
 * detailed_enrichment_init() and detailed_enrichment_setup() functions are 
 * used in the appropriate sections of the sf_feedback.*.c file being used.
 */
#ifndef PY_TESTING
#include "config.h"
#endif /* PY_TESTING */

#ifdef STAR_FORMATION

#ifndef PY_TESTING
#include "auxiliary.h"          // local_proc_id
#include "control_parameter.h"  // parameter parsing
#include "cosmology.h"          // tphys_from_tcode
#include "imf.h"                // stellar lifetimes
#include "parallel.h"           // MASTER_NODE
#include "starformation.h"      // star particle arrays
#include "tree.h"               // gas density functions
#include "units.h"              // units
#include "times.h"              // abox (used in debugging)
#endif /* PY_TESTING */

#include "feedback.detailed_enrich.h"

#include <stdlib.h>
#include <math.h>               // log10

// Get the needed attributes from the detailed enrichment file.
extern int detail_field_idx_agb_C;
extern int detail_field_idx_agb_N;
extern int detail_field_idx_agb_O;
extern int detail_field_idx_agb_Mg;
extern int detail_field_idx_agb_metals;
extern int detail_field_idx_agb_total;

// get the fixed metallicity if we aren't using enrichment
extern float fixed_metallicity;

/*
//  enrichment from AGB stars
//  stars are assumed to go through an instantaneous AGB phase at the end of their life time
*/

// declare function prototypes for internal functions
void enrichment_AGB_core(double*, double, double, double, double, double, double, double);

// Create the structure to hold configuration values. 
struct
{                 
  double max_mass;                 
  // no min mass, assumed that all stars lower than this contribute. 
}
AGB_detailed = {8.0};

// Also make a structure for code mass quantities. I initialize it with 
// 1 for inv_vol so that they can be used like this
// in the python tests, where code units aren't actually defined. 
struct
{
    double inv_vol; // cell inverse volume
}
AGB_detailed_code = {1};


#ifndef PY_TESTING
void AGB_detailed_config_init()
{
  control_parameter_add2(control_parameter_double,&AGB_detailed.max_mass,"AGB:max-mass","AGB.max_mass","the maximum mass of stars that enter AGB phase. Note that this is not checked against the minimum SN mass for consistency.");
}

void AGB_detailed_config_verify()
{
  VERIFY(AGB:max-mass, AGB_detailed.max_mass > 1.0);
  VERIFY(AGB:max-mass, AGB_detailed.max_mass < imf->max_mass);
}

void AGB_detailed_init()
{
    // diagnostics
    if(local_proc_id == MASTER_NODE) {
        cart_debug("AGB: max mass = %g Msun", AGB_detailed.max_mass);
    }
}

void AGB_detailed_setup(int level)
{
    AGB_detailed_code.inv_vol = cell_volume_inverse[level];
}

void AGB_enrichment(int level, int cell, int ipart, double t_next){
    /**
     * Handle the adding of elements to the cells
     */

    double star_total_metallicity, m_initial, log_Z, mass_lost;
    double m_turnoff_now, m_turnoff_next;
    double t, tn, dt, age, tb;
    // Get time information. These are all in years.
    t = tphys_from_tcode(particle_t[ipart]);
    tn = tphys_from_tcode(t_next);
    dt = tn - t;

    tb = tphys_from_tcode(star_tbirth[ipart]);
    #ifdef CLUSTER
    double t_ave = tphys_from_tcode(star_tbirth[ipart] + star_ave_age[ipart]);
    age = t - t_ave;
    #else /* CLUSTER */
    age = t - tb;
    #endif /* CLUSTER  */
    // don't need to check for negative ages, since we do a direct age check
    // below and negative ages won't pass that condition. 

    // Get stellar metallicity
    #ifdef ENRICHMENT
    star_total_metallicity = star_metallicity_II[ipart];
    #ifdef ENRICHMENT_SNIa
    star_total_metallicity += star_metallicity_Ia[ipart];
    #endif /* ENRICHMENT_SNIa */
    #ifdef ENRICHMENT_ELEMENTS
    star_total_metallicity += star_metallicity_AGB[ipart];
    #endif /* ENRICHMENT_ELEMENTS */
    #else /* ENRICHMENT */
    star_total_metallicity = fixed_metallicity;
    #endif /* ENRICHMENT */
    

    // Turn these ages into their equivalent main sequence turnoff masses. AGB
    // only start after a few 10s of Myr, so we can do a quick check to exit
    // early if we don't have AGB at all
    if (age < 10E6){
        return;
    }
    log_Z = log10(star_total_metallicity);
    m_turnoff_now  = pow(10, mlf_early_times(log10(age), log_Z));
    m_turnoff_next = pow(10, mlf_early_times(log10(age + dt), log_Z));

    // get stellar mass in code masses
    m_initial = star_initial_mass[ipart];

    if (m_turnoff_next < AGB_detailed.max_mass){ // will AGB start in the next timestep?
        // The variables returned by the core AGB function are:
        // C, N, O, Mg, S, Ca, Fe, metals, total
        // for a total of 9 items. We don't use the detailed enrichment indices
        // since there are a few elements that aren't included in the tables, 
        // (S, Ca, Fe) and the detailed enrichment indices are for the 
        // original tables. We include those extra elements here so need our
        // own set of indices
        double agb_yields[9];  
        #ifdef ENRICHMENT_ELEMENTS
        enrichment_AGB_core(agb_yields, m_turnoff_now, m_turnoff_next, 
                            m_initial, star_total_metallicity, 
                            star_metallicity_S[ipart], 
                            star_metallicity_Ca[ipart], 
                            star_metallicity_Fe[ipart]);
        #else  /* ENRICHMENT_ELEMENTS */ 
        // without detailed enrichment, the S, Ca, and Fe yields won't be used
        // so we can just put zeros there for simplicity.
        enrichment_AGB_core(agb_yields, m_turnoff_now, m_turnoff_next, 
                            m_initial, star_total_metallicity, 0, 0, 0);
        #endif /* ENRICHMENT_ELEMENTS */

        // Add all the ejecta back
        putback_stellar_mass_detailed(level, cell, ipart,
                                      agb_yields[8], 0, 0, // total, SNII, SNIa
                                      agb_yields[7], agb_yields[0], // AGB, C
                                      agb_yields[1], agb_yields[2], // N, O
                                      agb_yields[3], agb_yields[4], // Mg, S
                                      agb_yields[5], agb_yields[6]); // Ca, Fe
    }
}
#endif /* not PY_TESTING */

void enrichment_AGB_core(double *return_ejecta, double m_turnoff_now, 
                         double m_turnoff_next, double m_star, 
                         double metallicity_total, double metallicity_s, 
                         double metallicity_ca, double metallicity_fe){
    // The variables returned by this function are:
    // C, N, O, Mg, S, Ca, Fe, metals, total
    // for a total of 9 items

    // First integrate the IMF to get the number of AGB stars dying in this 
    // time interval. If the time interval partially includes AGB, we need to
    // make sure the limits include only AGB. 
    double n_agb;
    if (m_turnoff_next > AGB_detailed.max_mass){
        n_agb = 0;
    }
    else if (m_turnoff_now > AGB_detailed.max_mass){
        n_agb = m_star * imf_integral(m_turnoff_next, AGB_detailed.max_mass);
    }
    else{
        n_agb = m_star * imf_integral(m_turnoff_next, m_turnoff_now);
    }

    // Turn these into densities
    n_agb *= AGB_detailed_code.inv_vol;
    
    // have a temporary yields variable
    double yields[9];
    // The AGB model used will be the mean mass between the stars dying
    // at the beginning and end of this timestep, consistent with what is done
    // for SN
    double agb_mass = 0.5 * (m_turnoff_next + m_turnoff_now);
    if (agb_mass > AGB_detailed.max_mass){
        agb_mass = AGB_detailed.max_mass;
    }
    #ifndef PY_TESTING
    cart_assert(m_turnoff_next <= AGB_detailed.max_mass);
    #endif /* not PY_TESTING */
    get_yields_agb(metallicity_total, agb_mass, yields);

    // We now have the yields, and can turn them into what will actually 
    // be output. First we multiply what we have by the number of AGB stars to 
    // get the total yields of those elements
    int idx_field;
    for (idx_field = 0; idx_field < 9; idx_field++) {
        yields[idx_field] *= n_agb;
    } 
    // This array has the following quantities in it: C, N, O, Mg, 
    // metals other than Fe, Ca, S, and all ejecta. We can directly store the
    // ones that are directly calculated.
    return_ejecta[0] = yields[detail_field_idx_agb_C];
    return_ejecta[1] = yields[detail_field_idx_agb_N];
    return_ejecta[2] = yields[detail_field_idx_agb_O];
    return_ejecta[3] = yields[detail_field_idx_agb_Mg];
    return_ejecta[8] = yields[detail_field_idx_agb_total];

    // The other elements are not modified in AGB, so their ejecta is the
    // total ejecta times the existing fraction
    double s_ejecta  = yields[detail_field_idx_agb_total] * metallicity_s;
    double ca_ejecta = yields[detail_field_idx_agb_total] * metallicity_ca;
    double fe_ejecta = yields[detail_field_idx_agb_total] * metallicity_fe;
    // The metals variable didn't include these, so we should add them
    yields[detail_field_idx_agb_metals] += s_ejecta + ca_ejecta + fe_ejecta;

    // Then we can store them in the return variables
    return_ejecta[4] = s_ejecta;
    return_ejecta[5] = ca_ejecta;
    return_ejecta[6] = fe_ejecta;
    return_ejecta[7] = yields[detail_field_idx_agb_metals];
    // 8 is total ejecta and was done 15 lines above
}

#ifdef PY_TESTING
double *get_ejecta_agb_py(double m_turnoff_now, double m_turnoff_next, 
                          double m_star, double z_tot, double metallicity_s, 
                          double metallicity_ca, double metallicity_fe){
    static double return_yields[10]; 
    // length doesn't matter since python can't tell where it ends
    enrichment_AGB_core(return_yields, m_turnoff_now, m_turnoff_next, m_star, 
                        z_tot, metallicity_s, metallicity_ca, metallicity_fe);
    return return_yields;
} 
#endif /* PY_TESTING */

#endif /* STAR_FORMATION */


