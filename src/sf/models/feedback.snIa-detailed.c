/**
 * Note that the feedback.detailed-enrich.c file has much of the functionality
 * that this uses. For this to work correctly, make sure both the 
 * detailed_enrichment_init() and detailed_enrichment_setup() functions are 
 * used in the appropriate sections of the sf_feedback.*.c file being used.
 */
#ifndef PY_TESTING
#include "config.h"
#endif /* PY_TESTING */

#ifdef STAR_FORMATION

#ifndef PY_TESTING
#include "hydro.h"              // gas sound speed, temperature
#include "auxiliary.h"          // local_proc_id
#include "control_parameter.h"  // parameter parsing
#include "cosmology.h"          // tphys_from_tcode
#include "imf.h"                // stellar lifetimes
#include "parallel.h"           // MASTER_NODE
#include "starformation.h"      // star particle arrays
#include "tree.h"               // gas density functions
#include "units.h"              // units
#include "rand.h"               // random number generator
#include "times.h"              // abox (used in debugging)
#include "feedback.SNR-model.h" // SNR feedback model
#include "feedback.kinetic.h"   // distribute_momentum
#include "hydro_sgst.h"         // gas turbulence properties
#include "plugin.h"             // Plugins allowed in feedback
#endif /* PY_TESTING */
#include "feedback.detailed_enrich.h"
#include <math.h>
/*
//  Type Ia supernova feedback, where explosions are discretized.
*/

extern double dUfact;
extern double feedback_temperature_ceiling;
#ifdef SGST_SN_SOURCE
extern double feedback_turbulence_temperature_ceiling;
#endif /* SGST_SN_SOURCE */

// get the fixed metallicity if we aren't using enrichment
extern float fixed_metallicity;

// Get the needed attributes from the detailed enrichment file.
extern int detail_n_fields_sn_ia;
extern int detail_field_idx_sn_ia_C;
extern int detail_field_idx_sn_ia_N;
extern int detail_field_idx_sn_ia_O;
extern int detail_field_idx_sn_ia_Mg;
extern int detail_field_idx_sn_ia_S;
extern int detail_field_idx_sn_ia_Ca;
extern int detail_field_idx_sn_ia_Fe;
extern int detail_field_idx_sn_ia_metals;

// function prototypes
void feedback_sn_ia_core(double*, double, double, double, double, double, double);
double power_law_SNIa_DTD_integral(double, double, double);

/*
 * The SN Ia structure holds different attributes depending on which flags
 * are enabled. 
 * energy_per_explosion: Used to be called E_51, and is the average energy per 
 *                       SN Ia, in units of 10^51 ergs.
 * number_SNIa_per_mass: The number of SN Ia that occur in a 1 M_sun SSP over 
 *                       a Hubble time. This is often measured observationally.
 */
struct
{
    double energy_per_explosion;
    double number_SNIa_per_mass;
}
// The default value for the number of SN Ia per unit mass SSP comes from
// Maoz and Graur 2017. We use the value for field galaxies from Table 3. 
snIa_detailed_phys = {2.0E51, 1.6E-3};

// Also make a structure for code mass quantities, but initialize them with the
// physical values. This is done so that in my Python tests, where the code
// masses aren't actually set, using these values will return physical units
struct
{
    double energy_per_explosion;
    double number_SNIa_per_mass;
    double inv_vol; // cell inverse volume
    double log_10_m_start; 
    // When calculating the lifetime of an 8 solar mass star, we need the log of
    // 8, and I don't want to calculate that every time.
}
snIa_detailed_code = {2E51, 1.6E-3, 1, 0.9030899869919435};


double power_law_SNIa_DTD_integral(double age, double dt, double t_start){
    /**
     * Get the SN Ia per unit mass of an SSP in a given timestep.
     * 
     * The function chosen is a power law of index -1.13. This was measured
     * observationally by Maoz and Graur 2017 and is the measurement for
     * field galaxies in Table 3. This power law is applied at all ages SN Ia
     * are active. A good choice for the time to start is when the 8 M_sun star
     * dies (roughly 40 Myr), since that will be the first white dwarf produced.
     * At earlier times the rate is zero.
     * 
     * This is normalized to integrate to snIa.number_SNIa at a Hubble time,
     * assuming SNIa start when a 8 M_sun model leaves the MS. 
     * The normalization is described more here:
     * http://nbviewer.jupyter.org/github/gillenbrown/Tabulation/blob/master/notebooks/sn_Ia.ipynb
     * 
     * This analytically integrates that DTD in the given timestep.
     * 
     * Parameters
     * ----------
     * age: Age of the stellar population in years.
     * dt: Timestep length, such that the timestep goes from age to age+dt
     * t_start: Time at which SNIa start happening.
     * 
     * Returns
     * -------
     * The number of SN Ia of the SSP per unit mass at this age. Has units of 
     * number / (code masses). Multiplying by the initial mass of the 
     * stellar population gives the true SN Ia number.
     */
    double t_end, norm;
    t_end = age + dt;
    if (t_end < t_start){
        return 0;
    }
    // if we got here we are beyond the lifetime of the 8 M_sun star, and so 
    // we do have SN Ia
    // If SN start in this timestep, they start at t_start. We'll use age as 
    // the lower limit in the integration, so we replace it if needed
    if (age < t_start){
        age = t_start;
    }
    // 2.3 comes from the notebook, 0.13 comes from integration
    norm = snIa_detailed_code.number_SNIa_per_mass * 2.3480851917 / 0.13;
    return norm * (pow(age, -0.13) - pow(t_end, -0.13));
}

#ifndef PY_TESTING
void snIa_detailed_config_init()
{
    control_parameter_add3(control_parameter_double,&snIa_detailed_phys.energy_per_explosion,"snIa:energy-per-explosion","snIa.energy_per_explosion","e_51","average energy per type Ia supernova explosion, in 1e51 ergs.");
    control_parameter_add2(control_parameter_double, &snIa_detailed_phys.number_SNIa_per_mass, "snIa:number-SNIa", "snIa.number_SNIa", "The number of SN Ia that occur in a 1 M_sun SSP over a Hubble time. This is often measured observationally.");

    // double m_start;
    // control_parameter_add2(control_parameter_double, &m_start, "snIa:start-mass", "snIa.start_mass", "This controls the time at which SNIa start. At the time when this mass (in solar masses) leaves the main sequence, SNIa will start. 8.0 is the default value, as this is when SNII end and white dwarfs are first produced. Note that this is not checked against the SNII:min-mass parameter to ensure consistency.");
    // snIa_detailed_code.log_10_m_start = log10(m_start);
}


void snIa_detailed_config_verify()
{
    /*
    //  type Ia supernova feedback
    */
    VERIFY(snIa:energy-per-explosion, !(snIa_detailed_phys.energy_per_explosion < 0.0) );
    VERIFY(snIa:number-SNIa, !(snIa_detailed_phys.number_SNIa_per_mass < 0.0) );
}

void snIa_detailed_init()
{
    // If the user passed in a value for energy, we need to add back in the 1E51 
    // ergs. The default value already includes this, so we do a check for an 
    // unnaturaly small value, and add back the 1E51
    if (snIa_detailed_phys.energy_per_explosion < 100){
        snIa_detailed_phys.energy_per_explosion *= 1E51;
    }

    // diagnostics
    if(local_proc_id == MASTER_NODE)
    {
        cart_debug("SNIa: %.3e explosions per Msun with %.2e erg each, start when the %g Msun star leaves the main sequence",
                   snIa_detailed_phys.number_SNIa_per_mass,
                   snIa_detailed_phys.energy_per_explosion,
                   pow(10, snIa_detailed_code.log_10_m_start));
    }
}

void snIa_detailed_setup(int level)
{
    // Create a constant that can be used to turn a value in solar masses into 
    // a value in code masses, and ergs into code energy.  
    // Multiply a number in solar masses by this to turn it to code mass
    // or divide for code masses to solar masses. Parenthesis keep the order
    // of operations consistent when using division.
    double Msol_to_code_mass = constants->Msun / units->mass;
    double erg_to_code_energy = constants->erg / units->energy;

    // We want to convert the normalization for the SNIa rate into code masses,
    // rather than solar masses, as it was originally defined. 
    snIa_detailed_code.number_SNIa_per_mass = snIa_detailed_phys.number_SNIa_per_mass / Msol_to_code_mass;
    snIa_detailed_code.inv_vol = cell_volume_inverse[level];
    snIa_detailed_code.energy_per_explosion = snIa_detailed_phys.energy_per_explosion * erg_to_code_energy;
}


void snIa_thermal_feedback_detailed(int level, int cell, int ipart, 
                                    double t_next)
{
    /**
     * Do the feedback from SNIa in a given timestep. 
     * 
     * This is the main function that should be called by the parent feedback
     * routine. It includes the energy and metals injected by SNIa.
     * 
     * Supernovae can be discrete in this implementation, and we implement it 
     * as follows. In each timestep, we use the SNIa DTD to get the SNIa rate.
     * We then multiply by the timestep to get the number of SNIa in this 
     * timestep, where this value can be fractional. We add this to the number 
     * of leftover unexploded supernovae from previous timesteps. If this 
     * exceeds 1, we explode a supernova. It it does not exceed 1, we add the
     * fractional number of supernovae from this timestep to the counter, so 
     * that these unexploded supernovae are not thrown away, and will go 
     * off later.
     * 
     * Note that much of the work here is handled by the yields_sn_ia function.
     */
    double star_total_metallicity, energy_added, t_start; 
    double dteff, dU, dU_turb;
    double t, tn, dt, age, tb;
    // Get time information. These are all in years.
    t = tphys_from_tcode(particle_t[ipart]);
    tn = tphys_from_tcode(t_next);
    dt = tn - t;

    tb = tphys_from_tcode(star_tbirth[ipart]);
    #ifdef CLUSTER
    double t_ave = tphys_from_tcode(star_tbirth[ipart] + star_ave_age[ipart]);
    age = t - t_ave;
    #else /* CLUSTER */
    age = t - tb;
    #endif /* CLUSTER  */
    // don't need to check for negative ages, since we do a direct age check
    // below and negative ages won't pass that condition. 

    // and it's total metallicity
    #ifdef ENRICHMENT
    star_total_metallicity = star_metallicity_II[ipart];
    #ifdef ENRICHMENT_SNIa
    star_total_metallicity += star_metallicity_Ia[ipart];
    #endif /* ENRICHMENT_SNIa */
    #ifdef ENRICHMENT_ELEMENTS
    star_total_metallicity += star_metallicity_AGB[ipart];
    #endif /* ENRICHMENT_ELEMENTS */
    #else /* ENRICHMENT */
    star_total_metallicity = fixed_metallicity;
    #endif /* ENRICHMENT */

    // We say SNIa start when the 8 solar mass model leaves the MS. Get that
    // time
    t_start = pow(10, tlf(snIa_detailed_code.log_10_m_start, 
                          log10(star_total_metallicity)));

    if (age > t_start){
        // initialize arrays to store yields
        double sn_ejecta[detail_n_fields_sn_ia+2];  
        // all elements plus energy and unexploded SN

        // then get the quantities for this timestep. This function handles the 
        // heavy lifting for SNIa. 
        #ifdef DISCRETE_SN
        feedback_sn_ia_core(sn_ejecta, star_unexploded_sn[ipart], age, dt, 
                            star_initial_mass[ipart], star_total_metallicity, 
                            t_start);
        #else /* DISCRETE_SN */
        // with continuous SN we never have unexploded ones
        feedback_sn_ia_core(sn_ejecta, 0, age, dt, 
                            star_initial_mass[ipart], star_total_metallicity, 
                            t_start);
        #endif /* DISCRETE_SN */

        // Put the ejected material into the cells
        putback_stellar_mass_detailed(level, cell, ipart,
                                      sn_ejecta[detail_field_idx_sn_ia_metals],
                                      0, // SNII
                                      sn_ejecta[detail_field_idx_sn_ia_metals], 
                                      0, // AGB
                                      sn_ejecta[detail_field_idx_sn_ia_C],
                                      sn_ejecta[detail_field_idx_sn_ia_N],
                                      sn_ejecta[detail_field_idx_sn_ia_O],
                                      sn_ejecta[detail_field_idx_sn_ia_Mg],
                                      sn_ejecta[detail_field_idx_sn_ia_S],
                                      sn_ejecta[detail_field_idx_sn_ia_Ca],
                                      sn_ejecta[detail_field_idx_sn_ia_Fe]);

        energy_added = sn_ejecta[detail_n_fields_sn_ia];

        // increment the SN counter if we have discrete SN
        #ifdef DISCRETE_SN
        star_unexploded_sn[ipart] = sn_ejecta[detail_n_fields_sn_ia+1];
        #endif /* DISCRETE_SN */

        // Then do the feedback. This was copied from feedback.snIa.c
        // the MIN here caps the feedback to be at the maximum temperature 
        // allowed, essentially. See def of dUfact
        if (energy_added > 0){
            dU = MIN(energy_added,dUfact*cell_gas_density(cell));
        #ifdef SGST_SN_SOURCE
            dU_turb = sgst_model.fraction_from_SN*dU;
            dU_turb = MIN(  dU_turb,
                    MAX( 0.0,
                            (feedback_turbulence_temperature_ceiling / units->temperature)
                            * cell_gas_density(cell)
                            / ((sgs_turbulence_gamma-1)*constants->wmu)
                            - cell_gas_turbulent_energy(cell)
                    )  );

            cell_gas_turbulent_energy(cell) += dU_turb;
            cell_gas_energy(cell) += dU_turb;
            dU -= dU_turb;
        #endif /* SGST_SN_SOURCE */

            /* limit energy release and don't allow to explode in hot bubble */
            if(units->temperature*cell_gas_temperature(cell) < feedback_temperature_ceiling)
            {
                cell_gas_energy(cell) += dU;
                cell_gas_internal_energy(cell) += dU;
                cell_gas_pressure(cell) += dU*(cell_gas_gamma(cell)-1);
            }
        }
    }
}
#endif /* not PY_TESTING */

void feedback_sn_ia_core(double *yields, double unexploded_sn, double age, 
                         double dt, double m_star, double z, double t_start)
{
    /**
     * Calculate the feedback that should be injected from SNIa in a given 
     * timestep. This includes both metals and energy. 
     * 
     * Note that function doesn't access any ART variables, so that it can be
     * compiled outside of ART and used in Python tests. 
     * 
     * Parameters
     * ----------
     * yields: Array of doubles. The ejecta will be stored in this 
     *         variable rather than being directly returned. See below for what
     *         is stored here.
     * unexploded_sn: the value in the star_unexploded_sn stellar variable, 
     *                indicating the fractional number of SNIa that could
     *                have exploded by now, but that haven't because this
     *                counter hasn't reached 1.
     * age: Age of the progenitor stellar population, in years.
     * dt: Length of the timestep, in years.
     * m_star: Initial mass of the progenitor stellar population.
     * z: Metallicity of the progenitor stellar population.
     * t_start: Time at which SNIa start. This should be the lifetime of the 
     *          8 M_sun star since that was used to normalize the SNIa rates.
     * 
     * Returns
     * -------
     * Nothing, but the ejected masses are stored in the `yields` 
     * variable and can be accessed in the original scope. 
     * Even if ENRICHMENT_ELEMENTS is not used, it will contain the ejected  
     * metal density for all elements (as described in the 
     * feedback.detailed_enrich.c file), energy density injected by SNIa 
     * (energy divided by cell volume), and finally the number of leftover 
     * unexploded SN Ia. 
     * 
     * If ENRICHMENT_ELEMENTS is not used the unneeded variables can be 
     * unused in the parent function. I do it this way to capture the evolution
     * of the metal mass ejected with progenitor metallicity.
     * 
     * The units here were chosen so that these quantities can be directly 
     * added to cells. 
     */
    double number_sn_ia, sn_density, new_unexploded_sn, n_sn_this_timestep;
    int idx_field;
    double energy_this_timestep;

    // Integrate the DTD to get the number per unit mass in this timestep, then
    // multiply by the stellar mass to get the number in this timestep.
    number_sn_ia = power_law_SNIa_DTD_integral(age, dt, t_start) * m_star;
    // Add the previously unexploded SN
    number_sn_ia += unexploded_sn;

    // The actual number of discretized SN is the floor of this number, while 
    // the new unexploded number is the remainder
    #ifdef DISCRETE_SN
    n_sn_this_timestep = floor(number_sn_ia);
    new_unexploded_sn = number_sn_ia - n_sn_this_timestep;
    #else /* DISCRETE_SN */
    n_sn_this_timestep = number_sn_ia;
    new_unexploded_sn = 0;
    #endif /* DISCRETE_SN */

    // if we don't have any supernovae, we just return zeros, but with 
    // different lengths depending on what defines are used
    if (n_sn_this_timestep == 0){
        for (idx_field = 0; idx_field < detail_n_fields_sn_ia; idx_field++) {
            yields[idx_field] = 0;
        }
        // then add energy and leftover_sn manually
        yields[detail_n_fields_sn_ia] = 0;
        yields[detail_n_fields_sn_ia+1] = new_unexploded_sn;
        return;
    }
    // If we made it here we will have supernovae
    // similarly since we're going to be getting the yields, which eventually
    // need to end up as metal densities, we can turn the number of supernovae
    // into a supernova density, so that when we multiply this by the yields
    // we get the metal density like we want
    sn_density = n_sn_this_timestep * snIa_detailed_code.inv_vol;
    // The energy is easy to determine.
    energy_this_timestep = sn_density * snIa_detailed_code.energy_per_explosion;
    
    // Then we can get the yields for the number of SN we have
    // Fill the array sn_ejecta with the masses of the given elements
    // ejected by a single SN Ia explosion, in code masses.
    double single_sn_ejecta[detail_n_fields_sn_ia]; 
    get_yields_sn_ia(z, single_sn_ejecta);  
    for (idx_field = 0; idx_field < detail_n_fields_sn_ia; idx_field++) {
        yields[idx_field] = sn_density * single_sn_ejecta[idx_field];
    }
    // then add energy and leftover_sn manually
    yields[detail_n_fields_sn_ia] = energy_this_timestep;
    yields[detail_n_fields_sn_ia+1] = new_unexploded_sn;
}

// functions used for python testing
#ifdef PY_TESTING
double *sn_ia_core_py(double unexploded_sn, double age, double dt, 
                      double m_star, double z, double t_start){
    static double return_yields[20];
    // length doesn't matter, since python can't tell where it ends anyway
    feedback_sn_ia_core(return_yields, unexploded_sn, age, dt, m_star, 
                        z, t_start);
    return return_yields;
}

double get_sn_ia_number_py(double age, double dt, double t_start){
    return power_law_SNIa_DTD_integral(age, dt, t_start);
}
#endif

#endif /* STAR_FORMATION */