#ifndef __FEEDBACK_AGB_DETAILED__
#define __FEEDBACK_AGB_DETAILED__

// functions used externally
#ifdef STAR_FORMATION
#ifndef PY_TESTING

void AGB_detailed_config_init();
void AGB_detailed_config_verify();
void AGB_detailed_init();
void AGB_detailed_setup(int);
void AGB_enrichment(int, int, int, double);

#endif /* PY_TESTING */
#endif /* STAR_FORMATION */

#ifdef PY_TESTING
double *get_ejecta_agb_py(double, double, double, double, double, double, double);
#endif /* PY_TESTING */
#endif  /* Header guard */