#ifndef __FEEDBACK_DETAIL_ENRICH__
#define __FEEDBACK_DETAIL_ENRICH__

// functions used externally
void detailed_enrichment_config_init(void);
void detailed_enrichment_config_verify(void);
void detailed_enrichment_init(void);
void detailed_enrichment_setup(int);
void putback_stellar_mass_detailed(int, int, int, double, double, double, 
                                   double, double, double, double, double, 
                                   double, double, double);
double get_cumulative_mass_winds(double, double, double, double);
void get_yields_sn_ia(double, double*);
void get_yields_agb(double, double, double*);
void get_yields_sn_ii(double, double, double*);
void get_yields_hn_ii(double, double, double*);
void debug_enrichment(int, char*, double, char*, double);
double imf_integral(double, double);
double interpolate(double, double, double, double, double);
double extrapolate(double, double, double);

// functions used in my Python tests
#ifdef PY_TESTING
int *find_z_bound_idxs_agb_py(double);
int *find_z_bound_idxs_winds_py(double);
int *find_z_bound_idxs_sn_ii_py(double);
int *find_z_bound_idxs_sn_ia_py(double);
int guess_mass_idx_winds_py(double);
int *find_mass_bound_idxs_agb_py(double);
int *find_mass_bound_idxs_sn_ii_py(double);
int *find_mass_bound_idxs_hn_ii_py(double);
int *find_mass_bound_idxs_winds_py(double);
double *get_yields_sn_ia_py(double);
double get_masses_agb(int);
double get_masses_sn_ii(int);
double get_masses_hn_ii(int);
double get_masses_winds(int);
double get_z_winds(int);
double get_z_agb(int);
double get_z_sn_ii(int);
double get_z_sn_ia(int);
double *get_yields_raw_sn_ii_py(double, double);
double *get_yields_raw_hn_ii_py(double, double);
double *get_yields_raw_agb_py(double, double);
double imf_integral_py(double, double);
double interpolate_py(double, double, double, double, double);
double extrapolate_py(double, double, double);
double get_cumulative_mass_winds_py(double, double, double, double);

#endif  /* PY TESTING */
#endif  /* Header guard */
