#ifndef __FEEDBACK_WINDS_H__
#define __FEEDBACK_WINDS_H__

#ifdef STAR_FORMATION
void wind_config_init();
void wind_config_verify();
void wind_init();
void wind_setup(int level);
void stellar_wind_kick(int level,int cell,int ipart,double t_next);
#endif /* STAR_FORMATION */

#endif /* __FEEDBACK_WINDS_H__ */
