#include "config.h"
#if defined(HYDRO) && defined(STAR_FORMATION)

#include <math.h>
#include <string.h>

#include "auxiliary.h"
#include "control_parameter.h"
#include "cosmology.h"
#include "hydro.h"
#include "rand.h"
#include "rt.h"
#include "starformation_recipe.h"
#include "times.h"
#include "tree.h"
#include "units.h"


/*
//  Recipe based on Eq. (17) of GK10, with tau_{SF} = const = 1.5Gyr (a-la Genzel et al 2010 & Bigiel et al)
//  New H2 model, with line overlap
*/
#if defined(ENRICHMENT)

struct 
{
  double efficiency;          /* defined as Sigma_SFR*1.5Gyr/(1.36*Sigma_H2) */
  double variability;
  double fLW;
  double dust_to_gas_break;
  double qden, qfac, qmul;
  /* internal values */
  double factor;
  double Dbreak;
#ifndef RADIATIVE_TRANSFER
  double U_MW;                /* value to use without RT */
#endif /* RADIATIVE_TRANSFER */
}
  sfr = { 1.0, 0.0, 1, 0.05, 10.0, 1, 1, 0.0, 0.0
#ifndef RADIATIVE_TRANSFER
	  , 1.0
#endif /* RADIATIVE_TRANSFER */
  };


double CaimanFactorSF(int cell)
{
  if(sfr.qfac > 1.0)
    {
      double q = units->number_density*0.76*cell_gas_density(cell)/sfr.qden;
      return sfr.qmul*(sfr.qfac+q)/(1+q);
    }
  else return 1.0;
}

double CaimanFactorFB(int cell)
{
  if(sfr.qfac > 1.0)
    {
      double q = units->number_density*0.76*cell_gas_density(cell)/sfr.qden;
      return sfr.qmul*(1+sfr.qfac*q)/(1+q);
    }
  else return 1.0;
}


void CaimanSetW(const char *value, void *ptr, int ind)
{
  if(strncmp(value,"hr",2)==0 || strncmp(value,"HR",2)==0)
    {
      sfr.dust_to_gas_break = 0.05;
      sfr.qden = 10;
      sfr.qfac = 3;
      sfr.qmul = 1;
    }     
  if(strncmp(value,"mr",2)==0 || strncmp(value,"MR",2)==0)
    {
      sfr.dust_to_gas_break = 0.0;
      sfr.qden = 10;
      sfr.qfac = 10;
      sfr.qmul = 0.3;
    }
}


void CaimanListW(FILE *stream, const void *ptr)
{
}


void sfr_config_init()
{
  ControlParameterOps w = { CaimanSetW, CaimanListW };

  control_parameter_add2(control_parameter_double,&sfr.efficiency,"sf:efficiency","sfr.efficiency","the relative efficiency of the star formation law (relative to the constant depletion time-scale of 1.5 Gyr).");

  control_parameter_add2(control_parameter_double,&sfr.variability,"sf:variability","sfr.variability","the variability of the efficiency. If <sf:variability> is greater than 0, then it serves as a dispersion of a lognormal distribution with which the actual SF efficiency fluctuates around <sf:efficiency> (i.e. <sf:efficiency> remains the mean of the distribution).");

  control_parameter_add2(control_parameter_double,&sfr.fLW,"sf:fLW","sfr.fLW","an extra factor to boost the LW flux.");

  control_parameter_add(w,&sfr.factor,"%sf:caiman:weak-convergence-mode","Choice of the weak convergence correction factor (MR or HR).");

  control_parameter_add2(control_parameter_double,&sfr.dust_to_gas_break,"sf:dust-to-gas-break","sfr.dust_to_gas_break","the value of break in the dust-to-gas ratio at z=9 to use in computing the H2 formation rate when RT_CHEMISTRY is unset; in that case fitting formulae of GD14 are used, and they assume rapid H2 formation, which will not be true for low metallicity environments at early times. In these environments the formation of H2 is suppressed if the dust-to-gas ratio is lower than the break value. If that value is 0, then no limit is imposed (i.e. just GD14 is used).");

  control_parameter_add(control_parameter_double,&sfr.qden,"@cai1:qden","internal value.");
  control_parameter_add(control_parameter_double,&sfr.qfac,"@cai1:qfac","internal value.");
  control_parameter_add(control_parameter_double,&sfr.qmul,"@cai1:qmul","internal value.");

#ifndef RADIATIVE_TRANSFER
  control_parameter_add2(control_parameter_double,&sfr.U_MW,"sf:U_MW","sfr.U_MW","A constant value for the U_MW to use without RT.");
#endif /* RADIATIVE_TRANSFER */
}


void sfr_config_verify()
{
  VERIFY(sf:efficiency, sfr.efficiency > 0.0 );
  VERIFY(sf:variability, !(sfr.variability < 0.0) );
  VERIFY(sf:fLW, sfr.fLW > 0.0 );
  VERIFY(%sf:caiman:weak-convergence-mode, 1 );
  VERIFY(sf:dust-to-gas-break, !(sfr.dust_to_gas_break < 0.0) );
  VERIFY(@cai1:qden, sfr.qden > 0.0 );
  VERIFY(@cai1:qfac, sfr.qfac > 0.0 );
  VERIFY(@cai1:qmul, sfr.qmul > 0.0 );
#ifndef RADIATIVE_TRANSFER
  VERIFY(sf:U_MW, sfr.U_MW > 0 );
#endif /* RADIATIVE_TRANSFER */
}


void sfr_setup(int level)
{
  sfr.factor = sfr.efficiency*units->time/(1.5*constants->Gyr);
#ifdef RADIATIVE_TRANSFER
  sfr.Dbreak = sfr.dust_to_gas_break/pow(10*auni[level],3);
#else
  sfr.Dbreak = 0;
#endif
}


double fracH2_Caiman(int cell);

double sfr_rate(int cell)
{
#if defined(RADIATIVE_TRANSFER) && defined(RT_CHEMISTRY)
  /*
  //  Factor 1.36 is what observers add.
  */
  double fracH2 = 2*cell_H2_density(cell)/(0.76*cell_gas_density(cell));
#else

  double fracH2 = fracH2_Caiman(cell);

#endif

  fracH2 *= CaimanFactorSF(cell);

  if(sfr.variability > 0.0)
    {
      return sfr.factor*fracH2*cell_gas_density(cell)*cart_rand_lognormal(sfr.variability);
    }
  else
    {
      return sfr.factor*fracH2*cell_gas_density(cell);
    }
}


struct StarFormationRecipe sf_recipe_internal =
{
  sfr_rate,
  sfr_config_init,
  sfr_config_verify,
  sfr_setup
};

#else

#error "SF Recipe line-in-H2 only works with RADIATIVE_TRANSFER and ENRICHMENT activated."

#endif /* RADIATIVE_TRANSFER && ENRICHMENT */

#endif /* HYDRO && STAR_FORMATION */

double fracH2_Caiman(int cell)
{
#ifdef RADIATIVE_TRANSFER
  double U = rtUmwFS(cell);
  double D = sfr.fLW*rtDmwFL(cell);
#else
  double U = sfr.fLW*sfr.U_MW;
  double D = cell_gas_metal_density(cell)/(constants->Zsun*cell_gas_density(cell));
#endif /* RADIATIVE_TRANSFER */
  double D0 = 0.17;
  double g = sqrt(D*D+D0*D0);
  double q = sqrt(0.01+U);
  double SigmaR1 = 50/g*q/(1+0.69*q);
  double alp = 0.5 + 1/(1+sqrt(U*D*D/600));
  double rhoH = 0.76*cell_gas_density(cell);
  double SigmaH = 2*units->density*units->length/constants->Msun*constants->pc*constants->pc*rhoH*cell_sobolev_length(cell); /* 2 because Lsob is one-sided, Sigma = 2 rho Lsob */
  double R = pow(SigmaH/SigmaR1,alp);

  if(sfr.Dbreak > 0)
    {
      /*
      //  Reduce the fraction in low D environments
      */
      double q = sqrt(D/sfr.Dbreak);
      R *= q/(1+q);
    }

  return R/(1+R);
}
