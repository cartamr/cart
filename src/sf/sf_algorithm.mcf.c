#include "config.h"
#if defined(HYDRO) && defined(STAR_FORMATION)

#include <math.h>
#include <stdio.h>

#include "auxiliary.h"
#include "control_parameter.h"
#include "hydro.h"
#include "particle.h"
#include "rand.h"
#include "starformation.h"
#include "starformation_algorithm.h"
#include "tree.h"
#include "units.h"

#include "models/algorithm.mcf.h"
#include "models/algorithm.stargrowth.h"


/*
//  This algorithm governs creation of mgc particles and accretion of mass onto them.
//  All molecular gas is collapsed into particles if the molecular gas fraction 
//  exceeds the specified threshold.
*/


double sf_min_stellar_particle_mass = 0.0;   /* in Msun; used to be called dm_star_min */
double mstar_min;


void sfa_config_init()
{
    control_parameter_add4(control_parameter_double,&sf_min_stellar_particle_mass,"sf:min-stellar-particle-mass","sf_min_stellar_particle_mass","sf_min_stellar_particle_mass","dm_star_min","minimum mass for a newly created stellar particle, in solar masses. This value should be small enough to avoid artifically boosting the SFR in the low density gas.");
}


void sfa_config_verify()
{
    VERIFY(sf:min-stellar-particle-mass, !(sf_min_stellar_particle_mass < 0.0) );
}


void sfa_setup(int level)
{
    mstar_min = sf_min_stellar_particle_mass*constants->Msun/units->mass; 
}


void sfa_create(int level, int cell, double dtl, double dt_eff, float SFR)
{
    int ipart; 
    float dmstar;

    double fH2sf = mcf->fH2sf;
    if(mcf->fH2sfSlope != 0)
    {
        double SigmaGas = 2*units->density*units->length/constants->Msun*constants->pc*constants->pc*cell_gas_density(cell)*cell_sobolev_length(cell);
        fH2sf *= pow(SigmaGas/10,mcf->fH2sfSlope);
    }
    
    if(cart_rand() > fH2sf) return;
    
    dmstar = SFR*cell_gas_density(cell)*cell_volume[level]; /* sfr = fH2 for this method, all molecular gas is collapsed to mc particles */
       
    if(dmstar > 0.0)
    {
        /*
        //  Seed or Grow? Find youngest mc particle in cell to accrete onto, 
        //  but here it just serves to check if it exists. Growth handled in 
        //  feedback to be more consistent with ODE evolution times.
        */
        ipart = mcf_find_local_cloud(cell);
        /* create the new star */
        if(ipart == -1)
        {
            /*
            //  Seed, no prior MGCs
            */
            if(dmstar > mstar_min)
            {
                /*
                //  Allow for accretion of small masses onto existing particles, 
                //  but prevent creation of small particles.
                */
                ipart = create_star_particle2(cell,dmstar,dtl,STAR_TYPE_NORMAL,0.0);
                dmstar = 0;
            }
        }
        else
        {
			grow_star_particle2(ipart,dmstar,cell,level,0);
        }
    }
}

struct StarFormationAlgorithm sf_algorithm_internal = 
  {
    sfa_create,
    sfa_config_init,
    sfa_config_verify,
    NULL,
    sfa_setup
  };

#endif /* HYDRO && STAR_FORMATION */
