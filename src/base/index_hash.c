#include "config.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/types.h>

#include "auxiliary.h"
#include "index_hash.h"
#include "rand.h"

const index_hash_entry null_entry = { -1, -1 };

/*
//  Hash sfc_t -> int
//  Used as root_hash: check parallel.c
//  Can query but cannot get the whole list. For list use skiplist
*/

/*******************************************************
 * next_largest_prime
 *******************************************************/
int64_t next_largest_prime64( int64_t count )
/* purpose: calculates the next largest prime by brute
 *          force, used since primes make good hash sizes
 *          (keeps the number of hash collisions low)
 * returns: the next prime larger than count, or 3 if count <= 2
 */
{
	int64_t i;
	int is_prime;
	int64_t next_prime;

	if ( count > 2 ) {
		next_prime = ( count % 2 == 0 ) ? count + 1: count + 2;
	} else {
		next_prime = 3;
	}

	do {
		is_prime = 1;

		for ( i = 3; i <= sqrt(next_prime); i += 2 ) {
			if ( next_prime % i == 0 ) {
				is_prime = 0;
				next_prime += 2;
				break;
			}
		}
	} while ( !is_prime );

	return next_prime;
}

/*******************************************************
 * index_hash_create
 *******************************************************/
index_hash *index_hash_create( int size, sfc_t max_key, sfc_t *remote_index, int *local_index )
/* purpose: allocates space and initializes a new
 *          index hash which will contain at most size
 *          entries
 *
 * returns: a pointer to the newly allocated index hash
 */
{
	int i, j, k, h;
	int oldj;
	index_hash *hash;
	index_sub_hash *sub_hash;
	int iter = 0;
	int min_iter;
	int rekey;
	index_hash_entry tmp_entry, new_entry;
	int64_t a0, b0, min_a0, min_b0;
	int64_t total, min_total;

	/* allocate space for the new hash */
	hash = cart_alloc( index_hash, 1 );

	/* create universal hash function for first layer */
	hash->hash_size = size;
	hash->p = next_largest_prime64( 3*max_key );

	hash->sub_hashes = cart_alloc(index_sub_hash, hash->hash_size);

	min_total = 0;

	do {
		/* choose a first-level hash function */
		a0 = hash->p*(int64_t)cart_rand() + 1;
		b0 = hash->p*(int64_t)cart_rand();

		for ( i = 0; i < size; i++ ) {
			hash->sub_hashes[i].n = 0;
		}

		/* hash all values into sub hashes */
		for ( i = 0; i < size; i++ ) {
			h = (((int64_t)remote_index[i]*a0 + b0) % hash->p) % hash->hash_size;
			hash->sub_hashes[h].n++;
		}

		total = 0;
		for ( i = 0; i < size; i++ ) {
			hash->sub_hashes[i].n *= hash->sub_hashes[i].n;
			total += hash->sub_hashes[i].n;
		}

		if ( min_total == 0 || total < min_total ) {
			min_total = total;
			min_a0 = a0;
			min_b0 = b0;
		}

		iter++;
	} while ( min_total >= 2*size && iter < 20 );

	hash->a0 = min_a0;
	hash->b0 = min_b0;

	/* recompute sub_hashes n array */
	for ( i = 0; i < size; i++ ) {
		hash->sub_hashes[i].n = 0;
	}

	for ( i = 0; i < size; i++ ) {
		h = (((int64_t)remote_index[i]*hash->a0 + hash->b0) % hash->p) % hash->hash_size;
		hash->sub_hashes[h].n++;
	}

	total = 0;
	for ( i = 0; i < size; i++ ) {
		hash->sub_hashes[i].s = total;
		hash->sub_hashes[i].n *= hash->sub_hashes[i].n;
		total += hash->sub_hashes[i].n;
	}

	/* allocate space for the actual hash array */
	hash->hash_array = cart_alloc(index_hash_entry, total);

	for ( i = 0; i < total; i++ ) {
		hash->hash_array[i] = null_entry;
	}

	for ( i = 0; i < size; i++ ) {
		hash->sub_hashes[i].n = 0;
	}

	for ( i = 0; i < size; i++ ) {
		h = (((int64_t)remote_index[i]*hash->a0 + hash->b0) % hash->p) % hash->hash_size;
		sub_hash = &(hash->sub_hashes[h]);
		hash->hash_array[ sub_hash->s + sub_hash->n ].remote_index = remote_index[i];
		hash->hash_array[ sub_hash->s + sub_hash->n ].local_index = local_index[i];
		sub_hash->n++;
	}

	for ( i = 0; i < size; i++ ) {
		hash->sub_hashes[i].n *= hash->sub_hashes[i].n;
	}

	/* hash each secondary array */
#ifdef OPENMP_DECLARE_CONST
	#pragma omp parallel for default(none) shared(hash,null_entry,size) private(i,j,k,oldj,rekey,tmp_entry,new_entry,sub_hash) schedule(dynamic)
#else
	#pragma omp parallel for default(none) shared(hash,size) private(i,j,k,oldj,rekey,tmp_entry,new_entry,sub_hash) schedule(dynamic)
#endif
	for ( i = 0; i < size; i++ ) {
		sub_hash = &(hash->sub_hashes[i]);

		do {
			rekey = 0;

			/* choose secondary hash function */
			sub_hash->a = ((int64_t)hash->p*cart_rand()) + 1;
			sub_hash->b = ((int64_t)hash->p*cart_rand());

			for ( j = 0; j < sub_hash->n; j++ ) {
				if ( hash->hash_array[ sub_hash->s + j ].remote_index != -1 ) {
					/* find new hash location */
					new_entry = hash->hash_array[ sub_hash->s + j ];
					hash->hash_array[ sub_hash->s + j ] = null_entry;
					oldj = j;

					while (1) {
						k = ((sub_hash->a*(int64_t)new_entry.remote_index + sub_hash->b) % hash->p) % sub_hash->n;
						if ( hash->hash_array[ sub_hash->s + k ].remote_index == -1 ) {
							hash->hash_array[ sub_hash->s + k ] = new_entry;
							break;
						} else if ( k > oldj ) {
							tmp_entry = hash->hash_array[ sub_hash->s + k ];
							hash->hash_array[ sub_hash->s + k ] = new_entry;
							new_entry = tmp_entry;
							oldj = k;
						} else {
							/* unresolvable collision, need to rekey, put new_entry in first open spot */
							for ( k = 0; k < sub_hash->n; k++ ) {
								if ( hash->hash_array[ sub_hash->s + k ].remote_index == -1 ) {
								hash->hash_array[ sub_hash->s + k ] = new_entry;
									break;
								}
							}
							rekey = 1;
							break;
						}
					}
				}

				if ( rekey ) {
					break;
				}
			}
		} while ( j < sub_hash->n );
	}

	return hash;
}

/*******************************************************
 * index_hash_lookup
 *******************************************************/
int index_hash_lookup ( index_hash *hash, sfc_t remote_index )
/* purpose: finds the local_index corresponding to the
 *          given remote index in the given index hash.
 *
 * returns: the corresponding local index or -1 if the
 *          remote index was not found.
 */
{
	int h, k;
	index_sub_hash *sub_hash;

	if ( remote_index != INDEX_HASH_NULL_ENTRY ) {
		h = (((int64_t)remote_index*hash->a0 + hash->b0) % hash->p) % hash->hash_size;

		sub_hash = &(hash->sub_hashes[h]);
		if ( sub_hash->n > 0 ) {
			k = ((sub_hash->a*(int64_t)remote_index+sub_hash->b) % hash->p) % sub_hash->n;
			if ( hash->hash_array[ sub_hash->s + k ].remote_index == remote_index ) {
				return hash->hash_array[ sub_hash->s + k ].local_index;
			}
		}
	}

	return INDEX_HASH_NULL_ENTRY;
}

void index_hash_free( index_hash *hash ) {
	cart_assert( hash != NULL );

	if ( hash->hash_size > 0 ) {
		cart_free( hash->sub_hashes );
		cart_free( hash->hash_array );
	}

	cart_free( hash );
}
