#ifndef __SFC_H__
#define __SFC_H__

#ifndef CONFIGURED
#error "Missing config.h include."
#endif

#include <mpi.h>
#include <stdint.h>

#define SLAB     0
#define MORTON   1
#define HILBERT  2

#ifndef SFC
#define SFC      HILBERT
#else
#if SFC == SLAB
#ifndef SLAB_DIM
#define SLAB_DIM 1
#endif
#endif
#endif

#if num_root_grid_refinements > 10
typedef int64_t sfc_t;
#ifdef MPI_INT64_T
#define MPI_SFC     MPI_INT64_T
#else
#define MPI_SFC     MPI_LONG_LONG_INT
#endif
#else
typedef int sfc_t;
#define MPI_SFC     MPI_INT
#endif

#define nBitsPerDim     num_root_grid_refinements
#define nBits           (nDim * nBitsPerDim)
#define max_sfc_index   ((sfc_t)1<<nBits)

#define rollLeft(x,y,mask) ((x<<y) | (x>>(nDim-y))) & mask
#define rollRight(x,y,mask) ((x>>y) | (x<<(nDim-y))) & mask

sfc_t sfc_index_position( double position[nDim] );
sfc_t sfc_index( int coords[nDim] );
void sfc_coords( sfc_t index, int coords[nDim] );

#endif
