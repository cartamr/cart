#ifndef __INDEX_HASH_H__
#define __INDEX_HASH_H__

#include "sfc.h"

#include <sys/types.h>

#define INDEX_HASH_NULL_ENTRY		(-1)

typedef struct INDEX_HASH_ENTRY {
	int local_index;
	sfc_t remote_index;
} index_hash_entry;

typedef struct INDEX_SUB_HASH {
	int64_t a;
	int64_t b;
	int64_t s;
	int n;
} index_sub_hash;

typedef struct INDEX_HASH {
	int hash_size;
	int64_t a0, b0;
	int64_t p;
	index_sub_hash *sub_hashes;
	index_hash_entry *hash_array;
} index_hash;

index_hash *index_hash_create( int size, sfc_t max_key,
		sfc_t *remote_index, int *local_index );
void index_hash_free( index_hash *hash );
int index_hash_lookup ( index_hash *hash, sfc_t remote_index );

#endif /* __INDEX_HASH_H__ */
