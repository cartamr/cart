#ifndef __TIMES_H__
#define __TIMES_H__

#ifndef CONFIGURED
#error "Missing config.h include."
#endif

DECLARE_LEVEL_ARRAY(double,tl);

#ifdef COSMOLOGY
DECLARE_LEVEL_ARRAY(double,abox);
DECLARE_LEVEL_ARRAY(double,auni);
#endif /* COSMOLOGY */

void config_init_times(void);
void config_verify_times(void);

extern int step;
extern int time_enabled;

#ifdef COSMOLOGY
extern double auni_init;
extern double auni_end;
#else
extern double t_init;
extern double t_end;
#endif /* COSMOLOGY */

void simulation_time_init(void);

extern int max_level_for_collectives;

#endif /* __TIMES_H__ */
