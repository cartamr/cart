#ifndef __COMPILER_H__
#define __COMPILER_H__

#if defined(__GNUC__) && !defined(__INTEL_COMPILER) && !defined(_CRAYC)
#define COMPILER_GCC
#define GCC_VERSION (__GNUC__ * 10000 \
                     + __GNUC_MINOR__ * 100 \
                     + __GNUC_PATCHLEVEL__)
#elif defined(__INTEL_COMPILER)
#define COMPILER_INTEL
#elif defined(__PGIC__)
#define COMPILER_PORTLAND
#elif defined(__xlc__) || defined(__xlC__)
#define COMPILER_XLC
#elif defined(_CRAYC)
#define COMPILER_CRAY
#endif

#ifndef NO_COMMENTS_IN_HEADER
/*
// Some versions of GCC segfault when presented with OpenMP collapse clause, force them to be serial
*/
#endif
#if defined(COMPILER_GCC) && GCC_VERSION < 40200
#define OPENMP_NO_COLLAPSE_CLAUSE
#define OPENMP_PROBLEMATIC_CONSTRUCT
#endif

#ifndef NO_COMMENTS_IN_HEADER
/*
// Most compilers expect all symbols to be listed as private or shared when default(none) is used in an OpenMP
// pragma. GCC refuses to accept variables that are predetermined shared by virtue of being declared constant.
// This is apparently according to OpenMP spec v4.0 2.14.1.1:
//    "Variables with predetermined data-sharing attributes may not be listed in data-sharing
//     attribute clause"
// However only OpenMP 3.1 lists const-qualified type as having predetermined data-sharing attributes 
// (see v3.1 2.9.1.1) so that may change in future.
// 
// See also:
// http://openmp.org/forum/viewtopic.php?f=9&t=1109&p=4471&hilit=const#p4471
// http://cactuscode.org/pipermail/users/2012-November/003274.html
*/
#endif
#ifndef COMPILER_GCC
#define OPENMP_DECLARE_CONST
#endif

#ifndef NO_COMMENTS_IN_HEADER
/*
//  C++ compatibility: INT64_MAX is defined only if __STDC_LIMIT_MACROS set explicitly (ISO C99 standard)
*/
#endif
#ifdef __cplusplus
#define __STDC_LIMIT_MACROS
#endif

#ifndef NO_COMMENTS_IN_HEADER
/*
// Ensure mpi.h is included before stdio.h for intelmpi
// e.g. mpicxx.h:93:2: error: #error "SEEK_SET is #defined but must not be for the C++ binding of MPI. Include mpi.h before stdio.h"
*/
#endif
#ifdef CXX_MPI_STDIO_CONFLICT
#include <mpi.h>
#endif

#endif 
