/************************************
*                                   *
*  ALWAYS RUN                       *
*  > python cell_probe_generate.py  *
*  AFTER YOU MODIFY THIS FILE       *
*                                   *
************************************/
#include "config.h"


#include <math.h>


#include "auxiliary.h"
#include "cosmology.h"
#include "halos.h"
#include "hydro.h"
#include "parallel.h"
#include "particle.h"
#include "rt.h"
#include "starformation_recipe.h"
#include "tree.h"
#include "units.h"

#include "frt/frt_c.h"

#include "../drivers/start_analysis.h"

#include "../extra/21cm.h"

extern float pressure_floor_factor;


#include "cell_probe.h"


NEW_CELL_PROBE( task, "current MPI task", 0 )
{
  return local_proc_id;
}

NEW_CELL_PROBE( level, "refinement level", 0 )
{
  return cell_level(cell);
}

#ifdef COSMOLOGY
NEW_CELL_PROBE( distance_to_parent_halo, "distance to the center of the parent halo, as a fraction of the halo radius (or -1 if this cell does not belong to any halo)", 0 )
{
  if(data==NULL || data->HaloList==NULL || data->HaloList->map==NULL || data->HaloList->map[cell]==0)
    {
      return -1;
    }
  else
    {
      double x[3];
      cell_center_position(cell,x);
      const halo *h = data->HaloList->list + data->HaloList->map[cell]-1;
  
      return compute_distance_periodic(x,h->pos)/h->rhalo;
    }
}

NEW_CELL_PROBE( parent_halo_mass, "virial mass of a parent halo, in Msun (or 0 if this cell does not belong to any halo)", 0 )
{
  if(data==NULL || data->HaloList==NULL || data->HaloList->map==NULL || data->HaloList->map[cell]==0)
    {
      return 0;
    }
  else
    {
      const halo *h = data->HaloList->list + data->HaloList->map[cell]-1;
      return h->mvir*units->mass/constants->Msun;
    }
}
#endif /* COSMOLOGY */

#ifdef GRAVITY
NEW_CELL_PROBE( total_density, "total density (g/cm^3)", 0 )
{
  return units->density*cell_total_density(cell);
}
#endif /* GRAVITY */

#if defined(STAR_FORMATION)
#if defined(GRAVITY)
NEW_CELL_PROBE( stellar_density, "stellar density (g/cm^3)", 0 )
{
  return units->density*cell_stellar_density(cell);
}
#endif /* GRAVITY */

NEW_CELL_PROBE( star_formation_rate, "star formation rate (Msun/pc^3/yr)", 0 )
{
  return units->density/units->time*sf_recipe->rate(cell)/(constants->Msun/constants->pc/constants->pc/constants->pc/constants->yr);
}
#endif /* STAR_FORMATION */

#ifdef HYDRO
NEW_CELL_PROBE( gas_density, "gas density (g/cm^3)", 0 )
{
  return units->density*cell_gas_density(cell);
}

#ifdef COSMOLOGY
NEW_CELL_PROBE( gas_over_density, "gas density in units of the cosmic mean", 0 )
{
  return cell_gas_density(cell)*(cosmology->OmegaM/cosmology->OmegaB);
}
#endif /* COSMOLOGY */

NEW_CELL_PROBE( gas_number_density, "gas number density (cm^{-3})", 0 )
{
  return units->number_density*cell_gas_density(cell);
}

NEW_CELL_PROBE( temperature, "temperature (K)", 2 )
{
  return units->temperature*cell_gas_temperature(cell);
}

#if defined(COSMOLOGY) && defined(RADIATIVE_TRANSFER)
NEW_CELL_PROBE( brightness_temperature_Z, "21cm brightness temperature, with observing direction along the Z axis (K)", 0 )
{
  return ext21CellTb(cell,2,0);
}
#endif /* COSMOLOGY && RADIATIVE_TRANSFER */

NEW_CELL_PROBE( gas_velocity_X, "gas velocity along X direction (km/s)", 2 )
{
  return cell_momentum(cell,0)/cell_gas_density(cell)*units->velocity/constants->kms;
}

NEW_CELL_PROBE( gas_velocity_Y, "gas velocity along Y direction (km/s)", 2 )
{
  return cell_momentum(cell,1)/cell_gas_density(cell)*units->velocity/constants->kms;
}

NEW_CELL_PROBE( gas_velocity_Z, "gas velocity along Z direction (km/s)", 2 )
{
  return cell_momentum(cell,2)/cell_gas_density(cell)*units->velocity/constants->kms;
}

NEW_CELL_PROBE( baryon_column_density, "baryon column density (cm^{-2})", 0 )
{
  return cell_gas_density(cell)*cell_sobolev_length2(cell,level,NULL)*units->number_density*units->length;
}

NEW_CELL_PROBE( thermal_pressure, "thermal pressure (erg/cm^3)", 0 )
{
  return units->energy_density*(cell_gas_gamma(cell)-1)*cell_gas_internal_energy(cell);
}

NEW_CELL_PROBE( pressure_floor, "pressure_floor (erg/cm^3)", 0 )
{
  return pressure_floor_factor * constants->G*pow(units->density*units->length*cell_gas_density(cell)*cell_size[level],2.0);
}

NEW_CELL_PROBE( rotational_velocity, "rotational velocity (km/s)", 3 )
{
  if(data==NULL || data->Pos==NULL || data->Vel==NULL)
    {
      return -1;
    }
  else
    {
      int j;
      double vr, r2, v2;
      double dr[nDim];
      float dv[nDim];
      float H0 = 100.0*constants->kms/constants->Mpc*units->time;

      cell_center_position(cell,dr);
      compute_displacement_periodic(data->Pos,dr,dr);
      for(j=0; j<nDim; j++)
	{
	  dv[j] = cell_momentum(cell,j)/cell_gas_density(cell) - data->Vel[j] + H0*dr[j];
	}

      for(vr=r2=v2=0.0, j=0; j<nDim; j++)
	{
	  r2 += dr[j]*dr[j];
	  vr += dr[j]*dv[j];
	  v2 += dv[j]*dv[j];
	}

      return sqrt(v2-vr*vr/(1.0e-10+r2))*units->velocity/constants->kms;
    }
}

NEW_CELL_PROBE( local_gas_overdensity, "gas density in this cell over the average density in neighboring cells", 2 )
{
  float s;
  int i, nb[num_neighbors];

  cell_all_neighbors(cell,nb);
  //
  //  Mean of all neighbors
  //
  s = 0.0;
  for(i=0; i<num_neighbors; i++)
    {
      s += cell_gas_density(nb[i]);
    }
  s /= num_neighbors;
  return cell_gas_density(cell)/s;
}
#endif /* HYDRO */

#if defined(HYDRO) && defined(ENRICHMENT)
NEW_CELL_PROBE( gas_metallicity, "gas metallicity (solar units)", 2 )
{
  return cell_gas_metal_density(cell)/(constants->Zsun*cell_gas_density(cell));
}
#endif /* HYDRO && ENRICHMENT */


#if defined(HYDRO) && defined(RADIATIVE_TRANSFER)
NEW_CELL_PROBE( HI_fraction, "HI fraction", 2 )
{
  return cell_HI_fraction(cell);
}

NEW_CELL_PROBE( HII_fraction, "HII fraction", 2 )
{
  return cell_HII_fraction(cell);
}

NEW_CELL_PROBE( HeI_fraction, "HeI fraction", 2 )
{
  return cell_HeI_fraction(cell);
}

NEW_CELL_PROBE( HeII_fraction, "HeII fraction", 2 )
{
  return cell_HeII_fraction(cell);
}

NEW_CELL_PROBE( HeIII_fraction, "HeIII fraction", 2 )
{
  return cell_HeIII_fraction(cell);
}

NEW_CELL_PROBE( H2_fraction, "H2 fraction", 2 )
{
#ifdef RT_CHEMISTRY
  return cell_H2_fraction(cell);
#else
  double D0 = 0.17;
  double D = rtDmwFL(cell);
  double U = rtUmwFS(cell);
  double g = sqrt(D*D+D0*D0);
  double s = pow(0.001+0.1*U,0.7);
  double alpha = 1 + 0.7*sqrt(s)/(1+s);
  double SigmaR1 = 40/g*s/(1+s);
  double SigmaGas = 2*units->density*units->length/constants->Msun*constants->pc*constants->pc*cell_gas_density(cell)*cell_sobolev_length(cell); /* 2 because Lsob is one-sided, Sigma = 2 rho Lsob */
  double q = pow(SigmaGas/SigmaR1,alpha);
  double R = q*(1+0.25*q)/1.25;
  return R/(1+R);
#endif
}

NEW_CELL_PROBE( HI_number_density, "HI number density", 0 )
{
  return units->number_density*cell_HI_density(cell);
}

NEW_CELL_PROBE( HII_number_density, "HII number density", 0 )
{
  return units->number_density*cell_HII_density(cell);
}

NEW_CELL_PROBE( HeI_number_density, "HeI number density", 0 )
{
  return units->number_density*cell_HeI_density(cell);
}

NEW_CELL_PROBE( HeII_number_density, "HeII number density", 0 )
{
  return units->number_density*cell_HeII_density(cell);
}

NEW_CELL_PROBE( HeIII_number_density, "HeIII number density", 0 )
{
  return units->number_density*cell_HeIII_density(cell);
}

NEW_CELL_PROBE( H2_number_density, "H2 number density", 0 )
{
  return units->number_density*cell_H2_density(cell);
}

NEW_CELL_PROBE( dust_to_gas_ratio, "dust-to-gas ratio in MW units (D_{MW})", 2 )
{
  return rtDmw(cell);
}

NEW_CELL_PROBE( interstellar_radiation_field, "actual radiation field in MW units)", 0 )
{
  return rtUmw(cell);
}

NEW_CELL_PROBE( interstellar_radiation_fieldFS, "free-space radiation field in MW units (U_{MW})", 0 )
{
  return rtUmwFS(cell);
}

NEW_CELL_PROBE( normalized_interstellar_radiation_field, "radiation field over the background field", 0 )
{
  return rtUmw(cell)/rtUmw(-1);
}

NEW_CELL_PROBE( cooling_rate, "cooling rate per baryon in ergs/s", 2 )
{
  float cfun, hfun;
  rtGetCoolingRate(cell,&cfun,&hfun);
  return cfun;
}

NEW_CELL_PROBE( heating_rate, "heating rate per baryon in ergs/s", 2 )
{
  float cfun, hfun;
  rtGetCoolingRate(cell,&cfun,&hfun);
  return hfun;
}

NEW_CELL_PROBE( HI_ionization_rate, "HI ionization rate (1/s)", 0 )
{
  float rates[FRT_RATE_DIM];
  rtGetPhotoRates(cell,rates);
  return rates[FRT_RATE_IonizationHI];
}

NEW_CELL_PROBE( HI_normalized_ionization_rate, "HI ionization rate over the background rate", 0 )
{
  float rates[FRT_RATE_DIM], ravg;
  rtGetPhotoRates(-1,rates);
  ravg = rates[FRT_RATE_IonizationHI];
  rtGetPhotoRates(cell,rates);
  return rates[FRT_RATE_IonizationHI]/ravg;
}

NEW_CELL_PROBE( radiation_field_HI, "near radiation field at HI ionization edge (cm^{-3})", 0 )
{
  static float wlen[] = { 912 };
  float ngxi;

  rtGetRadiationField(cell,1,wlen,&ngxi);

  return ngxi;
}

NEW_CELL_PROBE( radiation_field_HeI, "near radiation field at HeI ionization edge (cm^{-3})", 0 )
{
  static float wlen[] = { 912/2.5 };
  float ngxi;

  rtGetRadiationField(cell,1,wlen,&ngxi);

  return ngxi;
}

NEW_CELL_PROBE( radiation_field_HeII, "near radiation field at HeII ionization edge (cm^{-3})", 0 )
{
  static float wlen[] = { 912/4 };
  float ngxi;

  rtGetRadiationField(cell,1,wlen,&ngxi);

  return ngxi;
}

NEW_CELL_PROBE( ionizing_luminosity, "ionizing luminosity in units of Lsun", 0 )
{
  return cell_rt_source(cell)*units->energy/units->time/constants->Lsun;
}
#endif /* HYDRO && RADIATIVE_TRANSFER */


