#ifndef __EXT_IFRIT_H__
#define __EXT_IFRIT_H__


#ifndef CONFIGURED
#error "Missing config.h include."
#endif


#include <mpi.h>


struct HALO;
struct CELL_PROBE;


typedef struct IFRIT_CONFIG
{
  MPI_Comm com;
  int max_buffer_size;
  int num_writers;
  int flags;
  int *selection;
}
ifrit_config_t;


#define IFRIT_FLAG_WRITE_MESH                    1
#define IFRIT_FLAG_WRITE_PARTICLES               2
#define IFRIT_FLAG_WRITE_STELLAR_PARTICLES       4
#define IFRIT_FLAG_SPLIT_STELLAR_PARTICLES       8
#define IFRIT_FLAG_MPI_ALLOW_BUFFER_ALIASING    16


void ifritOutputRegion(const char *fileroot, const char *filelabel, int pixel_level, const int nbin[], const double pos[], int nprobes, const struct CELL_PROBE *probes, ifrit_config_t *cfg);


#endif  /* __EXT_IFRIT_H__ */
