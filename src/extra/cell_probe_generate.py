import io
import fileinput


key = 'NEW_CELL_PROBE'
fh = io.open('cell_probe_generated.h','w')
fs = io.open('cell_probe_generated.c','w')
fl = io.open('cell_probe_list.txt','w')

fh.write('/* THIS FILE IS AUTO-GENERATED, DO NOT EDIT! */\n')
fs.write('/* THIS FILE IS AUTO-GENERATED, DO NOT EDIT! */\n')

fh.write('const cell_probe_t** cell_probe_head(void);\n')
fs.write('#include "config.h"\n')
fs.write('#include <stdio.h>\n')
fs.write('#include "cell_probe.h"\n')
fs.write('const cell_probe_t* cell_probe_list_internal[] = {\n')

for line in fileinput.input('cell_probe.c'):
    if(line[0:3]=='#if' or line[0:5]=='#else' or line[0:6]=='#endif'):
        fh.write(line)
        fs.write(line)
    if(line[0:len(key)] == key):
        s = line[len(key):].partition(',')
        fh.write('extern cell_probe_t cp_'+s[0].lstrip('( ')+';\n')
        fs.write('&cp_'+s[0].lstrip('( ')+',\n')
        fl.write('cp_'+s[0].lstrip('( ')+' '*(50-len(s[0]))+s[2][0:-6]+'\n')
        #fl.write('cp_'+s[0].lstrip('( ')+' '*(50-len(s[0]))+s[2].partition(',')[0]+'\n')

fs.write('NULL };\n')
fs.write('const cell_probe_t** cell_probe_head(){ return cell_probe_list_internal; }\n')

fh.close()
fs.close()
fl.close()

