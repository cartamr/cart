/* THIS FILE IS AUTO-GENERATED, DO NOT EDIT! */
const cell_probe_t** cell_probe_head(void);
extern cell_probe_t cp_task;
extern cell_probe_t cp_level;
#ifdef COSMOLOGY
extern cell_probe_t cp_distance_to_parent_halo;
extern cell_probe_t cp_parent_halo_mass;
#endif /* COSMOLOGY */
#ifdef GRAVITY
extern cell_probe_t cp_total_density;
#endif /* GRAVITY */
#if defined(STAR_FORMATION)
#if defined(GRAVITY)
extern cell_probe_t cp_stellar_density;
#endif /* GRAVITY */
extern cell_probe_t cp_star_formation_rate;
#endif /* STAR_FORMATION */
#ifdef HYDRO
extern cell_probe_t cp_gas_density;
#ifdef COSMOLOGY
extern cell_probe_t cp_gas_over_density;
#endif /* COSMOLOGY */
extern cell_probe_t cp_gas_number_density;
extern cell_probe_t cp_temperature;
#if defined(COSMOLOGY) && defined(RADIATIVE_TRANSFER)
extern cell_probe_t cp_brightness_temperature_Z;
#endif /* COSMOLOGY && RADIATIVE_TRANSFER */
extern cell_probe_t cp_gas_velocity_X;
extern cell_probe_t cp_gas_velocity_Y;
extern cell_probe_t cp_gas_velocity_Z;
extern cell_probe_t cp_baryon_column_density;
extern cell_probe_t cp_thermal_pressure;
extern cell_probe_t cp_pressure_floor;
extern cell_probe_t cp_rotational_velocity;
extern cell_probe_t cp_local_gas_overdensity;
#endif /* HYDRO */
#if defined(HYDRO) && defined(ENRICHMENT)
extern cell_probe_t cp_gas_metallicity;
#endif /* HYDRO && ENRICHMENT */
#if defined(HYDRO) && defined(RADIATIVE_TRANSFER)
extern cell_probe_t cp_HI_fraction;
extern cell_probe_t cp_HII_fraction;
extern cell_probe_t cp_HeI_fraction;
extern cell_probe_t cp_HeII_fraction;
extern cell_probe_t cp_HeIII_fraction;
extern cell_probe_t cp_H2_fraction;
#ifdef RT_CHEMISTRY
#else
#endif
extern cell_probe_t cp_HI_number_density;
extern cell_probe_t cp_HII_number_density;
extern cell_probe_t cp_HeI_number_density;
extern cell_probe_t cp_HeII_number_density;
extern cell_probe_t cp_HeIII_number_density;
extern cell_probe_t cp_H2_number_density;
extern cell_probe_t cp_dust_to_gas_ratio;
extern cell_probe_t cp_interstellar_radiation_field;
extern cell_probe_t cp_interstellar_radiation_fieldFS;
extern cell_probe_t cp_normalized_interstellar_radiation_field;
extern cell_probe_t cp_cooling_rate;
extern cell_probe_t cp_heating_rate;
extern cell_probe_t cp_HI_ionization_rate;
extern cell_probe_t cp_HI_normalized_ionization_rate;
extern cell_probe_t cp_radiation_field_HI;
extern cell_probe_t cp_radiation_field_HeI;
extern cell_probe_t cp_radiation_field_HeII;
extern cell_probe_t cp_ionizing_luminosity;
#endif /* HYDRO && RADIATIVE_TRANSFER */
