#ifndef __EXT_SPS_H__
#define __EXT_SPS_H__


#ifndef CONFIGURED
#error "Missing config.h include."
#endif


struct HALO_LIST;


typedef struct SPS_TABLE
{
  int nz, nt;
  float *lfnu, *lz, *lt;
  const char* label;
} sps_table;


sps_table* spsLoadTable(const char *fname, const char *label);
void spsDestroyTable(sps_table *table);
float spsSampleTable(const sps_table *table, float tStar, float zStar);


#if defined(PARTICLES) && defined(STAR_FORMATION)
/*
//  Compute luminosities in a given band from an FSPS table
*/

void extStarLums(int iStar, double tNow, float *mStar, float *tStar, float *zStar, int nbands, const sps_table * const *tables, float *lums);

void extGalLums(const char *fname, const struct HALO_LIST *halos, float fvir, float rmax, float tSFR, const char *spsfile);

#endif /* PARTICLES && STAR_FORMATION */

#endif  /* __EXT_SPS_H__ */
