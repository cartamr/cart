#ifndef __EXT_ISM_H__
#define __EXT_ISM_H__


#ifndef CONFIGURED
#error "Missing config.h include."
#endif


struct HALO;
struct HALO_LIST;
struct CELL_PROBE;


/*
//  Dump ASCII files with cell data, spherically averaged profiles, PDfs etc.
*/

void extDumpLevels(const char *fname, int nprobes, const struct CELL_PROBE *probes, int level1, int level2, struct HALO_LIST *halos, int binary);

void extDumpLevelsLowMemory(const char *fname, int nprobes, const struct CELL_PROBE *probes, int level1, int level2, struct HALO_LIST *halos, int binary);

#ifdef COSMOLOGY
void extDumpHaloProfiles(const char *fname, int nprobes, const struct CELL_PROBE *probes, float rmin, float rmax, int ndex, struct HALO_LIST *halos, int resolution_level, float outer_edge);
#endif

void extDumpPointProfile(const char *fname, int nprobes, const struct CELL_PROBE *probes, float rmin, float rmax, int ndex, double center[3]);

void extDumpPDF(const char *fname, const struct CELL_PROBE probe, float min, float max, int nbins, int level1, int level2, struct HALO_LIST *halos, int inlog);

/* PDF of probe, weighted by probe2; if pprobe2 is NULL, it is identical to extDumpPDF */
void extDumpPDF2(const char *fname, const struct CELL_PROBE probe, const struct CELL_PROBE *pprobe2, float min, float max, int nbins, int level1, int level2, struct HALO_LIST *halos, int inlog);

/*
//  SF law from stellar particles
*/
#if defined(PARTICLES) && defined(STAR_FORMATION)
void extStarFormationLaw(const char *fname, float spatial_scale, float time_scale, float stellar_age_limit, const struct HALO_LIST *halos);
#endif

/*
//  SF law for instantaneous star formation from gas only
*/
#if defined (HYDRO) && defined(STAR_FORMATION)
void extStarFormationLaw2(const char *fname, float spatial_scale, const struct HALO_LIST *halos);
#endif

/*
//  Dump stellar particles for a given halo within a given fraction of Rvir
*/
void extHaloStars(const char *fname, const struct HALO *h, float rmax);

#endif  /* __EXT_ISM_H__ */
