#include "config.h"
#if defined(RADIATIVE_TRANSFER) && defined(STAR_FORMATION) && defined(COSMOLOGY)

#include <math.h>
#include <stdio.h>
#include <string.h>

#include "auxiliary.h"
#include "hydro.h"
#include "iterators.h"
#include "parallel.h"
#include "particle.h"
#include "rt.h"
#include "starformation.h"
#include "times.h"
#include "tree.h"
#include "units.h"

#include "fesc.h"

#include "halo_tools.h"
#include "healpix.h"
#include "los.h"

#include "utils.h"


extern double sf_min_stellar_particle_mass;
extern int current_step_level;


double fe_912A_Integrand(int cell)
{
  return density_HI(cell);
}

double fe_912A_Finalizer(double integral)
{
  return exp(-6.3e-18*integral*units->number_density*units->length);
}


feInfo fe_912A = { fe_912A_Integrand, fe_912A_Finalizer };


typedef struct feLocType
{
  double pos[3];
  double rstart;
  float weight;
}
feLoc;



typedef struct feDataType
{
  double val;
  double len;
}
feData;


int feLOSWorker(int id, int cell, double r1, double r2, losBuffer data, void *pars)
{
  feData *d = (feData *)data.Data;
  feInfo *p = (feInfo *)pars;

  d->val += p->integrand(cell)*(r2-r1);
  d->len += (r2-r1);

  return 0;
}


void feLOSCollector(const losBuffer *result, int num_segments, const losSegment *segments, void *pars)
{
  int i;
  feData *dn, *dr = (feData *)result->Data;

  dr->val = 0.0;
  dr->len = 0.0;

  for(i=0; i<num_segments; i++)
    {
      dn = (feData *)segments[i].Buffer.Data;

      dr->val += dn->val;
      dr->len += dn->len;
    }
}


void feLoopOverLocations(feInfo a, int nside, int ntot, feLoc locs[], double len, double s[])
{
  int npix = 12*nside*nside;
  int i, l;
  feData output[npix];
  double tnext, tprev = -1;
  double *theta, *phi;
  losBuffer *lines;
  losSegment *segments;

  /*
  //  HealPIX is not thread-safe, so we fill in angle arrays serially.
  */
  theta = cart_alloc(double,npix);
  phi = cart_alloc(double,npix);
  for(i=0; i<npix; i++)
    {
      hp_pix2ang_nest(nside,i,theta+i,phi+i);
    }

  /*
  //  Prepare buffers for the LOS traversal
  */
  lines = cart_alloc(losBuffer,npix);
  for(i=0; i<npix; i++)
    {
      lines[i].Size = sizeof(feData);
      lines[i].Data = output + i;
    }

  /*
  //  Create worker segments. We only need to assign buffers, other data are set
  //  inside losTraverseSegment
  */
  segments = cart_alloc(losSegment,npix);
  for(i=0; i<npix; i++)
    {
      segments[i].Buffer.Size = lines[i].Size;
      segments[i].Buffer.Data = cart_alloc(char,lines[i].Size);
      memcpy(segments[i].Buffer.Data,lines[i].Data,lines[i].Size);
    }

  tprev = MPI_Wtime();

  for(l=0; l<ntot; l++)
    {
      s[npix] += locs[l].weight;

      /*
      //  Init LOS buffers
      */
#pragma omp parallel for default(none), private(i), shared(npix,lines,segments,a,output)
      for(i=0; i<npix; i++)
	{
	  output[i].val = output[i].len = 0.0;

	  memcpy(segments[i].Buffer.Data,lines[i].Data,lines[i].Size);
	}


      /*
      //  Inline losTraverseSky(...) for efficiency
      */
#pragma omp parallel for default(none), private(i), shared(l,npix,theta,phi,segments,locs,len,a), schedule(dynamic,1)
      for(i=0; i<npix; i++)
	{
	  losTraverseSegment(i,locs[l].pos,theta[i],phi[i],locs[l].rstart,len,max_level,feLOSWorker,segments+i,&a);
	}

      losCollectSegments(npix,lines,segments,feLOSCollector,&a);

      for(i=0; i<npix; i++)
	{
	  s[i] += locs[l].weight*a.finalizer(output[i].val);
	}

      tnext = MPI_Wtime();
      if(tnext > tprev+60)
	{
	  tprev = tnext;
	  cart_debug("%d out of %d (%4.1f%)",l,ntot,100.0*l/ntot);
	}
    }

  /*
  // Clean-up
  */
  for(i=0; i<npix; i++)
    {
      if(lines[i].Size > 0) cart_free(segments[i].Buffer.Data);
    }
  cart_free(segments);
  cart_free(lines);

  cart_free(theta);
  cart_free(phi);
}


void feLoopOverParts(const halo *h, float inner_edge, int sample, int *num, feLoc *locs)
{
  int j, l, n;
  float w;
  int i, cell;

  if(locs == NULL)
    {
      for(l=n=i=0; i<num_particles; i++) if(particle_is_star(i) && compute_distance_periodic(particle_x[i],h->pos)<h->rhalo)
	{
	  w = particle_mass[i]*rtSource(i);
	  if(w > 0)
	    {
	      if(l%sample == 0)
		{
		  n++;
		}
	      l++;
	    }
	}
      *num = n;
    }
  else
    {
      for(l=n=i=0; i<num_particles; i++) if(particle_is_star(i) && compute_distance_periodic(particle_x[i],h->pos)<h->rhalo)
	{
	  w = particle_mass[i]*rtSource(i);
	  if(w > 0)
	    {
	      if(l%sample == 0)
		{
		  cell = cell_find_position(particle_x[i]);
		  for(j=0; j<nDim; j++) locs[n].pos[j] = particle_x[i][j];
		  locs[n].rstart = inner_edge*cell_size[cell_level(cell)];
		  locs[n].weight = w;
		  n++;
		}
	      l++;
	    }
	}

      cart_assert(n == *num);
    }
}


void feLoopOverCells(const halo *h, float inner_edge, int sample, int *num, feLoc *locs)
{
  int j, l, n;
  float w;
  double pos[3];
  MESH_RUN_DECLARE(level,cell);

  if(locs == NULL)
    {
      l = n = 0;
      MESH_RUN_OVER_ALL_LEVELS_BEGIN(level);
      MESH_RUN_OVER_CELLS_OF_LEVEL_BEGIN(cell);
      cell_center_position(cell,pos);
      if(cell_is_leaf(cell) && compute_distance_periodic(pos,h->pos)<h->rhalo)
	{
	  w = cell_rt_source(cell)*cell_volume[level];
	  if(w > 0)
	    {
	      if(l%sample == 0)
		{
		  n++;
		}
	      l++;
	    }
	}
      MESH_RUN_OVER_CELLS_OF_LEVEL_END;
      MESH_RUN_OVER_LEVELS_END;

      *num = n;
    }
  else
    {
      l = n = 0;
      MESH_RUN_OVER_ALL_LEVELS_BEGIN(level);
      MESH_RUN_OVER_CELLS_OF_LEVEL_BEGIN(cell);
      cell_center_position(cell,pos);
      if(cell_is_leaf(cell) && compute_distance_periodic(pos,h->pos)<h->rhalo)
	{
	  w = cell_rt_source(cell)*cell_volume[level];
	  if(w > 0)
	    {
	      if(l%sample == 0)
		{
		  for(j=0; j<nDim; j++) locs[n].pos[j] = pos[j];
		  locs[n].rstart = inner_edge*cell_size[level];
		  locs[n].weight = w;
		  n++;
		}
	      l++;
	    }
	}
      MESH_RUN_OVER_CELLS_OF_LEVEL_END;
      MESH_RUN_OVER_LEVELS_END;

      cart_assert(n == *num);
    }
}


void feDoOneHalo(feInfo a, int nside, const halo *h, float outer_edge, float inner_edge, int sample, int sum_over_parts, double fesc[])
{
  int npix = 12*nside*nside;
  int i, n, ntot, *noff, *nloc;
  double sum[npix+1];
  feLoc *locs;

  cart_assert(nside > 0);
  cart_assert(h);

  for(i=0; i<=npix; i++) sum[i] = 0.0;

  if(sum_over_parts)
    {
      feLoopOverParts(h,inner_edge,sample,&n,NULL);
    }
  else
    {
      feLoopOverCells(h,inner_edge,sample,&n,NULL);
    }

  MPI_Allreduce(&n,&ntot,1,MPI_INT,MPI_SUM,mpi.comm.run);

  if(ntot == 0) return;

  noff = cart_alloc(int,num_procs);
  nloc = cart_alloc(int,num_procs);

  if(local_proc_id == MASTER_NODE)
    {
      nloc[0] = n;
      for(i=1; i<num_procs; i++)
	{
	  MPI_Recv(nloc+i,1,MPI_INT,i,0,mpi.comm.run,MPI_STATUS_IGNORE);  
	}

      /*
      // Compute offsets
      */
      noff[0] = 0;
      for(i=1; i<num_procs; i++)
	{
	  noff[i] = noff[i-1] + nloc[i-1]; 
	}
    }
  else
    {
      MPI_Send(&n,1,MPI_INT,MASTER_NODE,0,mpi.comm.run);
    }

  MPI_Bcast(noff,num_procs,MPI_INT,MASTER_NODE,mpi.comm.run);
  MPI_Bcast(nloc,num_procs,MPI_INT,MASTER_NODE,mpi.comm.run);

  cart_assert(n == nloc[local_proc_id]);

  cart_debug("Ray-tracing %d locations...",ntot);
  current_step_level += 3;

  locs = cart_alloc(feLoc,ntot);

  if(sum_over_parts)
    {
      feLoopOverParts(h,inner_edge,sample,&n,locs);
    }
  else
    {
      feLoopOverCells(h,inner_edge,sample,&n,locs);
    }

  cart_assert(n == nloc[local_proc_id]);

  /*
  //  Combine all locations together
  */
  if(local_proc_id == MASTER_NODE)
    {
      for(i=1; i<num_procs; i++)
	{
	  MPI_Recv(locs+noff[i],nloc[i]*sizeof(feLoc),MPI_BYTE,i,0,mpi.comm.run,MPI_STATUS_IGNORE);  
	}
    }
  else
    {
      MPI_Send(locs,n*sizeof(feLoc),MPI_BYTE,MASTER_NODE,0,mpi.comm.run);
    }

  MPI_Bcast(locs,ntot*sizeof(feLoc),MPI_BYTE,MASTER_NODE,mpi.comm.run);

  cart_free(noff);
  cart_free(nloc);

  /*
  //  Loop over all locations
  */
  feLoopOverLocations(a,nside,ntot,locs,outer_edge*h->rhalo,sum);

  cart_free(locs);

  MPI_Allreduce(sum,fesc,npix+1,MPI_DOUBLE,MPI_SUM,mpi.comm.run);

  current_step_level -= 3;
}


/*
// NG: seems like this one still has a bug somewhere...
*/
void extEscapeFraction1(const char *fname, feInfo a, int nside, struct HALO_LIST *halos, int resolution_level, float outer_edge, float inner_edge, int sample, int sum_over_parts)
{
  int npix = 12*nside*nside;
  int i, j, ih;
  int nidx, *idx;
  double fesc[npix+1], sfr, sfr1;
  FILE *f;
  double t;

  cart_assert(sample >= 1);

  if(sum_over_parts)
    {
      rtSetupSource(min_level);
    }

  if(local_proc_id == MASTER_NODE)
    {
      f = fopen(fname,"w");
      cart_assert(f != NULL);

      fprintf(f,"# Columns:\n"); 
      fprintf(f,"# 1. Halo id\n"); 
      fprintf(f,"# 2. Halo mass (Msun)\n"); 
      fprintf(f,"# 3. SFR (Msun/yr)\n"); 
      fprintf(f,"# 4-6. <fesc>, fesc_{10%}, fesc_{90%}\n"); 
      fprintf(f,"#\n"); 
      fflush(f);
    }

  nidx = 0;
  for(j=0; j<num_particles; j++) if(particle_is_star(j) && units->time*(tl[min_level]-star_tbirth[j])<10*constants->Myr)
    {
      nidx++;
    }

  idx = cart_alloc(int,nidx);
  nidx = 0;
  for(j=0; j<num_particles; j++) if(particle_is_star(j) && units->time*(tl[min_level]-star_tbirth[j])<10*constants->Myr)
    {
      idx[nidx++] = j;
    }

  for(ih=0; ih<halos->num_halos; ih++) if(halo_level(&(halos->list[ih]),mpi.comm.run)>=resolution_level)
    {
      t = MPI_Wtime();

      sfr = 0.0;
      for(j=0; j<nidx; j++) if(compute_distance_periodic(particle_x[idx[j]],halos->list[ih].pos)<halos->list[ih].rhalo)
	{
	  sfr += particle_mass[idx[j]];
	}
      sfr1 = units->mass*sfr/constants->Msun/1.0e7;

      MPI_Allreduce(&sfr1,&sfr,1,MPI_DOUBLE,MPI_SUM,mpi.comm.run);

      cart_debug("Halo #%d, SFR = %lg",halos->list[ih].id,sfr);
      if(sfr < 1.0e-10) continue;


      feDoOneHalo(a,nside,&(halos->list[ih]),outer_edge,inner_edge,sample,sum_over_parts,fesc);


      t = MPI_Wtime() - t;

      if(fesc[npix] > 0.0)
	{
	  for(i=0; i<npix; i++) fesc[i] /= fesc[npix];

	  for(i=0; i<npix-1; i++) for(j=i+1; j<npix; j++) if(fesc[i] > fesc[j])
	    {
	      fesc[npix] = fesc[i];
	      fesc[i] = fesc[j];
	      fesc[j] = fesc[npix];
	    }
	  
	  fesc[npix] = 0.0;
	  for(i=0; i<npix; i++) fesc[npix] += fesc[i];
	  fesc[npix] /= npix;

	  cart_debug("Fesc = %le %le %le (%7.1le s)",fesc[npix],fesc[nside*nside],fesc[11*nside*nside],t);

	  if(local_proc_id == MASTER_NODE)
	    {
	      fprintf(f,"%d %le %le %le %le %le\n",halos->list[ih].id,units->mass*halos->list[ih].mvir/constants->Msun,sfr,fesc[npix],fesc[nside*nside],fesc[11*nside*nside]);
	      fflush(f);
	    }
	}
    }

  cart_free(idx);

  if(local_proc_id == MASTER_NODE)
    {
      fclose(f);
    }
}


/*
//  Recursive worker
*/
void fe2RootCell(int cell, double pos[3], double rad, int nside, double dmax[], double rtOT[], double rtHI[], double rtHeI[])
{
  double p[3], dc;
  int level = cell_level(cell);

  cell_center_position(cell,p);
  dc = compute_distance_periodic(p,pos);

  if(fabs(dc-rad) < 0.7*cell_size[level])
    {
      if(cell_is_leaf(cell))
	{
	  {
	    int i, npix = 12*nside*nside;
	    double theta[npix], phi[npix];
	    double d, dp[3];

	    for(i=0; i<nDim; i++)
	      {
		dp[i] = p[i] - pos[i];
		if(dp[i] < -0.5*num_grid) dp[i] += num_grid;
		if(dp[i] >  0.5*num_grid) dp[i] -= num_grid;
	      }

	    for(i=0; i<npix; i++)
	      {
		hp_pix2ang_nest(nside,i,theta+i,phi+i);
		
		d = (sin(theta[i])*(cos(phi[i])*dp[0]+sin(phi[i])*dp[1])+cos(theta[i])*dp[2])/(1.0e-30+dc);
		if(d > dmax[i])
		  {
		    dmax[i] = d;
		    rtOT[i] = cell_var(cell,RT_VAR_OT_FIELD);
		    rtHI[i] = cell_var(cell,rt_field_offset+0);
		    rtHeI[i] = cell_var(cell,rt_field_offset+1);
		  }
	      }
	  }
	}
      else
	{
	  int l;
	  int children[num_children];

	  cell_all_children(cell,children);
	  for(l=0; l<num_children; l++)
	    {
	      fe2RootCell(children[l],pos,rad,nside,dmax,rtOT,rtHI,rtHeI);
	    }
	}
    }
}


void extEscapeFraction2(const char *fname, int nside, struct HALO_LIST *halos, int resolution_level, float outer_edge)
{
  int npix = 12*nside*nside;
  int i, j, ih;
  int nidx, *idx;
  double rtOT[npix], rtHI[npix], rtHeI[npix], dmax[npix], rad;
  double sfr, sfr1;
  FILE *f;
  double t;

  if(local_proc_id == MASTER_NODE)
    {
      f = fopen(fname,"w");
      cart_assert(f != NULL);

      fprintf(f,"# Columns:\n"); 
      fprintf(f,"# 1. Halo id\n"); 
      fprintf(f,"# 2. Halo mass (Msun)\n"); 
      fprintf(f,"# 3. SFR (Msun/yr)\n"); 
      fprintf(f,"# 4-7. <fescHI>, <fescHeI>, <fescHIalt>, <fescHeIalt>\n"); 
      fprintf(f,"#\n"); 
      fflush(f);
    }

  nidx = 0;
  for(j=0; j<num_particles; j++) if(particle_is_star(j) && units->time*(tl[min_level]-star_tbirth[j])<10*constants->Myr)
    {
      nidx++;
    }

  idx = cart_alloc(int,nidx);
  nidx = 0;
  for(j=0; j<num_particles; j++) if(particle_is_star(j) && units->time*(tl[min_level]-star_tbirth[j])<10*constants->Myr)
    {
      idx[nidx++] = j;
    }

  for(ih=0; ih<halos->num_halos; ih++) if(halo_level(&(halos->list[ih]),mpi.comm.run)>=resolution_level)
    {
      t = MPI_Wtime();

      sfr = 0.0;
      for(j=0; j<nidx; j++) if(compute_distance_periodic(particle_x[idx[j]],halos->list[ih].pos)<halos->list[ih].rhalo)
	{
	  sfr += particle_mass[idx[j]];
	}
      sfr1 = units->mass*sfr/constants->Msun/1.0e7;

      MPI_Allreduce(&sfr1,&sfr,1,MPI_DOUBLE,MPI_SUM,mpi.comm.run);

      cart_debug("Halo #%d, SFR = %lg",halos->list[ih].id,sfr);
      if(sfr < 1.0e-10) continue;

      rad = outer_edge*halos->list[ih].rhalo;
      
      for(i=0; i<npix; i++)
	{
	  dmax[i] = rtOT[i] = rtHI[i] = rtHeI[i] = 0;
	}

      for(i=0; i<num_root_cells; i++)
	{
	  fe2RootCell(i,halos->list[ih].pos,rad,nside,dmax,rtOT,rtHI,rtHeI);
	}

      MPI_Reduce(rtOT,rtOT,npix,MPI_DOUBLE,MPI_MAX,MASTER_NODE,mpi.comm.run);
      MPI_Reduce(rtHI,rtHI,npix,MPI_DOUBLE,MPI_MAX,MASTER_NODE,mpi.comm.run);
      MPI_Reduce(rtHeI,rtHeI,npix,MPI_DOUBLE,MPI_MAX,MASTER_NODE,mpi.comm.run);

      t = MPI_Wtime() - t;

      if(local_proc_id == MASTER_NODE)
	{
	  float fesc[4] = { 0, 0, 0, 0 };
	  int imin = 0;
	  double rtOTsum = 0;

	  for(i=0; i<npix; i++)
	    {
	      rtOTsum += rtOT[i];
	      if(rtOT[i] < rtOT[imin]) imin = i;
	    }
	  
	  for(i=0; i<npix; i++)
	    {
	      fesc[0] += rtHI[i];
	      fesc[1] += rtHeI[i];
	      if(rtOT[i] > rtOT[imin])
		{
		  fesc[2] += rtHI[i];
		  fesc[3] += rtHeI[i];
		}
	    }

	  if(rtOTsum > 0)
	    {
	      fesc[0] /= rtOTsum;
	      fesc[1] /= rtOTsum;
	    }

	  rtOTsum -= npix*rtOT[imin];
	  if(rtOTsum > 0)
	    {
	      fesc[2] /= rtOTsum;
	      fesc[3] /= rtOTsum;
	    }

	  fprintf(f,"%d %le %le %le %le %le %le\n",halos->list[ih].id,units->mass*halos->list[ih].mvir/constants->Msun,sfr,fesc[0],fesc[1],fesc[2],fesc[3]);
	  fflush(f);
	}
    }

  cart_free(idx);

  if(local_proc_id == MASTER_NODE)
    {
      fclose(f);
    }
}

#endif /* RADIATIVE_TRANSFER && STAR_FORMATION && COSMOLOGY */

