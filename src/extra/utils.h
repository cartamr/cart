#ifndef __EXT_UTILS_H__
#define __EXT_UTILS_H__


#ifndef CONFIGURED
#error "Missing config.h include."
#endif

struct CELL_PROBE;


/*
//  Miscelaneous helper functions
*/
float find_max_var(int var, double *pos, double dist);
float find_min_var(int var, double *pos, double dist);

float find_local_max_probe(const struct CELL_PROBE probe, double *pos, double dist);
float find_local_min_probe(const struct CELL_PROBE probe, double *pos, double dist);

float find_max_probe(const struct CELL_PROBE probe, double *pos, double dist);
float find_min_probe(const struct CELL_PROBE probe, double *pos, double dist);

void cell_interpolate_at_position(int cell, double pos[], int nvars, int vars[], float vals[]);

#if defined(HYDRO) && defined(RADIATIVE_TRANSFER)
float density_HI(int cell);
float density_H2(int cell);
#endif /* HYDRO && RADIATIVE_TRANSFER */


#endif  /* __EXT_UTILS_H__ */
