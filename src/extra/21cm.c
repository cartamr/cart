#include "config.h"


#include <math.h>
#include <stdio.h>
#include <string.h>


#if defined(COSMOLOGY) && defined(HYDRO) && defined(RADIATIVE_TRANSFER)

#include "auxiliary.h"
#include "cosmology.h"
#include "hydro.h"
#include "iterators.h"
#include "parallel.h"
#include "rt.h"
#include "times.h"
#include "tree.h"
#include "units.h"

#include "utils.h"
#include "../tools/fft/fft3.h"


float ext21CellTs(int cell)
{
  const double T10 = 0.068;    /* K   */
  const double A10 = 2.85e-15; /* 1/s */
  const float wlen[] = { 1216.0 };

  double Tcmb = 2.728/auni[min_level];
  double Tk = cell_gas_temperature(cell)*units->temperature;
  double kHH = 3.1e-11*pow(Tk,0.357)*exp(-32/Tk);                 /* 2006ApJ...637L...1K */
  double kpH = 0.24e-9*sqrt(Tk/(1+Tk/1.6e3*(1+0.03*Tk/1.6e3)));   /* my fit to 2007MNRAS.379..130F */
  double keH = 1.7e-9*pow(4.0e-4+pow(Tk/1.0e4,2.0),0.2);          /* my fit to 2007MNRAS.374..547F */
  double xa, xc;
  float nxi_lya;

  rtGetRadiationField(cell,1,wlen,&nxi_lya);

  xa = T10/(A10*Tcmb)*nxi_lya*3e10*4.5e-18*4/27.0;
  xc = T10/(A10*Tcmb)*(kHH*density_HI(cell)+keH*(cell_HII_density(cell)+cell_HeII_density(cell)+2*cell_HeIII_density(cell))+kpH*cell_HII_density(cell))*units->number_density;

  return Tk*(xa+xc+1)/(xa+xc+Tk/Tcmb);
}

/*
//  Recursively averages over childdren
//
//  mode=0:  full physics
//  mode&1:  no redshift space distortions
//  mode&2:  no lightcone effects
//  mode&4:  no self-absorption
//  mode&8:  Ts = Tk
*/
#define WITH_ZSPACE ((mode&1) == 0)
#define WITH_LTCONE ((mode&2) == 0)
#define WITH_SELFAB ((mode&4) == 0)
#define WITH_TSNETK ((mode&8) == 0)

float ext21CellTb(int cell, int dir, int mode)
{
  const double T10 = 0.068;    /* K   */
  const double A10 = 2.85e-15; /* 1/s */

  cart_assert(dir>=0 && dir<=nDim);

  /*
  //  Prefactors
  */
  double Tcmb = 2.728/auni[min_level];
  double H = Hubble(abox[min_level])*constants->kms/constants->Mpc;
  double fac21 = 3/(32*3.1415926)*pow(21.0,3.0)*A10*T10*units->number_density/H;  /* tau = 3/(32Pi) \lambda10^3 A10 T10/Ts nHI/H(z) - Tozzi et al 2000 */
  double facX2Z = H*units->length/constants->c;

  if(cell_is_leaf(cell))
    {
      float tau, f21, aofx, Ts;

      if(WITH_TSNETK)
	{
	  Ts = ext21CellTs(cell);
	}
      else
	{
	  Ts = cell_gas_temperature(cell)*units->temperature;
	}

      f21 = fac21;
      aofx = abox[min_level];
      if(WITH_LTCONE)
	{
	  float adz;
	  double pos[nDim];
	  
	  cell_center_position(cell,pos);

	  adz = facX2Z*(pos[dir]-0.5*num_grid);
	  f21 *= (1+1.5*adz);
	  aofx *= (1-adz);
	}

      tau = f21*density_HI(cell)/Ts;

      if(WITH_SELFAB)
 	{
	  if(tau>1.0e-2)
	    {
	      return aofx*(Ts-Tcmb)*(1.0-exp(-tau));
	    }
	  else
	    {
	      return aofx*(Ts-Tcmb)*tau*(1-0.5*tau);
	    }
	}
      else
	{
	  return aofx*(Ts-Tcmb)*tau;
	}
    }
  else
    {
      int j;
      int children[num_children];
      float w = 0.0;

      cell_all_children(cell,children);

      for(j=0; j<num_children; j++)
	{
	  w += ext21CellTb(children[j],dir,mode);
	}
     
      return w/num_children;
    }
}


void ext21PowerSpectra(const char *fileroot, int mesh_angl_level, int mesh_freq_level, int dir, int varout, int mode, int floor_level, int ncuts, float *cuts)
{
  const int max_reduce_size = 512*512*515;
  int zoom_angl_factor, zoom_freq_factor, uv1, uv2, zoom_factor[nDim];
  size_t mesh_angl_size, mesh_freq_size, mesh_angl_size2, num_mesh, num_mesh2, len;
  MESH_RUN_DECLARE(level,cell);
  float facV2X, facX2Z;
  double H;
  float facArea, dcut;
  fft_t *local_mesh, *global_mesh;
  int icut, hf = sizeof(size_t);
  char str[1000];
  FILE *f = NULL;
  fft3_plan_t *plan = NULL;

  cart_assert(nDim == 3);
  cart_assert(dir>=0 && dir<nDim);
  cart_assert(varout>=0 && varout<num_vars);
  cart_assert(floor_level>=min_level && floor_level<=max_level);
  cart_assert(ncuts==0 || cuts!=NULL);
  cart_assert(mode >= 0);
  cart_assert(fileroot!=NULL && strlen(fileroot)<990);

  cart_debug("mode=%d ncuts=%d",mode,ncuts);
  if(ncuts > 0)
    {
      int i;
      for(i=0; i<ncuts; i++)
	{
	  cart_debug("cut[%d] = %g",i,cuts[i]);
	}
    }

  /*
  //  Prefactors
  */
  H = Hubble(abox[min_level])*constants->kms/constants->Mpc;
  facV2X = units->velocity/(H*units->length);                 /* aH in code units at the box center, don't forget that units->length are for the physical distance */
  facX2Z = H*units->length/constants->c;

  /*
  //  Prepare the data cube mesh
  */
  zoom_angl_factor = 1 << (mesh_angl_level-min_level);
  zoom_freq_factor = 1 << (mesh_freq_level-min_level);
  mesh_angl_size = num_grid*zoom_angl_factor;
  mesh_freq_size = num_grid*zoom_freq_factor;
  cart_debug("mesh_size = (%ld,%ld) x %ld",mesh_angl_size,mesh_angl_size,mesh_freq_size);

  mesh_angl_size2 = mesh_angl_size + 2;
  num_mesh = mesh_angl_size*mesh_angl_size*mesh_freq_size;
  num_mesh2 = mesh_angl_size2*mesh_angl_size*mesh_freq_size;

  local_mesh = cart_alloc(fft_t,num_mesh2);
  
  uv1 = (dir+1) % 3;
  uv2 = (dir+2) % 3;

  zoom_factor[uv1] = zoom_factor[uv2] = zoom_angl_factor;
  zoom_factor[dir] = zoom_freq_factor;

  /*
  //  Compute brightness temperature along dir and put Tb into varout
  */
  MESH_RUN_OVER_LEVELS_BEGIN(level,min_level,floor_level);
#pragma omp parallel for default(none) private(_Index,cell) shared(_Num_level_cells,_Level_cells,cell_child_oct,dir,varout,cell_vars_data,mode,level,floor_level)
  MESH_RUN_OVER_CELLS_OF_LEVEL_BEGIN(cell);
  
  if(cell_is_leaf(cell) || level==floor_level)
    {
      cell_var(cell,varout) = ext21CellTb(cell,dir,mode);
    }

  MESH_RUN_OVER_CELLS_OF_LEVEL_END;
  MESH_RUN_OVER_LEVELS_END;

  /*
  //  Prepare FFTs
  */
  if(local_proc_id == MASTER_NODE)
    {
      int pads[] = { 0, 0, 0 };
      int dims[nDim], bbox[2*nDim];

      dims[0] = dims[1] = mesh_angl_size;
      dims[2] = mesh_freq_size;

      plan = fft3_new(MPI_COMM_SELF,dims,pads,bbox);

      cart_assert(dims[0] == mesh_angl_size2);

      global_mesh = fft3_allocate_data(plan);
      cart_assert(global_mesh != NULL);

      if((mode&16) > 0)
	{
	  sprintf(str,"%s.bin",fileroot);

	  f = fopen(str,"w");
	  cart_assert(f != NULL);

	  len = 3*sizeof(int);
	  fwrite(&len,hf,1,f);
	  fwrite(&mesh_angl_size,sizeof(int),1,f);
	  fwrite(&mesh_angl_size,sizeof(int),1,f);
	  fwrite(&mesh_freq_size,sizeof(int),1,f);
	  fwrite(&len,hf,1,f);
	}
    }
  else global_mesh = NULL;

  cart_debug("Computing Tb(z)...");

  for(icut=-1; icut<ncuts; icut++)
    {

      memset(local_mesh,0,sizeof(fft_t)*num_mesh2);

      if(icut < 0)
	{
	  dcut = 1.0e35;
	}
      else
	{
	  dcut = cuts[icut]*cosmology->OmegaB/cosmology->OmegaM;
	}

      /*
      //  Now collect local_mesh, making sure to shift <dir> into redshift space.
      //  This is a scatter operation, hence no OpenMP.
      */
      MESH_RUN_OVER_LEVELS_BEGIN(level,min_level,floor_level);

      facArea = cell_volume[level]/(cell_size[mesh_angl_level]*cell_size[mesh_angl_level]*cell_size[mesh_freq_level]);

      MESH_RUN_OVER_CELLS_OF_LEVEL_BEGIN(cell);

      if(cell_gas_density(cell)<dcut && (cell_is_leaf(cell) || level==floor_level))
	{
	  float v2x, w;
	  double pos[nDim], posL[nDim], posR[nDim];
	  int i, j, k, idx1[nDim], idx2[nDim];
	  int ii, jj, kk;

	  cell_center_position(cell,pos);

	  v2x = facV2X;
	  if(WITH_LTCONE)
	    {
	      float adz = facX2Z*(pos[dir]-0.5*num_grid);
	      v2x *= (1+0.5*adz);
	    }

	  for(j=0; j<nDim; j++)
	    {
	      posL[j] = pos[j] - 0.499*cell_size[level];
	      posR[j] = pos[j] + 0.499*cell_size[level];
	    }

	  /*
	  //  pos[dir] is in redshift space
	  */
	  if(WITH_ZSPACE)
	    {
	      int nbL = cell_neighbor(cell,2*dir+0);
	      int nbR = cell_neighbor(cell,2*dir+1);
	      float vc = cell_momentum(cell,dir)/cell_gas_density(cell);
	      posL[dir] += v2x*0.5*(cell_momentum(nbL,dir)/cell_gas_density(nbL)+vc);
	      posR[dir] += v2x*0.5*(cell_momentum(nbR,dir)/cell_gas_density(nbR)+vc);
	    }

	  for(j=0; j<nDim; j++)
	    {
	      idx1[j] = (int)(zoom_factor[j]*MIN(posL[j],posR[j]));
	      idx2[j] = (int)(zoom_factor[j]*MAX(posL[j],posR[j]));
	      cart_assert(idx1[j] <= idx2[j]);
	    }

	  w = facArea/(idx2[0]-idx1[0]+1)/(idx2[1]-idx1[1]+1)/(idx2[2]-idx1[2]+1);

	  for(k=idx1[dir]; k<=idx2[dir]; k++)
	    {
	      kk = (k+3*mesh_freq_size) % mesh_freq_size;
	      cart_assert(kk>=0 && kk<mesh_freq_size);

	      for(j=idx1[uv2]; j<=idx2[uv2]; j++)
		{
		  jj = (j+3*mesh_angl_size) % mesh_angl_size;
		  cart_assert(jj>=0 && jj<mesh_angl_size);

		  for(i=idx1[uv1]; i<=idx2[uv1]; i++)
		    {
		      ii = (i+3*mesh_angl_size) % mesh_angl_size;
		      cart_assert(ii>=0 && ii<mesh_angl_size);

		      local_mesh[ii+mesh_angl_size2*(jj+mesh_angl_size*kk)] += cell_var(cell,varout)*w;
		    }
		}
	    }
	}

      MESH_RUN_OVER_CELLS_OF_LEVEL_END;
      MESH_RUN_OVER_LEVELS_END;

      /*
      //  Split reduce for large mesh size
      */
      if(num_mesh2 > max_reduce_size)
	{
	  int step, nsteps = (num_mesh2+max_reduce_size-1)/max_reduce_size;
	  int size = (num_mesh2+nsteps-1)/nsteps;

          cart_debug("Doing MPI_Reduce in %d steps.",nsteps);

	  for(step=0; step<nsteps; step++)
	    {
	      int num_send = size;
	      int offset = size*step;

	      if(offset+num_send > num_mesh2)
		{
		  num_send = num_mesh2 - offset; 
		}
	      MPI_Reduce(local_mesh+offset,global_mesh+offset,num_send,MPI_TYPE_FFT,MPI_SUM,MASTER_NODE,mpi.comm.run);
	    }
	}
      else
	{
	  MPI_Reduce(local_mesh,global_mesh,num_mesh2,MPI_TYPE_FFT,MPI_SUM,MASTER_NODE,mpi.comm.run);
	}

      /*
      //  Output the data cube in IFrIT format
      */
      if(local_proc_id == MASTER_NODE)
	{
	  int i, j, k, num_bins, *nmod1d, *nmod2d;
	  double TbPos = 0.0, TbNeg = 0.0;
	  double TbRms = 0.0, TbAvg;
	  float *pows1d, *pows2d;
	  float kscale = auni[min_level]/abox[min_level];
	  float kunit = 2*3.1415926/box_size;
	  float punit = pow(box_size,3.0)/num_mesh/num_mesh;
	  FILE *fps;

	  if(f != NULL)
	    {
	      len = sizeof(fft_t)*num_mesh;
	      fwrite(&len,hf,1,f);
	      for(k=0; k<mesh_freq_size; k++)
		{
		  for(j=0; j<mesh_angl_size; j++)
		    {
		      fft_t *ptr = global_mesh + mesh_angl_size2*(j+mesh_angl_size*k);
		      fwrite(ptr,sizeof(fft_t),mesh_angl_size,f);
		    }
		}
	      fwrite(&len,hf,1,f);
	    }

	  /*
	  //  Compute the average
	  */
#pragma openmp parallel for default(none) private (k,j,i) shared(mesh_angl_size,mesh_angl_size2,mesh_freq_size,global_mesh) reduction(+:TbNeg,TbPos,TbRms)
	  for(k=0; k<mesh_freq_size; k++)
	    {
	      for(j=0; j<mesh_angl_size; j++)
		{
		  fft_t *ptr = global_mesh + mesh_angl_size2*(j+mesh_angl_size*k);
		  for(i=0; i<mesh_angl_size; i++)
		    {
		      if(ptr[i] < 0.0)
			{
			  TbNeg += ptr[i];
			}
		      else
			{
			  TbPos += ptr[i];
			}
		      TbRms += ptr[i]*ptr[i];
		    }
		}
	    }
	  TbPos /= num_mesh;
	  TbNeg /= num_mesh;
	  TbAvg = TbPos + TbNeg;
	  TbRms = sqrt(TbRms/num_mesh-TbAvg*TbAvg);
	  cart_debug("dcut=%9.3le <Tb>=%10.3le (%10.3le,%10.3le), RMS(Tb)=%10.3le",dcut,TbAvg,TbPos,TbNeg,TbRms);
	  /* for grepping out of log files */
	  cart_debug("TB21 %6.4lf %10.3le %10.3le",auni[min_level],TbAvg,TbRms);

	  /*
	  //  Compute power spectra
	  */
	  num_bins = mesh_angl_size/2;
	  if(num_bins > mesh_freq_size/2) num_bins = mesh_freq_size/2;

	  pows1d = cart_alloc(float,num_bins);
	  pows2d = cart_alloc(float,num_bins*num_bins);
	  nmod1d = cart_alloc(int,num_bins);
	  nmod2d = cart_alloc(int,num_bins*num_bins);

	  memset(pows1d,0,sizeof(float)*num_bins);
	  memset(pows2d,0,sizeof(float)*num_bins*num_bins);
	  memset(nmod1d,0,sizeof(int)*num_bins);
	  memset(nmod2d,0,sizeof(int)*num_bins*num_bins);

	  fft3_x2k(plan,global_mesh,FFT3_FLAG_WACKY_K_SPACE);

	  for(k=0; k<mesh_freq_size; k++)
	    {
	      float kvel;
	      int lvel;

	      if(k <= mesh_freq_size/2)
		{
		  kvel = kscale*k;
		}
	      else
		{
		  kvel = kscale*(mesh_freq_size-k);
		}

	      lvel = (int)(kvel+0.5);

	      for(j=0; j<mesh_angl_size; j++)
		{
		  float ky2;
		  size_t offset;
		  int jk[2];

		  if(j <= mesh_angl_size/2)
		    {
		      ky2 = j*j;
		    }
		  else
		    {
		      ky2 = (j-mesh_angl_size)*(j-mesh_angl_size);
		    }

		  offset = fft3_jk_index(plan,j,k,jk,FFT3_FLAG_WACKY_K_SPACE);
		  if(offset == (size_t)-1) continue;

		  for(i=0; i<mesh_angl_size2/2; i++)
		    {
		      float ksky, ps;
		      int w, lsky, lbin;
		      
		      ksky = kscale*sqrt(ky2+i*i);
		      lsky = (int)(ksky+0.5);
		      lbin = (int)(sqrt(ksky*ksky+kvel*kvel)+0.5);

		      w = (i==mesh_angl_size/2) ? 1 : 2;

		      ps = 
			global_mesh[2*i+0+offset*mesh_angl_size2]*global_mesh[2*i+0+offset*mesh_angl_size2] +
			global_mesh[2*i+1+offset*mesh_angl_size2]*global_mesh[2*i+1+offset*mesh_angl_size2];

		      if(lbin>=0 && lbin<num_bins)
			{
			  nmod1d[lbin] += w;
			  pows1d[lbin] += w*ps;
			}

		      if(lvel>=0 && lvel<num_bins && lsky>=0 && lsky<num_bins)
			{
			  nmod2d[lsky+num_bins*lvel] += w;
			  pows2d[lsky+num_bins*lvel] += w*ps;
			}
		    }
		}
	    }

	  for(i=0; i<num_bins; i++)
	    {
	      if(nmod1d[i] > 0) pows1d[i] /= nmod1d[i];
	      
	      for(j=0; j<num_bins; j++)
		{
		  if(nmod2d[j+num_bins*i] > 0) pows2d[j+num_bins*i] /= nmod2d[j+num_bins*i];
		}
	    }

	  if(icut < 0)
	    {
	      sprintf(str,"%s.ps1",fileroot);
	    }
	  else
	    {
	      sprintf(str,"%s.ps1.%d",fileroot,icut+1);
	    }

	  fps = fopen(str,"w");
	  cart_assert(fps != NULL);

	  fprintf(fps,"# k P(k) N(k)\n");

	  for(i=0; i<num_bins; i++)
	    {
	      fprintf(fps,"%g %g %d\n",kunit*i,punit*pows1d[i],nmod1d[i]);
	    }

	  fclose(fps);

	  if(icut < 0)
	    {
	      sprintf(str,"%s.ps2",fileroot);
	    }
	  else
	    {
	      sprintf(str,"%s.ps2.%d",fileroot,icut+1);
	    }

	  fps = fopen(str,"w");
	  cart_assert(fps != NULL);

	  fprintf(fps,"# kSky kVel P(k) N(k)\n");
	  for(j=0; j<num_bins; j++)
	    {
	      for(i=0; i<num_bins; i++)
		{
		  fprintf(fps,"%g %g %g %d\n",kunit*i,kunit*j,punit*pows2d[i+num_bins*j],nmod2d[i+num_bins*j]);
		}
	    }
	  
	  fclose(fps);

	  cart_free(pows1d);
	  cart_free(pows2d);
	  cart_free(nmod1d);
	  cart_free(nmod2d);
	}

      MPI_Barrier(mpi.comm.run);
    }

  /*
  //  Done
  */
  if(local_proc_id == MASTER_NODE)
    {
      if(f != NULL) fclose(f);
      cart_free(global_mesh);
      fft3_delete(plan);
    }
  cart_free(local_mesh);

  MPI_Barrier(mpi.comm.run);
}

#endif /* COSMOLOGY && HYDRO && RADIATIVE_TRANSFER */

