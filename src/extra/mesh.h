#ifndef __EXT_MESH_H__
#define __EXT_MESH_H__


#ifndef CONFIGURED
#error "Missing config.h include."
#endif


/*
//  Dump mesh structure (parameter mode changes the file format)
*/
void extDumpMeshStructure(const char *filename, int level_shift);
void extDumpMeshStructure1(const char *filename, int level_shift, int zwidth);
void extDumpMeshStructure2(const char *filename, int level_shift, int zwidth);

#endif  /* __EXT_MESH_H__ */

