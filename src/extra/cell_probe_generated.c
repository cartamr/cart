/* THIS FILE IS AUTO-GENERATED, DO NOT EDIT! */
#include "config.h"
#include <stdio.h>
#include "cell_probe.h"
const cell_probe_t* cell_probe_list_internal[] = {
&cp_task,
&cp_level,
#ifdef COSMOLOGY
&cp_distance_to_parent_halo,
&cp_parent_halo_mass,
#endif /* COSMOLOGY */
#ifdef GRAVITY
&cp_total_density,
#endif /* GRAVITY */
#if defined(STAR_FORMATION)
#if defined(GRAVITY)
&cp_stellar_density,
#endif /* GRAVITY */
&cp_star_formation_rate,
#endif /* STAR_FORMATION */
#ifdef HYDRO
&cp_gas_density,
#ifdef COSMOLOGY
&cp_gas_over_density,
#endif /* COSMOLOGY */
&cp_gas_number_density,
&cp_temperature,
#if defined(COSMOLOGY) && defined(RADIATIVE_TRANSFER)
&cp_brightness_temperature_Z,
#endif /* COSMOLOGY && RADIATIVE_TRANSFER */
&cp_gas_velocity_X,
&cp_gas_velocity_Y,
&cp_gas_velocity_Z,
&cp_baryon_column_density,
&cp_thermal_pressure,
&cp_pressure_floor,
&cp_rotational_velocity,
&cp_local_gas_overdensity,
#endif /* HYDRO */
#if defined(HYDRO) && defined(ENRICHMENT)
&cp_gas_metallicity,
#endif /* HYDRO && ENRICHMENT */
#if defined(HYDRO) && defined(RADIATIVE_TRANSFER)
&cp_HI_fraction,
&cp_HII_fraction,
&cp_HeI_fraction,
&cp_HeII_fraction,
&cp_HeIII_fraction,
&cp_H2_fraction,
#ifdef RT_CHEMISTRY
#else
#endif
&cp_HI_number_density,
&cp_HII_number_density,
&cp_HeI_number_density,
&cp_HeII_number_density,
&cp_HeIII_number_density,
&cp_H2_number_density,
&cp_dust_to_gas_ratio,
&cp_interstellar_radiation_field,
&cp_interstellar_radiation_fieldFS,
&cp_normalized_interstellar_radiation_field,
&cp_cooling_rate,
&cp_heating_rate,
&cp_HI_ionization_rate,
&cp_HI_normalized_ionization_rate,
&cp_radiation_field_HI,
&cp_radiation_field_HeI,
&cp_radiation_field_HeII,
&cp_ionizing_luminosity,
#endif /* HYDRO && RADIATIVE_TRANSFER */
NULL };
const cell_probe_t** cell_probe_head(){ return cell_probe_list_internal; }
