#include "config.h"

#if defined(HYDRO) && defined(GRAVITY) && defined(COSMOLOGY)

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "auxiliary.h"
#include "cell_buffer.h"
#include "control_parameter.h"
#include "cosmology.h"
#include "hydro.h"
#include "hydro_tracer.h"
#include "io.h"
#include "io_cart.h"
#include "iterators.h"
#include "parallel.h"
#include "sfc.h"
#include "skiplist.h"
#include "starformation.h"
#include "times.h"
#include "tree.h"
#include "units.h"
#include "refinement.h"

/* warning: hart format does not retain star_formation_volume or refinement_volume arrays */
/*  this will lead to a <=48byte difference between *.d->*.dh->*.d files */

DECLARE_LEVEL_ARRAY(double,tl_old);
DECLARE_LEVEL_ARRAY(double,dtl);
DECLARE_LEVEL_ARRAY(double,dtl_old);
DECLARE_LEVEL_ARRAY(int,level_sweep_dir);
#ifdef COSMOLOGY
DECLARE_LEVEL_ARRAY(double,abox_old);
#endif /* COSMOLOGY */
extern double auni_init;
extern int step;


/*#define DEBUG*/
/* #define ADDED_NEW_HART_ARRAYS_FOR_IFRIT */
#if (defined(BLASTWAVE_FEEDBACK) || defined(ELECTRON_ION_NONEQUILIBRIUM)) && defined ADDED_NEW_HART_ARRAYS_FOR_IFRIT
#error blastwave and electron ion fields are not recognized in HART or IFrIT by default, if you want them written alter IFrIT IO and define ADDED_NEW_HART_ARRAYS_FOR_IFRIT.
#endif


#define HART_num_enrichment_species 2

#ifdef RADIATIVE_TRANSFER
#define HART_NVAR_L6N  /* necessary for certain rt runs */
#define HART_rt_num_chem_species  8
#else
#define HART_rt_num_chem_species  0
#endif  /* RADIATIVE_TRANSFER */

#ifdef ADDED_NEW_HART_ARRAYS_FOR_IFRIT
#define HART_num_extra_hydro_vars num_extra_hydro_vars
#define HART_num_feedback_species num_feedback_species
#else
#define HART_num_extra_hydro_vars 0
#define HART_num_feedback_species 0
#endif /* ADDED_NEW_HART_ARRAYS_FOR_IFRIT */

#if defined(RADIATIVE_TRANSFER) && defined(RT_TRANSFER)
#ifndef HART_NVAR_L6N
const int HART_nvarMax = (2 + (rt_num_et_vars+1+rt_num_disk_vars));
#else
const int HART_nvarMax = (2 + (rt_num_et_vars+rt_num_disk_vars));
#endif /* HART_NVAR_L6N */
#else
const int HART_nvarMax = 2 ; //potential vars
#endif /* RADIATIVE_TRANSFER */

const int HART_num_hydro_vars = (num_hydro_vars + HART_num_enrichment_species - num_enrichment_species + HART_rt_num_chem_species - rt_num_chem_species
				 + HART_num_extra_hydro_vars - num_extra_hydro_vars + HART_num_feedback_species - num_feedback_species);


typedef struct {
	int cell;
	int refined;
	float *vars;
} cell_file_struct;

void read_hart_grid_binary( char *filename ) {
	int i, j, k, m, idx;
	int size,size2;
	FILE *input;
	char job[256];
	int minlevel, maxlevel;
	double t, dt;
	float adum, ainit;
	float boxh, Om0, Oml0, Omb0, h;
	int nextras;
	float extra[10];
	char lextra[10][256];
	int *oct_list;
	int  *cellrefined_buffer;
	float *vars_buffer;
	int endian;
	int ret;
	int level;
	int index;
	int next, prev;
	int iNOLL, iHOLL;
	int icell;
	int coords[nDim];
	int hart_max_oct;
	int *new_oct_map;
	int *hart_oct_map;
	int ncell0;
	cell_file_struct *child_cells;
	int iOctFree, nOct;

	/* this function is intended to be run serially */
	cart_assert( num_procs == 1 );

	input = fopen(filename,"r");
	if ( input == NULL ) {
		cart_error( "Unable to open file %s for reading!", filename );
	}

	fread(&size, sizeof(int), 1, input );
	endian = 0;
	if ( size != 256 ) {
		reorder( (char *)&size, sizeof(int) );
		if ( size != 256 ) {
			cart_error("Error: file %s is corrupted", filename );
		} else {
			endian = 1;
			cart_debug("Reordering bytes (file endianness is opposite program)");
		}
	}

	fread(&job, sizeof(char), 256, input );
	if ( !control_parameter_is_set("jobname") ) {
	  cart_debug("setting jobname to header value");
	  set_jobname( job );
	}
	fread(&size, sizeof(int), 1, input );

	/* istep, t, dt, adum, ainit */
	fread( &size, sizeof(int), 1, input );
	fread( &step, sizeof(int), 1, input );
	fread( &t, sizeof(double), 1, input );
	fread( &dt, sizeof(double), 1, input );
	fread( &adum, sizeof(float), 1, input );
	fread( &ainit, sizeof(float), 1, input );
	fread( &size, sizeof(int), 1, input );

	if ( endian ) {
		reorder( (char *)&step, sizeof(int) );
		reorder( (char *)&ainit, sizeof(float) );
		reorder( (char *)&adum, sizeof(float) );
	}

	auni[min_level] = adum;
	auni_init = ainit;

	/* boxh, Om0, Oml0, Omb0, hubble */
	fread( &size, sizeof(int), 1, input );
	fread( &boxh, sizeof(float), 1, input );
	fread( &Om0, sizeof(float), 1, input );
	fread( &Oml0, sizeof(float), 1, input );
	fread( &Omb0, sizeof(float), 1, input );
	fread( &h, sizeof(float), 1, input );
	fread( &size, sizeof(int), 1, input );

	if ( endian ) {
		reorder( (char *)&boxh, sizeof(float) );
		reorder( (char *)&Om0, sizeof(float) );
		reorder( (char *)&Oml0, sizeof(float) );
		reorder( (char *)&Omb0, sizeof(float) );
		reorder( (char *)&h, sizeof(float) );
	}

	box_size = boxh;
	cosmology_set(OmegaM, Om0 );
	cosmology_set(OmegaL, Oml0 );
	cosmology_set(OmegaB, Omb0 );
	cosmology_set( h, h );

	/* nextra (no evidence extras are used...) extra lextra */
	fread( &size, sizeof(int), 1, input );
	fread( &nextras, sizeof(int), 1, input );
	fread( &size, sizeof(int), 1, input );

	if ( endian ) {
		reorder( (char *)&nextras, sizeof(int) );
	}

	/* extra */
	fread( &size, sizeof(int), 1, input );
	fread( extra, sizeof(float), nextras, input );
	fread( &size, sizeof(int), 1, input );

	/* lextra */
	fread( &size, sizeof(int), 1, input );
	fread( lextra, 256*sizeof(char), nextras, input );
	fread( &size, sizeof(int), 1, input );

	/* Minlevel, MaxLevelNow */
	fread( &size, sizeof(int), 1, input );
	fread( &minlevel, sizeof(int), 1, input );
	fread( &maxlevel, sizeof(int), 1, input);
	fread( &size, sizeof(int), 1, input );

	if ( endian ) {
		reorder( (char *)&minlevel, sizeof(int) );
		reorder( (char *)&maxlevel, sizeof(int) );
	}

	if ( maxlevel > max_level ) {
		cart_error("File %s has more levels than compiled program (%u)", filename, maxlevel );
	}

	cart_assert( minlevel == min_level );

	/* tl */
	fread( &size, sizeof(int), 1, input );
	fread( tl, sizeof(double), maxlevel-minlevel+1, input );
	fread( &size, sizeof(int), 1, input);

	if ( endian ) {
		for ( i = minlevel; i <= maxlevel; i++ ) {
			reorder( (char *)&tl[i], sizeof(double) );
		}
	}

	/* dtl */
	fread( &size, sizeof(int), 1, input );
	fread( dtl, sizeof(double), maxlevel-minlevel+1, input);
	fread( &size, sizeof(int), 1, input );
	if ( endian ) {
		for ( i = minlevel; i <= maxlevel; i++ ) {
			reorder( (char *)&dtl[i], sizeof(double) );
		}
	}

	/* tl_old */
	fread( &size, sizeof(int), 1, input );
	fread( tl_old, sizeof(double), maxlevel-minlevel+1, input);
	fread( &size, sizeof(int), 1, input );

	if ( endian ) {
		for ( i = minlevel; i <= maxlevel; i++ ) {
			reorder( (char *)&tl_old[i], sizeof(double) );
		}
	}

	/* dtl_old */
	fread( &size, sizeof(int), 1, input );
	fread( dtl_old, sizeof(double), maxlevel-minlevel+1, input);
	fread( &size, sizeof(int), 1, input );

	if ( endian ) {
		for ( i = minlevel; i <= maxlevel; i++ ) {
			reorder( (char *)&dtl_old[i], sizeof(double) );
		}
	}

	/* iSO */
	fread( &size, sizeof(int), 1, input );
	fread( level_sweep_dir, sizeof(int), maxlevel-minlevel+1, input);
	fread( &size, sizeof(int), 1, input );

	if ( endian ) {
		for ( i = minlevel; i <= maxlevel; i++ ) {
			reorder( (char *)&level_sweep_dir[i], sizeof(int) );
		}
	}

#ifdef STAR_FORMATION
	for(i=0;i<nDim;i++) {
		star_formation_volume_min[i] = 0;
		star_formation_volume_max[i] = num_grid;
	}
	cart_debug("warning: hart format does not retain star_formation_volume or refinement_volume arrays ");
	cart_debug("this will lead to a <=48byte difference between *.d->*.dh->*.d files ");
#endif

	/* ncell0 */
	fread( &size, sizeof(int), 1, input );
	fread( &ncell0, sizeof(int), 1, input);
	fread( &size, sizeof(int), 1, input );

	if ( endian ) {
		reorder( (char *)&ncell0, sizeof(int) );
	}

	if ( ncell0 != (num_grid*num_grid*num_grid) ) {
		cart_error("File has different num_grid than compiled program");
	}

	init_tree();

	/* create hash for hart oct index -> our oct index */
	hart_max_oct = 0;
	hart_oct_map = NULL;

	cellrefined_buffer = cart_alloc( int, num_grid*num_grid );

	fread( &size, sizeof(int), 1, input );

	for ( coords[0] = 0; coords[0] < num_grid; coords[0]++ ) {
	  cart_debug("0-level: %d/%d hash",coords[0],num_grid);
		fread( cellrefined_buffer, sizeof(int), num_grid*num_grid, input );

		if ( endian ) {
			for ( i = 0; i < num_grid*num_grid; i++ ) {
				reorder( (char *)&cellrefined_buffer[i], sizeof(int) );
			}
		}

		int max_oct = -1;
		for ( i = 0; i < num_grid*num_grid; i++ ) {
			max_oct = MAX(cellrefined_buffer[i], max_oct);
		}
		if ( max_oct > hart_max_oct ) {
			new_oct_map = cart_alloc(int, max_oct+1);
			for ( i = 0; i < max_oct+1; i++ ) {
				new_oct_map[i] = -1;
			}

			if ( hart_oct_map ) {
				memcpy(new_oct_map, hart_oct_map, hart_max_oct*sizeof(int));
				free(hart_oct_map);
			}
			hart_oct_map = new_oct_map;
			hart_max_oct = max_oct;
		}

		i = 0;
		for ( coords[1] = 0; coords[1] < num_grid; coords[1]++ ) {
			for ( coords[2] = 0; coords[2] < num_grid; coords[2]++ ) {
				index = sfc_index( coords );
				icell = sfc_cell_location( index );
				if ( cellrefined_buffer[i] ) {
					cart_assert( cell_is_leaf( icell ) );
					ret = split_cell( icell );
					if ( ret ) {
						cart_error("Error splitting cell %d\n", icell );
					}

					hart_oct_map[cellrefined_buffer[i]] = cell_child_oct[icell];
				}

				i++;
			}
		}
	}
	fread( &size2, sizeof(int), 1, input );

	cart_free( cellrefined_buffer );

#ifdef HYDRO
	fread( &size, sizeof(int), 1, input );
	if ( endian ) {
		reorder( (char *)&size, sizeof(int) );
	}
#ifdef DEBUG
	cart_debug("Record size: %d",size);
#endif

	int num_hydro_vars_file = size/num_grid/num_grid/num_grid/sizeof(float);

	cart_debug("num_chem_species_code: %d",num_chem_species);
	cart_debug("num_hydro_vars_file: %d",num_hydro_vars_file);
	cart_assert(num_hydro_vars_file == HART_num_hydro_vars)

	vars_buffer = cart_alloc( float, num_grid*num_grid*num_hydro_vars_file );

	for ( coords[0] = 0; coords[0] < num_grid; coords[0]++ ) {
		cart_debug("0-level: %d/%d",coords[0],num_grid);
		fread( vars_buffer, sizeof(float), num_hydro_vars_file*num_grid*num_grid, input );

		if ( endian ) {
			for ( i = 0; i < num_hydro_vars_file*num_grid*num_grid; i++ ) {
				reorder( (char *)&vars_buffer[i], sizeof(float) );
			}
		}
		idx = 0;

		for ( coords[1] = 0; coords[1] < num_grid; coords[1]++ ) {
			for ( coords[2] = 0; coords[2] < num_grid; coords[2]++ ) {
				index = sfc_index( coords );
				icell = sfc_cell_location( index );
				i = idx*num_hydro_vars_file;

				cell_gas_density(icell) = vars_buffer[i++];
				cell_gas_energy(icell) = vars_buffer[i++];
				cell_momentum(icell,0) = vars_buffer[i++];
				cell_momentum(icell,1) = vars_buffer[i++];
				cell_momentum(icell,2) = vars_buffer[i++];
				cell_gas_pressure(icell) = vars_buffer[i++];
				cell_gas_gamma(icell) = vars_buffer[i++];
				cell_gas_internal_energy(icell) = vars_buffer[i++];
#if defined(ELECTRON_ION_NONEQUILIBRIUM) && defined(ADDED_NEW_HART_ARRAYS_FOR_IFRIT)
				cell_electron_internal_energy(icell) = vars_buffer[i++]; /* not used in HART */
#endif
#ifdef ENRICHMENT
				cell_gas_metal_density_II(icell) = vars_buffer[i++];
#ifdef ENRICHMENT_SNIa
				cell_gas_metal_density_Ia(icell) = vars_buffer[i++];
#else
				i++; //skip -- HART always writes enrich
#endif /*  ENRICHMENT_SNIa  */
#else
				i++; //skip -- HART always writes enrich
#endif /*  ENRICHMENT */
#ifdef RADIATIVE_TRANSFER
				cell_HI_density(icell) = vars_buffer[i++];
				cell_HII_density(icell) = vars_buffer[i++];
				cell_HeI_density(icell) = vars_buffer[i++];
				cell_HeII_density(icell) = vars_buffer[i++];
				cell_HeIII_density(icell) = vars_buffer[i++];
				cell_H2_density(icell) = vars_buffer[i++];
				for(m=0; m<HART_rt_num_chem_species - rt_num_chem_species; m++){
				  i++; //skip -- HART always writes H2+ and H- density
				}
#endif /* RADIATIVE TRANSFER */
#if defined(BLASTWAVE_FEEDBACK) && defined(ADDED_NEW_HART_ARRAYS_FOR_IFRIT)
				  cell_blastwave_time(icell) = vars_buffer[i++]; /* not used in HART */
#endif
				idx++;

			}
		}
	}
	fread( &size, sizeof(int), 1, input );
#ifdef DEBUG
	if ( endian ) {
		reorder( (char *)&size, sizeof(int) );
	}
	cart_debug("... matching size: %d",size);
#endif

	cart_free( vars_buffer );
#endif /* HYDRO */

#if defined(GRAVITY) || defined(RADIATIVE_TRANSFER)

	fread( &size, sizeof(int), 1, input );
	if ( endian ) {
		reorder( (char *)&size, sizeof(int) );
	}
#ifdef DEBUG
	cart_debug("Record size: %d",size);
#endif
	int nvar_file = size/num_grid/num_grid/num_grid/sizeof(float);

	if(HART_nvarMax != nvar_file){
#if defined(RADIATIVE_TRANSFER) && defined(RT_TRANSFER)
		cart_debug("HART_nvarMax = (2 + (rt_num_et_vars=%d + 1 + rt_num_fields_per_freq=%d * rt_num_freqs=%d ))", rt_num_et_vars,rt_num_fields_per_freq,rt_num_freqs);
		cart_debug("def RT_UV ? rt_num_freqs=4 : 3 ", rt_num_et_vars,rt_num_fields_per_freq,rt_num_freqs);
#else
		cart_debug("HART_nvarMax = 2");
#endif /* RADIATIVE_TRANSFER */
		cart_debug("you may need to alter the rt defs, define HART_NVAR_L6N, or play with HART_nvarMax manually");
		cart_error("nvar_file=%d != HART_nvarMax=%d",nvar_file,HART_nvarMax);
	}

	vars_buffer = cart_alloc( float, nvar_file*num_grid*num_grid );

	for ( coords[0] = 0; coords[0] < num_grid; coords[0]++ ) {
		fread( vars_buffer, sizeof(float), nvar_file*num_grid*num_grid, input );

		if ( endian ) {
			for ( i = 0; i < nvar_file*num_grid*num_grid; i++ ) {
				reorder( (char *)&vars_buffer[i], sizeof(float) );
			}
		}
		idx = 0;

		for ( coords[1] = 0; coords[1] < num_grid; coords[1]++ ) {
			for ( coords[2] = 0; coords[2] < num_grid; coords[2]++ ) {
				index = sfc_index( coords );
				icell = sfc_cell_location( index );

				i = idx*nvar_file;
#ifdef GRAVITY
				cell_potential(icell) = vars_buffer[i++];
				cell_potential_hydro(icell) = vars_buffer[i++];
#endif
#if defined(RADIATIVE_TRANSFER) && defined(RT_TRANSFER)
				for(j=0; j<rt_num_disk_vars; j++) {
					cell_var(icell,j+rt_disk_offset) = vars_buffer[i++];
				}
				for(j=0; j<rt_num_et_vars; j++) {
					cell_var(icell,j+rt_et_offset) = vars_buffer[i++];
				}
#ifndef HART_NVAR_L6N
				cell_rt_source(icell) = vars_buffer[i++];
#endif
#endif /* HART_NVAR_L6N */
				idx++;
			}
		}
	}
	fread( &size, sizeof(int), 1, input );
#ifdef DEBUG
	if ( endian ) {
		reorder( (char *)&size, sizeof(int) );
	}
	cart_debug("... matching size: %d",size);
#endif

	cart_free( vars_buffer );
#endif /* GRAVITY */

	/* iOctfree and nOct */
	fread( &size, sizeof(int), 1, input );
	fread( &iOctFree, sizeof(int), 1, input );
	fread( &nOct, sizeof(int), 1, input );
	fread( &size, sizeof(int), 1, input );

	if ( endian ) {
		reorder( (char *)&iOctFree, sizeof(int) );
		reorder( (char *)&nOct, sizeof(int) );
	}

	child_cells = cart_alloc( cell_file_struct, num_children );
	for(i=0; i<num_children; i++) child_cells[i].vars = cart_alloc(float, num_hydro_vars_file+nvar_file);

	for ( level = 1; level <= maxlevel; level++ ) {
		fread( &size, sizeof(int), 1, input );
		if ( endian ) {
			reorder( (char *)&size, sizeof(int) );
		}

		cart_assert( size == 3*sizeof(int) );

		fread( &size, sizeof(int), 1, input );
		fread( &iNOLL, sizeof(int), 1, input );
		fread( &iHOLL, sizeof(int), 1, input );

		if ( endian ) {
			reorder( (char *)&iNOLL, sizeof(int) );
			reorder( (char *)&iHOLL, sizeof(int) );
			reorder( (char *)&size, sizeof(int) );
		}

		fread( &size, sizeof(int), 1, input );

		i = 0;
		oct_list = cart_alloc(int, iNOLL);

		while ( iHOLL != 0 ) {
			oct_list[i] = hart_oct_map[iHOLL];
			cart_assert( oct_list[i] != NULL_OCT );
			cart_assert( oct_level[oct_list[i]] == level );

			fread( &size, sizeof(int), 1, input );
			if ( endian ) {
				reorder( (char *)&size, sizeof(int) );
			}

			cart_assert( size == 13*sizeof(int) );

			/* skip oct information that's not needed */
			fseek( input, 11*sizeof(int), SEEK_CUR );
			fread( &next, sizeof(int), 1, input );
			fread( &prev, sizeof(int), 1, input );
			fread( &size, sizeof(int), 1, input );

			if ( endian ) {
				reorder( (char *)&next, sizeof(int) );
			}

			/* goto next oct */
			iHOLL = next;
			i++;
		}

		cart_assert( i == iNOLL );

		for ( i = 0; i < iNOLL; i++ ) {
			for(j=0; j<num_children; j++) {
				fread( &size, sizeof(int), 1, input );
				fread( &child_cells[j].cell, sizeof(int), 1, input );
				fread( &child_cells[j].refined, sizeof(int), 1, input );
				fread( child_cells[j].vars, sizeof(float), num_hydro_vars_file+nvar_file, input );
				fread( &size, sizeof(int), 1, input );
			}

			for ( j = 0; j < num_children; j++ ) {
				icell = oct_child( oct_list[i], j );
				cart_assert( cell_level(icell) == level );

				if ( endian ) {
					reorder( (char *)&child_cells[j].refined, sizeof(int) );
				}

				if ( child_cells[j].refined > 0 ) {
					ret = split_cell( icell );
					if ( ret ) {
						cart_error("Error splitting cell %d", icell );
					}

					if ( child_cells[j].refined > hart_max_oct ) {
						new_oct_map = cart_alloc(int, child_cells[j].refined+iNOLL+1);
						for ( i = 0; i < child_cells[j].refined+iNOLL+1; i++ ) {
							new_oct_map[i] = -1;
						}

						if ( hart_oct_map ) {
							memcpy(new_oct_map, hart_oct_map, hart_max_oct*sizeof(int));
							free(hart_oct_map);
						}
						hart_oct_map = new_oct_map;
						hart_max_oct = child_cells[j].refined+iNOLL+1;
					}

					hart_oct_map[child_cells[j].refined] = cell_child_oct[icell];
				}

				if ( endian ) {
					for ( k = 0; k < num_hydro_vars_file+nvar_file; k++ ) {
						reorder( (char *)&child_cells[j].vars[k], sizeof(float) );
					}
				}

				k = 0;
				cell_gas_density(icell) = child_cells[j].vars[k++];
				cell_gas_energy(icell) = child_cells[j].vars[k++];
				cell_momentum(icell,0) = child_cells[j].vars[k++];
				cell_momentum(icell,1) = child_cells[j].vars[k++];
				cell_momentum(icell,2) = child_cells[j].vars[k++];
				cell_gas_pressure(icell) = child_cells[j].vars[k++];
				cell_gas_gamma(icell) = child_cells[j].vars[k++];
				cell_gas_internal_energy(icell) = child_cells[j].vars[k++];
#if defined(ELECTRON_ION_NONEQUILIBRIUM) && defined(ADDED_NEW_HART_ARRAYS_FOR_IFRIT)
				cell_electron_internal_energy(icell) = child_cells[j].vars[k++]; /* not used in HART */
#endif
#ifdef ENRICHMENT
				cell_gas_metal_density_II(icell) = child_cells[j].vars[k++];
#ifdef ENRICHMENT_SNIa
				cell_gas_metal_density_Ia(icell) = child_cells[j].vars[k++];
#else
				k++; //skip -- HART always writes enrich
#endif /*  ENRICHMENT_SNIa  */
#else
				k++; //skip -- HART always writes enrich
#endif /*  ENRICHMENT */
#ifdef RADIATIVE_TRANSFER
				cell_HI_density(icell) = child_cells[j].vars[k++];
				cell_HII_density(icell) = child_cells[j].vars[k++];
				cell_HeI_density(icell) = child_cells[j].vars[k++];
				cell_HeII_density(icell) = child_cells[j].vars[k++];
				cell_HeIII_density(icell) = child_cells[j].vars[k++];
				cell_H2_density(icell) = child_cells[j].vars[k++];
				for(m=0; m<HART_rt_num_chem_species - rt_num_chem_species; m++){
				  k++; //skip -- HART always writes H2+ and H- density
				}
#endif /* RADIATIVE TRANSFER */
#if defined(BLASTWAVE_FEEDBACK) && defined(ADDED_NEW_HART_ARRAYS_FOR_IFRIT)
				cell_blastwave_time(icell) = child_cells[j].vars[k++]; /* not used in HART */
#endif
				cart_assert(k == num_hydro_vars_file);
#ifdef GRAVITY
				cell_potential(icell) = child_cells[j].vars[k++];
				cell_potential_hydro(icell) = child_cells[j].vars[k++];
#endif /* GRAVITY */

#if defined(RADIATIVE_TRANSFER) && defined(RT_TRANSFER)
				for(m=0; m<rt_num_disk_vars; m++) {
					cell_var(icell,m+rt_disk_offset) = child_cells[j].vars[k++];
				}
				for(m=0; m<rt_num_et_vars; m++) {
					cell_var(icell,m+rt_et_offset) = child_cells[j].vars[k++];
				}
#ifndef HART_NVAR_L6N
				cell_rt_source(icell) = child_cells[j].vars[k++];
#endif /* RADIATIVE_TRANSFER */
				cart_assert(k == num_hydro_vars_file+nvar_file);
#endif /* HART_NVAR_L6N */
			}
		}

		cart_free( oct_list );
	}

	fclose(input);

	for(i=0; i<num_children; i++) cart_free(child_cells[i].vars);
	cart_free( child_cells );
	if ( hart_oct_map ) free(hart_oct_map);

	build_cell_buffer();
}

#endif /* HYDRO && GRAVITY && COSMOLOGY */
