#include "config.h"


#include <math.h>
#include <stdio.h>


#include "auxiliary.h"
#include "control_parameter.h"
#include "cosmology.h"
#include "halo_finder.h"
#include "hydro.h"
#include "io.h"
#include "iterators.h"
#include "parallel.h"
#include "particle.h"
#include "plugin.h"
#include "rand.h"
#include "rt_global.h"
#include "rt.h"
#include "starformation.h"
#include "system.h"
#include "times.h"
#include "tree.h"
#include "units.h"

#include "frt/frt_c.h"

#include "../run/step.h"

#include "../extra/21cm.h"
#include "../extra/cell_probe.h"
#include "../extra/galfind.h"
#include "../extra/ifrit.h"
#include "../extra/power.h"
#include "../extra/sps.h"
#include "../extra/synthetic_spectra.h"
#include "../extra/addqso.h"

#include<fenv.h>
void kill_nan_run(){
     feenableexcept(FE_DIVBYZERO | FE_INVALID | FE_OVERFLOW);
}

extern int num_power_foldings;
extern int power_mesh_size;

extern int rtOtvetMaxNumIter;

double qso_lum;//=1e46; //erg/s
//int MAXNUM_QSO_TRACER=6578;
int MAXNUM_QSO_TRACER;
//=200;
char PID_fpath_d[CONTROL_PARAMETER_STRING_LENGTH] = "";
const char* PID_fpath=PID_fpath_d;
//="/u/sciteam/chen18/ART/compile/find_tracer_star_a0.1201/halo0/halo0_starPID.txt";

void qsoInit();
void qsoReadPID();
void qsoAddSource(int level);
void qsoFindPosition(int level, MPI_Comm level_com);
void config_init_qsolum();
void config_verf_qsolum();
void config_init_qsohalo();
void config_verf_qsohalo();
void config_init_qso();
void config_verf_qso();
/*
//  Plugin
*/
#ifdef RADIATIVE_TRANSFER


void RunBegin();
void OutputIFrITFile(int level, MPI_Comm level_com);
void WriteLog();


plugin_t ngPlugin = { NULL };
plugin_t qsoPlugin = { NULL };

const plugin_t* add_plugin(int id)
{
  if(id == 0)
    {
      qsoPlugin.ConfigInit = config_init_qso;
      qsoPlugin.ConfigVerify = config_verf_qso;
      qsoPlugin.RunBegin = qsoInit;
      qsoPlugin.LevelStepBegin = qsoFindPosition;
      cart_debug("after find position");
      qsoPlugin.RTExternalSource = qsoAddSource;
      return &qsoPlugin;
    }
  if(id ==1){
    ngPlugin.RunBegin=kill_nan_run;
  }
  if(id == 2)
    {
      //ngPlugin.GlobalStepBegin = ngPlugin.RunEnd = WriteLog;
      ngPlugin.GlobalStepBegin = OutputIFrITFile;
      ngPlugin.LevelStepBegin = OutputIFrITFile;
      return &ngPlugin;
    }
  return NULL;
}

















void WriteLog()
{
  const double OutputsGLF[] = { 0.1026, 0.1051, 0.1112, 0.1200, 0.1280, 0.1350, 0.1400, 0.1450 };
  const int NumOutputsGLF = sizeof(OutputsGLF)/sizeof(double);

  //const double OutputsLOS[] = { 0.1000, 0.1111, 0.1200, 0.1280, 0.1350, 0.1400, 0.1428, 0.1449, 0.1470, 0.1492, 0.1515, 0.1538, 0.1562, 0.1587, 0.1612, 0.1639, 0.1666 };
  //const int NumOutputsLOS = sizeof(OutputsLOS)/sizeof(double);

  const float wlen[] = { 1000.0, 911.75, 504.25, 227.83 };
  const int nout = sizeof(wlen)/sizeof(float);
  int i, detGLF, detLOS;
  float nxi[nout], rates[FRT_RATE_DIM];
  float w, uJnu = 1.5808e-17/pow(auni[min_level],3.0);
  char path[999] = { 0 };

  const int navgs = 18;
  double sum[navgs], avgs[navgs];

  static double old_stellar_initial_mass = 0.0, old_tl = -1.1e30;
  double new_stellar_mass, new_stellar_initial_mass, smd, sfr;
  double stellar_mass, stellar_initial_mass, dtyears;

  FILE *f;
  char filename[999];
  MESH_RUN_DECLARE(level,cell);

  if(old_tl > tl[min_level]-0.001*dtl[min_level])
    {
      cart_debug("Skipping rei.log: old_tl=%lg tl=%lg dtl=%lg",old_tl,tl[min_level],dtl[min_level]);
      return;
    }

  cart_debug("Writing rei.log...");

  detGLF = detLOS = 0;
  for(i=0; i<NumOutputsGLF; i++)
    {
      if(auni[min_level]>=OutputsGLF[i] && auni_from_tcode(tl[min_level]-dtl[min_level])<OutputsGLF[i])
	{
	  detGLF = 1;
	  break;
	}
    }
/*
  for(i=0; i<NumOutputsLOS; i++)
    {
      if(auni[min_level]>=OutputsLOS[i] && auni_from_tcode(tl[min_level]-dtl[min_level])<OutputsLOS[i])
	{
	  detLOS = 1;
	  break;
	}
    }
*/
  if(detGLF) cart_debug("Saving GLF...");
  if(detLOS) cart_debug("Saving LOS..");

  if((detGLF || detLOS) && local_proc_id==MASTER_NODE)
    {
      int ret;
      char str[999];
      sprintf(path,"%s/a=%6.4f",output_directory,auni[min_level]);

      /*
      //  Create output directory
      */
      ret = system_mkdir(path);
      if(ret != 0)
        {
          cart_error("system_mkdir(\"%s\") returned with the error code %d. Abort.",path,ret);
        }

      if(detLOS)
	{
	  sprintf(str,"%s/los",path);
	  ret = system_mkdir(str);
	  if(ret != 0)
	    {
	      cart_error("system_mkdir(\"%s\") returned with the error code %d. Abort.",str,ret);
	    }
	}

      /*
      sprintf(str,"%s/los2",path);
      ret = system_mkdir(str);
      if(ret != 0)
        {
          cart_error("system_mkdir(\"%s\") returned with the error code %d. Abort.",str,ret);
        }
      */
    }

  /*
  //  Extract radiation field
  */
  rtGetPhotoRatesFS(-1,rates);
  rtGetRadiationFieldFS(-1,nout,wlen,nxi);

  /*
  //  Compute various mesh averages
  */
  for(i=0; i<navgs; i++) sum[i] = 0.0;

  MESH_RUN_OVER_ALL_LEVELS_BEGIN(level);
  MESH_RUN_OVER_CELLS_OF_LEVEL_BEGIN(cell);
  if(cell_is_leaf(cell))
    {
      float Tk = cell_gas_temperature(cell);
      float Ts = ext21CellTs(cell);
      float Tb = ext21CellTb(cell,0,0);

      w = cell_gas_density(cell)*cell_volume[level]/num_root_cells;
      sum[0] += w;
      sum[2] += w*cell_HI_fraction(cell);
      sum[4] += w*cell_HII_fraction(cell);
      sum[6] += w*cell_HeI_fraction(cell);
      sum[8] += w*cell_HeII_fraction(cell);
      sum[10] += w*cell_HeIII_fraction(cell);
      sum[12] += w*Tk;
      sum[14] += w*Ts;
      sum[16] += w*Tb;

      w = cell_volume[level]/num_root_cells;
      sum[1] += w;
      sum[3] += w*cell_HI_fraction(cell);
      sum[5] += w*cell_HII_fraction(cell);
      sum[7] += w*cell_HeI_fraction(cell);
      sum[9] += w*cell_HeII_fraction(cell);
      sum[11] += w*cell_HeIII_fraction(cell);
      sum[13] += w*Tk;
      sum[15] += w*Ts;
      sum[17] += w*Tb;
    }
  MESH_RUN_OVER_CELLS_OF_LEVEL_END;
  MESH_RUN_OVER_LEVELS_END;

  MPI_Allreduce(sum,avgs,navgs,MPI_DOUBLE,MPI_SUM,mpi.comm.run);

  if(fabs(avgs[1]-1) > 1.0e-5)
    {
      cart_error("Wrong average, <1>=%e",avgs[1]);
    }

  /*
  //  Properties of stars
  */
  stellar_mass = 0.0;
  stellar_initial_mass = 0.0;
  for(i=0; i<num_star_particles; i++) if(particle_level[i]!=FREE_PARTICLE_LEVEL && particle_is_star(i))
    {
      stellar_mass += particle_mass[i];
      stellar_initial_mass += star_initial_mass[i];
    }

  MPI_Reduce(&stellar_mass,&new_stellar_mass,1,MPI_DOUBLE,MPI_SUM,MASTER_NODE,mpi.comm.run);
  MPI_Reduce(&stellar_initial_mass,&new_stellar_initial_mass,1,MPI_DOUBLE,MPI_SUM,MASTER_NODE,mpi.comm.run);

  /*
  //  Ly-alpha spectra
  */
  if(detLOS)
    {
      double len = 2.5*num_grid; /* 50Mpc/h for 20 CHIMP box */
      char str[999];
      sprintf(str,"%s/los",path);

      ssDumpRandomSpectra(ss_HI1216,str,1000,len,10.0,max_level,-119,0);
    }

#ifdef RT_VAR_SOURCE
  if(detGLF)
    {
      halo_list *halos = gfRun(RT_VAR_SOURCE,VAR_ACCEL,0.0,5,999);
      if(halos != NULL)
	{
	  char str[999];

	  write_halo_list(halos,path);

	  /*
	  //  Galaxy luminosities
	  */
	  sprintf(str,"%s/gallums.res",path);
	  extGalLums(str,halos,1,100,2e7,"drt/muv1500.res");

	  /*
	  //  Galaxy-centered short HI spectra (for damping wings)
	  */
	  //sprintf(str,"%s/los2",path);
	  //ssDumpHalocentricSpectra(ss_HI1216,str,halos,0.5*num_grid,10.0,max_level,-1.0e4,10*constants->kpc/units->length,1.0e7*constants->Msun/units->mass,1L);

	  cart_free(halos);
	}
    }
#endif /* RT_VAR_SOURCE */

  if(local_proc_id == MASTER_NODE)
    {
      sprintf(filename,"%s/rei.log",logfile_directory);
      f = fopen(filename,(step == 0) ? "w" : "a");
      cart_assert(f != NULL);

      if(step == 0)
	{
	  fprintf(f,"# Columns:\n");
	  fprintf(f,"#   1. scale factor\n");
	  fprintf(f,"#   2-4. Photo rates (HI, HeI, HeII)\n");
	  fprintf(f,"#   5-8. Jnu (LW, HI, HeI, HeII)\n");
	  fprintf(f,"#   9-13. <XHI>, <XHII>, <XHeI>, <XHeII>, <XHeIII>, mass-weighted\n");
	  fprintf(f,"#   14-18. <XHI>, <XHII>, <XHeI>, <XHeII>, <XHeIII>, volume-weighted\n");
	  fprintf(f,"#   19. stellar mass density (Msun/CHIMP^3)\n");
	  fprintf(f,"#   20. star formation rate density (Msun/yr/CHIMP^3)\n");
	  fprintf(f,"#   21-23. <Tk>, <Ts>, <Tb>, mass-weighted\n");
	  fprintf(f,"#   24-26. <Tk>, <Ts>, <Tb>, volume-weighted\n");
	}

      fprintf(f,"%7.4f",auni[min_level]);
      fprintf(f," %10.3le %10.3le %10.3le",rates[FRT_RATE_IonizationHI],rates[FRT_RATE_IonizationHeI],rates[FRT_RATE_IonizationHeII]);
      fprintf(f," %10.3le %10.3le %10.3le %10.3le",uJnu*nxi[0],uJnu*nxi[1],uJnu*nxi[2],uJnu*nxi[3]);

      fprintf(f," %10.3le %10.3le %10.3le %10.3le %10.3le",avgs[2]/avgs[0],avgs[4]/avgs[0],avgs[6]/avgs[0],avgs[8]/avgs[0],avgs[10]/avgs[0]);
      fprintf(f," %10.3le %10.3le %10.3le %10.3le %10.3le",avgs[3]/avgs[1],avgs[5]/avgs[1],avgs[7]/avgs[1],avgs[9]/avgs[1],avgs[11]/avgs[1]);

      smd = new_stellar_mass*units->mass/constants->Msun/pow(box_size,3.0);
      dtyears = (tl[min_level]-old_tl)*units->time/constants->yr;
      sfr = (new_stellar_initial_mass-old_stellar_initial_mass)*units->mass/constants->Msun/dtyears/pow(box_size,3.0);

      fprintf(f," %10.3le %10.3le",smd,sfr);

      fprintf(f," %10.3le %10.3le %10.3le",avgs[12]/avgs[0],avgs[14]/avgs[0],avgs[16]/avgs[0]);
      fprintf(f," %10.3le %10.3le %10.3le",avgs[13]/avgs[1],avgs[15]/avgs[1],avgs[17]/avgs[1]);

      fprintf(f,"\n");
      fflush(f);
      fclose(f);
    }

  old_tl = tl[min_level];
  old_stellar_initial_mass = new_stellar_initial_mass;
}

//do not use Out4IFrIT1
//need to Modify Out4IFrIT2: pass level_com to Out4IFrIT2
void Out4IFrIT1(const int level, const int nprobes, const cell_probe_t *probes);
void Out4IFrIT2(const int level, const int nprobes, const cell_probe_t *probes);

void OutputIFrITFile(int level, MPI_Comm level_com)
{
  cell_probe_t probes[] = { cp_HI_fraction, cp_gas_over_density, cp_temperature, cp_gas_metallicity, cp_brightness_temperature_Z, cp_gas_velocity_Z };
  const int nprobes = sizeof(probes)/sizeof(cell_probe_t);

  if(num_grid < 512)
    {
      if(level == min_level+2) Out4IFrIT1(2,nprobes,probes);
    }
  else
    {
      if(level == min_level+1) Out4IFrIT2(1,nprobes,probes);
    }
}


void Out4IFrIT1(const int level, const int nprobes, const cell_probe_t *probes)
{
  const int nbin1 = num_grid*(1<<level);
  ifrit_config_t cfg = { mpi.comm.run, 0, 1, IFRIT_FLAG_WRITE_MESH };
  int nbin[] = { nbin1, nbin1, nbin1 };
  double pos[] = { 0.5*num_grid, 0.5*num_grid, 0.5*num_grid };
  char froot[999];
  char flabel[10];
  double t0 = MPI_Wtime();

  sprintf(froot,"%s/ifrit",output_directory);
  sprintf(flabel,"a=%6.4lf",auni[level]);

  cfg.num_writers = 1;
  ifritOutputRegion(froot,flabel,-1,nbin,pos,nprobes,probes,&cfg);

  cart_debug("IFrIT file writing time: %lg",MPI_Wtime()-t0);
}


void Out4IFrIT2(const int level, const int nprobes, const cell_probe_t *probes)
{
  int i, j, k, m, cell, nmax, nitems;
  double pos[3], dx;
  char fname[999];
  FILE *F;

  const int maxitems = 1000000;
  struct item_t
  {
    int64_t idx;
#ifdef __cplusplus
    float vars[10];
#else
    float vars[nprobes];
#endif
  }
  *items;

  double t0 = MPI_Wtime();

  items = cart_alloc(struct item_t,maxitems);

  /*
  //
  */
  cart_assert(sizeof(struct item_t) == (8+nprobes*sizeof(float)))

  sprintf(fname,"%s/ifrit-a=%6.4lf",output_directory,auni[level]);

  if(local_proc_id == MASTER_NODE)
    {
      if(system_mkdir(fname) != 0)
	{
	  cart_error("Unable to create dump directory");
	}
    }

  MPI_Barrier(mpi.comm.run);

  sprintf(fname,"%s/ifrit-a=%6.4lf/dump.%05d.bin",output_directory,auni[min_level],local_proc_id);

  dx = pow(0.5,(double)level);
  nmax = num_grid << level;
  cart_debug("Dumping pieces for IFrIT grid of size %d",nmax);
  nitems = 0;

  F = fopen(fname,"w");
  cart_assert(F != NULL);
  fwrite(&nmax,sizeof(int),1,F);
  fwrite(&nprobes,sizeof(int),1,F);
  fwrite(&num_procs,sizeof(int),1,F);

  for(k=0; k<nmax; k++)
    {
      pos[2] = dx*(k+0.5);
      for(j=0; j<nmax; j++)
	{
	  pos[1] = dx*(j+0.5);
	  for(i=0; i<nmax; i++)
	    {
	      pos[0] = dx*(i+0.5);
	      cell = cell_find_position_above_level(level,pos);
	      if(cell!=-1 && cell_is_local(cell))
		{
		  for(m=0; m<nprobes; m++)
		    {
		      items[nitems].vars[m] = probes[m].Value(level,cell,NULL);
		    }
		  items[nitems].idx = i + (int64_t)nmax*(j+nmax*k);
		  nitems++;
                    
		  if(nitems == maxitems)
		    {
		      fwrite(items,sizeof(struct item_t),nitems,F);
		      nitems = 0;
		    }
                }
            }
        }
    }

  if(nitems > 0)
    {
      fwrite(items,sizeof(struct item_t),nitems,F);
    }

  fclose(F);

  cart_free(items);

  cart_debug("IFrIT file writing time: %lg",MPI_Wtime()-t0);
}

#endif /* RADIATIVE_TRANSFER */

