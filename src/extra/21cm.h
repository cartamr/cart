#ifndef __EXT_21CM_H__
#define __EXT_21CM_H__


#ifndef CONFIGURED
#error "Missing config.h include."
#endif


#if defined(COSMOLOGY) && defined(HYDRO) && defined(RADIATIVE_TRANSFER)

/*
//  Compute 21cm spin temperature in a cell
*/
float ext21CellTs(int cell);

/*
//  Compute 21cm brightness temperature in a cell with observing direction dir and mode 
//
//  mode=0:  full physics
//  mode&1:  no redshift space distortions
//  mode&2:  no lightcone effects
//  mode&4:  no self-absorption
//  mode&8:  Ts = Tk
*/
float ext21CellTb(int cell, int dir, int mode);


/*
//  Dump the 2D and 1D power spectra (and, optionally, the data cube (2Dsky x 1D frequency), if mode&16 > 0), all dimensions in spatial units.
*/
void ext21PowerSpectra(const char *fileroot, int mesh_angl_level, int mesh_freq_level, int dir, int varout, int mode, int floor_level, int ncuts, float *cuts);

#endif /* COSMOLOGY && HYDRO && RADIATIVE_TRANSFER */

#endif  /* __EXT_21CM_H__ */
