#include "config.h"

#include <math.h>
#include <mpi.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "auxiliary.h"
#include "cosmology.h"
#include "hydro.h"
#include "iterators.h"
#include "parallel.h"
#include "particle.h"
#include "rt.h"
#include "starformation.h"
#include "times.h"
#include "tree.h"
#include "units.h"

#include "halo_tools.h"
#include "sps.h"


sps_table* spsLoadTable(const char *fname, const char *label)
{
  int i, j, nz, nt; 
  float *lfnu, *lz, *lt;
  FILE *f;

  f = fopen(fname,"r");
  if(f == NULL)
    {
      cart_debug("FSPS data file %s is not found.",fname);
      return NULL;
    }

  if(fscanf(f,"%d %d",&nz,&nt) != 2)
    {
      cart_debug("Corrupted FSPS file at header: %d %d.",nz,nt);
      return NULL;
    }

  lz = cart_alloc(float,nz);
  lt = cart_alloc(float,nt);
  lfnu = cart_alloc(float,nt*nz);

  if(fscanf(f,"%f",lz)!=1 || lz[0]!=0.0)
    {
      cart_debug("Corrupted FSPS file at loc(0,0).");
      cart_free(lfnu);
      cart_free(lt);
      cart_free(lz);
      return NULL;
    }

  for(i=0; i<nz; i++)
    {
      if(fscanf(f,"%g",lz+i)!=1 || lz[i]<1.0e-30)
	{
	  cart_debug("Corrupted FSPS file at loc(0,%d).",i+1);
	  cart_free(lfnu);
	  cart_free(lt);
	  cart_free(lz);
	  return NULL;
	}
      lz[i] = log10(lz[i]);
    }
  for(j=0; j<nt; j++)
    {
      if(fscanf(f,"%f",lt+j)!=1 || lt[j]<3)
	{
	  cart_debug("Corrupted FSPS file at loc(%d,0).",j+1);
	  cart_free(lfnu);
	  cart_free(lt);
	  cart_free(lz);
	  return NULL;
	}
      for(i=0; i<nz; i++)
	{
	  if(fscanf(f,"%g",lfnu+nz*j+i)!=1 || lfnu[nz*j+i]<1.0e-30)
	    {
	      cart_debug("Corrupted FSPS file at loc(%d,%d).",j+1,i+1);
	      cart_free(lfnu);
	      cart_free(lt);
	      cart_free(lz);
	      return NULL;
	    }
	}
    }

  fclose(f);

  sps_table *t = cart_alloc(sps_table,1);
  t->nz = nz;
  t->nt = nt;
  t->lz = lz;
  t->lt = lt;
  t->lfnu = lfnu;
  t->label = label;

  return t;
}


void spsDestroyTable(sps_table *t)
{
  cart_assert(t != NULL);

  cart_free(t->lfnu);
  cart_free(t->lt);
  cart_free(t->lz);
  cart_free(t);
}


float spsSampleTable(const sps_table *t, float tStar, float zStar)
{
  int iz, it;
  float ltStar, lzStar, wt, wz;

  ltStar = log10(1.0e-10+fabs(tStar));
  if(ltStar < t->lt[0]) ltStar = t->lt[0];
  if(ltStar > t->lt[t->nt-1]) ltStar = t->lt[t->nt-1];

  lzStar = log10(1.0e-30+fabs(zStar));
  if(lzStar < t->lz[0]) lzStar = t->lz[0];
  if(lzStar > t->lz[t->nz-1]) lzStar = t->lz[t->nz-1];
	  
  for(it=1; it<t->nt-1 && ltStar>t->lt[it]; it++); cart_assert(it>0 && it<t->nt);
  for(iz=1; iz<t->nz-1 && lzStar>t->lz[iz]; iz++); cart_assert(iz>0 && iz<t->nz);

  cart_assert(t->lt[it-1]<=ltStar && ltStar<=t->lt[it]);
  cart_assert(t->lz[iz-1]<=lzStar && lzStar<=t->lz[iz]);

  wt = (t->lt[it]-ltStar)/(t->lt[it]-t->lt[it-1]);
  wz = (t->lz[iz]-lzStar)/(t->lz[iz]-t->lz[iz-1]);

  return wt*(wz*t->lfnu[t->nz*(it-1)+iz-1]+(1-wz)*t->lfnu[t->nz*(it-1)+iz]) + (1-wt)*(wz*t->lfnu[t->nz*it+iz-1]+(1-wz)*t->lfnu[t->nz*it+iz]);
}


#if defined(PARTICLES) && defined(STAR_FORMATION)
void extStarLums(int iStar, double tNow, float *mStar, float *tStar, float *zStar, int nbands, const sps_table * const *tables, float *lums)
{
  int i;

  if(nbands > 0)
    {
      cart_assert(tables != NULL);
      cart_assert(lums != NULL);
    }

  *mStar = star_initial_mass[iStar]*units->mass/constants->Msun;

#ifdef COSMOLOGY
  *tStar = tNow - tphys_from_tcode(star_tbirth[iStar]);
#else
  *tStar = tNow - units->time*star_tbirth[iStar]/constants->yr;
#endif /* COSMOLOGY */

  *zStar = 0.0;
#ifdef ENRICHMENT
  *zStar += star_metallicity_II[iStar];
#ifdef ENRICHMENT_SNIa
  *zStar += star_metallicity_Ia[iStar];
#endif /* ENRICHMENT_SNIa */
#endif /* ENRICHMENT */

  for(i=0; i<nbands; i++)
    {
      lums[i] = *mStar*spsSampleTable(tables[i],*tStar,*zStar);
    }
}


void extGalLums(const char *fname, const struct HALO_LIST *halos, float fvir, float rmax, float tSFR, const char *spsfile)
{
  int ih, j; 
  double rad, sum;
  FILE *f;
  sps_table *t;
  char *is_star;
#ifdef COSMOLOGY
  double tNow = tphys_from_auni(auni[min_level]);
#else
  double tNow = units->time*tl[min_level]/constants->yr;
#endif

  rmax /= (units->length/constants->kpc);

  struct GalData
  {
    double Mstar;
    double Zstar;
    double SFR;
    double Lnu;
  };

  struct GalData *gals, *buf;

  if(halos == NULL)
    {
      cart_debug("No halo file is loaded. Skipping computing galaxy luminosities.");
      return;
    }

  t = spsLoadTable(spsfile,NULL);
  if(t == NULL)
    {
      cart_debug("FSPS data file %s is not found.",spsfile);
      return;
    }

  gals = cart_alloc(struct GalData,halos->num_halos);
  memset(gals,0,sizeof(struct GalData)*halos->num_halos);

  is_star = cart_alloc(char,num_particles);
#ifndef OPENMP_PROBLEMATIC_CONSTRUCT
#pragma omp parallel for default(none), private(j), shared(is_star,particle_species_indices,particle_id,num_particle_species,particle_level)
#endif
  for(j=0; j<num_particles; j++)
    {
      if(particle_level[j] == FREE_PARTICLE_LEVEL)
	{
	  is_star[j] = 0;
	}
      else
	{
	  is_star[j] = particle_is_star(j);
	}
    }

  /*
  // Loop over halos in reverse order, that way we mostly account for satellites
  */
  for(ih=halos->num_halos-1; ih>=0; ih--)
    {
      halo *h = halos->list + ih;
      double sumM = 0.0, sumZ = 0.0, sumS = 0.0, sumL = 0.0;

      rad = fvir*h->rhalo;
      if(rad > rmax) rad = rmax;

      /*
      //  Loop over particles
      */
#ifndef OPENMP_PROBLEMATIC_CONSTRUCT
#pragma omp parallel for default(none), private(j), shared(particle_x,h,rad,t,tNow,tSFR,is_star), reduction(+:sumM,sumZ,sumS,sumL)
#endif
      for(j=0; j<num_particles; j++) if(is_star[j]==1 && (compute_distance_periodic(particle_x[j],(double *)h->pos)<rad))
	{
	  float mStar, tStar, zStar, lum;

	  extStarLums(j,tNow,&mStar,&tStar,&zStar,1,&t,&lum);

	  sumL += lum;
	  if(tStar < tSFR) sumS += mStar/tSFR;
	  sumZ += mStar*zStar;
	  sumM += mStar;

	  //is_star[j] = 0;
	}

      gals[ih].Mstar = sumM;
      gals[ih].Zstar = sumZ;
      gals[ih].SFR = sumS;
      gals[ih].Lnu = sumL;
    }

  spsDestroyTable(t);

  sum = 0.0;
#pragma omp parallel for default(none), private(j), shared(star_initial_mass,is_star), reduction(+:sum)
  for(j=0; j<num_particles; j++) if(is_star[j] == 1)
    {
      sum +=  star_initial_mass[j];
    }
  cart_free(is_star);
  cart_debug("Unaccounted stellar mass: %lg",sum*units->mass/constants->Msun);

  buf = cart_alloc(struct GalData,halos->num_halos);
  MPI_Reduce(gals,buf,halos->num_halos*sizeof(struct GalData)/sizeof(double),MPI_DOUBLE,MPI_SUM,MASTER_NODE,mpi.comm.run);

  cart_free(gals);
  gals = buf;

  if(local_proc_id == MASTER_NODE)
    {
      f = fopen(fname,"w");
      if(f == NULL)
        {
          cart_error("Unable to open output file.");
        }
      fprintf(f,"# id.. Mstar.... Zstar.... SFR...... Lnu......\n");
      fprintf(f,"#\n");

      for(ih=0; ih<halos->num_halos; ih++) if(gals[ih].SFR > 0.0)
        {
          fprintf(f,"%6d %9.3e %9.3e %9.3e %9.3e\n",halos->list[ih].id,gals[ih].Mstar,gals[ih].Zstar/(1.0e-30+gals[ih].Mstar),gals[ih].SFR,gals[ih].Lnu);
        }
      
      fclose(f);
    }

  cart_free(gals);
}
#endif /* PARTICLES && STAR_FORMATION */

