#ifndef __EXT_CELL_PROBE_H__
#define __EXT_CELL_PROBE_H__

#ifndef CONFIGURED
#error "Missing config.h include."
#endif


struct HALO_LIST;


typedef struct CELL_PROBE_DATA
{
  const double *Pos;
  const double *Vel;
  const struct HALO_LIST *HaloList;
}
cell_probe_data_t;


typedef struct CELL_PROBE
{
  const char* Name;
  const char* Header;
  float (*Value)(int level, int cell, const cell_probe_data_t *data);
  /*
  //  <WeightId> values determine how the profiles are weighted: 
  //    0: volume average
  //    1: total mass average
  //    2: baryonic mass average
  //    3: HI mass average
  */
  int WeightId;
}
cell_probe_t;


#define NEW_CELL_PROBE(name,header,weight) \
  static float cp_##name##_fun(int level, int cell, const cell_probe_data_t *data); \
  cell_probe_t cp_##name = { #name, header, cp_##name##_fun, weight }; \
  static float cp_##name##_fun(int level, int cell, const cell_probe_data_t *data)


#include "cell_probe_generated.h"


#endif  /* __EXT_CELL_PROBE_H__ */
