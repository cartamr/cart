#ifndef __EXT_SYNTHETIC_SPECTRA_H__
#define __EXT_SYNTHETIC_SPECTRA_H__

#ifndef CONFIGURED
#error "Missing config.h include."
#endif


struct ssPointType;
typedef struct ssPointType ssPoint;

struct ssInfoType;
typedef struct ssInfoType ssInfo;

typedef struct ssLineType
{
  int NumPts, Size;
  double e[3];
  ssPoint* Data;
  const ssInfo *Info;
}
ssLine;

typedef struct ssSpectrumType
{
  int Length;
  double *Vels;
  double *Flux;
}
ssSpectrum;


extern const ssInfo *ss_HI1216;
extern const ssInfo *ss_HI1216L;


/*
//  Voigt profile and its integral between w1 and w2
*/
double voigt(double w, double wDopler, double wLorentz);
double voigtInt(double w1, double w2, double wDopler, double wLorentz);

#if defined(HYDRO) && defined(RADIATIVE_TRANSFER) && defined(COSMOLOGY)

/*
//  Sample data specified by <info> along several LOS starting at locations <pos>,
//  going in the directions (<theta>,<phi>), for the distance <len>, traversing cells
//  with level <floor_level> and above. The spectra can be offset in position
//  space by <pos_offset> (useful for omitting the vicinity of the starting
//  point - for example, if it is a center of a galaxy and we do not want local
//  absorption). Results are stored in <lines[num_specs]>.
*/
void ssSampleLOSData(const ssInfo *info, int num_specs, double *pos[], double theta[], double phi[], double len, int floor_level, double pos_offset, ssLine *lines);

/*
//  Create synthetic spectra specified by <info> for the along several LOS 
//  from <lines[num_specs]>, with systemic velocities <vel> (can be NULL if 
//  there is no systemic vel.), with velocity resolution <vel_pixel>. The spectra
//  can be offset in velocity space by <vel_offset> (useful for omitting the 
//  vicinity of the starting point - for example, if it is a center of a galaxy 
//  and we do not want local absorption). Results are stored in <output[num_specs]>.
*/
void ssComputeSpectra(const ssInfo *info, int num_specs, ssLine *lines, double *vel[], double vel_pixel, double vel_offset, ssSpectrum output[]);

/*
//  Two useful wrappers.
//  ssDumpRandomSpectra returns the mean flux; if path is NULL no
//            files are written, only the mean flux is returned.
//            It uses its own random number generator for repeatability.
//            <batch_size> spectra are computed at a time; internal memory 
//            use is proportional to <batch_size> but small <batch_size>
//            values are less efficient.
//            <dump_mode> sets what is being dumped: if it is < 0, then
//            only raw LOS data are saved, if it is > 0, then only spectra
//            are saved; if it is 0, everything is saved.
*/
double ssDumpRandomSpectra(const ssInfo *info, const char *path, int num, double len, double vel_pixel, int floor_level, int random_seed, int dump_mode);
double ssDumpRandomSpectra2(const ssInfo *info, const char *path, int num, double len, double vel_pixel, int floor_level, int random_seed, int dump_mode, int batch_size);

struct HALO_LIST;
/*
//  ssDumpHalocentricSpectra draws LOS using healpix, hence 
//             nside parameter. 
*/
void ssDumpHalocentricSpectra(const ssInfo *info, const char *path, struct HALO_LIST *halos, double len, double vel_pixel, int floor_level, double vel_offset, double pos_offset, double halo_min_mass, long nside);


#endif /* HYDRO && RADIATIVE_TRANSFER && COSMOLOGY */

#endif
