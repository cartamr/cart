#include "config.h"

#include <math.h>
#include <stdio.h>

#include "auxiliary.h"
#include "iterators.h"
#include "parallel.h"
#include "rt.h"
#include "tree.h"
#include "units.h"

#include "cell_probe.h"


void __find_local_minmax_internal(int var, const cell_probe_t *probe_ptr, int ismax, double *pos, double dist, float *vLim, int *cellLim)
{
  MESH_RUN_DECLARE(level,cell);
  int select;
  double p[3];

  *cellLim = 0;
  if(ismax)
    {
      *vLim = -1.0e35;
    }
  else
    {
      *vLim = 1.0e35;
    }

  cart_assert(probe_ptr!=NULL || (var>=0 && var<num_vars));

  MESH_RUN_OVER_ALL_LEVELS_BEGIN(level);
  MESH_RUN_OVER_CELLS_OF_LEVEL_BEGIN(cell);
  if(cell_is_leaf(cell))
    {
      if(pos!=NULL && dist>0.0)
	{
	  cell_center_position(cell,p);
	  select = (compute_distance_periodic(p,pos) < dist);
	}
      else select = 1;

      if(select)
	{
	  float v;
	  if(probe_ptr != NULL)
	    {
	      v = probe_ptr->Value(level,cell,NULL);
	    }
	  else
	    {
	      v = cell_var(cell,var);
	    }

	  if((ismax && *vLim<v) || (!ismax && *vLim>v))
	    {
	      *vLim = v;
	      *cellLim = cell;
	    }
	}
    }
  MESH_RUN_OVER_CELLS_OF_LEVEL_END;
  MESH_RUN_OVER_LEVELS_END;
}


float __find_minmax_internal(int var, const cell_probe_t *probe_ptr, int ismax, double *pos, double dist)
{
  float vLim;
  int cellLim;
  struct
  {
    float val;
    int rank;
  } in, out;

  __find_local_minmax_internal(var,probe_ptr,ismax,pos,dist,&vLim,&cellLim);
  
  in.val = vLim;
  MPI_Comm_rank(mpi.comm.run,&in.rank);
  MPI_Allreduce(&in,&out,1,MPI_FLOAT_INT,ismax?MPI_MAXLOC:MPI_MINLOC,mpi.comm.run);

  if(pos != NULL)
    {
      if(local_proc_id == out.rank)
	{
	  cell_center_position(cellLim,pos);
	}
      MPI_Bcast(pos,3,MPI_DOUBLE,out.rank,mpi.comm.run);
    }

  return out.val;
}


float find_max_var(int var, double *pos, double dist)
{
  return __find_minmax_internal(var,NULL,1,pos,dist);
}


float find_min_var(int var, double *pos, double dist)
{
  return __find_minmax_internal(var,NULL,0,pos,dist);
}


float find_max_probe(const struct CELL_PROBE probe, double *pos, double dist)
{
  return __find_minmax_internal(-1,&probe,1,pos,dist);
}


float find_min_probe(const struct CELL_PROBE probe, double *pos, double dist)
{
  return __find_minmax_internal(-1,&probe,0,pos,dist);
}


float find_local_max_probe(const struct CELL_PROBE probe, double *pos, double dist)
{
  float vLim;
  int cellLim;

  __find_local_minmax_internal(-1,&probe,1,pos,dist,&vLim,&cellLim);

  return vLim;
}


float find_local_min_probe(const struct CELL_PROBE probe, double *pos, double dist)
{
  float vLim;
  int cellLim;

  __find_local_minmax_internal(-1,&probe,0,pos,dist,&vLim,&cellLim);

  return vLim;
}


void cell_interpolate_at_position(int cell, double pos[], int nvars, int vars[], float vals[])
{
  int i, j, nb;
  int level = cell_level(cell);
  double center[nDim], dd;
  
  for(i=0; i<nvars; i++) vals[i] = 0.0;
      
  cell_center_position(cell,center);
  for(j=0; j<nDim; j++)
    {
      if(pos[j] > center[j])
	{
	  nb = cell_neighbor(cell,2*j+1);
	  dd = (pos[j]-center[j])/cell_size[level];
	}
      else
	{
	  nb = cell_neighbor(cell,2*j+0);
	  dd = (center[j]-pos[j])/cell_size[level];
	}

      cart_assert(dd>=0.0 && dd<=0.5);

      for(i=0; i<nvars; i++)
	{
	  vals[i] += ((1-dd)*cell_var(cell,vars[i])+dd*cell_var(nb,vars[i]))/nDim;
	}
    }
}


#if defined(HYDRO) && defined(RADIATIVE_TRANSFER)
float density_H2(int cell)
{
#ifdef RT_CHEMISTRY

  return 2*cell_H2_density(cell);

#else
  double S0 = units->length*cell_size[cell_level(cell)]/(100*constants->pc); /* as
 calibrated from 6 CHIMP runs */
  double S = (S0 > 0.3) ? S0 : 0.3;
  double D0 = 0.17*(1+1/(1+pow(S,5.0)));
  double U0 = 9*D0/S;
  double N0 = 14*sqrt(D0)/S;
  double D = rtDmwFL(cell);
  double g = sqrt(D*D+D0*D0);
  double q = log(1+pow(0.05/g+rtUmwFS(cell),0.67)/U0*pow(g,0.33));
  double nth = N0*q/g;
  double w = 0.8 + sqrt(q)/pow(S,0.33);
  double x = w*log(units->number_density*cell_gas_density(cell)*0.76/nth);
  double y = -x*(1-0.02*x+0.001*x*x);
  double fracH2 = 1/(1+exp(MIN(30,y)));

  return cell_HI_density(cell)*fracH2;

#endif
}


float density_HI(int cell)
{
#ifdef RT_CHEMISTRY
  return cell_HI_density(cell);
#else
  return MAX(0.0f,cell_HI_density(cell)-density_H2(cell));
#endif
}
#endif /* HYDRO && RADIATIVE_TRANSFER */

