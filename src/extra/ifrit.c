#include "config.h"

#include <math.h>
#include <stdio.h>
#include <string.h>

#include "auxiliary.h"
#include "cosmology.h"
#include "hydro.h"
#include "parallel.h"
#include "particle.h"
#include "rand.h"
#include "rt.h"
#include "starformation.h"
#include "times.h"
#include "tree.h"
#include "units.h"

#include "cell_probe.h"
#include "halo_tools.h"
#include "ifrit.h"
#include "ism.h"


#define ILINT      int
#define MPI_ILINT  MPI_INT

#define I_FLAG_STARS          1
#define I_FLAG_SPLIT_STARS    2
#define I_FLAG_ATTR_IS_MASS   4


#define CHECK
#define MESH_SINGLE_FILE


void ifritMesh_WriteArray(FILE *F, double *bb, int level, int nbin[3], cell_probe_t probe, ifrit_config_t *cfg);
void ifritParticles_WriteArray(FILE *F, double *bb, int flags, float *arr, int idx, const char *label, ifrit_config_t *cfg);


#ifdef STAR_FORMATION
extern double sf_min_stellar_particle_mass;
#endif


void ifritOutputRegion(const char *fileroot, const char *filelabel, int pixel_level, const int nbinIn[], const double pcenIn[], int nprobes, const struct CELL_PROBE *probes, ifrit_config_t *cfg)
{
  ifrit_config_t cfg_default = { mpi.comm.run, 0, 1, 7, NULL };
  int i, rank, size, nbin[3];
  double pcen[3], bb[6], dx;
  char str[999];

  if(cfg == NULL)
    {
      cfg = &cfg_default;
    }

  cart_assert(fileroot);

  MPI_Comm_size(cfg->com,&size);
  MPI_Comm_rank(cfg->com,&rank);

  for(i=0; i<nDim; i++)
    {
      cart_assert(nbinIn[i] > 0);

      nbin[i] = nbinIn[i];
      pcen[i] = pcenIn[i];
    }

  for(i=nDim; i<3; i++)
    {
      nbin[i] = 1;
      pcen[i] = 0.5;
    }

  if(pixel_level < 0)
    {
      int k = num_grid;
      pixel_level = min_level;
      while(k < nbin[0])
	{
	  k *= 2;
	  pixel_level++;
	}

      cart_debug("Level selected for IFrIT::OutputBox: %d",pixel_level);
    }

  dx = pow(0.5,(double)pixel_level);

  bb[0] = pcen[0] - 0.5*dx*nbin[0];
  bb[1] = pcen[0] + 0.5*dx*nbin[0];
  bb[2] = pcen[1] - 0.5*dx*nbin[1];
  bb[3] = pcen[1] + 0.5*dx*nbin[1];
  bb[4] = pcen[2] - 0.5*dx*nbin[2];
  bb[5] = pcen[2] + 0.5*dx*nbin[2];

  if(rank == 0)
    {
      cart_debug("Rebinning for IFrIT at (%lf,%lf,%lf) with cell size %lf",pcen[0],pcen[1],pcen[2],dx);
    }

  if(cfg->num_writers<=0 || cfg->num_writers>size) cfg->num_writers = size;
  if(cfg->num_writers > 1000) cfg->num_writers = 1000;

  if((cfg->flags&IFRIT_FLAG_WRITE_MESH)!=0 && probes!=NULL)
    {
      FILE *F = NULL;
      int num_writers, num_writers_saved = cfg->num_writers;

      if(cfg->num_writers > nbin[2]) cfg->num_writers = nbin[2];
#ifdef MESH_SINGLE_FILE
      num_writers = 1;
#else
      num_writers = cfg->num_writers;
#endif
      if(rank < num_writers)
	{
	  int stride, width;
	  ILINT len;

	  if(num_writers > 1)
	    {
	      if(filelabel != NULL)
		{
		  sprintf(str,"%s-mesh.%03d.%s.bin",fileroot,rank,filelabel);
		}
	      else
		{
		  sprintf(str,"%s-mesh.%03d.bin",fileroot,rank);
		}
	    }
	  else
	    {
	      if(filelabel != NULL)
		{
		  sprintf(str,"%s-mesh.%s.bin",fileroot,filelabel);
		}
	      else
		{
		  sprintf(str,"%s-mesh.bin",fileroot);
		}
	    }

	  F = fopen(str,"w");
	  if(F == NULL)
	    {
	      cart_debug("IFRIT ERROR: unable to open file %s for writing.",str);
	      return;
	    }
  
	  stride = (nbin[2]+num_writers-1)/num_writers;
	  if(rank < (num_writers-1))
	    {
	      width = stride;
	    }
	  else
	    {
	      width = nbin[2] - stride*rank;
	    }

	  cart_debug("Rebinning IFrIT mesh in slices of width=%d",stride);

	  len = 3*sizeof(int);
	  fwrite(&len,sizeof(ILINT),1,F);
	  fwrite(nbin+0,sizeof(int),1,F);
	  fwrite(nbin+1,sizeof(int),1,F);
	  fwrite(&width,sizeof(int),1,F);
	  fwrite(&len,sizeof(ILINT),1,F);
	}

      for(i=0; i<nprobes; i++)
	{
	  ifritMesh_WriteArray(F,bb,pixel_level,nbin,probes[i],cfg);
	}

      if(rank == 0)
	{
	  fclose(F);
	}

      cfg->num_writers = num_writers_saved;
    }

#ifdef PARTICLES
  if((cfg->flags&IFRIT_FLAG_WRITE_PARTICLES) != 0)
    {
      FILE *F = NULL;

      /*
      //  Dark matter particles
      */
      if(rank == 0)
	{
	  char str[999];
	  if(filelabel != NULL)
	    {
	      sprintf(str,"%s-parts.%s.bin",fileroot,filelabel);
	    }
	  else
	    {
	      sprintf(str,"%s-parts.bin",fileroot);
	    }

	  F = fopen(str,"w");
	  if(F == NULL)
	    {
	      cart_debug("IFRIT ERROR: unable to open file %s for writing.",str);
	      return;
	    }
	}

      ifritParticles_WriteArray(F,bb,0,NULL,0,"DM x",cfg);
#if (nDim > 1)
      ifritParticles_WriteArray(F,bb,0,NULL,1,"DM y",cfg);
#if (nDim > 2)
      ifritParticles_WriteArray(F,bb,0,NULL,2,"DM z",cfg);
#endif
#endif

      ifritParticles_WriteArray(F,bb,I_FLAG_ATTR_IS_MASS,particle_mass,-1,"DM mass",cfg);

      if(rank == 0)
	{
	  fclose(F);
	}
    }

#ifdef STAR_FORMATION
  if((cfg->flags&IFRIT_FLAG_WRITE_STELLAR_PARTICLES) != 0)
    {
      FILE *F = NULL;
      float *arr;
      int flags = I_FLAG_STARS;

      if((cfg->flags&IFRIT_FLAG_SPLIT_STELLAR_PARTICLES) != 0) flags |= I_FLAG_SPLIT_STARS;

      /*
      //  Stellar particles
      */
      if(rank == 0)
	{
	  char str[999];
	  if(filelabel != NULL)
	    {
	      sprintf(str,"%s-stars.%s.bin",fileroot,filelabel);
	    }
	  else
	    {
	      sprintf(str,"%s-stars.bin",fileroot);
	    }

	  F = fopen(str,"w");
	  if(F == 0)
	    {
	      cart_debug("IFRIT ERROR: unable to open file %s for writing.",str);
	      return;
	    }
	}

      ifritParticles_WriteArray(F,bb,flags,NULL,0,"stellar x",cfg);
#if (nDim > 1)
      ifritParticles_WriteArray(F,bb,flags,NULL,1,"stellar y",cfg);
#if (nDim > 2)
      ifritParticles_WriteArray(F,bb,flags,NULL,2,"stellar z",cfg);
#endif
#endif

      ifritParticles_WriteArray(F,bb,flags|I_FLAG_ATTR_IS_MASS,particle_mass,-1,"stellar mass",cfg);
      ifritParticles_WriteArray(F,bb,flags|I_FLAG_ATTR_IS_MASS,star_initial_mass,-1,"stellar initial mass",cfg);

      arr = cart_alloc(float,num_particles);

      for(i=0; i<num_particles; i++) if(particle_id[i]!=NULL_PARTICLE && particle_is_star(i))
	{
#ifdef COSMOLOGY
	  arr[i] = tphys_from_tcode(particle_t[i]) - tphys_from_tcode(star_tbirth[i]);
#else
	  arr[i] = particle_t[i] - star_tbirth[i];
#endif
	}

      ifritParticles_WriteArray(F,bb,flags,arr,-1,"stellar age",cfg);

      cart_free(arr);

#ifdef ENRICHMENT
      ifritParticles_WriteArray(F,bb,flags,star_metallicity_II,-1,"stellar metallicity II",cfg);
#ifdef ENRICHMENT_SNIa
      ifritParticles_WriteArray(F,bb,flags,star_metallicity_Ia,-1,"stellar metallicity Ia",cfg);
#endif /* ENRICHMENT_SNIa */
#endif /* ENRICHMENT */

      if(rank == 0)
	{
	  fclose(F);
	}
    }
#endif /* STAR_FORMATION */
#endif /* PARTICLES */
}


/*
// ************************************************
//
//              PRIVATE FUNCTIONS
// (should not be called from outside of this file)
//
// ************************************************
*/
#ifdef PARTICLES
double ifrit_gasdev()
{
  static int iset = 0;
  static double gset;
  double v1, v2, r2, fac;

  if(iset)
    {
      iset = 0;
      return gset;
    }
  else
    {
      do
	{
	  v1 = 2*cart_rand() - 1;
	  v2 = 2*cart_rand() - 1;
	  r2 = v1*v1 + v2*v2;
	}
      while(r2>1.0 || r2<1.0e-300);

      iset = 1;
      fac = sqrt(-2*log(r2)/r2);
      gset = v1*fac;
      return v2*fac;
    }
}


/*
//  Hasn't been converted too multiple writers yet
*/
void ifritParticles_WriteArray(FILE *F, double *bb, int flags, float *arr, int idx, const char *label, ifrit_config_t *cfg)
{
  const float dr = 0.33;
  int i, j, k, size, rank, nsub;
  ILINT nloc, ibuf, nbuf;
  ILINT ntemp, ntot, *nranks;
#ifdef STAR_FORMATION
  float dm_star_min = sf_min_stellar_particle_mass * constants->Msun / units->mass;
#endif
  float *fbuf;
  double *dbuf;
  double pcen[3];
  
  MPI_Comm_size(cfg->com,&size);
  MPI_Comm_rank(cfg->com,&rank);

  for(i=0; i<3; i++) pcen[i] = 0.5*(bb[2*i+0]+bb[2*i+1]);
  
  nloc = 0;
  for(j=0; j<num_particles; j++)
    {
      double x[3];
      for(i=0; i<3; i++)
	{
	  double dx = particle_x[j][i] - pcen[i];
	  if(dx < -0.5*num_grid) dx += num_grid;
	  if(dx >  0.5*num_grid) dx -= num_grid;
	  x[i] = pcen[i] + dx;
	}
      
      if(particle_id[j]!=NULL_PARTICLE &&
#ifdef STAR_FORMATION
	 particle_id_is_star(particle_id[j])==(flags&I_FLAG_STARS) &&
#endif
	 x[0]>bb[0] && x[0]<bb[1]
#if (nDim > 1)
	 && x[1]>bb[2] && x[1]<bb[3]
#if (nDim > 2)
	 && x[2]>bb[4] && x[2]<bb[5]
#endif
#endif
     && (cfg->selection==NULL || cfg->selection[j])
	 )
	{
      cart_assert(cfg->selection==NULL || cfg->selection[j]==0 || cfg->selection[j]==1)
	  nsub = 1;
#ifdef STAR_FORMATION
	  if(particle_id_is_star(particle_id[j])==(flags&I_FLAG_STARS) && (flags&I_FLAG_SPLIT_STARS)!=0)
	    {
	      nsub = (int)(0.5+particle_mass[j]/dm_star_min);
	      cart_assert(nsub > 0);
	    }
#endif
	  nloc += nsub;
	}
    }

  if(rank > 0)
    {     
      MPI_Send(&nloc,1,MPI_ILINT,0,1,cfg->com);
    }
  else
    {
      nranks = cart_alloc(ILINT,size);

      nranks[0] = nloc;
      for(i=1; i<size; i++)
	{
	  MPI_Recv(nranks+i,1,MPI_ILINT,i,1,cfg->com,MPI_STATUS_IGNORE); 
	}

      nbuf = 0;
      ntot = 0;
      for(i=0; i<size; i++)
	{
	  if(nranks[i] > nbuf) nbuf = nranks[i];
	  ntot += nranks[i];
	}

      if(cfg->max_buffer_size>0 && cfg->max_buffer_size<nbuf) nbuf = cfg->max_buffer_size;

      cart_debug("Saving %ld particles (%s)...",ntot,label);

      cart_assert(F != NULL);

      if(idx == 0)
	{
	  float w;

	  ntemp = sizeof(ILINT); fwrite(&ntemp,sizeof(ILINT),1,F);
	  ntemp = ntot;          fwrite(&ntemp,sizeof(ILINT),1,F);
	  ntemp = sizeof(ILINT); fwrite(&ntemp,sizeof(ILINT),1,F);

	  ntemp = 6*sizeof(float); fwrite(&ntemp,sizeof(ILINT),1,F);
	  w = bb[0]; fwrite(&w,sizeof(float),1,F);
	  w = bb[2]; fwrite(&w,sizeof(float),1,F);
	  w = bb[4]; fwrite(&w,sizeof(float),1,F);
	  w = bb[1]; fwrite(&w,sizeof(float),1,F);
	  w = bb[3]; fwrite(&w,sizeof(float),1,F);
	  w = bb[5]; fwrite(&w,sizeof(float),1,F);
	  ntemp = 6*sizeof(float); fwrite(&ntemp,sizeof(ILINT),1,F);
	}

      if(idx > -1) ntemp = ntot*sizeof(double); else ntemp = ntot*sizeof(float);

      fwrite(&ntemp,sizeof(ILINT),1,F);
    }

  MPI_Bcast(&nbuf,1,MPI_ILINT,0,cfg->com);

  if(idx > -1)
    {
      dbuf = cart_alloc(double,nbuf);
    }
  else
    {
      cart_assert(arr);
      fbuf = cart_alloc(float,nbuf);
    }

  ibuf = 0;
  for(j=0; j<num_particles; j++)
    {
      double x[3];
      for(i=0; i<3; i++)
	{
	  double dx = particle_x[j][i] - pcen[i];
	  if(dx < -0.5*num_grid) dx += num_grid;
	  if(dx >  0.5*num_grid) dx -= num_grid;
	  x[i] = pcen[i] + dx;
	}
      
      if(particle_id[j]!=NULL_PARTICLE &&
#ifdef STAR_FORMATION
	 particle_id_is_star(particle_id[j])==(flags&I_FLAG_STARS) &&
#endif
	 x[0]>bb[0] && x[0]<bb[1]
#if (nDim > 1)
	 && x[1]>bb[2] && x[1]<bb[3]
#if (nDim > 2)
	 && x[2]>bb[4] && x[2]<bb[5]
#endif
#endif
     && (cfg->selection==NULL || cfg->selection[j])
	 )
	{
	  nsub = 1;
#ifdef STAR_FORMATION
	  if(particle_id_is_star(particle_id[j])==(flags&I_FLAG_STARS) && (flags&I_FLAG_SPLIT_STARS)!=0)
	    {
	      nsub = (int)(0.5+particle_mass[j]/dm_star_min);
	      cart_assert(nsub > 0);
	    }
#endif

	  for(k=0; k<nsub; k++)
	    {
	      if(idx > -1)
		{
		  dbuf[ibuf++] = x[idx] + dr*cell_size[particle_level[j]]*(k>0?ifrit_gasdev():0);
		  if(ibuf == nbuf)
		    {
		      if(rank > 0)
			{
			  MPI_Send(dbuf,nbuf,MPI_DOUBLE,0,2,cfg->com);
			}
		      else
			{
			  fwrite(dbuf,sizeof(double),nbuf,F);
			}
		      ibuf = 0;
		    }
		}
	      else
		{
		  float val = arr[j];
		  if((flags&I_FLAG_ATTR_IS_MASS) != 0) val /= nsub;
		  fbuf[ibuf++] = val;
		  if(ibuf == nbuf)
		    {
		      if(rank > 0)
			{
			  MPI_Send(fbuf,nbuf,MPI_FLOAT,0,2,cfg->com);
			}
		      else
			{
			  fwrite(fbuf,sizeof(float),nbuf,F);
			}
		      ibuf = 0;
		    }
		}
	    }
	}
    }

  if(ibuf > 0)
    {
      if(idx > -1)
	{
	  if(rank > 0)
	    {
	      MPI_Send(dbuf,ibuf,MPI_DOUBLE,0,2,cfg->com);
	    }
	  else
	    {
	      fwrite(dbuf,sizeof(double),ibuf,F);
	    }
	}
      else
	{
	  if(rank > 0)
	    {
	      MPI_Send(fbuf,ibuf,MPI_FLOAT,0,2,cfg->com);
	    }
	  else
	    {
	      fwrite(fbuf,sizeof(float),ibuf,F);
	    }
	}
    }

  if(rank == 0)
    {
      for(i=1; i<size; i++)
	{
	  while(nranks[i] > 0)
	    {
	      ibuf = nbuf;
	      if(ibuf > nranks[i]) ibuf = nranks[i];

	      if(idx > -1)
		{
		  MPI_Recv(dbuf,ibuf,MPI_DOUBLE,i,2,cfg->com,MPI_STATUS_IGNORE);
		  fwrite(dbuf,sizeof(double),ibuf,F);
		}
	      else
		{
		  MPI_Recv(fbuf,ibuf,MPI_FLOAT,i,2,cfg->com,MPI_STATUS_IGNORE);
		  fwrite(fbuf,sizeof(float),ibuf,F);
		}

	      nranks[i] -= ibuf;
#ifdef CHECK
	      if(nranks[i] == 0)
		{
		  cart_debug("...received all from %d",i);
		}
#endif
	    }
	}
      
      fwrite(&ntemp,sizeof(ILINT),1,F);

      cart_free(nranks);
    }

  if(idx > -1)
    {
      cart_free(dbuf);
    }
  else
    {
      cart_free(fbuf);
    }
}
#endif /* PARTICLES */


void ifritMesh_WriteArray1(FILE *F, double *bb, int level, int nbin[3], cell_probe_t probe, ifrit_config_t *cfg);
void ifritMesh_WriteArray2(FILE *F, double *bb, int level, int nbin[3], cell_probe_t probe, ifrit_config_t *cfg);
void ifritMesh_WriteArray3(FILE *F, double *bb, int level, int nbin[3], cell_probe_t probe, ifrit_config_t *cfg);


void ifritMesh_WriteArray(FILE *F, double *bb, int level, int nbin[3], cell_probe_t probe, ifrit_config_t *cfg)
{
#ifdef MESH_SINGLE_FILE
  ifritMesh_WriteArray3(F,bb,level,nbin,probe,cfg);
#else
  ifritMesh_WriteArray2(F,bb,level,nbin,probe,cfg);
#endif
}


/*
//  Use buffers to send indicies and values to the head node, a-la Particle_WriteArray.
*/
void ifritMesh_WriteArray1(FILE *F, double *bb, int level, int nbin[3], cell_probe_t probe, ifrit_config_t *cfg)
{
  int writer, size, rank, stride, width, cell;
  ILINT ntot, nloc, ibuf, nbuf;
  ILINT ntemp, *nranks;
  float **fbuffers, *fbuf, *arr;
  ILINT **lbuffers, *lbuf;
  double pos[3], dx;
  MPI_Request **reqs1, **reqs2;
  int *ireqs, nreqs;

  cart_assert(probe.Value != NULL);

  MPI_Comm_size(cfg->com,&size);
  MPI_Comm_rank(cfg->com,&rank);

  stride = (nbin[2]+cfg->num_writers-1)/cfg->num_writers;

  cart_debug("Saving %s",probe.Name);

  fbuffers = cart_alloc(float*,cfg->num_writers);
  lbuffers = cart_alloc(ILINT*,cfg->num_writers);

  if(rank < cfg->num_writers)
    {
      nranks = cart_alloc(ILINT,size);

      cart_assert(F != NULL);

      if(rank < (cfg->num_writers-1))
	{
	  width = stride;
	}
      else
	{
	  width = nbin[2] - stride*rank;
	}

      ntemp = (ILINT)nbin[0]*nbin[1]*width*sizeof(float);
      fwrite(&ntemp,sizeof(ILINT),1,F);
      arr = cart_alloc(float,(ILINT)nbin[0]*nbin[1]*width);
    }

  dx = pow(0.5,(double)level);
  nbuf = 0;

  for(writer=0; writer<cfg->num_writers; writer++)
    {
      int i, j, k, kk = stride*writer, kmax = stride;
      if(writer == (cfg->num_writers-1)) kmax = nbin[2] - kk;

#ifdef CHECK
      if(rank < cfg->num_writers)
	{
	  memset(arr,0,ntemp);
	}
#endif

      nloc = 0;
      for(k=0; k<kmax; k++)
	{
	  pos[2] = bb[4] + dx*(kk+k+0.5);
	  pos[2] -= num_grid*floor(pos[2]/num_grid);
	  for(j=0; j<nbin[1]; j++)
	    {
	      pos[1] = bb[2] + dx*(j+0.5);
	      pos[1] -= num_grid*floor(pos[1]/num_grid);
	      for(i=0; i<nbin[0]; i++)
		{
		  pos[0] = bb[0] + dx*(i+0.5);
		  pos[0] -= num_grid*floor(pos[0]/num_grid);
		  cell = cell_find_position_above_level(level,pos);
		  if(cell!=-1 && cell_is_local(cell))
		    {
		      nloc++;
		    }
		}
	    }
	}

      if(rank != writer)
	{     
	  MPI_Send(&nloc,1,MPI_ILINT,writer,1,cfg->com);
	}
      else
	{
	  nranks[rank] = nloc;
	  for(i=0; i<size; i++) if(i != rank)
	    {
	      MPI_Recv(nranks+i,1,MPI_ILINT,i,1,cfg->com,MPI_STATUS_IGNORE); 
	    }

	  ntot = 0;
	  for(i=0; i<size; i++)
	    {
	      if(nranks[i] > nbuf) nbuf = nranks[i];
	      ntot += nranks[i];
	    }

	  cart_debug("Saving %ld cells...",ntot);

	  cart_assert(ntot == (ILINT)nbin[0]*nbin[1]*width);
	}
    }

  ibuf = nbuf;
  MPI_Allreduce(&ibuf,&nbuf,1,MPI_ILINT,MPI_MAX,cfg->com);
  cart_assert(nbuf > 0); 

  if(cfg->max_buffer_size>0 && cfg->max_buffer_size<nbuf)
    {
      nreqs = (nbuf+cfg->max_buffer_size-1)/cfg->max_buffer_size;
      nbuf = cfg->max_buffer_size;
    }
  else
    {
      nreqs = 1;
    }

#ifdef CHECK
  cart_debug("buffer size=%ld",nbuf);
#endif
 
  /*
  //  Setup requests
  */
  ireqs = cart_alloc(int,cfg->num_writers); 
  reqs1 = cart_alloc(MPI_Request*,cfg->num_writers); 
  reqs2 = cart_alloc(MPI_Request*,cfg->num_writers); 
  for(writer=0; writer<cfg->num_writers; writer++)
    {
      int i;

      ireqs[writer] = 0;
      reqs1[writer] = cart_alloc(MPI_Request,nreqs); 
      reqs2[writer] = cart_alloc(MPI_Request,nreqs); 

      for(i=0; i<nreqs; i++)
	{
	  reqs1[writer][i] = MPI_REQUEST_NULL;
	  reqs2[writer][i] = MPI_REQUEST_NULL;
	}
    }

  for(writer=0; writer<cfg->num_writers; writer++)
    {
      int i, j, k, m, kk = stride*writer, kmax = stride;
      if(writer == (cfg->num_writers-1)) kmax = nbin[2] - kk;

      fbuf = fbuffers[writer] = cart_alloc(float,nbuf);
      lbuf = lbuffers[writer] = cart_alloc(ILINT,nbuf);

      ibuf = 0;
      for(k=0; k<kmax; k++)
	{
	  pos[2] = bb[4] + dx*(kk+k+0.5);
	  pos[2] -= num_grid*floor(pos[2]/num_grid);
	  for(j=0; j<nbin[1]; j++)
	    {
	      pos[1] = bb[2] + dx*(j+0.5);
	      pos[1] -= num_grid*floor(pos[1]/num_grid);
	      for(i=0; i<nbin[0]; i++)
		{
		  pos[0] = bb[0] + dx*(i+0.5);
		  pos[0] -= num_grid*floor(pos[0]/num_grid);
		  cell = cell_find_position_above_level(level,pos);
		  if(cell!=-1 && cell_is_local(cell))
		    {
		      fbuf[ibuf] = probe.Value(level,cell,NULL);
		      lbuf[ibuf] = i + (ILINT)nbin[0]*(j+nbin[1]*k);
		      ibuf++;
		      if(ibuf == nbuf)
			{
			  if(rank != writer)
			    {
			      MPI_Isend(lbuf,ibuf,MPI_ILINT,writer,2,cfg->com,reqs1[writer]+ireqs[writer]);
			      MPI_Isend(fbuf,ibuf,MPI_FLOAT,writer,2,cfg->com,reqs2[writer]+ireqs[writer]);
			      ireqs[writer]++;
			    }
			  else
			    {
			      for(m=0; m<ibuf; m++)
				{
#ifdef CHECK
				  if(lbuf[m]<0 || lbuf[m]>=ntot || arr[lbuf[m]] > 0.0)
				    {
				      cart_error("Internal check failed: A lbuf[%d]=%ld, arr[]=%g",m,lbuf[m],arr[lbuf[m]]);
				    }
#endif		      
				  arr[lbuf[m]] = fbuf[m];
				}
			    }
			  ibuf = 0;
			}
		    }
		}
	    }
	}

      if(ibuf > 0)
	{
	  if(rank != writer)
	    {
	      MPI_Isend(lbuf,ibuf,MPI_ILINT,writer,2,cfg->com,reqs1[writer]+ireqs[writer]);
	      MPI_Isend(fbuf,ibuf,MPI_FLOAT,writer,2,cfg->com,reqs2[writer]+ireqs[writer]);
	      ireqs[writer]++;
	    }
	  else
	    {
	      for(m=0; m<ibuf; m++)
		{
#ifdef CHECK
		  if(lbuf[m]<0 || lbuf[m]>=ntot || arr[lbuf[m]] > 0.0)
		    {
		      cart_error("Internal check failed: B lbuf[%d]=%ld arr[]=%g",m,lbuf[m],arr[lbuf[m]]);
		    }
#endif		      
		  arr[lbuf[m]] = fbuf[m];
		}
	    }
	}
    }

  if(rank < cfg->num_writers)
    {
      int i, work, flag;

      fbuf = fbuffers[rank];
      lbuf = lbuffers[rank];

      do
	{
	  int m;

	  work = 0;

	  for(i=0; i<size; i++) if(i!=rank && nranks[i]>0)
	    {
	      work = 1;
		  
	      MPI_Iprobe(i,2,cfg->com,&flag,MPI_STATUS_IGNORE);
	      if(!flag) continue;

	      ibuf = nbuf;
	      if(ibuf > nranks[i]) ibuf = nranks[i];

	      MPI_Recv(lbuf,ibuf,MPI_ILINT,i,2,cfg->com,MPI_STATUS_IGNORE);
	      MPI_Recv(fbuf,ibuf,MPI_FLOAT,i,2,cfg->com,MPI_STATUS_IGNORE);
	      for(m=0; m<ibuf; m++)
		{
#ifdef CHECK
		  if(lbuf[m]<0 || lbuf[m]>=ntot || arr[lbuf[m]] > 0.0)
		    {
		      cart_error("Internal check failed: recv(%d) lbuf[%d]=%ld, arr[]=%g",i,m,lbuf[m],arr[lbuf[m]]);
		    }
#endif		      
		  arr[lbuf[m]] = fbuf[m];
		}

	      nranks[i] -= ibuf;
	    }
	}
      while(work);

#ifdef CHECK
      for(i=0; i<size; i++) if(i != rank)
	{
	  if(nranks[i] != 0)
	    {
	      cart_error("Internal check failed: did not received %ld values from %d",nranks[i],i);
	    }
	}
#endif

      fwrite(arr,sizeof(float),(ILINT)nbin[0]*nbin[1]*width,F);
      fwrite(&ntemp,sizeof(ILINT),1,F);

      cart_free(nranks);
      cart_free(arr);
    }

  /*
  //  Wait for all to complete
  */
  MPI_Barrier(cfg->com);

  for(writer=0; writer<cfg->num_writers; writer++)
    {
      int i,flag;

      /*
      //  MPI_Test deletes the request when it returns flag=1
      */
      for(i=0; i<ireqs[writer]; i++)
	{
	  MPI_Test(reqs1[writer]+i,&flag,MPI_STATUS_IGNORE);
	  if(flag == 0)
	    {
	      cart_error("Internal check failed: uncompleted request 1, writer=%d",writer);
	    }
	  MPI_Test(reqs2[writer]+i,&flag,MPI_STATUS_IGNORE);
	  if(flag == 0)
	    {
	      cart_error("Internal check failed: uncompleted request 2, writer=%d",writer);
	    }
	}

      cart_free(reqs2[writer]);
      cart_free(reqs1[writer]);
      cart_free(lbuffers[writer]);
      cart_free(fbuffers[writer]);
    }

  cart_free(reqs2);
  cart_free(reqs1);
  cart_free(ireqs);
  cart_free(lbuffers);
  cart_free(fbuffers);
}


/*
//  Use MPI_Reduce to reduce array slabs from each node, will transfer a large number of zeroes.
*/
void ifritMesh_WriteArray2(FILE *F, double *bb, int level, int nbin[3], cell_probe_t probe, ifrit_config_t *cfg)
{
  int writer, size, rank, stride, width, cell, nmaxint;
  ILINT ntot, nmax;
  ILINT ntemp;
  float *arr;
  double pos[3], dx;

  cart_assert(probe.Value != NULL);

  MPI_Comm_size(cfg->com,&size);
  MPI_Comm_rank(cfg->com,&rank);

  stride = (nbin[2]+cfg->num_writers-1)/cfg->num_writers;

  cart_debug("Saving %s",probe.Name);

  nmax = (ILINT)nbin[0]*nbin[1]*stride;
  nmaxint = nbin[0]*nbin[1]*stride;
  cart_assert(nmax == nmaxint); /* MPI_Reduce requires an int count */

  arr = cart_alloc(float,nmax);

  if(rank < cfg->num_writers)
    {
      cart_assert(F != NULL);

      if(rank < (cfg->num_writers-1))
	{
	  width = stride;
	}
      else
	{
	  width = nbin[2] - stride*rank;
	}

      ntemp = (ILINT)nbin[0]*nbin[1]*width*sizeof(float);
      fwrite(&ntemp,sizeof(ILINT),1,F);
    }

  dx = pow(0.5,(double)level);

  for(writer=0; writer<cfg->num_writers; writer++)
    {
      int key, color;
      MPI_Comm writer_com;
      int i, j, k, kk = stride*writer, kmax = stride;

      if(writer == (cfg->num_writers-1)) kmax = nbin[2] - kk;

      memset(arr,0,nmax*sizeof(float));

      ntot = 0;
      for(k=0; k<kmax; k++)
	{
	  pos[2] = bb[4] + dx*(kk+k+0.5);
	  pos[2] -= num_grid*floor(pos[2]/num_grid);
	  for(j=0; j<nbin[1]; j++)
	    {
	      pos[1] = bb[2] + dx*(j+0.5);
	      pos[1] -= num_grid*floor(pos[1]/num_grid);
	      for(i=0; i<nbin[0]; i++)
		{
		  pos[0] = bb[0] + dx*(i+0.5);
		  pos[0] -= num_grid*floor(pos[0]/num_grid);
		  cell = cell_find_position_above_level(level,pos);

		  if(cell!=-1 && cell_is_local(cell))
		    {
		      arr[i+(ILINT)nbin[0]*(j+nbin[1]*k)] = probe.Value(level,cell,NULL);
		      ntot++;
		    }
		}
	    }
	}

      if(rank == writer)
	{
	  color = 1;
	  key = 0;
	}
      else
	{
	  color = (ntot > 0);
	  key = rank + 1;
	}

      MPI_Comm_split(cfg->com,color,key,&writer_com);

      if(color != 0)
	{
	  if((cfg->flags & IFRIT_FLAG_MPI_ALLOW_BUFFER_ALIASING) == 0)
	    {
	      float *buf = NULL;
	      if(rank == writer)
		{
		  buf = cart_alloc(float,nmax);
		}
	      MPI_Reduce(arr,buf,nmax,MPI_FLOAT,MPI_SUM,0,writer_com);
	      if(rank == writer)
		{
		  cart_free(arr);
		  arr = buf;
		}
	    }
	  else
	    {
	      MPI_Reduce(arr,arr,nmax,MPI_FLOAT,MPI_SUM,0,writer_com);
	    }
	}

      MPI_Comm_free(&writer_com);

      if(rank == writer)
	{
	  ILINT l;
#ifdef CHECK
	  cart_assert(width == kmax);

	  for(l=0; l<(ILINT)nbin[0]*nbin[1]*width; l++)
	    {
	      if(arr[l] == 0.0)
		{
		  cart_error("Internal check failed: unfilled array value arr[%ld]=%g (%d,%d,%d), writer=%d, kk=%d",l,arr[l],l%nbin[0],(l/nbin[0])%nbin[1],l/nbin[0]/nbin[1],writer,kk);
		}
	    }
#endif
	  fwrite(arr,sizeof(float),(ILINT)nbin[0]*nbin[1]*width,F);
	  fwrite(&ntemp,sizeof(ILINT),1,F);
	}
    }

  cart_free(arr);
}


/*
//  Use MPI_Reduce to reduce array slabs from each node, will transfer a large number of zeroes.
//  Number of slabs is cfg->num_writers
*/
void ifritMesh_WriteArray3(FILE *F, double *bb, int level, int nbin[3], cell_probe_t probe, ifrit_config_t *cfg)
{
  int slab, size, rank, stride, cell, nmaxint;
  ILINT ntot, nmax;
  ILINT ntemp;
  float *arr;
  double pos[3], dx;

  cart_assert(probe.Value != NULL);

  MPI_Comm_size(cfg->com,&size);
  MPI_Comm_rank(cfg->com,&rank);

  stride = (nbin[2]+cfg->num_writers-1)/cfg->num_writers;

  cart_debug("Saving %s",probe.Name);

  nmax = (ILINT)nbin[0]*nbin[1]*stride;
  nmaxint = nbin[0]*nbin[1]*stride;
  cart_assert(nmax == nmaxint); /* MPI_Reduce requires an int count */

  arr = cart_alloc(float,nmax);

  if(rank == 0)
    {
      cart_assert(F != NULL);

      ntemp = (ILINT)nbin[0]*nbin[1]*nbin[2]*sizeof(float);
      fwrite(&ntemp,sizeof(ILINT),1,F);
    }

  dx = pow(0.5,(double)level);

  for(slab=0; slab<cfg->num_writers; slab++)
    {
      int i, j, k, kk = stride*slab, kmax = stride;

      if(slab == (cfg->num_writers-1)) kmax = nbin[2] - kk;

      cart_debug("... writing slab %d (of %d)",slab,cfg->num_writers);

      ntot = (ILINT)nbin[0]*nbin[1]*kmax;

      memset(arr,0,ntot*sizeof(float));

      for(k=0; k<kmax; k++)
	{
	  pos[2] = bb[4] + dx*(kk+k+0.5);
	  pos[2] -= num_grid*floor(pos[2]/num_grid);
	  for(j=0; j<nbin[1]; j++)
	    {
	      pos[1] = bb[2] + dx*(j+0.5);
	      pos[1] -= num_grid*floor(pos[1]/num_grid);
	      for(i=0; i<nbin[0]; i++)
		{
		  pos[0] = bb[0] + dx*(i+0.5);
		  pos[0] -= num_grid*floor(pos[0]/num_grid);
		  cell = cell_find_position_above_level(level,pos);
		  if(cell!=-1 && cell_is_local(cell))
		    {
		      arr[i+(ILINT)nbin[0]*(j+nbin[1]*k)] = probe.Value(level,cell,NULL);
		    }
		}
	    }
	}

      if((cfg->flags & IFRIT_FLAG_MPI_ALLOW_BUFFER_ALIASING) == 0)
	{
	  float *buf = NULL;
	  if(rank == 0)
	    {
	      buf = cart_alloc(float,nmax);
	    }
	  MPI_Reduce(arr,buf,ntot,MPI_FLOAT,MPI_SUM,0,cfg->com);
	  if(rank == 0)
	    {
	      cart_free(arr);
	      arr = buf;
	    }
	}
      else
	{
	  MPI_Reduce(arr,arr,ntot,MPI_FLOAT,MPI_SUM,0,cfg->com);
	}

      if(rank == 0)
	{
	  fwrite(arr,sizeof(float),ntot,F);
	}
    }

  if(rank == 0)
    {
      fwrite(&ntemp,sizeof(ILINT),1,F);
    }

  cart_free(arr);
}

