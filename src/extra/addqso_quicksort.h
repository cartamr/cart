#ifndef __ADDQSO__
#define __ADDQSO__

#ifndef CONFIGURED
#error "Missing config.h include"
#endif

#if defined(COSMOLOGY) && defined(HYDRO) && defined(STAR_FORMATION) && defined(RADIATIVE_TRANSFER)

extern double qso_lum; // in erg/s
extern int MAXNUM_QSO_TRACER;
extern const char* PID_fpath;
void config_init_qsolum();
void config_verf_qsolum();
void config_init_qsohalo();
void config_verf_qsohalo();
void qso_initialize_tracer();
void qsoReadPID();
void update_tracer(double* tracer_pos,double* buffer_pos_gather,int ntracer, int comm_size);
void qsoInit();
void qsoFindPosition(int level, MPI_Comm level_comm);
void qsoAddSource(int level);
int cmpfunc (const void * aptr, const void * bptr);
double find_median(int n,double original_arr[]);

#endif /*if defined(rt etc) */
#endif /*__ADDQSO__*/
