#ifndef __EXT_CD_H__
#define __EXT_CD_H__

#ifndef CONFIGURED
#error "Missing config.h include."
#endif


/*
//  Buffer for keeping information used during the LOS traversal
*/
#ifndef CD_MAX_NVALS
#define CD_MAX_NVALS 4
#endif

struct CELL_PROBE;

typedef struct cdDataType
{
  double val[CD_MAX_NVALS];
  double len;
}
cdData;


/*
//  Compute column densities of <nvars> (not more than CD_MAX_NVALS) grid 
//  variables (with indicies supplied in <vars[]> array, sampled over the
//  sky using the HealPIX binning with <nside> bins per section (total 
//  number of rays is 12*nside^2) with common origin <pos>, sampling radii 
//  between <rstart> and <rend>, not deeper than <floor_level>. The final 
//  result is returned in <output[]>, whose dimension must be 12*nside^2.
*/
void cdTraverseSky(int nvals, const struct CELL_PROBE *probes, int nside, double pos[3], double rstart, double rend, int floor_level, cdData output[]);

#endif  /* __EXT_CD_H__ */
