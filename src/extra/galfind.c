#include "config.h"

#if defined(COSMOLOGY) && defined(PARTICLES) && defined(STAR_FORMATION)


#include <limits.h>
#include <math.h>
#include <string.h>


#include "galfind.h"

#include "auxiliary.h"
#include "cell_buffer.h"
#include "halos.h"
#include "halo_finder.h"
#include "iterators.h"
#include "parallel.h"
#include "particle.h"
#include "tree.h"
#include "units.h"

#ifdef _OPENMP
#include <omp.h>
#endif


#define NPOS        (2*nDim+1)

extern int halo_particle_list_flag;


void _gfFunAdd(void *invec, void *inoutvec, int *len, MPI_Datatype *dtype)
{
  int i, j;
  double dx;

  for(i=0; i<*len; i++)
    {
      double *pIn = (double *)invec + NPOS*i;
      double *pOut = (double *)inoutvec + NPOS*i;

      if(pIn[nDim] > 0.0)
	{
	  //
	  //  Mass
	  //
	  pOut[nDim] += pIn[nDim];
	  double fm = pIn[nDim]/pOut[nDim];

	  //
	  //  Positions
	  //
	  for(j=0; j<nDim; j++)
	    {
	      dx = pIn[j] - pOut[j];
	      if(dx >  0.5*num_grid) dx -= num_grid;
	      if(dx < -0.5*num_grid) dx += num_grid;
	      pOut[j] += fm*dx;
	    }

	  //
	  //  Velocities
	  //
	  for(j=nDim+1; j<NPOS; j++)
	    {
	      pOut[j] += fm*(pIn[j]-pOut[j]);
	    }
	}
    }
}


int _gfIdx(int cell, int var)
{
  union
  {
    int iv;
    float fv;
  } 
  d;
  d.fv = cell_var(cell,var);
  return d.iv;
}


void _gfSetIdx(int cell, int val, int var)
{
  union
  {
    int iv;
    float fv;
  }
  d;
  d.iv = val;
  cell_var(cell,var) = d.fv;
}


halo_list* _gfFinish(int numGals, int numCut, int varTemp);
void _gfCompressIds(int *maxold, int varTemp);


halo_list* gfRun(int varStellarDensity, int varTemp, double denCut, int numCut, int maxiter)
{
  MESH_RUN_DECLARE(level,cell);
  double d, m, ms1 = 0, ms2 = 0;
  double ms[2], lms[2];
  int iter, nmod, maxidx;
  int with_update;
  float *save_field = NULL;
  halo_list *ret = NULL;

  halo_particle_list_flag = 0;

  if(varTemp == 0)
    {
      int i;

      save_field = cart_alloc(float,num_cells);

      for(i=0; i<num_cells; i++)
	{
	  save_field[i] = cell_var(i,VAR_ACCEL);
	}
      varTemp = VAR_ACCEL;
    }

  //
  //  Set zeros onto buffer cells
  //
  MESH_RUN_OVER_ALL_LEVELS_BEGIN2(level,CELL_TYPE_BUFFER);
#pragma omp parallel for default(none), private(cell,_Index), shared(cell_vars_data,level,_Num_level_cells,_Level_cells,cell_child_oct,varTemp)
  MESH_RUN_OVER_CELLS_OF_LEVEL_BEGIN(cell);

  _gfSetIdx(cell,0,varTemp);

  MESH_RUN_OVER_CELLS_OF_LEVEL_END;
  MESH_RUN_OVER_LEVELS_END;

  //
  //  Set cell indexes onto local cells
  //
  MESH_RUN_OVER_ALL_LEVELS_BEGIN(level);
#ifdef OPENMP_DECLARE_CONST
#pragma omp parallel for default(none), private(cell,_Index,d,m), shared(cell_vars_data,level,_Num_level_cells,_Level_cells,cell_child_oct,units,constants,cell_volume,denCut,varStellarDensity,varTemp), reduction(+:ms1,ms2)
#else
#pragma omp parallel for default(none), private(cell,_Index,d,m), shared(cell_vars_data,level,_Num_level_cells,_Level_cells,cell_child_oct,units,constants,denCut,varStellarDensity,varTemp), reduction(+:ms1,ms2)
#endif
  MESH_RUN_OVER_CELLS_OF_LEVEL_BEGIN(cell);

  d = cell_var(cell,varStellarDensity)*units->density/constants->Msun*constants->pc*constants->pc*constants->pc;
  m = cell_var(cell,varStellarDensity)*cell_volume[level];

  if(d > denCut)
    {
      _gfSetIdx(cell,1+cell,varTemp);
      ms1 += m;
    }
  else
    {
      _gfSetIdx(cell,0,varTemp);
      ms2 += m;
    }

  MESH_RUN_OVER_CELLS_OF_LEVEL_END;
  MESH_RUN_OVER_LEVELS_END;

  lms[0] = ms1 + ms2;
  lms[1] = ms2;
  MPI_Allreduce(lms,ms,2,MPI_DOUBLE,MPI_SUM,mpi.comm.run);
  
  if(ms[0] > 0.0) cart_debug("GalFind: cut=%le missed=%le",denCut,ms[1]/ms[0]);

  //
  //  Main loop
  //
  iter = 0;
  nmod = num_cells;
  with_update = 0;
  while(nmod>0 && (maxiter<1 || iter<maxiter))
    {
      int idx;

      MESH_RUN_DECLARE(level,cell);

      nmod = 0;
      maxidx = 0;
      iter++;

      MESH_RUN_OVER_ALL_LEVELS_BEGIN(level);
      MESH_RUN_OVER_CELLS_OF_LEVEL_BEGIN(cell);
  
      idx = _gfIdx(cell,varTemp);
      if(idx > 0)
	{
	  int j, inb, nb[num_neighbors], minidx = idx;

	  cell_all_neighbors(cell,nb);

	  for(j=0; j<num_neighbors; j++)
	    {
	      inb = _gfIdx(nb[j],varTemp);
	      if(inb>0 && inb<minidx) minidx = inb;
	    }
	  if(minidx < idx)
	    {
	      idx = minidx;
	      _gfSetIdx(cell,idx,varTemp);
	      nmod++;
	    }

	  for(j=0; j<num_neighbors; j++)
	    {
	      if(_gfIdx(nb[j],varTemp)>idx && cell_is_local(nb[j]))
		{
		  _gfSetIdx(nb[j],idx,varTemp);
		  nmod++;
		}
	    }

	  if(maxidx < idx)
	    {
	      maxidx = idx;
	    }
	}

      MESH_RUN_OVER_CELLS_OF_LEVEL_END;
      MESH_RUN_OVER_LEVELS_END;
      
      if(nmod==0 && !with_update)
	{
	  int idx;

	  with_update = 1;
	  _gfCompressIds(&maxidx,varTemp);

	  if(maxidx > INT_MAX/num_procs)
	    {
	      cart_error("Too many halos to address with an int: ng=%d, np=%d",maxidx,num_procs);
	    } 

	  MESH_RUN_OVER_ALL_LEVELS_BEGIN(level);
#pragma omp parallel for default(none), private(cell,_Index,idx), shared(level,_Num_level_cells,_Level_cells,cell_child_oct,num_procs,local_proc_id,varTemp)
	  MESH_RUN_OVER_CELLS_OF_LEVEL_BEGIN(cell);
  
	  idx = _gfIdx(cell,varTemp);
	  if(idx > 0) _gfSetIdx(cell,idx*num_procs+local_proc_id,varTemp);

	  MESH_RUN_OVER_CELLS_OF_LEVEL_END;
	  MESH_RUN_OVER_LEVELS_END;

	  nmod = num_procs - 1;
	}

      if(with_update)
	{
	  int nglob, vars[] = { VAR_ACCEL };

	  MPI_Allreduce(&nmod,&nglob,1,MPI_INT,MPI_SUM,mpi.comm.run);
	  nmod = nglob;
	  MPI_Allreduce(&maxidx,&nglob,1,MPI_INT,MPI_MAX,mpi.comm.run);
	  maxidx = nglob;

	  MESH_RUN_OVER_LEVELS_BEGIN(level,min_level,max_level);
	  update_buffer_level(level,vars,1);
	  MESH_RUN_OVER_LEVELS_END;
	}

      cart_debug("GalFind: Iter=%d, nmod=%d, maxidx=%d",iter,nmod,maxidx);

    }

  if(maxiter<1 || iter<maxiter)
    {
      ret = _gfFinish(maxidx,numCut,varTemp);
    }
  else
    {
      cart_debug("Maximum number of iterations (%d) reached.",maxiter);
    }

  if(save_field != NULL)
    {
      int i;

      for(i=0; i<num_cells; i++)
	{
	  cell_var(i,VAR_ACCEL) = save_field[i];
	}

      cart_free(save_field);
    }

  return ret;
}


halo_list* _gfFinish(int numGals, int numCut, int varTemp)
{
  static int init = 1;
  static MPI_Datatype Double7;
  MPI_Op Add;

  int i, j, ih;
  double *grhalo, *rhalo, rmin;
  int *gnum, *num;
  double *gpmv, *pmv;
  halo_list *hlist;


  if(init)
    {
      MPI_Type_contiguous(NPOS,MPI_DOUBLE,&Double7);
      MPI_Type_commit(&Double7);
      init = 0;
    }

  MPI_Op_create(_gfFunAdd,1,&Add);

  num = cart_alloc(int,numGals);
  pmv = cart_alloc(double,NPOS*numGals);

  memset(pmv,0,sizeof(double)*NPOS*numGals);
  memset(num,0,sizeof(int)*numGals);

  cart_debug("GalFind: Mapping galaxies...");

#pragma omp parallel for default(none), private(i,j,ih), shared(particle_species_indices,num_particle_species,particle_x,particle_v,particle_mass,particle_id,pmv,num,numGals,varTemp)
  for(i=0; i<num_particles; i++) if(particle_is_star(i))
    {
      int cell = cell_find_position(particle_x[i]);
      if(cell>-1 && _gfIdx(cell,varTemp)>0)
	{
	  double fm, dx[nDim];
	  
	  ih = _gfIdx(cell,varTemp) - 1;
	  if(ih >= numGals)
	    {
	      cart_error("Assert fails: ih=%d > numGals=%d",ih,numGals);
	    }

	  if(num[ih] == 0)
	    {
	      for(j=0; j<nDim; j++)
		{
		  dx[j] = particle_x[i][j];
		}
	    }
	  else
	    {
	      for(j=0; j<nDim; j++)
		{
		  dx[j] = particle_x[i][j] - pmv[ih*NPOS+j];
		  if(dx[j] >  0.5*num_grid) dx[j] -= num_grid;
		  if(dx[j] < -0.5*num_grid) dx[j] += num_grid;
		  cart_assert(fabs(dx[j]) <= num_grid/2);
		}
	    }

#pragma omp atomic	  
	  num[ih]++;

#pragma omp atomic	  
	  pmv[ih*NPOS+nDim] += particle_mass[i];
	  fm = particle_mass[i]/pmv[ih*NPOS+nDim];

	  for(j=0; j<nDim; j++)
	    {
#pragma omp atomic	  
	      pmv[ih*NPOS+j] += fm*dx[j];
	    }

	  for(j=0; j<nDim; j++)
	    {
#pragma omp atomic	  
	      pmv[ih*NPOS+nDim+1+j] += fm*(particle_v[i][j]-pmv[ih*NPOS+nDim+1+j]);
	    }
	}
    }

  gnum = cart_alloc(int,numGals);
  MPI_Allreduce(num,gnum,numGals,MPI_INT,MPI_SUM,mpi.comm.run);
  cart_free(num);
  num = gnum;

  gpmv = cart_alloc(double,NPOS*numGals);
  MPI_Allreduce(pmv,gpmv,numGals,Double7,Add,mpi.comm.run);
  cart_free(pmv);
  pmv = gpmv;

#pragma omp parallel for default(none), private(ih,j), shared(pmv,numGals)
  for(ih=0; ih<numGals; ih++) if(pmv[ih*NPOS+nDim] > 0)
    {
      for(j=0; j<nDim; j++)
	{
	  if(pmv[ih*NPOS+j] < 0) pmv[ih*NPOS+j] += num_grid;
	  if(pmv[ih*NPOS+j] > num_grid) pmv[ih*NPOS+j] -= num_grid;

	  if(pmv[ih*NPOS+j]<0 || pmv[ih*NPOS+j]>num_grid)
	    {
	      cart_error("ASSERT: ih=%d pos[%d] = %lg",ih,j,pmv[ih*NPOS+j]);
	    }
	}
    }


  /*
  //  Measure the sizes
  */
  //cart_debug("GalFind: Measuring sizes...");

  rhalo = cart_alloc(double,numGals);

  rmin = cell_size[max_level_now_global(mpi.comm.run)];
  for(i=0; i<numGals; i++) rhalo[i] = rmin;

#pragma omp parallel for default(none), private(i,j,ih), shared(particle_species_indices,num_particle_species,particle_x,particle_id,pmv,numGals,rhalo,varTemp)
  for(i=0; i<num_particles; i++) if(particle_is_star(i))
    {
      int cell = cell_find_position(particle_x[i]);
      if(cell>-1 && _gfIdx(cell,varTemp)>0)
	{
	  double r ;

	  ih = _gfIdx(cell,varTemp) - 1;
	  cart_assert(ih < numGals);

	  r = compute_distance_periodic(particle_x[i],pmv+ih*NPOS);
	  if(r > rhalo[ih])
	    {
#pragma omp critical
	      if(r > rhalo[ih]) rhalo[ih] = r;
	    }
	}
    }

  grhalo = cart_alloc(double,numGals);
  MPI_Allreduce(rhalo,grhalo,numGals,MPI_DOUBLE,MPI_MAX,mpi.comm.run);
  cart_free(rhalo);
  rhalo = grhalo;

  /*
  //  Create a hlist data structure
  */
  hlist = cart_alloc(halo_list,1);
  hlist->num_halos = hlist->size = numGals;
  hlist->list = cart_alloc(halo,numGals);
  hlist->map = 0;

  for(i=ih=0; i<numGals; i++) if(num[i] > numCut)
    {
      hlist->list[ih].id = i + 1;
      hlist->list[ih].particles = NULL;
      hlist->list[ih].binding_order = NULL;
      hlist->list[ih].flag = 0;
      hlist->list[ih].np = num[i];
      hlist->list[ih].vmax = 0;
      hlist->list[ih].rmax = 0;
      hlist->list[ih].mvir = pmv[i*NPOS+nDim];
      hlist->list[ih].rvir = hlist->list[ih].rhalo = rhalo[i];

      for(j=0; j<nDim; j++)
	{
	  hlist->list[ih].pos[j] = pmv[i*NPOS+j];
	  hlist->list[ih].vel[j] = pmv[i*NPOS+nDim+1+j];
	}
      ih++;
    }

  hlist->num_halos = numGals = ih;

  cart_free(rhalo);
  cart_free(pmv);
  cart_free(num);

  MPI_Op_free(&Add);

  cart_debug("GalFind: Done.");

  return hlist;
}


void _gfCompressIds(int *maxold, int varTemp)
{
  MESH_RUN_DECLARE(level,cell);

  int iold, maxnew = 0;
  int *old2new = cart_alloc(int,*maxold+1);

  memset(old2new,0,sizeof(int)*(*maxold+1));

  MESH_RUN_OVER_ALL_LEVELS_BEGIN(level);
  MESH_RUN_OVER_CELLS_OF_LEVEL_BEGIN(cell);
  
  iold = _gfIdx(cell,varTemp);
  cart_assert(iold <= *maxold);

  if(iold > 0)
    {
      if(old2new[iold] == 0)
	{
	  maxnew++;
	  old2new[iold] = maxnew;
	}
      _gfSetIdx(cell,old2new[iold],varTemp);
    }

  MESH_RUN_OVER_CELLS_OF_LEVEL_END;
  MESH_RUN_OVER_LEVELS_END;

  cart_free(old2new);

  cart_debug("GalFind: Compressed from %d to %d",*maxold+1,maxnew+1);

  *maxold = maxnew;
}

#endif /* PARTICLES && STAR_FORMATION */

