#include "config.h"
#include "control_parameter.h"
#include <math.h>
#include <mpi.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "auxiliary.h"
#include "rt_transfer.h"
#include "units.h"
#include "cosmology.h"
#include "hydro.h"
#include "iterators.h"
#include "parallel.h"
#include "particle.h"
#include "rt.h"
#include "starformation.h"
#include "times.h"
#include "timing.h"
#include "tree.h"

#define QSO_TRACER_FINDER_TIMER 55
#define QSO_ALLREDUCE_TIMER 56
#define QSO_SORTER_TIMER 57

float prevtime=0.;

//use stars median position as quasar tracer

// #define MAXNUM_QSO_TRACER 35
//char* PID_fpath="/home/hqchen/reionization/visualize/qso/PID.txt";
//
double find_median(int n,double original_arr[]);


//extern int max_mpi_sync_level;
extern float rt_uv_emissivity_stars;
extern float rt_uv_emissivity_quasars;
extern double qso_lum; //in erg/s
extern int MAXNUM_QSO_TRACER;
extern const char* PID_fpath;

//https://stackoverflow.com/questions/36714030/c-sort-float-array-while-keeping-track-of-indices/36714204
typedef struct {
  int index;
  particleid_t PID;
} PIDindex; 

int cmp(const void *aptr, const void *bptr){
  PIDindex *a=(PIDindex *)aptr;
  PIDindex *b=(PIDindex *)bptr;
  return ((a->PID > b->PID) ? 1 : ((a->PID < b->PID) ? -1 : 0));
}





double qsoPos[3];

int QSO_MASTER_RANK=0;

#define NOT_FOUND -10000.
particleid_t *tracer_PID;
double *tracer_x;
double *tracer_y;
double *tracer_z;

char fname_qsoPos[30]="qsoPos_"; 
FILE *fpqsoPos;

void config_init_qsohalo(){
  control_parameter_add(control_parameter_string,PID_fpath,"qso:tracerIDfile","path to the star tracer of qso");
  control_parameter_add(control_parameter_int,&MAXNUM_QSO_TRACER,"qso:tracernumber","number of tracer stars");
}

void config_verf_qsohalo(){
  cart_debug("PID_fpath=%s,#startracer=%d",PID_fpath,MAXNUM_QSO_TRACER);
  VERIFY(qso:tracerIDfile,1);
  VERIFY(qso:tracernumber,(MAXNUM_QSO_TRACER>0)&&(MAXNUM_QSO_TRACER<10000));
}

void config_init_qsolum(){
  control_parameter_add(control_parameter_double,&qso_lum,"qso:luminosity","qso ionizing luminosity in erg/s");
}

void config_verf_qsolum(){
  cart_debug("qso_lum=%.2le erg/s",qso_lum);
  VERIFY(qso:luminosity,(qso_lum>1e33)&&(qso_lum<1e50));
}

void config_init_qso(){
  config_init_qsolum();
  config_init_qsohalo();
}
void config_verf_qso(){
  config_verf_qsolum();
  config_verf_qsohalo();
}


void qso_initialize_tracer(){
  char rankstr[10];
  cart_debug("local_proc_id=%d",local_proc_id);
  sprintf(rankstr,"%05d",local_proc_id);
  strcat(fname_qsoPos,rankstr);
  strcat(fname_qsoPos,".txt");
  cart_debug("convert int to str: %s",rankstr);
  cart_debug("output file name is: %s",fname_qsoPos);
  fpqsoPos=fopen(fname_qsoPos,"a");
  fprintf(fpqsoPos, "auni level x y z [code unit]\n");
  fclose(fpqsoPos);
  tracer_PID=cart_alloc(particleid_t,MAXNUM_QSO_TRACER);
  tracer_x=cart_alloc(double,MAXNUM_QSO_TRACER);
  tracer_y=cart_alloc(double,MAXNUM_QSO_TRACER);
  tracer_z=cart_alloc(double,MAXNUM_QSO_TRACER);
}
void qsoReadPID(){
// read from 
  FILE* fp;
  int i;
  fp=fopen(PID_fpath,"r");
  for (i=0; i<MAXNUM_QSO_TRACER; i++){
      fscanf(fp,"%ld",&tracer_PID[i]); 
  }
  fclose(fp);
  gsl_sort (tracer_PID, 1, MAXNUM_QSO_TRACER); //sort tracer_PID in ascending order soto find them quickly

//  for (i=0; i<MAXNUM_QSO_TRACER; i++){
//      cart_debug("tracer_PID[%d]=%ld\n",i,tracer_PID[i]); 
//  }
  for (i=0;i<MAXNUM_QSO_TRACER;i++){
    tracer_x[i]=NOT_FOUND;
    tracer_y[i]=NOT_FOUND;
    tracer_z[i]=NOT_FOUND;
  }
}

void update_tracer(double* tracer_pos,double* buffer_pos_gather, 
                    int ntracer, int comm_size){
  int j;
  for (j=0;j<ntracer;j++){
    if (buffer_pos_gather[j]>0){
      tracer_pos[j]=buffer_pos_gather[j];
    }
  }
}


void qsoInit()
{
  //config_init_qso();
  //config_verf_qso();
  qso_initialize_tracer();
  qsoReadPID();
}


void qsoFindPosition(int level, MPI_Comm level_comm)
{
  if (level>max_level_for_collectives) return;
    start_time(QSO_TRACER_FINDER_TIMER);
  int level_comm_size;
  MPI_Comm_size(level_comm,&level_comm_size);
  int  i,ipart, icheck;
  double *buffer_x,*buffer_y,*buffer_z;
  double *buffer_x_gather,*buffer_y_gather,*buffer_z_gather;
  PIDindex *copyAllPID;

  buffer_x=cart_alloc(double,MAXNUM_QSO_TRACER);
  buffer_y=cart_alloc(double,MAXNUM_QSO_TRACER);
  buffer_z=cart_alloc(double,MAXNUM_QSO_TRACER);
  buffer_x_gather=cart_alloc(double,MAXNUM_QSO_TRACER);
  buffer_y_gather=cart_alloc(double,MAXNUM_QSO_TRACER);
  buffer_z_gather=cart_alloc(double,MAXNUM_QSO_TRACER);
  copyAllPID=cart_alloc(PIDindex,num_particles);
  for (i=0;i<MAXNUM_QSO_TRACER;i++){
    buffer_x[i]=NOT_FOUND;
    buffer_y[i]=NOT_FOUND;
    buffer_z[i]=NOT_FOUND;
  }
  int findcnt=0;
    //copy particle_id
  for (ipart = 0; ipart < num_particles; ipart++ ) {
    copyAllPID[ipart].index=ipart;
    copyAllPID[ipart].PID=particle_id[ipart];
  }
    //sort particle using qsort
  qsort(copyAllPID,num_particles,sizeof(copyAllPID[0]),cmp);
    //outer loop tracer, inner loop particle_id
  int istop=0;
  for (icheck=0;icheck< MAXNUM_QSO_TRACER;icheck++){
    for (ipart = istop;ipart < num_particles; ipart++ ) {
        if ((copyAllPID[ipart].PID==tracer_PID[icheck]) &&
            (particle_level[copyAllPID[ipart].index] != FREE_PARTICLE_LEVEL) &&
            particle_is_star(copyAllPID[ipart].index)){


          buffer_x[icheck] = particle_x[ipart][0];
          buffer_y[icheck] = particle_x[ipart][1];
          buffer_z[icheck] = particle_x[ipart][2]; 
          istop=ipart;
          findcnt+=1;
          break;
        } 
      }
  }
  cart_debug("finish searching for stars at level %d, found %d stars",level,findcnt);
    cart_debug("find stars used time: %le",end_time(QSO_TRACER_FINDER_TIMER));
    start_time(QSO_ALLREDUCE_TIMER);
  
  MPI_Allreduce(buffer_x,buffer_x_gather,MAXNUM_QSO_TRACER,
                        MPI_DOUBLE,MPI_MAX,level_comm);
  MPI_Allreduce(buffer_y,buffer_y_gather,MAXNUM_QSO_TRACER,
                        MPI_DOUBLE,MPI_MAX,level_comm);
  MPI_Allreduce(buffer_z,buffer_z_gather,MAXNUM_QSO_TRACER,
                        MPI_DOUBLE,MPI_MAX,level_comm);
    cart_debug("all reduce cost time: %le",end_time(QSO_ALLREDUCE_TIMER));
  /*
  MPI_Barrier(level_comm);
  for (i=0;i<MAXNUM_QSO_TRACER*num_procs;i++){
    cart_debug("buffer_x_gather[%d]=%f",i,buffer_x_gather[i]);
  }
  */
    start_time(QSO_SORTER_TIMER);
  update_tracer(tracer_x,buffer_x_gather,MAXNUM_QSO_TRACER,level_comm_size);
  update_tracer(tracer_y,buffer_y_gather,MAXNUM_QSO_TRACER,level_comm_size);
  update_tracer(tracer_z,buffer_z_gather,MAXNUM_QSO_TRACER,level_comm_size);

  qsoPos[0]=find_median(MAXNUM_QSO_TRACER,tracer_x);
  qsoPos[1]=find_median(MAXNUM_QSO_TRACER,tracer_y);
  qsoPos[2]=find_median(MAXNUM_QSO_TRACER,tracer_z);
    cart_debug("sort used time:%le",end_time(QSO_SORTER_TIMER));

  if (auni[level]-prevtime>1e-8){
    fpqsoPos = fopen(fname_qsoPos,"a");
    fprintf(fpqsoPos, "%15.8e %d %15.8e %15.8e %15.8e \n",
    auni[level],level,qsoPos[0],qsoPos[1],qsoPos[2]); 
    prevtime=auni[level];
    fclose(fpqsoPos);
  }
  cart_free(buffer_x);
  cart_free(buffer_y);
  cart_free(buffer_z);
  cart_free(buffer_x_gather);
  cart_free(buffer_y_gather);
  cart_free(buffer_z_gather);
}


void qsoAddSource(int level)
{
/*
    MESH_RUN_DECLARE(tmplevel,cell);
    MESH_RUN_OVER_ALL_LEVELS_BEGIN(tmplevel);
    MESH_RUN_OVER_CELLS_OF_LEVEL_BEGIN(cell);
    if(cell_is_leaf(cell)){
        int var;
        for(var=0;var<num_vars;var++){
            if (isnan(cell_var(cell,var))) cart_error("NAN cell!!! %d %g",var,cell_var(cell,var));
        }
    }
    MESH_RUN_OVER_CELLS_OF_LEVEL_END;
    MESH_RUN_OVER_LEVELS_END;
*/
  //double pos[3] = { 0.5*num_grid, 0.5*num_grid, 0.5*num_grid };
  //
  //fwrite(qsoPos,sizeof(double),3,fname_qsoPos);
  double value_code = qso_lum*units->time/(units->mass*constants->c*constants->c)/(rt_uv_emissivity_stars+rt_uv_emissivity_quasars);
  static int start = 1;

  if(start)
    {
      start = 0;
      cart_debug("LQSO_code = %lg",value_code);
    }
  
  rtTransferAssignExtraSourceDensity(level,qsoPos,value_code);

  //cart_error("I am not implemented!");
}


int cmpfunc (const void * aptr, const void * bptr) {
  double a = *(double*)aptr;
  double b = *(double*)bptr;
  
  return ((a > b) ? 1 : ((a < b) ? -1 : 0));
}

double find_median(int n,double original_arr[]){
  int i;
  double xmin,xmax,median;
  double *x;
  x=cart_alloc(double,n);
  gsl_stats_minmax(&xmin,&xmax,original_arr,1,n);
  //cart_debug("xmin=%f; xmax=%f", xmin, xmax);

// periodic condition 
  if ((xmax-xmin)<0.5*num_grid){
    for (i=0;i<n;i++){
      x[i]=original_arr[i];
    }
  }
  else{
    for (i=0;i<n;i++){
      if(x[i]<0.5*num_grid){
        x[i]=original_arr[i]+num_grid; 
      }
      else{ 
        x[i]=original_arr[i];
      }
    }
  } 
  qsort(x,n,sizeof(double),cmpfunc);  
  if (n%2==0) {
    median = ((x[n/2] + x[n/2 - 1]) / 2.0);
  }
  else { median = x[n/2];
  }

  if (median<num_grid){
    return median;
  }
  else{
    return median-num_grid;
  }
}

