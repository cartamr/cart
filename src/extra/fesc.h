#ifndef __EXT_FESC_H__
#define __EXT_FESC_H__

#ifndef CONFIGURED
#error "Missing config.h include."
#endif


struct HALO_LIST;


/*
//  Information for what is actually escaping:
//  fesc = Finalizer(\int_rmin^rmax Integrand() dr)
*/
typedef struct feInfoType
{
  double (*integrand)(int cell);
  double (*finalizer)(double integral);
}
feInfo;


extern feInfo fe_912A;


/*
//  Compute escape fractions specified by feInfo<a> using the HealPIX 
//  binning with <nside> bins per section (total number of rays is 
//  12*nside^2) for all halos resolved at least to <resolution_level>;
//  escape fractions are integrated to <outer_edge> fraction of the virial
//  radius, skipping <inner_edge>*cell_size[starting-cell] in the beginning;
//  sources can be downsamples by <sample> to speed-up the calculation; 
//  sources are stellar particles or the grid-assigned radiation source 
//  (usually faster) dependending on whether <sum_over_parts> is true or false.
*/
void extEscapeFraction1(const char *fname, feInfo a, int nside, struct HALO_LIST *halos, int resolution_level, float outer_edge, float inner_edge, int sample, int sum_over_parts);
void extEscapeFraction2(const char *fname, int nside, struct HALO_LIST *halos, int resolution_level, float outer_edge);

#endif  /* __EXT_FESC_H__ */
