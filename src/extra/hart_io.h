#ifndef __HART_IO_H__
#define __HART_IO_H__

#if defined(GRAVITY) && defined(COSMOLOGY) && defined(HYDRO)
void read_hart_grid_binary( char *filename );
#endif /* GRAVITY && COSMOLOGY && HYDRO */

#endif /* __HART_IO_H__ */
