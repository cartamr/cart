#include "config.h"

#include <math.h>
#include <string.h>

#include "auxiliary.h"
#include "healpix.h"
#include "los.h"
#include "parallel.h"
#include "tree.h"


/*
//  Traverse a segment of a LOS located on a single processor.
*/
void losTraverseSegment(int id, double pos[3], double theta, double phi, double rstart, double rend, int floor_level, losWorkerCallback worker, losSegment *segment, void *pars)
{
  int j, cell, level, ret = 0;
  double e[3], posBox[nDim], posCell[nDim];
  double x, h, r = rstart;
  int start = 1;

  cart_assert(worker != 0);
  cart_assert(segment != NULL);

  segment->Id = id;
  segment->Range[0] = segment->Range[1] = 0.0;

  e[0] = sin(theta)*cos(phi);
  e[1] = sin(theta)*sin(phi);
  e[2] = cos(theta);

  while(r <= rend)
    {
      for(j=0; j<nDim; j++)
	{
	  posBox[j] = pos[j] + r*e[j];
	  posBox[j] = posBox[j] - num_grid*floor(posBox[j]/num_grid);
	}

      cell = cell_find_position_above_level(floor_level,posBox);
      if(cell >= 0)
	{
	  cell_center_position(cell,posCell);
	  level = cell_level(cell);
	}
      else
	{
	  for(j=0; j<nDim; j++) posCell[j] = 0.5 + floor(posBox[j]);
	  level = min_level;
	}

      h = 1.75; /* just a bit above sqrt(3) */
      for(j=0; j<nDim; j++)
	{
	  x = (posBox[j]-posCell[j])/cell_size[level];

	  if(e[j] > 0.0) 
	    x = (0.5-x)/e[j];
	  else if(e[j] < 0.0) 
	    x = (-0.5-x)/e[j];
	  else
	    x = 10.0;

	  if(h > x) h = x;
	}
      h += 0.01;
      h *= cell_size[level];

      if(cell>=0 && cell_is_local(cell))
	{
	  if(start)
	    {
	      segment->Range[0] = r;
	      start = 0;
	    }
	  ret = worker(id,cell,r,r+h,segment->Buffer,pars);
	  r += h;
	  segment->Range[1] = r;
	  if(ret != 0) break;
	}
      else r += h;
    }

  segment->WorkerReturnCode = ret;
}


/*
//  Collect all LOS segments from different processors on the master node for several segments
//  and broadcast them back.
*/
void losCollectSegments(int num_lines, const losBuffer *results, const losSegment *segments, losCollectorCallback collector, void *pars)
{
  int i, k, size;

  cart_assert(num_lines > 0);

  if(local_proc_id == MASTER_NODE)
    {
      losSegment *coll_buffer, *line_buffer;
      char **data_buffer;

      cart_assert(collector != 0);

      line_buffer = cart_alloc(losSegment,num_procs*num_lines);
      data_buffer = cart_alloc(char*,num_procs);

      for(k=0; k<num_lines; k++) line_buffer[k] = segments[k];

      for(i=1; i<num_procs; i++)
	{
	  losSegment *lines = line_buffer + i*num_lines;

	  MPI_Recv(lines,sizeof(losSegment)*num_lines,MPI_BYTE,i,0,mpi.comm.run,MPI_STATUS_IGNORE);

	  for(size=k=0; k<num_lines; k++) size += lines[k].Buffer.Size;
	  if(size > 0)
	    {
	      data_buffer[i] = cart_alloc(char,size);
	      MPI_Recv(data_buffer[i],size,MPI_BYTE,i,0,mpi.comm.run,MPI_STATUS_IGNORE);

	      lines[0].Buffer.Data = data_buffer[i];
	      for(k=1; k<num_lines; k++) lines[k].Buffer.Data = (char *)lines[k-1].Buffer.Data + lines[k-1].Buffer.Size;
	    }
	  else data_buffer[i] = NULL;
	}
      
      coll_buffer = cart_alloc(losSegment,num_procs);

      for(k=0; k<num_lines; k++)
	{
	  for(i=0; i<num_procs; i++) coll_buffer[i] = line_buffer[k+i*num_lines];
	  collector(results+k,num_procs,coll_buffer,pars);
	}

      cart_free(coll_buffer);

      for(i=1; i<num_procs; i++) if(data_buffer[i] != NULL)
	{
	  cart_free(data_buffer[i]);
	}
      cart_free(data_buffer);
      cart_free(line_buffer);
    }
  else
    {
      MPI_Send((void *)segments,sizeof(losSegment)*num_lines,MPI_BYTE,MASTER_NODE,0,mpi.comm.run);

      for(size=k=0; k<num_lines; k++) size += segments[k].Buffer.Size;
      if(size > 0)
	{
	  char *buffer;
	  int mapped = 1;

	  /*
	  //  Optimization - check that the data for the input segments are continuous in memory.
	  */
	  for(k=1; mapped && k<num_lines; k++)
	    {
	      if((char *)segments[k-1].Buffer.Data+segments[k-1].Buffer.Size != (char *)segments[k].Buffer.Data)
		{
		  mapped = 0;
		}
	    }

	  if(mapped)
	    {
	      buffer = (char *)segments[0].Buffer.Data;
	    }
	  else
	    {
	      char *p = buffer = cart_alloc(char,size);

	      for(k=0; k<num_lines; k++) if(segments[k].Buffer.Size > 0)
		{
		  memcpy(p,segments[k].Buffer.Data,segments[k].Buffer.Size);
		  p += segments[k].Buffer.Size;
		}
	    }

	  MPI_Send(buffer,size,MPI_BYTE,MASTER_NODE,0,mpi.comm.run);

	  if(!mapped) cart_free(buffer);
	}
    }

  for(size=k=0; k<num_lines; k++) size += results[k].Size;
  if(size > 0)
    {
      char *buffer;
      int mapped = 1;

      /*
      //  Optimization - check that the data for the result buffers are continuous in memory.
      */
      for(k=1; mapped && k<num_lines; k++)
	{
	  if((char *)results[k-1].Data+results[k-1].Size != (char *)results[k].Data)
	    {
	      mapped = 0;
	    }
	}

      if(mapped)
	{
	  buffer = (char *)results[0].Data;
	}
      else
	{
	  char *p = buffer = cart_alloc(char,size);

	  for(k=0; k<num_lines; k++) if(results[k].Size > 0)
	    {
	      memcpy(p,results[k].Data,results[k].Size);
	      p += results[k].Size;
	    }
	}

      MPI_Bcast(buffer,size,MPI_BYTE,MASTER_NODE,mpi.comm.run);

      if(!mapped)
	{
	  char *p = buffer;

	  for(k=0; k<num_lines; k++) if(results[k].Size > 0)
	    {
	      memcpy(results[k].Data,p,results[k].Size);
	      p += results[k].Size;
	    }

	  cart_free(buffer);
	}
    }
}


/*
//  Collect all LOS segments from different processors on the master node for one LOS
//  and broadcast them back.
*/
void losCollectSegments1(const losBuffer *result, const losSegment *segment, losCollectorCallback collector, void *pars)
{
  int i;
  losSegment *line;

  if(local_proc_id == MASTER_NODE)
    {
      cart_assert(collector != 0);

      line = cart_alloc(losSegment,num_procs);

      line[0] = *segment;
      for(i=1; i<num_procs; i++)
	{
	  MPI_Recv(line+i,sizeof(losSegment),MPI_BYTE,i,0,mpi.comm.run,MPI_STATUS_IGNORE);

	  if(line[i].Buffer.Size > 0)
	    {
	      line[i].Buffer.Data = cart_alloc(char,line[i].Buffer.Size);
	      MPI_Recv(line[i].Buffer.Data,line[i].Buffer.Size,MPI_BYTE,i,0,mpi.comm.run,MPI_STATUS_IGNORE);
	    }
	}
      
      collector(result,num_procs,line,pars);

      for(i=1; i<num_procs; i++) if(line[i].Buffer.Size > 0)
	{
	  cart_free(line[i].Buffer.Data);
	}
      cart_free(line);
    }
  else
    {
      MPI_Send((void *)segment,sizeof(losSegment),MPI_BYTE,MASTER_NODE,0,mpi.comm.run);
      if(segment->Buffer.Size > 0)
	{
	  MPI_Send(segment->Buffer.Data,segment->Buffer.Size,MPI_BYTE,MASTER_NODE,0,mpi.comm.run);
	}
    }

  if(result->Size > 0)
    {
      MPI_Bcast(result->Data,result->Size,MPI_BYTE,MASTER_NODE,mpi.comm.run);
    }
}


/*
//  Traverse LOS over the whole sky, sampled by HealPIX
*/
void losTraverseSky(int nside, double pos[3], double rstart, double rend, int floor_level, losBuffer *lines, losWorkerCallback worker, losCollectorCallback collector, void *pars)
{
  int npix = 12*nside*nside;
  int i;
  double *theta, *phi;
  losSegment *segments;

  cart_assert(lines != NULL);
  cart_assert(worker != NULL);
  cart_assert(collector != NULL);
  
  theta = cart_alloc(double,npix);
  phi = cart_alloc(double,npix);

  /*
  //  HealPIX is not thread-safe, so we fill in angle arrays serially.
  */
  for(i=0; i<npix; i++)
    {
      hp_pix2ang_nest(nside,i,theta+i,phi+i);
    }

  /*
  //  Create worker segments. We only need to assign buffers, other data are set
  //  inside losTraverseSegment
  */
  segments = cart_alloc(losSegment,npix);
  for(i=0; i<npix; i++)
    {
      segments[i].Buffer.Size = lines[i].Size;
      if(lines[i].Size > 0)
	{
	  segments[i].Buffer.Data = cart_alloc(char,lines[i].Size);
	  memcpy(segments[i].Buffer.Data,lines[i].Data,lines[i].Size);
	}
    }

  /*
  //  Loop over directions
  */
#pragma omp parallel for default(none), private(i), shared(npix,theta,phi,segments,pos,rstart,rend,floor_level,worker,pars), schedule(dynamic,1)
  for(i=0; i<npix; i++)
    {
      losTraverseSegment(i,pos,theta[i],phi[i],rstart,rend,floor_level,worker,segments+i,pars);
    }

  /*
  //  Collect segments serially
  */
  losCollectSegments(npix,lines,segments,collector,pars);

  /*
  // Clean-up
  */
  for(i=0; i<npix; i++)
    {
      if(lines[i].Size > 0) cart_free(segments[i].Buffer.Data);
    }
  cart_free(segments);
  cart_free(theta);
  cart_free(phi);
}

