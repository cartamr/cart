#ifndef __EXT_GALFIND_H__
#define __EXT_GALFIND_H__

#ifndef CONFIGURED
#error "Missing config.h include."
#endif


#if defined(COSMOLOGY) && defined(PARTICLES) && defined(STAR_FORMATION)

struct HALO_LIST;

struct HALO_LIST* gfRun(int varStellarDensity, int varTemp, double denCut, int numCut, int maxiter);


#endif /* COSMOLOGY && PARTICLES && STAR_FORMATION */
#endif /* __EXT_GALFIND_H__ */

