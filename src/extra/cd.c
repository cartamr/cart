#include "config.h"

#include <math.h>
#include <stdlib.h>

#include "auxiliary.h"
#include "tree.h"

#include "cd.h"
#include "cell_probe.h"
#include "los.h"


/*
//  Compute column densities of a set of vars to a given distance
*/
typedef struct cdParsType
{
  int nvals;
  const cell_probe_t *probes;
}
cdPars;


int cdWorker(int id, int cell, double r1, double r2, losBuffer data, void *pars)
{
  int j;
  cdData *d = (cdData *)data.Data;
  cdPars *p = (cdPars *)pars;
  int level = cell_level(cell);

  for(j=0; j<p->nvals; j++)
    {
      d->val[j] += p->probes[j].Value(level,cell,NULL)*(r2-r1);
    }
  d->len += (r2-r1);

  return 0;
}


void cdCollector(const losBuffer *result, int num_segments, const losSegment *segments, void *pars)
{
  int i, j;
  cdData *dn, *dr = (cdData *)result->Data;
  cdPars *p = (cdPars *)pars;

  for(j=0; j<p->nvals; j++)
    {
      dr->val[j] = 0.0;
    }
  dr->len = 0.0;

  for(i=0; i<num_segments; i++)
    {
      dn = (cdData *)segments[i].Buffer.Data;

      for(j=0; j<p->nvals; j++)
	{
	  dr->val[j] += dn->val[j];
	}
      dr->len += dn->len;
    }
}


/*
//  Compute column densities of <nvars> grid variables (with indicies 
//  supplied in <vars[]> array, sampled over the sky using the HealPIX 
//  binning with <nside> bins per section (total number of rays is 
//  12*nside^2) with common origin <pos>, sampling radii 
//  between <rstart> and <rend>, not deeper than <floor_level>. 
//  The final result is returned in <output[]>, whose
//  dimension must be 12*nside^2
*/
void cdTraverseSky(int nvals, const cell_probe_t *probes, int nside, double pos[3], double rstart, double rend, int floor_level, cdData output[])
{
  int npix = 12*nside*nside;
  int i, j;
  losBuffer *lines;
  cdPars pars;

  cart_assert(0<nvals && nvals<=CD_MAX_NVALS);

  pars.nvals = nvals;
  pars.probes = probes;

  /*
  //  Prepare buffers for the LOS traversal
  */
  lines = cart_alloc(losBuffer,npix);
  for(i=0; i<npix; i++)
    {
      lines[i].Size = sizeof(cdData);
      lines[i].Data = output + i;
    }

  /*
  //  Init LOS buffers
  */
#pragma omp parallel for default(none), private(i,j), shared(npix,lines,nvals,output)
  for(i=0; i<npix; i++)
    {
      for(j=0; j<nvals; j++) output[i].val[j] = 0.0;
      output[i].len = 0.0;
    }

  losTraverseSky(nside,pos,rstart,rend,floor_level,lines,cdWorker,cdCollector,&pars);

  cart_free(lines);
}

