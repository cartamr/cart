#include "config.h"

#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef _OPENMP
#include <omp.h>
#endif

#include "auxiliary.h"
#include "cosmology.h"
#include "halos.h"
#include "hydro.h"
#include "parallel.h"
#include "rand.h"
#include "system.h"
#include "times.h"
#include "tree.h"
#include "units.h"

#include "cell_probe.h"
#include "healpix.h"
#include "ism.h"
#include "los.h"
#include "synthetic_spectra.h"


#if defined(HYDRO) && defined(RADIATIVE_TRANSFER) && defined(COSMOLOGY)


#ifndef SS_MAX_VARS
#define SS_MAX_VARS  4
#endif


struct ssInfoType
{
  double AtomMass;  /* ion's atomic mass */
  double Wavelength; /* in angstroms */
  double OsStrength; 
  double DampCoefft;
  int NumVars;
  cell_probe_t Vars[SS_MAX_VARS];
  const char* FileExt;
};


struct ssPointType
{
  double r;
  double v;
  float vals[SS_MAX_VARS+1];  /* +1 is for temperature */
};



/*
//  Known absorption lines
*/
ssInfo ss_HI1216_impl;
const ssInfo *ss_HI1216 = &ss_HI1216_impl;

ssInfo ss_HI1216L_impl;
const ssInfo *ss_HI1216L = &ss_HI1216L_impl;

void ss_Init()
{
  static int start = 1;

  if(start)
    {
      start = 0;
      
      ss_HI1216_impl.AtomMass = 1;
      ss_HI1216_impl.Wavelength = 1215.67;
      ss_HI1216_impl.OsStrength = 0.416; 
      ss_HI1216_impl.DampCoefft = 6.265e8;
      ss_HI1216_impl.NumVars = 1;
      ss_HI1216_impl.Vars[0] = cp_HI_number_density;
      ss_HI1216_impl.FileExt = "fHI";

      ss_HI1216L_impl.AtomMass = 1;
      ss_HI1216L_impl.Wavelength = 1215.67;
      ss_HI1216L_impl.OsStrength = 0.416; 
      ss_HI1216L_impl.DampCoefft = 6.265e8;
      ss_HI1216L_impl.NumVars = 3;
      ss_HI1216L_impl.Vars[0] = cp_HI_number_density;
      ss_HI1216L_impl.Vars[1] = cp_gas_number_density;
      ss_HI1216L_impl.Vars[2] = cp_gas_metallicity;
      ss_HI1216L_impl.Vars[3] = cp_HI_ionization_rate;
      ss_HI1216L_impl.FileExt = "fHI";
    }
}


int ssWorker(int id, int cell, double r1, double r2, losBuffer data, void *pars)
{
  int j;
  ssLine *d = (ssLine*)data.Data;
  const ssInfo *i = d->Info;
  int level = cell_level(cell);

  ssPoint p;
  p.vals[0] = cp_temperature.Value(level,cell,NULL);
  for(j=0; j<i->NumVars; j++)
    {
      p.vals[j+1] = i->Vars[j].Value(level,cell,NULL);
    }
  p.r = 0.5*(r2+r1);
  p.v = 0.0;
  for(j=0; j<nDim; j++) p.v += cell_momentum(cell,j)*d->e[j];
  p.v /= cell_gas_density(cell);

  if(d->Size == d->NumPts)
    {
      ssPoint *tmp;

      d->Size = (int)(d->Size*1.5+100);
#pragma omp critical
      tmp = cart_alloc(ssPoint,d->Size);
      if(d->NumPts > 0)
	{
	  memcpy(tmp,d->Data,d->NumPts*sizeof(ssPoint));
	  cart_free(d->Data);
	}
      d->Data = tmp;
    }
  
  d->Data[d->NumPts++] = p;

  return 0;
}


int ssPointCmp(const void *p1, const void *p2)
{
  const ssPoint *v1 = (const ssPoint*)p1;
  const ssPoint *v2 = (const ssPoint*)p2;
  if(v1->r > v2->r) return 1; else if(v1->r < v2->r) return -1; else return 0;
}


void ssCollector1(const losBuffer *result, int num_segments, const losSegment *segments, void *pars)
{
  int i, npts = 0;

  for(i=0; i<num_segments; i++)
    {
      const ssLine *d = (const ssLine*)segments[i].Buffer.Data;
      cart_assert(d != 0);
      npts += d->NumPts;
    }

  *((int *)result->Data) = npts;
}


void ssCollector2(const losBuffer *result, int num_segments, const losSegment *segments, void *pars)
{
  int i, j, n, npts = 0;
  ssPoint *arr = (ssPoint*)result->Data;

  for(i=0; i<num_segments; i++)
    {
      const ssPoint *d = (const ssPoint *)segments[i].Buffer.Data;
      n = segments[i].Buffer.Size/sizeof(ssPoint);
      cart_assert(n==0 || d!=0);

      for(j=0; j<n; j++)
	{
	  arr[npts++] = d[j];
	}
    }

  cart_assert(sizeof(ssPoint)*npts == result->Size);

  qsort(arr,npts,sizeof(ssPoint),ssPointCmp);
}


void ssIntegrateLOSFlux(const ssInfo* info, ssLine *line, ssSpectrum *spec)
{
  static const double DTAUMIN = 1.0e-5;

  int i, l, ll, lu;
  double xtl, xtr, vtl, vtr, dx, dv, vb, facts, dtau;
  ssPoint *los = line->Data; /* cache for speed */

  int nn = spec->Length;
  double *w = spec->Vels;
  double *f = spec->Flux;
  double wmin = w[0];
  double wstp = w[1] - w[0];

  double am = info->AtomMass;
  double vd = 2.99793e5*4*3.1415927/info->DampCoefft;
  double fact0 = 8.85282e-21*2.99793e5*units->length* /* 8.85282e-21 = PI e^2/(m_e c^2) (A/cm) */
    info->Wavelength*info->OsStrength;

  for(i=1; i<line->NumPts; i++)
    {
      vb = 0.12856*sqrt(0.5*(los[i-1].vals[0]+los[i].vals[0])/am);
 
      xtl = los[i-1].r;
      xtr = los[i].r;

      vtl = MIN(los[i-1].v,los[i].v);
      vtr = MAX(los[i-1].v,los[i].v);

      dx = xtr - xtl;
      dv = vtr - vtl;

      ll = (int)(0.5+(vtl-wmin)/wstp);
      lu = (int)(0.5+(vtr-wmin)/wstp);
      if(ll <   0) ll = 0;
      if(ll >= nn) ll = nn - 1;
      if(lu <  ll) lu = ll;
      if(lu >= nn) lu = nn - 1;

      facts = fact0*0.5*(los[i-1].vals[1]+los[i].vals[1]);

      if(dv > 0.001*vb)
	{
	  /*
	  // Not a caustic
	  */
	  facts = facts*(dx/dv);
	  for(l=ll; l<=lu; l++)
	    {
	      f[l] += facts*voigtInt(vtl-w[l],vtr-w[l],vb,vd);
	    }
	  for(l=lu+1; l<nn; l++)
	    {
	      dtau = facts*voigtInt(vtl-w[l],vtr-w[l],vb,vd);
	      f[l] += dtau;
	      if(dtau < DTAUMIN) break;
	    }
	  for(l=ll-1; l>=0; l--)
	    {
	      dtau = facts*voigtInt(vtl-w[l],vtr-w[l],vb,vd);
	      f[l] += dtau;
	      if(dtau < DTAUMIN) break;
	    }
	}
      else
	{
	  /*
	  //  Velocity caustics
	  */
	  facts = facts*dx;
	  for(l=ll; l<=lu; l++)
	    {
	      f[l] += facts*voigt(w[l]-vtl,vb,vd);
	    }
	  for(l=lu+1; l<nn; l++)
	    {
	      dtau = facts*voigt(w[l]-vtl,vb,vd);
	      f[l] += dtau;
	      if(dtau < DTAUMIN) break;
            }
	  for(l=ll-1; l>=0; l--)
	    {
	      dtau = facts*voigt(w[l]-vtl,vb,vd);
	      f[l] += dtau;
	      if(dtau < DTAUMIN) break;
            }
	}
    }

  for(l=0; l<nn; l++)
    {
      f[l] = exp(-f[l]);
    }
}


void ssSampleLOSData(const ssInfo *info, int num_specs, double *pos[], double theta[], double phi[], double len, int floor_level, double pos_offset, ssLine *lines)
{
  int i, *nlos;
  losBuffer *results;
  losSegment *segments;
  ssPoint **los_buffers;

  ss_Init();

  cart_assert(num_specs > 0);
  cart_assert(lines != NULL);

  /*
  //  Init worker buffers
  */
  nlos = cart_alloc(int,num_specs);
  results = cart_alloc(losBuffer,num_specs);
  segments = cart_alloc(losSegment,num_specs);
  los_buffers = cart_alloc(ssPoint*,num_specs);

#pragma omp parallel for default(none), private(i), shared(num_specs,lines,segments,results,pos,theta,phi,pos_offset,floor_level,len,nlos,info), schedule(dynamic,1)
  for(i=0; i<num_specs; i++)
    {
      lines[i].Info = info;

      lines[i].e[0] = sin(theta[i])*cos(phi[i]);
      lines[i].e[1] = sin(theta[i])*sin(phi[i]);
      lines[i].e[2] = cos(theta[i]);

      lines[i].NumPts = lines[i].Size = 0;
      lines[i].Data = NULL;

      segments[i].Buffer.Size = sizeof(ssLine);
      segments[i].Buffer.Data = &lines[i];

      losTraverseSegment(i,pos[i],theta[i],phi[i],pos_offset,len,floor_level,ssWorker,segments+i,NULL);

      results[i].Size = sizeof(int);
      results[i].Data = nlos + i;
    }

  losCollectSegments(num_specs,results,segments,ssCollector1,NULL);

  for(i=0; i<num_specs; i++)
    {
      los_buffers[i] = cart_alloc(ssPoint,nlos[i]+1);  /*  in case nlos[i] == 0  */

      segments[i].Buffer.Size = lines[i].NumPts*sizeof(ssPoint);
      segments[i].Buffer.Data = lines[i].Data;
      results[i].Size = sizeof(ssPoint)*nlos[i];
      results[i].Data = los_buffers[i];
    }

  losCollectSegments(num_specs,results,segments,ssCollector2,NULL);

  for(i=0; i<num_specs; i++)
    {
      if(lines[i].Data != NULL) cart_free(lines[i].Data);
      lines[i].Data = los_buffers[i];
      lines[i].NumPts = nlos[i];
    }

  cart_free(los_buffers);
  cart_free(segments);
  cart_free(results);
  cart_free(nlos);
}


void ssComputeSpectra(const ssInfo *info, int num_specs, ssLine *lines, double *vel[], double vel_pixel, double vel_offset, ssSpectrum specs[])
{
  int i;
  double *vel_start, aH, wboff;

  ss_Init();

  cart_assert(num_specs > 0);

  aH = auni[min_level]*Hubble(auni[min_level])/cosmology->h*units->length_in_chimps;  /* this H per code unit */
  wboff = 5*0.12856*1.5e2*sqrt(1/info->AtomMass);

  /*
  //  Set the velocity space
  */
  vel_start = cart_alloc(double,num_specs);

#pragma omp parallel for default(none), private(i), shared(num_specs,vel,aH,wboff,units,constants,specs,vel_start,vel_pixel,vel_offset,lines), schedule(dynamic,1)
  for(i=0; i<num_specs; i++)
    {
      int j;
      double vmin, vmax, vsys;
      ssPoint *los = lines[i].Data;

      /*
      //  Is there a systemic velocity?
      */
      if(vel[i] != NULL)
	{
	  vsys = vel[i][0]*lines[i].e[0] + vel[i][1]*lines[i].e[1] + vel[i][2]*lines[i].e[2];
	}
      else
	{
	  vsys = 0;
	}

      for(j=0; j<lines[i].NumPts; j++)
	{
	  los[j].v = aH*los[j].r + (los[j].v-vsys)*units->velocity/constants->kms;
	}

      vmin = vmax = los[0].v;
      for(j=1; j<lines[i].NumPts; j++)
	{
	  vmin = MIN(vmin,los[j].v);
	  vmax = MAX(vmax,los[j].v);
	}
      vmax -= wboff;

      /*
      //  If there is a systemic velocity, start the spectrum at v=0
      */
      if(vel[i] != NULL)
	{
	  vmin = 0.0;
	}
      else
	{
	  vmin += wboff;
	}

      vmin += vel_offset;

      specs[i].Length = 1 + (int)(0.5+(vmax-vmin)/vel_pixel);
      if(specs[i].Length < 1) specs[i].Length = 1;
      vel_start[i] = vmin;
    }

  /*
  //  Allocate spectral data
  */
  for(i=0; i<num_specs; i++)
    {
      specs[i].Vels = cart_alloc(double,specs[i].Length);
      specs[i].Flux = cart_alloc(double,specs[i].Length);
    }

#pragma omp parallel for default(none), private(i), shared(num_specs,specs,vel_start,vel_pixel)
  for(i=0; i<num_specs; i++)
    {
      int j;

      for(j=0; j<specs[i].Length; j++)
	{
	  specs[i].Vels[j] = vel_start[i] + vel_pixel*j;
	  specs[i].Flux[j] = 0.0;
	}
    }

  cart_free(vel_start);

  /*
  //  Now we have all LOS data, compute fluxes.
  */
#pragma omp parallel for default(none), private(i), shared(num_specs,specs,lines,info), schedule(dynamic,1)
  for(i=0; i<num_specs; i++)
    {
      ssIntegrateLOSFlux(info,lines+i,specs+i);
    }
}


float ss_ran3(int *idum);

/*
//  Two useful wrappers
*/
double ssDumpRandomSpectra2(const ssInfo *info, const char *path, int num, double len, double vel_pixel, int floor_level, int random_seed, int dump_mode, int batch_size)
{
  int i, k;
  double favg = 0.0;
  int navg = 0;
  ssLine *lines;
  ssSpectrum *specs;
  double **pos, **vel, *theta, *phi, *pos_buffer;

  cart_assert(batch_size > 0);

  pos = cart_alloc(double*,batch_size);
  vel = cart_alloc(double*,batch_size);
  phi = cart_alloc(double,batch_size);
  theta = cart_alloc(double,batch_size);

  pos_buffer = cart_alloc(double,3*batch_size);
  for(k=0; k<batch_size; k++)
    {
      pos[k] = pos_buffer + 3*k;
      vel[k] = NULL;
    }

  /*
  //  Spectrum specialization
  */
  lines = cart_alloc(ssLine,batch_size);
  specs = cart_alloc(ssSpectrum,batch_size);

  for(i=0; i<num; i+=batch_size)
    {
      int j, n = batch_size;
      if(i+n > num) n = num - i; 
      
      for(k=0; k<n; k++)
	{
	  for(j=0; j<3; j++) pos[k][j] = ss_ran3(&random_seed)*num_grid;
	  phi[k] = ss_ran3(&random_seed)*2*M_PI;
	  theta[k] = acos(-1+2*ss_ran3(&random_seed));
	}

      ssSampleLOSData(info,n,pos,theta,phi,len,floor_level,0.0,lines);

      /*
      //  Write raw data if path is not NULL
      */
      if(dump_mode<=0 && path!=NULL && local_proc_id==MASTER_NODE)
	{
	  int v;
	  FILE *f;
	  char str[999];

	  for(k=0; k<n; k++)
	    {
	      sprintf(str,"%s/los.%05d.raw",path,i+k);
	      f = fopen(str,"w");
	      cart_assert(f != NULL);
	      fprintf(f,"# Columns:\n");
	      fprintf(f,"#   distance along LOS (in CHIMPs)\n");
	      fprintf(f,"#   peculiar velocity  (in km/s)\n");
	      fprintf(f,"#   %s\n",cp_temperature.Header);
	      for(v=0; v<info->NumVars; v++)
		{
		  fprintf(f,"#   %s\n",info->Vars[v].Header);
		}
	      fprintf(f,"# pos=(%lg,%lg,%lg), angles=(%lg,%lg)\n",pos[k][0],pos[k][1],pos[k][2],theta[k],phi[k]);
	      for(j=0; j<lines[k].NumPts; j++)
		{
		  fprintf(f,"%9.5lf %8.1lf",lines[k].Data[j].r*units->length_in_chimps,lines[k].Data[j].v*units->velocity/constants->kms);
		  for(v=0; v<=info->NumVars; v++) /* tem is the extra one */
		    {
		      fprintf(f," %9.3le",lines[k].Data[j].vals[v]);
		    }
		  fprintf(f,"\n");
		}
	      fclose(f);
	    }
	}

       if(dump_mode >= 0)
	{
	  ssComputeSpectra(info,n,lines,vel,vel_pixel,0.0,specs);

	  for(k=0; k<n; k++)
	    {
	      navg += specs[k].Length;
#pragma omp parallel for default(none), private(j), shared(k,specs), reduction(+:favg)
	      for(j=0; j<specs[k].Length; j++)
		{
		  favg += specs[k].Flux[j];
		}

	      /*
	      //  Write spectra if path is not NULL
	      */
	      if(path!=NULL && local_proc_id==MASTER_NODE)
		{
		  FILE *f;
		  char str[999];

		  sprintf(str,"%s/los.%05d.%s",path,i+k,info->FileExt);
		  f = fopen(str,"w");
		  cart_assert(f != NULL);
		  fprintf(f,"# vel (km/s)   flux\n");
		  fprintf(f,"# pos=(%lg,%lg,%lg), angles=(%lg,%lg)\n",pos[k][0],pos[k][1],pos[k][2],theta[k],phi[k]);
		  for(j=0; j<specs[k].Length; j++)
		    {
		      fprintf(f,"%8.1lf , %7.5lf\n",specs[k].Vels[j],specs[k].Flux[j]);
		    }
		  fclose(f);
		}

	      cart_free(specs[k].Vels);
	      cart_free(specs[k].Flux);
	    }
	}

      for(k=0; k<n; k++)
	{
	  cart_free(lines[k].Data);
	}
    }

  if(navg > 0)
    {
      favg /= navg;
    }

  cart_free(specs);
  cart_free(lines);

  cart_free(pos_buffer);
  cart_free(theta);
  cart_free(phi);
  cart_free(vel);
  cart_free(pos);

  return favg;
}


double ssDumpRandomSpectra(const ssInfo *info, const char *path, int num, double len, double vel_pixel, int floor_level, int random_seed, int dump_los_data)
{
#ifdef _OPENMP
  int nomp = omp_get_max_threads();
#else
  int nomp = 1;
#endif

  return ssDumpRandomSpectra2(info,path,num,len,vel_pixel,floor_level,random_seed,dump_los_data,1);
}


void ssDumpHalocentricSpectra(const ssInfo *info, const char *path, struct HALO_LIST *halos, double len, double vel_pixel, int floor_level, double vel_offset, double pos_offset, double halo_min_mass, long nside)
{
  int ihalo;
  int ipix, npix = 12*nside*nside;
  ssLine *lines;
  ssSpectrum *specs;
  double **pos, **vel, *theta, *phi, *pos_buffer, *vel_buffer;

  if(path == NULL)
    {
      cart_debug("No destination path specified. Skipping ssDumpHalocentricSpectra_HI1216...");
      return;
    }

  pos = cart_alloc(double*,npix);
  vel = cart_alloc(double*,npix);
  phi = cart_alloc(double,npix);
  theta = cart_alloc(double,npix);

  pos_buffer = cart_alloc(double,3*npix);
  vel_buffer = cart_alloc(double,3*npix);
  for(ipix=0; ipix<npix; ipix++)
    {
      pos[ipix] = pos_buffer + 3*ipix;
      vel[ipix] = vel_buffer + 3*ipix;
    }

  /*
  //  Spectrum specialization
  */
  lines = cart_alloc(ssLine,npix);
  specs = cart_alloc(ssSpectrum,npix);

  /*
  //  Galaxy-centered short HI spectra (for damping wings)
  */
  for(ihalo=0; ihalo<halos->num_halos; ihalo++) if(halos->list[ihalo].mvir > halo_min_mass)
    {
      int j, n = INT_MAX;

      for(ipix=0; ipix<npix; ipix++)
	{
	  hp_pix2ang_nest(nside,ipix,theta+ipix,phi+ipix);
	  for(j=0; j<3; j++)
	    {
	      pos[ipix][j] = halos->list[ihalo].pos[j];
	      vel[ipix][j] = halos->list[ihalo].vel[j];
	    }
	}

      ssSampleLOSData(info,npix,pos,theta,phi,len,floor_level,pos_offset,lines);
      ssComputeSpectra(info,npix,lines,vel,vel_pixel,vel_offset,specs);

      for(ipix=0; ipix<npix; ipix++)
	{
	  if(specs[ipix].Length < n) n = specs[ipix].Length;
	}

      if(local_proc_id == MASTER_NODE)
	{
	  int j;
	  FILE *f;
	  char str[999];
	  sprintf(str,"%s/los.%05d.fHI",path,ihalo);

	  f = fopen(str,"w");
	  cart_assert(f != NULL);
	  fprintf(f,"# id = %d, M = %lg\n",halos->list[ihalo].id,halos->list[ihalo].mvir*units->mass/constants->Msun);
	  fprintf(f,"# vel (km/s)   flux\n");
	  for(j=0; j<n; j++)
	    {
	      fprintf(f,"%8.1lf , %7.5lf",specs[0].Vels[j],specs[0].Flux[j]);
	      for(ipix=1; ipix<npix; ipix++)
		{
		  cart_assert(fabs(specs[0].Vels[j]-specs[ipix].Vels[j]) < 0.1*vel_pixel);
		  fprintf(f," , %7.5lf",specs[ipix].Flux[j]);
		}
	      fprintf(f,"\n");
	    }
	     
	  fclose(f);
	}

      for(ipix=0; ipix<npix; ipix++)
	{
	  cart_free(lines[ipix].Data);
	  cart_free(specs[ipix].Vels);
	  cart_free(specs[ipix].Flux);
	}
    }

  cart_free(specs);
  cart_free(lines);

  cart_free(vel_buffer);
  cart_free(pos_buffer);
  cart_free(theta);
  cart_free(phi);
  cart_free(vel);
  cart_free(pos);
}

#endif /* HYDRO && RADIATIVE_TRANSFER && COSMOLOGY */


#define MBIG 1000000000
#define MSEED 161803398
#define MZ 0
#define FAC (1.0/MBIG)

float ss_ran3(int *idum)
{
        static int inext,inextp;
        static long ma[56];
        static int iff=0;
        long mj,mk;
        int i,ii,k;

        if (*idum < 0 || iff == 0) {
                iff=1;
                mj=MSEED-(*idum < 0 ? -*idum : *idum);
                mj %= MBIG;
                ma[55]=mj;
                mk=1;
                for (i=1;i<=54;i++) {
                        ii=(21*i) % 55;
                        ma[ii]=mk;
                        mk=mj-mk;
                        if (mk < MZ) mk += MBIG;
                        mj=ma[ii];
                }
                for (k=1;k<=4;k++)
                        for (i=1;i<=55;i++) {
                                ma[i] -= ma[1+(i+30) % 55];
                                if (ma[i] < MZ) ma[i] += MBIG;
                        }
                inext=0;
                inextp=31;
                *idum=1;
        }
        if (++inext == 56) inext=1;
        if (++inextp == 56) inextp=1;
        mj=ma[inext]-ma[inextp];
        if (mj < MZ) mj += MBIG;
        ma[inext]=mj;
        return mj*FAC;
}

#undef MBIG
#undef MSEED
#undef MZ
#undef FAC


double voigt(double w, double wb, double wl)
{
  // 0.832554611 = sqrt(ln(2))

  double wd = 0.832554611*wb;
  double wv = 0.5346*wl + sqrt(0.2166*wl*wl+wd*wd);
  double d = (wl-wb)/(wl+wb);
  double cl = 0.68109 + d*(0.61293 + d*(-0.18384 - 0.11568*d));

  double x = (w/wv);
  x *= x;

  return (cl/3.1415927/(x+1)+(1-cl)*0.832554611/1.77245385*exp(-0.693147181*x))/wv;
}


double voigtInt(double w1, double w2, double wb, double wl)
{
  // 0.832554611 = sqrt(ln(2))

  double wd = 0.832554611*wb;
  double wv = 0.5346*wl + sqrt(0.2166*wl*wl+wd*wd);
  double d = (wl-wb)/(wl+wb);
  double cl = 0.68109 + d*(0.61293 + d*(-0.18384 - 0.11568*d));

  double x1 = w1/wv;
  double x2 = w2/wv;

  return cl/3.1415927*(atan(x2)-atan(x1)) + (1-cl)*0.5*(erf(0.832554611*x2)-erf(0.832554611*x1));
}

