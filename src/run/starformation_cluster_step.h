#ifndef __STAR_FORMATION_CLUSTER_STEP_H__
#define __STAR_FORMATION_CLUSTER_STEP_H__

#ifndef CONFIGURED
#error "Missing config.h include."
#endif

#ifdef STAR_FORMATION

void cluster_destruction(int level, double dt);
void cluster_move_to_peak(int level);

#endif /* STAR_FORMATION */

#endif
