#include "config.h"
#if defined(STAR_FORMATION) && defined(HYDRO) && defined(CLUSTER_DETAIL_OUTPUT)

#include <math.h>
#include <stdio.h>

#include "auxiliary.h"
#include "iterators.h"
#include "parallel.h"
#include "particle.h"
#include "rand.h"
#include "starformation.h"
#include "starformation_recipe.h"
#include "starformation_algorithm.h"
#include "starformation_feedback.h"
#include "times.h"
#include "timing.h"
#include "tree.h"
#include "units.h"

#include "cosmology.h"

#include "starformation_step.h"
#include "step.h"

#ifdef LOG_STAR_CREATION
#include "logging.h"
#endif

int loop_over_clusters(int icell, double freq_t, int freq_part);

void output_clusters(int level, int time_multiplier) {
  int i, j;
  int icell;
  int num_level_cells;
  int *level_cells;
  double dt_eff;
  float *sfr;

  start_time( WORK_TIMER );

  select_level( level, CELL_TYPE_LOCAL | CELL_TYPE_LEAF,  &num_level_cells, &level_cells );

/* cannot be done shared-memory parallel */
/* #pragma omp parallel for default(none), private(i,icell), shared(num_level_cells,level_cells,level,sf_formstar,dt_eff, sfr, dtl), schedule(dynamic) */
  for(i=0;i<num_level_cells; i++)
    {
        loop_over_clusters(level_cells[i], 2e5, 1);
    }

  cart_free(level_cells);
  end_time( WORK_TIMER );
}

/* loop over cluster particles within star forming cells 
 * output cluster properties if needed.
 * frequency controls the fraction of clusters to be printed out */
extern void eigenvalue_wiki(double A[6], double *l);
extern double max2(double x, double y, double z);
int loop_over_clusters(int icell, double mass_thre, int freq_part){
    int ipart;
    double sage;
    double digit;
    double dv, pos[nDim], dx;
    int icell_peak, nb26[26], j;

    int level;
    double l2_inv;
    int CubeStencilSize = 26;
    double phis[CubeStencilSize];
    double phi_c, phi_ii, phi_xx, phi_yy, phi_zz, phi_xy, phi_xz, phi_yz;
    double tidal_tensor[6], eigenvalue[3];

    if (icell<0) return -1;
    
    dv = 0.0;
    ipart = cell_particle_list[icell]; 
    while ( ipart != NULL_PARTICLE )
    {
	    if(particle_is_star(ipart)
    #ifdef STAR_PARTICLE_TYPES
	        && (star_particle_type[ipart] == STAR_TYPE_NORMAL 
	            || star_particle_type[ipart] == STAR_TYPE_FAST_GROWTH)
    #endif
	      )
	    {
            sage = (particle_t[ipart] - star_tbirth[ipart])*units->time/constants->yr;

	        /* if (sage < 1.5e7 && particle_id[ipart]%freq_part==0 && digit<0.0) */
	        if (sage > 1.5e7 && star_initial_mass[ipart]*units->mass/constants->Msun>mass_thre && particle_id[ipart]%freq_part==0)
	        //if (particle_id[ipart]==freq_part && digit<0.0)
            {
                /*
                cart_debug("Hui: id=%d, icell=%d, particle_level=%d, mi=%f, mc=%f, age=%f, density=%f, f_H2=%f",
                particle_id[ipart], icell, particle_level[ipart],
                star_initial_mass[ipart]*units->mass/constants->Msun, particle_mass[ipart]*units->mass/constants->Msun,
                sage, cell_gas_density(icell)*units->number_density*constants->XH, cell_H2_fraction(icell));
                */

                /* relative velocity to the gas cell */
                /* distance to the cell center */
                /* find the cell id of the density peak of the 27 cells */
                /*
	            dv = sqrt( pow(particle_v[ipart][0]-cell_momentum(icell,0)/cell_gas_density(icell) , 2)+
		        pow(particle_v[ipart][1]-cell_momentum(icell,1)/cell_gas_density(icell) , 2)+
		        pow(particle_v[ipart][2]-cell_momentum(icell,2)/cell_gas_density(icell) , 2) );

                cell_center_position( icell, pos );
                dx = sqrt( (particle_x[ipart][0]-pos[0])*(particle_x[ipart][0]-pos[0])+
                    (particle_x[ipart][1]-pos[1])*(particle_x[ipart][1]-pos[1])+
                    (particle_x[ipart][2]-pos[2])*(particle_x[ipart][2]-pos[2]) );
                
                icell_peak = icell;
                GetCubeStencil(cell_level(icell), icell, nb26);
                for( j=0; j<26; j++)
                {
                    if ( nb26[j] != -1 )
                    {
                #ifdef RADIATIVE_TRANSFER
                        if ( cell_H2_density(icell_peak)<cell_H2_density(nb26[j]) )
                #else
                        if ( cell_gas_density(icell_peak)<cell_gas_density(nb26[j]) )
                #endif
                            icell_peak = nb26[j];
                    }
                }
                */
                /*
                cart_debug("Hui: id=%d, particle_level=%d, icell=%d, mi=%f, mc=%f, age=%f, density=%f, f_H2=%f, metallicity=%f",
                particle_id[ipart], cell_level(icell), icell,
                star_initial_mass[ipart]*units->mass/constants->Msun,
                particle_mass[ipart]*units->mass/constants->Msun, sage/1e6,
                cell_gas_density(icell)*units->number_density*constants->XH,
                cell_H2_fraction(icell),
                star_metallicity_II[ipart]
                );
                */
                /*
                cart_debug("Hui: dv=%f,%f,%f",
                        (particle_v[ipart][0]-cell_momentum(icell, 0)/cell_gas_density(icell))*units->velocity/constants->kms, 
                        (particle_v[ipart][1]-cell_momentum(icell, 1)/cell_gas_density(icell))*units->velocity/constants->kms, 
                        (particle_v[ipart][2]-cell_momentum(icell, 2)/cell_gas_density(icell))*units->velocity/constants->kms
                        );
                */
                
                level = particle_level[ipart];
                l2_inv = cell_size_inverse[level]*cell_size_inverse[level];

                phi_c = cell_potential(icell);
                phi_ii = 0.0;
                GetCubeStencil(level, icell, nb26);
                for ( j=0; j<CubeStencilSize; j++ ) phis[j] = cell_potential(nb26[j]);
                // for ( j=0; j<18; j++ ) cart_assert( phis[j]!=0.0 );
            
                tidal_tensor[0] = (phis[0]+phis[1]-2*phi_c)*l2_inv;
                tidal_tensor[3] = (phis[2]+phis[3]-2*phi_c)*l2_inv;
                tidal_tensor[5] = (phis[4]+phis[5]-2*phi_c)*l2_inv;
                tidal_tensor[1] = 0.25*(phis[6]+phis[9]-phis[7]-phis[8])*l2_inv;
                tidal_tensor[2] = 0.25*(phis[10]+phis[15]-phis[11]-phis[14])*l2_inv;
                tidal_tensor[4] = 0.25*(phis[12]+phis[17]-phis[13]-phis[16])*l2_inv;
                //eigenvalue_wiki(tidal_tensor, eigenvalue);
                for (j=0; j<6; j++) tidal_tensor[j] /= units->time*units->time/constants->Gyr/constants->Gyr;

                FILE * fp;
                char tidal_filename[256];
                sprintf(tidal_filename, "./tidal/%d.txt", particle_id[ipart]);
                fp = fopen(tidal_filename, "a");
                fprintf(fp, "%g,%g,%g,%f,%f,%f,%f,%f,%f,%f,%f,%f\n",
                tphys_from_tcode(tl[level])/1e9,
                particle_mass[ipart]*units->mass/constants->Msun,
                sage, particle_x[ipart][0], particle_x[ipart][1], particle_x[ipart][2],
                tidal_tensor[0], tidal_tensor[1], tidal_tensor[2], tidal_tensor[3], tidal_tensor[4], tidal_tensor[5]);
                fclose(fp);
                
                /*
                cart_debug("Hui: age, TT_full===%f, %f,%f,%f,%f,%f,%f",
                            tphys_from_tcode(tl[level]),
                            tidal_tensor[0], tidal_tensor[1], tidal_tensor[2], tidal_tensor[3], tidal_tensor[4], tidal_tensor[5]);
                */
                //cart_debug("Hui: ipart=%d, sage=%f, eig1=%f, eig2=%f, eig3=%f", particle_id[ipart], sage, eigenvalue[0], eigenvalue[1], eigenvalue[2]);
                /*
                for ( j=0; j<18; j++ )
                {
                    if ( phis[j]==0.0 )
                    {
                    cart_debug("ipart=%d, sage=%f, mass=%f, TT_diag=%f,%f,%f,%f,%f,%f",
                            particle_id[ipart], sage, star_initial_mass[ipart]*units->mass/constants->Msun,
                            tidal_tensor[0], tidal_tensor[1], tidal_tensor[2], tidal_tensor[3], tidal_tensor[4], tidal_tensor[5]);
                    break;
                    }
                }
                if (j==18)
                {
                    cart_debug("ipart=%d, sage=%f, mass=%f, TT_full=%f,%f,%f,%f,%f,%f",
                            particle_id[ipart], sage, star_initial_mass[ipart]*units->mass/constants->Msun,
                            tidal_tensor[0], tidal_tensor[1], tidal_tensor[2], tidal_tensor[3], tidal_tensor[4], tidal_tensor[5]);
                }
                */
            }
	    }
	    ipart = particle_list_next[ipart];
    }
    return -1;
}

#endif /* STAR_FORMATION && HYDRO */
