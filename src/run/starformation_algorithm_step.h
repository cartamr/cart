#ifndef __STAR_FORMATION_ALGORITHM_STEP_H__
#define __STAR_FORMATION_ALGORITHM_STEP_H__


#ifndef CONFIGURED
#error "Missing config.h include."
#endif


#ifdef STAR_FORMATION
#ifdef LOG_STAR_DESTRUCTION
extern int local_remove_counter;
#endif /* LOG_STAR_DESTRUCTION */
void star_destruction(int level);
#endif /* STAR_FORMATION */

#endif
