#include "config.h"
#ifdef SGS_TURBULENCE

#include <math.h>

#include "auxiliary.h"
#include "hydro_sgst.h"
#include "iterators.h"
#include "tree.h"
#include "units.h"

#include "step.h"
#include "hydro_step.h"
#include "hydro_sgst_step.h"


#ifndef M_SQRT1_2
#define M_SQRT1_2	0.70710678118654752440	/* 1/sqrt(2) */
#endif
#ifndef M_SQRT1_3
#define M_SQRT1_3	0.57735026918962584421	/* 1/sqrt(3) */
#endif

extern int momentum_permute[2*nDim][nDim];

extern int sweep_direction;
extern int sweep_dimension;

extern double dtx;

#ifndef SGST_SHEAR_IMPROVED
extern float ref[num_cells];
#endif /* !SGST_SHEAR_IMPROVED */

#ifdef SGST_SHEAR_IMPROVED
void sgst_init_mean_velocities();
#endif /* SGST_SHEAR_IMPROVED */

void hydro_sgst_init() {
#ifdef SGST_SHEAR_IMPROVED
		sgst_init_mean_velocities();
#endif /* SGST_SHEAR_IMPROVED */
}

double sgst_tau_ij_traceless( int icell, int i, int j) {
	double tau_ij;
	double nl, dvdx1, dvdx2, dvdx_norm2, S_ij;
	int L, R;
	double vL, vR;
	int level, k, l;

	level = cell_level(icell);

	L = cell_neighbor( icell, (2*i) );
	R = cell_neighbor( icell, (2*i+1) );

	vL = cell_turb_velocity(L,j);
	vR = cell_turb_velocity(R,j);

	S_ij = (vR - vL);

	L = cell_neighbor( icell, (2*j) );
	R = cell_neighbor( icell, (2*j+1) );

	vL = cell_turb_velocity(L,i);
	vR = cell_turb_velocity(R,i);

	S_ij += (vR - vL);
	S_ij *= (0.25 * cell_size_inverse[level]);

	if ( i == j ) {
#ifdef SGST_SHEAR_IMPROVED
		nl = 0.0;
		for (k = 0; k < nDim; k++) {
			L = cell_neighbor( icell, (2*k) );
			R = cell_neighbor( icell, (2*k+1) );

			vL = cell_turb_velocity(L,k);
			vR = cell_turb_velocity(R,k);

			nl += (vR - vL);
		}
		S_ij -= nl * cell_size_inverse[level] / 6.;

#else
		S_ij += ref[icell] / 3.; // ref = -div(v)
#endif /* SGST_SHEAR_IMPROVED */
	}

	tau_ij = 2. * sgst_model.c1 * cell_size[level] * sqrt(2. * cell_gas_density(icell) * cell_gas_turbulent_energy(icell)) * S_ij;

	if ( sgst_model.c2 > 0. ) {
		dvdx_norm2 = 0.0;
		for (k = 0; k < nDim; k++) {
			for (l = 0; l < nDim; l++) {
				L = cell_neighbor( icell, (2*k) );
				R = cell_neighbor( icell, (2*k+1) );

				vL = cell_turb_velocity(L,l);
				vR = cell_turb_velocity(R,l);

				dvdx1 = (vR - vL);

				dvdx_norm2 += dvdx1 * dvdx1;
			}
		}
		dvdx_norm2 *= 2.;

		if ( dvdx_norm2 > 0. ) {
			nl = 0.0;
			for (k = 0; k < nDim; k++) {
				L = cell_neighbor( icell, (2*k) );
				R = cell_neighbor( icell, (2*k+1) );

				vL = cell_turb_velocity(L,i);
				vR = cell_turb_velocity(R,i);

				dvdx1 = (vR - vL);

				vL = cell_turb_velocity(L,j);
				vR = cell_turb_velocity(R,j);

				dvdx2 = (vR - vL);

				nl += dvdx1 * dvdx2;
			}
			tau_ij -= 4. * sgst_model.c2 * cell_gas_turbulent_energy(icell) * nl / dvdx_norm2;
		}

		if ( i == j ) {
			tau_ij += (2./3.) * sgst_model.c2 * cell_gas_turbulent_energy(icell) ;
		}
	}

	return tau_ij;
}

void hydro_sgst_add_turbulent_fluxes( int cell_list[4], double f[num_hydro_vars-1] ) {
	int i, dim_v;
	int L, R;
	double tauL, tauR;
	double vL, vR;
	double Flux_of_E, Flux_of_K, Flux_of_e;
	double DL, DR, gradL, gradR;

	L = cell_list[1];
	R = cell_list[2];

	Flux_of_E = 0.0;
	for ( i = 0; i < nDim; i++ ) {
		dim_v = momentum_permute[sweep_direction][i];

		tauL = sgst_tau_ij_traceless( L, sweep_dimension, dim_v );
		tauR = sgst_tau_ij_traceless( R, sweep_dimension, dim_v );

		f[i+1] -= dtx * 0.5 * ( tauR + tauL );

		vL = cell_gas_velocity(L,dim_v);
		vR = cell_gas_velocity(R,dim_v);

		Flux_of_E -= vR*tauR + vL*tauL;
	}
	Flux_of_E *= 0.5;

	/* Turbulent diffusion: */
	Flux_of_K = Flux_of_e = 0.0;
	if ( sgst_model.ck > 0.0 ) {
		DL = sqrt( cell_gas_density(L)*cell_gas_turbulent_energy(L) );
		DR = sqrt( cell_gas_density(R)*cell_gas_turbulent_energy(R) );

		gradL = (cell_gas_turbulent_energy(cell_list[2])/cell_gas_density(cell_list[2])) - (cell_gas_turbulent_energy(cell_list[0])/cell_gas_density(cell_list[0]));
		gradR = (cell_gas_turbulent_energy(cell_list[3])/cell_gas_density(cell_list[3])) - (cell_gas_turbulent_energy(cell_list[1])/cell_gas_density(cell_list[1]));
		Flux_of_K = - sgst_model.ck * 0.25 * ( DL * gradL + DR * gradR );

		gradL = (cell_gas_internal_energy(cell_list[2])/cell_gas_density(cell_list[2])) - (cell_gas_internal_energy(cell_list[0])/cell_gas_density(cell_list[0]));
		gradR = (cell_gas_internal_energy(cell_list[3])/cell_gas_density(cell_list[3])) - (cell_gas_internal_energy(cell_list[1])/cell_gas_density(cell_list[1]));
		Flux_of_e = - sgst_model.ck * 0.25 * ( DL * gradL + DR * gradR );

		Flux_of_E += Flux_of_K + Flux_of_e;
	}

	f[4] += dtx * Flux_of_E;
	f[6+num_electronion_noneq_vars] += dtx * Flux_of_e;
	f[7+num_electronion_noneq_vars] += dtx * Flux_of_K;
}


#define dvdx(i,j) _dvdx[i*nDim+j]
#define S_ij(i,j) _S_ij[i*nDim+j]

#ifdef SGST_SHEAR_IMPROVED
#define dudx(i,j) _dudx[i*nDim+j]
#define U_ij(i,j) _U_ij[i*nDim+j]
#else
#define dudx(i,j) dvdx(i,j)
#define U_ij(i,j) S_ij(i,j)
#endif /* SGST_SHEAR_IMPROVED */

void hydro_sgst_advance_turbulent_energy( int level ) {
	int i, icell, dim_v, dim_x, dim_y;
	int num_level_cells;
	int *level_cells;
	float Ksgs_max, Ksgs_old;
	double dK, f;
	double v_turb, v_seed;

	const double dtx = cell_size_inverse[level] * dtl[level];

	const double c1_factor = sgst_model.c1 * dtx * 0.25 * M_SQRT1_2;
	const double c2_factor = sgst_model.c2 * dtx * 2.0;
	const double ce_factor = sgst_model.ce * dtx;

	int L, R;
	double vL, vR;

	double *_dvdx, *_dudx;
	double dvdx_norm2, dvdx_trace, dudx_trace;

	double *_S_ij, *_U_ij;
	double S_norm2, vki_vkj_Sij;

	select_level( level, CELL_TYPE_LOCAL | CELL_TYPE_LEAF, &num_level_cells, &level_cells );

#pragma omp parallel \
		default(none), \
		shared(level,num_level_cells,level_cells,cell_vars_data,sgst_v_rms_ceiling,constants,units), \
		private(i,icell,dim_v,dim_x,dim_y,_dudx,_dvdx,dvdx_norm2,dvdx_trace,dudx_trace,_U_ij,_S_ij,S_norm2,vki_vkj_Sij,vL,vR,L,R,Ksgs_max,Ksgs_old,dK,f,v_turb,v_seed)
	{
		_dvdx = cart_alloc( double, nDim*nDim );
		_S_ij = cart_alloc( double, nDim*nDim );
#ifdef SGST_SHEAR_IMPROVED
		_dudx = cart_alloc( double, nDim*nDim );
		_U_ij = cart_alloc( double, nDim*nDim );
#endif /* SGST_SHEAR_IMPROVED */

#pragma omp for
		for ( i = 0; i < num_level_cells; i++ ) {
			icell = level_cells[i];

			Ksgs_old = cell_gas_turbulent_energy(icell);
			v_turb = sqrt( Ksgs_old / cell_gas_density(icell) ); // turbulent velocity

			/* SGS turbulence dissipation into internal energy: */
			f = ce_factor * v_turb; 												// ratio of timestep to decay time f = dt / t_decay:
			dK = ((f*(f+4))/((f+2)*(f+2))) * Ksgs_old; 			 					// solution for dK/dt = - rho*epsilon
			cell_gas_turbulent_energy(icell) -= dK;
			cell_gas_internal_energy(icell) += dK;

#ifndef SGST_ISOTROPIC

			/* velocity gradients (v_i,j) */
			for ( dim_v = 0; dim_v < nDim; dim_v++ ) {
				for ( dim_x = 0; dim_x < nDim; dim_x++ ) {

					L = cell_neighbor( icell, (2*dim_x) );
					R = cell_neighbor( icell, (2*dim_x+1) );

					vL = cell_turb_velocity(L,dim_v);
					vR = cell_turb_velocity(R,dim_v);

					dvdx(dim_v,dim_x) = ( vR - vL );

#ifdef SGST_SHEAR_IMPROVED
					vL = cell_gas_velocity(L,dim_v);
					vR = cell_gas_velocity(R,dim_v);

					dudx(dim_v,dim_x) = ( vR - vL );
#endif /* SGST_SHEAR_IMPROVED */
				}
			}

			/* velocity divergence (v_i,i) */
			dvdx_trace = 0.0;
			for ( dim_v = 0; dim_v < nDim; dim_v++ ) {
				dvdx_trace += dvdx(dim_v,dim_v);
			}

			/* Traceless rate-of-strain tensor (S*_ij) */
			for ( dim_v = 0; dim_v < nDim; dim_v++ ) {
				S_ij(dim_v,dim_v) = dvdx(dim_v,dim_v) - dvdx_trace/3.;
				for ( dim_x = dim_v+1; dim_x < nDim; dim_x++ ) {
					S_ij(dim_v,dim_x) = S_ij(dim_x,dim_v) = 0.5 * ( dvdx(dim_v,dim_x) + dvdx(dim_x,dim_v) );
				}
			}

#ifdef SGST_SHEAR_IMPROVED
			dudx_trace = 0.0;
			for ( dim_v = 0; dim_v < nDim; dim_v++ ) {
				dudx_trace += dudx(dim_v,dim_v);
			}

			/* Total rate-of-strain tensor (U_ij) */
			for ( dim_v = 0; dim_v < nDim; dim_v++ ) {
				U_ij(dim_v,dim_v) = dudx(dim_v,dim_v) - dudx_trace/3.;
				for ( dim_x = dim_v+1; dim_x < nDim; dim_x++ ) {
					U_ij(dim_v,dim_x) = U_ij(dim_x,dim_v) = 0.5 * ( dudx(dim_v,dim_x) + dudx(dim_x,dim_v) );
				}
			}
#endif /* SGST_SHEAR_IMPROVED */

			/* Norms of v_i,j and S*_ij */
			dvdx_norm2 = 0.0;
			S_norm2 = 0.0;
			for ( dim_v = 0; dim_v < nDim; dim_v++ ) {
				dvdx_norm2 += dvdx(dim_v,dim_v)*dvdx(dim_v,dim_v);
				S_norm2 += S_ij(dim_v,dim_v)*U_ij(dim_v,dim_v);
				for ( dim_x = dim_v+1; dim_x < nDim; dim_x++ ) {
					dvdx_norm2 += dvdx(dim_v,dim_x)*dvdx(dim_v,dim_x) + dvdx(dim_x,dim_v)*dvdx(dim_x,dim_v);
					S_norm2 += 2.0*S_ij(dim_v,dim_x)*U_ij(dim_v,dim_x);
				}
			}
			dvdx_norm2 *= 2.0;
			S_norm2 *= 2.0;

			/* non-linear term v_k,i * v_k,j * S_ij */
			if ( c2_factor > 0.0 && dvdx_norm2 > 0.0 ) {
				vki_vkj_Sij = 0.0;
				for ( dim_v = 0; dim_v < nDim; dim_v++ ) {
					for ( dim_x = 0; dim_x < nDim; dim_x++ ) {
						vki_vkj_Sij += dvdx(dim_x,dim_v) * dvdx(dim_x,dim_v) * U_ij(dim_x,dim_x);
						for ( dim_y = dim_x+1; dim_y < nDim; dim_y++ ) {
							vki_vkj_Sij += 2.0 * dvdx(dim_x,dim_v) * dvdx(dim_y,dim_v) * U_ij(dim_x,dim_y);
						}
					}
				}
				vki_vkj_Sij /= dvdx_norm2;
			} else {
				vki_vkj_Sij = 0.0;
			}

			/* Anisotropic eddy-viscosity: */
			v_seed = c1_factor * S_norm2;											// characteristic turbulent velocity seed
			dK = (v_seed*(v_seed+2.0*v_turb)) * cell_gas_density(icell);			// solution for dK/dt = Sigma

			cell_gas_turbulent_energy(icell) += dK;

			/* Non-linear stress: */
			f = c2_factor * vki_vkj_Sij;
			dK = ( f == 0.0 ) ? 0.0 : (exp(-f)-1) * Ksgs_old;
			cell_gas_turbulent_energy(icell) += dK;

#endif /* !SGST_ISOTROPIC */

			Ksgs_max = 0.5 * cell_gas_density(icell) * pow( sgst_v_rms_ceiling*constants->kms/units->velocity, 2 );
			cell_gas_turbulent_energy(icell) = MAX( Ksgs_min, MIN( cell_gas_turbulent_energy(icell), Ksgs_max ) );
		}
	} /* end of omp-parallel region */

	cart_free( level_cells );
}



#ifdef SGST_SHEAR_IMPROVED
void sgst_init_mean_velocities() {
	int dim,i,icell,num_level_cells,level;
	int *level_cells;

	for ( level = min_level; level <= max_level; level++ ) {
		select_level( level, CELL_TYPE_ANY, &num_level_cells, &level_cells );

#pragma omp parallel for default(none), private(i,icell,dim), shared(num_level_cells,level_cells,cell_vars_data)
		for ( i = 0; i < num_level_cells; i++ ) {
			icell = level_cells[i];
			for ( dim = 0; dim < nDim; dim++ ) {
				cell_sgst_mean_velocity(icell, dim) = cell_momentum(icell, dim) / cell_gas_density(icell);
			}

		}
		cart_free( level_cells );
	}
}

void hydro_sgst_update_mean_velocities( int level ) {
	int dim,i,icell,num_level_cells;
	int *level_cells;
	float sgst_shear_weight;

	if ( sgst_model.shear_time > 0.0 ) {
		sgst_shear_weight = (2.*M_PI*M_SQRT1_3) * dtl[level] / (sgst_model.shear_time*constants->yr/units->time);
		sgst_shear_weight = MIN( sgst_shear_weight, 1.0 );
	} else {
		sgst_shear_weight = 1.0;
	}

#ifdef SGST_DEBUG
	cart_debug("SGST_DEBUG: t_shear = %.3e Myr; weight = %.3e",sgst_model.shear_time*1e-6,sgst_shear_weight);
#endif /* SGST_DEBUG */

	select_level( level, CELL_TYPE_ANY_LEAF, &num_level_cells, &level_cells );
#pragma omp parallel for default(none), private(i,icell,dim), shared(num_level_cells,level_cells,cell_vars_data,sgst_shear_weight)
	for ( i = 0; i < num_level_cells; i++ ) {
		icell = level_cells[i];

		for (dim=0; dim<nDim; dim++) {
			cell_sgst_mean_velocity(icell,dim) = ( 1.0 - sgst_shear_weight ) * cell_sgst_mean_velocity(icell,dim) + sgst_shear_weight * cell_momentum(icell,dim) / cell_gas_density(icell);
		}
	}
	cart_free( level_cells );
}
#endif /* SGST_SHEAR_IMPROVED */
#endif /*SGS_TURBULENCE*/
