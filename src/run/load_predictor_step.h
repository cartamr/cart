#ifndef __LOAD_PREDICTOR_STEP_H__
#define __LOAD_PREDICTOR_STEP_H__

#ifndef CONFIGURED
#error "Missing config.h include."
#endif

void lp_level_step_end( int level );

#endif
