#ifndef __HYDRO_SGST_STEP_H__
#define __HYDRO_SGST_STEP_H__

#ifndef CONFIGURED
#error "Missing config.h include."
#endif

#ifdef SGS_TURBULENCE

void hydro_sgst_init();
void hydro_sgst_advance_turbulent_energy( int level );
void hydro_sgst_add_turbulent_fluxes( int cell_list[4], double f[ /* num_hydro_vars-1 */ ] );
void hydro_sgst_update_mean_velocities( int level );

#endif /* SGS_TURBULENCE */
#endif /* __HYDRO_SGST_H__ */
