#include "config.h"
#if defined(STAR_FORMATION) && defined(PARTICLES)

#include <math.h>
#include "auxiliary.h"
#include "iterators.h"
#include "particle.h"
#include "starformation_cluster.h"
#include "times.h"
#include "timing.h"
#include "tree.h"
#include "units.h"
#include "starformation.h"

#include "starformation_cluster_step.h"
#include "step.h"

extern double cluster_age_spread_code;
extern double cluster_max_gap_code;

#if defined(CLUSTER_BOUND_FRACTION)
void cluster_destruction(int level, double dt) {
	int i;
	int ipart;
	int iter_cell;
	int num_level_cells;
	int *level_cells;
    double star_age; //, star_gap;

	start_time( WORK_TIMER );

    setup_cluster_destruction();

	select_level( level, CELL_TYPE_LOCAL | CELL_TYPE_LEAF, &num_level_cells, &level_cells );
#pragma omp parallel for private(iter_cell,ipart)
	for ( i = 0; i < num_level_cells; i++ ) {
		iter_cell = level_cells[i];

		ipart = cell_particle_list[iter_cell];
		while ( ipart != NULL_PARTICLE ) {
			if ( particle_is_star(ipart) 
#ifdef STAR_PARTICLE_TYPES
			     && (   star_particle_type[ipart] == STAR_TYPE_NORMAL 
                                 || star_particle_type[ipart] == STAR_TYPE_STARII 
                                 || star_particle_type[ipart] == STAR_TYPE_FAST_GROWTH) 
#endif /* STAR_PARTICLE_TYPES */
			) {
                star_age = particle_t[ipart] - star_tbirth[ipart];
                // star_gap = particle_t[ipart] - star_tfinal[ipart];
                // cart_debug("ipart=%d, age=%f", ipart, star_age*units->time/constants->Myr);
                if (star_age>cluster_age_spread_code && star_fbound[ipart]>1e-6)
                // if ( (star_age>cluster_age_spread_code || star_gap>cluster_max_gap_code) && star_fbound[ipart]>1e-6)
                    single_cluster_destruction(level,iter_cell,ipart,dt);
			}

			ipart = particle_list_next[ipart];
		}
	}

	cart_free(level_cells);

	end_time( WORK_TIMER );
}
#endif /* CLUSTER_BOUND_FRACTION */

#if defined(CLUSTER_FIXED)
extern int neighbor_side_child[num_neighbors][4];
extern int sf_min_level;
void move_to_peak(int icell_from, int icell_to, int ipart);

void cluster_move_to_peak(int level)
{
    int i, j, m;
    int ipart, ipart_move;
    int iter_cell, icell_peak, icell_child, iside;
    int n_active;
    int num_level_cells;
    int *level_cells;
    int nb[num_neighbors];
    double dot_prod, norm_peak, norm_part, cos_vel, cos_temp;

	start_time( WORK_TIMER );

    cart_assert( level>=sf_min_level );

	select_level( level, CELL_TYPE_LOCAL | CELL_TYPE_LEAF, &num_level_cells, &level_cells );

#pragma omp parallel for default(none), private(iter_cell,ipart,ipart_move,nb,icell_child,icell_peak,iside,n_active,i,j,dot_prod,norm_peak,norm_part,cos_vel,cos_temp), shared(cluster_age_spread_code,neighbor_side_child,num_level_cells,level_cells,level,cell_vars_data,cell_particle_list,particle_id,particle_species_indices,num_particle_species,particle_list_next,star_particle_type,particle_t,star_tbirth,units,constants,particle_v,cell_child_oct), schedule(dynamic)
	for ( m = 0; m < num_level_cells; m++ ) {
		iter_cell = level_cells[m];
        
        /* find the cell id of the density maximum of the 27 cells
        int nb26[26];
        icell_peak = iter_cell;
        GetCubeStencil(level, iter_cell, nb26);
        for( i=0; i<26; i++)
        {
            if ( nb26[i] != -1 )
            {
                if ( cell_is_refined(nb26[i]) )
                {
                    for ( j=0;j<num_children;j++ )
                    {
                        icell_child = cell_child(nb26[i], j);
                        if ( cell_is_leaf(icell_child) )
                        {
                        #ifdef RADIATIVE_TRANSFER
                            if ( cell_H2_density(icell_peak)<cell_H2_density(icell_child) )
                        #else 
                            if ( cell_gas_density(icell_peak)<cell_gas_density(icell_child) )
                        #endif
                                icell_peak = icell_child;
                        }
                    }
                }
                else
                {
                #ifdef RADIATIVE_TRANSFER
                    if ( cell_H2_density(icell_peak)<cell_H2_density(nb26[i]) )
                #else
                    if ( cell_gas_density(icell_peak)<cell_gas_density(nb26[i]) )
                #endif
                        icell_peak = nb26[i];
                }
            }
        }
        */

        /* find the cell id of the density maximum of the 6 immediate neighbors */
        icell_peak = iter_cell;
        cell_all_neighbors( iter_cell, nb );

        for ( i=0; i<num_neighbors; i++ )
        {
            if ( nb[i]!=-1 && cell_level(nb[i])>=level ) /* only consider cells at same or finer level */
            {
                /* if the nb cell is refined, only consider the four children
                 * in the nb cell that are closest to the i-th face of iter_cell */
                if ( cell_is_refined(nb[i]) )
                {
                    for ( j=0; j<4; j++)
                    {
                        iside = neighbor_side_child[i][j];
                        icell_child = cell_child(nb[i], iside);
                        cart_assert( cell_is_leaf(icell_child) );
                        #ifdef RADIATIVE_TRANSFER
                            if ( cell_H2_density(icell_peak)<cell_H2_density(icell_child) )
                        #else /* RADIATIVE_TRANSFER */
                            if ( cell_gas_density(icell_peak)<cell_gas_density(icell_child) )
                        #endif /* RADIATIVE_TRANSFER */
                                icell_peak = icell_child;
                    }
                }
                else
                {
                #ifdef RADIATIVE_TRANSFER
                    if ( cell_H2_density(icell_peak)<cell_H2_density(nb[i]) )
                #else /* RADIATIVE_TRANSFER */
                    if ( cell_gas_density(icell_peak)<cell_gas_density(nb[i]) )
                #endif /* RADIATIVE_TRANSFER */
                        icell_peak = nb[i];
                }
            }
        }
        
        /* move active particle to density maximum if the velocity vectors of
         * active particle and the density maximum are in the same direction */
        if (icell_peak!=iter_cell)
        {
            /* how many active particles are in density maximum? */
            cart_assert( cell_is_leaf(icell_peak) );
            ipart = cell_particle_list[icell_peak];
            n_active = 0;
		    while ( ipart != NULL_PARTICLE ) 
            {
                if (particle_is_star(ipart)
        #ifdef STAR_PARTICLE_TYPES
	            && (star_particle_type[ipart] == STAR_TYPE_NORMAL 
	                || star_particle_type[ipart] == STAR_TYPE_FAST_GROWTH)
        #endif /* STAR_PARTICLE_TYPES */
                )
                {
                    if ( particle_t[ipart]-star_tbirth[ipart] < cluster_age_spread_code ) n_active++;
                }

                ipart = particle_list_next[ipart];
            }

            /* doesn't allow cluster moves to cell that already has more than one active particles */
            // if (n_active<=1)
            if ( n_active==0 && 10*cell_H2_fraction(iter_cell)>cell_H2_fraction(icell_peak) ) /* more restrict condition */
            {
                ipart_move = NULL_PARTICLE;
                cos_vel = -1.;
		        ipart = cell_particle_list[iter_cell];
		        while ( ipart != NULL_PARTICLE ) 
                {
                    if (particle_is_star(ipart)
                #ifdef STAR_PARTICLE_TYPES
	                && (star_particle_type[ipart] == STAR_TYPE_NORMAL 
	                    || star_particle_type[ipart] == STAR_TYPE_FAST_GROWTH)
                #endif /* STAR_PARTICLE_TYPES */
                    )
                    {
                        if ( particle_t[ipart]-star_tbirth[ipart] < cluster_age_spread_code )
			            {
                            dot_prod = 0.0;
                            norm_peak = 0.0;
                            norm_part = 0.0;
                            for (i=0; i<nDim; i++)
                            {
                                dot_prod += cell_momentum(icell_peak, i)/cell_gas_density(icell_peak) * particle_v[ipart][i];
                                norm_peak += pow( cell_momentum(icell_peak, i)/cell_gas_density(icell_peak), 2 );
                                norm_part += pow( particle_v[ipart][i], 2 );
                            }
                            cos_temp = dot_prod/sqrt(norm_peak*norm_part);
                            
                            if (cos_temp>cos_vel)
                            {
                                cos_vel = cos_temp;
                                ipart_move = ipart;
                            }
                        }
                    }
                    ipart = particle_list_next[ipart];
                }


                if ( ipart_move!=NULL_PARTICLE && cos_vel>0.9 ) /* can change to 0.9 */
                {
                    cart_debug("Hui: move ipart=%d from cell %d to cell %d with cos_vel %f",
                            particle_id[ipart_move], iter_cell, icell_peak, cos_vel);
                    /*
                    if (cos_vel>1.0+1e-9)
                    {
                    cart_debug("Hui: Vpeak = %f, %f, %f; Vpart = %f, %f, %f; cos_vel=%f",
                            cell_momentum(icell_peak, 0)/cell_gas_density(icell_peak)*units->velocity/constants->kms,
                            cell_momentum(icell_peak, 1)/cell_gas_density(icell_peak)*units->velocity/constants->kms,
                            cell_momentum(icell_peak, 2)/cell_gas_density(icell_peak)*units->velocity/constants->kms,
                            particle_v[ipart_move][0]*units->velocity/constants->kms,
                            particle_v[ipart_move][1]*units->velocity/constants->kms,
                            particle_v[ipart_move][2]*units->velocity/constants->kms,
                            cos_vel
                            );
                    }
                    */
                    move_to_peak(iter_cell, icell_peak, ipart_move);
                }
            }
        }
    }

	cart_free(level_cells);

	end_time( WORK_TIMER );
}

/* move particle from icell_from to icell_to
 * put back momentum of ipart to icell_from and
 * reset velocity of ipart to the same as icell_to */
void move_to_peak(int icell_from, int icell_to, int ipart)
{
    int level_from, level_to;
    double pos[nDim];

    cart_assert( cell_is_leaf(icell_to) );

    level_from = cell_level(icell_from);
    level_to = cell_level(icell_to);
    /* put momentum of ipart back to icell_from */
    cell_momentum(icell_from,0) += particle_v[ipart][0]*particle_mass[ipart]*cell_volume_inverse[level_from];
    cell_momentum(icell_from,1) += particle_v[ipart][1]*particle_mass[ipart]*cell_volume_inverse[level_from];
    cell_momentum(icell_from,2) += particle_v[ipart][2]*particle_mass[ipart]*cell_volume_inverse[level_from];

    /* move ipart to the center of icell_to */
    cell_center_position( icell_to, pos );
    particle_x[ipart][0] = pos[0];
    particle_x[ipart][1] = pos[1];
    particle_x[ipart][2] = pos[2];

    /* split the momentum of icell_to to gas and ipart
     * so that they share the same velocity */
    particle_v[ipart][0] = cell_momentum(icell_to,0) / (cell_gas_density(icell_to)+particle_mass[ipart]*cell_volume_inverse[level_to]);
    particle_v[ipart][1] = cell_momentum(icell_to,1) / (cell_gas_density(icell_to)+particle_mass[ipart]*cell_volume_inverse[level_to]);
    particle_v[ipart][2] = cell_momentum(icell_to,2) / (cell_gas_density(icell_to)+particle_mass[ipart]*cell_volume_inverse[level_to]);

    cell_momentum(icell_to,0) -= particle_v[ipart][0]*particle_mass[ipart]*cell_volume_inverse[level_to];
    cell_momentum(icell_to,1) -= particle_v[ipart][1]*particle_mass[ipart]*cell_volume_inverse[level_to];
    cell_momentum(icell_to,2) -= particle_v[ipart][2]*particle_mass[ipart]*cell_volume_inverse[level_to];
}

#endif /* CLUSTER_FIXED */

#endif /* STAR_FORMATION && PARTICLE && CLUSTER_BOUND_FRACTION */
