#ifndef __STAR_FORMATION_STEP_H__
#define __STAR_FORMATION_STEP_H__

#ifndef CONFIGURED
#error "Missing config.h include."
#endif


#ifdef STAR_FORMATION

#ifdef HYDRO
void star_formation( int level, int time_multiplier );
#ifdef CLUSTER_DETAIL_OUTPUT
void output_clusters( int level, int time_multiplier);
#endif /* CLUSTER_DETAIL_OUTPUT */
#endif /* HYDRO */

#endif /* STAR_FORMATION */

#endif
