#include "config.h"

#include "auxiliary.h"
#include "iterators.h"
#include "load_predictor.h"
#include "particle.h"
#include "sfc.h"
#include "step.h"
#include "timing.h"
#include "tree.h"

void lp_level_step_end( int level ) {
	int i;

	cart_assert( level >= min_level && level <= max_level );

	/* sample data for the load predictor when enabled, when on the last step of the level, and
		when that level was run */
	if ( !load_predictor_flag || num_steps_on_level[level] != steps_to_take_on_level[level] ||
			local_steps_on_level[level] == 0 ) {
		step_worktimes[level-min_level] = 0;
		for ( i = 0; i < NPREDVARS; i++ ) {
			step_predvars[level*NPREDVARS + i] = 0.0;
		}
		return;
	}

	/* take average work time over number of steps */
	step_worktimes[level-min_level] = total_time_level_step(WORK_TIMER,level);

	int nlocalleaf = 0;
	int nlocalsplit = 0;

	if ( level < max_level ) {
		nlocalsplit = num_cells_per_level[level+1]/num_children;
	}
	nlocalleaf = num_cells_per_level[level] - nlocalsplit;

	step_predvars[level*NPREDVARS + PREDVAR_LEAF_CELL] = nlocalleaf;

#ifdef REFINEMENT
	step_predvars[level*NPREDVARS + PREDVAR_REFINED_CELL] = nlocalsplit;
#endif /* REFINEMENT */

#ifdef PARTICLES
	int *level_cells;
	int num_level_cells;
	int ipart = 0;
	int nparticles = 0;
	int nstarparticles = 0;

	select_level( level, CELL_TYPE_LOCAL, &num_level_cells, &level_cells );
	for (i = 0; i < num_level_cells; ++i) {
		ipart = cell_particle_list[level_cells[i]];

		while ( ipart != NULL_PARTICLE ) {
			nparticles++;
#ifdef STAR_FORMATION
			if (particle_is_star(ipart)) nstarparticles++;
#endif /* STAR_FORMATION */
			ipart = particle_list_next[ipart];
		}
	}

	cart_free( level_cells );

	step_predvars[level*NPREDVARS + PREDVAR_NONSTAR] = nparticles - nstarparticles;

#ifdef STAR_FORMATION
	step_predvars[level*NPREDVARS + PREDVAR_STAR] = nstarparticles;
#endif /* STAR_FORMATION */
#endif /* PARTICLES */
}
