#include "config.h"

#include <stdio.h>

#include "auxiliary.h"
#include "cell_buffer.h"
#include "index_hash.h"
#include "oct_hash.h"
#include "iterators.h"
#include "pack.h"
#include "parallel.h"
#include "sfc.h"
#include "timing.h"
#include "tree.h"


int buffer_enabled = 0;

/* these are allocated in lb.c during domain decomposition */
#ifdef SFC_DOMAIN_DECOMPOSITION
index_hash *buffer_root_hash = NULL;
#else
int *buffer_cell_proc_owner = NULL;
#endif

/*
//  Local buffers are local to me but buffered by someone else
//  Remote buffers are the opposite
//  (names could be reversed, Doug does not remember exactly).
//  Created during a call to build_cell_buffer, modified during refinement.
//  
//  cell_data: |local root cells|root buffer cells|...|
//                                                ^first oct
//
*/
DEFINE_LEVEL_ARRAY(int*,num_remote_buffers);
DEFINE_LEVEL_ARRAY(int**,remote_buffers);
DEFINE_LEVEL_ARRAY(int*,num_local_buffers);
DEFINE_LEVEL_ARRAY(int**,local_buffers);

DEFINE_LEVEL_ARRAY(int,num_buffer_cells);  /* number of cells we're buffering */
DEFINE_LEVEL_ARRAY(int,buffer_oct_list);   /* linked list for buffered octs */

/*******************************************************
 * init_cell_buffer
 ******************************************************/
void init_cell_buffer()
/* purpose: initializes buffer_cells array
 *          (i.e. an empty buffer)
 */
{
	int i, j;

	buffer_enabled = 0;

	for ( i = min_level; i <= max_level; i++ ) {
		num_buffer_cells[i] = 0;
		buffer_oct_list[i] = NULL_OCT;

		num_remote_buffers[i] = cart_alloc(int,num_procs);
		num_local_buffers[i] = cart_alloc(int,num_procs);
		remote_buffers[i] = cart_alloc(int*,num_procs);
		local_buffers[i] = cart_alloc(int*,num_procs);

		for ( j = 0; j < num_procs; j++ ) {
			num_remote_buffers[i][j] = 0;
			num_local_buffers[i][j] = 0;
			remote_buffers[i][j] = NULL;
			local_buffers[i][j] = NULL;
		}
	}
}

void cell_buffer_add_remote_octs( int level, int processor, int *local_octs, int num_local_octs ) {
	int i;
	int *new_buffer;

	cart_assert( level > 0 && level <= max_level );

	new_buffer = cart_alloc(int, num_remote_buffers[level][processor] + num_local_octs);

	/* add local_octs to end of array */
	for ( i = 0; i < num_remote_buffers[level][processor]; i++ ) {
		new_buffer[i] = remote_buffers[level][processor][i];
	}

	for ( i = 0; i < num_local_octs; i++ ) {
		new_buffer[ num_remote_buffers[level][processor] + i ] = local_octs[i];
	}

	num_remote_buffers[level][processor] += num_local_octs;
	cart_free( remote_buffers[level][processor] );
	remote_buffers[level][processor] = new_buffer;
}

void cell_buffer_add_local_octs( int level, int processor, int *local_octs, int num_new_octs ) {
	int i;
	int *new_buffer;

	cart_assert( level > 0 && level <= max_level );

	new_buffer = cart_alloc(int, num_local_buffers[level][processor] + num_new_octs);

	/* add at end (like remote octs) */
	for ( i = 0; i < num_local_buffers[level][processor]; i++ ) {
		new_buffer[i] = local_buffers[level][processor][i];
	}

	for ( i = 0; i < num_new_octs; i++ ) {
		new_buffer[ num_local_buffers[level][processor] + i ] = local_octs[i];
	}

	num_local_buffers[level][processor] += num_new_octs;
	cart_free( local_buffers[level][processor] );
	local_buffers[level][processor] = new_buffer;

	cart_assert( level == max_level || num_local_buffers[level][processor] >= num_local_buffers[level+1][processor] / num_children );
}

void cell_buffer_delete_local_octs( int level, int proc, int num_entries, int *entries ) {
	int i, j;
	int *new_buffer;

	cart_assert( level > min_level && level <= max_level );
	cart_assert( proc >= 0 && proc < num_procs );

	for ( i = 0; i < num_entries; i++ ) {
		cart_assert( entries[i] >= 0 && entries[i] < num_local_buffers[level][proc] );
		cart_assert( local_buffers[level][proc][entries[i]] >= 0 );
		local_buffers[level][proc][entries[i]] = -1;
	}

	new_buffer = cart_alloc(int, num_local_buffers[level][proc] - num_entries);

	j = 0;
	for ( i = 0; i < num_local_buffers[level][proc]; i++ ) {
		if ( local_buffers[level][proc][i] != -1 ) {
			new_buffer[j++] = local_buffers[level][proc][i];
		}
	}
	num_local_buffers[level][proc] -= num_entries;
	cart_assert( j == num_local_buffers[level][proc] );
	cart_free( local_buffers[level][proc] );
	local_buffers[level][proc] = new_buffer;
}

void cell_buffer_delete_remote_octs( int level, int proc, int num_entries, int *entries ) {
	int i, j;
	int *new_buffer;

	cart_assert( level > min_level && level <= max_level );
	cart_assert( proc >= 0 && proc < num_procs );

	for ( i = 0; i < num_entries; i++ ) {
		cart_assert( entries[i] >= 0 && entries[i] < num_remote_buffers[level][proc] );
		cart_assert( remote_buffers[level][proc][entries[i]] >= 0 );
		remote_buffers[level][proc][entries[i]] = -1;
	}

	new_buffer = cart_alloc(int, num_remote_buffers[level][proc] - num_entries);

	j = 0;
	for ( i = 0; i < num_remote_buffers[level][proc]; i++ ) {
		if ( remote_buffers[level][proc][i] != -1 ) {
			new_buffer[j++] = remote_buffers[level][proc][i];
		}
	}
	num_remote_buffers[level][proc] -= num_entries;
	cart_assert( j == num_remote_buffers[level][proc] );
	cart_free( remote_buffers[level][proc] );
	remote_buffers[level][proc] = new_buffer;
}

/*
//  Prunes children of the buffer cells that do not need to be buffered, because they are too far away from the border on their level.
*/
int cell_can_prune( int cell, int proc ) {
	int i;
	int neighbor;
	sfc_t sfc;
	int neighbors[num_neighbors];
	int neighbors2[num_secondary_neighbors];
	int can_prune = 1;

	cart_assert( cell >= 0 && cell < num_cells );
	cart_assert( cell_is_local(cell) );

	/* check direct neighbors */
	cell_all_neighbors( cell, neighbors );

	if ( proc == PROC_ANY ) {
		for ( i = 0; i < num_neighbors; i++ ) {
			cart_assert( neighbors[i] == NULL_OCT || cell_parent_root_sfc(neighbors[i]) >= 0 );
			if ( neighbors[i] == NULL_OCT ||
					sfc_type( cell_parent_root_sfc(neighbors[i]) ) == CELL_TYPE_BUFFER ) {
				can_prune = 0;
				break;
			}
		}

		/* check secondary neighbors */
		for ( i = 0; can_prune && i < num_secondary_neighbors; i++ ) {
			neighbors2[i] = cell_neighbor( neighbors[ secondary_neighbors[i][0] ],
					secondary_neighbors[i][1] );

			cart_assert( neighbors2[i] == NULL_OCT || cell_parent_root_sfc(neighbors2[i]) >= 0 );
			if ( neighbors2[i] == NULL_OCT ||
					sfc_type( cell_parent_root_sfc(neighbors2[i]) ) == CELL_TYPE_BUFFER ) {
				can_prune = 0;
				break;
			}
		}

		/* check tertiary neighbors */
		for ( i = 0; can_prune && i < num_tertiary_neighbors; i++ ) {
			neighbor = cell_neighbor( neighbors2[ tertiary_neighbors[i][0] ],
					tertiary_neighbors[i][1] );

			cart_assert( neighbor == NULL_OCT || cell_parent_root_sfc(neighbor) >= 0 );
			if ( neighbor == NULL_OCT ||
					sfc_type( cell_parent_root_sfc(neighbor) ) == CELL_TYPE_BUFFER ) {
				can_prune = 0;
				break;
			}
		}
	} else {
		/* check primary neighbors */
		for ( i = 0; i < num_neighbors; i++ ) {
			if ( neighbors[i] == NULL_OCT ) {
				can_prune = 0;
				break;
			} else {
				sfc = cell_parent_root_sfc( neighbors[i] );
				cart_assert( sfc >= 0 && sfc < max_sfc_index );

				if ( sfc_processor_owner(sfc) == proc ||
						sfc_type(sfc) == CELL_TYPE_BUFFER ) {
					can_prune = 0;
					break;
				}
			}
		}

		/* check secondary neighbors */
		for ( i = 0; can_prune && i < num_secondary_neighbors; i++ ) {
			neighbors2[i] = cell_neighbor( neighbors[ secondary_neighbors[i][0] ],
					secondary_neighbors[i][1] );

			if ( neighbors2[i] == NULL_OCT ) {
				can_prune = 0;
				break;
			} else {
				sfc = cell_parent_root_sfc( neighbors2[i] );
				cart_assert( sfc >= 0 && sfc < max_sfc_index );

				if ( sfc_processor_owner(sfc) == proc ||
						sfc_type(sfc) == CELL_TYPE_BUFFER ) {
					can_prune = 0;
					break;
				}
			}
		}

		/* check tertiary neighbors */
		for ( i = 0; can_prune && i < num_tertiary_neighbors; i++ ) {
			neighbor = cell_neighbor( neighbors2[ tertiary_neighbors[i][0] ],
					tertiary_neighbors[i][1] );

			if ( neighbor == NULL_OCT ) {
				can_prune = 0;
				break;
			} else {
				sfc = cell_parent_root_sfc( neighbor );
				cart_assert( sfc >= 0 && sfc < max_sfc_index );

				if ( sfc_processor_owner(sfc) == proc ||
						sfc_type(sfc) == CELL_TYPE_BUFFER ) {
					can_prune = 0;
					break;
				}
			}
		}
	}

	return can_prune;
}

/*******************************************************
 * build_cell_buffer
 *******************************************************/
void build_cell_buffer()
/* purpose: builds a list of the cells the local processor needs
 *          to have a complete buffer, then all processors communicate
 *          so each processor has a local copy of the needed cells.
 */
{
	int i, j;
	int icell;
	sfc_t sfc;
	int proc, level;
	int processor;
	sfc_t neighbors[num_stencil];
	int count[max_level-min_level+1];
	int ioct;

	pack *buffer_list;

	cart_assert( root_cells_enabled );
	cart_assert( !buffer_enabled );
	cart_debug("building cell buffer");

	/* re-initialize buffer array for levels > 0 */
	for ( i = min_level+1; i <= max_level; i++ ) {
		num_buffer_cells[i] = 0;
		buffer_oct_list[i] = NULL_OCT;
	}

	/* initialize lists */
	for ( i = min_level; i <= max_level; i++ ) {
		for ( j = 0; j < num_procs; j++ ) {
			num_remote_buffers[i][j] = 0;
			num_local_buffers[i][j] = 0;
			remote_buffers[i][j] = NULL;
			local_buffers[i][j] = NULL;
		}
	}

	if ( num_procs == 1 ) {
		buffer_enabled = 1;
		repair_neighbors();
		cart_debug("buffer enabled");
		return;
	}

	start_time( BUILD_CELL_BUFFER_TIMER );

	/* create pack to hold buffer trees */
	buffer_list = pack_init( CELL_TYPE_BUFFER );

	/* In lb.c we have constructed the list of SFC we buffer (and assigned them an index)
	 * Unfortunately we need the opposite information (which of our local root trees are
	 * buffered elsewhere). Communicate those lists to other ranks rather than recomputing,
	 * as sfc_processor_owner is expensive now.
	 *
	 * Note: these lists can be int not sfc_t because we still use integer for per-rank
	 * array indices.
	 */
	int *send_sfc_count = cart_alloc(int, num_procs);
	int *recv_sfc_count = cart_alloc(int, num_procs);
	int *send_sfc_offset = cart_alloc(int, num_procs);
	int *recv_sfc_offset = cart_alloc(int, num_procs);
	sfc_t *send_sfc_list = cart_alloc(sfc_t, num_buffer_cells[min_level]);

	for ( proc = 0; proc < num_procs; proc++ ) {
		send_sfc_count[proc] = 0;
	}

	for ( icell = num_cells_per_level[min_level]; icell < num_cells_per_level[min_level]+num_buffer_cells[min_level]; icell++ ) {
#ifdef SFC_DOMAIN_DECOMPOSITION
		proc = sfc_processor_owner(root_cell_sfc_index[icell]);
#else
		/* here we use the array directly, to avoid index_hash_lookup call */
		proc = buffer_cell_proc_owner[icell-num_cells_per_level[min_level]];
#endif
		send_sfc_count[proc]++;
	}

	send_sfc_offset[0] = 0;
	for ( proc = 1; proc < num_procs; proc++ ) {
		send_sfc_offset[proc] = send_sfc_offset[proc-1]+send_sfc_count[proc-1];
	}

	for ( icell = num_cells_per_level[min_level]; icell < num_cells_per_level[min_level]+num_buffer_cells[min_level]; icell++ ) {
#ifdef SFC_DOMAIN_DECOMPOSITION
		/* sfc_processor_owner is log num_procs so we can use it here */
		proc = sfc_processor_owner(root_cell_sfc_index[icell]);
#else
		/* here we use the array directly, to avoid index_hash_lookup call */
		proc = buffer_cell_proc_owner[icell-num_cells_per_level[min_level]];
#endif
		send_sfc_list[ send_sfc_offset[proc]++ ] = root_cell_sfc_index[icell];
	}

	send_sfc_offset[0] = 0;
	for ( proc = 1; proc < num_procs; proc++ ) {
		send_sfc_offset[proc] = send_sfc_offset[proc-1]+send_sfc_count[proc-1];
	}

	/* let each rank know how many to expect */
	MPI_Alltoall( send_sfc_count, 1, MPI_INT,
			recv_sfc_count, 1, MPI_INT,
			mpi.comm.run );

	recv_sfc_offset[0] = 0;
	for ( proc = 1; proc < num_procs; proc++ ) {
		recv_sfc_offset[proc] = recv_sfc_offset[proc-1]+recv_sfc_count[proc-1];
	}

	int num_recv_sfc = recv_sfc_offset[num_procs-1]+recv_sfc_count[proc-1];
	sfc_t *recv_sfc_list = cart_alloc(sfc_t, num_recv_sfc);

	MPI_Alltoallv( send_sfc_list, send_sfc_count, send_sfc_offset, MPI_SFC,
			recv_sfc_list, recv_sfc_count, recv_sfc_offset, MPI_SFC, mpi.comm.run );

	cart_free( send_sfc_list );
	cart_free( send_sfc_offset );
	cart_free( send_sfc_count );

	for ( proc = 0; proc < num_procs; proc++ ) {
		for ( i = recv_sfc_offset[proc]; i < recv_sfc_offset[proc]+recv_sfc_count[proc]; i++ ) {
			pack_add_root_tree(buffer_list, proc, recv_sfc_list[i]);
		}
	}

	cart_free( recv_sfc_list );
	cart_free( recv_sfc_count );
	cart_free( recv_sfc_offset );

	pack_apply( buffer_list );

	/* process received buffer cells */
	for ( proc = 0; proc < num_procs; proc++ ) {
		num_local_buffers[min_level][proc] = buffer_list->num_receiving_cells[proc][min_level];
		num_remote_buffers[min_level][proc] = buffer_list->num_sending_cells[proc][min_level];

		for ( level = min_level+1; level <= max_level; level++ ) {
			num_remote_buffers[level][proc] = buffer_list->num_sending_cells[proc][level] / num_children;
			num_local_buffers[level][proc] = buffer_list->num_receiving_cells[proc][level] / num_children;
		}

		for ( level = min_level; level <= max_level; level++ ) {
			cart_assert( num_remote_buffers[level][proc] >= 0 );
			remote_buffers[level][proc] = cart_alloc(int, num_remote_buffers[level][proc] );
			count[level] = 0;

			local_buffers[level][proc] = cart_alloc(int, num_local_buffers[level][proc] );
		}

		for ( i = 0; i < buffer_list->num_sending_cells[proc][min_level]; i++ ) {
			remote_buffers[min_level][proc][i] = sfc_cell_location( buffer_list->root_cells[proc][i] );
			cart_assert( remote_buffers[min_level][proc][i] >= 0 &&
					remote_buffers[min_level][proc][i] < num_cells_per_level[min_level] );
		}

		/* pack remote octs */
		for ( i = 0; i < buffer_list->num_sending_cells_total[proc]; i++ ) {
			ioct = buffer_list->cell_refined[proc][i];
			cart_assert( ioct == NULL_OCT || ( ioct >= 0 && ioct < num_octs ) );

			if ( ioct != NULL_OCT ) {
				level = oct_level[ioct];
				remote_buffers[level][proc][count[level]] = ioct;
				count[level]++;
			}
		}
	}

	pack_communicate( buffer_list );

	pack_destroy( buffer_list );

	buffer_enabled = 1;

	repair_neighbors();

	end_time( BUILD_CELL_BUFFER_TIMER );

	cart_debug("buffer enabled");
}

/*******************************************************
 * destroy_cell_buffer
 *******************************************************/
void destroy_cell_buffer()
/* purpose: deallocates all root cell space taken up by
 *          cell buffer and update lists
 */
{
	int num_level_cells;
	int *level_cells;
	int i, j;

	if ( num_procs == 1 ) {
		cart_assert( num_buffer_cells[min_level] == 0 );
		buffer_enabled = 0;
		return;
	}

	/* free all buffer trees */
	select_level( min_level, CELL_TYPE_BUFFER, &num_level_cells, &level_cells );
	for ( i = 0; i < num_level_cells; i++ ) {
		cell_free( level_cells[i] );
	}
	cart_free( level_cells );

	/* clear linked lists */
	for ( i = min_level; i <= max_level; i++ ) {
		num_buffer_cells[i] = 0;
		buffer_oct_list[i] = NULL_OCT;

		for ( j = 0; j < num_procs; j++ ) {
			cart_free( remote_buffers[i][j] );
			cart_free( local_buffers[i][j] );
		}
	}

	buffer_enabled = 0;

	cart_debug("cell buffer disabled");
}


void test_update_buffer(int level, int const* g2l, int tot)
{
  int proc, i, j, index, icell;

  for ( proc = 0; proc < num_procs; proc++ ) {

    if ( level == min_level )
    {
      for ( i = 0; i < num_remote_buffers[min_level][proc]; i++ )
      {
        index = remote_buffers[min_level][proc][i];
        if( g2l[index] == -1 || index > tot)  { printf("BADBAD!\n"); exit(0); }
      }

      for ( i = 0; i < num_local_buffers[min_level][proc]; i++ )
      {
        index = local_buffers[min_level][proc][i];
        if( g2l[index] == -1 || index > tot)  { printf("BADBAD!\n"); exit(0); }
      }
    }
    else
    {
      for ( i = 0; i < num_remote_buffers[level][proc]; i++ )
      {
        for ( j = 0; j < num_children; j++ )
        {
          icell = oct_child( remote_buffers[level][proc][i], j );
          if( g2l[icell] == -1 || icell > tot)  { printf("BADBAD!\n"); exit(0); }
        }
      }

      for ( i = 0; i < num_local_buffers[level][proc]; i++ )
      {
        for ( j = 0; j < num_children; j++ )
        {
          index = local_buffers[level][proc][i];
          icell = oct_child( index, j );
          if( g2l[icell] == -1 || icell > tot)  { printf("BADBAD!\n"); exit(0); }
        }
      }
    }
  }
}

/*******************************************************
 * update_buffer_level
 *******************************************************/
void update_buffer_level( int level, const int *var_indices, const int num_update_vars )
/* purpose: updates all remotely buffered cells of the given
 *          level and receives updates for all locally buffered
 *          cells
 */
{
	cell_buffer_scatter(level, var_indices, num_update_vars);
}

/*******************************************************
 * cell_buffer_scatter
 *******************************************************/
void cell_buffer_scatter( int level, const int *var_indices, const int num_update_vars )
/* purpose: updates all remotely buffered cells of the given
 *          level and receives updates for all locally buffered
 *          cells
 */
{
	int i, j, k;
	int var_counter;
	int index;
	int icell;
	int proc;
	int buffer_size, buffer_ptr, recv_count;

	MPI_Request requests[MAX_PROCS];
	MPI_Request receives[MAX_PROCS];

	float *buffer;
	int recv_offset[MAX_PROCS];

	if ( num_procs == 1 ) {
		return;
	}

	start_time( COMMUNICATION_TIMER );
	start_time( UPDATE_TIMER );

	cart_assert( num_update_vars > 0 && num_update_vars <= num_vars );
	cart_assert( level >= min_level && level <= max_level );

	/* allocate buffer */
	buffer_size = 0;
	for ( proc = 0; proc < num_procs; proc++ ) {
		cart_assert( num_local_buffers[level][proc] >= 0 );
		cart_assert( num_remote_buffers[level][proc] >= 0 );

		if ( level == min_level ) {
			buffer_size += num_update_vars *
				( num_local_buffers[min_level][proc] + num_remote_buffers[min_level][proc] );
		} else {
			buffer_size += num_update_vars * num_children *
				( num_local_buffers[level][proc] + num_remote_buffers[level][proc] );
		}
	}

	buffer = cart_alloc(float, buffer_size );

	/* set up receives */
	buffer_ptr = 0;
	for ( proc = 0; proc < num_procs; proc++ ) {
		if ( num_local_buffers[level][proc] > 0 ) {
			recv_offset[proc] = buffer_ptr;

			if ( level == min_level ) {
				recv_count = num_update_vars * num_local_buffers[min_level][proc];
			} else {
				recv_count = num_update_vars * num_children * num_local_buffers[level][proc];
			}

			MPI_Irecv( &buffer[buffer_ptr], recv_count, MPI_FLOAT, proc, num_update_vars,
				mpi.comm.run, &receives[proc] );

			buffer_ptr += recv_count;
		} else {
			receives[proc] = MPI_REQUEST_NULL;
		}
	}

	/* set up sends */
	for ( proc = 0; proc < num_procs; proc++ ) {
		if ( num_remote_buffers[level][proc] > 0 ) {
			var_counter = buffer_ptr;

			if ( level == min_level ) {
				for ( i = 0; i < num_remote_buffers[min_level][proc]; i++ ) {
					index = remote_buffers[min_level][proc][i];
					for ( j = 0; j < num_update_vars; j++ ) {
						buffer[var_counter++] = cell_var( index, var_indices[j] );
					}
				}

				MPI_Isend( &buffer[buffer_ptr], num_remote_buffers[min_level][proc] * num_update_vars,
					MPI_FLOAT, proc, num_update_vars, mpi.comm.run, &requests[proc] );
			} else {
				for ( i = 0; i < num_remote_buffers[level][proc]; i++ ) {
					for ( j = 0; j < num_children; j++ ) {
						icell = oct_child( remote_buffers[level][proc][i], j );
						for ( k = 0; k < num_update_vars; k++ ) {
							buffer[var_counter++] = cell_var( icell, var_indices[k] );
						}
					}
				}

				MPI_Isend( &buffer[buffer_ptr], num_remote_buffers[level][proc] * num_update_vars * num_children,
					MPI_FLOAT, proc, num_update_vars, mpi.comm.run, &requests[proc] );
			}

			buffer_ptr = var_counter;
		} else {
			requests[proc] = MPI_REQUEST_NULL;
		}
	}

	cart_assert( buffer_size == buffer_ptr );

	start_time( UPDATE_RECV_TIMER );

	do {
		MPI_Waitany( num_procs, receives, &proc, MPI_STATUS_IGNORE );

		if ( proc != MPI_UNDEFINED ) {
			var_counter = recv_offset[proc];
			if ( level == min_level ) {
				for ( i = 0; i < num_local_buffers[min_level][proc]; i++ ) {
					index = local_buffers[min_level][proc][i];

					for ( j = 0; j < num_update_vars; j++ ) {
						cell_var( index, var_indices[j] ) = buffer[var_counter++];
					}
				}
			} else {
				for ( i = 0; i < num_local_buffers[level][proc]; i++ ) {
					index = local_buffers[level][proc][i];

					for ( j = 0; j < num_children; j++ ) {
						icell = oct_child( index, j );
						for ( k = 0; k < num_update_vars; k++ ) {
							cell_var( icell, var_indices[k] ) = buffer[var_counter++];
						}
					}
				}
			}
		}
	} while ( proc != MPI_UNDEFINED );

	end_time( UPDATE_RECV_TIMER );

	/* wait for all sends to complete (later add latency hiding, delay freeing buffers?) */
	start_time( UPDATE_SEND_TIMER );
	MPI_Waitall( num_procs, requests, MPI_STATUSES_IGNORE );
	end_time( UPDATE_SEND_TIMER );

	cart_free(buffer);

	end_time( UPDATE_TIMER );
	end_time( COMMUNICATION_TIMER );
}

/*******************************************************
 * cell_buffer_gather
 *******************************************************/
void cell_buffer_gather( int level, const int *var_indices, const int num_gather_vars )
/* purpose: gathers variables from all remotely buffered cells and merges (adds)
 *          their values
 */
{
	int i, j, k;
	int var_counter;
	int index;
	int icell;
	int proc;
	int buffer_size, buffer_ptr, recv_count;

	MPI_Request requests[MAX_PROCS];
	MPI_Request receives[MAX_PROCS];

	float *buffer;
	int recv_offset[MAX_PROCS];

	if ( num_procs == 1 ) {
		return;
	}

	start_time( COMMUNICATION_TIMER );
	start_time( UPDATE_TIMER );

	cart_assert( num_gather_vars > 0 && num_gather_vars <= num_vars );
	cart_assert( level >= min_level && level <= max_level );

	/* allocate buffer */
	buffer_size = 0;
	for ( proc = 0; proc < num_procs; proc++ ) {
		if ( level == min_level ) {
			buffer_size += num_gather_vars *
				( num_local_buffers[min_level][proc] + num_remote_buffers[min_level][proc] );
		} else {
			buffer_size += num_gather_vars * num_children *
				( num_local_buffers[level][proc] + num_remote_buffers[level][proc] );
		}
	}

	buffer = cart_alloc(float, buffer_size);

	/* set up receives */
	buffer_ptr = 0;
	for ( proc = 0; proc < num_procs; proc++ ) {
		if ( num_remote_buffers[level][proc] > 0 ) {
			recv_offset[proc] = buffer_ptr;

			if ( level == min_level ) {
				recv_count = num_gather_vars * num_remote_buffers[min_level][proc];
			} else {
				recv_count = num_gather_vars * num_children * num_remote_buffers[level][proc];
			}

			MPI_Irecv( &buffer[buffer_ptr], recv_count, MPI_FLOAT, proc, num_gather_vars,
				mpi.comm.run, &receives[proc] );

			buffer_ptr += recv_count;
		} else {
			receives[proc] = MPI_REQUEST_NULL;
		}
	}

	/* set up sends */
	for ( proc = 0; proc < num_procs; proc++ ) {
		if ( num_local_buffers[level][proc] > 0 ) {
			var_counter = buffer_ptr;

			if ( level == min_level ) {
				for ( i = 0; i < num_local_buffers[min_level][proc]; i++ ) {
					index = local_buffers[min_level][proc][i];
					for ( j = 0; j < num_gather_vars; j++ ) {
						buffer[var_counter++] = cell_var( index, var_indices[j] );
					}
				}

				MPI_Isend( &buffer[buffer_ptr], num_local_buffers[min_level][proc] * num_gather_vars,
					MPI_FLOAT, proc, num_gather_vars, mpi.comm.run, &requests[proc] );
			} else {
				for ( i = 0; i < num_local_buffers[level][proc]; i++ ) {
					for ( j = 0; j < num_children; j++ ) {
						icell = oct_child( local_buffers[level][proc][i], j );
						for ( k = 0; k < num_gather_vars; k++ ) {
							buffer[var_counter++] = cell_var( icell, var_indices[k] );
						}
					}
				}

				MPI_Isend( &buffer[buffer_ptr], num_local_buffers[level][proc] * num_gather_vars * num_children,
					MPI_FLOAT, proc, num_gather_vars, mpi.comm.run, &requests[proc] );
			}

			buffer_ptr = var_counter;
		} else {
			requests[proc] = MPI_REQUEST_NULL;
		}
	}

	cart_assert( buffer_size == buffer_ptr );

	start_time( UPDATE_RECV_TIMER );

	do {
		MPI_Waitany( num_procs, receives, &proc, MPI_STATUS_IGNORE );

		if ( proc != MPI_UNDEFINED ) {
			var_counter = recv_offset[proc];
			if ( level == min_level ) {
				for ( i = 0; i < num_remote_buffers[min_level][proc]; i++ ) {
					index = remote_buffers[min_level][proc][i];

					for ( j = 0; j < num_gather_vars; j++ ) {
						cell_var( index, var_indices[j] ) += buffer[var_counter++];
					}
				}
			} else {
				for ( i = 0; i < num_remote_buffers[level][proc]; i++ ) {
					index = remote_buffers[level][proc][i];
					for ( j = 0; j < num_children; j++ ) {
						icell = oct_child( index, j );
						for ( k = 0; k < num_gather_vars; k++ ) {
							cell_var( icell, var_indices[k] ) += buffer[var_counter++];
						}
					}
				}
			}
		}
	} while ( proc != MPI_UNDEFINED );

	end_time( UPDATE_RECV_TIMER );

	/* wait for all sends to complete (later add latency hiding, delay freeing buffers?) */
	start_time( UPDATE_SEND_TIMER );
	MPI_Waitall( num_procs, requests, MPI_STATUSES_IGNORE );
	end_time( UPDATE_SEND_TIMER );

	cart_free(buffer);

	end_time( UPDATE_TIMER );
	end_time( COMMUNICATION_TIMER );
}

/*******************************************************
 * update_cached_buffer_level
 *******************************************************/
void update_cached_buffer_level( int level, int var, float *cvar, const int * g2l, int tot )
/* purpose: updates cached array representing buffered cells
 */
{
	int i, j;
	int var_counter;
	int icell;
	int ioct;
	int proc;
	int buffer_size, buffer_ptr, recv_count;

	MPI_Request requests[MAX_PROCS];
	MPI_Request receives[MAX_PROCS];
	MPI_Status statuses[MAX_PROCS];
	MPI_Status status;

	float *buffer;
	int recv_offset[MAX_PROCS];

	if ( num_procs == 1 ) {
		return;
	}

	start_time( COMMUNICATION_TIMER );
	start_time( UPDATE_TIMER );

	cart_assert( level >= min_level && level <= max_level );

	/* allocate buffer */
	buffer_size = 0;
	for ( proc = 0; proc < num_procs; proc++ ) {
		cart_assert( num_local_buffers[level][proc] >= 0 );
		cart_assert( num_remote_buffers[level][proc] >= 0 );

		if ( level == min_level ) {
			buffer_size += num_local_buffers[min_level][proc]
				+ num_remote_buffers[min_level][proc] ;
		} else {
			buffer_size += num_children *
				( num_local_buffers[level][proc]
				  + num_remote_buffers[level][proc] );
		}
	}

	buffer = cart_alloc(float, buffer_size );

	/* set up receives */
	buffer_ptr = 0;
	for ( proc = 0; proc < num_procs; proc++ ) {
		if ( num_local_buffers[level][proc] > 0 ) {
			recv_offset[proc] = buffer_ptr;

			if ( level == min_level ) {
				recv_count = num_local_buffers[min_level][proc];
			} else {
				recv_count = num_children * num_local_buffers[level][proc];
			}

			MPI_Irecv( &buffer[buffer_ptr], recv_count, MPI_FLOAT, proc, 1,
				mpi.comm.run, &receives[proc] );

			buffer_ptr += recv_count;
		} else {
			receives[proc] = MPI_REQUEST_NULL;
		}
	}

	/* set up sends */
	for ( proc = 0; proc < num_procs; proc++ ) {
		if ( num_remote_buffers[level][proc] > 0 ) {
			var_counter = buffer_ptr;

			if ( level == min_level ) {
				for ( i = 0; i < num_remote_buffers[min_level][proc]; i++ ) {
					icell = remote_buffers[min_level][proc][i];
					if(icell>=tot || g2l[icell]==-1)  buffer[var_counter++] =  cell_var( icell, var );
					else buffer[var_counter++] = cvar[g2l[icell]];
				}

				MPI_Isend( &buffer[buffer_ptr], num_remote_buffers[min_level][proc],
					MPI_FLOAT, proc, 1, mpi.comm.run, &requests[proc] );
			} else {
				for ( i = 0; i < num_remote_buffers[level][proc]; i++ ) {
					for ( j = 0; j < num_children; j++ ) {
						icell = oct_child( remote_buffers[level][proc][i], j );
						if(icell>=tot || g2l[icell]==-1)  buffer[var_counter++] = cell_var( icell, var );
						else buffer[var_counter++] = cvar[g2l[icell]];
					}
				}

				MPI_Isend( &buffer[buffer_ptr], num_remote_buffers[level][proc] * num_children,
					MPI_FLOAT, proc, 1, mpi.comm.run, &requests[proc] );
			}

			buffer_ptr = var_counter;
		} else {
			requests[proc] = MPI_REQUEST_NULL;
		}
	}

	cart_assert( buffer_size == buffer_ptr );

	start_time( UPDATE_RECV_TIMER );

	do {
		MPI_Waitany( num_procs, receives, &proc, &status );

		if ( proc != MPI_UNDEFINED ) {
			var_counter = recv_offset[proc];
			if ( level == min_level ) {
				for ( i = 0; i < num_local_buffers[min_level][proc]; i++ ) {
					icell = local_buffers[min_level][proc][i];
					if(icell>=tot || g2l[icell]==-1)  ++var_counter;
					else cvar[g2l[icell]] = buffer[var_counter++];
				}
			} else {
				for ( i = 0; i < num_local_buffers[level][proc]; i++ ) {
					ioct = local_buffers[level][proc][i];
					cart_assert( ioct >= 0 && ioct < num_octs );

					for ( j = 0; j < num_children; j++ ) {
						icell = oct_child( ioct, j );
						if(icell>=tot || g2l[icell]==-1)  ++var_counter;
						else cvar[g2l[icell]] = buffer[var_counter++];
					}
				}
			}
		}
	} while ( proc != MPI_UNDEFINED );

	end_time( UPDATE_RECV_TIMER );

	start_time( UPDATE_SEND_TIMER );
	MPI_Waitall( num_procs, requests, statuses );
	end_time( UPDATE_SEND_TIMER );

	cart_free(buffer);

	end_time( UPDATE_TIMER );
	end_time( COMMUNICATION_TIMER );
}

int *remote_buffers_level_proc;
int compare_remote_buffers( const void *a, const void *b ) {
	return ( remote_buffers_level_proc[*(int *)a] - remote_buffers_level_proc[*(int *)b] );
}

/*
//  split/join does all communication of buffer changes during refinement.
//  cells to split are local cells that this rank splits in the current step.
//  Called during the refinement process.
*/
void split_buffer_cells( int level, int *cells_to_split, int num_cells_to_split ) {
	int i, j, k, m;
	int proc;
	int child, ichild;
	int icell;
	int index;
	int result;
	int *buffer_cells_to_split[MAX_PROCS];
	float *new_buffer_cell_vars;
	int *cells_split[MAX_PROCS];
	float *new_cell_vars[MAX_PROCS];
	int *local_octs;
	int *new_octs;
	int *order;
	int num_cells_to_send;
	int num_cells_to_recv;
	int num_cells_split;
	int num_cell_vars;

	int num_sends = 0;
	MPI_Status status;
	MPI_Request receives[MAX_PROCS];
	MPI_Request sends[2*MAX_PROCS];

	cart_assert( level < max_level );

	start_time( SPLIT_BUFFER_TIMER );
	start_time( COMMUNICATION_TIMER );

	/* set up receive buffers */
	for ( proc = 0; proc < num_procs; proc++ ) {
		if ( num_local_buffers[level][proc] > 0 ) {
			/* cells already split cannot be split, so subtract # of level+1 octs */
			if ( level == min_level ) {
				num_cells_to_recv = num_local_buffers[level][proc] - num_local_buffers[level+1][proc];
			} else {
				num_cells_to_recv = num_children*num_local_buffers[level][proc] - num_local_buffers[level+1][proc];
			}
			cart_assert( num_cells_to_recv >= 0 );

			buffer_cells_to_split[proc] = cart_alloc(int, num_cells_to_recv);

			MPI_Irecv( buffer_cells_to_split[proc], num_cells_to_recv, MPI_INT,
					proc, 0, mpi.comm.run, &receives[proc] );
		} else {
			receives[proc] = MPI_REQUEST_NULL;
		}
	}

	/* generate list of cells to send to each proc */

	/* prune split cells to those which may be buffered by any other rank */
	for ( i = 0; i < num_cells_to_split; i++ ) {
		if ( cells_to_split[i] != NULL_OCT && cell_can_prune( cells_to_split[i], PROC_ANY ) ) {
			cells_to_split[i] = -1;
		}
	}

	/* move -1's to the end */
	qsort( cells_to_split, num_cells_to_split, sizeof(int), compare_ints );

	/* now how many do we have left? */
	for ( num_cells_split = 0; num_cells_split < num_cells_to_split; num_cells_split++ ) {
		if ( cells_to_split[num_cells_split] == -1 ) {
			break;
		}
	}

	/* count number split by each proc */
	for ( proc = 0; proc < num_procs; proc++ ) {
		if ( num_remote_buffers[level][proc] > 0 ) {
			if ( level == min_level ) {
				num_cells_to_send = MIN(num_cells_split, num_remote_buffers[level][proc] - num_remote_buffers[level+1][proc]);
			} else {
				num_cells_to_send = MIN(num_cells_split, num_children*num_remote_buffers[level][proc] - num_remote_buffers[level+1][proc]);
			}

			new_octs = cart_alloc(int, num_cells_to_send);
			cells_split[proc] = cart_alloc(int, num_cells_to_send);
			num_cells_to_send = 0;

			order = cart_alloc(int, num_remote_buffers[level][proc] );
			for ( i = 0; i < num_remote_buffers[level][proc]; i++ ) {
				order[i] = i;
			}

			remote_buffers_level_proc = remote_buffers[level][proc];
			qsort( order, num_remote_buffers[level][proc], sizeof(int), compare_remote_buffers );

			/* loop through split cells and count those buffered by proc
			 * assumes remote_buffers and cells_to_split are both sorted */
			k = 0;
			for ( j = 0; j < num_cells_split; j++ ) {
				if ( level == min_level ) {
					index = cells_to_split[j];
					cart_assert( cell_is_root_cell(index) );
				} else {
					index = cell_parent_oct(cells_to_split[j]);
				}

				while ( k < num_remote_buffers[level][proc] &&
						index > remote_buffers[level][proc][order[k]] ) {
					k++;
				}

				if ( k < num_remote_buffers[level][proc] &&
						index == remote_buffers[level][proc][order[k]] &&
						!cell_can_prune(cells_to_split[j], proc) ) {
					new_octs[num_cells_to_send] = cell_child_oct[cells_to_split[j]];
					cells_split[proc][num_cells_to_send] = (level==min_level) ? order[k]: num_children*order[k]+cell_child_number(cells_to_split[j]);
					num_cells_to_send++;
				}
			}

			cart_free(order);

			/* now allocate space to store these cells */
			new_cell_vars[proc] = cart_alloc(float, num_vars*num_children*num_cells_to_send );
			num_cell_vars = 0;

			for ( j = 0; j < num_cells_to_send; j++ ) {
				/* pack each new child */
				for ( child = 0; child < num_children; child++ ) {
					ichild = oct_child(new_octs[j], child);
					for ( m = 0; m < num_vars; m++ ) {
						new_cell_vars[proc][num_cell_vars++] = cell_var(ichild,m);
					}
				}
			}

			cart_assert( num_cell_vars == num_vars*num_children*num_cells_to_send );

			/* send split cell arrays to other proc */
			MPI_Isend( cells_split[proc], num_cells_to_send, MPI_INT,
				proc, 0, mpi.comm.run, &sends[num_sends++] );
			MPI_Isend( new_cell_vars[proc], num_cell_vars, MPI_FLOAT, proc, 0,
				mpi.comm.run, &sends[num_sends++] );

			/* add all newly buffered octs to remote_buffers */
			cell_buffer_add_remote_octs( level+1, proc, new_octs, num_cells_to_send );
			cart_free( new_octs );
		}
	}

	/* now wait to receive from each proc */
	do {
		start_time( SPLIT_BUFFER_COMMUNICATION_TIMER );
		MPI_Waitany( num_procs, receives, &proc, &status );
		end_time( SPLIT_BUFFER_COMMUNICATION_TIMER );

		if ( proc != MPI_UNDEFINED ) {
			MPI_Get_count( &status, MPI_INT, &num_cells_to_recv );
			new_buffer_cell_vars = cart_alloc(float, num_children*num_vars*num_cells_to_recv );

			MPI_Recv( new_buffer_cell_vars, num_children*num_vars*num_cells_to_recv,
				MPI_FLOAT, proc, 0, mpi.comm.run, MPI_STATUS_IGNORE );

			num_cell_vars = 0;

			local_octs = cart_alloc(int, num_cells_to_recv);

			for ( j = 0; j < num_cells_to_recv; j++ ) {
				index = buffer_cells_to_split[proc][j];
				if ( level == min_level ) {
					cart_assert( index >= 0 && index < num_local_buffers[level][proc] );
					icell = local_buffers[level][proc][index];
				} else {
					/* index contains num_children*[index in local_buffers array] + cell_child */
					cart_assert( index/num_children >= 0 && index/num_children < num_local_buffers[level][proc] );
					icell = oct_child(local_buffers[level][proc][index/num_children], index % num_children);
				}

				cart_assert( icell >= 0 && icell < num_cells );
				cart_assert( !cell_is_local(icell) );
				cart_assert( cell_is_leaf(icell) );

				/* split cell */
				result = split_cell(icell);
				if ( result ) {
					cart_error("Error splitting buffer cell %d", icell);
				}

				/* copy over cell vars */
				for ( child = 0; child < num_children; child++ ) {
					ichild = cell_child(icell,child);
					for ( m = 0; m < num_vars; m++ ) {
						cell_var(ichild,m) = new_buffer_cell_vars[num_cell_vars++];
					}
				}

				local_octs[j] = cell_child_oct[icell];
			}

			cell_buffer_add_local_octs( level+1, proc, local_octs, num_cells_to_recv );

			cart_free( local_octs );

			cart_free( buffer_cells_to_split[proc] );
			cart_free( new_buffer_cell_vars );
		}
	} while ( proc != MPI_UNDEFINED );

	/* wait for all sends to complete */
	start_time( SPLIT_BUFFER_COMMUNICATION_TIMER );
	MPI_Waitall( num_sends, sends, MPI_STATUSES_IGNORE );
	end_time( SPLIT_BUFFER_COMMUNICATION_TIMER );

	/* free all send buffers */
	for ( proc = 0; proc < num_procs; proc++ ) {
		if (num_remote_buffers[level][proc] > 0) {
			cart_free( cells_split[proc] );
			cart_free( new_cell_vars[proc] );
		}
	}

	end_time( COMMUNICATION_TIMER );
	end_time( SPLIT_BUFFER_TIMER );
}

void join_buffer_cells( int level, int *octs_to_join, int num_octs_to_join ) {
	int i, j, k;
	int proc;
	int result;
	int *buffer_octs_to_join[MAX_PROCS];
	int *octs_joined[MAX_PROCS];
	int num_octs_joined;
	int num_octs_to_send;
	int oct_index, cell_index;
	int *order;
	MPI_Status status;
	MPI_Request receives[MAX_PROCS];
	MPI_Request sends[MAX_PROCS];

	start_time( JOIN_BUFFER_TIMER );
	start_time( COMMUNICATION_TIMER );

	/* set up receives */
	for ( proc = 0; proc < num_procs; proc++ ) {
		if ( num_local_buffers[level+1][proc] > 0 ) {
			buffer_octs_to_join[proc] = cart_alloc(int, num_local_buffers[level+1][proc]);
			MPI_Irecv( buffer_octs_to_join[proc], num_local_buffers[level+1][proc],
					MPI_INT, proc, 0, mpi.comm.run, &receives[proc] );
		} else {
			receives[proc] = MPI_REQUEST_NULL;
		}
	}

	qsort( octs_to_join, num_octs_to_join, sizeof(int), compare_ints );

	for ( proc = 0; proc < num_procs; proc++ ) {
		if ( num_remote_buffers[level+1][proc] > 0 ) {
			octs_joined[proc] = cart_alloc(int, MIN(num_octs_to_join, num_remote_buffers[level+1][proc]) );
			num_octs_to_send = 0;

			order = cart_alloc(int, num_remote_buffers[level+1][proc] );
			for ( i = 0; i < num_remote_buffers[level+1][proc]; i++ ) {
				order[i] = i;
			}

			remote_buffers_level_proc = remote_buffers[level+1][proc];
			qsort( order, num_remote_buffers[level+1][proc], sizeof(int), compare_remote_buffers );

			/* build list of joined octs buffered by this processor  */
			k = 0;
			for ( j = 0; j < num_octs_to_join; j++ ) {
				while ( k < num_remote_buffers[level+1][proc] &&
						octs_to_join[j] > remote_buffers[level+1][proc][order[k]] ) {
					k++;
				}
				if ( k < num_remote_buffers[level+1][proc] &&
						octs_to_join[j] == remote_buffers[level+1][proc][order[k]] ) {
					octs_joined[proc][num_octs_to_send++] = order[k];
				}
			}

			cart_free(order);

			MPI_Isend( octs_joined[proc], num_octs_to_send, MPI_INT, proc, 0, mpi.comm.run, &sends[proc] );

			/* delete joined octs from remote_oct list */
			cell_buffer_delete_remote_octs( level+1, proc, num_octs_to_send, octs_joined[proc] );
		} else {
			octs_joined[proc] = NULL;
			sends[proc] = MPI_REQUEST_NULL;
		}
	}

	do {
		start_time( JOIN_BUFFER_COMMUNICATION_TIMER );
		MPI_Waitany( num_procs, receives, &proc, &status );
		end_time( JOIN_BUFFER_COMMUNICATION_TIMER );

		if ( proc != MPI_UNDEFINED ) {
			MPI_Get_count( &status, MPI_INT, &num_octs_joined );

			for ( j = 0; j < num_octs_joined; j++ ) {
				cart_assert( buffer_octs_to_join[proc][j] >= 0 && buffer_octs_to_join[proc][j] < num_local_buffers[level+1][proc] );
				oct_index = local_buffers[level+1][proc][buffer_octs_to_join[proc][j]];

				cart_assert( oct_index >= 0 && oct_index < num_octs );
				cart_assert( oct_level[oct_index] == level+1 );

				cell_index = oct_parent_cell[oct_index];

				result = join_cell(cell_index);
				if ( result ) {
					cart_error("Error joining buffer cell %d", cell_index );
				}
			}

			cell_buffer_delete_local_octs(level+1, proc, num_octs_joined, buffer_octs_to_join[proc]);
			cart_free( buffer_octs_to_join[proc] );
		}
	} while ( proc != MPI_UNDEFINED );

	/* wait for all sends to complete */
	start_time( JOIN_BUFFER_COMMUNICATION_TIMER );
	MPI_Waitall( num_procs, sends, MPI_STATUSES_IGNORE );
	end_time( JOIN_BUFFER_COMMUNICATION_TIMER );

	for ( proc = 0; proc < num_procs; proc++ ) {
		if ( octs_joined[proc] != NULL ) {
			cart_free(octs_joined[proc]);
		}
	}

	end_time( COMMUNICATION_TIMER );
	end_time( JOIN_BUFFER_TIMER );
}
