#include "config.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "auxiliary.h"
#include "cell_buffer.h"
#include "iterators.h"
#include "pack.h"
#include "parallel.h"
#include "sfc.h"
#include "tree.h"
#include "particle.h"
#include "starformation.h"

/*
//  Used in agn.c and halo_finder.c - buffers particles in buffered cells for neighbor searches.
//  Buffers species separately, as some species never need to be buffered.
//  Intent to be used locally, only in between global steps.
*/


#ifdef PARTICLES
int particle_buffer_enabled = 0;

void build_particle_buffer( int species, int subspecies ) {
	int i;
	int level, proc;
	int ipart, ipart2, icell;
	int count;
	int num_level_cells, *level_cells;
	int particle_list_to_send[MAX_PROCS];

	cart_assert( !particle_buffer_enabled );

	for ( proc = 0; proc < num_procs; proc++ ) {
		particle_list_to_send[proc] = NULL_PARTICLE;

		count = 0;
		for ( level = min_level; level <= max_level; level++ ) {
			select_local_buffered_cells( level, proc, &num_level_cells, &level_cells );

			for ( i = 0; i < num_level_cells; i++ ) {
				icell = level_cells[i];
				ipart = cell_particle_list[icell];
				while ( ipart != NULL_PARTICLE ) {
					ipart2 = particle_list_next[ipart];
					if ( species == -1 ||
							( particle_species(particle_id[ipart]) == species
#ifdef STAR_PARTICLE_TYPES
							  && ( species != num_particle_species-1 || subspecies == -1 ||
								  star_particle_type[ipart] == subspecies )
#endif /* STAR_PARTICLE_TYPES */
							) ) {
						particle_list_prev[ipart] = particle_list_to_send[proc];
						particle_list_to_send[proc] = ipart;
					}
					ipart = ipart2;
				}
			}
			cart_free( level_cells );
		}
    }

	/* communicate */
    trade_particle_lists( particle_list_to_send, -1, SAVE_PARTICLE_LISTS );
	rebuild_particle_lists();
	particle_buffer_enabled = 1;
}

void destroy_particle_buffer() {
	int icell, i, level, num_level_cells, *level_cells;

	cart_assert( particle_buffer_enabled );

	/* loop over buffer cells and remove any particles within */
	for ( level = min_level; level <= max_level; level++ ) {
		select_level( level, CELL_TYPE_BUFFER, &num_level_cells, &level_cells );
		for ( i = 0; i < num_level_cells; i++ ) {
			icell = level_cells[i];
			if ( cell_particle_list[icell] != NULL_PARTICLE ) {
				particle_list_free( cell_particle_list[icell] );
			}
			cell_particle_list[icell] = NULL_PARTICLE;
		}
		cart_free( level_cells );
	}
	particle_buffer_enabled = 0;
}

#endif /* PARTICLES */
