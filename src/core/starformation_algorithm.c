#include "config.h"
#if defined(HYDRO) && defined(STAR_FORMATION)


#include <string.h>

#include "auxiliary.h"
#include "control_parameter.h"
#include "starformation_algorithm.h"

extern struct StarFormationAlgorithm sf_algorithm_internal;
const struct StarFormationAlgorithm *sf_algorithm = &sf_algorithm_internal;

const char *sf_algorithm_name = NULL;

/*
//  Configuration
*/
void control_parameter_list_star_formation_algorithm(FILE *stream, const void *ptr)
{
  fprintf(stream,"<%s>",sf_algorithm_name);
}


void config_init_star_formation_algorithm()
{
  static char *ptr = "star_formation_algorithm";
  ControlParameterOps r = { NULL, control_parameter_list_star_formation_algorithm };

  sf_algorithm_name = MACRO_AS_STRING(SF_ALGORITHM);

  control_parameter_add(r,ptr,"sf:algorithm","an algorithm for making, evolving, and deleting stellar particles. This parameter is for listing only, and must be set with SF_ALGORITHM variable in the Makefile. See /src/sf for available methods.");

  if(sf_algorithm_internal.config_init != NULL) sf_algorithm_internal.config_init();
}


void config_verify_star_formation_algorithm()
{
  cart_assert(sf_algorithm_internal.create_star_particles != NULL);

  VERIFY(sf:algorithm, 1 );

  if(sf_algorithm_internal.config_verify != NULL) sf_algorithm_internal.config_verify();
}


void init_star_formation_algorithm()
{
  if(sf_algorithm_internal.init != NULL) sf_algorithm_internal.init();
}

#endif /* HYDRO && STAR_FORMATION */
