#ifndef __HYDRO_TRACER_H__
#define __HYDRO_TRACER_H__

#ifndef CONFIGURED
#error "Missing config.h include."
#endif

#if defined(HYDRO) && defined(HYDRO_TRACERS)

/*
//  C++ compatibility: INT64_MAX is defined only if __STDC_LIMIT_MACROS set explicitly (ISO C99 standard)
*/
#if defined(__cplusplus) && !defined(__STDC_LIMIT_MACROS)
#define __STDC_LIMIT_MACROS
#endif

#include <stdint.h>
#include <limits.h>

#ifdef OLDSTYLE_32BIT_TRACERID
#define tracerid_t      int
#define MPI_TRACERID_T  MPI_INT
#define TRACERID_MAX    INT_MAX
#else
#define tracerid_t      int64_t
#ifdef MPI_INT64_T
#define MPI_TRACERID_T  MPI_INT64_T
#else
#define MPI_TRACERID_T  MPI_LONG_LONG_INT
#endif
#define TRACERID_MAX    INT64_MAX
#endif

#define NULL_TRACER     ((tracerid_t)-1)

extern tracerid_t tracer_id[num_tracers];
extern double tracer_x[num_tracers][nDim];
extern double tracer_t[num_tracers];
extern int tracer_list_next[num_tracers];
extern int tracer_list_prev[num_tracers];

extern int cell_tracer_list[num_cells];

extern int num_tracer_row;
extern int num_local_tracers;
extern tracerid_t num_tracers_total;
extern int next_free_tracer;
extern int free_tracer_list;
extern int tracer_list_enabled;

extern int num_hydro_vars_traced;
extern int hydro_vars_traced[];
extern char *hydro_vars_traced_labels[];

int tracer_alloc( tracerid_t id );
void tracer_free( int tracer );
void tracer_list_free( int ihead );

void init_hydro_tracers();
void set_hydro_tracers( int min_tracer_level );

#ifdef PARTICLES
void set_hydro_tracers_to_particles();
void set_hydro_tracers_to_particles2(double pos[3], double rad);
#endif /* PARTICLES */

void update_tracer_list( int level );
void trade_tracer_lists( int tracer_list_to_send[MAX_PROCS], int trade_level );
void build_tracer_list();
void split_tracer_list( int icell );
void join_tracer_list( int icell );
void insert_tracer( int icell, int part );
void delete_tracer( int icell, int part );

int *gather_root_tracer_lists();
void rebuild_tracer_lists();

int compare_tracer_ids( const void *a, const void *b );

#ifdef HYDRO_TRACERS_MONTE_CARLO
void mc_tracer_list_init();
void mc_tracer_list_send( int level );
#endif /* HYDRO_TRACERS_MONTE_CARLO */

#endif /* defined(HYDRO) && defined(HYDRO_TRACERS) */

#endif
