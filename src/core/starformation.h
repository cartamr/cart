#ifndef __STARFORMATION_H__
#define __STARFORMATION_H__

#ifndef CONFIGURED
#error "Missing config.h include."
#endif


#ifdef STAR_FORMATION

#include "particle.h"

#ifdef STAR_PARTICLE_TYPES
extern int star_particle_type[num_star_particles];
#endif /* STAR_PARTICLE_TYPES */

#define STAR_TYPE_DELETED       (-1)
#define STAR_TYPE_NORMAL        0
#define STAR_TYPE_AGN           1
#define STAR_TYPE_STARII        2
#define STAR_TYPE_FAST_GROWTH   3


extern double star_formation_volume_min[nDim];
extern double star_formation_volume_max[nDim];

DECLARE_LEVEL_ARRAY(int,star_formation_frequency);
#ifdef CLUSTER_DETAIL_OUTPUT
DECLARE_LEVEL_ARRAY(int,cluster_output_frequency);
#endif /* CLUSTER_DETAIL_OUTPUT */

extern int num_local_star_particles;

extern double total_stellar_mass;
extern double total_stellar_initial_mass;

extern float star_tbirth[num_star_particles];
extern float star_initial_mass[num_star_particles];

#ifdef ENRICHMENT
extern float star_metallicity_II[num_star_particles];
#ifdef ENRICHMENT_SNIa
extern float star_metallicity_Ia[num_star_particles];
#endif /* ENRICHMENT_SNIa */
#ifdef ENRICHMENT_ELEMENTS
/* Star metallicity will be named the same thing, even for 
   individual elements, since it is the same format:
   star_metallicity_X = mass of element X / total mass of star particle */
extern float star_metallicity_AGB[num_star_particles];
extern float star_metallicity_C[num_star_particles];
extern float star_metallicity_N[num_star_particles];
extern float star_metallicity_O[num_star_particles];
extern float star_metallicity_Mg[num_star_particles];
extern float star_metallicity_S[num_star_particles];
extern float star_metallicity_Ca[num_star_particles];
extern float star_metallicity_Fe[num_star_particles];
#endif /* ENRICHMENT_ELEMENTS*/
#endif /* ENRICHMENT */
#ifdef DISCRETE_SN
// Also have one variable that will serve as the SN counter. It will accumulate
// fractional supernovae through many timesteps until it reaches one, when a
// supernova will explode
extern float star_unexploded_sn[num_star_particles];
#endif /* DISCRETE_SN */
#ifdef CLUSTER
extern float star_tfinal[num_star_particles];
extern float star_ave_age[num_star_particles];
extern float star_metal_dispersion[num_star_particles];
#ifdef CLUSTER_BOUND_FRACTION
extern float star_fbound[num_star_particles];
#endif /* CLUSTER_BOUND_FRACTION */
#ifdef CLUSTER_INITIAL_BOUND
extern float star_ibound[num_star_particles];
#endif /* CLUSTER_INITIAL_BOUND */
#ifdef CLUSTER_DEBUG
extern float star_age_spread[num_star_particles];
#endif /* CLUSTER_DEBUG */
#endif /* CLUSTER */

#ifndef COSMOLOGY
extern double sf_delay;
#endif /* !COSMOLOGY */

void config_init_star_formation();
void config_verify_star_formation();

void init_star_formation();
void star_formation_rate( int level, int num_level_cells, int *level_cells, float *sfr);
int create_star_particle( int icell, float mass, double pdt, int type );
int create_star_particle2(int icell, float mass, double pdt, int type, float init_mass); 
int create_star_particle3(int icell, float mass, double pdt, double dt_eff, int type);


void start_star_allocation();
void end_star_allocation();
particleid_t star_particle_alloc();

/* global parameters */

extern double sf_sampling_timescale;
#ifdef CLUSTER_DETAIL_OUTPUT
extern double cluster_output_timescale;
#endif /* CLUSTER_DETAIL_OUTPUT */

#endif /* STAR_FORMATION */

#endif
