#ifndef __PARALLEL_H__
#define __PARALLEL_H__

#ifndef CONFIGURED
#error "Missing config.h include."
#endif

#include "index_hash.h"
#include "parallel_config.h"
#include "sfc.h"

/*
//  This is a trick to ensure communicator compliance
*/
#ifdef MPI_COMM_WORLD
#undef MPI_COMM_WORLD
#endif


#define MPI_COMM_WORLD                  MPI_COMM_WORLD_should_not_be_used_directly


#define	MASTER_NODE			0


extern struct ART_MPI_TYPE mpi;


extern int num_procs;
extern int local_proc_id;
extern int tasks_per_node;

extern int root_cells_enabled;

#ifdef SFC_DOMAIN_DECOMPOSITION
extern sfc_t proc_sfc_index[MAX_PROCS+1];
#else
extern index_hash *root_hash;
#endif
extern sfc_t *root_cell_sfc_index;

void configure_runtime_setup(void); // Should be called by ALL tasks

void config_init_parallel(void);
void config_verify_parallel(void);

int sfc_is_local( sfc_t sfc );
int sfc_processor_owner( sfc_t sfc );  /* will return -1 if owner is not known by the current rank */
int sfc_type( sfc_t sfc );
int sfc_cell_location( sfc_t sfc );

#define MPI_CUSTOM_NONE			0x0U

extern unsigned int mpi_custom_flags;

void print_comm_contents(MPI_Comm com, const char *name);

#endif
