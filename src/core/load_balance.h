#ifndef __LOAD_BALANCE_H__
#define __LOAD_BALANCE_H__

#ifndef CONFIGURED
#error "Missing config.h include."
#endif

extern float reserve_cell_fraction;
#ifdef PARTICLES
extern float reserve_particle_fraction;
#endif /* PARTICLES */

extern int load_balance_frequency;

void config_init_load_balance();
void config_verify_load_balance();

void load_balance();

#endif
