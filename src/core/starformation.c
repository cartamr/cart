#include "config.h"
#if defined(PARTICLES) && defined(STAR_FORMATION)

#include "agn.h"
#include "auxiliary.h"
#include "control_parameter.h"
#include "cosmology.h"
#include "hydro.h"
#include "parallel.h"
#include "particle.h"
#include "starformation.h"
#include "starformation_algorithm.h"
#include "starformation_feedback.h"
#include "starformation_recipe.h"
#include "times.h"
#include "tree.h"
#include "units.h"

#include "imf.h"

#ifdef ENRICHMENT_ELEMENTS
#include "../sf/models/feedback.detailed_enrich.h"
// Get the needed attributes from the detailed enrichment file for use in the
// metalliicty floor calculation
extern int detail_n_fields_sn_ii;
extern int detail_field_idx_sn_ii_C;
extern int detail_field_idx_sn_ii_N;
extern int detail_field_idx_sn_ii_O;
extern int detail_field_idx_sn_ii_Mg;
extern int detail_field_idx_sn_ii_S;
extern int detail_field_idx_sn_ii_Ca;
extern int detail_field_idx_sn_ii_Fe;
extern int detail_field_idx_sn_ii_metals;
extern int detail_field_idx_sn_ii_total;
#endif /* ENRICHMENT_ELEMENTS */

int num_local_star_particles = 0;
particleid_t next_star_id_block = -1;
int num_new_stars = 0;

double total_stellar_mass = 0.0;
double total_stellar_initial_mass = 0.0;

float star_tbirth[num_star_particles];
float star_initial_mass[num_star_particles];

#ifdef ENRICHMENT
float star_metallicity_II[num_star_particles];
#ifdef ENRICHMENT_SNIa
float star_metallicity_Ia[num_star_particles];
#endif /* ENRICHMENT_SNIa */
#ifdef ENRICHMENT_ELEMENTS
/* Star metallicity will be named the same thing, even for 
   individual elements, since it is the same format:
   star_metallicity_X = mass of element X / total mass of star particle */
float star_metallicity_AGB[num_star_particles];
float star_metallicity_C[num_star_particles];
float star_metallicity_N[num_star_particles];
float star_metallicity_O[num_star_particles];
float star_metallicity_Mg[num_star_particles];
float star_metallicity_S[num_star_particles];
float star_metallicity_Ca[num_star_particles];
float star_metallicity_Fe[num_star_particles];
#endif /* ENRICHMENT_ELEMENTS*/
#endif /* ENRICHMENT */
// Also have one variable that will serve as the SN counter. It will accumulate
// fractional supernovae through many timesteps until it reaches one, when a
// supernova will explode
#ifdef DISCRETE_SN
float star_unexploded_sn[num_star_particles];
#endif /* DISCRETE_SN */

#ifdef CLUSTER
float star_tfinal[num_star_particles];
float star_ave_age[num_star_particles];
float star_metal_dispersion[num_star_particles];
#ifdef CLUSTER_BOUND_FRACTION
float star_fbound[num_star_particles];
#endif /* CLUSTER_BOUND_FRACTION */
#ifdef CLUSTER_INITIAL_BOUND
float star_ibound[num_star_particles];
#endif /* CLUSTER_INITIAL_BOUND */
#ifdef CLUSTER_DEBUG
float star_age_spread[num_star_particles];
#endif /* CLUSTER_DEBUG */
#endif /* CLUSTER */

#ifdef STAR_PARTICLE_TYPES
int star_particle_type[num_star_particles];
#endif /* STAR_PARTICLE_TYPES */

double star_formation_volume_min[nDim] = {0., 0., 0.};
double star_formation_volume_max[nDim] = {num_grid, num_grid, num_grid};

// declare function prototypes for internal function
int create_star_particle_base(int icell, float mass, double pdt, int type, float init_mass, float dt_eff);

#ifdef HYDRO

DEFINE_LEVEL_ARRAY(int,star_formation_frequency);
#ifdef CLUSTER_DETAIL_OUTPUT
DEFINE_LEVEL_ARRAY(int,cluster_output_frequency);
#endif /* CLUSTER_DETAIL_OUTPUT */

/* star formation parameters */
int sf_min_level = min_level;  /* Minimum level on which to create stars */

double sf_min_gas_number_density = 0.1;      /* in cm^{-3}; used to be called rho_SF */
double sf_max_gas_temperature = 2.0e4;       /* in K; used to be called T_SF */

double sf_sampling_timescale = 1.0e6;        /* in yrs; used to be called dtmin_SF, also in HART */

#ifdef CLUSTER_DETAIL_OUTPUT
double cluster_output_timescale = 1.0e5;        /* in yrs; used to be called dtmin_SF, also in HART */
#endif /* CLUSTER_DETAIL_OUTPUT */

float sf_min_overdensity = 200;

float sf_metallicity_floor = 0.0;            /* this is an obscure parameter, read its help string in config_init_star_formation(). */

#ifndef COSMOLOGY
double sf_delay = 0.;
#endif /* !COSMOLOGY */

void config_init_star_formation()
{
  /*
  //  General parameters
  */
  control_parameter_add2(control_parameter_int,&sf_min_level,"sf:min-level","sf_min_level","minimum level on which do star formation. Cells with level < <sf:min-level> form no stars no matter what.");

  control_parameter_add3(control_parameter_double,&sf_min_gas_number_density,"sf:min-gas-number-density","sf_min_gas_number_density","rho_sf","the gas total hydrogen number density threshold for star formation, in cm^{-3}. No star formation is done for gas at lower densities.");

  control_parameter_add3(control_parameter_double,&sf_max_gas_temperature,"sf:max-gas-temperature","sf_max_gas_temperature","t_sf","the maximum gas temperature (in K) for star formation. No star formation is done in hotter gas.");

  control_parameter_add3(control_parameter_time,&sf_sampling_timescale,"sf:sampling-timescale","sf_sampling_timescale","dtmin_sf","the timescale on which the conditions for star formation are checked. This is a numerical parameter only, no physical results should depend on it; its value should be sufficiently smaller than the <sf:timescale> parameter.  This parameter used to be called 'dtmin_SF' in HART.");
#ifdef CLUSTER_DETAIL_OUTPUT
  control_parameter_add3(control_parameter_time,&cluster_output_timescale,"sf:cluster-output-timescale","cluster_output_timescale","dtmin_cluster","the timescale on which the conditions for output cluster particle properties.");
#endif /* CLUSTER_DETAIL_OUTPUT */

  control_parameter_add2(control_parameter_float,&sf_min_overdensity,"sf:min-overdensity","sf_min_overdensity","the value of the overdensity (in total mass, not gas mass) below which the gas density threshold is not allowed to descend. I.e., rho_min = MAX(<sf:min-gas-number-density>/(constants->XH*units->number_density),<sf:min-overdensity>*cosmology->OmegaB/cosmology->OmegaM");

  control_parameter_add2(control_parameter_float,&sf_metallicity_floor,"sf:metallicity-floor","sf_metallicity_floor","the minimum amount of metallicity (in solar units) sprinkled in the cell where a stellar particle is created. This parameter is designed to emulate metal enrichment from the first stars and/or early, unresolved episode of star formation. Just BEFORE a stellar particle is created, if the metallicity in the cell is less than this value, it is increased to this value. Thus, all stellar particles are created in the gas with at least this metallicity. This value is NOT taken into account when computing the SFR, only when actually creating a stellar particle. It is not related to rt:dust-to-gas floor, which MAY affect the SFR.");

#ifndef COSMOLOGY
  control_parameter_add2(control_parameter_time,&sf_delay,"sf:delay","sf_delay","time delay before star formation is on.");
#endif /* !COSMOLOGY */

  /* 
  //  IMF 
  */
  config_init_imf();

  config_init_star_formation_recipe();
  config_init_star_formation_feedback();
  config_init_star_formation_algorithm();
}


void config_verify_star_formation()
{
  /*
  //  General parameters
  */
  VERIFY(sf:min-level, sf_min_level>=min_level && sf_min_level<=max_level );

  VERIFY(sf:min-gas-number-density, sf_min_gas_number_density > 0.0 );

  VERIFY(sf:max-gas-temperature, sf_max_gas_temperature > 10.0 );

  VERIFY(sf:sampling-timescale, sf_sampling_timescale >= 0.0 ); 
#ifdef CLUSTER_DETAIL_OUTPUT
  VERIFY(sf:cluster-output-timescale, cluster_output_timescale >= 0.0 ); 
#endif /* CLUSTER_DETAIL_OUTPUT */

  VERIFY(sf:metallicity-floor, !(sf_metallicity_floor < 0.0) );

  VERIFY(sf:min-overdensity, 1 );

#ifndef COSMOLOGY
  VERIFY(sf:delay, 1 );
#endif /* !COSMOLOGY */

  /* 
  //  IMF 
  */
  config_verify_imf();

  config_verify_star_formation_recipe();
  config_verify_star_formation_feedback();
  config_verify_star_formation_algorithm();
}

#endif /* HYDRO */


void init_star_formation()
{
  int j;
  
  init_star_formation_feedback();
  init_star_formation_algorithm();

  /*
  //  NG: This limit is very obscure, disable by default
  */
  for(j=0; j<nDim; j++)
    {
      star_formation_volume_min[j] = 0.0;
      star_formation_volume_max[j] = num_grid;
    }
}


#ifdef HYDRO

void star_formation_rate(int level, int num_level_cells, int *level_cells, float *sfr) {
	int i, j;
	int cell;
	double pos[nDim];
	int do_star_formation;
	double tem_max, rho_min;

	tem_max = sf_max_gas_temperature/(constants->wmu*units->temperature);
	rho_min = sf_min_gas_number_density/(constants->XH*units->number_density);
#ifdef COSMOLOGY
	rho_min = MAX(rho_min,sf_min_overdensity*cosmology->OmegaB/cosmology->OmegaM);
#endif

	if(sf_recipe->setup != NULL) sf_recipe->setup(level);

	for(i=0; i<num_level_cells; i++)
	{
		cell = level_cells[i];

		sfr[i] = -1.0;
		if(cell_is_leaf(cell))
		{
			/* check position */
			cell_center_position(cell,pos);

			do_star_formation = 1;
			for(j=0; j<nDim; j++)
			{
				if(pos[j]<star_formation_volume_min[j] || pos[j]>star_formation_volume_max[j])
				{
					do_star_formation = 0;
				}
			}

			if(do_star_formation && cell_gas_density(cell)>rho_min && cell_gas_temperature(cell)<tem_max)
			{
				sfr[i] = sf_recipe->rate(cell);
			}
		}
	}
}


int create_star_particle_base( int icell, float mass, double pdt, int type, float init_mass, float dt_eff ) {
	int i;
	int ipart;
	particleid_t id;
	int level;
	double pos[nDim];
	float new_density;
	float density_fraction, thermal_pressure;
	float true_mass, true_init_mass;

	cart_assert( icell >= 0 && icell < num_cells );
	cart_assert( mass > 0.0 );

	id = star_particle_alloc();
	ipart = particle_alloc( id );
	cart_assert( ipart < num_star_particles );

	/* ensure star particle cannot consume entire cell gas mass */
	true_mass = MIN( mass, 0.667*cell_volume[cell_level(icell)]*cell_gas_density(icell) );
	true_init_mass = MIN(init_mass, 0.667*cell_volume[cell_level(icell)]*cell_gas_density(icell) );

	/*
	//  This is an obscure parameter, read its help string in 
	//  config_init_star_formation().
	*/
#ifdef ENRICHMENT
	if(sf_metallicity_floor>0.0 && cell_gas_metal_density_II(icell)<sf_metallicity_floor*constants->Zsun*cell_gas_density(icell))
	  {
	    cell_gas_metal_density_II(icell) =  sf_metallicity_floor*constants->Zsun*cell_gas_density(icell);
		#ifdef ENRICHMENT_ELEMENTS
		// here we add the yields of a Z=0 supernova of the highest mass 
		// (since that's the best (?) approximation of a pop 3 supernovae in 
		// the current yield setup)
		double ejecta[detail_n_fields_sn_ii];
		get_yields_sn_ii(0.0, 40.0, ejecta);  // largest model (no interpolation)
		// Then scale the ejecta based on the metals we need to add
		double metal_mass = cell_gas_metal_density_II(icell) * cell_volume[cell_level(icell)];
		double n_sn_equivalent = metal_mass / ejecta[detail_field_idx_sn_ii_metals];
		// Then scale this by the density before adding all fields to the cell
		n_sn_equivalent *= cell_volume_inverse[cell_level(icell)];
		cell_gas_metal_density_C(icell)  = n_sn_equivalent * ejecta[detail_field_idx_sn_ii_C];
		cell_gas_metal_density_N(icell)  = n_sn_equivalent * ejecta[detail_field_idx_sn_ii_N];
		cell_gas_metal_density_O(icell)  = n_sn_equivalent * ejecta[detail_field_idx_sn_ii_O];
		cell_gas_metal_density_Mg(icell) = n_sn_equivalent * ejecta[detail_field_idx_sn_ii_Mg];
		cell_gas_metal_density_S(icell)  = n_sn_equivalent * ejecta[detail_field_idx_sn_ii_S];
		cell_gas_metal_density_Ca(icell) = n_sn_equivalent * ejecta[detail_field_idx_sn_ii_Ca];
		cell_gas_metal_density_Fe(icell) = n_sn_equivalent * ejecta[detail_field_idx_sn_ii_Fe];
		#endif /* ENRICHMENT_ELEMENTS */
	  }
#endif

#ifdef STAR_PARTICLE_TYPES
	star_particle_type[ipart] = type;
#endif

	/* place particle at center of cell with cell momentum */
	cell_center_position(icell, pos );
	level = cell_level(icell);

	for ( i = 0; i < nDim; i++ ) {
		particle_x[ipart][i] = pos[i];
	}

	for ( i = 0; i < nDim; i++ ) {
		particle_v[ipart][i] = cell_momentum(icell,i) / cell_gas_density(icell);
	}

	particle_t[ipart] = tl[level];
	particle_dt[ipart] = pdt;

	star_tbirth[ipart] = tl[level];
	particle_mass[ipart] = true_mass;
	star_initial_mass[ipart] = true_init_mass;

#ifdef STAR_PARTICLE_TYPES
	if( star_particle_type[ipart] == STAR_TYPE_NORMAL || star_particle_type[ipart] == STAR_TYPE_STARII || star_particle_type[ipart] == STAR_TYPE_FAST_GROWTH ) {
#endif /* STAR_PARTICLE_TYPES */

#ifdef ENRICHMENT
            star_metallicity_II[ipart] = cell_gas_metal_density_II(icell) / cell_gas_density(icell);
#ifdef ENRICHMENT_SNIa
            star_metallicity_Ia[ipart] = cell_gas_metal_density_Ia(icell) / cell_gas_density(icell);
#endif /* ENRICHMENT_SNIa */
#ifdef ENRICHMENT_ELEMENTS
			star_metallicity_AGB[ipart] = cell_gas_metal_density_AGB(icell) / cell_gas_density(icell);
			star_metallicity_C[ipart]   = cell_gas_metal_density_C(icell)   / cell_gas_density(icell);
			star_metallicity_N[ipart]   = cell_gas_metal_density_N(icell)   / cell_gas_density(icell);
			star_metallicity_O[ipart]   = cell_gas_metal_density_O(icell)   / cell_gas_density(icell);
			star_metallicity_Mg[ipart]  = cell_gas_metal_density_Mg(icell)  / cell_gas_density(icell);
			star_metallicity_S[ipart]   = cell_gas_metal_density_S(icell)   / cell_gas_density(icell);
			star_metallicity_Ca[ipart]  = cell_gas_metal_density_Ca(icell)  / cell_gas_density(icell);
			star_metallicity_Fe[ipart]  = cell_gas_metal_density_Fe(icell)  / cell_gas_density(icell);
#endif /* ENRICHMENT_ELEMENTS */
#endif /* ENRICHMENT */

#ifdef DISCRETE_SN
			// The star particle has had no supernovae yet, since it's just 
			// forming.
			star_unexploded_sn[ipart] = 0.0;
#endif /* DISCRETE_SN */

#ifdef CLUSTER
    /* when creating cluster particle, the final time for this particle is the current time.
     * the average star particle age (Myr) is HALF of the current timestep tdl[level]=pdt */
    star_tfinal[ipart] = dt_eff;
    // star_ave_age[ipart] = particle_t[ipart]-star_tbirth[ipart];
    star_ave_age[ipart] = 0.5 * dt_eff;
    /* track the level of cluster formation */
#ifdef ENRICHMENT_ELEMENTS
    star_metal_dispersion[ipart] = star_metallicity_Fe[ipart]*star_metallicity_Fe[ipart]*true_mass;
#else
    star_metal_dispersion[ipart] = star_metallicity_II[ipart]*star_metallicity_II[ipart]*true_mass;
#endif /* ENRICHMENT_ELEMENTS */
#ifdef CLUSTER_BOUND_FRACTION
    /* the initial cluster bound fraction is 1. */
    star_fbound[ipart] = 1.0;
#endif /* CLUSTER_BOUND_FRACTION */
#ifdef CLUSTER_INITIAL_BOUND
    /* this variable records the maximum baryonic mass within the sphere
     * Here only dm is recorded for simplicity. */
    star_ibound[ipart] = true_mass;
#endif /* CLUSTER_INITIAL_BOUND */
#endif /* CLUSTER */

#ifdef STAR_PARTICLE_TYPES
	} else if( star_particle_type[ipart] == STAR_TYPE_AGN ) {
#ifdef ENRICHMENT
            star_metallicity_II[ipart] = 0.0;
#ifdef ENRICHMENT_SNIa
            star_metallicity_Ia[ipart] = 0.0;
#endif /* ENRICHMENT_SNIa */
#ifdef ENRICHMENT_ELEMENTS
			/* AGN have no metals */
			star_metallicity_AGB[ipart] = 0.0;
			star_metallicity_C[ipart] = 0.0;
			star_metallicity_N[ipart] = 0.0;
			star_metallicity_O[ipart] = 0.0;
			star_metallicity_Mg[ipart] = 0.0;
			star_metallicity_S[ipart] = 0.0;
			star_metallicity_Ca[ipart] = 0.0;
			star_metallicity_Fe[ipart] = 0.0;
			
#endif /* ENRICHMENT_ELEMENTS */
#endif /* ENRICHMENT */
#ifdef DISCRETE_SN
			// AGN have no supernovae
			star_unexploded_sn[ipart] = 0.0;
#endif /* DISCRETE_SN */
	} else {
            cart_error("Initial metallicity for star_particle_type %d must be explicitly defined on creation",star_particle_type[ipart]);
	}	
#endif /* STAR_PARTICLE_TYPES */

	/* insert particle into cell linked list */
	insert_particle( icell, ipart );

	/* adjust cell values */
	new_density = cell_gas_density(icell) - true_mass * cell_volume_inverse[level];
	density_fraction = new_density / cell_gas_density(icell);

	/*
	// NG: this is to allow non-thermal pressure contribution
	*/
	thermal_pressure = MAX((cell_gas_gamma(icell)-1.0)*cell_gas_internal_energy(icell),0.0);
	cell_gas_pressure(icell) = MAX(0.0,cell_gas_pressure(icell)-thermal_pressure);

	cell_gas_density(icell) = new_density;
	cell_gas_internal_energy(icell) *= density_fraction;
	cell_momentum(icell,0) *= density_fraction;
	cell_momentum(icell,1) *= density_fraction;
	cell_momentum(icell,2) *= density_fraction;
		
	cell_gas_pressure(icell) += thermal_pressure*density_fraction;

	// We assume that extra energy remains constant per volume
	for ( i = 0; i < num_extra_energy_variables; i++ ) {
		cell_gas_energy(icell) -= cell_extra_energy_variables(icell,i);
	}
	cell_gas_energy(icell) *= density_fraction;
	for ( i = 0; i < num_extra_energy_variables; i++ ) {
		cell_gas_energy(icell) += cell_extra_energy_variables(icell,i);
	}

	for ( i = 0; i < num_chem_species; i++ ) {
		cell_advected_variable(icell,i) *= density_fraction;
	}

#ifdef BLASTWAVE_FEEDBACK
	start_blastwave(icell);
#endif /* BLASTWAVE_FEEDBACK */

	return ipart;
}

int create_star_particle(int icell, float mass, double pdt, int type){
	int ipart;
	ipart = create_star_particle_base(icell, mass, pdt, type, mass, pdt);
	return ipart;
}

int create_star_particle2( int icell, float mass, double pdt, int type, float init_mass ){
	int ipart;
	ipart = create_star_particle_base(icell, mass, pdt, type, init_mass, pdt);
	return ipart;
}

int create_star_particle3( int icell, float mass, double pdt, double dt_eff, int type ){
	int ipart;
	ipart = create_star_particle_base(icell, mass, pdt, type, mass, dt_eff);
	return ipart;
}

#endif /* HYDRO */



void start_star_allocation() {
	cart_assert( num_particle_species >= 1 );
	num_new_stars = 0;
	next_star_id_block = particle_species_indices[num_particle_species];
}

particleid_t star_particle_alloc() {
	particleid_t id;

	cart_assert( num_new_stars >= 0 );
	cart_assert( next_star_id_block >= particle_species_indices[num_particle_species] );

	id = next_star_id_block + local_proc_id;
	next_star_id_block += num_procs;
	num_new_stars++;
	return id;
}

void end_star_allocation() {
	int i;
	int proc;
	int ipart;
	int block;
	int max_stars;	
	particleid_t new_id;
	int total_new_stars;
	particleid_t *block_ids;
	int proc_new_stars[MAX_PROCS];

	/* collect number of stars created */
	MPI_Allgather( &num_new_stars, 1, MPI_INT, proc_new_stars, 1, MPI_INT, mpi.comm.run );

	/* find how many "blocks" to expect */
	max_stars = 0;
	total_new_stars = 0;
	for ( proc = 0; proc < num_procs; proc++ ) {
		if ( proc_new_stars[proc] > max_stars ) {
			max_stars = proc_new_stars[proc];
		}

		total_new_stars += proc_new_stars[proc];
	}

	if ( total_new_stars > 0 ) {
		/* create lists of indices for each block */
		block_ids = cart_alloc(particleid_t, max_stars);

		block_ids[0] = 0;
		for ( block = 1; block < max_stars; block++ ) {
			block_ids[block] = block_ids[block-1];
			for ( proc = 0; proc < num_procs; proc++ ) {
				if ( proc_new_stars[proc] >= block ) {
					block_ids[block]++;
				}
			}
		}
	
		/* find all newly allocated stars and remap their id's (keeping order) */
		for ( ipart = 0; ipart < num_star_particles; ipart++ ) {
			if ( particle_level[ipart] != FREE_PARTICLE_LEVEL && 
					particle_id[ipart] >= particle_species_indices[num_particle_species] ) {
	
				block = ( particle_id[ipart] - particle_species_indices[num_particle_species] ) / num_procs;
				cart_assert( block < max_stars );
				proc = ( particle_id[ipart] - particle_species_indices[num_particle_species] ) % num_procs;
				new_id = particle_species_indices[num_particle_species] + block_ids[block];
	
				for ( i = 0; i < proc; i++ ) {
					if ( proc_new_stars[i] > block ) {
						new_id++;
					}
				}
				
				cart_assert( new_id <= particle_id[ipart] && 
					new_id < particle_species_indices[num_particle_species]+total_new_stars );

				particle_id[ipart] = new_id;
			}
		}

		cart_free(block_ids);
	}

	particle_species_indices[num_particle_species] += total_new_stars;
	particle_species_num[num_particle_species-1] += total_new_stars;
	num_particles_total += total_new_stars;
	num_new_stars = 0;
	next_star_id_block = particle_species_indices[num_particle_species];
}

#endif /* PARTICLES && STAR_FORMATION */
