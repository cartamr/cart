#ifndef __CELL_BUFFER_H__
#define __CELL_BUFFER_H__

#ifndef CONFIGURED
#error "Missing config.h include."
#endif


#include "index_hash.h"
#include "oct_hash.h"

extern int buffer_enabled;

#ifdef SFC_DOMAIN_DECOMPOSITION
extern index_hash *buffer_root_hash;
#else
extern int *buffer_cell_proc_owner;
#endif

DECLARE_LEVEL_ARRAY(int,num_buffer_cells);
DECLARE_LEVEL_ARRAY(int,buffer_oct_list);

DECLARE_LEVEL_ARRAY(int*,num_remote_buffers);
DECLARE_LEVEL_ARRAY(int**,remote_buffers);
DECLARE_LEVEL_ARRAY(int*,num_local_buffers);
DECLARE_LEVEL_ARRAY(int**,local_buffers);

void init_cell_buffer();
void build_cell_buffer();
void destroy_cell_buffer();

void test_update_buffer ( int level, const int *g2l, int tot);
void cell_buffer_scatter( int level, const int *var_indices, const int num_update_vars );
void cell_buffer_gather( int level, const int *var_indices, const int num_gather_vars );
void update_buffer_level( int level, const int *var_indices, const int num_update_vars );
void update_cached_buffer_level( int level, int var, float *cvar, const int * g2l, int num_total_cells );

void split_buffer_cells( int level, int *cells_to_split, int num_cells_to_split );
void join_buffer_cells( int level, int *octs_to_join, int num_octs_to_join );

#define PROC_ANY   -1

int cell_can_prune( int cell, int proc );

#endif
