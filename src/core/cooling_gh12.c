#include "config.h"
#if defined(COOLING) && !defined(RADIATIVE_TRANSFER) && defined(COOLING_GH12)

#include <math.h>
#include <string.h>

#include "auxiliary.h"
#include "control_parameter.h"
#include "cooling.h"
#include "units.h"


struct
{
  float Plw, Ph1, Pg1, Pc6;
}
clCfg = { 1.0e-10, 0, 0, 0 };

float clFac = 1.0;


void frtinitcf_(const int *ierr, const char *path, int len);
void frtcfcache_(const float *Den, const float *Z, const float *Plw, const float *Ph1, const float *Pg1, const float *Pc6, int *icache, float *rcache, int *err);
void frtcfgetln_(const int *mode, const float *alt, int *icache, float *rcache, float *cfun, float *hfun);



#ifdef _OPENMP
#include <omp.h>
#endif

#ifndef GH12_MAX_NUM_THREADS
#define GH12_MAX_NUM_THREADS 16
#endif

struct clCacheType
{
  double Zlog, nHlog;
  float Z, nb, fac;
  int icache[12];
  float rcache[13];
}
clCache[GH12_MAX_NUM_THREADS];


void config_init_cooling() 
{
	control_parameter_add2(control_parameter_float,&clCfg.Plw,"cooling:P_LW", "clCfg.Plw","H2 photodissociation rate in the Lyman-Werner bands in 1/s");
	control_parameter_add2(control_parameter_float,&clCfg.Ph1,"cooling:P_HI", "clCfg.Ph1","HI photoionization rate in 1/s");
	control_parameter_add2(control_parameter_float,&clCfg.Pg1,"cooling:P_HeI","clCfg.Pg1","HeI photoionization rate in 1/s");
	control_parameter_add2(control_parameter_float,&clCfg.Pc6,"cooling:P_CVI","clCfg.Pc6","CVI photoionization rate in 1/s");
}


void config_verify_cooling()
{
	VERIFY(cooling:P_LW,  clCfg.Plw >= 0.0 );
	VERIFY(cooling:P_HI,  clCfg.Ph1 >= 0.0 );
	VERIFY(cooling:P_HeI, clCfg.Pg1 >= 0.0 );
	VERIFY(cooling:P_CVI, clCfg.Pc6 >= 0.0 );
}


void init_cooling()
{
  int ierr = 0;
  const char *path = "drt/cf_table.I2.dat";
  int i;

  frtinitcf_(&ierr,path,strlen(path));
  
  if(ierr != 0) cart_error("Error in reading %s data file: %d",path,ierr);

  for(i=0; i<GH12_MAX_NUM_THREADS; i++)
    {
      clCache[i].Zlog = 1e10;
      clCache[i].nHlog = 1e10;
    }
}


void set_cooling_redshift( double auni )
{
#ifdef COSMOLOGY
#error "Not implemented."
#endif
}


cooling_t cooling_rate( double nHlog, double T_g, double Zlog )
{
  static int mode = 0;
#ifdef _OPENMP
  int nomp = omp_get_max_threads();
  int iomp = omp_get_thread_num();
#else
  int nomp = 1;
  int iomp = 0;
#endif
  int mode = 0;
  struct clCacheType *cache = clCache + iomp;
  float Tlog = log(1.0e-10+fabs(T_g)), cfun, hfun;
  cooling_t ret;

  cart_assert(iomp < GH12_MAX_NUM_THREADS);

  if(Zlog!=cache->Zlog || nHlog!=cache->nHlog)
    {
      int err;
      float w;

      cache->Zlog = Zlog;
      cache->nHlog = nHlog;
      cache->Z = pow(10.0,Zlog);
      w = 1.4/(1.0-0.02*cache->Z);
      cache->nb = pow(10.0,nHlog)*w;
      cache->fac = w*w;
      
      frtcfcache_(&cache->nb,&cache->Z,&clCfg.Plw,&clCfg.Ph1,&clCfg.Pg1,&clCfg.Pc6,cache->icache,cache->rcache,&err);
    }

  frtcfgetln_(&mode,&Tlog,cache->icache,cache->rcache,&cfun,&hfun);

  ret.Cooling = cfun*cache->fac*clFac;
  ret.Heating = hfun*cache->fac*clFac;

  return ret;
}


double cooling_fion( double nHlog, double T_g, double Zlog )
{
  cart_error("NOT IMPLEMENTED.");
  return 0;
}


#endif /* COOLING && !RADIATIVE_TRANSFER && COOLING_GH12 */


