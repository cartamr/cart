#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <math.h>

#include "auxiliary.h"
#include "cell_buffer.h"
#include "control_parameter.h"
#include "cosmology.h"
#include "hydro.h"
#include "hydro_tracer.h"
#include "index_hash.h"
#include "io.h"
#include "io_artio.h"
#include "io_cart.h"
#include "iterators.h"
#include "lb.h"
#include "load_predictor.h"
#include "parallel.h"
#include "particle.h"
#include "plugin.h"
#include "refinement.h"
#include "rt_transfer.h"
#include "sfc.h"
#include "starformation.h"
#include "system.h"
#include "timing.h"
#include "times.h"
#include "tree.h"
#include "units.h"

#ifdef RADIATIVE_TRANSFER
#include "frt/frt_c.h"
#endif /* RADIATIVE_TRANSFER */

#include "../tools/artio/artio.h"

extern int restart_frequency;
extern int current_restart_backup;
extern int num_backup_restarts;

DECLARE_LEVEL_ARRAY(double,dtl);
DECLARE_LEVEL_ARRAY(double,tl_old);
DECLARE_LEVEL_ARRAY(double,dtl_old);
DECLARE_LEVEL_ARRAY(int,level_sweep_dir);
DECLARE_LEVEL_ARRAY(int,time_refinement_factor);
DECLARE_LEVEL_ARRAY(int,time_refinement_factor_old);

#ifdef COSMOLOGY
DECLARE_LEVEL_ARRAY(double,abox_old);
#endif

size_t artio_buffer_size = 0;
int num_artio_grid_files = 1;
int artio_grid_allocation_strategy = ARTIO_ALLOC_EQUAL_SFC;
int artio_create_fileset_directory = 0;

#if defined(PARTICLES) || defined(HYDRO_TRACERS)
int num_artio_particle_files = 1;
int artio_particle_allocation_strategy = ARTIO_ALLOC_EQUAL_SFC;
#endif /* PARTICLES */

#define artio_num_label_digits_min  1
#define artio_num_label_digits_max 16

#ifdef COSMOLOGY
int artio_num_label_digits = 4;
extern int grid_output_frequency;
#ifdef PARTICLES
extern int particle_output_frequency;
#endif /* PARTICLES */
#ifdef HYDRO_TRACERS
extern int tracer_output_frequency;
#endif /* HYDRO_TRACERS */
#else
int artio_num_label_digits = 6;
#endif /* COSMOLOGY */


int num_extra_variables = 0;
int num_missing_variables = 0;
#ifdef ALLOW_MISSING_FIELDS_RESTART
#define num_missing_variables_max 128
char *missing_var_labels[num_missing_variables_max];
#define num_extra_variables_max 128
char *extra_var_labels[num_missing_variables_max];
#endif /* ALLOW_MISSING_FIELDS_RESTART */

void artio_restart_load_balance( artio_fileset *handle );

void control_parameter_set_allocation_strategy(const char *value, void *ptr, int ind) {
	if ( strcmp( value, "ARTIO_ALLOC_EQUAL_SFC" ) == 0 ) {
		*(int *)ptr = ARTIO_ALLOC_EQUAL_SFC;
	} else if ( strcmp( value, "ARTIO_ALLOC_EQUAL_PROC" ) == 0 ) {
		*(int *)ptr = ARTIO_ALLOC_EQUAL_PROC;
#ifdef ARTIO_POSIX
	} else if ( strcmp( value, "ARTIO_ALLOC_ONE_TO_ONE" ) == 0 ) {
		*(int *)ptr = ARTIO_ALLOC_ONE_TO_ONE;
#endif /* ARTIO_POSIX */
	} else {
		cart_error("Invalid allocation strategy %s", value );
	}
}

void control_parameter_list_allocation_strategy(FILE *stream, const void *ptr) {
	switch( *(int *)ptr ) {
		case ARTIO_ALLOC_EQUAL_SFC :
			fprintf(stream,"ARTIO_ALLOC_EQUAL_SFC");
			break;
		case ARTIO_ALLOC_EQUAL_PROC :
			fprintf(stream,"ARTIO_ALLOC_EQUAL_PROC");
			break;
#ifdef ARTIO_POSIX
		case ARTIO_ALLOC_ONE_TO_ONE :
			fprintf(stream,"ARTIO_ALLOC_ONE_TO_ONE");
			break;
#endif /* ARTIO_POSIX */
		default :
			fprintf(stream,"UNKNOWN");
	}
}

void config_init_io_artio() {
	ControlParameterOps control_parameter_allocation_strategy = {
		control_parameter_set_allocation_strategy,
		control_parameter_list_allocation_strategy };

	control_parameter_add2(control_parameter_size,&artio_buffer_size,"io:artio-buffer-size","artio_buffer_size","Size of internal buffer used by artio to batch read/write requests.");

	num_artio_grid_files = num_procs;
	control_parameter_add3(control_parameter_int,&num_artio_grid_files,"io:num-grid-files","num_grid_files","io:num-files","Number of output grid files. Defaults to the number of MPI tasks.");
	control_parameter_add3(control_parameter_allocation_strategy,&artio_grid_allocation_strategy,"io:grid-file-allocation-strategy","grid_allocation_strategy","io:file-allocation-strategy","Determine how root cells are divided amongst the output files.  Supported options: ARTIO_ALLOC_EQUAL_SFC, ARTIO_ALLOC_EQUAL_PROC, ARTIO_ALLOC_ONE_TO_ONE");

	control_parameter_add2(control_parameter_bool,&artio_create_fileset_directory,"io:artio-create-fileset-directory","artio_create_fileset_directory","When enabled will group artio outputs into subdirectories of output_directory (boolean value)");

#if defined(PARTICLES) || defined(HYDRO_TRACERS)
	num_artio_particle_files = num_procs;
	control_parameter_add3(control_parameter_int,&num_artio_particle_files,"io:num-particle-files","num_particle_files","io:num-files","Number of output particle files.  Defaults to the number of MPI tasks.");
	control_parameter_add3(control_parameter_allocation_strategy,&artio_particle_allocation_strategy,"io:particle-file-allocation-strategy","particle_allocation_strategy","io:file-allocation-strategy","Determine how root cells are divided amongst the output files.  Supported options: ARTIO_ALLOC_EQUAL_SFC, ARTIO_ALLOC_EQUAL_PROC, ARTIO_ALLOC_ONE_TO_ONE");
#endif /* PARTICLES || HYDRO_TRACERS */

	control_parameter_add2(control_parameter_int,&artio_num_label_digits,"io:num-label-digits","artio_num_label_digits","Number of significant digits in filename label.");
}

void config_verify_io_artio() {
	VERIFY(io:artio-buffer-size, 1 );
	if ( control_parameter_is_set("io:artio-buffer-size") ) {
		artio_fileset_set_buffer_size(artio_buffer_size);
	}

	VERIFY(io:num-grid-files, num_artio_grid_files > 0 && num_artio_grid_files < max_sfc_index );
#ifdef ARTIO_POSIX
	VERIFY(io:grid-file-allocation-strategy, artio_grid_allocation_strategy == ARTIO_ALLOC_EQUAL_SFC || artio_grid_allocation_strategy == ARTIO_ALLOC_EQUAL_PROC || artio_grid_allocation_strategy == ARTIO_ALLOC_ONE_TO_ONE );
#else
	VERIFY(io:grid-file-allocation-strategy, artio_grid_allocation_strategy == ARTIO_ALLOC_EQUAL_SFC || artio_grid_allocation_strategy == ARTIO_ALLOC_EQUAL_PROC );
#endif

	VERIFY(io:artio-create-fileset-directory, artio_create_fileset_directory == 0 || artio_create_fileset_directory == 1 );

#if defined(PARTICLES) || defined(HYDRO_TRACERS)
	VERIFY(io:num-particle-files, num_artio_particle_files > 0 && num_artio_particle_files < max_sfc_index );
#ifdef ARTIO_POSIX
	VERIFY(io:particle-file-allocation-strategy, artio_particle_allocation_strategy == ARTIO_ALLOC_EQUAL_SFC || artio_particle_allocation_strategy == ARTIO_ALLOC_EQUAL_PROC || artio_particle_allocation_strategy == ARTIO_ALLOC_ONE_TO_ONE );
#else
	VERIFY(io:particle-file-allocation-strategy, artio_particle_allocation_strategy == ARTIO_ALLOC_EQUAL_SFC || artio_particle_allocation_strategy == ARTIO_ALLOC_EQUAL_PROC );
#endif
#endif /* PARTICLES || HYDRO_TRACERS */

	VERIFY(io:num-label-digits, artio_num_label_digits >= artio_num_label_digits_min && artio_num_label_digits <= artio_num_label_digits_max );
}

void read_artio_grid(artio_fileset *handle, int file_max_level);
void write_artio_grid(artio_fileset *handle);

#if defined(PARTICLES) || defined(HYDRO_TRACERS)
void read_artio_particles(artio_fileset *handle);
void write_artio_particles(artio_fileset *handle, int fileset_options);
#endif /* PARTICLES || HYDRO_TRACERS */

void define_file_variables(int *num_variables, char *variable_labels[num_vars],
		int variable_indices[num_vars]) {
	int j;

#define add_variable(label,index)    \
		variable_labels[*num_variables] = cart_alloc(char, strlen(label)+1); \
		strcpy( variable_labels[*num_variables], label ); \
		variable_indices[*num_variables] = index; \
		(*num_variables)++;

	*num_variables = 0;

#ifdef HYDRO
	add_variable( "HVAR_GAS_DENSITY", HVAR_GAS_DENSITY );
	add_variable( "HVAR_GAS_ENERGY", HVAR_GAS_ENERGY );
	add_variable( "HVAR_PRESSURE", HVAR_PRESSURE );
	add_variable( "HVAR_GAMMA", HVAR_GAMMA );
	add_variable( "HVAR_INTERNAL_ENERGY", HVAR_INTERNAL_ENERGY );
	add_variable( "HVAR_MOMENTUM_X", HVAR_MOMENTUM+0 );
	add_variable( "HVAR_MOMENTUM_Y", HVAR_MOMENTUM+1 );
	add_variable( "HVAR_MOMENTUM_Z", HVAR_MOMENTUM+2 );
#ifdef ELECTRON_ION_NONEQUILIBRIUM
	add_variable( "HVAR_ELECTRON_INTERNAL_ENERGY", HVAR_ELECTRON_INTERNAL_ENERGY );
#endif /* ELECTRON_ION_NONEQUILIBRIUM */
#ifdef RADIATIVE_TRANSFER
	add_variable( "RT_HVAR_HI", RT_HVAR_OFFSET+0 );
	add_variable( "RT_HVAR_HII", RT_HVAR_OFFSET+1 );
	add_variable( "RT_HVAR_HeI", RT_HVAR_OFFSET+2 );
	add_variable( "RT_HVAR_HeII", RT_HVAR_OFFSET+3 );
	add_variable( "RT_HVAR_HeIII", RT_HVAR_OFFSET+4 );
	add_variable( "RT_HVAR_H2", RT_HVAR_OFFSET+5 );
#endif /* RADIATIVE_TRANSFER */
#ifdef ENRICHMENT
	add_variable( "HVAR_METAL_DENSITY_II", HVAR_METAL_DENSITY_II );
#ifdef ENRICHMENT_SNIa
	add_variable( "HVAR_METAL_DENSITY_Ia", HVAR_METAL_DENSITY_Ia );
#endif /* ENRICHMENT_SNIa */
#ifdef DUST_EVOLUTION
	add_variable( "HVAR_DUST_DENSITY", HVAR_DUST_DENSITY );
#endif /* DUST_EVOLUTION */
#ifdef ENRICHMENT_ELEMENTS
	add_variable( "HVAR_METAL_DENSITY_AGB", HVAR_METAL_DENSITY_AGB );
	add_variable( "HVAR_METAL_DENSITY_C", HVAR_METAL_DENSITY_C );
	add_variable( "HVAR_METAL_DENSITY_N", HVAR_METAL_DENSITY_N );
	add_variable( "HVAR_METAL_DENSITY_O", HVAR_METAL_DENSITY_O );
	add_variable( "HVAR_METAL_DENSITY_Mg", HVAR_METAL_DENSITY_Mg );
	add_variable( "HVAR_METAL_DENSITY_S", HVAR_METAL_DENSITY_S );
	add_variable( "HVAR_METAL_DENSITY_Ca", HVAR_METAL_DENSITY_Ca );
	add_variable( "HVAR_METAL_DENSITY_Fe", HVAR_METAL_DENSITY_Fe );
#endif /* ENRICHMENT_ELEMENTS */
#endif /* ENRICHMENT */
#ifdef BLASTWAVE_FEEDBACK
	add_variable( "HVAR_BLASTWAVE_TIME", HVAR_BLASTWAVE_TIME );
#endif /* BLASTWAVE_FEEDBACK */
#ifdef INERT_GAS_TRACER
	add_variable( "HVAR_INERT_GAS_TRACER", HVAR_INERT_GAS_TRACER );
	for ( j = 1; j < num_inert_gas_tracers; j++ ) {
		variable_labels[*num_variables] = cart_alloc(char, 32);
		snprintf(variable_labels[*num_variables], 32, "HVAR_INERT_GAS_TRACER_%d", j );
		variable_indices[*num_variables] = HVAR_INERT_GAS_TRACER + j;
		(*num_variables)++;
	}
#endif /* INERT_GAS_TRACER */
#ifdef SGS_TURBULENCE
	add_variable( "HVAR_GAS_TURBULENT_ENERGY", HVAR_GAS_TURBULENT_ENERGY );
#endif /* SGS_TURBULENCE */
#ifdef EXTRA_PRESSURE_SOURCE
	add_variable( "VAR_EXTRA_PRESSURE_SOURCE", VAR_EXTRA_PRESSURE_SOURCE );
#endif
#ifdef SGST_SHEAR_IMPROVED
	add_variable( "HVAR_SGST_MEAN_VELOCITY_X", HVAR_SGST_MEAN_VELOCITY+0 );
	add_variable( "HVAR_SGST_MEAN_VELOCITY_Y", HVAR_SGST_MEAN_VELOCITY+1 );
	add_variable( "HVAR_SGST_MEAN_VELOCITY_Z", HVAR_SGST_MEAN_VELOCITY+2 );
#endif /* SGST_SHEAR_IMPROVED */
#ifdef COSMIC_RAYS
	for( j = 0; j < cr_num_bins; j++ ) {
		variable_labels[*num_variables] = cart_alloc(char, 32);
		snprintf(variable_labels[*num_variables], 32, "COSMIC_RAYS_BIN_%d_DENSITY", j );
		variable_indices[*num_variables] = HVAR_COSMIC_RAYS_BIN_DENSITY + j;
		(*num_variables)++;
	}
	for( j = 0; j < cr_num_bins; j++ ) {
		variable_labels[*num_variables] = cart_alloc(char, 32);
		snprintf(variable_labels[*num_variables], 32, "COSMIC_RAYS_BIN_%d_ENERGY", j );
		variable_indices[*num_variables] = HVAR_COSMIC_RAYS_BIN_ENERGY + j;
		(*num_variables)++;
	}
#endif /* COSMIC_RAYS */
#endif /* HYDRO */

#ifdef GRAVITY
	add_variable( "VAR_POTENTIAL", VAR_POTENTIAL );
#ifdef HYDRO
	add_variable( "VAR_POTENTIAL_HYDRO", VAR_POTENTIAL_HYDRO );
#endif /* HYDRO */
#endif /* GRAVITY */

#ifdef RADIATIVE_TRANSFER
	for( j = 0; j < rt_num_disk_vars; j++ ) {
		variable_labels[*num_variables] = cart_alloc(char, 64);
		snprintf(variable_labels[*num_variables], 64, "RT_DISK_VAR_%d", j );
		variable_indices[*num_variables] = rt_disk_offset + j;
		(*num_variables)++;
	}
#endif /* RADIATIVE_TRANSFER */

#undef add_variable
}

void get_star_secondary_var_idxs(int *num_star_secondary_vars, int *idx_basic,
								 int *idx_enrich, int *idx_enrich_Ia, 
								 int *idx_enrich_agb, int *idx_discrete, 
								 int *idx_cluster, int *idx_cluster_bound_fraction, 
								 int *idx_cluster_debug, int *idx_cluster_initial_bound) {
	/* This function parses the defines to determine how many secondary 
	   variables star particles have, and what they are. The indices determining
	   where different aspects start will be determined too. These are returned
	   by modifying the pointers to integers passed in. If a particular variable
	   does not exist, the index for it will be -1. */

	/* start the counter for how many variables we have */
	int num_temp_vars = 0;

	/* star formation is required. */
	#ifdef STAR_FORMATION
		/* birth time, initial mass, and mass are the first ones and always present */
		*idx_basic = num_temp_vars;
		num_temp_vars += 3;

		/* Next is ENRICHMENT, which only contains the SN II metallicity */
		#ifdef ENRICHMENT
			*idx_enrich = num_temp_vars;
			num_temp_vars += 1;
		#else
			*idx_enrich = -1;
		#endif /* ENRICHMENT */

		/* Then Type Ia enrichment */
		#ifdef ENRICHMENT_SNIa
			*idx_enrich_Ia = num_temp_vars;
			num_temp_vars += 1;
		#else
			*idx_enrich_Ia = -1;
		#endif /* ENRICHMENT_SNIa */

		// Then the new enrichment, which has 8: Z_AGB, C, N, O, Mg, S, Ca, Fe
		#ifdef ENRICHMENT_ELEMENTS
			*idx_enrich_agb = num_temp_vars;
			num_temp_vars += 8;
		#else
			*idx_enrich_agb = -1;
		#endif /* ENRICHMENT_ELEMENTS */

		// Then the SN counter
		#ifdef DISCRETE_SN
			*idx_discrete = num_temp_vars;
			num_temp_vars += 1;
		#else
			*idx_discrete = -1;
		#endif /* DISCRETE_SN */

		/* Then cluster star formation with 3: termination time, average age, metal dispersion */
		#ifdef CLUSTER
			*idx_cluster = num_temp_vars;
			num_temp_vars += 3;
		#else
			*idx_cluster = -1;
		#endif /* CLUSTER */

		/* Cluster bound fraction */
		#ifdef CLUSTER_BOUND_FRACTION
			*idx_cluster_bound_fraction = num_temp_vars;
			num_temp_vars += 1;
		#else
			*idx_cluster_bound_fraction = -1;
		#endif /* CLUSTER_BOUND_FRACTION */

		/* Cluster debug has the age spread */
		#ifdef CLUSTER_DEBUG
			*idx_cluster_debug = num_temp_vars;
			num_temp_vars += 1;
		#else
			*idx_cluster_debug = -1;
		#endif /* CLUSTER_DEBUG */

		/* Cluster initial bound fraction */
		#ifdef CLUSTER_INITIAL_BOUND
			*idx_cluster_initial_bound = num_temp_vars;
			num_temp_vars += 1;
		#else
			*idx_cluster_initial_bound = -1;
		#endif /* CLUSTER_INITIAL_BOUND */
	#else /* STAR_FORMATION */
		*idx_basic = -1;
		*idx_enrich = -1;
		*idx_enrich_Ia = -1;
		*idx_enrich_agb = -1;
		*idx_cluster = -1;
		*idx_cluster_bound_fraction = -1;
		*idx_cluster_debug = -1;
		*idx_cluster_initial_bound = -1;
	#endif /* STAR_FORMATION */

	/* then assign the total number of variables */
	*num_star_secondary_vars = num_temp_vars;
}


#if defined(PARTICLES) || defined(HYDRO_TRACERS)
void define_particle_variables( int *pnum_file_species, char ***pspecies_labels,
		int **pnum_primary_variables, char ****pprimary_variable_labels,
		int **pnum_secondary_variables, char ****psecondary_variable_labels,
		int fileset_options ) {

	int i, j;
	const char dim[] = { 'X', 'Y', 'Z' };

	int num_file_species = 0;

#ifdef PARTICLES
	int num_nbody_species;

	if ( fileset_options & WRITE_PARTICLES ) {
		num_file_species += num_particle_species;

#ifdef STAR_FORMATION
		num_nbody_species = num_particle_species - 1;
#else
		num_nbody_species = num_particle_species;
#endif /* STAR_FORMATION */
	}
#endif /* PARTICLES */

#ifdef HYDRO_TRACERS
	if ( fileset_options & WRITE_TRACERS ) {
		num_file_species++;
	}
#endif /* HYDRO_TRACERS */

	char **species_labels = cart_alloc(char *, num_file_species);
	int *num_primary_variables = cart_alloc(int, num_file_species);
	int *num_secondary_variables = cart_alloc(int, num_file_species);
	char ***primary_variable_labels = cart_alloc(char **, num_file_species);
	char ***secondary_variable_labels = cart_alloc(char **, num_file_species);

#ifdef PARTICLES
	if ( fileset_options & WRITE_PARTICLES ) {
		for ( i = 0; i < num_nbody_species; i++ ) {
			species_labels[i] = cart_alloc(char, 64);
			snprintf(species_labels[i], 64, "N-BODY");
			num_primary_variables[i] = 2*nDim+1;
			num_secondary_variables[i] = 0;
		}

#ifdef STAR_FORMATION
		species_labels[num_nbody_species] = cart_alloc(char, 64);
		snprintf(species_labels[num_nbody_species], 64, "STAR");
		num_primary_variables[num_nbody_species] = 2*nDim+1;

	/* set up counters and variables for indexes */
	int num_secondary_vars_temp;
	int idx_basic, idx_enrich, idx_enrich_Ia, idx_enrich_agb, idx_discrete;
	int idx_cluster, idx_cluster_bound_fraction, idx_cluster_debug, idx_cluster_initial_bound;
	/* Then initialize them. */
	get_star_secondary_var_idxs(&num_secondary_vars_temp,
	                            &idx_basic, &idx_enrich, &idx_enrich_Ia, 
								&idx_enrich_agb, &idx_discrete,
								&idx_cluster, &idx_cluster_bound_fraction,
								&idx_cluster_debug, &idx_cluster_initial_bound);

	num_secondary_variables[num_nbody_species] = num_secondary_vars_temp;

	/* this part of the code makes the labels for the variables.
	secondary_variable_labels is a char array passed into this function */ 
                secondary_variable_labels[num_nbody_species] = cart_alloc(char *, num_secondary_variables[num_nbody_species]);

		secondary_variable_labels[num_nbody_species][idx_basic] = cart_alloc(char, 64);
		snprintf(secondary_variable_labels[num_nbody_species][idx_basic], 64, "BIRTH_TIME");

		secondary_variable_labels[num_nbody_species][idx_basic+1] = cart_alloc(char, 64);
		snprintf(secondary_variable_labels[num_nbody_species][idx_basic+1], 64, "INITIAL_MASS");

		secondary_variable_labels[num_nbody_species][idx_basic+2] = cart_alloc(char, 64);
		snprintf(secondary_variable_labels[num_nbody_species][idx_basic+2], 64, "MASS");

#ifdef ENRICHMENT
		secondary_variable_labels[num_nbody_species][idx_enrich] = cart_alloc(char, 64);
		snprintf(secondary_variable_labels[num_nbody_species][idx_enrich], 64, "METALLICITY_SNII");
#ifdef ENRICHMENT_SNIa
		secondary_variable_labels[num_nbody_species][idx_enrich_Ia] = cart_alloc(char, 64);
		snprintf(secondary_variable_labels[num_nbody_species][idx_enrich_Ia], 64, "METALLICITY_SNIa");
#endif /* ENRICHMENT_SNIa */
#ifdef ENRICHMENT_ELEMENTS
		secondary_variable_labels[num_nbody_species][idx_enrich_agb] = cart_alloc(char, 64);
		snprintf(secondary_variable_labels[num_nbody_species][idx_enrich_agb], 64, "METALLICITY_AGB");

		secondary_variable_labels[num_nbody_species][idx_enrich_agb+1] = cart_alloc(char, 64);
		snprintf(secondary_variable_labels[num_nbody_species][idx_enrich_agb+1], 64, "METALLICITY_C");

		secondary_variable_labels[num_nbody_species][idx_enrich_agb+2] = cart_alloc(char, 64);
		snprintf(secondary_variable_labels[num_nbody_species][idx_enrich_agb+2], 64, "METALLICITY_N");

		secondary_variable_labels[num_nbody_species][idx_enrich_agb+3] = cart_alloc(char, 64);
		snprintf(secondary_variable_labels[num_nbody_species][idx_enrich_agb+3], 64, "METALLICITY_O");
		
		secondary_variable_labels[num_nbody_species][idx_enrich_agb+4] = cart_alloc(char, 64);
		snprintf(secondary_variable_labels[num_nbody_species][idx_enrich_agb+4], 64, "METALLICITY_Mg");

		secondary_variable_labels[num_nbody_species][idx_enrich_agb+5] = cart_alloc(char, 64);
		snprintf(secondary_variable_labels[num_nbody_species][idx_enrich_agb+5], 64, "METALLICITY_S");

		secondary_variable_labels[num_nbody_species][idx_enrich_agb+6] = cart_alloc(char, 64);
		snprintf(secondary_variable_labels[num_nbody_species][idx_enrich_agb+6], 64, "METALLICITY_Ca");

		secondary_variable_labels[num_nbody_species][idx_enrich_agb+7] = cart_alloc(char, 64);
		snprintf(secondary_variable_labels[num_nbody_species][idx_enrich_agb+7], 64, "METALLICITY_Fe");
#endif /* ENRICHMENT_ELEMENTS */
#endif /* ENRICHMENT */

#ifdef DISCRETE_SN
		secondary_variable_labels[num_nbody_species][idx_discrete] = cart_alloc(char, 64);
		snprintf(secondary_variable_labels[num_nbody_species][idx_discrete], 64, "UNEXPLODED_SN");
#endif /* DISCRETE_SN */

#ifdef CLUSTER
		secondary_variable_labels[num_nbody_species][idx_cluster] = cart_alloc(char, 64);
		snprintf(secondary_variable_labels[num_nbody_species][idx_cluster], 64, "TERMINATION_TIME");

		secondary_variable_labels[num_nbody_species][idx_cluster+1] = cart_alloc(char, 64);
		snprintf(secondary_variable_labels[num_nbody_species][idx_cluster+1], 64, "AVERAGE_AGE");

		secondary_variable_labels[num_nbody_species][idx_cluster+2] = cart_alloc(char, 64);
		snprintf(secondary_variable_labels[num_nbody_species][idx_cluster+2], 64, "METAL_DISPERSION");

#ifdef CLUSTER_BOUND_FRACTION
		secondary_variable_labels[num_nbody_species][idx_cluster_bound_fraction] = cart_alloc(char, 64);
		snprintf(secondary_variable_labels[num_nbody_species][idx_cluster_bound_fraction], 64, "BOUND_FRACTION");
#endif /* CLUSTER_BOUND_FRACTION */

#ifdef CLUSTER_DEBUG
		secondary_variable_labels[num_nbody_species][idx_cluster_debug] = cart_alloc(char, 64);
		snprintf(secondary_variable_labels[num_nbody_species][idx_cluster_debug], 64, "AGE_SPREAD");
#endif /* CLUSTER_DEBUG */

#ifdef CLUSTER_INITIAL_BOUND
		secondary_variable_labels[num_nbody_species][idx_cluster_initial_bound] = cart_alloc(char, 64);
		snprintf(secondary_variable_labels[num_nbody_species][idx_cluster_initial_bound], 64, "INITIAL_BOUND_FRACTION");
#endif /* CLUSTER_INITIAL_BOUND */
#endif /* CLUSTER */

#endif /* STAR_FORMATION */

		/* copy primary variable names */
		for ( i = 0; i < num_particle_species; i++ ) {
			primary_variable_labels[i] = cart_alloc(char *, num_primary_variables[i]);
			for ( j = 0; j < nDim; j++ ) {
				primary_variable_labels[i][j] = cart_alloc(char, 64);
				snprintf(primary_variable_labels[i][j], 64, "POSITION_%c", dim[j]);
			}

			for ( j = 0; j < nDim; j++ ) {
				primary_variable_labels[i][nDim+j] = cart_alloc(char, 64);
				snprintf(primary_variable_labels[i][nDim+j], 64, "VELOCITY_%c", dim[j]);
			}

			primary_variable_labels[i][2*nDim] = cart_alloc(char, 64);
			snprintf(primary_variable_labels[i][2*nDim], 64, "TIMESTEP");
		}
	}
#endif /* PARTICLES */

#ifdef HYDRO_TRACERS
	if ( fileset_options & WRITE_TRACERS ) {
		cart_assert(num_file_species > 0);
		species_labels[num_file_species-1] = cart_alloc(char, 64);
		snprintf(species_labels[num_file_species-1], 64, "HYDRO-TRACER");

		num_primary_variables[num_file_species-1] = nDim; /* position */
		num_secondary_variables[num_file_species-1] = num_hydro_vars_traced;

		primary_variable_labels[num_file_species-1] = cart_alloc(char *, num_primary_variables[num_file_species-1]);
		for ( j = 0; j < nDim; j++ ) {
			primary_variable_labels[num_file_species-1][j] = cart_alloc(char, 64);
			snprintf(primary_variable_labels[num_file_species-1][j], 64, "POSITION_%c", dim[j]);
		}

		secondary_variable_labels[num_file_species-1] = cart_alloc(char *, num_secondary_variables[num_file_species-1]);
		for ( j = 0; j < num_hydro_vars_traced; j++ ) {
			secondary_variable_labels[num_file_species-1][j] = cart_alloc(char, 64);
			snprintf(secondary_variable_labels[num_file_species-1][j], 64, hydro_vars_traced_labels[j]);
		}
	}
#endif /* HYDRO_TRACERS */

	*pnum_file_species = num_file_species;
	*pspecies_labels = species_labels;
	*pnum_primary_variables = num_primary_variables;
	*pnum_secondary_variables = num_secondary_variables;
	*pprimary_variable_labels = primary_variable_labels;
	*psecondary_variable_labels = secondary_variable_labels;
}
#endif /* PARTICLES || TRACERS */

void pack_cell_vars(int icell, int num_pack_vars, int *var_indices,
		float *variables) {
	int i;

#pragma novector
	for (i = 0; i < num_pack_vars; i++) {
		variables[i] = cell_var(icell, var_indices[i]);
	}
}

void unpack_cell_vars(int icell, int num_sim_vars, int *sim_var_indices,
		int *file_var_indices, float *variables) {
	int i;

	for (i = 0; i < num_sim_vars; i++) {
#ifdef ALLOW_MISSING_FIELDS_RESTART
		cell_var(icell, sim_var_indices[i]) = ( file_var_indices[i] < 0 ) ? 0.0 : variables[file_var_indices[i]];
#else
		cell_var(icell, sim_var_indices[i]) = variables[file_var_indices[i]];
#endif /* ALLOW_MISSING_FIELDS_RESTART */

	}
}

void create_artio_filename( int filename_flag, char *label, char *filename ) {
	char dir[256];

	switch(filename_flag) {
		case WRITE_SAVE:
			if ( artio_create_fileset_directory ) {
				sprintf( dir, "%s/%s_%s/", output_directory, jobname, label );
				if ( system_mkdir( dir ) ) {
					cart_error("Unable to create directory %s for artio fileset!", dir );
				}
				sprintf( filename, "%s/%s_%s", dir, jobname, label );
			} else {
				sprintf( filename, "%s/%s_%s", output_directory, jobname, label );
			}
			break;
		case WRITE_BACKUP:
			sprintf( filename, "%s/%s_%d", output_directory, jobname, current_restart_backup );
			break;
		default:
			cart_error("Unknown filename_flag in create_artio_filename! %d", filename_flag );
	}
}

void write_artio_restart_worker( char *filename, int fileset_write_options );

void write_artio_restart( int grid_filename_flag, int particle_filename_flag, int tracer_filename_flag ) {
	char label[256];
	char filename[256];
	int fileset_options = 0;

#ifdef COSMOLOGY
	int min_frequency,artio_num_label_digits_needed;
	double a_dif;
	min_frequency = MIN( restart_frequency, grid_output_frequency );
#ifdef PARTICLES
	min_frequency = MIN( particle_output_frequency, min_frequency );
#endif /* PARTICLES */
#ifdef HYDRO_TRACERS
	min_frequency = MIN( tracer_output_frequency, min_frequency );
#endif /* HYDRO_TRACERS */
	if ( min_frequency > 0 ) {
		a_dif = auni_from_tcode(tl[min_level]+min_frequency*dtl[min_level])-auni[min_level];
		artio_num_label_digits_needed = (int)( ceil( -log10( a_dif ) ) );
		artio_num_label_digits_needed = MAX( artio_num_label_digits_min , artio_num_label_digits_needed );
		if ( artio_num_label_digits < artio_num_label_digits_needed ) {
			cart_debug("WARNING! Number of significant digits in filename label is probably too small to create unique filename label for the next output fileset. io:num-label-digits = %d, da ~ %.2e.", artio_num_label_digits, a_dif );
			//artio_num_label_digits = artio_num_label_digits_needed;
		} else if ( artio_num_label_digits > artio_num_label_digits_max ) {
			cart_debug("WARNING! Timestep is probably too short to create unique filename label for the next output fileset. io:num-label-digits = %d, da ~ %.2e.", artio_num_label_digits, a_dif );
			//artio_num_label_digits = artio_num_label_digits_max;
		}
	}
	sprintf(label,"a%0*.*f",artio_num_label_digits+2,artio_num_label_digits,auni[min_level]);
#else
	sprintf(label,"%0*d",artio_num_label_digits,step);
#endif /* COSMOLOGY */

	/* write backup */
	if ( grid_filename_flag == WRITE_BACKUP ||
			particle_filename_flag == WRITE_BACKUP ||
			tracer_filename_flag == WRITE_BACKUP ) {
		current_restart_backup = (current_restart_backup+1) % num_restart_backups;
		fileset_options = ((grid_filename_flag == WRITE_BACKUP) ? WRITE_GRID : 0) |
							((particle_filename_flag == WRITE_BACKUP) ? WRITE_PARTICLES: 0) |
							((tracer_filename_flag == WRITE_BACKUP) ? WRITE_TRACERS: 0);
		create_artio_filename(WRITE_BACKUP, label, filename);
		write_artio_restart_worker(filename, fileset_options);
	}

	/* write save */
	if ( grid_filename_flag == WRITE_SAVE ||
			particle_filename_flag == WRITE_SAVE ||
			tracer_filename_flag == WRITE_SAVE ) {
		fileset_options = ((grid_filename_flag == WRITE_SAVE) ? WRITE_GRID : 0) |
							((particle_filename_flag == WRITE_SAVE) ? WRITE_PARTICLES: 0) |
							((tracer_filename_flag == WRITE_SAVE) ? WRITE_TRACERS: 0);
		create_artio_filename(WRITE_SAVE, label, filename);
		write_artio_restart_worker(filename, fileset_options);
	}
}

void write_artio_restart_worker( char *filename, int fileset_write_options ) {
	int i, j;
	artio_fileset *handle;
	int num_levels;
	FILE *restart;
	char restart_filename[256];
	char str[ARTIO_MAX_STRING_LENGTH];
#ifdef RADIATIVE_TRANSFER
	frt_intg n;
	frt_real *data;
	float *buffer;
#endif /* RADIATIVE_TRANSFER */
	artio_context con = { mpi.comm.run };

	if ( fileset_write_options & WRITE_GRID || fileset_write_options & WRITE_PARTICLES || fileset_write_options & WRITE_TRACERS ) {
		cart_debug("creating parallel file %s jobname %s", filename, jobname );
		handle = artio_fileset_create( filename, SFC,
				num_root_cells, num_cells_per_level[min_level], &con );
		if ( handle == NULL ) {
			cart_error("Unable to create parallel file %s", filename );
		}

		/* write header variables */
		num_levels = max_level_now_global(mpi.comm.run) - min_level + 1;

		artio_parameter_set_string( handle, "jobname", (char *)jobname );

		/* timestepping variables */
		artio_parameter_set_int( handle, "step", step );
		artio_parameter_set_double_array( handle, "tl", num_levels, tl );
		artio_parameter_set_double_array( handle, "tl_old", num_levels, tl_old );
		artio_parameter_set_double_array( handle, "dtl", num_levels, dtl );
		artio_parameter_set_double_array( handle, "dtl_old", num_levels, dtl_old );

#ifdef COSMOLOGY
		artio_parameter_set_double( handle, "auni_init", auni_init );
		artio_parameter_set_double( handle, "auni_end", auni_end );
#else
		artio_parameter_set_double( handle, "t_init", t_init );
		artio_parameter_set_double( handle, "t_end", t_end );
#endif

		artio_parameter_set_int_array( handle, "time_refinement_factor", num_levels, time_refinement_factor );
		artio_parameter_set_int_array( handle, "time_refinement_factor_old", num_levels, time_refinement_factor_old );

#ifdef HYDRO
		artio_parameter_set_int_array( handle, "hydro_sweep_direction", num_levels, level_sweep_dir );
#endif

		artio_parameter_set_int( handle, "current_restart_backup", current_restart_backup );

		/* unit parameters */
		artio_parameter_set_double( handle, "box_size", box_size );
#ifdef COSMOLOGY
		artio_parameter_set_double( handle, "OmegaM", cosmology->OmegaM );
		artio_parameter_set_double( handle, "OmegaL", cosmology->OmegaL );
		artio_parameter_set_double( handle, "OmegaB", cosmology->OmegaB );
		artio_parameter_set_double( handle, "hubble", cosmology->h );
		artio_parameter_set_double( handle, "DeltaDC", cosmology->DeltaDC );

		artio_parameter_set_double_array( handle, "abox", num_levels, abox );
		artio_parameter_set_double_array( handle, "auni", num_levels, auni );

		artio_parameter_set_double( handle, "Hbox", Hubble(abox[min_level]) );
#endif /* COSMOLOGY */

		/* write unit conversion factors independent of cosmology flag */
		artio_parameter_set_double( handle, "mass_unit", primary_units->mass );
		artio_parameter_set_double( handle, "time_unit", primary_units->time );
		artio_parameter_set_double( handle, "length_unit", primary_units->length );

#ifdef PARTICLES
		/* energy conservation variables */
		artio_parameter_set_double( handle, "energy:tintg", tintg );
		artio_parameter_set_double( handle, "energy:ekin", ekin );
		artio_parameter_set_double( handle, "energy:ekin1", ekin1 );
		artio_parameter_set_double( handle, "energy:au0", au0 );
		artio_parameter_set_double( handle, "energy:aeu0", aeu0 );
		artio_parameter_set_double( handle, "energy:ap0", ap0 );
#endif /* PARTICLES */

#ifdef REFINEMENT
		/* refinement boundaries */
		artio_parameter_set_double_array( handle, "refinement_volume_min",
				nDim, refinement_volume_min );
		artio_parameter_set_double_array( handle, "refinement_volume_max",
				nDim, refinement_volume_max );
#endif /* REFINEMENT */

#ifdef STAR_FORMATION
		artio_parameter_set_double_array( handle, "star_formation_volume_min",
				nDim, star_formation_volume_min );
		artio_parameter_set_double_array( handle, "star_formation_volume_max",
				nDim, star_formation_volume_max );

		artio_parameter_set_double( handle, "total_stellar_mass", total_stellar_mass );
		artio_parameter_set_double( handle, "total_stellar_initial_mass", total_stellar_initial_mass );
#endif /* STAR_FORMATION */

#ifdef RADIATIVE_TRANSFER
		n = 0;
		frtCall(packradiationfields)(&n,0);
		if(n < 1)
		{
			cart_error("Unable to pack Radiation Field data.");
		}

		data = cart_alloc(frt_real, n );
		frtCall(packradiationfields)(&n,data);

		if(sizeof(float) == sizeof(frt_real))
		{
			buffer = (float *)data;
		}
		else
		{
			buffer = cart_alloc(float,n);
			for(i=0; i<n; i++) buffer[i] = data[i];
		}

		artio_parameter_set_float_array( handle, "radiation_background", n, buffer );

		if(sizeof(float) == sizeof(frt_real))
		{
		}
		else
		{
			cart_free(buffer);
		}

		cart_free(data);

#ifdef RT_SINGLE_SOURCE
		artio_parameter_set_int( handle, "rt:single_source:level", rtSingleSourceLevel );
		artio_parameter_set_float( handle, "rt:single_source:value", rtSingleSourceValue );
		artio_parameter_set_double_array( handle, "rt:single_source:pos", nDim, rtSingleSourcePos );
#endif
#endif /* RADIATIVE_TRANSFER */

		/* load balance parameters */
		artio_parameter_set_int( handle, "num_octs_per_mpi_task", num_octs );
#ifdef PARTICLES
		artio_parameter_set_int( handle, "num_particles_per_mpi_task", num_particles );
#ifdef STAR_FORMATION
		artio_parameter_set_int( handle, "num_star_particles_per_mpi_task", num_star_particles );
#endif /* STAR_FORMATION */
#endif /* PARTICLES */

#ifdef SFC_DOMAIN_DECOMPOSITION
		int64_t local_proc_sfc_index[MAX_PROCS+1];
		for ( i = 0; i < num_procs+1; i++ ) {
			local_proc_sfc_index[i] = proc_sfc_index[i];
		}

		artio_parameter_set_long_array( handle, "mpi_task_sfc_index", num_procs+1, local_proc_sfc_index );
#else
		if ( local_proc_id == MASTER_NODE ) {
			cart_debug("Warning: non-sfc domain decomposition is not stored with the file. A load balancing step will be performed on file load!");
		}
#endif /* SFC_DOMAIN_DECOMPOSITION */

		/* load predictor information */
		if ( load_predictor_flag ) {
			artio_parameter_set_int( handle, "num_load_predictor_variables", NPREDVARS );
			artio_parameter_set_string_array(handle, "load_predictor_variables", NPREDVARS, PREDVAR_NAME );

			for ( i = 0; i < NPREDVARS; i++ ) {
				sprintf(str, "load_predictor_coefficient_%d", i);
				artio_parameter_set_float_array(handle, str, num_levels, &load_coeffs[(i-min_level)*NPREDVARS]);
			}
		}

		if ( fileset_write_options & WRITE_GRID ) {
			write_artio_grid(handle);
		}

#if defined(PARTICLES) || defined(HYDRO_TRACERS)
		if ( fileset_write_options & WRITE_PARTICLES || fileset_write_options & WRITE_TRACERS ) {
			write_artio_particles(handle, fileset_write_options);
		}
#endif /* PARTICLES */

		if ( artio_fileset_close(handle) != ARTIO_SUCCESS ) {
			cart_error("Error finalizing fileset %s, be careful, it may be corrupted!",
				filename );
		}
	}

	if ( local_proc_id == MASTER_NODE &&
			fileset_write_options & WRITE_GRID
#ifdef PARTICLES
			&& fileset_write_options & WRITE_PARTICLES
#endif /* PARTICLES */
#ifdef HYDRO_TRACERS
			&& fileset_write_options & WRITE_TRACERS
#endif /* HYDRO_TRACERS */
	) {
		/* write out restart file */
		sprintf( restart_filename, "%s/restart.dat", output_directory );
		restart = fopen( restart_filename, "w" );
		if ( restart == NULL ) {
			cart_error("Unable to open restart file %s for writing!", restart_filename );
		}
		fprintf( restart, "%s\n", filename );
		fclose(restart);
	}
}

void write_artio_grid( artio_fileset *handle) {
	int i, j;
	sfc_t sfc;
	int icell, ioct, ichild;
	int ret;
	int num_file_vars;
	int var_indices[num_vars];
	char *var_labels[num_vars];
	int refined[num_children];
	int num_level_octs, num_next_level_octs;
	int *level_octs, *next_level_octs;
	int num_octs_per_level[max_level - min_level + 1];
	int level;
	float variables[num_vars * num_children];

	start_time( GAS_WRITE_IO_TIMER );

	/* build list of variables to write */
	define_file_variables(&num_file_vars, var_labels, var_indices);

	if ( artio_fileset_add_grid( handle,
				num_artio_grid_files, artio_grid_allocation_strategy,
				num_file_vars, var_labels ) != ARTIO_SUCCESS ) {
		cart_error("Error adding grid to fileset");
	}

	for (j=0;j<num_file_vars;j++) {
		cart_free(var_labels[j]);
	}

	for ( icell = 0; icell < num_cells_per_level[min_level]; icell++ ) {
		sfc = root_cell_sfc_index[icell];
		if ( artio_fileset_add_grid_sfc( handle, sfc,
					tree_max_level(icell), tree_oct_count(icell) ) != ARTIO_SUCCESS ) {
			cart_error("Error adding sfc %ld to fileset", sfc);
		}
	}

	ret = artio_fileset_commit_grid( handle );
	if ( ret != ARTIO_SUCCESS ) {
		cart_error("Error finishing grid index, ARTIO error = %d", ret );
	}

	for ( icell = 0; icell < num_cells_per_level[min_level]; icell++ ) {
		sfc = root_cell_sfc_index[icell];
		pack_cell_vars( icell, num_file_vars, var_indices, variables );
		root_tree_level_oct_count( icell, num_octs_per_level );

		level = 1;
		while ( level <= max_level && num_octs_per_level[level-1] > 0 ) level++;
		cart_assert( level-1 == tree_max_level(icell) );

		if ( artio_grid_write_root_cell_begin(handle,(int64_t)sfc, variables, level-1,
				num_octs_per_level) != ARTIO_SUCCESS ) {
			cart_error("Error writing grid root cell at sfc %ld", sfc );
		}

		level = min_level+1;

		if ( cell_is_refined(icell) ) {
			num_next_level_octs = 1;
			next_level_octs = cart_alloc( int, num_next_level_octs );
			next_level_octs[0] = cell_child_oct[icell];
		} else {
			num_next_level_octs = 0;
		}

		while ( num_next_level_octs > 0 ) {
			num_level_octs = num_next_level_octs;
			level_octs = next_level_octs;

			num_next_level_octs = 0;
			if ( level < max_level && num_octs_per_level[level] > 0 ) {
				next_level_octs = cart_alloc( int, num_children*num_level_octs );
			}

			if ( artio_grid_write_level_begin(handle,level) != ARTIO_SUCCESS ) {
				cart_error("Error writing level %u in sfc %ld", level, sfc );
			}

			for ( i = 0; i < num_level_octs; i++ ) {
				ioct = level_octs[i];
				for ( j = 0; j < num_children; j++ ) {
					ichild = oct_child( ioct, j );
					pack_cell_vars( ichild, num_file_vars, var_indices, &variables[num_file_vars*j] );
					if ( cell_is_refined(ichild) ) {
						next_level_octs[num_next_level_octs++] = cell_child_oct[ichild];
						refined[j] = 1;
					} else {
						refined[j] = 0;
					}
				}

				if ( artio_grid_write_oct(handle, variables, refined) != ARTIO_SUCCESS ) {
					cart_error("Error writing oct %d on level %d, sfc = %ld",
						ioct, level, sfc );
				}
			}

			cart_free( level_octs );
			if ( artio_grid_write_level_end(handle) != ARTIO_SUCCESS ) {
				cart_error("Error completing writing level %u in sfc %ld", level, sfc );
			}
			level++;
		}

		if ( artio_grid_write_root_cell_end(handle) != ARTIO_SUCCESS ) {
			cart_error("Error completing writing grid root tree at sfc %ld", sfc );
		}
	}

	end_time( GAS_WRITE_IO_TIMER );
}

#if defined(PARTICLES) || defined(HYDRO_TRACERS)
void write_artio_particles( artio_fileset *handle, int fileset_options ) {
	int i, j, k, m;
	int ret;
	sfc_t sfc;
	int icell;
	int64_t id;
	double *primary_variables;
	float *secondary_variables;
	int subspecie = 0;

#ifdef PARTICLES
	int ipart;
	int *root_tree_particle_list;
#endif /* PARTICLES */

#ifdef HYDRO_TRACERS
	int itracer;
	int *root_tree_tracer_list;
#endif

	int num_file_species;
	char **species_labels;
	int *num_primary_variables;
	char ***primary_variable_labels;
	int *num_secondary_variables;
	char ***secondary_variable_labels;

	int *num_particles_per_species;

	start_time( PARTICLE_WRITE_IO_TIMER );

	define_particle_variables( &num_file_species, &species_labels,
			&num_primary_variables, &primary_variable_labels,
			&num_secondary_variables, &secondary_variable_labels,
			fileset_options );

	int max_secondary_variables = 0;
	int max_primary_variables = 0;
	for ( i = 0; i < num_file_species; i++ ) {
		max_primary_variables = MAX(max_primary_variables, num_primary_variables[i]);
		max_secondary_variables = MAX(max_secondary_variables, num_secondary_variables[i]);
	}
	primary_variables = cart_alloc(double, max_primary_variables);
	secondary_variables = cart_alloc(float, max_secondary_variables);

	ret = artio_fileset_add_particles( handle,
				num_artio_particle_files,
				artio_particle_allocation_strategy,
				num_file_species,
				species_labels,
				num_primary_variables,
				num_secondary_variables,
				primary_variable_labels,
				secondary_variable_labels );
	if ( ret != ARTIO_SUCCESS ) {
		cart_error("Error adding particles to fileset, error = %d", ret);
	}

	for ( i = 0; i < num_file_species; i++ ) {
		for ( j = 0; j < num_primary_variables[i]; j++ ) {
			cart_free( primary_variable_labels[i][j] );
		}
		cart_free( primary_variable_labels[i] );

		for ( j = 0; j < num_secondary_variables[i]; j++ ) {
			cart_free( secondary_variable_labels[i][j] );
		}
		if ( num_secondary_variables[i] > 0 ) {
			cart_free( secondary_variable_labels[i] );
		}
	}

	cart_free(species_labels);
	cart_free(num_primary_variables);
	cart_free(primary_variable_labels);
	cart_free(num_secondary_variables);
	cart_free(secondary_variable_labels);

	num_particles_per_species = cart_alloc(int, num_file_species);

#ifdef PARTICLES
	if ( fileset_options & WRITE_PARTICLES ) {
		artio_parameter_set_float_array( handle, "particle_species_mass",
				num_particle_species, particle_species_mass );

		/* redo particle linked list */
		root_tree_particle_list = gather_root_particle_lists();
	}
#endif /* PARTICLES */

#ifdef HYDRO_TRACERS
	if ( fileset_options & WRITE_TRACERS ) {
		/* redo tracer linked list */
		root_tree_tracer_list = gather_root_tracer_lists();
	}
#endif /* HYDRO_TRACERS */

	/* count number of particles of each type (specie + tracer)
	 * for each root cell and call ARTIO api */
	for ( i = 0; i < num_cells_per_level[min_level]; i++ ) {
		/* count particle species in root cell */
		for ( j = 0; j < num_file_species; j++ ) {
			num_particles_per_species[j] = 0;
		}

#ifdef PARTICLES
		if ( fileset_options & WRITE_PARTICLES ) {
			ipart = root_tree_particle_list[i];
			while ( ipart != NULL_PARTICLE ) {
				num_particles_per_species[particle_species(particle_id[ipart])]++;
				ipart = particle_list_prev[ipart];
			}
		}
#endif /* PARTICLES */

#ifdef HYDRO_TRACERS
		if ( fileset_options & WRITE_TRACERS ) {
			itracer = root_tree_tracer_list[i];
			while ( itracer != NULL_TRACER ) {
				num_particles_per_species[num_file_species-1]++;
				itracer = tracer_list_prev[itracer];
			}
		}
#endif /* HYDRO_TRACERS */

		sfc = root_cell_sfc_index[i];
		if ( artio_fileset_add_particle_sfc( handle, sfc,
					num_particles_per_species ) != ARTIO_SUCCESS ) {
			cart_error("Error adding sfc %ld to particle fileset", sfc);
		}
	}

	ret = artio_fileset_commit_particles(handle);
	if ( ret != ARTIO_SUCCESS ) {
		cart_error("Error finishing particle index for fileset, ARTIO error = %d", ret );
	}

	/* set up counters and indices for secondary variables */
	int num_secondary_vars_temp;
	int idx_basic, idx_enrich, idx_enrich_Ia, idx_enrich_agb, idx_discrete;
	int idx_cluster, idx_cluster_bound_fraction, idx_cluster_debug, idx_cluster_initial_bound;
	/* Then initialize them. */
	get_star_secondary_var_idxs(&num_secondary_vars_temp,
	                            &idx_basic, &idx_enrich, &idx_enrich_Ia, 
								&idx_enrich_agb, &idx_discrete,
								&idx_cluster, &idx_cluster_bound_fraction,
								&idx_cluster_debug, &idx_cluster_initial_bound);

	for ( i = 0; i < num_cells_per_level[min_level]; i++ ) {
		sfc = root_cell_sfc_index[i];

		for ( j = 0; j < num_file_species; j++ ) {
			num_particles_per_species[j] = 0;
		}

#ifdef PARTICLES
		if ( fileset_options & WRITE_PARTICLES ) {
			ipart = root_tree_particle_list[i];
			while ( ipart != NULL_PARTICLE ) {
				num_particles_per_species[particle_species(particle_id[ipart])]++;
				ipart = particle_list_prev[ipart];
			}
		}
#endif /* PARTICLES */
#ifdef HYDRO_TRACERS
		if ( fileset_options & WRITE_TRACERS ) {
			itracer = root_tree_tracer_list[i];
			while ( itracer != NULL_TRACER ) {
				num_particles_per_species[num_file_species-1]++;
				itracer = tracer_list_prev[itracer];
			}
		}
#endif /* HYDRO_TRACERS */

		if ( artio_particle_write_root_cell_begin(handle,(int64_t)sfc,
				num_particles_per_species) != ARTIO_SUCCESS ) {
			cart_error("Error writing particle root cell sfc %ld", sfc );
		}

#ifdef PARTICLES
		if ( fileset_options & WRITE_PARTICLES ) {
			/* this code assumes two types of particles: N-body, Stars */
			ipart = root_tree_particle_list[i];
			for ( j = 0; j < num_particle_species; j++ ) {
				if ( artio_particle_write_species_begin( handle, j ) != ARTIO_SUCCESS ) {
					cart_error("Error writing particle species %d, sfc %ld", j, sfc );
				}

				for ( k = 0; k < num_particles_per_species[j]; k++ ) {
					id = particle_id[ipart];
					cart_assert( particle_species(particle_id[ipart]) == j );

					for ( m = 0; m < nDim; m++ ) {
						primary_variables[m] = particle_x[ipart][m];
					}
					for ( m = 0; m < nDim; m++ ) {
						primary_variables[nDim+m] = particle_v[ipart][m];
					}
					primary_variables[2*nDim] = particle_dt[ipart];
#ifdef STAR_FORMATION
					if ( j == num_particle_species - 1 ) {
						cart_assert( particle_is_star(ipart) );

#ifdef STAR_PARTICLE_TYPES
						subspecie = star_particle_type[ipart];
#endif /* STAR_PARTICLE_TYPES */
						/* Use the indices we found earlier to put things where they belong */
						secondary_variables[idx_basic] = star_tbirth[ipart];
						secondary_variables[idx_basic+1] = star_initial_mass[ipart];
						secondary_variables[idx_basic+2] = particle_mass[ipart];
#ifdef ENRICHMENT
						secondary_variables[idx_enrich] = star_metallicity_II[ipart];
#ifdef ENRICHMENT_SNIa
						secondary_variables[idx_enrich_Ia] = star_metallicity_Ia[ipart];
#endif /* ENRICHMENT_SNIa */
#ifdef ENRICHMENT_ELEMENTS
						secondary_variables[idx_enrich_agb] = star_metallicity_AGB[ipart];
						secondary_variables[idx_enrich_agb+1] = star_metallicity_C[ipart];
						secondary_variables[idx_enrich_agb+2] = star_metallicity_N[ipart];
						secondary_variables[idx_enrich_agb+3] = star_metallicity_O[ipart];
						secondary_variables[idx_enrich_agb+4] = star_metallicity_Mg[ipart];
						secondary_variables[idx_enrich_agb+5] = star_metallicity_S[ipart];
						secondary_variables[idx_enrich_agb+6] = star_metallicity_Ca[ipart];
						secondary_variables[idx_enrich_agb+7] = star_metallicity_Fe[ipart];
#endif /* ENRICHMENT_ELEMENTS */
#endif /* ENRICHMENT */

#ifdef DISCRETE_SN
						secondary_variables[idx_discrete] = star_unexploded_sn[ipart];
#endif /* DISCRETE_SN */
#ifdef CLUSTER
						secondary_variables[idx_cluster] = star_tfinal[ipart];
						secondary_variables[idx_cluster+1] = star_ave_age[ipart];
						secondary_variables[idx_cluster+2] = star_metal_dispersion[ipart];
#ifdef CLUSTER_BOUND_FRACTION
						secondary_variables[idx_cluster_bound_fraction] = star_fbound[ipart];
#endif /* CLUSTER_BOUND_FRACTION */
#ifdef CLUSTER_DEBUG
						secondary_variables[idx_cluster_debug] = star_age_spread[ipart];
#endif /* CLUSTER_DEBUG */
#ifdef CLUSTER_INITIAL_BOUND
						secondary_variables[idx_cluster_initial_bound] = star_ibound[ipart];
#endif /* CLUSTER_INITIAL_BOUND */
#endif /* CLUSTER */
#ifdef STAR_PARTICLE_TYPES
					} else {
						subspecie = 0;
#endif /* STAR_PARTICLE_TYPES */
					}
#endif /* STAR_FORMATION */

					if ( artio_particle_write_particle( handle, id, subspecie,
								primary_variables, secondary_variables ) != ARTIO_SUCCESS ) {
						cart_error("Error writing particle id %ld, species %d, sfc %ld",
								id, j, sfc );
					}
					ipart = particle_list_prev[ipart];
				}

				if ( artio_particle_write_species_end(handle) != ARTIO_SUCCESS ) {
					cart_error("Error completing writing species %d in sfc %ld", j, sfc );
				}
			}
		}
#endif /* PARTICLES */

#ifdef HYDRO_TRACERS
		if ( fileset_options & WRITE_TRACERS ) {
			if ( artio_particle_write_species_begin( handle, num_file_species-1 ) != ARTIO_SUCCESS ) {
				cart_error("Error writing hydro tracer species %d, sfc %ld", num_file_species-1, sfc );
			}

			itracer = root_tree_tracer_list[i];
			while ( itracer != NULL_TRACER ) {
				id = tracer_id[itracer];
				for ( m = 0; m < nDim; m++ ) {
					primary_variables[m] = tracer_x[itracer][m];
				}
				/* primary_variables[nDim] = tracer_dt[itracer]; */

				/* this assumes tracers always sit in leaf cells */
				icell = cell_find_position(tracer_x[itracer]);
				for ( m = 0; m < num_hydro_vars_traced; m++ ) {
					secondary_variables[m] = cell_var(icell, hydro_vars_traced[m]);
				}

				if ( artio_particle_write_particle( handle, id, 0,
							primary_variables, secondary_variables ) != ARTIO_SUCCESS ) {
					cart_error("Error writing hydro tracer particle id %ld, sfc %ld",
							id, sfc );
				}
				itracer = tracer_list_prev[itracer];
			}

			if ( artio_particle_write_species_end(handle) != ARTIO_SUCCESS ) {
				cart_error("Error completing writing hydro tracer species %d in sfc %ld", j, sfc );
			}
		}
#endif /* HYDRO_TRACERS */

		if ( artio_particle_write_root_cell_end(handle) != ARTIO_SUCCESS ) {
			cart_error("Error completing writing particles in sfc %ld", sfc );
		}
	}

#ifdef PARTICLES
	if ( fileset_options & WRITE_PARTICLES ) {
		cart_free( root_tree_particle_list );

		/* return particle_list_prev to doublely-linked-list state */
		rebuild_particle_lists();
	}
#endif /* PARTICLES */

#ifdef HYDRO_TRACERS
	if ( fileset_options & WRITE_TRACERS ) {
		cart_free( root_tree_tracer_list );
		rebuild_tracer_lists();
	}
#endif /* HYDRO_TRACERS */

	cart_free(primary_variables);
	cart_free(secondary_variables);
	cart_free(num_particles_per_species);

	end_time( PARTICLE_WRITE_IO_TIMER );
}
#endif /* PARTICLES || HYDRO_TRACERS */

void artio_restart_load_balance( artio_fileset *handle ) {
	int i, j;
	int level;
	int num_leafs, num_refined;
	int *constrained_quantities;
	float *cell_work;
	int page, file_max_level, num_oct_levels;
	int64_t sfc, start_sfc, end_sfc, num_local_sfc;
	float *variables;
	int *num_octs_per_level;
	int num_file_variables;
	int *boundary_level;
	sfc_t *sfc_list;

	int grid_flag = 0;
	int particle_flag = 0;

	lb_decomposition *decomposition;

	int per_proc_constraints[num_constraints] = {
		(double)num_cells,
#ifdef PARTICLES
		(double)num_particles
#endif /* PARTICLES */
	};

	if ( num_procs == 1 ) {
		lb_uniform_decomposition();
		return;
	}

	/* determine local sfc range for domain decomposition */
	/*
	//  These ranks are just for loading the  information
	*/
	start_sfc = (int64_t)num_root_cells*(int64_t)local_proc_id/num_procs;
	end_sfc = (int64_t)num_root_cells*(int64_t)(local_proc_id+1)/num_procs - 1;
	num_local_sfc = end_sfc - start_sfc + 1;

	sfc_list = cart_alloc(sfc_t, num_local_sfc);
	for ( i = 0; i < num_local_sfc; i++ ) {
		sfc_list[i] = start_sfc+i;
	}

	constrained_quantities = cart_alloc(int, num_constraints*num_local_sfc );
	cell_work = cart_alloc(float, num_local_sfc);

	for ( i = 0; i < num_local_sfc; i++ ) {
		cell_work[i] = 0.0;
	}

	for ( i = 0; i < num_constraints*num_local_sfc; i++ ) {
		constrained_quantities[i] = 0;
	}

	boundary_level = cart_alloc(int, num_neighbors*num_local_sfc);
	for ( i = 0; i < num_neighbors*num_local_sfc; i++ ) {
		boundary_level[i] = min_level;
	}

	/* load grid information */
	if ( artio_fileset_has_grid(handle) ) {
		grid_flag = 1;
		cart_assert(artio_parameter_get_int(handle, "num_grid_variables", &num_file_variables) == ARTIO_SUCCESS);
		cart_assert((artio_parameter_get_int( handle, "max_refinement_level", &file_max_level )==ARTIO_SUCCESS || artio_parameter_get_int( handle, "grid_max_level", &file_max_level )==ARTIO_SUCCESS) && file_max_level <= max_level);
		num_octs_per_level = cart_alloc( int, file_max_level );

		int ret = artio_grid_cache_sfc_range(handle, start_sfc, end_sfc );
		if ( artio_grid_cache_sfc_range(handle, start_sfc, end_sfc ) != ARTIO_SUCCESS ) {
			cart_error("Error caching grid sfc range %ld to %ld for artio_restart_load_balance", start_sfc, end_sfc );
		}

		for ( sfc = start_sfc; sfc <= end_sfc; sfc++ ) {
			if ( artio_grid_read_root_cell_begin(handle, sfc, NULL, NULL,
					&num_oct_levels, num_octs_per_level ) != ARTIO_SUCCESS ) {
				cart_error("Error reading grid root cell %ld", sfc );
			}

			/* contribution from root cell */
			constrained_quantities[num_constraints*(sfc-start_sfc)]++;
			cell_work[sfc-start_sfc] += work_per_cell(min_level, (num_oct_levels>0) ? 1: 0);
			for(j=0; j<num_neighbors; j++) boundary_level[num_neighbors*(sfc-start_sfc)+j] = num_oct_levels;

			for ( level = 1; level <= num_oct_levels; level++ ) {
				constrained_quantities[num_constraints*(sfc-start_sfc)] += num_children*num_octs_per_level[level-1];

				if ( level < num_oct_levels ) {
					num_refined = num_octs_per_level[level];
				} else {
					num_refined = 0;
				}
				num_leafs = num_children*num_octs_per_level[level-1] - num_refined;

				cell_work[sfc-start_sfc] += work_per_cell(level, 0)*num_leafs;
				cell_work[sfc-start_sfc] += work_per_cell(level, 1)*num_refined;
			}

			artio_grid_read_root_cell_end(handle);
		}

		cart_free( num_octs_per_level );

		artio_grid_clear_sfc_cache(handle);
	}

/* we only estimate work for true particles, not HYDRO_TRACERS. If the file
 * contains tracers but PARTICLES is not enabled, there's no point in reading
 * through the file */
#ifdef PARTICLES
	if ( artio_fileset_has_particles(handle) ) {
		particle_flag = 1;
		int species;
		int num_file_species;
		artio_parameter_get_int( handle, "num_particle_species", &num_file_species);
		int *num_particles_per_species = cart_alloc(int, num_file_species);

		int *nbody_flag = cart_alloc(int, num_file_species);
		int *star_flag = cart_alloc(int, num_file_species);

		char **species_labels = cart_alloc(char *, num_file_species);
		for ( i = 0; i < num_file_species; i++ ) {
			species_labels[i] = cart_alloc(char, ARTIO_MAX_STRING_LENGTH);
		}
		if ( artio_parameter_get_string_array(handle, "particle_species_labels",
					num_file_species, species_labels ) != ARTIO_SUCCESS ) {
			cart_error("Unable to read particle species labels from artio header file, which may be corrupt.");
		}

		/* check for N-BODY and STAR particle types, all others are ignored  */
		for ( species = 0; species < num_file_species; species++ ) {
			if ( strcmp(species_labels[species], "N-BODY") == 0 ) {
				nbody_flag[species] = 1;
			} else {
				nbody_flag[species] = 0;
			}

			star_flag[species] = 0;
#ifdef STAR_FORMATION
			if ( strcmp(species_labels[species], "STAR") == 0 ) {
				star_flag[species] = 1;
			}
#endif /* STAR_FORMATION */
			cart_free(species_labels[species]);
		}
		cart_free(species_labels);

		if ( artio_particle_cache_sfc_range(handle, start_sfc, end_sfc ) != ARTIO_SUCCESS ) {
			cart_error("Error caching particle sfc range %ld to %ld for artio_restart_load_balance", start_sfc, end_sfc );
		}

		for ( sfc = start_sfc; sfc <= end_sfc; sfc++ ) {
			if ( artio_particle_read_root_cell_begin( handle, sfc,
						num_particles_per_species ) != ARTIO_SUCCESS ) {
				cart_error("Error reading particle root cell %ld", sfc );
			}

			/* assume majority of particles will sit on max level */
			for ( species = 0; species < num_file_species; species++ ) {
				if ( nbody_flag[species] || star_flag[species] ) {
					constrained_quantities[num_constraints*(sfc-start_sfc)+1] += num_particles_per_species[species];
					cell_work[sfc-start_sfc] += work_per_particle(min_level, star_flag[species])*num_particles_per_species[species];
				}
			}

			artio_particle_read_root_cell_end(handle);
		}

		cart_free( nbody_flag );
		cart_free( star_flag );
		cart_free( num_particles_per_species );

		artio_particle_clear_sfc_cache(handle);
	}
#endif /* PARTICLES */

	/* ensure that some work has been estimated */
	if ( !grid_flag && !particle_flag ) {
		cart_error("Unable to load balance based on an artio file without either grid or particle data!");
	}

	decomposition = lb_domain_decomposition( num_local_sfc, sfc_list, cell_work,
		num_constraints, constrained_quantities, per_proc_constraints, boundary_level );
	lb_apply_decomposition( decomposition );
	lb_destroy_decomposition( decomposition );

	cart_free( boundary_level );
	cart_free( constrained_quantities );
	cart_free( cell_work );
	cart_free( sfc_list );
}

void read_artio_restart(const char *label, int *load_balance_after_io_flag) {
	int i, j, k;
	artio_fileset *handle;
	int sfc_order;
	int64_t num_file_root_cells;
	int level;
	int file_max_level;
	int num_levels;
	int num_file_predvars;
	float *file_load_coeffs;
	char **file_predvars;
	char filename[256];

#ifdef SFC_DOMAIN_DECOMPOSITION
	int errflag;
	int64_t mpi_task_sfc_index[MAX_PROCS+1];
	sfc_t new_sfc_index[MAX_PROCS+1];
	int num_file_procs = 0;
	int num_file_octs = -1;
#ifdef PARTICLES
	int num_file_particles = -1;
#ifdef STAR_FORMATION
	int num_file_star_particles = -1;
#endif /* STAR_FORMATION */
#endif /* PARTICLES */
#endif /* SFC_DOMAIN_DECOMPOSITION */

	FILE *restart;
	char str[ARTIO_MAX_STRING_LENGTH];
	float file_volume_min[nDim];
	float file_volume_max[nDim];
#ifdef RADIATIVE_TRANSFER
	frt_intg n;
	frt_real *data;
	float *buffer;
#endif /* RADIATIVE_TRANSFER */
	artio_context con = { mpi.comm.run };

#ifdef COSMOLOGY
	double OmM0, OmB0, OmL0, h100, DelDC;
#else
	double mass_unit, time_unit, length_unit;
#endif

	if ( label == NULL ) {
		/* try to load from restart.dat */
		sprintf( filename, "%s/restart.dat", output_directory );
		restart = fopen( filename, "r" );

		if ( restart == NULL ) {
			cart_debug("Unable to locate restart.dat, trying default filename!");
			sprintf( filename, "%s/%s", output_directory, jobname );
		} else {
			fscanf( restart, "%s\n", filename );
			fclose(restart);
		}
	} else {
		sprintf( filename, "%s/%s_%s", output_directory, jobname, label );
	}

	handle = artio_fileset_open(filename, ARTIO_OPEN_HEADER, &con);

	/*
	//  Try the fileset directory too
	*/
	if( handle == NULL && label != NULL ) {
		sprintf( filename, "%s/%s_%s/%s_%s", output_directory, jobname, label, jobname, label );
		handle = artio_fileset_open(filename, ARTIO_OPEN_HEADER, &con);
	}

	if( handle == NULL ) {
		cart_error("Unable to open ARTIO fileset %s",filename);
	}

	if ( artio_parameter_get_long(handle, "num_root_cells", &num_file_root_cells) != ARTIO_SUCCESS ) {
		cart_error("Unable to load number of root cells from artio header file!");
	}

	if (num_file_root_cells != num_root_cells) {
		cart_error( "Number of root cells in file %s header does not match compiled value: %d vs %d",
				filename, num_file_root_cells, num_root_cells);
	}

	if ( !artio_fileset_has_grid(handle) ||
			artio_fileset_open_grid(handle) != ARTIO_SUCCESS ) {
		cart_debug("Warning: the fileset does not contain grid data, the code cannot be restarted from a complete snapshot");
	}

	if ( artio_fileset_has_particles(handle) ) {
#if defined(PARTICLES) || defined(HYDRO_TRACERS)
		if ( artio_fileset_open_particles(handle) != ARTIO_SUCCESS ) {
			cart_error("Error occurred opening ARTIO particle file.");
		}
#else
		cart_debug("Warning: ARTIO fileset contains particles but neither PARTICLES or HYDRO_TRACERS is defined");
#endif /* PARTICLES || HYDRO_TRACERS */
#ifdef PARTICLES
	} else {
		cart_debug("Warning: fileset does not contain particles, the code cannot be restarted from a complete snapshot");
#endif /* PARTICLES */
	}

/* If SFC_DOMAIN_DECOMPOSITION enabled we may be able to read decomposition from ARTIO header, if that fails, or
 * if we're not using SFC_DOMAIN_DECOMPOSITION we must do a load balancing step on read */
#ifdef SFC_DOMAIN_DECOMPOSITION
	errflag = (artio_parameter_get_array_length( handle, "mpi_task_sfc_index", &num_file_procs ) != ARTIO_SUCCESS);
	errflag |= (artio_parameter_get_int( handle, "num_octs_per_mpi_task", &num_file_octs ) != ARTIO_SUCCESS);
#ifdef PARTICLES
	errflag |= (artio_parameter_get_int( handle, "num_particles_per_mpi_task", &num_file_particles ) != ARTIO_SUCCESS);
#ifdef STAR_FORMATION
	errflag |= (artio_parameter_get_int( handle, "num_star_particles_per_mpi_task", &num_file_star_particles ) != ARTIO_SUCCESS);
#endif /* STAR_FORMATION */
#endif /* PARTICLES */

	if ( errflag
			|| num_file_procs != num_procs+1
			|| num_file_octs > num_octs
#ifdef PARTICLES
			|| num_file_particles > num_particles
#ifdef STAR_FORMATION
			|| num_file_star_particles > num_star_particles
#endif /* STAR_FORMATION */
#endif /* PARTICLES */
	) {
		if ( local_proc_id == MASTER_NODE ) {
			cart_debug("One or more load balancing parameters in the ARTIO fileset");
			cart_debug("are missing or exceed the current configuration limits");
			cart_debug("(-1 indicates missing):");
			cart_debug("==================================================");
			cart_debug("| %20s | %10s | %10s |", "Parameter", "File", "Current" );
			cart_debug("==================================================");
			cart_debug("| %20s | %10d | %10d |", "num_procs", num_file_procs-1, num_procs );
			cart_debug("| %20s | %10d | %10d |", "num_octs", num_file_octs, num_octs );
#ifdef PARTICLES
			cart_debug("| %20s | %10d | %10d |", "num_particles", num_file_particles, num_particles );
#ifdef STAR_FORMATION
			cart_debug("| %20s | %10d | %10d |", "num_star_particles", num_file_star_particles, num_star_particles );
#endif /* STAR_FORMATION */
#endif /* PARTICLES */
			cart_debug("==================================================");
			cart_debug("Performing an IO load balancing read to ensure a valid distribution");
		}
		artio_restart_load_balance( handle );
		*load_balance_after_io_flag = 1;
	} else {
		cart_debug("Skipping load balance...");
		artio_parameter_get_long_array(handle, "mpi_task_sfc_index", num_file_procs, mpi_task_sfc_index);
		for ( i = 0; i <= num_procs; i++ ) {
			new_sfc_index[i] = (sfc_t)mpi_task_sfc_index[i];
		}

		lb_sfc_decomposition(new_sfc_index);
		*load_balance_after_io_flag = 0;
	}
#else
	/* TODO: create on-disk representation of arbitrary domain decomposition */
	artio_restart_load_balance(handle);
	*load_balance_after_io_flag = 1;
#endif /* SFC_DOMAIN_DECOMPOSITION */

	/* load all simulation parameters here */
	artio_parameter_get_string( handle, "jobname", str );
	/* only set if jobname was not modified by the config file */
	if(strcmp(jobname,"ART") == 0) set_jobname( str );
	cart_debug("jobname: %s", jobname );

	if ( (artio_parameter_get_int( handle, "sfc_type", &sfc_order ) != ARTIO_SUCCESS &&
		  artio_parameter_get_int( handle, "sfc", &sfc_order ) != ARTIO_SUCCESS) || sfc_order != SFC ) {
		cart_error("Grid fileset has undefined or different sfc indexing than compiled code!");
	}

	/* check maximum level */
	if ( (artio_parameter_get_int( handle, "max_refinement_level", &file_max_level ) != ARTIO_SUCCESS &&
	      artio_parameter_get_int( handle, "grid_max_level", &file_max_level ) != ARTIO_SUCCESS) || file_max_level > max_level ) {
		cart_error("Grid fileset has undefined refinement levels or contains more levels than compiled code!");
	}
	num_levels = file_max_level+1;

	if ( artio_parameter_get_int( handle, "current_restart_backup", &current_restart_backup ) != ARTIO_SUCCESS ) {
		current_restart_backup = (step%((restart_frequency>0) ? restart_frequency : 1) + 1) % num_restart_backups;
	} else {
		current_restart_backup = (current_restart_backup+1) % num_restart_backups;
	}

	/* timestepping variables */
	if ( artio_parameter_get_int( handle, "step", &step ) != ARTIO_SUCCESS ||
			artio_parameter_get_double_array( handle, "tl", num_levels, tl ) != ARTIO_SUCCESS ||
			artio_parameter_get_double_array( handle, "tl_old", num_levels, tl_old ) != ARTIO_SUCCESS ||
			artio_parameter_get_double_array( handle, "dtl", num_levels, dtl ) != ARTIO_SUCCESS ||
			artio_parameter_get_double_array( handle, "dtl_old", num_levels, dtl_old ) ||
			artio_parameter_get_int_array( handle, "time_refinement_factor",
					num_levels, time_refinement_factor ) != ARTIO_SUCCESS ||
			artio_parameter_get_int_array( handle, "time_refinement_factor_old",
					num_levels, time_refinement_factor_old ) != ARTIO_SUCCESS ) {
		cart_error("Unable to read timestep information from artio header file!");
	}

#ifdef COSMOLOGY
	if ( artio_parameter_get_double( handle, "auni_init", &auni_init ) != ARTIO_SUCCESS ||
			artio_parameter_get_double_array( handle, "abox", num_levels, abox ) != ARTIO_SUCCESS ||
			artio_parameter_get_double_array( handle, "auni", num_levels, auni ) != ARTIO_SUCCESS ) {
		cart_error("Unable to read expansion factors from artio header file!");
	}

	/* allow configuration to override end parameter, otherwise take from file */
	if ( !control_parameter_exists("auni_end") || !control_parameter_is_set("auni_end") ) {
		if ( artio_parameter_get_double( handle, "auni_end", &auni_end ) != ARTIO_SUCCESS ) {
			cart_debug("Unable to read simulation stopping parameter auni_end from file. Using default parameter auni_end = ", auni_end);
		}
	}
#else
	/* don't throw an error since t_init was not always an artio parameter */
	artio_parameter_get_double( handle, "t_init", &t_init );
	if ( !control_parameter_exists("t_end") || !control_parameter_is_set("t_end") ) {
		if ( artio_parameter_get_double( handle, "t_end", &t_end ) != ARTIO_SUCCESS ) {
			cart_debug("Unable to read simulation stopping parameter t_end from file. Using default parameter = ", t_end);
		}
	}
#endif /* COSMOLOGY */

	for ( level = file_max_level+1; level < max_level; level++ ) {
		tl[level] = tl[file_max_level];
		tl_old[level] = tl_old[file_max_level];
		dtl[level] = dtl[level-1];
		dtl_old[level] = dtl[level-1];

#ifdef COSMOLOGY
		abox[level] = abox[file_max_level];
		auni[level] = abox[file_max_level];
		abox_old[level] = abox_old[file_max_level];
#endif /* COSMOLOGY */
	}

#ifdef HYDRO
	artio_parameter_get_int_array( handle, "hydro_sweep_direction", num_levels, level_sweep_dir );
	for ( level = file_max_level+1; level < max_level; level++ ) {
		level_sweep_dir[level] = 0;
	}
#endif

	/* manually mark time as being set */
	time_enabled = 1;

	/* unit parameters */
	if ( artio_parameter_get_double( handle, "box_size", &box_size ) != ARTIO_SUCCESS ) {
		cart_error("Unable to read box size from artio header file!");
	}

#ifdef COSMOLOGY
	if ( artio_parameter_get_double( handle, "OmegaM", &OmM0 ) != ARTIO_SUCCESS ||
			artio_parameter_get_double( handle, "OmegaL", &OmL0 ) != ARTIO_SUCCESS ||
			artio_parameter_get_double( handle, "OmegaB", &OmB0 ) != ARTIO_SUCCESS ||
			artio_parameter_get_double( handle, "hubble", &h100 ) != ARTIO_SUCCESS ||
			artio_parameter_get_double( handle, "DeltaDC", &DelDC ) ) {
		cart_error("Unable to read cosmology information from artio header file!");
	}

	cosmology_set(OmegaM,OmM0);
	cosmology_set(OmegaL,OmL0);
	cosmology_set(OmegaB,OmB0);
	cosmology_set(h,h100);
	cosmology_set(DeltaDC,DelDC);

#else
	if ( artio_parameter_get_double( handle, "mass_unit", &mass_unit ) != ARTIO_SUCCESS ||
			artio_parameter_get_double( handle, "time_unit", &time_unit ) != ARTIO_SUCCESS ||
			artio_parameter_get_double( handle, "length_unit", &length_unit ) != ARTIO_SUCCESS ) {
		cart_error("Unable to read unit information from artio header file!");
	}

	units_set( mass_unit, time_unit, length_unit );
#endif /* COSMOLOGY */

	units_init();

#ifdef COSMOLOGY
	for ( level = min_level; level <= file_max_level; level++ ) {
		abox_old[level] = abox_from_tcode(tl_old[level]);
	}
#endif

#ifdef PARTICLES
	/* energy conservation variables */
	if ( artio_parameter_get_double( handle, "energy:tintg", &tintg ) != ARTIO_SUCCESS ||
			artio_parameter_get_double( handle, "energy:ekin", &ekin ) != ARTIO_SUCCESS ||
			artio_parameter_get_double( handle, "energy:ekin1", &ekin1 ) != ARTIO_SUCCESS ||
			artio_parameter_get_double( handle, "energy:au0", &au0 ) != ARTIO_SUCCESS ||
			artio_parameter_get_double( handle, "energy:aeu0", &aeu0 ) != ARTIO_SUCCESS ||
			artio_parameter_get_double( handle, "energy:ap0", &ap0 ) != ARTIO_SUCCESS ) {
		cart_error("Unable to read energy conservation variables from artio header file!");
	}
#endif /* PARTICLES */

#ifdef REFINEMENT
	/* refinement boundaries */
	if ( artio_parameter_get_double_array( handle, "refinement_volume_min",
				nDim, refinement_volume_min ) != ARTIO_SUCCESS ||
			artio_parameter_get_double_array( handle, "refinement_volume_max",
				nDim, refinement_volume_max ) != ARTIO_SUCCESS ) {

		/* try again with float */
		if ( artio_parameter_get_float_array( handle, "refinement_volume_min",
					nDim, file_volume_min ) != ARTIO_SUCCESS ||
				artio_parameter_get_float_array( handle, "refinement_volume_max",
					nDim, file_volume_max ) != ARTIO_SUCCESS ) {
			cart_error("Unable to read refinement volume from artio header file!");
		} else {
			for ( i = 0; i < nDim; i++ ) {
				refinement_volume_min[i] = file_volume_min[i];
				refinement_volume_max[i] = file_volume_max[i];
			}
		}
	}
#endif /* REFINEMENT */

#ifdef STAR_FORMATION
	if ( artio_parameter_get_double_array( handle, "star_formation_volume_min",
				nDim, star_formation_volume_min ) != ARTIO_SUCCESS ||
			artio_parameter_get_double_array( handle, "star_formation_volume_max",
				nDim, star_formation_volume_max ) != ARTIO_SUCCESS ) {

		/* try again with float */
		if ( artio_parameter_get_float_array( handle, "star_formation_volume_min",
					nDim, file_volume_min ) != ARTIO_SUCCESS ||
				artio_parameter_get_float_array( handle, "star_formation_volume_max",
					nDim, file_volume_max ) != ARTIO_SUCCESS ) {
			cart_debug("Unable to read star formation volume from artio header file!");
		} else {
			for ( i = 0; i < nDim; i++ ) {
				star_formation_volume_min[i] = file_volume_min[i];
				star_formation_volume_max[i] = file_volume_max[i];
			}
		}
	}

	if ( artio_parameter_get_double( handle, "total_stellar_mass",
				&total_stellar_mass ) != ARTIO_SUCCESS ||
			artio_parameter_get_double( handle, "total_stellar_initial_mass",
				&total_stellar_initial_mass ) != ARTIO_SUCCESS ) {
		cart_debug("Unable to read stellar mass variables from artio header file.  Star formation rates reported in sf.log may not be correct");
		total_stellar_mass = total_stellar_initial_mass = 0.0;
	}
#endif /* STAR_FORMATION */

#ifdef RADIATIVE_TRANSFER
	n = 0;
	frtCall(unpackradiationfields)(&n,0);
	if(n < 1)
	  {
	    cart_error("Unable to unpack Radiation Field data.");
	  }

	data = cart_alloc(frt_real, n );

	if(sizeof(float) == sizeof(frt_real))
	  {
	    buffer = (float *)data;
	  }
	else
	  {
	    buffer = cart_alloc(float,n);
	  }

	if ( artio_parameter_get_float_array( handle, "radiation_background", n, buffer ) != ARTIO_SUCCESS ) {
#ifndef ALLOW_MISSING_FIELDS_RESTART
		cart_error("Unable to read radiation background from artio header!");
#else
		cart_debug("WARNING: Radiation background cannot be read from the artio header and will be set to 0.");
		for(i=0; i<n; i++) buffer[i] = 0.0;
#endif /* ALLOW_MISSING_FIELDS_RESTART */
	}

	if(sizeof(float) == sizeof(frt_real))
	  {
	  }
	else
	  {
	    for(i=0; i<n; i++) data[i] = buffer[i];
	    cart_free(buffer);
	  }

	frtCall(unpackradiationfields)(&n,data);
	cart_free(data);

#ifdef RT_SINGLE_SOURCE
	if ( artio_parameter_get_int( handle, "rt:single_source:level",
				&rtSingleSourceLevel ) != ARTIO_SUCCESS ||
			artio_parameter_get_float( handle, "rt:single_source:value",
				&rtSingleSourceValue ) != ARTIO_SUCCESS ||
			artio_parameter_get_double_array( handle, "rt:single_source:pos",
				nDim, rtSingleSourcePos ) != ARTIO_SUCCESS ) {
		cart_error("Error reading rt source variables from artio header!");
	}
#endif
#endif /* RADIATIVE_TRANSFER */

	/* load previously computed load_predictor_coefficients */
	if ( load_predictor_flag && artio_parameter_get_int(handle, "num_local_predictor_variables", &num_file_predvars) == ARTIO_SUCCESS ) {
		file_load_coeffs = cart_alloc(float, file_max_level);
		file_predvars = cart_alloc(char *, num_file_predvars);
		for (i = 0; i < num_file_predvars; i++ ) {
			file_predvars[i] = cart_alloc(char, ARTIO_MAX_STRING_LENGTH);
		}

		if (artio_parameter_get_string_array(handle, "load_predictor_variables", num_file_predvars, file_predvars) != ARTIO_SUCCESS ) {
			cart_error("Unable to read load predictor variable names from artio header file, which may be corrupt.");
		}

		/* match file predvars with executable predvars */
		for ( i = 0; i < NPREDVARS; i++ ) {
			for ( j = 0; j < num_file_predvars; j++ ) {
				if ( strcmp(PREDVAR_NAME[i], file_predvars[j]) == 0 ) {
					sprintf(str, "load_predictor_coefficient_%d", j);
					artio_parameter_get_float_array(handle, str, file_max_level, file_load_coeffs);

					for ( k = min_level; k < MIN(max_level, file_max_level); k++ ) {
						load_coeffs[(k-min_level)*NPREDVARS + i] = file_load_coeffs[k];
					}
					for ( k = file_max_level; k < max_level; k++ ) {
						load_coeffs[(k-min_level)*NPREDVARS + i] = 0.0;
					}
					break;
				}
			}
			if ( j == num_file_predvars ) {
				cart_debug("warning: didn't find predictor variable %s in header file, setting to 0.");
				for ( k = min_level; k < max_level; k++ ) {
					load_coeffs[(k-min_level)*NPREDVARS + i] = 0.0;
				}
			}
		}

		for ( i = 0; i < num_file_predvars; i++ ) {
			cart_free(file_predvars[i]);
		}
		cart_free(file_predvars);
		cart_free(file_load_coeffs);
	}

	if ( artio_fileset_has_grid(handle) ) {
		read_artio_grid(handle, file_max_level);
	}

#if defined(PARTICLES) || defined(HYDRO_TRACERS)
	if ( artio_fileset_has_particles(handle) ) {
		read_artio_particles(handle);
	}
#endif /* PARTICLES || HYDRO_TRACERS */

	artio_fileset_close(handle);

#ifdef ALLOW_MISSING_FIELDS_RESTART
	if ( num_missing_variables > 0 || num_extra_variables > 0 ) {
		/*
		 * This plugin point can be used to initialize fields that are missing in the restart file.
		 * Note that buffer cells have not been initialized yet (the buffer will be build after load balancing);
		 * if buffer cells must be used to initialize missing fields (e.g., if missing fields depend on the gradients of other variables)
		 * then the buffer must be built inside this plugin.
		 */
		start_time( PLUGIN_TIMER );
		PLUGIN_POINT(SetMissingFields,(num_missing_variables,missing_var_labels,num_extra_variables,extra_var_labels));
		end_time( PLUGIN_TIMER );
		for ( i = 0; i < num_missing_variables; i++ ) {
			cart_free(missing_var_labels[i]);
		}
		num_missing_variables = 0;
		for ( i = 0; i < num_extra_variables; i++ ) {
			cart_free(extra_var_labels[i]);
		}
		num_extra_variables = 0;
	}
#endif /* ALLOW_MISSING_FIELDS_RESTART */
}

void read_artio_grid( artio_fileset *handle, int file_max_level ) {
	int i, j;
	int64_t sfc;
	int icell, ioct, ichild;
	int *oct_order, *next_level_order;
	int next_level_octs;

	int num_sim_variables, num_file_variables;
	int *file_var_indices;
	int sim_var_indices[num_vars];
	char *sim_var_labels[num_vars];

	float * root_variables;
	int num_tree_levels;
	int *num_octs_per_level;
	int level;
	float * oct_variables;
	int oct_refined[num_children];
	char ** file_variables;

	start_time( GAS_READ_IO_TIMER );

	if ( artio_parameter_get_int(handle, "num_grid_variables", &num_file_variables) != ARTIO_SUCCESS ) {
		cart_error("Unable to locate grid data in artio fileset");
	}

	/* load list of variables the code expects */
	define_file_variables(&num_sim_variables, sim_var_labels, sim_var_indices);

	if (num_file_variables < num_sim_variables) {
#ifdef ALLOW_MISSING_FIELDS_RESTART
		cart_debug(
				"WARNING: input file contains fewer variables than code expects (%d vs %d), hope you know what you're doing...",
				num_file_variables, num_sim_variables);
#else
		cart_error("Not enough variables in file header!" );
#endif /* ALLOW_MISSING_FIELDS_RESTART */
	} else if (num_file_variables > num_sim_variables) {
		cart_debug(
				"WARNING: input file contains more variables than code expects (%d vs %d), hope you know what you're doing...",
				num_file_variables, num_sim_variables);
	}


	file_var_indices = cart_alloc(int, num_sim_variables);
	root_variables = cart_alloc(float, num_file_variables);
	oct_variables = cart_alloc(float, 8 * num_file_variables);
	file_variables = cart_alloc(char *, num_file_variables);
	for(i=0; i<num_file_variables; i++) file_variables[i] = cart_alloc(char, ARTIO_MAX_STRING_LENGTH);

	if ( artio_parameter_get_string_array(handle, "grid_variable_labels",
			num_file_variables, file_variables ) != ARTIO_SUCCESS ) {
		cart_error("Unable to read grid variable labels from artio header file, which may be corrupt.");
	}

	/* match expected variables with variables in file */
	for (i = 0; i < num_sim_variables; i++) {
		for (j = 0; j < num_file_variables; j++) {
			if ( file_variables[j] == NULL ) continue;
			if (!strcmp(sim_var_labels[i], file_variables[j])) {
				file_var_indices[i] = j;
#ifdef DEBUG_ARTIO_FIELD_MAPPING
				cart_debug("Matched sim index %2d (cell_var index %2d) with file index %2d for field %s", i, sim_var_indices[i], j, file_variables[j] );
#endif /* DEBUG_ARTIO_FIELD_MAPPING */
				cart_free( file_variables[j] );
				break;
			}
		}

		if (j == num_file_variables) {
#ifdef ALLOW_MISSING_FIELDS_RESTART
			cart_debug("WARNING: field %s is missing in the input file and will be set to 0",sim_var_labels[i]);
			file_var_indices[i] = -1;
			cart_assert( num_missing_variables < num_missing_variables_max );
			missing_var_labels[num_missing_variables] = cart_alloc(char, strlen(sim_var_labels[i])+1);
			strcpy( missing_var_labels[num_missing_variables], sim_var_labels[i] );
			num_missing_variables++;
#else
			cart_error("Unable to locate expected variable %s in header",sim_var_labels[i]);
#endif /* ALLOW_MISSING_FIELDS_RESTART */
		}

		cart_free(sim_var_labels[i]);
	}

#ifdef ALLOW_MISSING_FIELDS_RESTART
	if (num_missing_variables > 0) {
		cart_debug("WARNING: input file is missing %d field(s). By default these fields will be set to 0. To initialize them manually, use SetMissingFields plugin point in src/core/io_artio.c.",num_missing_variables);
	}
#endif /* ALLOW_MISSING_FIELDS_RESTART */

	for( i = 0; i < num_file_variables; i++ ) {
		if ( file_variables[i] != NULL ) {
			cart_debug("WARNING: field %s from the input file will not be used in the current run",file_variables[i]);
#ifdef ALLOW_MISSING_FIELDS_RESTART
			cart_assert( num_extra_variables < num_extra_variables_max );
			extra_var_labels[num_extra_variables] = cart_alloc(char, strlen(file_variables[i])+1);
			strcpy( extra_var_labels[num_extra_variables], file_variables[i] );
#endif /* ALLOW_MISSING_FIELDS_RESTART */
			cart_free( file_variables[i] );
			num_extra_variables++;
		}
	}
	cart_free( file_variables );

	if (num_extra_variables > 0) cart_debug("WARNING: input file contains %d extra field(s) that are not used in the run",num_extra_variables);
	cart_assert( num_file_variables-num_extra_variables == num_sim_variables-num_missing_variables );

	num_octs_per_level = cart_alloc( int, file_max_level );

	/* cache file offsets */
	sfc_t first_sfc_index = num_root_cells;
	sfc_t last_sfc_index = 0;
	for ( i = 0; i < num_cells_per_level[min_level]; i++ ) {
		first_sfc_index = MIN(root_cell_sfc_index[i], first_sfc_index);
		last_sfc_index = MAX(root_cell_sfc_index[i], last_sfc_index);
	}

	if ( artio_grid_cache_sfc_range( handle, first_sfc_index, last_sfc_index ) != ARTIO_SUCCESS ) {
		cart_error("Error caching grid sfc range %ld to %ld for read_artio_grid",
				first_sfc_index, last_sfc_index );
	}

	/* load each space-filling-curve index in turn */
	for ( icell = 0; icell < num_cells_per_level[min_level]; icell++ ) {
		sfc = root_cell_sfc_index[icell];

		if ( artio_grid_read_root_cell_begin(handle, sfc, NULL, root_variables,
					&num_tree_levels, num_octs_per_level) != ARTIO_SUCCESS ) {
			cart_error( "Error reading grid root cell sfc %ld", sfc );
		}

		unpack_cell_vars(icell, num_sim_variables, sim_var_indices,
				file_var_indices, root_variables);

		if (num_tree_levels > 0) {
			if ( split_cell(icell) ) {
				cart_error("Unable to split root cell, sfc = %ld", sfc );
			}

			next_level_order = cart_alloc(int, 1);
			next_level_order[0] = cell_child_oct[icell];
			next_level_octs = 1;
		}

		for (level = 0; level < num_tree_levels; level++) {
			if ( artio_grid_read_level_begin(handle, level+1) != ARTIO_SUCCESS ) {
				cart_error("Error reading level %d, sfc %ld", level+1, sfc );
			}

			oct_order = next_level_order;
			cart_assert(num_octs_per_level[level] == next_level_octs);

			if (level < num_tree_levels-1) {
				next_level_order = cart_alloc(int, num_octs_per_level[level+1] );
			} else {
				next_level_order = NULL;
			}
			next_level_octs = 0;

			for (i = 0; i < num_octs_per_level[level]; i++) {
				ioct = oct_order[i];
				if ( artio_grid_read_oct(handle, NULL,
						oct_variables, oct_refined) != ARTIO_SUCCESS ) {
					cart_error("Error reading oct %d on level %d, sfc %ld", ioct,
						level+1, sfc );
				}

				for (j = 0; j < num_children; j++) {
					ichild = oct_child(ioct, j);

					unpack_cell_vars(ichild, num_sim_variables, sim_var_indices,
							file_var_indices, &oct_variables[num_file_variables*j]);

					if (oct_refined[j] > 0) {
						cart_assert( level < num_tree_levels );
						cart_assert( next_level_octs < num_octs_per_level[level+1] );

						if ( split_cell(ichild) ) {
							cart_error("Unable to split cell, sfc = %ld, level = %d, ichild = %d",
								sfc, level+1, ichild );
						}

						next_level_order[next_level_octs++] = cell_child_oct[ichild];
					}
					else if (oct_refined[j] < 0) {
						cart_assert( level <= num_tree_levels );

						if ( split_cell(ichild) ) {
							cart_error("Unable to split cell, sfc = %ld, level = %d, ichild = %d",
								sfc, level+1, ichild );
						}

						int jj;
						int cc[num_children];
						cell_all_children(ichild,cc);
						for(jj=0; jj<num_children; jj++)
						  {
						    unpack_cell_vars(cc[jj],num_sim_variables,sim_var_indices,file_var_indices,&oct_variables[num_file_variables*j]);
						  }
					}
				}
			}

			cart_free(oct_order);
			if ( artio_grid_read_level_end(handle) != ARTIO_SUCCESS ) {
				cart_error("Error reading level %d, sfc = %ld", level+1, sfc);
			}
		}

		if ( artio_grid_read_root_cell_end(handle) != ARTIO_SUCCESS ) {
			cart_error( "Error ending read of grid root cell sfc %ld", sfc );
		}
	}

	artio_grid_clear_sfc_cache(handle);

	cart_free(file_var_indices);
	cart_free( num_octs_per_level );
	cart_free(root_variables);
	cart_free (oct_variables);

	end_time( GAS_READ_IO_TIMER );
}

#if defined(PARTICLES) || defined(HYDRO_TRACERS)
void read_artio_particles( artio_fileset *handle ) {
	int i, j;
	sfc_t sfc;
	int icell;
	int64_t pid;
	int species;
	int subspecies;
	int num_file_species;
	double *primary_variables;
	int max_primary_variables = 0;
	float *secondary_variables;
	int max_secondary_variables = 0;
	int *num_particles_per_species;

#ifdef STAR_FORMATION
	int read_star_flag = 0;
#endif /* STAR_FORMATION */
#ifdef HYDRO_TRACERS
	int read_tracers_flag = 0;
#endif /* HYDRO_TRACERS */

	int file_particle_species;
	char **species_labels;
	int *species_num_int;
	int64_t *species_num_long;

	start_time( PARTICLE_READ_IO_TIMER );

	artio_parameter_get_int( handle, "num_particle_species", &num_file_species);
	num_particles_per_species = cart_alloc(int, num_file_species);

	for ( species = 0; species < num_file_species; species++ ) {
		char species_label[64];
		int num_variables;

		sprintf(species_label, "species_%02u_primary_variable_labels", species);
		if ( artio_parameter_get_array_length(handle, species_label, &num_variables) != ARTIO_SUCCESS ) {
			cart_error("Unable to read primary variable labels for species %d", species);
		}
		max_primary_variables = MAX(max_primary_variables, num_variables);

		sprintf(species_label, "species_%02u_secondary_variable_labels", species);
#ifdef STAR_FORMATION
		if ( artio_parameter_get_array_length(handle, species_label, &num_variables) != ARTIO_SUCCESS && species == num_file_species-1 ) {
			cart_error("Unable to read secondary variable labels for species %d", species);
		}
		max_secondary_variables = MAX(max_secondary_variables, num_variables);
#endif /* STAR_FORMATION */
	}
	primary_variables = cart_alloc(double, max_primary_variables);
	secondary_variables = cart_alloc(float, max_secondary_variables);

	species_labels = cart_alloc(char *, num_file_species);
	for ( i = 0; i < num_file_species; i++ ) {
		species_labels[i] = cart_alloc(char, ARTIO_MAX_STRING_LENGTH);
	}
	if ( artio_parameter_get_string_array(handle, "particle_species_labels",
				num_file_species, species_labels ) != ARTIO_SUCCESS ) {
		cart_error("Unable to read particle species labels from artio header file, which may be corrupt.");
	}

	/* this is just an easy way to check consistency. ARTIO makes no restriction on the ordering of particle 
	 * species types, while CART expects a particular order (N-body, STAR, HYDRO-TRACER) */
	for ( species = 0; species < num_file_species - 1; species++ ) {
		if ( strcmp(species_labels[species], "HYDRO-TRACER") == 0 ) {
			cart_error("Error: HYDRO-TRACER species is expected to be last in the file, but found at %d (rather than %d). CART doesn't know what to do now...",
				species, num_file_species-1);
		}
	}

	/* detect HYDRO-TRACER particles */
	if ( strcmp(species_labels[num_file_species-1], "HYDRO-TRACER") == 0 ) {
#ifdef HYDRO_TRACERS
		read_tracers_flag = 1;
#else
		cart_debug("Warning: HYDRO-TRACER particles found in ARTIO fileset, but feature not enabled. They will skipped when reading.");
#endif /* HYDRO_TRACERS */
		file_particle_species = num_file_species - 1;
	} else {
		file_particle_species = num_file_species;
	}

	for ( species = 0; species < file_particle_species - 1; species++ ) {
		if ( strcmp(species_labels[species], "STAR") == 0 ) {
			cart_error("Error: STAR species is expected to be last (or next to last) in the file, but found at %d (rather than %d). CART doesn't know what to do now...",
					species, file_particle_species-1);
		}
	}

	if ( file_particle_species > 0 && strcmp(species_labels[file_particle_species-1], "STAR") == 0 ) {
#ifdef STAR_FORMATION
		read_star_flag = 1;
#else
		cart_debug("Warning: STAR particles found in ARTIO fileset, but STAR_FORMATION not defined. They will be read as N-body particles.");
#endif /* STAR_FORMATION */
	}

#ifdef PARTICLES
	num_particle_species = file_particle_species;
	if ( num_particle_species > MAX_PARTICLE_SPECIES ) {
		cart_error("ARTIO file contains more than the available particle species.  Increase MAX_PARTICLE_SPECIES.");
	}
#endif /* PARTICLES */

	species_num_int = cart_alloc(int, num_file_species);
	species_num_long = cart_alloc(int64_t, num_file_species);
	if ( artio_parameter_get_int_array( handle, "num_particles_per_species",
				num_file_species, species_num_int ) == ARTIO_SUCCESS ) {
		/* promote to int64_t for cleaner code */
		for ( i = 0; i < num_file_species; i++ ) {
			species_num_long[i] = species_num_int[i];
		}
	} else if ( artio_parameter_get_long_array( handle, "num_particles_per_species",
			num_file_species, species_num_long ) != ARTIO_SUCCESS ) {
		cart_error("Unable to load particle species counts from artio header file!");
	}

#ifdef PARTICLES
	for ( i = 0; i < num_particle_species; i++ ) {
		/* detect overflow in downcast */
		if ( sizeof(particleid_t) < sizeof(int64_t) && species_num_long[i] >= PARTICLEID_MAX ) {
			cart_error("Detected truncation in converting artio particle species to internal precision.  Use int64 particle ids!");
		}
		particle_species_num[i] = (particleid_t)species_num_long[i];
	}
	particle_species_indices[0] = 0;
	for ( i = 0; i < num_particle_species; i++ ) {
		particle_species_indices[i+1] = particle_species_indices[i] + particle_species_num[i];
	}
	num_particles_total = particle_species_indices[num_particle_species];
#endif /* PARTICLES */

#ifdef HYDRO_TRACERS
	if ( read_tracers_flag ) {
		num_tracers_total = species_num_long[num_file_species-1];
	} else {
		num_tracers_total = 0;
	}
#endif /* HYDRO_TRACERS */

	cart_free(species_num_int);
	cart_free(species_num_long);

#ifdef PARTICLES
	float *file_species_mass = cart_alloc(float, file_particle_species);
	if ( artio_parameter_get_float_array( handle, "particle_species_mass",
			file_particle_species, file_species_mass ) != ARTIO_SUCCESS ) {
		cart_error("Unable to load particle species mass from artio header file!");
	}

	for ( i = 0; i < num_particle_species; i++ ) {
		particle_species_mass[i] = file_species_mass[i];
	}

	cart_free(file_species_mass);
#endif /* PARTICLES */

	/* cache file offsets: can be very inefficient for widely varying sfc.
	 * TODO: instead use ARTIO's ranges for "optimal" chunking */
	sfc_t first_sfc_index = num_root_cells;
	sfc_t last_sfc_index = 0;
	for ( i = 0; i < num_cells_per_level[min_level]; i++ ) {
		first_sfc_index = MIN(root_cell_sfc_index[i], first_sfc_index);
		last_sfc_index = MAX(root_cell_sfc_index[i], last_sfc_index);
	}

	int ret = artio_particle_cache_sfc_range( handle, first_sfc_index, last_sfc_index );
	if ( ret != ARTIO_SUCCESS ) {
		cart_error("ret = %d", ret );
//	if ( artio_particle_cache_sfc_range( handle, first_sfc_index, last_sfc_index ) != ARTIO_SUCCESS ) {
		cart_error("Error caching particle sfc range %ld to %ld for read_artio_particles",
				first_sfc_index, last_sfc_index );
	}

	/* set up counters and indices for secondary variables */
	int num_secondary_vars_temp;
	int idx_basic, idx_enrich, idx_enrich_Ia, idx_enrich_agb, idx_discrete;
	int idx_cluster, idx_cluster_bound_fraction, idx_cluster_debug, idx_cluster_initial_bound;
	/* Then initialize them. */
	get_star_secondary_var_idxs(&num_secondary_vars_temp,
	                            &idx_basic, &idx_enrich, &idx_enrich_Ia, 
								&idx_enrich_agb, &idx_discrete,
								&idx_cluster, &idx_cluster_bound_fraction,
								&idx_cluster_debug, &idx_cluster_initial_bound);

	/* load each space-filling-curve index in turn */
	for ( icell = 0; icell < num_cells_per_level[min_level]; icell++ ) {
		sfc = root_cell_sfc_index[icell];

		if ( artio_particle_read_root_cell_begin(handle, sfc, num_particles_per_species ) !=
				ARTIO_SUCCESS ) {
			cart_error("Error reading particle root cell %ld", sfc );
		}

#ifdef PARTICLES
		int ipart;
		for ( species = 0; species < num_particle_species; species++ ) {
			if ( artio_particle_read_species_begin(handle, species) != ARTIO_SUCCESS ) {
				cart_error("Error beginning reading particle species %d, sfc = %ld", species, sfc );
			}

			for ( i = 0; i < num_particles_per_species[species]; i++ ) {
				if ( artio_particle_read_particle(handle, &pid, &subspecies,
							primary_variables, secondary_variables ) != ARTIO_SUCCESS ) {
					cart_error("Error reading particle %d, species %d, sfc %ld",
						i, species, sfc );
				}

				/* unpack variables */
				if ( pid >= PARTICLEID_MAX ) {
					cart_error("Error storing particle id %ld in particleid_t integer!", pid );
				}
				ipart = particle_alloc((particleid_t)pid);

				for ( j = 0; j < nDim; j++ ) {
					particle_x[ipart][j] = primary_variables[j];
				}

				for ( j = 0; j < nDim; j++ ) {
					particle_v[ipart][j] = primary_variables[nDim+j];
				}

				particle_dt[ipart] = primary_variables[2*nDim];
				particle_t[ipart] = tl[min_level];

#ifdef STAR_FORMATION
				if ( read_star_flag && species == num_particle_species - 1 ) {
#ifdef STAR_PARTICLE_TYPES
					star_particle_type[ipart] = subspecies;
#endif /* STAR_PARTICLE_TYPES */

					star_tbirth[ipart] = secondary_variables[idx_basic];
					star_initial_mass[ipart] = secondary_variables[idx_basic+1];
					particle_mass[ipart] = secondary_variables[idx_basic+2];
#ifdef ENRICHMENT
					star_metallicity_II[ipart] = secondary_variables[idx_enrich];
#ifdef ENRICHMENT_SNIa
					star_metallicity_Ia[ipart] = secondary_variables[idx_enrich_Ia];
#endif /* ENRICHMENT_SNIa */
#ifdef ENRICHMENT_ELEMENTS
					star_metallicity_AGB[ipart] = secondary_variables[idx_enrich_agb];
					star_metallicity_C[ipart]   = secondary_variables[idx_enrich_agb+1];
					star_metallicity_N[ipart]   = secondary_variables[idx_enrich_agb+2];
					star_metallicity_O[ipart]   = secondary_variables[idx_enrich_agb+3];
					star_metallicity_Mg[ipart]  = secondary_variables[idx_enrich_agb+4];
					star_metallicity_S[ipart]   = secondary_variables[idx_enrich_agb+5];
					star_metallicity_Ca[ipart]  = secondary_variables[idx_enrich_agb+6];
					star_metallicity_Fe[ipart]  = secondary_variables[idx_enrich_agb+7];
#endif /* ENRICHMENT_ELEMENTS */
#endif /* ENRICHMENT */

#ifdef DISCRETE_SN
					star_unexploded_sn[ipart] = secondary_variables[idx_discrete];
#endif /* DISCRETE_SN */

#ifdef CLUSTER
					star_tfinal[ipart] = secondary_variables[idx_cluster];
					star_ave_age[ipart] = secondary_variables[idx_cluster+1];
					star_metal_dispersion[ipart] = secondary_variables[idx_cluster+2];
#ifdef CLUSTER_BOUND_FRACTION
					star_fbound[ipart] = secondary_variables[idx_cluster_bound_fraction];
#endif /* CLUSTER_BOUND_FRACTION */
#ifdef CLUSTER_DEBUG
					star_age_spread[ipart] = secondary_variables[idx_cluster_debug];
#endif /* CLUSTER_DEBUG */
#ifdef CLUSTER_INITIAL_BOUND
					star_ibound[ipart] = secondary_variables[idx_cluster_initial_bound];
#endif /* CLUSTER_INITIAL_BOUND */
#endif /* CLUSTER */
				} else {
					particle_mass[ipart] = particle_species_mass[species];
				}
#else
				particle_mass[ipart] = particle_species_mass[species];
#endif /* STAR_FORMATION */
			}

			if ( artio_particle_read_species_end(handle) != ARTIO_SUCCESS ) {
				cart_error("Error completing reading particle species %d, sfc = %ld",
					species, sfc );
			}
		}
#endif /* PARTICLES */

#ifdef HYDRO_TRACERS
		int itracer;
		if ( read_tracers_flag ) {
			species = num_file_species - 1;
			if ( artio_particle_read_species_begin(handle, species) != ARTIO_SUCCESS ) {
				cart_error("Error beginning reading hydro tracer species %d, sfc = %ld", species, sfc );
			}

			for ( i = 0; i < num_particles_per_species[species]; i++ ) {
				if ( artio_particle_read_particle(handle, &pid, &subspecies,
							primary_variables, secondary_variables ) != ARTIO_SUCCESS ) {
					cart_error("Error reading hydro-tracer %d, species %d, sfc %ld",
							i, species, sfc );
				}

				/* unpack variables */
				if ( pid >= TRACERID_MAX ) {
					cart_error("Error storing tracer id %ld in tracerid_t integer!", pid );
				}
				itracer = tracer_alloc((tracerid_t)pid);

				for ( j = 0; j < nDim; j++ ) {
					tracer_x[itracer][j] = primary_variables[j];
				}
				tracer_t[itracer] = tl[min_level];
			}
		}
#endif /* HYDRO_TRACERS */

		if ( artio_particle_read_root_cell_end(handle) != ARTIO_SUCCESS ) {
			cart_error("Error completing reading sfc %ld", sfc );
		}
	}

	cart_free(primary_variables);
	cart_free(secondary_variables);
	cart_free(num_particles_per_species);

	artio_particle_clear_sfc_cache(handle);
	end_time( PARTICLE_READ_IO_TIMER );

#ifdef PARTICLES
	build_particle_list();
#endif /* PARTICLES */
#ifdef HYDRO_TRACERS
	build_tracer_list();
#endif /* HYDRO_TRACERS */
}
#endif /* PARTICLES || HYDRO_TRACERS */
