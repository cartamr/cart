#ifndef __HYDRO_SGST_H__
#define __HYDRO_SGST_H__

#ifndef CONFIGURED
#error "Missing config.h include."
#endif

#ifdef SGS_TURBULENCE

#define Ksgs_min (0.0)
#define Eint_min (1e-30)
#define Etot_min (1e-30)

#define cell_gas_velocity(icell,dim)		( cell_momentum(icell, dim) / cell_gas_density(icell) )
#ifdef SGST_SHEAR_IMPROVED
#define cell_turb_velocity(icell,dim)		( cell_gas_velocity(icell, dim) - cell_sgst_mean_velocity(icell, dim) )
#else /* SGST_SHEAR_IMPROVED */
#define cell_turb_velocity(icell,dim)		( cell_gas_velocity(icell, dim) )
#endif /* SGST_SHEAR_IMPROVED */

struct sgst_model_t
{
	float c1; 					/* Large-eddy viscosity coefficient */
	float c2; 					/* Non-linear viscosity coefficient */
	float ck; 					/* Turbulent diffusion coefficient */
	float ce; 					/* Turbulence dissipation coefficient */

	float gamma;				/* Turbulent gamma (should be 5/3) */

	double shear_time;			/* Shear time in years */
	double fraction_from_SN;	/* Fraction of SN energy that goes to SGS turbulence */
};
extern struct sgst_model_t sgst_model;

extern float sgst_v_rms_ceiling;

void config_init_hydro_sgst();
void config_verify_hydro_sgst();
float cell_gas_turbulent_temperature( int cell );

#endif /* SGS_TURBULENCE */
#endif /* __HYDRO_SGST_H__ */
