#include "config.h"
#if defined(HYDRO) && defined(STAR_FORMATION)


#include <string.h>

#include "auxiliary.h"
#include "control_parameter.h"
#include "starformation_recipe.h"



extern struct StarFormationRecipe sf_recipe_internal;
const struct StarFormationRecipe *sf_recipe = &sf_recipe_internal;

const char *sf_recipe_name = NULL;


/*
//  Configuration
*/
void control_parameter_list_star_formation_recipe(FILE *stream, const void *ptr)
{
  fprintf(stream,"<%s>",sf_recipe_name);
}


void config_init_star_formation_recipe()
{
  static char *ptr = "star_formation_recipe";
  ControlParameterOps r = { NULL, control_parameter_list_star_formation_recipe };

  sf_recipe_name = MACRO_AS_STRING(SF_RECIPE);

  control_parameter_add(r,ptr,"sf:recipe","a recipe for star formation. This parameter is for listing only, and must be set with SF_RECIPE variable in the Makefile. See /src/sf for available recipes.");

  if(sf_recipe_internal.config_init != NULL) sf_recipe_internal.config_init();
}


void config_verify_star_formation_recipe()
{
  cart_assert(sf_recipe_internal.rate != NULL);

  VERIFY(sf:recipe, 1 );

  if(sf_recipe_internal.config_verify != NULL) sf_recipe_internal.config_verify();
}

#endif /* HYDRO && STAR_FORMATION */
