#include "config.h"
#ifdef COSMIC_RAYS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "auxiliary.h"
#include "control_parameter.h"
#include "iterators.h"
#include "tree.h"
#include "units.h"
#include "cosmic_rays.h"

void control_parameter_set_cosmic_rays_gammas(const char *value, void *ptr, int ind) {
	int num_gammas_input = 0;
	char *str, *tok;
	float v1, v2;

	str = cart_alloc(char,strlen(value)+1);
	strcpy(str,value); /* strtok destroys the input string */

	tok = strtok(str," ");

	while(tok != NULL) {

		if (sscanf(tok,"%g/%g",&v1,&v2) == 2) {
			cosmic_rays_bin_gamma(num_gammas_input) = v1/v2;
		} else if (sscanf(tok,"%g",&v1) == 1) {
			cosmic_rays_bin_gamma(num_gammas_input) = v1;
		} else {
			cart_error("Invalid cosmic rays gamma token %s in line %s",tok,value);
		}

		if ( cosmic_rays_bin_gamma(num_gammas_input) <= 0.0 ) {
			cart_error("Invalid cosmic rays gamma for bin %d: %g",num_gammas_input,cosmic_rays_bin_gamma(num_gammas_input));
		}

		if ( num_gammas_input >= cr_num_bins ) {
			cart_error("Too many cosmic rays gammas in config file");
		}

		num_gammas_input++;
		tok = strtok(NULL," ");

	}

	if ( num_gammas_input != cr_num_bins ) {
		cart_error("Not enough cosmic rays gammas in config file: %d were found while %d were expected.",num_gammas_input,cr_num_bins);
	}

	cart_free(str);
}

void control_parameter_print_name(FILE *f, const char *name);

void control_parameter_list_cosmic_rays_gammas(FILE *stream, const void *ptr) {
	const int num_per_line = 10;
	int i;
	int newline = 0;

	for(i=0; i<cr_num_bins; i++)    {
		if(newline)	{
			fprintf(stream,"\n");
			control_parameter_print_name(stream,"cosmic-rays:gammas");
			newline = 0;
		}

		fprintf(stream,"%g ",cosmic_rays_bin_gamma(i));

		if((i+1)%num_per_line == 0)  newline = 1;
	}
}


void config_init_cosmic_rays() {
	int j;
	ControlParameterOps control_parameter_cosmic_rays_gammas = { control_parameter_set_cosmic_rays_gammas, control_parameter_list_cosmic_rays_gammas };
	char help_cosmic_rays_gammas[1024];

	for ( j = 0; j < cr_num_bins; j++ ) {
		cosmic_rays_bin_gamma(j) = cr_default_gamma;
	}

	sprintf(help_cosmic_rays_gammas,"Cosmic rays gammas. If specified, must be specified for each of %d bins. Default is %f.",cr_num_bins,cr_default_gamma);

	control_parameter_add3(control_parameter_cosmic_rays_gammas,extra_energy_gammas,"cosmic-rays:gammas","cosmic-rays-gammas","cosmic_rays_bin_gamma",help_cosmic_rays_gammas);

}

void config_verify_cosmic_rays() {
	int j;

	for ( j = 0; j < cr_num_bins; j++ ) {
		if ( cosmic_rays_bin_gamma(j) <= 0.0 ) {
			cart_error("Invalid cosmic rays gamma for bin %d: %g",j,cosmic_rays_bin_gamma(j));
		}
	}

	VERIFY(cosmic-rays:gammas, 1 );

}

#endif /* COSMIC_RAYS */
