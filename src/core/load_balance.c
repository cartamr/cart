#include "config.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "auxiliary.h"
#include "cache.h"
#include "cell_buffer.h"
#include "control_parameter.h"
#include "density.h"
#include "hydro_tracer.h"
#include "io.h"
#include "iterators.h"
#include "lb.h"
#include "load_balance.h"
#include "load_predictor.h"
#include "pack.h"
#include "parallel.h"
#include "particle.h"
#include "sfc.h"
#include "times.h"
#include "timing.h"
#include "tree.h"
#include "tree_linkedlist.h"

float reserve_cell_fraction = 0.3;
#ifdef PARTICLES
float reserve_particle_fraction = 0.1;
#endif /* PARTICLES */
int load_balance_frequency = 0;

/*
// NG: use this to block tree optimization in load balance
// useful for faster start-up in the analysis mode
*/
int load_balance_optimized = 1;

/*
//  Configuration
*/
void control_parameter_list_load_balance_method(FILE *stream, const void *ptr)
{
  fprintf(stream,"<%s>",MACRO_AS_STRING(LB_METHOD));
}


void config_init_load_balance()
{
  static char *ptr = "load_balance_method";
  ControlParameterOps r = { NULL, control_parameter_list_load_balance_method };

  control_parameter_add(r,ptr,"load-balance-method","a method for performing load balance. This parameter is for listing only, and must be set with LB_METHOD variable in the Makefile. See /src/lb for available methods.");

  control_parameter_add2(control_parameter_int,&load_balance_frequency,"frequency:load-balance","load_balance_frequency","frequency (in global time steps) for balancing the load across the nodes. Zero value disables load balancing altogether.");

  control_parameter_add3(control_parameter_float,&reserve_cell_fraction,"reserve-cell-fraction","reserve_cell_fraction","est_buffer_fraction","Fraction of cells held in reserve for cell buffer and refinement.  This parameter is used in load balancing.");

#ifdef PARTICLES
  control_parameter_add2(control_parameter_float,&reserve_particle_fraction,"reserve-particle-fraction","reserve_cell_fraction","Fraction of particles held in reserve for cell buffer and refinement.  This parameter is used in load balancing.");
#endif
}


void config_verify_load_balance()
{
  VERIFY(load-balance-method, 1 );

  VERIFY(frequency:load-balance, load_balance_frequency >= 0 );

  VERIFY(reserve-cell-fraction, reserve_cell_fraction >= 0.0 && reserve_cell_fraction < 1.0 );

#ifdef PARTICLES
  VERIFY(reserve-particle-fraction, reserve_particle_fraction >= 0.0 && reserve_particle_fraction < 1.0 );
#endif
}

void load_balance() {
	int i, j;
	int level;
	int proc;
	int root_cell;
	int first_oct, old_first_oct;
	int old_num_root_cells;
	int new_oct;

#ifdef PARTICLES
	int ipart;
	int num_parts;
	int num_parts_to_send[MAX_PROCS];
	int particle_list_to_send[MAX_PROCS];
#ifdef STAR_FORMATION
	int num_stars;
#endif
#endif /* PARTICLES */

#ifdef HYDRO_TRACERS
	int itracer;
	int tracer_list_to_send[MAX_PROCS];
#endif /* HYDRO_TRACER */

	float *local_work;
	int *local_constraints;
	int *boundary_level;

	sfc_t sfc, parent_sfc;
	int neighbors[num_neighbors];

	int icell, ioct;
	int num_level_cells;
	int *level_cells;
	int *new_index;
	pack *transfer_cells;

	lb_decomposition *decomposition;

	int per_proc_constraints[num_constraints] = {
			(1.0-reserve_cell_fraction)*(double)num_cells,
#ifdef PARTICLES
			(1.0-reserve_particle_fraction)*(double)num_particles
#endif /* PARTICLES */
	};

	cart_debug("in load balance");

	/* don't bother doing anything when only 1 processor exists */
	if ( num_procs == 1 ) {
		if ( !root_cells_enabled ) {
			lb_uniform_decomposition();
		}
		if ( !buffer_enabled ) {
			build_cell_buffer();
		}

		return;
	}

	start_time( LOAD_BALANCE_TIMER );

	/* compute information for each root tree */
	local_work = cart_alloc(float, num_cells_per_level[min_level] );
	local_constraints = cart_alloc(int, num_constraints*num_cells_per_level[min_level] );
	boundary_level = cart_alloc(int, num_neighbors*num_cells_per_level[min_level] );

	for ( i = 0; i < num_cells_per_level[min_level]; i++ ) {
		local_work[i] = 0.0;
	}

	for ( i = 0; i < num_constraints*num_cells_per_level[min_level]; i++ ) {
		local_constraints[i] = 0;
	}

	for ( i = 0; i < num_neighbors*num_cells_per_level[min_level]; i++ ) {
		boundary_level[i] = min_level;
	}

	/* compute work and constraints per root cell */
	for ( level = min_level; level <= max_level; level++ ) {
		select_level( level, CELL_TYPE_LOCAL, &num_level_cells, &level_cells );
		/* THIS LOOP MUST REMAIN SERIAL, or protect local_work/local_constraints (Doug - 2/27/2010) */
		for ( i = 0; i < num_level_cells; i++ ) {
			icell = level_cells[i];
			parent_sfc = cell_parent_root_sfc(icell);
			root_cell = sfc_cell_location(parent_sfc);
			cart_assert( root_cell >= 0 && root_cell < num_cells_per_level[min_level] );

			local_work[root_cell] += work_per_cell(level, cell_is_refined(icell));
			local_constraints[num_constraints*root_cell]++;

			cell_all_neighbors( icell, neighbors );
			for ( j = 0; j < num_neighbors; j++ ) {
				if ( cell_parent_root_sfc(neighbors[j]) != parent_sfc ) {
					boundary_level[num_neighbors*root_cell+j] =
						MAX( boundary_level[num_neighbors*root_cell+j],
								cell_level(neighbors[j]) );
				}
			}

#ifdef PARTICLES
			/* count number of particles in cell */
			num_parts = 0;
#ifdef STAR_FORMATION
			num_stars = 0;
#endif
			ipart = cell_particle_list[icell];
			while ( ipart != NULL_PARTICLE ) {
				num_parts++;
#ifdef STAR_FORMATION
				if ( particle_is_star(ipart) ) num_stars++;
#endif /* STAR_FORMATION */
				ipart = particle_list_next[ipart];
			}

#ifdef STAR_FORMATION
			local_work[root_cell] += work_per_particle(level, 1) * num_stars;
			num_parts -= num_stars;
#endif
			local_work[root_cell] += work_per_particle(level, 0) * num_parts;

			local_constraints[num_constraints*root_cell+1] += num_parts;
#endif /* PARTICLES */
		}

		cart_free( level_cells );
	}

	/* call load balancing routine */
	decomposition = lb_domain_decomposition( num_cells_per_level[min_level],
			root_cell_sfc_index, local_work, num_constraints, local_constraints,
			per_proc_constraints, boundary_level );

	cart_free( local_work );
	cart_free( local_constraints );
	cart_free( boundary_level );

#if defined(PARTICLES) || defined(HYDRO_TRACERS)
	for ( proc = 0; proc < num_procs; proc++ ) {
#ifdef PARTICLES
		particle_list_to_send[proc] = NULL_PARTICLE;
#endif /* PARTICLES */
#ifdef HYDRO_TRACERS
		tracer_list_to_send[proc] = NULL_TRACER;
#endif /* HYDRO_TRACERS */
	}

	for ( level = min_level; level <= max_level; level++ ) {
		select_level( level, CELL_TYPE_LOCAL, &num_level_cells, &level_cells );

		for ( i = 0; i < num_level_cells; i++ ) {
			icell = level_cells[i];
			proc = lb_sfc_processor_owner( decomposition, cell_parent_root_sfc(icell) );
			cart_assert( proc >= 0 && proc < num_procs );

			if ( proc != local_proc_id ) {
#ifdef PARTICLES
				if ( cell_particle_list[icell] != NULL_PARTICLE ) {
					/* find end of cell list */
					ipart = cell_particle_list[icell];
					while ( particle_list_next[ipart] != NULL_PARTICLE ) {
						ipart = particle_list_next[ipart];
					}
					particle_list_prev[cell_particle_list[icell]] = particle_list_to_send[proc];
					particle_list_to_send[proc] = ipart;
					cell_particle_list[icell] = NULL_PARTICLE;
				}
#endif /* PARTICLES */
#ifdef HYDRO_TRACERS
				if ( cell_tracer_list[icell] != NULL_TRACER ) {
					/* find end of cell list */
					itracer = cell_tracer_list[icell];
					while ( tracer_list_next[itracer] != NULL_TRACER ) {
						itracer = tracer_list_next[itracer];
					}
					tracer_list_next[itracer] = tracer_list_to_send[proc];
					tracer_list_to_send[proc] = cell_tracer_list[icell];
					cell_tracer_list[icell] = NULL_TRACER;
				}
#endif /* HYDRO_TRACERS */
			}
		}

		free( level_cells );
	}
#endif /* PARTICLES || HYDRO_TRACERS */

	/* store the first real oct index in case number of root cells changes */
	old_num_root_cells = num_cells_per_level[min_level];
	old_first_oct = cell_parent_oct( num_cells_per_level[min_level] +
			num_buffer_cells[min_level] ) + 1;

	/* get the cell buffer out of the way (easier to rebuild it later) */
	if ( buffer_enabled ) {
		destroy_cell_buffer();
	}

	/* set up list of cells to send/recv */
	transfer_cells = pack_init( CELL_TYPE_LOCAL );

	new_index = cart_alloc(int, num_cells_per_level[min_level]);

	/* pack cells to move */
	for ( icell = 0; icell < num_cells_per_level[min_level]; icell++ ) {
		sfc = root_cell_sfc_index[icell];
		proc = lb_sfc_processor_owner(decomposition, sfc);

		if ( proc != local_proc_id ) {
			pack_add_root_tree( transfer_cells, proc, sfc );
			new_index[icell] = -1;
		} else {
			new_index[icell] = lb_sfc_cell_position(decomposition, sfc);
			cart_assert(new_index[icell] >= 0 && new_index[icell] < num_cells);
		}
	}

	/* finish packing cells so we can move them */
	pack_apply( transfer_cells );

	/* free trees which were just packed */
	for ( proc = 0; proc < num_procs; proc++ ) {
		for ( icell = 0; icell < num_cells_per_level[min_level]; icell++ ) {
			if ( new_index[icell] == -1 ) {
				cell_free(icell);
			}
		}
	}

	/* make transfer official */
	lb_apply_decomposition( decomposition );
	lb_destroy_decomposition( decomposition );

	/* move octs out of the way */
	first_oct = cell_parent_oct( num_cells_per_level[min_level] +
					num_buffer_cells[min_level] ) + 1;
	next_free_oct = MAX( next_free_oct, first_oct );

	/* remove all octs < first_oct from free_oct_list */
	ioct = free_oct_list;
	while ( ioct != NULL_OCT ) {
		if ( ioct < first_oct ) {
			if ( ioct == free_oct_list ) {
				free_oct_list = oct_next[ioct];
			} else {
				oct_next[oct_prev[ioct]] = oct_next[ioct];
			}

			if ( oct_next[ioct] != NULL_OCT ) {
				oct_prev[oct_next[ioct]] = oct_prev[ioct];
			}
		}

		ioct = oct_next[ioct];
	}

	/* add old root octs to free_oct_list */
	for ( ioct = first_oct; ioct < old_first_oct; ioct++ ) {
		cart_assert( oct_level[ioct] == FREE_OCT_LEVEL );
		oct_next[ioct] = free_oct_list;
		oct_prev[ioct] = NULL_OCT;

		if ( free_oct_list != NULL_OCT ) {
			oct_prev[free_oct_list] = ioct;
		}
		free_oct_list = ioct;
	}

	/* move non-free octs from root cell space */
	for ( ioct = 0; ioct < first_oct; ioct++ ) {
		if ( oct_level[ioct] != FREE_OCT_LEVEL ) {
			/* move it out of the way */
			new_oct = oct_alloc();
			oct_move( ioct, new_oct );

			/* don't use oct_free on ioct since it'll be placed in free oct list */
			linked_list_remove( &local_oct_list[oct_level[ioct]], ioct );
			oct_level[ioct] = FREE_OCT_LEVEL;
			oct_parent_root_cell[ioct] = NULL_OCT;
			oct_is_local[ioct] = -1;
			oct_parent_cell[ioct] = NULL_OCT;

			for ( i = 0; i < num_neighbors; i++ ) {
				oct_neighbors[ioct][i] = NULL_OCT;
			}
		}
	}

	/* move root cells to proper positions */
	for ( icell = 0; icell < old_num_root_cells; icell++ ) {
		while ( new_index[icell] >= 0 && new_index[icell] != icell ) {
			if ( new_index[icell] >= old_num_root_cells ||
					new_index[ new_index[icell] ] == -1 ) {
				/* move */
				cell_move( icell, new_index[icell] );
				new_index[icell] = -1;
			} else {
				cart_assert( new_index[icell] > icell );

				/* swap */
				cell_swap( icell, new_index[icell] );
				i = new_index[icell];
				new_index[icell] = new_index[i];
				new_index[i] = i;
			}
		}
	}

	cart_free( new_index );

	/* repair oct_parent_root_cells (this wasn't formerly necessary as we stored sfc rather than index) */
	for ( level = min_level; level < max_level; level++ ) {
		select_level( level, CELL_TYPE_LOCAL | CELL_TYPE_REFINED, &num_level_cells, &level_cells );
		for ( i = 0; i < num_level_cells; i++ ) {
			icell = level_cells[i];
			oct_parent_root_cell[cell_child_oct[icell]] = cell_parent_root_cell(icell);
		}
		cart_free(level_cells);
	}

	for ( icell = num_cells_per_level[min_level]; icell < num_cells_per_level[min_level] +
			num_buffer_cells[min_level]; icell++ ) {
		cart_assert( cell_child_oct[icell] == NULL_OCT );
#ifdef PARTICLES
		cart_assert( cell_particle_list[icell] == NULL_PARTICLE );
#endif /* PARTICLES */
	}

	/* need to repair neighbors here for can_prune */
	repair_neighbors();

	/* communicate trees to new owners */
	pack_communicate( transfer_cells );

	/* cleanup transfer list */
	pack_destroy( transfer_cells );

	for ( icell = num_cells_per_level[min_level]; icell < num_cells_per_level[min_level] +
			num_buffer_cells[min_level]; icell++ ) {
		cart_assert( cell_child_oct[icell] == NULL_OCT );
	}

	/* reorder cells for cache efficiency */
	if(load_balance_optimized) cache_reorder_tree();

	for ( icell = num_cells_per_level[min_level]; icell < num_cells_per_level[min_level] +
			num_buffer_cells[min_level]; icell++ ) {
		cart_assert( cell_child_oct[icell] == NULL_OCT );
	}

	/* re-enable cell buffer */
	build_cell_buffer();

	/* move particles */
#ifdef PARTICLES
	/* send and receive particles which switched processor */
	trade_particle_lists( particle_list_to_send, -1, FREE_PARTICLE_LISTS );

	/* reorder particles for cache efficiency */
	if(load_balance_optimized) cache_reorder_particles();
#endif /* PARTICLES */

	/* move tracers */
#ifdef HYDRO_TRACERS
	/* send and receive particles which switched processor */
	trade_tracer_lists( tracer_list_to_send, -1 );

	/* reorder tracers? */
#endif /* HYDRO_TRACERS */

	cart_debug("done with load balance");

	end_time( LOAD_BALANCE_TIMER );
}
