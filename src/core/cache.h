#ifndef __CACHE_H__
#define __CACHE_H__

#ifndef CONFIGURED
#error "Missing config.h include."
#endif

void cache_reorder_tree();

#ifdef PARTICLES
void cache_reorder_particles();
#endif /* PARTICLES */

#endif
