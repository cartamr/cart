#include "config.h"

#include <gsl/gsl_multimin.h>

#include "control_parameter.h"
#include "load_balance.h"
#include "load_predictor.h"
#include "auxiliary.h"
#include "iterators.h"
#include "parallel.h"
#include "tree.h"
#include "pack.h"
#include "sfc.h"
#include "particle.h"

float cost_per_cell = 1.0;

#ifdef PARTICLES
float cost_per_particle = 0.25;
#ifdef STAR_FORMATION
float cost_per_stellar_particle = 1.0;
#endif /* STAR_FORMATION */
#endif /* PARTICLES */

int load_predictor_flag = 1;

/*
// Minimum number of independent samples required to use
// a particular variable as input to the load predictor
*/
const int MIN_PREDVAR_SAMPLES = 2;

float load_coeffs[(max_level-min_level+1)*NPREDVARS];
int step_predvars[(max_level-min_level+1)*NPREDVARS];
DEFINE_LEVEL_ARRAY(double, step_worktimes);
DEFINE_LEVEL_ARRAY(float, level_cost);

#define PREDVAR(var) #var,
char *PREDVAR_NAME[] = {
#include "load_predictor_variables.h"
};
#undef PREDVAR

const double lp_big_number = 1e5;
const double lp_boundary_scale = 1e9;

int lp_solve( int num_data, int num_predvars, double *predvars, double *worktimes, double *result );

void config_init_load_predictor() {
  int level;

  control_parameter_add2(control_parameter_float,&cost_per_cell,"cost-per-cell","cost_per_cell","computational cost per cell. This parameter is used in load balancing.");

#ifdef PARTICLES
  control_parameter_add2(control_parameter_float,&cost_per_particle,"cost-per-particle","cost_per_particle","computational cost per particle. This parameter is used in load balancing.");

#ifdef STAR_FORMATION
  control_parameter_add2(control_parameter_float,&cost_per_stellar_particle,"cost-per-stellar-particle","cost_per_stellar_particle","computational cost per stellar particle. This parameter is used in load balancing.");
#endif /* STAR_FORMATION */
#endif /* PARTICLES */

  control_parameter_add2(control_parameter_bool,&load_predictor_flag,"load-predictor","load_predictor","Enable/disable using the load predictor in load balancing (T or F).");

  /* initialize load balance coefficients here to guarantee array is set */
  for ( level = min_level; level <= max_level; level++ ) {
	   
                load_coeffs[NPREDVARS*(level-min_level)+PREDVAR_LEAF_CELL] = cost_per_cell;
#ifdef REFINEMENT
                load_coeffs[NPREDVARS*(level-min_level)+PREDVAR_REFINED_CELL] = cost_per_cell;
#endif /* REFINEMENT */

#ifdef PARTICLES
                load_coeffs[NPREDVARS*(level-min_level)+PREDVAR_NONSTAR] = cost_per_particle;
#ifdef STAR_FORMATION
                load_coeffs[NPREDVARS*(level-min_level)+PREDVAR_STAR] = cost_per_stellar_particle;
#endif /* STAR_FORMATION */
#endif /* PARTICLES */

                level_cost[level] = (1 << level);
        }
}

void config_verify_load_predictor() {
	int level;

	VERIFY(cost-per-cell, cost_per_cell >= 0.0);
#ifdef PARTICLES
	VERIFY(cost-per-particle, cost_per_particle >= 0.0);
#ifdef STAR_FORMATION
	VERIFY(cost-per-stellar-particle, cost_per_stellar_particle >= 0.0);
#endif /* STAR_FORMATION */
#endif /* PARTICLES */

	VERIFY(load-predictor, load_predictor_flag == 0 || load_predictor_flag == 1);

	/* re-initialize load balance coefficients since they may have been changed */
	for ( level = min_level; level <= max_level; level++ ) {
		load_coeffs[NPREDVARS*(level-min_level)+PREDVAR_LEAF_CELL] = cost_per_cell;
#ifdef REFINEMENT
		load_coeffs[NPREDVARS*(level-min_level)+PREDVAR_REFINED_CELL] = cost_per_cell;
#endif /* REFINEMENT */

#ifdef PARTICLES
		load_coeffs[NPREDVARS*(level-min_level)+PREDVAR_NONSTAR] = cost_per_particle;
#ifdef STAR_FORMATION
		load_coeffs[NPREDVARS*(level-min_level)+PREDVAR_STAR] = cost_per_stellar_particle;
#endif /* STAR_FORMATION */
#endif /* PARTICLES */
	}
}

float work_per_cell(int level, int refined) {
#ifdef REFINEMENT
	if ( refined ) {
		return load_coeffs[(level-min_level)*NPREDVARS + PREDVAR_REFINED_CELL]*level_cost[level];
	}
#endif /* REFINEMENT */

	return load_coeffs[(level-min_level)*NPREDVARS + PREDVAR_LEAF_CELL]*level_cost[level];
}

#ifdef PARTICLES
float work_per_particle(int level, int star) {
#ifdef STAR_FORMATION
	if ( star ) {
		return load_coeffs[(level-min_level)*NPREDVARS + PREDVAR_STAR]*level_cost[level];
	}
#endif /* STAR_FORMATION */

	return load_coeffs[(level-min_level)*NPREDVARS + PREDVAR_NONSTAR]*level_cost[level];
}
#endif /* PARTICLES */

void load_predictor() {
	int level;
	int status;
	int predvar;
	int proc;
	double *level_vars = NULL;
	double *level_times = NULL;
	double *worktimes = NULL;
	double solution[NPREDVARS];
	int *procflag = NULL;
	int *predvars = NULL;
	int num_data_vars;
	int num_data;
	int varflag[NPREDVARS];
	int var_offset;
	int times_offset;
	int level_offset;
	double avg_coeff;

	if (!load_predictor_flag) return;

	if ( local_proc_id == MASTER_NODE ) {
		predvars = cart_alloc(int, NPREDVARS*num_procs*(max_level-min_level+1));
		worktimes = cart_alloc(double,num_procs*(max_level-min_level+1));
		procflag = cart_alloc(int, num_procs);
	}

	/* gather level data */
	MPI_Gather(step_predvars, NPREDVARS*(max_level-min_level+1), MPI_INT,
			predvars, NPREDVARS*(max_level-min_level+1), MPI_INT, MASTER_NODE,
			mpi.comm.run);

	MPI_Gather(step_worktimes, (max_level-min_level+1), MPI_DOUBLE,
			worktimes, (max_level-min_level+1), MPI_DOUBLE, MASTER_NODE,
			mpi.comm.run);

	if ( local_proc_id == MASTER_NODE ) {
		level_vars = cart_alloc(double, NPREDVARS*num_procs);
		level_times = cart_alloc(double,num_procs);

		for ( level = min_level; level <= max_level; level++ ) {
			for ( predvar = 0; predvar < NPREDVARS; predvar++ ) {
				varflag[predvar] = 0;
			}

			/* Determine which processes and PREDVARS have data available */
			for ( proc = 0; proc < num_procs; proc++ ) {
				procflag[proc] = 0;
				for ( predvar = 0; predvar < NPREDVARS; predvar++ ) {
					if ( predvars[NPREDVARS*((max_level-min_level+1)*proc + level-min_level) + predvar] > 0 ) {
						procflag[proc] = 1;
						varflag[predvar]++;
					}
				}

				/* eliminate datapoints from procs with no recorded time */
				if ( worktimes[(max_level-min_level+1)*proc + level-min_level] == 0.0 ) {
					procflag[proc] = 0;
				}
			}

			/* compute the reduced set of predvars and procs */
			num_data_vars = 0;
			for ( predvar = 0; predvar < NPREDVARS; predvar++ ) {
				/* ensure a minimum of datapoints with that variable */
				if ( varflag[predvar] >= MIN_PREDVAR_SAMPLES ) num_data_vars++;
			}

			num_data = 0;
			for ( proc = 0; proc < num_procs; proc++ ) {
				if ( procflag[proc] ) num_data++;
			}

			if ( num_data > 0 && num_data_vars > 0 ) {
				/* construct arrays with only useable sample points */
				var_offset = times_offset = 0;
				for ( proc = 0; proc < num_procs; proc++ ) {
					if ( procflag[proc] ) {
						for ( predvar = 0; predvar < NPREDVARS; predvar++ ) {
							if ( varflag[predvar] ) {
								level_vars[var_offset++] = (double)predvars[NPREDVARS*((max_level-min_level+1)*proc + level-min_level) + predvar];
							}
						}
						level_times[times_offset++] = worktimes[(max_level-min_level+1)*proc+level-min_level];
					}
				}

				/* solve for coefficients */
				lp_solve( num_data, num_data_vars, level_vars, level_times, solution );

				/* fill in coefficients obtained by lp_solve. If not enough datapoints
				 * exist and on level > fitted_level_req copy from lower level
				 * Otherwise keep last measurement/guess/manually set coefficient */
				level_offset = (level-min_level)*NPREDVARS;
				var_offset = 0;
				for (predvar = 0; predvar < NPREDVARS; ++predvar) {
					if ( varflag[predvar] >= MIN_PREDVAR_SAMPLES ) {
						load_coeffs[level_offset + predvar] = solution[var_offset++];
					} else {
						load_coeffs[level_offset + predvar] = 0.0;
					}
				}
				cart_assert( var_offset == num_data_vars );
			} else {
				level_offset = (level-min_level)*NPREDVARS;
				for (predvar = 0; predvar < NPREDVARS; ++predvar) {
					load_coeffs[level_offset + predvar] = 0.0;
				}
			}
		}

		/* use average of levels with predicted coefficients to fill in for
		 * levels that lack sufficient data for a measurement */
		for (predvar = 0; predvar < NPREDVARS; ++predvar) {
			avg_coeff = 0.0;
			num_data = 0;
			for ( level = min_level; level <= max_level; level++ ) {
				if ( load_coeffs[(level-min_level)*NPREDVARS+predvar] > 0.0 ) {
					avg_coeff += load_coeffs[(level-min_level)*NPREDVARS+predvar];
					num_data++;
				}
			}

			if ( num_data > 0 ) {
				avg_coeff /= (double)num_data;
				for ( level = min_level; level <= max_level; level++ ) {
					if ( load_coeffs[(level-min_level)*NPREDVARS+predvar] == 0.0 ) {
						load_coeffs[(level-min_level)*NPREDVARS+predvar] = avg_coeff;
					}
				}
			}
		}

		cart_debug("LOAD PREDICTOR:");
		for ( level = min_level; level <= max_level; level++ ) {
			for ( predvar = 0; predvar < NPREDVARS; predvar++ ) {
				cart_debug("load_coeffs[level=%d][predvar=%d] = %e", level, predvar, load_coeffs[(level-min_level)*NPREDVARS+predvar]);
			}
		}

		cart_free( level_vars );
		cart_free( level_times );

		cart_free(predvars);
		cart_free(worktimes);
		cart_free(procflag);
	}

	MPI_Bcast(load_coeffs, NPREDVARS*(max_level-min_level+1), MPI_INT, MASTER_NODE, mpi.comm.run);
}

/*
// The following three functions are all needed for nonlinear fitting of
// the coefficients.
*/
typedef struct {
	int num_data;
	int num_predvars;
	double *predvars;
	double *worktimes;
	double *predtimes;
} lp_params;

double lp_f (const gsl_vector *v, void *params);
void lp_df (const gsl_vector *v, void *params, gsl_vector *df);
void lp_fdf (const gsl_vector *x, void *params, double *f, gsl_vector *df) ;

/*
Here, "params" will be unused for the time being, as parametrization is actually
stored in the matrix and response vector above, globally visible
*/

int lp_solve( int num_data, int num_predvars, double *predvars, double *worktimes, double *result ) {
	/*
	// gsl_multimin_fdfminimizer_type "T" defines the type of
	// minimization algorithm used; the best type really depends on the
	// sort of problem in question
	*/
	const gsl_multimin_fdfminimizer_type *T = gsl_multimin_fdfminimizer_conjugate_fr;
	gsl_multimin_fdfminimizer *s;
	gsl_vector *x;
	gsl_multimin_function_fdf lp_func;
	lp_params p;
	int iter, status;
	int i_c, i_p;

	lp_func.n = num_predvars;
	lp_func.params = (void *)&p;
	lp_func.f = &lp_f;
	lp_func.df = &lp_df;
	lp_func.fdf = &lp_fdf;

	p.num_data = num_data;
	p.num_predvars = num_predvars;
	p.predvars = predvars;
	p.worktimes = worktimes;
	p.predtimes = cart_alloc(double,num_predvars*num_data);

	x = gsl_vector_alloc(lp_func.n);
	for (i_c = 0; i_c < lp_func.n; ++i_c) {
		gsl_vector_set(x, i_c, 1./lp_boundary_scale*10);
	}

	s = gsl_multimin_fdfminimizer_alloc (T, lp_func.n);

	/*
	// Concerning "gsl_multimin_fdfminimizer_set", from the website:
	//
	// "This function initializes the minimizer [s] to minimize the
	// function [lp_func] starting from the initial point x. The size of
	// the first trial step is given by step_size (4th argument). The
	// accuracy of the line minimization is specified by the fifth, and
	// final, argument. The precise meaning of this parameter depends on
	// the method used. Typically the line minimization is considered
	// successful if the gradient of the function g is orthogonal to the
	// current search direction p to a relative accuracy of tol, where
	// dot(p,g) < tol |p| |g|."
	*/
	gsl_multimin_fdfminimizer_set (s, &lp_func, x, 0.0000001, 1e-8);

	/*
	// In this do loop, the body of the loop represents an iteration of
	// the minimizer. Note that if gsl_multimin_test_gradient returns
	// "GSL_SUCCESS" that we're done, or that if it returns
	// "GSL_CONTINUE" we keep going. However, first we need to get to
	// gsl_multimin_test_gradient, and to do that, we can't have
	// gsl_multimin_fdfminimizer_iterate return nonzero (apparently an
	// error condition)
	*/

	iter = 0;
	status = 0;

	do {
		iter++;
		status = gsl_multimin_fdfminimizer_iterate (s);
		if (status) break;

		status = gsl_multimin_test_gradient (s->gradient, 1e-5);
	} while (status == GSL_CONTINUE && iter < 1000);

	/* return best result regardless */
	for (i_c = 0; i_c < lp_func.n; ++i_c) {
		result[i_c] = gsl_vector_get( s->x, i_c);
	}

	if ( iter < 1000 ) {
		status = 0;
	} else {
		status = 1;
	}

	cart_free(p.predtimes);
	gsl_multimin_fdfminimizer_free(s);
	gsl_vector_free(x);

	return status;
}


double
lp_f (const gsl_vector *v, void *params)
{
  int i_r, i_c;
  double chisq_val = 0;
  double pred = 0;
  lp_params *p = (lp_params *)params;

  for (i_r = 0; i_r < p->num_data; ++i_r) {
    pred = 0;

    for (i_c = 0; i_c < p->num_predvars; ++i_c) {
      pred += p->predvars[p->num_predvars*i_r+i_c]*gsl_vector_get(v, i_c);
    }

    chisq_val += (pred - p->worktimes[i_r])*(pred - p->worktimes[i_r]);
  }

  /* Now use tanh functions to blow up the returned value if a
     coefficient's gone negative */

  for (i_c = 0; i_c < p->num_predvars; ++i_c) {
      chisq_val += lp_big_number * (1 - tanh( lp_boundary_scale*gsl_vector_get(v, i_c) ));
  }

  return chisq_val;
}


void
lp_df (const gsl_vector *v, void *params,
		gsl_vector *df)
{
	int i_r, i_c;
	double component; /* As in, gradient vector component */
	lp_params *p = (lp_params *)params;

	for (i_r = 0; i_r < p->num_data; ++i_r) {
		p->predtimes[i_r] = 0;
		for (i_c = 0; i_c < p->num_predvars; ++i_c) {
			p->predtimes[i_r] += p->predvars[p->num_predvars*i_r+i_c]*gsl_vector_get(v, i_c);
		}
	}

	for (i_c = 0; i_c < p->num_predvars; ++i_c) {
		component = 0;

		for (i_r = 0; i_r < p->num_data; ++i_r) {
			component += 2*( (p->predtimes[i_r]-p->worktimes[i_r])/p->worktimes[i_r] )*p->predvars[p->num_predvars*i_r+i_c];
		}

		component -= lp_big_number*lp_boundary_scale*(1-tanh(lp_boundary_scale*gsl_vector_get(v, i_c))*tanh(lp_boundary_scale*gsl_vector_get(v, i_c)));

		gsl_vector_set(df, i_c, component );
	}
}

/* Compute both f and df together. */
void
lp_fdf (const gsl_vector *x, void *params,
	double *f, gsl_vector *df)
{
	*f = lp_f(x, params);
	lp_df(x, params, df);
}
