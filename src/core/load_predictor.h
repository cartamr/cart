#ifndef __LOAD_PREDICTOR_H__
#define __LOAD_PREDICTOR_H__


#ifndef CONFIGURED
#error "Missing config.h include."
#endif

#define PREDVAR(var) PREDVAR_##var,
enum PREDVARS {
#include "load_predictor_variables.h"
NPREDVARS
};
#undef PREDVAR

extern char *PREDVAR_NAME[];

extern int load_predictor_flag;
extern int step_predvars[(max_level-min_level+1)*NPREDVARS];
extern float load_coeffs[(max_level-min_level+1)*NPREDVARS];

DECLARE_LEVEL_ARRAY(double, step_worktimes);
DECLARE_LEVEL_ARRAY(float, level_cost);

void config_init_load_predictor();
void config_verify_load_predictor();

void load_predictor();
float work_per_cell(int level, int refined);
float work_per_particle(int level, int star);

#endif
