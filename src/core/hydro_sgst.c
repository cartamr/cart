#include "config.h"
#ifdef SGS_TURBULENCE

#include <math.h>

#include "auxiliary.h"
#include "control_parameter.h"
#include "iterators.h"
#include "tree.h"
#include "units.h"

#include "hydro_sgst.h"


struct sgst_model_t sgst_model =
#ifdef SGST_ISOTROPIC
{ 0.000, 0.0, 0.0, 1.41, 5.0/3.0, 1e7, 0.0 };
#elif defined(SGST_SUPERSONIC)
{ 0.020, 0.7, 0.4, 1.58, 5.0/3.0, 1e7, 0.0 };
#else
{ 0.095, 0.0, 0.4, 1.58, 5.0/3.0, 1e7, 0.0 };
#endif

float sgst_v_rms_ceiling = 1e4; //in km/s

void config_init_hydro_sgst() {
#ifdef SGST_ISOTROPIC
	control_parameter_add2(control_parameter_float,&sgst_model.gamma,"sgst:gamma","sgst_model.gamma","gamma for turbulent energy. Note that anisotropic SGS model works only for gamma = 5/3");
#else
	control_parameter_add3(control_parameter_float,&sgst_model.c1,"sgst:c1","sgst_model.c1","sgs-turbulence:eddy-viscosity-coef","Eddy-viscosity closure contribution into SGS turbulent stress tensor (C_1).");

	control_parameter_add3(control_parameter_float,&sgst_model.c2,"sgst:c2","sgst_model.c2","sgs-turbulence:non-linear-viscosity-coef","Non-linear closure contribution into SGS turbulent stress tensor (C_2).");

	control_parameter_add3(control_parameter_float,&sgst_model.ck,"sgst:ck","sgst_model.ck","sgs-turbulence:diffusion-coef","SGS turbulent diffusion efficiency coefficient (C_kappa).");
#endif /* SGST_ISOTROPIC */
	control_parameter_add3(control_parameter_float,&sgst_model.ce,"sgst:ce","sgst_model.ce","sgs-turbulence:dissipation-coef","SGS turbulence dissipation rate = ce*sqrt(K/rho)/cell_size (i.e. t_dis ~ turbulent crossing time / ce).");

	control_parameter_add3(control_parameter_float,&sgst_v_rms_ceiling,"sgst:v-rms-ceiling","sgst_v_rms_ceiling","sgst-v-rms-ceiling","Maximal turbulent RMS v allowed in SGS model (in km/s). By default sgst_v_rms_ceiling = 1e4");
#ifdef SGST_SHEAR_IMPROVED
	control_parameter_add3(control_parameter_time,&sgst_model.shear_time,"sgst:shear-time","sgst_model.shear_time","sgs-turbulence:shear-time","Characteristic turbulent time-scale for SGS Shear-Improved Model (T_c).");
#endif /* SGST_SHEAR_IMPROVED */
#ifdef SGST_SN_SOURCE
	control_parameter_add3(control_parameter_double,&sgst_model.fraction_from_SN,"sgst:fraction-from-SN","sgst_model.fraction_from_SN","sgs-turbulence:fraction-from-SN","Fraction of SN energy that goes to SGS turbulence.");
#endif /* SGST_SN_SOURCE */
}

void config_verify_hydro_sgst() {
#ifdef SGST_ISOTROPIC
	VERIFY(sgst:gamma, sgst_model.gamma > 1.0);
#else /* SGST_ISOTROPIC */
	VERIFY(sgst:c1, sgst_model.c1 >= 0.0 );

	VERIFY(sgst:c2, sgst_model.c2 >= 0.0 );

	VERIFY(sgst:ck, sgst_model.ck >= 0.0 );
#endif /* SGST_ISOTROPIC */

	VERIFY(sgst:ce, sgst_model.ce >= 0.0 );

	VERIFY(sgst:v-rms-ceiling, sgst_v_rms_ceiling >= 1e4  );

#ifdef SGST_SHEAR_IMPROVED
	VERIFY(sgst:shear-time, sgst_model.shear_time >= 0.0 );
#endif /* SGST_SHEAR_IMPROVED */

#ifdef SGST_SN_SOURCE
	VERIFY(sgst:fraction-from-SN, sgst_model.fraction_from_SN >= 0.0 && sgst_model.fraction_from_SN <= 1.0 );
#endif /* SGST_SN_SOURCE */

	sgs_turbulence_gamma = sgst_model.gamma;
}

float cell_gas_turbulent_temperature (int cell) {
	if ( cell_gas_density(cell) > 0.0 ) {
	        return (sgst_model.gamma-1)*constants->wmu*cell_gas_turbulent_energy(cell)/cell_gas_density(cell);
	} else return 0.0;
}

#endif /*SGS_TURBULENCE*/
