#include "config.h"
#ifdef GRAVITY 

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "auxiliary.h"
#include "cell_buffer.h"
#include "control_parameter.h"
#include "cosmology.h"
#include "gravity.h"
#include "iterators.h"
#include "parallel.h"
#include "sfc.h"
#include "root_grid_fft.h"
#include "times.h"
#include "timing.h"
#include "tree.h"
#include "units.h"

#ifdef _OPENMP
#include <omp.h>
#endif

#include "../tools/fft/fft3.h"

extern int step;

int num_initial_smooth_iterations = 1000;
int num_initial_smooth_steps = 0; 
int num_smooth_iterations = 60;   // used to be called MAX_SOR_ITER
float spectral_radius = 0.95;     // used to be called rhoJ


void root_grid_fft_get_cell_ijk(int cell, int ijk[nDim]);
void root_grid_fft_gravity_worker(const root_grid_fft_t *config, int id, fft_t *fft_source, fft_t *fft_output, int flags);


void config_init_gravity()
{
  control_parameter_add2(control_parameter_int,&num_initial_smooth_iterations,"gravity:num-initial-iterations","num_initial_smooth_iterations","number of iterations in the gravity relaxation solver for the first num_initial_smooth_steps global timesteps");

  control_parameter_add2(control_parameter_int,&num_initial_smooth_steps,"gravity:num-initial-steps","num_initial_smooth_steps","number of global timesteps to use gravity:num-initial-iterations rather than gravity:num-iterations in the relaxation solver");

  control_parameter_add3(control_parameter_int,&num_smooth_iterations,"gravity:num-iterations","num_smooth_iterations","MAX_SOR_ITER","number of iterations in the gravity relation solver (smooth)");

  control_parameter_add3(control_parameter_float,&spectral_radius,"gravity:spectral-radius","spectral_radius","rhoJ","Jacobi spectra radius for successful overrelation iterations.");
}

void config_verify_gravity()
{
  VERIFY(gravity:num-initial-iterations, num_initial_smooth_iterations > 0);

  VERIFY(gravity:num-initial-steps, num_initial_smooth_steps >= 0);

  VERIFY(gravity:num-iterations, num_smooth_iterations > 0 );

  VERIFY(gravity:spectral-radius, spectral_radius>0.0 && spectral_radius<1.0 );
}


void solve_poisson( int level, int flag ) {
	const int potential_vars[1] = { VAR_POTENTIAL };

	cart_assert( level >= min_level && level <= max_level );

	start_time( GRAVITY_TIMER );

	if ( level == min_level ) {
		root_grid_fft_exec(VAR_TOTAL_MASS,1,potential_vars,root_grid_fft_gravity_worker);
	} else {
		relax( level, flag );
	}

	end_time( GRAVITY_TIMER );
}

void relax( int level, int flag ) {
	if ( level > min_level && flag == 0 ) {
		prolongate( level );
	}
		
	smooth( level );
}

void prolongate( int level ) {
	int i,j;
	const int prolongation_vars[1] = { VAR_POTENTIAL };
	int icell;
	int num_level_cells;
	int *level_cells;

	cart_assert( level >= min_level+1 );

	start_time( WORK_TIMER );

	select_level( level-1, CELL_TYPE_LOCAL | CELL_TYPE_REFINED, &num_level_cells, &level_cells );
#pragma omp parallel for default(none), private(i,icell,j), shared(num_level_cells,level_cells,cell_vars_data,cell_child_oct)
	for ( i = 0; i < num_level_cells; i++ ) {
		icell = level_cells[i];

		for ( j = 0; j < num_children; j++ ) {
			cell_potential( cell_child(icell,j) ) = cell_interpolate( icell, j, VAR_POTENTIAL );
		}
	}
	cart_free( level_cells );

	end_time( WORK_TIMER );

	/* update buffers */
	start_time( PROLONGATE_UPDATE_TIMER );
	update_buffer_level( level, prolongation_vars, 1 );
	end_time( PROLONGATE_UPDATE_TIMER );
}

void smooth( int level ) {
	int niter;
	int i, j, k;
	int num_local_blocks;
	int num_direct_blocks;
	int num_indirect_blocks;
	int num_blocks;
	double *phi_red, *phi_black;
	double *rho_red, *rho_black;
	int *ext_red, *ext_black;
	int coords[nDim], coords_neighbor[nDim];
	int neighbor, direction, direct;
	int *oct_list;
	int max_red_border_cells;
	int max_black_border_cells;
	int cur_oct, block, child, icell, ioct;
	int proc;
	int num_send_octs[MAX_PROCS];
	int num_recv_octs[MAX_PROCS];
	int *send_indices[MAX_PROCS];
	int *recv_indices[MAX_PROCS];
	double *buffer_red[MAX_PROCS];
	double *buffer_black[MAX_PROCS];
	double *packed_red[MAX_PROCS];
	double *packed_black[MAX_PROCS];
	int num_sendrequests;
	int num_recvrequests;
	MPI_Request send_requests[MAX_PROCS];
	MPI_Request recv_requests[MAX_PROCS];

	const int block_index[num_children] = {
		#if nDim == 3 
			0, 0, 1, 1, 2, 2, 3, 3
		#else
			#error "Unknown nDim in block_index (smooth)"
		#endif
	};

	const int color[num_children] = {
		#if nDim == 3
			0, 1, 1, 0, 1, 0, 0, 1
		#else
			#error "Unknown nDim in color (smooth)"
		#endif
	};

	const int smooth_vars[1] = { VAR_POTENTIAL };

#ifdef GRAVITY_NO_GLOBAL_ARRAY
    int *ind = cart_alloc(int, num_octs);
#else
    static int ind[num_octs];
#endif /* GRAVITY_NO_GLOBAL_ARRAY */

	cart_assert ( level > min_level );

	start_time( SMOOTH_TIMER );
	start_time( SMOOTH_SETUP_TIMER );
	start_time( WORK_TIMER );

	select_level_octs_bydirect( level, &num_local_blocks, &num_direct_blocks,
		&num_indirect_blocks, &oct_list);

	num_blocks = num_local_blocks + num_direct_blocks + num_indirect_blocks;

#ifdef DEBUG
	/* set all ind entries to -1 to check for faulty lookups */
#pragma omp parallel for default(none), shared(ind)
	for ( i = 0; i < num_octs; i++ ) {
		ind[i] = -1;
	}
#endif /* DEBUG */

#pragma omp parallel for default(none), shared(ind,oct_list,num_blocks)
	for ( i = 0; i < num_blocks; i++ ) {
		ind[oct_list[i]] = i;
	}

	for ( proc = 0; proc < num_procs; proc++ ) {
		num_recv_octs[proc] = num_local_buffers[level][proc];
		if ( num_recv_octs[proc] > 0 ) {
			recv_indices[proc] = cart_alloc(int, num_recv_octs[proc]);

			/* convert from local oct indices to packed array indices */
			for ( i = 0; i < num_recv_octs[proc]; i++ ) {
				recv_indices[proc][i] = (num_children/2)*ind[local_buffers[level][proc][i]];
			}
		}

		num_send_octs[proc] = num_remote_buffers[level][proc];
		if ( num_send_octs[proc] > 0 ) {
			send_indices[proc] = cart_alloc(int, num_send_octs[proc]);

			/* convert from local oct indices to packed array indices */
			for ( i = 0; i < num_send_octs[proc]; i++ ) {
				send_indices[proc][i] = (num_children/2)*ind[remote_buffers[level][proc][i]];
			}
		}
	}

	ext_red = cart_alloc(int, num_children*(num_local_blocks+num_direct_blocks)*nDim);
	ext_black = &ext_red[(num_children/2)*(num_local_blocks+num_direct_blocks)*nDim];

	max_red_border_cells = 0;
	max_black_border_cells = 0;

#pragma omp parallel
	{
#ifdef _OPENMP
		int nt = omp_get_num_threads();
		int it = omp_get_thread_num();
#else
		int nt = 1;
		int it = 0;
#endif

		int i, j, k, cur_oct;
		int neighbor, direction, direct, block, icell, ioct;

		int tot, len, ls, le;

		tot = num_local_blocks;
		len = tot/nt;
		ls = it*len;
		le = (it==nt-1) ? tot : (ls+len);

		int num_red_border_cells = 0;
		int num_black_border_cells = 0;

		/* compute neighbors for local blocks */
		for ( i = ls; i < le; i++ ) {
			cur_oct = oct_list[i];

			for ( j = 0; j < num_children; j++ ) {
				icell = (num_children/2)*i+block_index[j];

				for ( k = 0; k < nDim; k++ ) {
					direction = external_direction[j][k];
					neighbor = oct_neighbors[cur_oct][direction];
					cart_assert( neighbor != NULL_OCT );
					if ( cell_level(neighbor) != level-1 ) {
					        cart_debug("cur_oct = %d, direction = %d", cur_oct, direction );
						cart_debug("neighbor = %d", neighbor );
						cart_debug("level = %d", level );
						cart_debug("cell_level(neighbor) = %d", cell_level(neighbor) );
						cart_debug("local? %d", cell_is_local(neighbor));
					}
					cart_assert( cell_level(neighbor) == level-1 );

					if ( cell_is_refined(neighbor) ) {
						block = ind[cell_child_oct[neighbor]];
						cart_assert( block >= 0 && block < num_blocks );

						if ( color[j] == 0 ) {
							ext_red[nDim*icell+k] = (num_children/2)*block + block_index[ local[j][direction] ];
						} else {
							ext_black[nDim*icell+k] = (num_children/2)*block + block_index[ local[j][direction] ];
						}
					} else {
						if ( color[j] == 0 ) {
							ext_red[nDim*icell+k] = (num_children/2)*num_blocks + num_black_border_cells*nt+it;
							num_black_border_cells++;
						} else {
							ext_black[nDim*icell+k] = (num_children/2)*num_blocks + num_red_border_cells*nt+it;
							num_red_border_cells++;
						}
					}
				}
			}
		}

		tot = num_direct_blocks;
		len = tot/nt;
		ls = it*len + num_local_blocks;
		le = (it==nt-1) ? (num_local_blocks + tot) : (ls + len);

		/* compute neighbors for red direct blocks */
		for ( i = ls; i < le; i++ ) {
			cur_oct = oct_list[i];

			for ( j = 0; j < num_children; j++ ) {
				if ( color[j] == 0 ) {
					icell = (num_children/2)*i+block_index[j];

					for ( k = 0; k < nDim; k++ ) {
						direction = external_direction[j][k];
						neighbor = oct_neighbors[cur_oct][direction];
						cart_assert( neighbor != NULL_OCT );
						cart_assert( cell_level(neighbor) == level-1 );

						if ( cell_is_local(neighbor) ) {
							break;
						}
					}

					if ( k < nDim ) {
						for ( k = 0; k < nDim; k++ ) {
							direction = external_direction[j][k];
							neighbor = oct_neighbors[cur_oct][direction];

							cart_assert( neighbor != NULL_OCT );
							cart_assert( cell_level(neighbor) == level-1 );

							if ( cell_is_refined(neighbor) ) {
								block = ind[cell_child_oct[neighbor]];
								cart_assert( block >= 0 && block < num_blocks );

								ext_red[nDim*icell+k] = (num_children/2)*block + block_index[ local[j][direction] ];
							} else {
								ext_red[nDim*icell+k] = (num_children/2)*num_blocks + num_black_border_cells*nt+it;
								num_black_border_cells++;
							}
						}
					} else {
						for ( k = 0; k < nDim; k++ ) {
							ext_red[nDim*icell+k] = 0;
						}
					}
				}
			}
		}

#pragma omp critical(max_border_cells)
		{
			if ( num_red_border_cells > max_red_border_cells ) {
				max_red_border_cells = num_red_border_cells;
			}

			if ( num_black_border_cells > max_black_border_cells ) {
				max_black_border_cells = num_black_border_cells;
			}
		}

		num_red_border_cells = 0;
		num_black_border_cells = 0;

#pragma omp barrier
#pragma omp master
		{
			/* now allocate space for cell values (since we know the size exactly) */
			phi_red = cart_alloc(double, num_children*num_blocks + (max_red_border_cells+max_black_border_cells)*nt);
			phi_black = &phi_red[(num_children/2)*num_blocks + max_red_border_cells*nt];
		}
#pragma omp barrier

		tot = num_local_blocks;
		len = tot/nt;
		ls = it*len;
		le = (it==nt-1) ? tot : (ls+len);

		/* compute border cell values */
		for ( i = ls; i < le; i++ ) {
			cur_oct = oct_list[i];

			for ( j = 0; j < num_children; j++ ) {
				icell = (num_children/2)*i+block_index[j];

				for ( k = 0; k < nDim; k++ ) {
					direction = external_direction[j][k];
					neighbor = oct_neighbors[cur_oct][direction];
					cart_assert( neighbor != NULL_OCT );
					cart_assert( cell_level(neighbor) == level-1 );

					if ( cell_is_leaf(neighbor) ) {
						if ( color[j] == 0 ) {
							phi_black[(num_children/2)*num_blocks+num_black_border_cells*nt+it] =
								cell_interpolate( neighbor, local[j][direction], VAR_POTENTIAL );
							num_black_border_cells++;
						} else {
							phi_red[(num_children/2)*num_blocks+num_red_border_cells*nt+it] =
								cell_interpolate( neighbor, local[j][direction], VAR_POTENTIAL );
							num_red_border_cells++;
						}
					}
				}
			}
		}

		tot = num_direct_blocks;
		len = tot/nt;
		ls = it*len + num_local_blocks;
		le = (it==nt-1) ? (tot + num_local_blocks) : (ls + len);

		/* compute values for neighbors of red direct blocks */
		for ( i = ls; i < le; i++ ) {
			cur_oct = oct_list[i];

			for ( j = 0; j < num_children; j++ ) {
				if ( color[j] == 0 ) {
					icell = (num_children/2)*i+block_index[j];

					for ( k = 0; k < nDim; k++ ) {
						direction = external_direction[j][k];
						neighbor = oct_neighbors[cur_oct][direction];
						cart_assert( neighbor != NULL_OCT );
						cart_assert( cell_level(neighbor) == level-1 );

						if ( cell_is_local(neighbor) ) {
							break;
						}
					}

					if ( k < nDim ) {
						for ( k = 0; k < nDim; k++ ) {
							direction = external_direction[j][k];
							neighbor = oct_neighbors[cur_oct][direction];
							cart_assert( neighbor != NULL_OCT );
							cart_assert( cell_level(neighbor) == level-1 );

							if ( cell_is_leaf(neighbor) ) {
								phi_black[(num_children/2)*num_blocks+num_black_border_cells*nt+it] =
									cell_interpolate( neighbor, local[j][direction], VAR_POTENTIAL );
								num_black_border_cells++;
							}
						}
					}
				}
			}
		}
	} /* end of pragma parallel */

#ifdef GRAVITY_NO_GLOBAL_ARRAY
	cart_free( ind );
#endif /* GRAVITY_NO_GLOBAL_ARRAY */

	rho_red = cart_alloc(double, num_children*num_blocks);
	rho_black = &rho_red[(num_children/2)*num_blocks];

	/* pack cell_blocks */
#pragma omp parallel for default(none), private(i,j,child), shared(num_blocks,oct_list,phi_red,phi_black,cell_vars_data,rho_red,rho_black)
	for ( i = 0; i < num_blocks; i++ ) {
		j = (num_children/2)*i;
		child = oct_child(oct_list[i],0);

		/* child 0 (red) */
		phi_red[j]      = cell_potential(child);
		rho_red[j]      = cell_total_mass(child);

		/* child 1 (black) */
		phi_black[j]    = cell_potential(child+1);
		rho_black[j]    = cell_total_mass(child+1);

		/* child 2 (black) */
		phi_black[j+1]  = cell_potential(child+2);
		rho_black[j+1]  = cell_total_mass(child+2);

		/* child 3 (red) */
		phi_red[j+1]    = cell_potential(child+3);
		rho_red[j+1]    = cell_total_mass(child+3);

		/* child 4 (black) */
		phi_black[j+2]  = cell_potential(child+4);
		rho_black[j+2]  = cell_total_mass(child+4);

		/* child 5 (red) */
		phi_red[j+2]    = cell_potential(child+5);
		rho_red[j+2]    = cell_total_mass(child+5);

		/* child 6 (red) */
		phi_red[j+3]    = cell_potential(child+6);
		rho_red[j+3]    = cell_total_mass(child+6);

		/* child 7 (black) */
		phi_black[j+3]  = cell_potential(child+7);
		rho_black[j+3]  = cell_total_mass(child+7);
	}

	end_time( WORK_TIMER );

	/* create communications objects for iteration steps */
	start_time( COMMUNICATION_TIMER );

	num_sendrequests = 0;
	num_recvrequests = 0;
	for ( proc = 0; proc < num_procs; proc++ ) {
		if ( num_send_octs[proc] > 0 ) {
			packed_red[proc] = cart_alloc(double, num_children*num_send_octs[proc]);
			packed_black[proc] = &packed_red[proc][(num_children/2)*num_send_octs[proc]];

			MPI_Send_init( packed_red[proc], num_children*num_send_octs[proc], MPI_DOUBLE, proc, 0,
					mpi.comm.run, &send_requests[num_sendrequests++] );
		}

		if ( num_recv_octs[proc] > 0 ) {
			buffer_red[proc] = cart_alloc(double, num_children*num_recv_octs[proc]);
			buffer_black[proc] = &buffer_red[proc][(num_children/2)*num_recv_octs[proc]];

			MPI_Recv_init( buffer_red[proc], num_children*num_recv_octs[proc], MPI_DOUBLE,
					proc, 0, mpi.comm.run, &recv_requests[num_recvrequests++] );
		}
	}

	end_time( COMMUNICATION_TIMER );
	end_time( SMOOTH_SETUP_TIMER );

	niter = ( step < num_initial_smooth_steps ) ? 
				num_initial_smooth_iterations : num_smooth_iterations;

#pragma omp parallel 
	{
#ifdef _OPENMP
		int nt = omp_get_num_threads();
		int it = omp_get_thread_num();
#else
		int nt = 1;
		int it = 0;
#endif

		int tot, len, ls, le;
		int block, proc, iter, icell;
		int i, j, k;

		double phi_ext_neighbors;
		double phi0, phi1, phi2, phi3, phi4, phi5, phi6, phi7;

		double wsor = 2.0; /* done for wsor_(1/2) calc, really == 1.0 */
		double wsor6 = 1.0 / 6.0;
		double trfi2 = units->potential * cell_size_inverse[level];

		/* work loop */
		for ( iter = 0; iter < niter; iter++ ) {

			if ( iter > 0 ) {
#pragma omp barrier
#pragma omp master
				{
					start_time( COMMUNICATION_TIMER );

					start_time( SMOOTH_COMMUNICATION_TIMER );
					MPI_Startall( num_recvrequests, recv_requests );
					end_time( SMOOTH_COMMUNICATION_TIMER );
				}

				/* pack into arrays for communication */
				for ( proc = 0; proc < num_procs; proc++ ) {
#pragma omp for nowait
					for ( i = 0; i < num_send_octs[proc]; i++ ) {
						j = send_indices[proc][i];
						k = (num_children/2)*i;

						packed_red[proc][k]   = phi_red[j];
						packed_red[proc][k+1] = phi_red[j+1];
						packed_red[proc][k+2] = phi_red[j+2];
						packed_red[proc][k+3] = phi_red[j+3];

						packed_black[proc][k]   = phi_black[j];
						packed_black[proc][k+1] = phi_black[j+1];
						packed_black[proc][k+2] = phi_black[j+2];
						packed_black[proc][k+3] = phi_black[j+3];
					}
				}

#pragma omp barrier
#pragma omp master
				{
					/* do communication here */
					start_time( SMOOTH_COMMUNICATION_TIMER );
					MPI_Startall( num_sendrequests, send_requests );
					MPI_Waitall( num_recvrequests, recv_requests, MPI_STATUSES_IGNORE );
					end_time( SMOOTH_COMMUNICATION_TIMER );
				}
#pragma omp barrier

				/* now move from buffers to actual array */
				for ( proc = 0; proc < num_procs; proc++ ) {
#pragma omp for nowait
					for ( i = 0; i < num_recv_octs[proc]; i++ ) {
						j = recv_indices[proc][i];
						k = (num_children/2)*i;

						phi_red[j]   = buffer_red[proc][k];
						phi_red[j+1] = buffer_red[proc][k+1];
						phi_red[j+2] = buffer_red[proc][k+2];
						phi_red[j+3] = buffer_red[proc][k+3];

						phi_black[j]   = buffer_black[proc][k];
						phi_black[j+1] = buffer_black[proc][k+1];
						phi_black[j+2] = buffer_black[proc][k+2];
						phi_black[j+3] = buffer_black[proc][k+3];
					}
				}

/* barrier only needed for timing */
#pragma omp barrier
#pragma omp master
				{
					end_time( COMMUNICATION_TIMER );
				}
			}

#pragma omp master
			{
				start_time( WORK_TIMER );
			}

			/* red (local red and buffered red) */
			tot = num_local_blocks + num_direct_blocks;
			len = tot/nt;
			ls = it*len;
			le = (it==nt-1) ? tot : (ls+len);

			for ( i = ls; i < le; ++i ) {
				block = (num_children/2)*i;

				phi1 = phi_black[block];
				phi2 = phi_black[block+1];
				phi4 = phi_black[block+2];
				phi7 = phi_black[block+3];

				/* child 0 */
				icell = block;
				phi_ext_neighbors = phi_black[ext_red[nDim*icell]]
					+   phi_black[ext_red[nDim*icell+1]] 
					+   phi_black[ext_red[nDim*icell+2]];
				phi_red[icell] = phi_red[icell] 
					+ wsor6 * ( phi_ext_neighbors + phi1 + phi2 + phi4 - 6.0*phi_red[icell] )
					- trfi2 * rho_red[icell];

				/* child 3 */
				icell = icell+1;  /* block + 1 */
				phi_ext_neighbors = phi_black[ext_red[nDim*icell]]
					+   phi_black[ext_red[nDim*icell+1]]
					+   phi_black[ext_red[nDim*icell+2]];
				phi_red[icell] = phi_red[icell]
					+ wsor6 * ( phi_ext_neighbors + phi1 + phi2 + phi7 - 6.0*phi_red[icell] )
					- trfi2 * rho_red[icell];

				/* child 5 */
				icell = icell+1;  /* block + 2 */
				phi_ext_neighbors = phi_black[ext_red[nDim*icell]]
					+   phi_black[ext_red[nDim*icell+1]]
					+   phi_black[ext_red[nDim*icell+2]];
				phi_red[icell] = phi_red[icell]
					+ wsor6 * ( phi_ext_neighbors + phi1 + phi4 + phi7 - 6.0*phi_red[icell] )
					- trfi2 * rho_red[icell];

				/* child 6 */
				icell = icell+1;  /* block + 3 */
				phi_ext_neighbors = phi_black[ext_red[nDim*icell]]
					+   phi_black[ext_red[nDim*icell+1]]
					+   phi_black[ext_red[nDim*icell+2]];
				phi_red[icell] = phi_red[icell]
					+ wsor6 * ( phi_ext_neighbors + phi2 + phi4 + phi7 - 6.0*phi_red[icell] )
					- trfi2 * rho_red[icell];
			}

			/* chebyshev acceleration */
			wsor = 1.0 / ( 1.0 - 0.25 * spectral_radius*spectral_radius*wsor );
			wsor6 = wsor / 6.0;
			trfi2 = wsor * units->potential * cell_size_inverse[level];

			/* black (just local black cells) */
			tot = num_local_blocks;
			len = tot/nt;
			ls = it*len;
			le = (it==nt-1) ? tot : (ls+len);

#pragma omp barrier
			for ( i = ls; i < le; ++i ) {
				block = (num_children/2)*i;

				phi0 = phi_red[block];
				phi3 = phi_red[block+1];
				phi5 = phi_red[block+2];
				phi6 = phi_red[block+3];

				/* child 1 */
				icell = block;
				phi_ext_neighbors = phi_red[ext_black[nDim*icell]]
					+   phi_red[ext_black[nDim*icell+1]]
					+   phi_red[ext_black[nDim*icell+2]];
				phi_black[icell] = phi_black[icell]
					+ wsor6 * ( phi_ext_neighbors + phi0 + phi3 + phi5 - 6.0*phi_black[icell] )
					- trfi2 * rho_black[icell];

				/* child 2 */
				icell = icell+1;  /* block + 1 */
				phi_ext_neighbors = phi_red[ext_black[nDim*icell]]
					+   phi_red[ext_black[nDim*icell+1]]
					+   phi_red[ext_black[nDim*icell+2]];
				phi_black[icell] = phi_black[icell]
					+ wsor6 * ( phi_ext_neighbors + phi0 + phi3 + phi6 - 6.0*phi_black[icell] )
					- trfi2 * rho_black[icell];

				/* child 4 */
				icell = icell+1;  /* block + 2 */
				phi_ext_neighbors = phi_red[ext_black[nDim*icell]]
					+   phi_red[ext_black[nDim*icell+1]]
					+   phi_red[ext_black[nDim*icell+2]];
				phi_black[icell] = phi_black[icell]
					+ wsor6 * ( phi_ext_neighbors + phi0 + phi5 + phi6 - 6.0*phi_black[icell] )
					- trfi2 * rho_black[icell];

				/* child 7 */
				icell = icell+1;  /* block + 3 */
				phi_ext_neighbors = phi_red[ext_black[nDim*icell]]
					+   phi_red[ext_black[nDim*icell+1]]
					+   phi_red[ext_black[nDim*icell+2]];
				phi_black[icell] = phi_black[icell]
					+ wsor6 * ( phi_ext_neighbors + phi3 + phi5 + phi6 - 6.0*phi_black[icell] )
					- trfi2 * rho_black[icell];
			}

			/* chebyshev acceleration */
			wsor = 1.0 / ( 1.0 - 0.25 * spectral_radius*spectral_radius*wsor );
			wsor6 = wsor / 6.0;
			trfi2 = wsor * units->potential * cell_size_inverse[level];

#pragma omp barrier
#pragma omp master
			{
				end_time( WORK_TIMER );

				if ( iter > 0 ) {
					/* wait for packed sends to complete */
					start_time( COMMUNICATION_TIMER );
					start_time( SMOOTH_COMMUNICATION_TIMER );
					MPI_Waitall( num_sendrequests, send_requests, MPI_STATUSES_IGNORE );
					end_time( SMOOTH_COMMUNICATION_TIMER );
					end_time( COMMUNICATION_TIMER );
				}
			}
		}
	} /* end of omp parallel */

	/* write back to cell array */
	start_time( WORK_TIMER );

#pragma omp parallel for default(none), private(i,j,child), shared(num_local_blocks,oct_list,cell_vars_data,phi_red,phi_black)
	for ( i = 0; i < num_local_blocks; i++ ) {
		j = (num_children/2)*i;
		child = oct_child(oct_list[i],0);

		/* child 0 (red) */
		cell_potential(child) = phi_red[j];

		/* child 1 (black) */
		cell_potential(child+1) = phi_black[j];

		/* child 2 (black) */
		cell_potential(child+2) = phi_black[j+1];

		/* child 3 (red) */
		cell_potential(child+3) = phi_red[j+1];

		/* child 4 (black) */
		cell_potential(child+4) = phi_black[j+2];

		/* child 5 (red) */
		cell_potential(child+5) = phi_red[j+2];

		/* child 6 (red) */
		cell_potential(child+6) = phi_red[j+3];

		/* child 7 (black) */
		cell_potential(child+7) = phi_black[j+3];
	}

	/* free MPI_Requests */
	for ( i = 0; i < num_sendrequests; i++ ) {
		MPI_Request_free( &send_requests[i] );
	}

	for ( i = 0; i < num_recvrequests; i++ ) {
		MPI_Request_free( &recv_requests[i] );
	}

	/* cleanup allocated memory */
	cart_free( ext_red );
	cart_free( phi_red );
	cart_free( rho_red );
	cart_free( oct_list );

	for ( proc = 0; proc < num_procs; proc++ ) {
		if ( num_send_octs[proc] > 0 ) {
			cart_free( send_indices[proc] );
			cart_free( packed_red[proc] );
		}

		if ( num_recv_octs[proc] > 0 ) {
			cart_free( recv_indices[proc] );
			cart_free( buffer_red[proc] );
		}
	}

	end_time( WORK_TIMER );
	end_time( SMOOTH_TIMER );

	/* update buffer cells */
	start_time( SMOOTH_UPDATE_TIMER );
	update_buffer_level( level, smooth_vars, 1 );
	end_time( SMOOTH_UPDATE_TIMER );
}

void restrict_to_level( int level ) {
	int i,j;
	double sum;
	const double rfw = 1.0 / (double)num_children;
	const int restrict_vars[1] = { VAR_POTENTIAL };

	int icell;
	int num_level_cells;
	int *level_cells;

	start_time( GRAVITY_TIMER );
	start_time( WORK_TIMER );

	select_level( level, CELL_TYPE_LOCAL | CELL_TYPE_REFINED, &num_level_cells, &level_cells );
#pragma omp parallel for default(none), private(i,icell,sum,j), shared(num_level_cells,level_cells,cell_child_oct,cell_vars_data)
	for ( i = 0; i < num_level_cells; i++ ) {
		icell = level_cells[i];
		sum = 0.0;
		for ( j = 0; j < num_children; j++ ) {
			sum += cell_potential(cell_child(icell,j));
		}

		cell_potential(icell) = sum*rfw;
	}
	cart_free( level_cells );

	end_time( WORK_TIMER );

	/* update buffers */
	start_time( RESTRICT_UPDATE_TIMER );
	update_buffer_level( level, restrict_vars, 1 );
	end_time( RESTRICT_UPDATE_TIMER );

	end_time( GRAVITY_TIMER );
}


void root_grid_fft_gravity_worker(const root_grid_fft_t *config, int id, fft_t *fft_source, fft_t *fft_output, int flags)
{
  int i, j, k, jk[2];
  double G, G_jk;
  double green[num_grid];
  double lambda;
  double trphi;
  size_t offset;

  cart_assert(config->bbox[0]==0 && config->bbox[1]==num_grid);

  trphi = -1.5*units->potential/(num_grid*num_grid*num_grid);

  /* precompute G(k) */
  lambda = M_PI/num_grid;
#pragma omp parallel for default(none), private(i), shared(green,lambda)
  for(i=0; i<num_grid; i++)
    {
      green[i] = sin(lambda*i)*sin(lambda*i);
    }

#pragma omp parallel for default(none), private(i,j,k,G,G_jk,offset,jk), shared(config,green,fft_source,fft_output,flags,trphi)
  for(k=0; k<config->dims[2]; k++)
    {
      for(j=0; j<config->dims[1]; j++)
	{
	  offset = fft3_jk_index(config->plan,j,k,jk,flags);
	  if(offset == (size_t)-1) continue;

	  G_jk = green[jk[0]] + green[jk[1]];
	  offset *= config->dims[0];

	  for(i=0; i<=num_grid/2; i++)
	    {
	      if(i==0 && jk[0]==0 && jk[1]==0)
		{
		  G = 0.0;
		}
	      else
		{
		  G = trphi/(G_jk+green[i]);
		}

	      fft_output[2*i+0+offset] = G*fft_source[2*i+0+offset];
	      fft_output[2*i+1+offset] = G*fft_source[2*i+1+offset];
	    }
	}
    }
}

#endif /* GRAVITY */

