#ifndef __CLUSTER_DESTRUCTION_H__
#define __CLUSTER_DESTRUCTION_H__

#ifndef CONFIGURED
#error "Missing config.h include."
#endif

#if defined(PARTICLES) && defined(STAR_FORMATION) && defined(CLUSTER_BOUND_FRACTION)

#include "particle.h"

void config_init_cluster_destruction();
void config_verify_cluster_destruction();
void setup_cluster_destruction();

void single_cluster_destruction(int level, int icell, int ipart, double dt);

#endif /* PARTICLE && STAR_FORMATION && CLUSTER_BOUND_FRACTION */

#endif
