#include "config.h"
#if defined(PARTICLES) && defined(STAR_FORMATION) && defined(CLUSTER_BOUND_FRACTION)

#include <math.h>

#include "auxiliary.h"
#include "control_parameter.h"
#include "cosmology.h"
#include "hydro.h"
#include "parallel.h"
#include "particle.h"
#include "times.h"
#include "tree.h"
#include "units.h"
#include "starformation.h"

extern double cluster_age_spread_code;

void GetCubeStencil(int level, int cell, int *nb);
void eigenvalue_wiki( double A[6], double *l );
double max3( double x, double y, double z);

struct
{
    double critical_mass;
    double mass_slope;
    double tidaltime_norm;
    double isotime_norm;
}
cd = {2e5, 2./3., 10., 17.};

double critical_mass_code;
double tidaltime_norm_code;
double isotime_norm_code;

double p_tid_norm=100.0; /* see eq. (4) of Gnedin, Ostriker & Tremaine 2014.
                            changed from 41.4 to 100 on Feb 12 2019 by Gillen Brown. */
double p_tid_norm_code;

void config_init_cluster_destruction()
{
  /*
  //  General parameters
  */
  control_parameter_add2(control_parameter_int,&cd.critical_mass,"cd:critical-mass","cd_critical_mass","critical mass (in Msun) for tidal timescale. see Gnedin, Ostriker & Tremaine 2014 eq. (4)");
  control_parameter_add2(control_parameter_double,&cd.mass_slope,"cd:mass-slope","cd_mass_slope","the slope of mass dependency for tidal timescale. see Gnedin, Ostriker & Tremaine 2014 eq. (4)");
  control_parameter_add2(control_parameter_int,&cd.tidaltime_norm,"cd:tidaltime-norm","cd_tidaltime_norm","The normalization of tidal timescale (in Gyr) at critical mass. see Gnedin, Ostriker & Tremaine 2014 eq. (4)");
  control_parameter_add2(control_parameter_int,&cd.isotime_norm,"cd:isotime-norm","cd_isotime_norm","The normalization of half-mass relaxation timescale (in Gyr) at critical mass. see Gnedin, Ostriker & Tremaine 2014 eq. (5)");
}

void config_verify_cluster_destruction()
{
    VERIFY(cd:critical-mass, cd.critical_mass>=0.0);
    VERIFY(cd:mass-slope, cd.mass_slope>=0.0);
    VERIFY(cd:tidaltime-norm, cd.tidaltime_norm>=0.0);
    VERIFY(cd:isotime-norm, cd.isotime_norm>=0.0);
}

void setup_cluster_destruction()
{
    critical_mass_code = cd.critical_mass * constants->Msun/units->mass;
    tidaltime_norm_code = cd.tidaltime_norm * constants->Gyr/units->time;
    isotime_norm_code = cd.isotime_norm * constants->Gyr/units->time;
    p_tid_norm_code = p_tid_norm*constants->kms/constants->kpc*units->time;
}

double initial_bound_fraction(double eps_int){
    /**
     * Calculate the bound fraction of a cluster based on its star formation
     * efficiency. Uses the prescription from H. Li et al 2019 (MNRAS submitted)
     * Equation 17. The best fit parameters are listed below in the text.
     */
    double alpha_star = 0.48;
    double f_sat = 0.94;
    double f_bound;
    f_bound = (erf(sqrt(3 * eps_int / alpha_star)) -
              (sqrt(12 * eps_int / (3.14159265358979 * alpha_star)) *
              exp(-3 * eps_int / alpha_star))) * f_sat;
    return MIN(MAX(f_bound, 0.0), 1.0);  // restrict to 0 <= f_bound <= 1
}

/* Three ways of estimating the strength of the tidal destruction:
 * 1. maximum of the absolute eigenvalue of the tidal tensor
 * 2. maximum of the diagonal element
 * 3. trace */
void single_cluster_destruction(int level, int icell, int ipart, double dt)
{
    int j;
    int mode = 0; /* default mode is to use the eigen value of tidal tensor */
    int neighbors[num_neighbors];
    int L1, R1;
    double phi_l, phi_r, phi_c, phi_ii;
    double p_tid, t_tid, t_iso;
    double M_bound;
    double star_age;
    double l2_inv;
    double f_bound_initial, eps_int, eps_core = 0.5; /* correction factor of star formation efficiency due to protostellar outflow */

    int CubeStencilSize = 26;
    int nb26[CubeStencilSize];
    double phis[CubeStencilSize];
    double phi_xx, phi_yy, phi_zz, phi_xy, phi_xz, phi_yz;
    double tidal_tensor[6], eigenvalue[3];

    /* skip cluster that has already been destructed */
    cart_assert( star_fbound[ipart]>1e-6 );
    /* skip low mass clusters */
    if ( star_initial_mass[ipart]*units->mass/constants->Msun<1.0 ) return;

    l2_inv = cell_size_inverse[level]*cell_size_inverse[level];

    phi_c = cell_potential(icell);
    phi_ii = 0.0;

    GetCubeStencil(level, icell, nb26);
    for ( j=0; j<CubeStencilSize; j++ )
    {
        /* if cannot get potential of all relevant neighbors,
         * try to use the maximum diag element of the tensor */
        if ( j<18 && nb26[j]==-1 ) { mode = 1; break; }
        else phis[j] = cell_potential(nb26[j]);
    }

    if ( mode==0 )
    {
        /* 1. eigenvalue of the tidal tensor */
        for ( j=0; j<18; j++ ) cart_assert( phis[j] != 0.0 );

        tidal_tensor[0] = (phis[0]+phis[1]-2*phi_c)*l2_inv;
        tidal_tensor[3] = (phis[2]+phis[3]-2*phi_c)*l2_inv;
        tidal_tensor[5] = (phis[4]+phis[5]-2*phi_c)*l2_inv;
        tidal_tensor[1] = 0.25*(phis[6]+phis[9]-phis[7]-phis[8])*l2_inv;
        tidal_tensor[2] = 0.25*(phis[10]+phis[15]-phis[11]-phis[14])*l2_inv;
        tidal_tensor[4] = 0.25*(phis[12]+phis[17]-phis[13]-phis[16])*l2_inv;
        eigenvalue_wiki(tidal_tensor, eigenvalue);
        phi_ii = max3(fabs(eigenvalue[0]),fabs(eigenvalue[1]), fabs(eigenvalue[2]));
    }
    else if ( mode==1 )
    {
        /* 2. maximum element along the diag */
        cell_all_neighbors(icell, neighbors);
        for ( j=0; j<nDim; j++ )
        {
            L1 = neighbors[2*j];
            R1 = neighbors[2*j+1];

            phi_l = cell_potential(L1);
            phi_r = cell_potential(R1);
            phi_ii = MAX( fabs(phi_l+phi_r-2*phi_c)*l2_inv, phi_ii );
        }
    }
    else if ( mode==2 )
    {
        /* 3. trace of the tidal matrix */
        cell_all_neighbors(icell, neighbors);
        for ( j=0; j<num_neighbors; j++ )
        { phi_ii += cell_potential(neighbors[j]); }
        phi_ii = fabs(phi_ii-num_neighbors*phi_c)*cell_size_inverse[level]*cell_size_inverse[level];
    }
    else cart_debug("only support three modes!");

    cart_assert( phi_ii>0.0 );

    /* correct the unit of potential here with abox */
    phi_ii *= abox[level] * abox[level];

    p_tid = sqrt(3./phi_ii) * p_tid_norm_code;

#ifdef CLUSTER_INITIAL_BOUND
    /* note that star_ibound is the maximum baryonic mass with sphere during the course of cluster accretion,
     * rather than the actual initial bound fraction. A conversion shown below is needed. */
    /* if (star_ibound[ipart]<star_initial_mass[ipart]) star_ibound[ipart] = star_initial_mass[ipart]; */
    cart_assert(star_ibound[ipart]>=star_initial_mass[ipart]);
    /* Initial bound fraction prescription changed March 6 2019 by Gillen Brown
     * Now cotains the presciption found in H. Li et al 2019 (MNRAS submitted)
     * Old prescription is kept but commented out
     */
    // f_bound_initial = MIN( star_initial_mass[ipart] / star_ibound[ipart] / eps_core, 1.0 ); // old prescription
    eps_int = star_initial_mass[ipart] / star_ibound[ipart];
    f_bound_initial = initial_bound_fraction(eps_int);

    M_bound = star_initial_mass[ipart] * f_bound_initial * star_fbound[ipart];
#else
    M_bound = star_initial_mass[ipart] * star_fbound[ipart];
    /* M_bound = particle_mass[ipart] * star_fbound[ipart]; */
#endif /* CLUSTER_INITIAL_BOUND */
    t_tid = tidaltime_norm_code * pow(M_bound/critical_mass_code, cd.mass_slope) * p_tid;
    t_iso = isotime_norm_code * M_bound / critical_mass_code;

/* debug
#ifdef COSMOLOGY
	  star_age = tphys_from_tcode(particle_t[ipart]) - tphys_from_tcode(star_tbirth[ipart]);
#else
	  star_age = particle_t[ipart] - star_tbirth[ipart];
#endif
    cart_debug("Before: x=%f,y=%f,z=%f, rho=%e, t_dyn=%e, t_tid=%e, M=%e, M_bound=%f, age=%e",
            particle_x[ipart][0],particle_x[ipart][1],particle_x[ipart][2],
            cell_gas_density(icell)*units->density/constants->gpercc,
            sqrt(3./phi_ii)*units->time/constants->Myr,
            t_tid*units->time/constants->Myr,
            star_initial_mass[ipart]*units->mass/constants->Msun,
            star_fbound[ipart],
            star_age*1e-6);
*/
    /* star_fbound[ipart] *= MAX( (1-dt/MIN(t_tid, t_iso)), 0.0); */
    cart_assert( t_tid>0.0 && t_iso>0.0 );
    star_fbound[ipart] *= exp(-1*dt/MIN(t_tid, t_iso));
    
    return;
}



/* calculate eigenvalue of symmetric matrix using Cardono-Lagrange Method.
 * The peudo-code is described in wiki "Eigenvalue algorithm" */
void eigenvalue_wiki( double A[6], double *l )
{
    double a11, a12, a13, a22, a23, a33;
    double b11, b12, b13, b22, b23, b33;
    double p1, p2, p, q, phi, r;

    a11 = A[0];
    a12 = A[1];
    a13 = A[2];
    a22 = A[3];
    a23 = A[4];
    a33 = A[5];

    p1 = a12*a12 + a13*a13 + a23*a23;
    if (p1==0)
    {
    // if matrix is diagonal
        l[0] = a11;
        l[1] = a22;
        l[2] = a33;
        return;
    }
    else
    {
    // if matrix is not diagonal, transform A to B, so that A = qI+pB
        q = (a11+a22+a33)/3.;
        p2 = (a11-q)*(a11-q) + (a22-q)*(a22-q) + (a33-q)*(a33-q) + 2*p1;
        p = sqrt(p2/6.);
        b11 = (a11-q)/p;
        b22 = (a22-q)/p;
        b33 = (a33-q)/p;
        b12 = a12/p;
        b13 = a13/p;
        b23 = a23/p;

        // calculate the determinant of B
        r = 0.5*( b11*(b22*b33-b23*b23) + b12*(b13*b23-b12*b33) + b13*(b12*b23-b13*b22) );

        if (r<=-1) phi = M_PI/3.;
        else if (r>=1) phi = 0;
        else phi = acos(r)/3.;

        l[0] = q + 2*p*cos(phi);
        l[1] = q + 2*p*cos(phi+2*M_PI/3.);
        l[2] = 3*q - l[0] - l[1];
        return;
    }

}

double max3(double x, double y, double z)
{
    double m = x;
    (m<y) && (m=y);
    (m<z) && (m=z);
    return m;
}

#endif /* PARTICLE && STAR_FORMATION && CLUSTER_BOUND_FRACTION */
