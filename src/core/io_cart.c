#include "config.h"

#include <dirent.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "auxiliary.h"
#include "cell_buffer.h"
#include "control_parameter.h"
#include "cosmology.h"
#include "hydro_tracer.h"
#include "io.h"
#include "io_cart.h"
#include "iterators.h"
#include "lb.h"
#include "load_predictor.h"
#include "parallel.h"
#include "particle.h"
#include "refinement.h"
#include "rt_io.h"
#include "sfc.h"
#include "starformation.h"
#include "times.h"
#include "timing.h"
#include "tree.h"
#include "units.h"

DECLARE_LEVEL_ARRAY(double,tl_old);
DECLARE_LEVEL_ARRAY(double,dtl);
DECLARE_LEVEL_ARRAY(double,dtl_old);
DECLARE_LEVEL_ARRAY(int,level_sweep_dir);
#ifdef COSMOLOGY
DECLARE_LEVEL_ARRAY(double,abox_old);
#endif /* COSMOLOGY */
extern double auni_init;
extern int step;
extern int restart_frequency;
extern int current_restart_backup;
extern int num_backup_restarts;

#ifndef PARTICLE_HEADER_MAGIC
#define PARTICLE_HEADER_MAGIC           (0.1234f)
#endif

int cart_particle_num_row = num_grid;
int num_cart_input_files = 1;

int cart_grid_file_mode = 0;
int cart_particle_file_mode = 0;
int cart_ignore_file_ngrid = 0;

#ifdef HYDRO_TRACERS
int cart_tracer_file_mode = 0;
#endif /* HYDRO_TRACERS */

#ifdef RADIATIVE_TRANSFER
/*
//  This is a helper function used for backward compatibility -
//  the absolute values of radiation fields changed in 1.9 for a
//  plausible reason.
*/
void rescale_radiation_fields(float scale);
#endif /* RADIATIVE_TRANSFER */


void config_init_io_cart() {
	control_parameter_add2(control_parameter_int,&num_cart_input_files,"io:num-cart-input-files","num_cart_input_files","Number of parallel input files in the old art format.");
	control_parameter_add2(control_parameter_int,&cart_particle_num_row,"io:cart-particle-num-row","cart_particle_num_row","Page size for old style ART particle format.");

	control_parameter_add2(control_parameter_int,&cart_particle_file_mode,"io:cart-particle-file-mode","particle_file_mode","Precision for old style ART particle format. 0 = double-precision positions, double-precision times (default), 1 = double-precision positions, single-precision times, 2 = single-precision positions, single-precision times");
	control_parameter_add2(control_parameter_int,&cart_grid_file_mode,"io:cart-grid-file-mode","grid_file_mode","Grid file mode (ask Nick)");

#ifdef HYDRO_TRACERS
	control_parameter_add2(control_parameter_int, &cart_tracer_file_mode, "io:cart-tracer-file-mode", "tracer_file_mode", "Precision of hydro tracer positions to write to file. 0 = double precision, 1 = single precision.");
#endif /* HYDRO_TRACERS */
}

void config_verify_io_cart() {
	VERIFY(io:num-cart-input-files, num_cart_input_files > 0 && num_cart_input_files <= num_procs );
	VERIFY(io:cart-particle-num-row, cart_particle_num_row > 0 );
	VERIFY(io:cart-particle-file-mode, cart_particle_file_mode >= 0 && cart_particle_file_mode <= 2 );
	VERIFY(io:cart-grid-file-mode, cart_grid_file_mode >= 0 && cart_grid_file_mode <= 4 );
#ifdef HYDRO_TRACERS
	VERIFY(io:cart-tracer-file-mode, cart_tracer_file_mode >= 0 && cart_tracer_file_mode <= 1 );
#endif /* HYDRO_TRACERS */
}

#ifdef SFC_DOMAIN_DECOMPOSITION

void read_cart_restart(const char *label, int *load_balance_after_io_flag) {
	char *p;
	FILE *restart;
	char filename[256];
	char filename_gas[256];
	char filename1[256];
	char filename2[256];
	char filename3[256];
	char filename4[256];
	char filename_tracers[256];

	/* cart format is deprecated, force a proper load balance no matter what */
	*load_balance_after_io_flag = 1;

	if ( label == NULL ) {
		/* read filenames from restart file */
		sprintf( filename, "%s/restart.dat", output_directory );
		restart = fopen( filename, "r" );

		if ( restart == NULL ) {
			cart_error("Unable to locate restart.dat, please specify jobname or create restart.dat!");
		}

		fscanf( restart, "%s\n", filename_gas );
#ifdef HYDRO
#ifdef HYDRO_TRACERS
		fscanf( restart, "%s\n", filename_tracers );
#endif /* HYDRO_TRACERS */
#endif /* HYDRO */
		fscanf( restart, "%s\n", filename1 );
		fscanf( restart, "%s\n", filename2 );
		fscanf( restart, "%s\n", filename3 );
#ifdef STAR_FORMATION
		fscanf( restart, "%s\n", filename4 );
#endif /* STAR_FORMATION */
		fclose(restart);
	} else {
		sprintf( filename_gas, "%s/%s_%s.d", output_directory, jobname, label );
		sprintf( filename1,  "%s/%s_%s.dph", output_directory, jobname, label );
		sprintf( filename2, "%s/%s_%s.dxv", output_directory, jobname, label );
		sprintf( filename3, "%s/%s_%s.dpt", output_directory, jobname, label );
		sprintf( filename4, "%s/%s_%s.dst", output_directory, jobname, label );
		sprintf( filename_tracers, "%s/%s_%s.dtr", output_directory, jobname, label );
	}

	/* do load balancing */
	restart_load_balance_cart( filename_gas, filename1, filename2 );

	cart_debug("Reading grid restart...");
	start_time( GAS_READ_IO_TIMER );

	read_cart_grid_binary( filename_gas );

#ifdef RADIATIVE_TRANSFER
	/*
	//  In version 1.9 the absolute values of radiation fields changed -
	//  rescale stored values to maintain backward compatibility.
	*/
	rescale_radiation_fields(1.4e-4);
#endif /* RADIATIVE_TRANSFER */

	end_time( GAS_READ_IO_TIMER );

#if defined(HYDRO) && defined(HYDRO_TRACERS)
	start_time( PARTICLE_READ_IO_TIMER );
	read_cart_hydro_tracers( filename_tracers );
	end_time( PARTICLE_READ_IO_TIMER );
#endif /* HYDRO && HYDRO_TRACERS */

#ifdef PARTICLES
	cart_debug("Reading particle restart...");
	start_time( PARTICLE_READ_IO_TIMER );
#ifdef STAR_FORMATION
	read_cart_particles(filename1, filename2, filename3, filename4);
#else
	read_cart_particles(filename1, filename2, filename3, NULL);
#endif /* STAR_FORMATION */
	end_time( PARTICLE_READ_IO_TIMER );
#endif /* PARTICLES */

	/* cart files don't save current_restart_backup, try to detect from filename */
	p = strrchr( filename_gas, '_' );
	if ( p != NULL && sscanf( p, "_%d.d", &current_restart_backup ) == 1 ) {
		current_restart_backup = (current_restart_backup+1) % num_restart_backups;
	} else {
		current_restart_backup = (step%((restart_frequency>0) ? restart_frequency : 1) + 1) % num_restart_backups;
	}
}

#endif /* SFC_DOMAIN_DECOMPOSITION */

void set_cart_grid_file_mode(int mode)
{
  if(mode>=0 && mode<=4) cart_grid_file_mode = mode;
  else cart_error("Invalid grid file mode (%d)", mode );
}

#ifdef PARTICLES
void set_cart_particle_file_mode(int mode)
{
  if(mode>=0 && mode<=2) cart_particle_file_mode = mode;
  else cart_error("Invalide particle file mode (%d)", mode );
}


/*
//  Create multiple versions of restart_load_balance using
//  C-style templates
*/
#define FUNCTION                      restart_load_balance_cart_particle_float
#define PARTICLE_FLOAT                float
#include "io_cart1.def"

#define FUNCTION                      restart_load_balance_cart_particle_double
#define PARTICLE_FLOAT                double
#include "io_cart1.def"
#endif /* PARTICLES */

void restart_load_balance_cart( char *grid_filename,
		char *particle_header_filename, char *particle_data ) {
	int i, j;
	sfc_t index;
	sfc_t *sfc_list;
	float *cell_work;
	int *constrained_quantities;

	FILE *input;
	int endian;
	int size, value;
	int *cellrefined;
	char filename[256];

	lb_decomposition *decomposition;

	int per_proc_constraints[num_constraints] = {
		num_cells,
#ifdef PARTICLES
		num_particles
#endif /* PARTICLES */
	};

	if ( num_procs == 1 ) {
		lb_uniform_decomposition();
		return;
	}

	if ( local_proc_id == MASTER_NODE ) {
		/* do load balancing */
		constrained_quantities = cart_alloc(int, num_constraints*num_root_cells );
		cell_work = cart_alloc(float, num_root_cells );

		for ( index = 0; index < num_root_cells; index++ ) {
			cell_work[index] = 0.0;
		}

		for ( index = 0; index < num_constraints*num_root_cells; index++ ) {
			constrained_quantities[index] = 0;
		}

		/* load particle work information */
#ifdef PARTICLES
		cart_debug("before load balancing particles");
		if ( particle_header_filename != NULL ) {
			switch(cart_particle_file_mode) {
				case 0:
				case 1:
					restart_load_balance_cart_particle_double(particle_header_filename,particle_data,1.0,cell_work,constrained_quantities);
					break;
				case 2:
					restart_load_balance_cart_particle_float(particle_header_filename,particle_data,1.0,cell_work,constrained_quantities);
					break;
				default:
					cart_error("Invalid cart_particle_file_mode=%d",cart_particle_file_mode);
			}
		}
		cart_debug("load_balanced particles");
#endif /* PARTICLES */

		if ( grid_filename != NULL ) {
			index = 0;
			for ( i = 0; i < num_cart_input_files; i++ ) {
				if ( num_cart_input_files == 1 ) {
					sprintf( filename, "%s", grid_filename );
				} else {
					sprintf( filename, "%s."ART_PROC_FORMAT, grid_filename, i );
				}

				input = fopen( filename, "r" );
				if ( input == NULL ) {
					cart_error( "Unable to open file %s for reading!", filename );
				}

				if ( i == 0 ) {
					/* skip over header information */
					fread( &size, sizeof(int), 1, input );
					endian = 0;
					if ( size != 256 ) {
						reorder( (char *)&size, sizeof(int) );
						if ( size != 256 ) {
							cart_error("Error: file %s is corrupted", filename );
						} else {
							endian = 1;
						}
					}

					fseek( input, 256*sizeof(char)+sizeof(int), SEEK_CUR );

					value = 0;
					while ( value != num_root_cells ) {
						fread( &size, sizeof(int), 1, input );

						if ( endian ) {
							reorder( (char *)&size, sizeof(int) );
						}

						if ( size == sizeof(int) ) {
							fread( &value, sizeof(int), 1, input );

							if ( endian ) {
								reorder( (char *)&value, sizeof(int) );
							}
						} else {
							fseek( input, size, SEEK_CUR );
						}

						fread( &size, sizeof(int), 1, input );
					}
				}

				/* read cellrefined */
				fread( &size, sizeof(int), 1, input );

				if ( endian ) {
					reorder( (char *)&size, sizeof(int) );
				}

				size /= sizeof(int);
				cellrefined = cart_alloc(int, size);
				fread( cellrefined, sizeof(int), size, input );

				fclose(input);

				if ( endian ) {
					for ( j = 0; j < size; j++ ) {
						reorder( (char *)&cellrefined[j], sizeof(int) );
					}
				}

				for ( j = 0; j < size; j++ ) {
					constrained_quantities[num_constraints*index] += cellrefined[j];
					/* we don't know what level each cell is on or whether they are refined */
					cell_work[index] += work_per_cell(min_level,0)*cellrefined[j];
					index++;
				}

				cart_free( cellrefined );
			}
		} else {
			for ( index = 0; index < num_root_cells; index++ ) {
				constrained_quantities[num_constraints*index] = 1;
				cell_work[index] += work_per_cell(min_level,0);
			}
		}

		sfc_list = cart_alloc(sfc_t, num_root_cells);
		for ( index = 0; index < num_root_cells; index++ ) {
			sfc_list[index] = index;
		}

		decomposition = lb_domain_decomposition(num_root_cells, sfc_list, cell_work,
				num_constraints, constrained_quantities, per_proc_constraints,
				NULL);

		cart_free( sfc_list );
		cart_free( cell_work );
		cart_free( constrained_quantities );
	} else {
		decomposition = lb_domain_decomposition(0, NULL, NULL, num_constraints,
				NULL, NULL, NULL );
	}

	lb_apply_decomposition( decomposition );
	lb_destroy_decomposition( decomposition );
}

#ifdef PARTICLES

void read_cart_particle_header( char *header_filename, particle_header *header, int *endian, int *nbody_flag ) {
	int i;
	FILE *input;
	nbody_particle_header nbody_header;
	char desc[46];
	int size;
	char *p;

	*nbody_flag = 0;
	*endian = 0;

	/* read file header */
	input = fopen( header_filename, "r");
	if ( input == NULL ) {
		cart_error("Unable to open particle file %s", header_filename );
	}

	fread( &size, sizeof(int), 1, input );
	fread( desc, sizeof(char), 45, input );
	desc[45] = '\0';

	cart_debug( "Particle header file: %s", desc );

	if ( !control_parameter_is_set("jobname") ) {
		/* trim spaces from jobname */
	  p = desc + strlen(desc);
	  while (*--p == ' ') *p = '\0';
	  cart_debug("setting jobname to header value: %s",desc);
	  set_jobname( desc );
	}

	if ( size != sizeof(particle_header)+45 ) {
		if ( size == sizeof(nbody_particle_header)+45 ) {
			*nbody_flag = 1;
		} else {
			reorder( (char *)&size, sizeof(int) );

			if ( size != sizeof(particle_header)+45 ) {
				if ( size == sizeof(nbody_particle_header)+45 ) {
					*endian = 1;
					*nbody_flag = 1;
				} else {
					cart_error("Size mismatch in reading particle file header %s (%u vs %u)",
							header_filename, size, sizeof(particle_header)+45 );
				}
			} else {
				*endian = 1;
			}
		}
	}

	if ( *nbody_flag ) {
		cart_debug("USING OLD NBODY FILE FORMAT (hope that's what you wanted...)");

		fread( &nbody_header, sizeof(nbody_particle_header), 1, input );

		header->aunin = nbody_header.aunin;
		header->auni0 = nbody_header.auni0;
		header->amplt = nbody_header.amplt;
		header->astep = nbody_header.astep;
		header->istep = nbody_header.istep;
		header->partw = nbody_header.partw;
		header->tintg = nbody_header.tintg;
		header->ekin = nbody_header.ekin;
		header->ekin1 = nbody_header.ekin1;
		header->ekin2 = nbody_header.ekin2;
		header->au0 = nbody_header.au0;
		header->aeu0 = nbody_header.aeu0;
		header->Nrow = nbody_header.Nrow;
		header->Ngrid = nbody_header.Ngrid;
		header->Nspecies = nbody_header.Nspecies;
		header->Nseed = nbody_header.Nseed;
		header->OmM0 = nbody_header.OmM0;
		header->OmL0 = nbody_header.OmL0;
		header->h100 = nbody_header.h100;
		header->Wp5  = nbody_header.Wp5;
		header->OmK0 = nbody_header.OmK0;

		header->OmB0 = 1.0e-4;

		header->magic1    = nbody_header.magic1;
		header->magic2    = nbody_header.magic2;
		header->DelDC    = nbody_header.DelDC;
		header->abox     = nbody_header.abox;
		header->Hbox     = nbody_header.Hbox;

		for ( i = 0; i < 10; i++ ) {
			header->mass[i] = nbody_header.mass[i];
			header->num[i] = nbody_header.num[i];
		}

	} else {
		fread( header, sizeof(particle_header), 1, input );
	}

	fread( &size, sizeof(int), 1, input );
	fclose(input);

	if ( *endian ) {
		reorder( (char *)&header->aunin, sizeof(float) );
		reorder( (char *)&header->auni0, sizeof(float) );
		reorder( (char *)&header->amplt, sizeof(float) );
		reorder( (char *)&header->astep, sizeof(float) );
		reorder( (char *)&header->istep, sizeof(int) );
		reorder( (char *)&header->partw, sizeof(float) );
		reorder( (char *)&header->tintg, sizeof(float) );
		reorder( (char *)&header->ekin, sizeof(float) );
		reorder( (char *)&header->ekin1, sizeof(float) );
		reorder( (char *)&header->ekin2, sizeof(float) );
		reorder( (char *)&header->au0, sizeof(float) );
		reorder( (char *)&header->aeu0, sizeof(float) );
		reorder( (char *)&header->Nrow, sizeof(int) );
		reorder( (char *)&header->Ngrid, sizeof(int) );
		reorder( (char *)&header->Nspecies, sizeof(int) );
		reorder( (char *)&header->Nseed, sizeof(int) );
		reorder( (char *)&header->OmM0, sizeof(float) );
		reorder( (char *)&header->OmL0, sizeof(float) );
		reorder( (char *)&header->h100, sizeof(float) );
		reorder( (char *)&header->Wp5, sizeof(float) );
		reorder( (char *)&header->OmK0, sizeof(float) );

		if ( !(*nbody_flag) ) {
			reorder( (char *)&header->OmB0, sizeof(float) );
		}

		reorder( (char *)&header->magic1, sizeof(float) );
		reorder( (char *)&header->DelDC, sizeof(float) );
		reorder( (char *)&header->abox, sizeof(float) );
		reorder( (char *)&header->Hbox, sizeof(float) );
		reorder( (char *)&header->magic2, sizeof(float) );

		for ( i = 0; i < header->Nspecies; i++ ) {
			reorder( (char *)&header->mass[i], sizeof(float) );
			reorder( (char *)&header->num[i], sizeof(int) );
		}
	}

	/*
	//  NG: Check for legacy files
	*/
	if(header->magic1!= PARTICLE_HEADER_MAGIC || header->magic2 != PARTICLE_HEADER_MAGIC )
	  {
	    /*
	    //  A legacy file with the garbage in it. Set the DC mode to 0
	    */
	    header->DelDC = 0.0;
	    header->abox = header->aunin;

		cart_debug("Detected legacy format particle file (no DC mode set).");
	  }
}

void read_cart_header_to_units( char *header_filename) {
	double dt;
	int endian;
	particle_header header;
	int nbody_flag=0;

	if ( local_proc_id == MASTER_NODE ) {
	    read_cart_particle_header( header_filename, &header, &endian, &nbody_flag );

	    cart_debug("auni  = %e", header.aunin );
	    cart_debug("auni0 = %e", header.auni0 );
	    cart_debug("amplt = %e", header.amplt );
	    cart_debug("astep = %e", header.astep );
	    cart_debug("istep = %u", header.istep );
	    cart_debug("Ngrid = %u", header.Ngrid );
	    cart_debug("Nrow = %u", header.Nrow );
	    cart_debug("Nspecies = %u", header.Nspecies );
	    cart_debug("OmegaM = %e", header.OmM0 );
	    cart_debug("OmegaL = %e", header.OmL0 );
	    cart_debug("hubble = %e", header.h100 );
	    cart_debug("OmegaB = %e", header.OmB0 );

	    cart_debug("DelDC = %e", header.DelDC );
	    cart_debug("Lbox/h = %e", header.fill[NFILL_HEADER-1] );

#ifdef COSMOLOGY
	    cart_debug("abox  = %e", header.abox );

	    /* set cosmology & units */
	    cosmology_set(OmegaM,header.OmM0);
	    cosmology_set(OmegaL,header.OmL0);
	    cosmology_set(OmegaB,header.OmB0);
	    cosmology_set(h,header.h100);
	    cosmology_set(DeltaDC,header.DelDC);
	    box_size = header.fill[NFILL_HEADER-1];
	    cart_debug("box_size[chimps]=%f ",box_size);

	    /* NG: trust only the global scale factor */
	    auni[min_level]	= header.aunin;
	    auni_init	= header.auni0;

	    tl[min_level]  = tcode_from_auni(auni[min_level]);
	    abox[min_level] = abox_from_auni(auni[min_level]);
	    abox_old[min_level] = abox[min_level] - header.astep;

	    if ( header.astep > 0.0 ) {
			dt = tl[min_level] - tcode_from_abox(abox[min_level]-header.astep);
		} else {
			dt = 0.0;
	    }

#else
	    cart_debug("tl    = %e", header.abox );
	    tl[min_level]  = header.abox;
	    dt = 0.0;
	    units_set(header.OmM0,header.OmB0,header.OmL0);

#endif /* COSMOLOGY */

	    step                  = header.istep;
	    cart_particle_num_row = header.Nrow;

	    /* only root node needs to keep energy conservation variables */
	    tintg		= header.tintg;
	    ekin		= header.ekin;
	    ekin1		= header.ekin1;
	    au0         = header.au0;
	    aeu0		= header.aeu0;

#ifdef COSMOLOGY
	    ap0         = abox_from_tcode( tl[min_level] - 0.5*dt );
#else
	    ap0         = 1.0;
#endif
	}

#ifdef COSMOLOGY
//	    MPI_Bcast( (char *)header, sizeof(particle_header), MPI_BYTE, MASTER_NODE, mpi.comm.run );
	MPI_Bcast( (char *)cosmology, sizeof(struct CosmologyParameters), MPI_BYTE, MASTER_NODE, mpi.comm.run );
	MPI_Bcast( &auni[min_level], 1, MPI_DOUBLE, MASTER_NODE, mpi.comm.run );
	MPI_Bcast( &abox[min_level], 1, MPI_DOUBLE, MASTER_NODE, mpi.comm.run );
	MPI_Bcast( &auni_init, 1, MPI_DOUBLE, MASTER_NODE, mpi.comm.run );
	MPI_Bcast( &box_size, 1, MPI_DOUBLE, MASTER_NODE, mpi.comm.run );
#endif /* COSMOLOGY */

	MPI_Bcast( &tl[min_level], 1, MPI_DOUBLE, MASTER_NODE, mpi.comm.run );
	MPI_Bcast( &dt, 1, MPI_DOUBLE, MASTER_NODE, mpi.comm.run );

	time_enabled = 1;
	units_init();
}

/*
//  Create multiple versions of read_particles using
//  C-style templates
*/
#define FUNCTION                      read_cart_particles_float_time_float
#define PARTICLE_FLOAT                float
#define MPI_PARTICLE_FLOAT            MPI_FLOAT
#define PARTICLE_TIMES_FLOAT          float
#define MPI_PARTICLE_TIMES_FLOAT      MPI_FLOAT
#include "io_cart2.def"

#define FUNCTION                      read_cart_particles_double_time_float
#define PARTICLE_FLOAT                double
#define MPI_PARTICLE_FLOAT            MPI_DOUBLE
#define PARTICLE_TIMES_FLOAT          float
#define MPI_PARTICLE_TIMES_FLOAT      MPI_FLOAT
#include "io_cart2.def"

#define FUNCTION                      read_cart_particles_double_time_double
#define PARTICLE_FLOAT                double
#define MPI_PARTICLE_FLOAT            MPI_DOUBLE
#define PARTICLE_TIMES_FLOAT          double
#define MPI_PARTICLE_TIMES_FLOAT      MPI_DOUBLE
#include "io_cart2.def"

void read_cart_particles( char *header_filename, char *data_filename,
			char *timestep_filename, char *stellar_filename) {
  switch(cart_particle_file_mode)
    {
    case 0:
      {
	read_cart_particles_double_time_double(header_filename,data_filename,timestep_filename,stellar_filename);
	break;
      }
    case 1:
    case -1:
      {
	read_cart_particles_double_time_float(header_filename,data_filename,timestep_filename,stellar_filename);
	break;
      }
    case 2:
    case -2:
      {
	read_cart_particles_float_time_float(header_filename,data_filename,timestep_filename,stellar_filename);
	break;
      }
    default:
      {
	cart_error("Invalid cart_particle_file_mode=%d",cart_particle_file_mode);
      }
    }
}
#endif /* PARTICLES */


#ifdef HYDRO
#ifdef HYDRO_TRACERS

/*
//  Create multiple versions of read_cart_hydro_tracers using C-style templates
*/
#define FUNCTION_READ                   read_cart_hydro_tracers_double
#define HYDRO_TRACER_POSITION_FLOAT     double
#include "io_cart3_hydro_tracer.def"

#define FUNCTION_READ                   read_cart_hydro_tracers_float
#define HYDRO_TRACER_POSITION_FLOAT     float
#include "io_cart3_hydro_tracer.def"

void read_cart_hydro_tracers( char *filename ) {
    switch(cart_tracer_file_mode) {
        case 0: {
            read_cart_hydro_tracers_double(filename);
            break;
        }
        case 1: {
            read_cart_hydro_tracers_float(filename);
            break;
        }
        default: {
            cart_error("Invalid cart_tracer_file_mode = %d", cart_tracer_file_mode);
        }
    }
}

#endif /* HYDRO_TRACERS */
#endif /* HYDRO */

#ifdef SFC_DOMAIN_DECOMPOSITION

/* two helpers */
void read_cart_grid_binary_top_level_vars(int num_in_vars, int jskip, int num_out_vars, int *out_var, FILE *input, int endian, int file_parent, int file_index, int local_file_root_cells, int page_size, int *proc_num_cells, long *proc_cell_index, int *file_sfc_index);
void read_cart_grid_binary_lower_level_vars(int num_in_vars, int jskip, int num_out_vars, int *out_var, FILE *input, int endian, int file_parent, int file_index, long *total_cells, int page_size, int *proc_num_cells, int level, long *first_page_count, long *proc_first_index, long *proc_cell_index, int *current_level);


void read_cart_grid_binary( char *filename ) {
	int i, j;
	int size;
	int num_read, flag;
	FILE *input;
	char job[256];
	int minlevel, maxlevel;
	double t, dt;
	float adum, ainit;
	float boxh, OmM0, OmL0, OmB0, h100;
	int nextras;
	float extra[10];
	char lextra[10][256];
	int *cellrefined[MAX_PROCS], *cellrefinedbuffer;
	int *order;
	int *current_level;
	int endian;
	int proc;
	int ret;
	int level;
	int icell, ioct;
	int cell_counts;
	int page_size;
	int ncell0, sfc_order;
	long current_level_count, current_read_count;
	long next_level_count;
	int file_index, file_parent;
	int local_file_root_cells;
	int page_count;
	long count, start;
	long total_cells[max_level-min_level+1];
	int file_parent_proc[MAX_PROCS];
	int proc_num_cells[MAX_PROCS];
	int proc_cur_cells[MAX_PROCS];
	int proc_page_count[MAX_PROCS];
	long first_page_count[MAX_PROCS];
	long proc_next_level_octs[MAX_PROCS];
	long proc_cell_index[MAX_PROCS];
	long proc_first_index[MAX_PROCS+1];
	long next_proc_index[MAX_PROCS+1];
	int file_sfc_index[MAX_PROCS+1];
	char parallel_filename[256];
	int num_requests, num_send_requests;
	int continue_reading, ready_to_read;
	MPI_Request requests[2*MAX_PROCS];
	MPI_Request send_requests[MAX_PROCS];
	MPI_Status status;

	float file_volume_min[nDim];
	float file_volume_max[nDim];

#ifdef COSMOLOGY
	struct CosmologyParameters temp_cosmo;
#endif /* COSMOLOGY */

	int skip_hvar = -1;
	int skip_ovar = -1;
	int hydro_vars[num_hydro_vars+1];
	int num_other_vars = 0;
	int *other_vars = 0;
	int num_in_hydro_vars, num_in_other_vars;
#ifdef RADIATIVE_TRANSFER
	int rt_var_offset = 0;
#endif

#ifdef HYDRO
	/*
	// Maintain the same order as in a previous version
	 */
	hydro_vars[0] = HVAR_GAS_DENSITY;
	hydro_vars[1] = HVAR_GAS_ENERGY;
	hydro_vars[2] = HVAR_MOMENTUM + 0;
	hydro_vars[3] = HVAR_MOMENTUM + 1;
	hydro_vars[4] = HVAR_MOMENTUM + 2;
	hydro_vars[5] = HVAR_PRESSURE;
	hydro_vars[6] = HVAR_GAMMA;
	hydro_vars[7] = HVAR_INTERNAL_ENERGY;
#ifdef ELECTRON_ION_NONEQUILIBRIUM
	hydro_vars[8] = HVAR_ELECTRON_INTERNAL_ENERGY;
#endif
	for(j=0; j<num_chem_species; j++) {
		hydro_vars[num_hydro_vars-num_chem_species+j] = HVAR_ADVECTED_VARIABLES+j;
	}
#endif /* HYDRO */

#if defined(GRAVITY) || defined(RADIATIVE_TRANSFER)
#ifdef GRAVITY
	num_other_vars++;
#ifdef HYDRO
	num_other_vars++;
#endif /* HYDRO */
#endif
#ifdef RADIATIVE_TRANSFER
	rt_var_offset = num_other_vars;
	num_other_vars += rt_num_disk_vars;
#endif

	other_vars = cart_alloc(int, num_other_vars );

#ifdef GRAVITY
	other_vars[0] = VAR_POTENTIAL;
#ifdef HYDRO
	other_vars[1] = VAR_POTENTIAL_HYDRO;
#endif /* HYDRO */
#endif
#ifdef RADIATIVE_TRANSFER
	for(j=0; j<rt_num_disk_vars; j++) other_vars[rt_var_offset+j] = rt_disk_offset + j;
#endif
#endif /* GRAVITY || RADIATIVE_TRANSFER */

	switch(cart_grid_file_mode)
	  {
	  case  0:
	    {
	      num_in_hydro_vars = num_hydro_vars;
	      num_in_other_vars = num_other_vars;
	      break;
	    }
	  case  1:
	    {
	      num_in_hydro_vars = num_hydro_vars + 6;
	      num_in_other_vars = num_other_vars + 6;
	      break;
	    }
	  case  2:
	    {
	      num_in_hydro_vars = num_hydro_vars + 6;
	      num_in_other_vars = num_other_vars + 8;
	      break;
	    }
#ifdef BLASTWAVE_FEEDBACK
	  case  3:
	    {
	      num_in_hydro_vars = num_hydro_vars - 1;
	      num_in_other_vars = num_other_vars ;
	      skip_hvar = HVAR_BLASTWAVE_TIME - HVAR_GAS_DENSITY; //fundhydrovars
	      break;
	    }
#endif /* BLASTWAVE_FEEDBACK */
	  case  4:
	    {
	      num_in_hydro_vars = num_hydro_vars;
	      num_in_other_vars = num_other_vars + 2;
	      break;
	    }
	  default:
	    {
	      cart_error("Invalid cart_grid_file_mode=%d in a call to  read_grid_binary",cart_grid_file_mode);
	    }
	  }

	cart_assert( num_cart_input_files >= 1 && num_cart_input_files <= num_procs );

	page_size = num_grid*num_grid;

	/* set up global file information */
	proc = 0;
	for ( i = 0; i < num_cart_input_files; i++ ) {
		while ( proc*num_cart_input_files / num_procs != i ) {
			proc++;
		}
		file_parent_proc[i] = proc;
	}

	/* determine parallel output options */
	file_index = local_proc_id * num_cart_input_files / num_procs;
	file_parent = file_parent_proc[file_index];

	/* open file handle if parent of parallel file */
	if ( local_proc_id == file_parent ) {
		if ( num_cart_input_files == 1 ) {
			input = fopen(filename,"r");
		} else {
			sprintf( parallel_filename, "%s."ART_PROC_FORMAT, filename, file_index );
			input = fopen(parallel_filename, "r");
		}

		if ( input == NULL ) {
			cart_error( "read_grid_binary: unable to open file %s for reading!", filename );
		}
	}

	/* the header exists only in the first file */
	if ( local_proc_id == MASTER_NODE ) {
		cart_assert( local_proc_id == file_parent );

		fread(&size, sizeof(int), 1, input );
		endian = 0;
		if ( size != 256 ) {
			reorder( (char *)&size, sizeof(int) );
			if ( size != 256 ) {
				cart_error("Error: file %s is corrupted", filename );
			} else {
				endian = 1;
				cart_debug("Reordering bytes (file endianness is opposite program)");
			}
		}

		fread(job, sizeof(char), 256, input );
		if ( !control_parameter_is_set("jobname") ) {
		  cart_debug("setting jobname to header value: %s",job);
		  set_jobname( job );
		}
		fread(&size, sizeof(int), 1, input );

		/* istep, t, dt, adum, ainit */
		fread( &size, sizeof(int), 1, input );
		fread( &step, sizeof(int), 1, input );
		fread( &t, sizeof(double), 1, input );
		fread( &dt, sizeof(double), 1, input );
		fread( &adum, sizeof(float), 1, input );
		fread( &ainit, sizeof(float), 1, input );
		fread( &size, sizeof(int), 1, input );

		if ( endian ) {
			reorder( (char *)&step, sizeof(int) );
			reorder( (char *)&ainit, sizeof(float) );
		}

		/* boxh, Om0, Oml0, Omb0, hubble */
		fread( &size, sizeof(int), 1, input );
		fread( &boxh, sizeof(float), 1, input );
		fread( &OmM0, sizeof(float), 1, input );
		fread( &OmL0, sizeof(float), 1, input );
		fread( &OmB0, sizeof(float), 1, input );
		fread( &h100, sizeof(float), 1, input );
		fread( &size, sizeof(int), 1, input );

		if ( endian ) {
			reorder( (char *)&boxh, sizeof(float) );
			reorder( (char *)&OmM0, sizeof(float) );
			reorder( (char *)&OmL0, sizeof(float) );
			reorder( (char *)&OmB0, sizeof(float) );
			reorder( (char *)&h100, sizeof(float) );
		}

		box_size = boxh;

#ifdef COSMOLOGY
		auni_init = ainit;

		cosmology_set(OmegaM,OmM0);
		cosmology_set(OmegaL,OmL0);
		cosmology_set(OmegaB,OmB0);
		cosmology_set(h,h100);

		/*
		//  NG: we do not set the DC mode here since we
		//  assume it will be set by the particle reader.
		//  The DC mode is only relevant for cosmology, and
		//  it is unlikely that a cosmology run will not
		//  include particles. And if it even does, then
		//  there is no sense whatsoever to set
		//  a non-trivial DC mode.
		//
		//  cosmology_set(DeltaDC,0.0);
		 */

		temp_cosmo = *cosmology;

#else
		/* do nothing, units now set for all procs together */
#endif /* COSMOLOGY */

		/* nextra (no evidence extras are used...) extra lextra */
		fread( &size, sizeof(int), 1, input );
		fread( &nextras, sizeof(int), 1, input );
		fread( &size, sizeof(int), 1, input );

		if ( endian ) {
			reorder( (char *)&nextras, sizeof(int) );
		}

		/* extra */
		fread( &size, sizeof(int), 1, input );
		fread( extra, sizeof(float), nextras, input );
		fread( &size, sizeof(int), 1, input );

		/* lextra */
		fread( &size, sizeof(int), 1, input );
		fread( lextra, 256*sizeof(char), nextras, input );
		fread( &size, sizeof(int), 1, input );

		/* Minlevel, MaxLevelNow */
		fread( &size, sizeof(int), 1, input );
		fread( &minlevel, sizeof(int), 1, input );
		fread( &maxlevel, sizeof(int), 1, input);
		fread( &size, sizeof(int), 1, input );

		if ( endian ) {
			reorder( (char *)&minlevel, sizeof(int) );
			reorder( (char *)&maxlevel, sizeof(int) );
		}

		if ( maxlevel > max_level ) {
			cart_error("File %s has more levels than compiled program (%u)", filename, maxlevel );
		}

		cart_assert( minlevel == min_level );

		/* tl */
		fread( &size, sizeof(int), 1, input );
		fread( tl, sizeof(double), maxlevel-minlevel+1, input );
		fread( &size, sizeof(int), 1, input);

		if ( endian ) {
			for ( i = minlevel; i <= maxlevel; i++ ) {
				reorder( (char *)&tl[i], sizeof(double) );
			}
		}

		/* dtl */
		fread( &size, sizeof(int), 1, input );
		fread( dtl, sizeof(double), maxlevel-minlevel+1, input);
		fread( &size, sizeof(int), 1, input );

		if ( endian ) {
			for ( i = minlevel; i <= maxlevel; i++ ) {
				reorder( (char *)&dtl[i], sizeof(double) );
			}
		}

		/* tl_old */
		fread( &size, sizeof(int), 1, input );
		fread( tl_old, sizeof(double), maxlevel-minlevel+1, input);
		fread( &size, sizeof(int), 1, input );

		if ( endian ) {
			for ( i = minlevel; i <= maxlevel; i++ ) {
				reorder( (char *)&tl_old[i], sizeof(double) );
			}
		}

		/* dtl_old */
		fread( &size, sizeof(int), 1, input );
		fread( dtl_old, sizeof(double), maxlevel-minlevel+1, input);
		fread( &size, sizeof(int), 1, input );

		if ( endian ) {
			for ( i = minlevel; i <= maxlevel; i++ ) {
				reorder( (char *)&dtl_old[i], sizeof(double) );
			}
		}

		/* iSO (sweep direction for flux solver) */
#ifdef HYDRO
		fread( &size, sizeof(int), 1, input );
		fread( level_sweep_dir, sizeof(int), maxlevel-minlevel+1, input);
		fread( &size, sizeof(int), 1, input );

		if ( endian ) {
			for ( i = minlevel; i <= maxlevel; i++ ) {
				reorder( (char *)&level_sweep_dir[i], sizeof(int) );
			}
		}

		for ( i = maxlevel+1; i <= max_level; i++ ) {
			level_sweep_dir[i] = 0;
		}
#endif /* HYDRO */

		/* sfc ordering used */
		fread( &size, sizeof(int), 1, input );
		fread( &sfc_order, sizeof(int), 1, input);
		fread( &size, sizeof(int), 1, input );

		if ( endian ) {
			reorder( (char *)&sfc_order, sizeof(int) );
		}

		if ( sfc_order != SFC ) {
			cart_error("File has different sfc indexing (%d) than program (%d)",sfc_order,SFC);
		}

		/* refinement volume */
		fread( &size, sizeof(int), 1, input );
		fread( file_volume_min, sizeof(float), nDim, input );
		fread( file_volume_max, sizeof(float), nDim, input );
		fread( &size, sizeof(int), 1, input );

		if ( endian ) {
			for ( i = 0; i < nDim; i++ ) {
				reorder( (char *)&file_volume_min[i], sizeof(float) );
				reorder( (char *)&file_volume_max[i], sizeof(float) );
			}
		}

#ifdef REFINEMENT
		for ( i = 0; i < nDim; i++ ) {
			refinement_volume_min[i] = file_volume_min[i];
			refinement_volume_max[i] = file_volume_max[i];
		}
#endif

#ifdef STAR_FORMATION
		/* star formation volume */
		fread( &size, sizeof(int), 1, input );
		fread( file_volume_min, sizeof(float), nDim, input );
		fread( file_volume_max, sizeof(float), nDim, input );
		fread( &size, sizeof(int), 1, input );

		if ( endian ) {
			for ( i = 0; i < nDim; i++ ) {
				reorder( (char *)&file_volume_min[i], sizeof(float) );
				reorder( (char *)&file_volume_max[i], sizeof(float) );
			}
		}

		for ( i = 0; i < nDim; i++ ) {
			star_formation_volume_min[i] = file_volume_min[i];
			star_formation_volume_max[i] = file_volume_max[i];
		}
#endif /* STAR_FORMATION */

		/* ncell0 */
		fread( &size, sizeof(int), 1, input );
		fread( &ncell0, sizeof(int), 1, input);
		fread( &size, sizeof(int), 1, input );

		if ( endian ) {
			reorder( (char *)&ncell0, sizeof(int) );
		}

		if ( ncell0 != (num_grid*num_grid*num_grid) ) {
			cart_error("File has different num_grid than compiled program");
		}
	}

	/* send header information to all other processors */
	MPI_Bcast( &endian, 1, MPI_INT, MASTER_NODE, mpi.comm.run );
	MPI_Bcast( &minlevel, 1, MPI_INT, MASTER_NODE, mpi.comm.run );
	MPI_Bcast( &maxlevel, 1, MPI_INT, MASTER_NODE, mpi.comm.run );
	MPI_Bcast( &step, 1, MPI_INT, MASTER_NODE, mpi.comm.run );
	MPI_Bcast( &box_size, 1, MPI_DOUBLE, MASTER_NODE, mpi.comm.run );

	MPI_Bcast( tl, maxlevel-minlevel+1, MPI_DOUBLE, MASTER_NODE, mpi.comm.run );
	MPI_Bcast( dtl, maxlevel-minlevel+1, MPI_DOUBLE, MASTER_NODE, mpi.comm.run );
	MPI_Bcast( dtl_old, maxlevel-minlevel+1, MPI_DOUBLE, MASTER_NODE, mpi.comm.run );

	time_enabled = 1;

#ifdef HYDRO
	MPI_Bcast( level_sweep_dir, max_level-min_level+1, MPI_INT, MASTER_NODE, mpi.comm.run );
#endif /* HYDRO */

#ifdef COSMOLOGY
	MPI_Bcast( &auni_init, 1, MPI_DOUBLE, MASTER_NODE, mpi.comm.run );
	MPI_Bcast( (char *)&temp_cosmo, sizeof(struct CosmologyParameters), MPI_BYTE, MASTER_NODE, mpi.comm.run );
	cosmology_copy(&temp_cosmo);
#else
	MPI_Bcast( &h100, 1, MPI_DOUBLE, MASTER_NODE, mpi.comm.run );
	MPI_Bcast( &OmM0, 1, MPI_DOUBLE, MASTER_NODE, mpi.comm.run );

	MPI_Bcast( &OmB0, 1, MPI_DOUBLE, MASTER_NODE, mpi.comm.run );
	MPI_Bcast( &OmL0, 1, MPI_DOUBLE, MASTER_NODE, mpi.comm.run );

	units_set(OmM0,OmB0,OmL0);
#endif /* COSMOLOGY */

	units_init();

#ifdef REFINEMENT
	MPI_Bcast( refinement_volume_min, nDim, MPI_DOUBLE, MASTER_NODE, mpi.comm.run );
	MPI_Bcast( refinement_volume_max, nDim, MPI_DOUBLE, MASTER_NODE, mpi.comm.run );
#endif

#ifdef STAR_FORMATION
	MPI_Bcast( star_formation_volume_min, nDim, MPI_DOUBLE, MASTER_NODE, mpi.comm.run );
	MPI_Bcast( star_formation_volume_max, nDim, MPI_DOUBLE, MASTER_NODE, mpi.comm.run );
#endif /* STAR_FORMATION */

	if ( local_proc_id == file_parent ) {
		fread( &size, sizeof(int), 1, input );

		if ( endian ) {
			reorder( (char *)&size, sizeof(int) );
		}

		local_file_root_cells = size / sizeof(int);
		cellrefinedbuffer = cart_alloc(int, local_file_root_cells );

		num_read = fread( cellrefinedbuffer, sizeof(int), local_file_root_cells, input );

		if ( num_read != local_file_root_cells ) {
			cart_error("I/O error in read_grid_binary: num_read = %d, local_file_root_cells = %d",
				num_read, local_file_root_cells );
		}

		fread( &size, sizeof(int), 1, input );

		if ( endian ) {
			for ( i = 0; i < local_file_root_cells; i++ ) {
				reorder( (char *)&cellrefinedbuffer[i], sizeof(int) );
			}
		}

		if ( local_proc_id == MASTER_NODE ) {
			cell_counts = local_file_root_cells;

			file_sfc_index[0] = 0;
			for ( i = 1; i < num_cart_input_files; i++ ) {
				/* need to know how many root cells are in each file */
				/* receive cellrefined */
				file_sfc_index[i] = cell_counts;
				MPI_Recv( &file_sfc_index[i+1], 1, MPI_INT, file_parent_proc[i],
					0, mpi.comm.run, &status );
				cell_counts += file_sfc_index[i+1];
			}
			file_sfc_index[num_cart_input_files] = cell_counts;

			cart_assert( cell_counts == num_root_cells );
		} else {
			/* send cellrefined array to MASTER_NODE */
			MPI_Send( &local_file_root_cells, 1, MPI_INT, MASTER_NODE, 0, mpi.comm.run );
		}
	}

	/* send block information */
	MPI_Bcast( file_sfc_index, num_cart_input_files+1, MPI_INT, MASTER_NODE, mpi.comm.run );

	/* determine how many cells to expect from each processor */
	for ( proc = 0; proc < num_procs; proc++ ) {
		proc_num_cells[proc] = 0;
	}

	for ( i = 0; i < num_cart_input_files; i++ ) {
		if ( proc_sfc_index[local_proc_id] < file_sfc_index[i+1] &&
				proc_sfc_index[local_proc_id+1] >= file_sfc_index[i] ) {
			proc_num_cells[ file_parent_proc[i] ] =
					MIN( proc_sfc_index[local_proc_id+1], file_sfc_index[i+1] ) -
					MAX( proc_sfc_index[local_proc_id], file_sfc_index[i] );
		}
	}

	order = cart_alloc(int, num_cells_per_level[min_level] );
	cellrefined[local_proc_id] = cart_alloc(int, num_cells_per_level[min_level] );

	num_requests = 0;

	/* set up non-blocking receives */
	count = 0;
	for ( proc = 0; proc < num_procs; proc++ ) {
		if ( proc_num_cells[proc] > 0 ) {
			if ( proc == local_proc_id ) {
				/* copy data directly */
				start = MAX( 0, proc_sfc_index[local_proc_id] - file_sfc_index[file_index] );
				cart_assert( start >= 0 && start <= local_file_root_cells - proc_num_cells[local_proc_id] );
				for ( i = 0; i < proc_num_cells[local_proc_id]; i++ ) {
					cellrefined[local_proc_id][count+i] = cellrefinedbuffer[start+i];
				}
			} else {
				MPI_Irecv( &cellrefined[local_proc_id][count], proc_num_cells[proc], MPI_INT, proc,
					proc_num_cells[proc], mpi.comm.run, &requests[num_requests++] );
			}

			count += proc_num_cells[proc];
		}
	}

	cart_assert( count == num_cells_per_level[min_level] );

	/* need to avoid deadlocks! senders may also be receivers! */
	if ( local_proc_id == file_parent ) {
		start = 0;
		next_proc_index[proc] = 0;

		/* send all necessary information */
		for ( proc = 0; proc < num_procs; proc++ ) {
			next_proc_index[proc+1] = 0;

			if ( proc == local_proc_id ) {
				for ( i = 0; i < proc_num_cells[local_proc_id]; i++ ) {
					if ( cellrefinedbuffer[start+i] > 1 ) {
						next_proc_index[proc+1]++;
					}
				}
				start += proc_num_cells[local_proc_id];
			} else if ( start < local_file_root_cells && proc != local_proc_id &&
					file_sfc_index[file_index]+start < proc_sfc_index[proc+1] &&
					file_sfc_index[file_index]+start >= proc_sfc_index[proc] ) {
				count = MIN( proc_sfc_index[proc+1], file_sfc_index[file_index+1] ) -
						MAX( file_sfc_index[file_index] + start, proc_sfc_index[proc] );
				cart_assert( count > 0 && start + count <= local_file_root_cells );
				for ( i = 0; i < count; i++ ) {
					if ( cellrefinedbuffer[start+i] > 1 ) {
						next_proc_index[proc+1]++;
					}
				}
				MPI_Isend( &cellrefinedbuffer[start], count, MPI_INT, proc,
					count, mpi.comm.run, &requests[num_requests++] );
				start += count;
			}
		}

		cart_assert( start == local_file_root_cells );
	}

	/* wait for sends/receives to complete */
	MPI_Waitall( num_requests, requests, MPI_STATUSES_IGNORE );

	if ( local_proc_id == file_parent ) {
		cart_free( cellrefinedbuffer );
	}

	current_level_count = 0;
	next_level_count = 0;
	for ( proc = 0; proc < num_procs; proc++ ) {
		proc_cell_index[proc] = proc_sfc_index[local_proc_id] + current_level_count;
		proc_next_level_octs[proc] = 0;

		for ( i = 0; i < proc_num_cells[proc]; i++ ) {
			if ( cellrefined[local_proc_id][current_level_count] > 1 ) {
				ret = split_cell(current_level_count);
				if ( ret ) {
					cart_error("Unable to finish splitting root cells, ran out of octs?");
				}
				cart_assert( next_level_count < num_cells_per_level[min_level] );
				order[next_level_count++] = cell_child_oct[current_level_count];
				proc_next_level_octs[proc]++;
			}
			current_level_count++;
		}
	}

	cart_assert( next_level_count <= num_cells_per_level[min_level] );

	cart_free( cellrefined[local_proc_id] );
	read_cart_grid_binary_top_level_vars(num_in_hydro_vars,skip_hvar,num_hydro_vars,hydro_vars,input,endian,file_parent,file_index,
			local_file_root_cells,page_size,proc_num_cells,proc_cell_index,file_sfc_index);
#if defined(GRAVITY) || defined(RADIATIVE_TRANSFER)
	read_cart_grid_binary_top_level_vars(num_in_other_vars,skip_ovar,num_other_vars,other_vars,input,endian,file_parent,file_index,
			local_file_root_cells,page_size,proc_num_cells,proc_cell_index,file_sfc_index);
#endif /* GRAVITY || RADIATIVE_TRANSFER */

	/* now read levels */
	for ( level = min_level+1; level <= maxlevel; level++ ) {
		num_requests = 0;
		count = 0;
		for ( proc = 0; proc < num_procs; proc++ ) {
			proc_num_cells[proc] = proc_next_level_octs[proc]*num_children;

			if ( proc_num_cells[proc] > 0 && proc != local_proc_id ) {
				MPI_Irecv( &first_page_count[proc], 1, MPI_LONG, proc,
						0, mpi.comm.run, &requests[num_requests++] );
			} else {
				first_page_count[proc] = 0;
			}

			proc_cell_index[proc] = count;
			count += proc_next_level_octs[proc];
			proc_next_level_octs[proc] = 0;
		}

		if ( local_proc_id == file_parent ) {
			fread( &size, sizeof(int), 1, input );
			if ( endian ) {
				reorder( (char *)&size, sizeof(int) );
			}

			if ( size == sizeof(int) ) {
				fread( &size, sizeof(int), 1, input );

				if ( endian ) {
					reorder( (char *)&size, sizeof(int) );
				}

				total_cells[level] = (long)size;
			} else if ( size == sizeof(long) )  {
				fread( &total_cells[level], sizeof(long), 1, input );
				if ( endian ) {
					reorder( (char *)&total_cells[level], sizeof(long) );
				}
			} else {
				cart_error("File format error in %s, size = %d!", filename, size );
			}
			fread( &size, sizeof(int), 1, input );
			fread( &size, sizeof(int), 1, input );

			cart_debug("total_cells[%u] = %d", level, total_cells[level] );

			proc_first_index[0] = 0;
			for ( proc = 1; proc <= num_procs; proc++ ) {
				proc_first_index[proc] = (long)num_children*next_proc_index[proc] +
								proc_first_index[proc-1];
				next_proc_index[proc] = 0;
			}

			cart_assert( proc_first_index[num_procs] == total_cells[level] );

			for ( proc = 0; proc < num_procs; proc++ ) {
				if ( proc_first_index[proc+1]-proc_first_index[proc] > 0 && proc != local_proc_id ) {
					MPI_Isend( &proc_first_index[proc], 1, MPI_LONG, proc,
						0, mpi.comm.run, &requests[num_requests++] );
				}
			}

			cellrefinedbuffer = cart_alloc(int, MIN( total_cells[level], page_size ) );
		}

		MPI_Waitall( num_requests, requests, MPI_STATUSES_IGNORE );

		num_requests = 0;
		for ( proc = 0; proc < num_procs; proc++ ) {
			proc_cur_cells[proc] = 0;

			if ( proc_num_cells[proc] > 0 && proc != local_proc_id ) {
				/* set up receive */
				proc_page_count[proc] = MIN( page_size - first_page_count[proc] % page_size,
								proc_num_cells[proc] );
				cellrefined[proc] = cart_alloc(int, MIN( page_size, proc_num_cells[proc] ) );
				MPI_Irecv( cellrefined[proc], proc_page_count[proc], MPI_INT, proc,
					proc_page_count[proc], mpi.comm.run, &requests[proc] );
				num_requests++;
			} else {
				proc_page_count[proc] = 0;
				requests[proc] = MPI_REQUEST_NULL;
			}
		}

		current_level = order;
		order = cart_alloc(int, num_children*next_level_count );

		current_level_count = 0;
		current_read_count = 0;
		next_level_count = 0;
		continue_reading = 1;
		ready_to_read = 1;

		while ( continue_reading ) {
			flag = 0;

			if ( local_proc_id == file_parent && current_read_count < total_cells[level] && ready_to_read ) {
				/* read in a page */
				page_count = MIN( page_size, total_cells[level] - current_read_count );
				num_read = fread( cellrefinedbuffer, sizeof(int), page_count, input );

				if ( num_read != page_count ) {
					cart_error("I/O Error in read_grid_binary: num_read = %u, page_count = %u",
						num_read, page_count );
				}

				if ( endian ) {
					for ( i = 0; i < page_count; i++ ) {
						reorder( (char *)&cellrefinedbuffer[i], sizeof(int) );
					}
				}

				/* send info to other processors */
				start = 0;
				num_send_requests = 0;

				for ( proc = 0; proc < num_procs; proc++ ) {
					if ( start < page_count && current_read_count + start >= proc_first_index[proc] &&
							current_read_count + start < proc_first_index[proc+1] ) {

						count = MIN( proc_first_index[proc+1], current_read_count+page_count ) -
							MAX( proc_first_index[proc], current_read_count + start );

						cart_assert( count > 0 && count <= page_count );

						for ( i = 0; i < count; i++ ) {
							cart_assert( start + i < page_count );
							if ( cellrefinedbuffer[start+i] ) {
								next_proc_index[proc+1]++;
							}
						}

						if ( proc == local_proc_id ) {
							/* copy into local buffer */
							proc_page_count[local_proc_id] = count;
							cellrefined[local_proc_id] =
								cart_alloc(int, proc_page_count[local_proc_id] );
							for ( i = 0; i < proc_page_count[local_proc_id]; i++ ) {
								cart_assert( start + i < page_count );
								cellrefined[local_proc_id][i] = cellrefinedbuffer[start+i];
							}
						} else {
							MPI_Isend( &cellrefinedbuffer[start], count, MPI_INT, proc,
								count, mpi.comm.run, &send_requests[num_send_requests++] );
						}

						start += count;
					}
				}

				cart_assert( start == page_count );

				current_read_count += page_count;
				ready_to_read = 0;

				/* split local cells first */
				if ( proc_page_count[local_proc_id] > 0 ) {
					flag = 1;
					proc = local_proc_id;
				} else if ( num_requests > 0 ) {
					/* see if we've received anything */
					MPI_Testany( num_procs, requests, &proc, &flag, MPI_STATUS_IGNORE );
				}

			} else if ( num_requests > 0 ) {
				/* wait for a receive to complete */
				MPI_Waitany( num_procs, requests, &proc, MPI_STATUS_IGNORE );
				num_requests--;
				flag = 1;
			}

			if ( flag == 1 && proc != MPI_UNDEFINED ) {
				cart_assert( proc >= 0 && proc < num_procs );

				/* split cells in received page */
				i = 0;
				while ( i < proc_page_count[proc] ) {
					cart_assert( proc_cell_index[proc] + proc_cur_cells[proc]/num_children
							< num_cells_per_level[level]/num_children );
					ioct = current_level[ proc_cell_index[proc] + proc_cur_cells[proc]/num_children ];

					cart_assert( ioct >= 0 && ioct < num_octs );
					cart_assert( oct_level[ioct] == level );

					for ( j = 0; j < num_children; j++ ) {
						icell = oct_child( ioct, j );
						cart_assert( icell >= 0 && icell < num_cells );
						cart_assert( cell_level(icell) == level );

						if ( cellrefined[proc][i] == 1 ) {
							ret = split_cell(icell);

							if ( ret ) {
								cart_error("Unable to finish splitting cells, ran out of octs?");
							}

							order[ proc_cell_index[proc]*num_children +
								proc_next_level_octs[proc] ] = cell_child_oct[icell];
							next_level_count++;
							proc_next_level_octs[proc]++;

						}

						i++;
						proc_cur_cells[proc]++;
						current_level_count++;
					}
				}

				/* if necessary, start a new receive from that proc */
				if ( proc_cur_cells[proc] < proc_num_cells[proc] && proc != local_proc_id ) {
					proc_page_count[proc] = MIN( page_size,
							proc_num_cells[proc] - proc_cur_cells[proc] );
					cart_assert( proc_page_count[proc] > 0 && proc_page_count[proc] <= page_size );
					MPI_Irecv( cellrefined[proc], proc_page_count[proc], MPI_INT,
						proc, proc_page_count[proc], mpi.comm.run, &requests[proc] );
					num_requests++;
				} else {
					proc_page_count[proc] = 0;
					cart_free( cellrefined[proc] );
					requests[proc] = MPI_REQUEST_NULL;
				}

				flag = 0;
			}

			/* check if we're done reading */
			if ( local_proc_id == file_parent ) {
				if ( num_requests > 0 ) {
					/* see if sends have completed */
					MPI_Testall( num_send_requests, send_requests,
							&ready_to_read, MPI_STATUSES_IGNORE );
				} else {
					MPI_Waitall( num_send_requests, send_requests, MPI_STATUSES_IGNORE );
					num_send_requests = 0;
					ready_to_read = 1;
				}

				if ( current_read_count >= total_cells[level] &&
						current_level_count >= num_cells_per_level[level] ) {
					continue_reading = 0;
				}
			} else {
				if ( current_level_count >= num_cells_per_level[level] ) {
					continue_reading = 0;
				}
			}
		}

		if ( local_proc_id == file_parent ) {
			if ( !ready_to_read ) {
				MPI_Waitall( num_send_requests, send_requests, MPI_STATUSES_IGNORE );
			}

			cart_free( cellrefinedbuffer );

			fread( &size, sizeof(int), 1, input );
		}

		/* repack the order array */
		count = 0;
		for ( proc = 0; proc < num_procs; proc++ ) {
			for ( i = 0; i < proc_next_level_octs[proc]; i++ ) {
				cart_assert( proc_cell_index[proc]*num_children + i  >= count );
				cart_assert( order[ proc_cell_index[proc]*num_children + i ] >= 0 &&
						order[ proc_cell_index[proc]*num_children + i ] < num_octs );
				cart_assert( oct_level[ order[ proc_cell_index[proc]*num_children + i ] ] == level+1 );

				order[count++] = order[ proc_cell_index[proc]*num_children + i ];
			}
		}

		cart_assert( count == next_level_count );

		read_cart_grid_binary_lower_level_vars(num_in_hydro_vars,skip_hvar,num_hydro_vars,hydro_vars,input,endian,file_parent,
					file_index,total_cells,page_size,proc_num_cells,level,first_page_count,
					proc_first_index,proc_cell_index,current_level);

#if defined(GRAVITY) || defined(RADIATIVE_TRANSFER)
		read_cart_grid_binary_lower_level_vars(num_in_other_vars,skip_ovar,num_other_vars,other_vars,input,endian,file_parent,file_index,
					total_cells,page_size,proc_num_cells,level,first_page_count,proc_first_index,
					proc_cell_index,current_level);
#endif /* GRAVITY || RADIATIVE_TRANSFER */

		cart_free( current_level );
	}

	if ( local_proc_id == file_parent ) {
		fclose( input );
	}

	if(other_vars != 0) cart_free(other_vars);

	cart_free( order );

#ifdef RADIATIVE_TRANSFER
	/* Load RF data */
	rtReadRadiationFieldData(filename,1);
#endif
}

#ifdef BLASTWAVE_FEEDBACK
extern double blastwave_time_floor;
extern double blastwave_time_cut;
extern double blastwave_time_code_max;
#endif /* BLASTWAVE_FEEDBACK */

float set_skipped_var(int jskip){
#ifdef BLASTWAVE_FEEDBACK
  if(jskip == HVAR_BLASTWAVE_TIME - HVAR_GAS_DENSITY){
    return (float)1.1*blastwave_time_floor;
  }else{
    cart_error("skipped variables %d not set",jskip);
    return -1;
  }
#else
  cart_error("skipped variables %d not set",jskip);
  return -1;
#endif /* BLASTWAVE_FEEDBACK */
}

void read_cart_grid_binary_top_level_vars(int num_in_vars, int jskip, int num_out_vars, int *out_var, FILE *input, int endian,
		int file_parent, int file_index, int local_file_root_cells, int page_size, int *proc_num_cells,
		long *proc_cell_index, int *file_sfc_index) {
        int i, j, m, jout;
	int size;
	float *cellvars[MAX_PROCS], *cellvars_buffer, *cellvars_read_buffer;
	int num_requests, num_send_requests;
	int proc;
	int proc_cur_cells[MAX_PROCS];
	int proc_page_count[MAX_PROCS];
	long current_level_count, current_read_count;
	int continue_reading, ready_to_read;
	int num_read, flag;
	int page_count;
	long start, count;
	int icell;
	MPI_Request requests[2*MAX_PROCS];
	MPI_Request send_requests[MAX_PROCS];

	if(num_out_vars < 1) return;
	cart_assert( num_in_vars >= num_out_vars || (jskip >= 0 && (num_in_vars >= num_out_vars-1)) );

	if ( local_proc_id == file_parent ) {
		fread( &size, sizeof(int), 1, input );
		cellvars_buffer = cart_alloc(float, num_out_vars*MIN( local_file_root_cells, page_size ) );
		cellvars_read_buffer = cart_alloc(float, num_in_vars*MIN( local_file_root_cells, page_size ) );
	}

	num_requests = 0;

	for ( proc = 0; proc < num_procs; proc++ ) {
		proc_cur_cells[proc] = 0;

		if ( proc_num_cells[proc] > 0 && proc != local_proc_id )  {
			/* set up receive */
			proc_page_count[proc] = MIN( proc_num_cells[proc],
					page_size - ( proc_cell_index[proc] -
					file_sfc_index[(int)((proc * num_cart_input_files)/ num_procs)]) % page_size);
			cellvars[proc] = cart_alloc(float, num_out_vars*MIN( page_size, proc_num_cells[proc] ) );
			MPI_Irecv( cellvars[proc], num_out_vars*proc_page_count[proc], MPI_FLOAT, proc, proc_page_count[proc],
					mpi.comm.run, &requests[proc] );
			num_requests++;
		} else {
			proc_page_count[proc] = 0;
			requests[proc] = MPI_REQUEST_NULL;
		}
	}

	current_level_count = 0;
	current_read_count = 0;
	continue_reading = 1;
	ready_to_read = 1;

	while ( continue_reading ) {
		flag = 0;

		if ( local_proc_id == file_parent && current_read_count < local_file_root_cells && ready_to_read ) {
			/* read in a page */
			page_count = MIN( page_size, local_file_root_cells - current_read_count );
			num_read = fread( cellvars_read_buffer, sizeof(float), num_in_vars*page_count, input );

			if ( num_read != num_in_vars*page_count ) {
				cart_error("I/O Error in read_grid_binary: num_read = %u, num_in_vars*page_count = %u",
						num_read, num_in_vars*page_count );
			}

			for(i=0; i<page_count; i++)
			  {
			    jout = 0; for(j=0; j<MAX(num_in_vars,num_out_vars); j++)
			      {
				while( jout == jskip ){
				  cellvars_buffer[num_out_vars*i+jskip] = set_skipped_var(jskip);
				  jout++;
				}
				if( jout < num_out_vars ) {cellvars_buffer[num_out_vars*i+jout] = cellvars_read_buffer[num_in_vars*i+j];}
				jout++;
			      }
			  }

			if ( endian ) {
				for ( i = 0; i < num_out_vars*page_count; i++ ) {
					reorder( (char *)&cellvars_buffer[i], sizeof(float) );
				}
			}

			/* send info to other processors */
			start = 0;
			num_send_requests = 0;
			for ( proc = 0; proc < num_procs; proc++ ) {
				if ( start < page_count &&
						file_sfc_index[file_index]+current_read_count+start < proc_sfc_index[proc+1] &&
						file_sfc_index[file_index]+current_read_count+start >= proc_sfc_index[proc] ) {

					count = MIN( proc_sfc_index[proc+1], file_sfc_index[file_index]+current_read_count+page_count ) -
							MAX( proc_sfc_index[proc],  file_sfc_index[file_index] + current_read_count + start );
					cart_assert( count > 0 && count <= page_count );

					if ( proc == local_proc_id ) {
						/* copy into local buffer */
						proc_page_count[local_proc_id] = count;
						cellvars[local_proc_id] = cart_alloc(float, num_out_vars*proc_page_count[local_proc_id] );

						for ( i = 0; i < num_out_vars*proc_page_count[local_proc_id]; i++ ) {
							cart_assert( num_out_vars*start + i < num_out_vars*page_count );
							cellvars[local_proc_id][i] = cellvars_buffer[num_out_vars*start+i];
						}
					} else {
						MPI_Isend( &cellvars_buffer[num_out_vars*start], num_out_vars*count, MPI_FLOAT, proc, count,
								mpi.comm.run, &send_requests[num_send_requests++] );
					}

					start += count;
				}
			}

			cart_assert( start == page_count );
			current_read_count += page_count;
			ready_to_read = 0;

			/* unpack local cells first */
			if ( proc_page_count[local_proc_id] > 0 ) {
				flag = 1;
				proc = local_proc_id;
			} else if ( num_requests > 0 ) {
				/* see if we've received anything */
				MPI_Testany( num_procs, requests, &proc, &flag, MPI_STATUS_IGNORE );
			}
		} else if ( num_requests > 0 ) {
			/* wait for a receive to complete */
			MPI_Waitany( num_procs, requests, &proc, MPI_STATUS_IGNORE );
			num_requests--;
			flag = 1;
		}

		if ( flag == 1 && proc != MPI_UNDEFINED ) {
			cart_assert( proc >= 0 && proc < num_procs );

			/* unpack received page */
			i = 0;
			while ( i < num_out_vars*proc_page_count[proc] ) {
				icell = sfc_cell_location( proc_cell_index[proc] + proc_cur_cells[proc]);
				cart_assert( icell >= 0 && icell < num_cells_per_level[min_level] );

				for ( m = 0; m < num_out_vars; m++ ) {
					cell_var(icell,out_var[m]) = cellvars[proc][i++];
				}

				current_level_count++;
				proc_cur_cells[proc]++;
			}

			/* if necessary, start a new receive for that proc */
			if ( proc_cur_cells[proc] < proc_num_cells[proc] && proc != local_proc_id ) {
				proc_page_count[proc] = MIN( page_size, proc_num_cells[proc] - proc_cur_cells[proc] );
				cart_assert( proc_page_count[proc] > 0 && proc_page_count[proc] <= page_size );
				MPI_Irecv( cellvars[proc], num_out_vars*proc_page_count[proc], MPI_FLOAT, proc, proc_page_count[proc],
						mpi.comm.run, &requests[proc] );
				num_requests++;
			} else {
				proc_page_count[proc] = 0;
				cart_free( cellvars[proc] );
				requests[proc] = MPI_REQUEST_NULL;
			}
		}

		if ( local_proc_id == file_parent ) {
			if ( num_requests > 0 ) {
				/* see if sends have completed, if they have we can continue to read */
				MPI_Testall( num_send_requests, send_requests, &ready_to_read, MPI_STATUSES_IGNORE );
			} else {
				/* not waiting on any receives, so we can't do anything until
				 * we read in additional data */
				MPI_Waitall( num_send_requests, send_requests, MPI_STATUSES_IGNORE );
				num_send_requests = 0;
				ready_to_read = 1;
			}

			if ( current_read_count >= local_file_root_cells && current_level_count >= num_cells_per_level[min_level] ) {
				continue_reading = 0;
			}
		} else {
			if ( current_level_count >= num_cells_per_level[min_level] ) {
				continue_reading = 0;
			}
		}
	}

	if ( local_proc_id == file_parent ) {
		if ( !ready_to_read ) {
			MPI_Waitall( num_send_requests, send_requests, MPI_STATUSES_IGNORE );
		}

		cart_free( cellvars_read_buffer );
		cart_free( cellvars_buffer );
		fread( &size, sizeof(int), 1, input );
	}
}


void read_cart_grid_binary_lower_level_vars(int num_in_vars, int jskip, int num_out_vars, int *out_var, FILE *input, int endian, int file_parent, int file_index, long *total_cells, int page_size, int *proc_num_cells, int level, long *first_page_count, long *proc_first_index, long *proc_cell_index, int *current_level)
{
  int i, j, m, jout;
  int size;
  float *cellvars[MAX_PROCS], *cellvars_buffer, *cellvars_read_buffer;
  int num_requests, num_send_requests;
  int proc;
  int proc_cur_cells[MAX_PROCS];
  int proc_page_count[MAX_PROCS];
  long current_level_count, current_read_count;
  int continue_reading, ready_to_read;
  int num_read, flag;
  int page_count;
  long start, count;
  int icell, ioct;
  MPI_Request requests[2*MAX_PROCS];
  MPI_Request send_requests[MAX_PROCS];


  if(num_out_vars < 1) return;
  cart_assert( num_in_vars >= num_out_vars || (jskip >= 0 && (num_in_vars >= num_out_vars-1)) );

  if ( local_proc_id == file_parent )
    {
      fread( &size, sizeof(int), 1, input );
      cellvars_buffer = cart_alloc(float, num_out_vars*MIN( total_cells[level], page_size ) );
      cellvars_read_buffer = cart_alloc(float, num_in_vars*MIN( total_cells[level], page_size ) );
    }

  num_requests = 0;

  for ( proc = 0; proc < num_procs; proc++ )
    {
      proc_cur_cells[proc] = 0;

      if ( proc_num_cells[proc] > 0 && proc != local_proc_id )
	{
	  /* set up receive */
	  proc_page_count[proc] = MIN( page_size - first_page_count[proc] % page_size, proc_num_cells[proc] );
	  cellvars[proc] = cart_alloc(float, num_out_vars*MIN( page_size, proc_num_cells[proc] ) );
	  MPI_Irecv( cellvars[proc], num_out_vars*proc_page_count[proc], MPI_FLOAT, proc, proc_page_count[proc], mpi.comm.run, &requests[proc] );
	  num_requests++;
	}
      else
	{
	  proc_page_count[proc] = 0;
	  requests[proc] = MPI_REQUEST_NULL;
	}
    }

  current_level_count = 0;
  current_read_count = 0;
  continue_reading = 1;
  ready_to_read = 1;

  while ( continue_reading )
    {
      flag = 0;

      if ( local_proc_id == file_parent && current_read_count < total_cells[level] && ready_to_read )
	{
	  /* read in a page */
	  page_count = MIN( page_size, total_cells[level] - current_read_count );
	  num_read = fread( cellvars_read_buffer, sizeof(float), num_in_vars*page_count, input );

	  if ( num_read != num_in_vars*page_count )
	    {
	      cart_error("I/O Error in read_grid_binary: num_read = %u, num_out_vars*page_count = %u", num_read, num_out_vars*page_count );
	    }

	  for(i=0; i<page_count; i++)
	    {
	      jout=0; for(j=0; j<MAX(num_in_vars,num_out_vars); j++)
		{
		  while( jout == jskip ){
		    cellvars_buffer[num_out_vars*i+jskip] = set_skipped_var(jskip);
		    jout++;
		  }
		  if( jout < num_out_vars ) {cellvars_buffer[num_out_vars*i+jout] = cellvars_read_buffer[num_in_vars*i+j];}
		  jout++;
		}
	    }

	  if ( endian )
	    {
	      for ( i = 0; i < num_out_vars*page_count; i++ )
		{
		  reorder( (char *)&cellvars_buffer[i], sizeof(float) );
		}
	    }

	  /* send info to other processors */
	  start = 0;
	  num_send_requests = 0;
	  for ( proc = 0; proc < num_procs; proc++ )
	    {
	      if ( start < page_count && current_read_count + start >= proc_first_index[proc] && current_read_count + start < proc_first_index[proc+1] )
		{

		  count = MIN( proc_first_index[proc+1], current_read_count+page_count ) - MAX( proc_first_index[proc], current_read_count + start );
		  cart_assert( count > 0 && count <= page_count );

		  if ( proc == local_proc_id )
		    {
		      /* copy into local buffer */
		      proc_page_count[local_proc_id] = count;

		      cellvars[local_proc_id] = cart_alloc(float, num_out_vars*proc_page_count[local_proc_id] );

		      for ( i = 0; i < num_out_vars*proc_page_count[local_proc_id]; i++ )
			{
			  cart_assert( num_out_vars*start + i < num_out_vars*page_count );
			  cellvars[local_proc_id][i] = cellvars_buffer[num_out_vars*start+i];
			}
		    }
		  else
		    {
		      MPI_Isend( &cellvars_buffer[num_out_vars*start], num_out_vars*count, MPI_FLOAT, proc, count, mpi.comm.run, &send_requests[num_send_requests++] );
		    }

		  start += count;
		}
	    }

	  cart_assert( start == page_count );
	  current_read_count += page_count;
	  ready_to_read = 0;

	  /* unpack local cells first */
	  if ( proc_page_count[local_proc_id] > 0 )
	    {
	      flag = 1;
	      proc = local_proc_id;
	    }
	  else if ( num_requests > 0 )
	    {
	      /* see if we've received anything */
	      MPI_Testany( num_procs, requests, &proc, &flag, MPI_STATUS_IGNORE );
	    }
	}
      else if ( num_requests > 0 )
	{
	  /* wait for a receive to complete */
	  MPI_Waitany( num_procs, requests, &proc, MPI_STATUS_IGNORE );
	  num_requests--;
	  flag = 1;
	}

      if ( flag == 1 && proc != MPI_UNDEFINED )
	{
	  cart_assert( proc >= 0 && proc < num_procs );

	  /* unpack received page */
	  i = 0;

	  while ( i < num_out_vars*proc_page_count[proc] )
	    {
	      ioct = current_level[ proc_cell_index[proc] + proc_cur_cells[proc]/num_children];
	      cart_assert( ioct >= 0 && ioct < num_octs );

	      for ( j = 0; j < num_children; j++ )
		{
		  icell = oct_child( ioct, j );
		  cart_assert( icell >= 0 && icell < num_cells );
		  cart_assert( cell_level(icell) == level );

		  for ( m = 0; m < num_out_vars; m++ )
		    {
		      cell_var(icell,out_var[m]) = cellvars[proc][i++];
		    }


		  current_level_count++;
		  proc_cur_cells[proc]++;
		}
	    }

	  /* if necessary, start a new receive for that proc */
	  if ( proc_cur_cells[proc] < proc_num_cells[proc] && proc != local_proc_id )
	    {
	      proc_page_count[proc] = MIN( page_size, proc_num_cells[proc] - proc_cur_cells[proc] );
	      cart_assert( proc_page_count[proc] > 0 && proc_page_count[proc] <= page_size );
	      MPI_Irecv( cellvars[proc], num_out_vars*proc_page_count[proc], MPI_FLOAT, proc, proc_page_count[proc], mpi.comm.run, &requests[proc] );
	      num_requests++;
	    }
	  else
	    {
	      proc_page_count[proc] = 0;
	      cart_free( cellvars[proc] );
	      requests[proc] = MPI_REQUEST_NULL;
	    }

	  flag = 0;
	}

      if ( local_proc_id == file_parent )
	{
	  if ( num_requests > 0 )
	    {
	      /* see if sends have completed */
	      MPI_Testall( num_send_requests, send_requests, &ready_to_read, MPI_STATUSES_IGNORE );
	    }
	  else
	    {
	      MPI_Waitall( num_send_requests, send_requests, MPI_STATUSES_IGNORE );
	      num_send_requests = 0;
	      ready_to_read = 1;
	    }

	  if ( current_read_count >= total_cells[level] && current_level_count >= num_cells_per_level[level] )
	    {
	      continue_reading = 0;
	    }
	}
      else
	{
	  if ( current_level_count >= num_cells_per_level[level] )
	    {
	      continue_reading = 0;
	    }
	}
    }

  if ( local_proc_id == file_parent )
    {
      if ( !ready_to_read )
	{
	  MPI_Waitall( num_send_requests, send_requests, MPI_STATUSES_IGNORE );
	}

      cart_free( cellvars_buffer );
      fread( &size, sizeof(int), 1, input );
    }
}

#endif /* SFC_DOMAIN_DECOMPOSITION */

#ifdef RADIATIVE_TRANSFER
/*
//  This is a helper function used for backward compatibility -
//  the absolute values of radiation fields changed in 1.9 for a
//  plausible reason.
*/
void rescale_radiation_fields(float scale)
{
  int cell, j;

#pragma omp parallel for default(none), private(cell,j), shared(cell_vars_data,scale)
  for(cell=0; cell<num_cells; cell++)
    {
#ifdef rt_far_freq_offset
      for(j=0; j<rt_far_freq_offset; j++)
#else
      for(j=0; j<rt_num_disk_vars; j++)
#endif
        {
          cell_var(cell,rt_disk_offset+j) *= scale;
        }
    }

}
#endif /* RADIATIVE_TRANSFER */
