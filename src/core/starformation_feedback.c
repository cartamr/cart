#include "config.h"
#ifdef STAR_FORMATION

#include <stdio.h>
#include <string.h>

#include "auxiliary.h"
#include "control_parameter.h"
#include "imf.h"
#include "starformation_recipe.h"
#include "starformation_feedback.h"
#include "tree.h"
#include "units.h"

extern struct StellarFeedbackParticle sf_feedback_particle_internal;
const struct StellarFeedbackParticle *sf_feedback_particle = &sf_feedback_particle_internal;

extern struct StellarFeedbackCell sf_feedback_cell_internal;
const struct StellarFeedbackCell *sf_feedback_cell = &sf_feedback_cell_internal;

const char *sf_feedback_name = NULL;

double feedback_turbulence_temperature_ceiling = 1.0e8;
double feedback_sampling_timescale = 0;           /* in yrs */
double feedback_temperature_ceiling = 1.0e8;  /* Used to be called T_max_feedback; also, was a define in HART */
double feedback_speed_time_ceiling = 1e4;  /* do not accelerate flows so that time-steps drop below~1e4yr (1e3km/s at 10pc) for a flow that is has v=0 wrt the grid*/
double feedback_speed_ceiling = 5e3;  /* do not accelerate flows with speed larger than 5e3km/s */

DEFINE_LEVEL_ARRAY(int,star_feedback_frequency);

#ifdef BLASTWAVE_FEEDBACK
double blastwave_time = { 50.0e6 };
#endif /* BLASTWAVE_FEEDBACK */


void control_parameter_list_star_formation_feedback(FILE *stream, const void *ptr)
{
  fprintf(stream,"<%s>",sf_feedback_name);
}


void config_init_star_formation_feedback()
{
  static char *ptr = "star_formation_feedback";
  ControlParameterOps r = { NULL, control_parameter_list_star_formation_feedback };

  sf_feedback_name = MACRO_AS_STRING(SF_FEEDBACK);

  /*
  // call config_init for the current feedback model
  */
  if(sf_feedback_particle->config_init != NULL) sf_feedback_particle->config_init();

  /*
  //  other
  */
  control_parameter_add(r,ptr,"sf:feedback","a feedback model for star formation. This parameter is for listing only, and must be set with SF_FEEDBACK variable in the Makefile. See /src/sf for available feedback models.");

  control_parameter_add2(control_parameter_time,&feedback_sampling_timescale,"fb:sampling-timescale","feedback_sampling_timescale","the intervals at which feedback routines are called. Must be 0 for extra pressure.");

  control_parameter_add2(control_parameter_time,&feedback_speed_time_ceiling,"fb:time-ceiling","feedback_speed_time_ceiling","minimum cell crossing time feedback can contribute to. Kinetic feedback doesn't add to gas speed above this limit.");

  control_parameter_add2(control_parameter_double,&feedback_speed_ceiling,"fb:speed-ceiling","feedback_speed_ceiling","maximum cell speed that feedback can contribute to. Kinetic feedback doesn't add to gas speed above this limit.");

  control_parameter_add3(control_parameter_double,&feedback_temperature_ceiling,"fb:temperature-ceiling","feedback_temperature_ceiling","T_max_feedback","maximum gas temperature for the feedback to operate. No feedback is allowed in the gas with the temperature above this limit.");

  control_parameter_add3(control_parameter_double,&feedback_turbulence_temperature_ceiling,"fb:turbulence-temperature-ceiling","feedback_turbulence_temperature_ceiling","turb_T_max_feedback","maximum turbulence temperature for the feedback to operate. No feedback is allowed in the gas with the temperature above this limit.");

#ifdef BLASTWAVE_FEEDBACK 
  control_parameter_add3(control_parameter_time,&blastwave_time,"blastwave-time","bw:blast-time","bw.blast_time","time before cells can cool in blastwave feedback subgrid model.");
#endif /* BLASTWAVE_FEEDBACK */
}


void config_verify_star_formation_feedback()
{
  cart_assert(sf_feedback_particle_internal.rt_source != NULL);
  cart_assert(sf_feedback_particle_internal.hydro_feedback != NULL);

  VERIFY(sf:feedback, 1 );

  /*
  // call config_verify for the current feedback model
  */
  if(sf_feedback_particle->config_verify != NULL) sf_feedback_particle->config_verify();

  /*
  //  other
  */
  VERIFY(fb:temperature-ceiling, feedback_temperature_ceiling > 1.0e6 );
  VERIFY(fb:time-ceiling, feedback_speed_time_ceiling < 1.0e6 && feedback_speed_time_ceiling >0);
  VERIFY(fb:speed-ceiling, feedback_speed_ceiling < 1.0e4 && feedback_speed_ceiling >0);
  VERIFY(fb:turbulence-temperature-ceiling, feedback_turbulence_temperature_ceiling >= 1.0e1 );

#ifdef EXTRA_PRESSURE_SOURCE
  /* 
  // extra pressure is set by feedback and then zeroed rather than being a conserved quantity. 
  // so it would be turned off after the first zeroing 
  // or left on for the duration of sampling timescale until the next feedback step.
  */
  VERIFY(fb:sampling-timescale, feedback_sampling_timescale == 0);
#else
  VERIFY(fb:sampling-timescale, feedback_sampling_timescale >= 0 && feedback_sampling_timescale < 1e10);
#endif /* EXTRA_PRESSURE_SOURCE */

#ifdef BLASTWAVE_FEEDBACK 
  VERIFY(blastwave-time, !(blastwave_time < 0.0) );
#endif /* BLASTWAVE_FEEDBACK */
}


void init_star_formation_feedback()
{
  /*
  // call init for the current feedback model
  */
  if(sf_feedback_particle->init != NULL) sf_feedback_particle->init();
}


#if defined(HYDRO) && defined(PARTICLES)

#ifdef BLASTWAVE_FEEDBACK
void start_blastwave(int icell)
{
  cell_blastwave_time(icell) =  cell_gas_density(icell) * blastwave_time;
}
#endif /* BLASTWAVE_FEEDBACK */


double dKfact;
double dUfact;  /* must be here to simplify OpenMP directives */
double dvfact;  
void setup_star_formation_feedback(int level)
{
#ifdef SGS_TURBULENCE
  dKfact = feedback_turbulence_temperature_ceiling/(units->temperature*constants->wmu*(sgs_turbulence_gamma-1));
#endif /* SGS_TURBULENCE */
  dUfact = feedback_temperature_ceiling/(units->temperature*constants->wmu*(constants->gamma-1));
  /* speed ceiling -- multiply by density for momentum ceiling */
  dvfact = MIN( cell_size[level] / (feedback_speed_time_ceiling*constants->yr/units->time), feedback_speed_ceiling*constants->kms/units->velocity ) ;

  /*
  // call setup for the current feedback model
  */
  if(sf_feedback_particle->setup != NULL) sf_feedback_particle->setup(level);
}

#endif /* HYDRO && PARTICLES */

#endif /* STAR_FORMATION */
