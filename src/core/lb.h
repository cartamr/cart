#ifndef __LB_H__
#define __LB_H__

#ifndef CONFIGURED
#error "Missing config.h include."
#endif

#include <stdint.h>

#include "index_hash.h"
#include "sfc.h"

typedef struct LB_DECOMPOSITION lb_decomposition;

int lb_sfc_processor_owner( lb_decomposition *decomposition, sfc_t sfc );
int lb_sfc_cell_position( lb_decomposition *decomposition, sfc_t sfc );
int lb_sfc_cell_is_local( lb_decomposition *decomposition, sfc_t sfc );

/* decompositions useful for initial conditions codes */
void lb_uniform_decomposition();
void lb_sfc_decomposition( sfc_t *new_sfc_index );

lb_decomposition *lb_allocate_decomposition();
void lb_destroy_decomposition( lb_decomposition *decomposition );

void lb_finalize_decomposition( lb_decomposition *decomposition );
void lb_apply_decomposition( lb_decomposition *decomposition );

/* functions to apply result of lb_decompose to decomposition object */
void lb_set_sfc_range(lb_decomposition *decomposition, sfc_t sfc_start, sfc_t sfc_end );
void lb_set_sfc_list(lb_decomposition *decomposition, int num_sfc, sfc_t *sfc_list);

/*
//  Called by each rank in mpi.comm.run.
//  each local_... array sized to num_local_root_cells
//  local_constrained_quantities[constraint+num_constrained_quantitities*local_root_cell]
//  boundary_level[6*num_local_root_cells]
*/
lb_decomposition *lb_domain_decomposition( int num_local_root_cells,
		sfc_t *local_sfc_list, float *local_work,
		int num_constrained_quantitities, int *local_constrained_quantities, int *per_proc_constraints,
		int *boundary_level );

void lb_decompose( int num_local_root_cells,
		sfc_t *local_sfc_list, float *local_work,
		int num_constrained_quantities,
		int *local_constrained_quantities,
		int *per_proc_constraints,
		int *boundary_level,
		lb_decomposition *decomposition );

#endif /* __LB_H__ */
