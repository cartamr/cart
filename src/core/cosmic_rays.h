#ifndef __COSMIC_RAYS_H__
#define __COSMIC_RAYS_H__

#ifdef COSMIC_RAYS

#define cr_default_gamma (4./3.)

void config_init_cosmic_rays();
void config_verify_cosmic_rays();

#endif /* COSMIC_RAYS */

#endif
