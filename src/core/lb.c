#include "config.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "auxiliary.h"
#include "cell_buffer.h"
#include "lb.h"
#include "parallel.h"
#include "sfc.h"
#include "skiplist.h"
#include "tree.h"


/*
//  A pseudo-domain decompisiton is linear division num_root_cells/num_procs
//  A rank that "owns" that sfc index knows where the cell is coming from; ranks query them to 
//  ask for the rank that owns their cells.
//
//  This decomposition is used in lb_decomposition_finalize to communicate rank-locations for
//  sfc indices in the new domain decomposition (also an implementation is used in artio).
*/


struct LB_DECOMPOSITION {
	int local_decomposition_set;
	int num_local_root_cells;
	int num_buffer_root_cells;
	sfc_t *root_cell_sfc_index;

#ifdef SFC_DOMAIN_DECOMPOSITION
	sfc_t proc_sfc_index[MAX_PROCS+1];
	index_hash *buffer_root_hash;
#else
	index_hash *root_hash;
	index_hash *new_root_proc;
	sfc_t *sfc_list;
	int *buffer_cell_proc_owner;
#endif
};


void lb_uniform_decomposition() {
	int proc;
	sfc_t new_sfc_index[MAX_PROCS+1];
	for( proc = 0; proc <= num_procs; proc++) {
		new_sfc_index[proc] = ((uint64_t)num_root_cells*(uint64_t)proc)/num_procs;
	}
	lb_sfc_decomposition(new_sfc_index);
}

void lb_sfc_decomposition( sfc_t *new_sfc_index ) {
	lb_decomposition *decomposition = lb_allocate_decomposition();
	lb_set_sfc_range(decomposition, new_sfc_index[local_proc_id], new_sfc_index[local_proc_id+1]-1);
	lb_finalize_decomposition( decomposition );
	lb_apply_decomposition( decomposition );
	lb_destroy_decomposition( decomposition );
}

int lb_sfc_cell_position( lb_decomposition *decomposition, sfc_t sfc ) {
#ifdef SFC_DOMAIN_DECOMPOSITION
	if ( sfc >= decomposition->proc_sfc_index[local_proc_id] &&
			sfc < decomposition->proc_sfc_index[local_proc_id+1] ) {
		return sfc - decomposition->proc_sfc_index[local_proc_id];
	} else {
		return -1;
	}
#else
	return index_hash_lookup( decomposition->root_hash, sfc );
#endif /* SFC_DOMAIN_DECOMPOSITION */
}

int lb_sfc_processor_owner( lb_decomposition *decomposition, sfc_t sfc ) {
#ifdef SFC_DOMAIN_DECOMPOSITION
	int a, b, c;
	a = 0;
	b = num_procs-1;
	while ( a != b ) {
		c = ( a + b + 1) / 2;
		if ( sfc < decomposition->proc_sfc_index[c] ) {
			b = c-1;
		} else {
			a = c;
		}
	}
	return a;
#else
	return index_hash_lookup(decomposition->new_root_proc, sfc);
#endif
}

int lb_sfc_is_local( lb_decomposition *decomposition, sfc_t sfc ) {
#ifdef SFC_DOMAIN_DECOMPOSITION
	return ( sfc >= decomposition->proc_sfc_index[local_proc_id] &&
			 sfc < decomposition->proc_sfc_index[local_proc_id+1] );
#else
	cart_assert( decomposition->root_hash != NULL );
	int index = index_hash_lookup(decomposition->root_hash, sfc);
	return ( index >= 0 && index < decomposition->num_local_root_cells );
#endif
}

lb_decomposition *lb_allocate_decomposition() {
	lb_decomposition *decomposition = cart_alloc(lb_decomposition, 1);

	decomposition->local_decomposition_set = 0;
	decomposition->num_local_root_cells = 0;
	decomposition->num_buffer_root_cells = 0;
	decomposition->root_cell_sfc_index = NULL;

#ifdef SFC_DOMAIN_DECOMPOSITION
	int proc;
	for (proc = 0; proc <= num_procs; proc++ ) {
		decomposition->proc_sfc_index[proc] = 0;
	}
	decomposition->buffer_root_hash = NULL;
#else
	decomposition->root_hash = NULL;
	decomposition->sfc_list = NULL;
	decomposition->new_root_proc = NULL;
	decomposition->buffer_cell_proc_owner = NULL;
#endif

	return decomposition;
}

void lb_destroy_decomposition( lb_decomposition *decomposition ) {
	if (decomposition->root_cell_sfc_index != NULL) cart_free(decomposition->root_cell_sfc_index);
#ifdef SFC_DOMAIN_DECOMPOSITION
	if (decomposition->buffer_root_hash != NULL) index_hash_free(decomposition->buffer_root_hash);
#else
	if (decomposition->root_hash != NULL) index_hash_free(decomposition->root_hash);
	if (decomposition->new_root_proc != NULL) index_hash_free(decomposition->new_root_proc);
	if (decomposition->sfc_list != NULL) cart_free(decomposition->sfc_list);
	if (decomposition->buffer_cell_proc_owner != NULL) cart_free(decomposition->buffer_cell_proc_owner);
#endif
	cart_free(decomposition);
}

void lb_apply_decomposition( lb_decomposition *decomposition ) {
	/* TODO: check that we have a valid decomposition */

#ifdef SFC_DOMAIN_DECOMPOSITION
	int proc;
	for ( proc = 0; proc <= num_procs; proc++ ) {
#ifdef DEBUG
		if ( local_proc_id == MASTER_NODE ) {
			cart_debug("proc_sfc_index[%d] = %ld", proc, decomposition->proc_sfc_index[proc]);
		}
#endif
		proc_sfc_index[proc] = decomposition->proc_sfc_index[proc];
	}

	if ( buffer_root_hash != NULL ) {
		index_hash_free(buffer_root_hash);
	}
	buffer_root_hash = decomposition->buffer_root_hash;
	decomposition->buffer_root_hash = NULL;
#else
	if ( root_hash != NULL ) {
		index_hash_free(root_hash);
	}
	root_hash = decomposition->root_hash;
	cart_assert(root_hash != NULL);
	decomposition->root_hash = NULL;

	if ( buffer_cell_proc_owner != NULL ) {
		cart_free(buffer_cell_proc_owner);
	}
	buffer_cell_proc_owner = decomposition->buffer_cell_proc_owner;
	decomposition->buffer_cell_proc_owner = NULL;
#endif /* SFC_DOMAIN_DECOMPOSITION */

	num_cells_per_level[min_level] = decomposition->num_local_root_cells;
	num_buffer_cells[min_level] = decomposition->num_buffer_root_cells;

	if ( root_cell_sfc_index != NULL ) {
		cart_free( root_cell_sfc_index );
	}
	root_cell_sfc_index = decomposition->root_cell_sfc_index;
	decomposition->root_cell_sfc_index = NULL;

	next_free_oct = MAX( next_free_oct,
		cell_parent_oct( num_cells_per_level[min_level] + num_buffer_cells[min_level] ) + 1 );
	root_cells_enabled = 1;
}

void lb_set_sfc_range(lb_decomposition *decomposition,
		sfc_t sfc_start, sfc_t sfc_end ) {
	cart_assert( sfc_start >= 0 && sfc_start < num_root_cells );
	cart_assert( sfc_end >= sfc_start && sfc_end < num_root_cells );

	decomposition->num_local_root_cells = sfc_end-sfc_start+1;

#ifdef SFC_DOMAIN_DECOMPOSITION
	/* ensure we don't call this twice */
	cart_assert(decomposition->proc_sfc_index[local_proc_id] == 0);

	decomposition->proc_sfc_index[local_proc_id] = sfc_start;
	decomposition->proc_sfc_index[local_proc_id+1] = sfc_end+1;
#else
	int i;
	cart_assert(decomposition->sfc_list == NULL);

	decomposition->sfc_list = cart_alloc(sfc_t, sfc_end-sfc_start+1);
	for ( i = 0; i <= sfc_end-sfc_start; i++ ) {
		decomposition->sfc_list[i] = i + sfc_start;
	}
#endif
	decomposition->local_decomposition_set = 1;
}

void lb_set_sfc_list(lb_decomposition *decomposition, int num_sfc, sfc_t *sfc_list) {
#ifdef SFC_DOMAIN_DECOMPOSITION
	cart_error("SFC_DOMAIN_DECOMPOSITION cannot handle arbitrary decompositions. Use a different lb algorithm or don't define SFC_DOMAIN_DECOMPOSITION.");
#else
	/* this function should only be called once */
	cart_assert(decomposition->sfc_list == NULL);

	/* could try to avoid double-memory and copying, but modularity... */
	decomposition->sfc_list = cart_alloc(sfc_t, num_sfc);
	memcpy(decomposition->sfc_list, sfc_list, num_sfc*sizeof(sfc_t));
	decomposition->num_local_root_cells = num_sfc;
	decomposition->local_decomposition_set = 1;
#endif
}

void lb_finalize_decomposition( lb_decomposition *decomposition ) {
	int i;
	int index;
	sfc_t sfc;
	int *local_index;

	if ( !decomposition->local_decomposition_set ) {
		cart_error("Load balancing algorithm failed to set the local decomposition (this function must call either lb_set_sfc_range_decomposition or lb_set_sfc_list_decomposition)");
	}

#ifndef SFC_DOMAIN_DECOMPOSITION
	/* create a temporary root_hash including only local root cells */
	local_index = cart_alloc(int, decomposition->num_local_root_cells);
	for ( i = 0; i < decomposition->num_local_root_cells; i++ ) {
		local_index[i] = i;
	}
	decomposition->root_hash = index_hash_create(decomposition->num_local_root_cells,
			num_root_cells, decomposition->sfc_list, local_index);
	cart_free(local_index);
#endif

	/* determine number of root cells to be buffered */
	skiplist *buffer_list = skiplist_init();
	int neighbors[num_stencil];

	#pragma omp parallel for default(none) private(sfc,neighbors,i) shared(buffer_list,local_proc_id,decomposition)
	for ( index = 0; index < decomposition->num_local_root_cells; index++ ) {
#ifdef SFC_DOMAIN_DECOMPOSITION
		sfc = index + decomposition->proc_sfc_index[local_proc_id];
#else
		sfc = decomposition->sfc_list[index];
#endif
		cart_assert(sfc >= 0 && sfc < num_root_cells);

		root_cell_uniform_stencil(sfc, neighbors);

		for ( i = 0; i < num_stencil; i++ ) {
			if ( !lb_sfc_is_local(decomposition, neighbors[i]) ) {
				#pragma omp critical (build_root_buffer_skiplist)
				skiplist_insert( buffer_list, neighbors[i] );
			}
		}
	}

	decomposition->num_buffer_root_cells = skiplist_size(buffer_list);

	/* cells to array positions */
	/* TODO: when a cell is already local, keep it at the same array position */
	decomposition->root_cell_sfc_index = cart_alloc(int, decomposition->num_local_root_cells+decomposition->num_buffer_root_cells);
	for ( i = 0; i < decomposition->num_local_root_cells; i++ ) {
#ifdef SFC_DOMAIN_DECOMPOSITION
		decomposition->root_cell_sfc_index[i] = decomposition->proc_sfc_index[local_proc_id] + i;
#else
		decomposition->root_cell_sfc_index[i] = decomposition->sfc_list[i];
#endif
	}
	skiplist_iterate( buffer_list );
	while ( skiplist_next(buffer_list, &sfc) ) {
		decomposition->root_cell_sfc_index[i++] = sfc;
	}
	cart_assert( i == decomposition->num_local_root_cells+decomposition->num_buffer_root_cells );
	skiplist_destroy(buffer_list);

#ifdef SFC_DOMAIN_DECOMPOSITION
	sfc_t local_sfc_index = decomposition->proc_sfc_index[local_proc_id];
	MPI_Allgather(&local_sfc_index, 1, MPI_SFC, decomposition->proc_sfc_index, 1, MPI_SFC, mpi.comm.run);
	decomposition->proc_sfc_index[num_procs] = num_root_cells;

	if ( decomposition->num_local_root_cells != decomposition->proc_sfc_index[local_proc_id+1] - decomposition->proc_sfc_index[local_proc_id]) {
		cart_error("Error: inconsistent lb decomposition detected, different ranks have different proc_sfc_index values!");
	}

	/* construct buffer_root_hash */
	local_index = cart_alloc(int, decomposition->num_buffer_root_cells);
	for ( i = 0; i < decomposition->num_buffer_root_cells; i++ ) {
		local_index[i] = decomposition->num_local_root_cells + i;
	}
	decomposition->buffer_root_hash = index_hash_create(decomposition->num_buffer_root_cells, num_root_cells,
		&decomposition->root_cell_sfc_index[decomposition->num_local_root_cells], local_index);
	cart_free(local_index);
#else /* SFC_DOMAIN_DECOMPOSITION */
	cart_free(decomposition->sfc_list);
	decomposition->sfc_list = NULL;

	/* create the final root_hash including local and buffer root cells */
	index_hash_free(decomposition->root_hash);
	int total_root_cells = decomposition->num_local_root_cells+decomposition->num_buffer_root_cells;
	local_index = cart_alloc(int, total_root_cells);
	for ( i = 0; i < total_root_cells; i++ ) {
		local_index[i] = i;
	}
	decomposition->root_hash = index_hash_create(total_root_cells, num_root_cells,
			decomposition->root_cell_sfc_index, local_index);
	cart_free(local_index);

	/* communicate decomposition to those ranks that need it (2-way indirection */
	/* lb_sfc_processor_owner needs to know if we own it now or if we will own it where it is or going */
	int proc;

	/* distribute sfc equally amongst MPI ranks */
	sfc_t num_sfc_per_proc = (num_root_cells+num_procs-1) / num_procs;
	sfc_t sfc_offset_range_start = local_proc_id*num_sfc_per_proc;
	sfc_t sfc_offset_range_end = MIN(num_root_cells, (local_proc_id+1)*num_sfc_per_proc);
	sfc_t sfc_offset_size = sfc_offset_range_end - sfc_offset_range_start;

	/* communicate sfc lists */
	int *send_sfc_count = cart_alloc(int, num_procs);
	int *recv_sfc_count = cart_alloc(int, num_procs);
	int *send_sfc_offset = cart_alloc(int, num_procs);
	int *recv_sfc_offset = cart_alloc(int, num_procs);

	sfc_t *send_sfc_list = cart_alloc(sfc_t, MAX(decomposition->num_local_root_cells, num_cells_per_level[min_level]));
	sfc_t *recv_sfc_list = cart_alloc(sfc_t, sfc_offset_size);

	/* count the number of new decomposition sfcs to send to each process */
	for ( proc = 0; proc < num_procs; proc++ ) {
		send_sfc_count[proc] = 0;
	}

	for ( i = 0; i < decomposition->num_local_root_cells; i++ ) {
		proc = decomposition->root_cell_sfc_index[i] / num_sfc_per_proc;
		send_sfc_count[proc]++;
	}

	send_sfc_offset[0] = 0;
	for ( proc = 1; proc < num_procs; proc++ ) {
		send_sfc_offset[proc] = send_sfc_offset[proc-1]+send_sfc_count[proc-1];
	}

	for ( i = 0; i < decomposition->num_local_root_cells; i++ ) {
		proc = decomposition->root_cell_sfc_index[i] / num_sfc_per_proc;
		send_sfc_list[ send_sfc_offset[proc]++ ] = decomposition->root_cell_sfc_index[i];
	}

	send_sfc_offset[0] = 0;
	for ( proc = 1; proc < num_procs; proc++ ) {
		send_sfc_offset[proc] = send_sfc_offset[proc-1]+send_sfc_count[proc-1];
	}

	/* let each rank know how many to expect */
	MPI_Alltoall( send_sfc_count, 1, MPI_INT,
			recv_sfc_count, 1, MPI_INT,
			mpi.comm.run );

	/* ensure we've received the correct number of sfc indices */
	recv_sfc_offset[0] = 0;
	for ( proc = 1; proc < num_procs; proc++ ) {
		recv_sfc_offset[proc] = recv_sfc_offset[proc-1]+recv_sfc_count[proc-1];
	}

	if ( recv_sfc_offset[num_procs-1]+recv_sfc_count[proc-1] != sfc_offset_size ) {
		cart_error("Error; inconsistent lb decomposition detected, some sfc indices are not assigned to any rank!");
	}

	MPI_Alltoallv( send_sfc_list, send_sfc_count, send_sfc_offset, MPI_SFC,
		recv_sfc_list, recv_sfc_count, recv_sfc_offset, MPI_SFC, mpi.comm.run );

	int *new_sfc_owner = cart_alloc(int, sfc_offset_size);

	proc = 0;
	for ( i = 0; i < sfc_offset_size; i++ ) {
		cart_assert( recv_sfc_list[i] >= sfc_offset_range_start &&
			recv_sfc_list[i] < sfc_offset_range_end );
		while ( proc < num_procs-1 && i == recv_sfc_offset[proc+1] ) {
			proc++;
		}
		new_sfc_owner[ recv_sfc_list[i] - sfc_offset_range_start ] = proc;
	}

	/* count the number of old decomposition sfcs to send to each process */
	for ( proc = 0; proc < num_procs; proc++ ) {
		send_sfc_count[proc] = 0;
	}

	for ( i = 0; i < num_cells_per_level[min_level]; i++ ) {
		proc = root_cell_sfc_index[i] / num_sfc_per_proc;
		send_sfc_count[proc]++;
	}

	send_sfc_offset[0] = 0;
	for ( proc = 1; proc < num_procs; proc++ ) {
		send_sfc_offset[proc] = send_sfc_offset[proc-1]+send_sfc_count[proc-1];
	}

	for ( i = 0; i < num_cells_per_level[min_level]; i++ ) {
		proc = root_cell_sfc_index[i] / num_sfc_per_proc;
		send_sfc_list[ send_sfc_offset[proc]++ ] = root_cell_sfc_index[i];
	}

	send_sfc_offset[0] = 0;
	for ( proc = 1; proc < num_procs; proc++ ) {
		send_sfc_offset[proc] = send_sfc_offset[proc-1]+send_sfc_count[proc-1];
	}

	MPI_Alltoall( send_sfc_count, 1, MPI_INT,
			recv_sfc_count, 1, MPI_INT,
			mpi.comm.run );

	/* construct per-rank offsets (note: recv_offsets and counts may be 0 if there is no pre-existing decomposition) */
	recv_sfc_offset[0] = 0;
	for ( proc = 1; proc < num_procs; proc++ ) {
		recv_sfc_offset[proc] = recv_sfc_offset[proc-1]+recv_sfc_count[proc-1];
	}

	MPI_Alltoallv( send_sfc_list, send_sfc_count, send_sfc_offset, MPI_SFC,
			recv_sfc_list, recv_sfc_count, recv_sfc_offset, MPI_SFC, mpi.comm.run );

	cart_free( send_sfc_list );

	int *send_proc_list = cart_alloc(int, sfc_offset_size);
	int *recv_proc_list = cart_alloc(int, num_cells_per_level[min_level]);

	/* convert from sfc -> processor owner in new decomposition */
	for ( i = 0; i < sfc_offset_size; i++ ) {
		cart_assert( recv_sfc_list[i] >= sfc_offset_range_start &&
				recv_sfc_list[i] < sfc_offset_range_end );
		send_proc_list[i] = new_sfc_owner[ recv_sfc_list[i] - sfc_offset_range_start ];
	}

	cart_free( recv_sfc_list );

	/* return mapping of sfc -> new owner (note reversal of send/recv sizes and offsets) */
	MPI_Alltoallv( send_proc_list, recv_sfc_count, recv_sfc_offset, MPI_INT,
			recv_proc_list, send_sfc_count, send_sfc_offset, MPI_INT, mpi.comm.run );

	cart_free( send_proc_list );

	/* create mapping of current sfc to new owner */
	decomposition->new_root_proc = index_hash_create(num_cells_per_level[min_level], num_root_cells,
			root_cell_sfc_index, recv_proc_list);

	cart_free(recv_proc_list);

	/* do same 2-way indirect mapping to communicate owners of buffer cells */
	send_sfc_list = cart_alloc(sfc_t, decomposition->num_buffer_root_cells);

	for ( proc = 0; proc < num_procs; proc++ ) {
		send_sfc_count[proc] = 0;
	}

	for ( i = 0; i < decomposition->num_buffer_root_cells; i++ ) {
		proc = decomposition->root_cell_sfc_index[decomposition->num_local_root_cells+i] / num_sfc_per_proc;
		send_sfc_count[proc]++;
	}

	send_sfc_offset[0] = 0;
	for ( proc = 1; proc < num_procs; proc++ ) {
		send_sfc_offset[proc] = send_sfc_offset[proc-1]+send_sfc_count[proc-1];
	}

	for ( i = 0; i < decomposition->num_buffer_root_cells; i++ ) {
		sfc = decomposition->root_cell_sfc_index[decomposition->num_local_root_cells+i];
		proc = sfc / num_sfc_per_proc;
		send_sfc_list[ send_sfc_offset[proc]++ ] = sfc;
	}

	send_sfc_offset[0] = 0;
	for ( proc = 1; proc < num_procs; proc++ ) {
		send_sfc_offset[proc] = send_sfc_offset[proc-1]+send_sfc_count[proc-1];
	}

	/* let each rank know how many to expect */
	MPI_Alltoall( send_sfc_count, 1, MPI_INT,
			recv_sfc_count, 1, MPI_INT,
			mpi.comm.run );

	recv_sfc_offset[0] = 0;
	for ( proc = 1; proc < num_procs; proc++ ) {
		recv_sfc_offset[proc] = recv_sfc_offset[proc-1]+recv_sfc_count[proc-1];
	}

	int num_recv_sfc = recv_sfc_offset[num_procs-1]+recv_sfc_count[proc-1];
	recv_sfc_list = cart_alloc(sfc_t, num_recv_sfc);

	MPI_Alltoallv( send_sfc_list, send_sfc_count, send_sfc_offset, MPI_SFC,
			recv_sfc_list, recv_sfc_count, recv_sfc_offset, MPI_SFC, mpi.comm.run );

	cart_free( send_sfc_list );

	/* convert recv_sfc_list to proc owner */
	send_proc_list = cart_alloc(int, num_recv_sfc);
	recv_proc_list = cart_alloc(int, decomposition->num_buffer_root_cells);

	/* convert from sfc -> processor owner in new decomposition */
	for ( i = 0; i < num_recv_sfc; i++ ) {
		cart_assert( recv_sfc_list[i] >= sfc_offset_range_start &&
				recv_sfc_list[i] < sfc_offset_range_end );
		send_proc_list[i] = new_sfc_owner[ recv_sfc_list[i] - sfc_offset_range_start ];
	}

	cart_free( recv_sfc_list );
	cart_free( new_sfc_owner );

	MPI_Alltoallv( send_proc_list, recv_sfc_count, recv_sfc_offset, MPI_INT,
			recv_proc_list, send_sfc_count, send_sfc_offset, MPI_INT, mpi.comm.run );

	cart_free( send_proc_list );

	/* unpack recv proc list into buffer_cell_proc_owner */
	decomposition->buffer_cell_proc_owner = cart_alloc(int, decomposition->num_buffer_root_cells);

	for ( i = 0; i < decomposition->num_buffer_root_cells; i++ ) {
		proc = decomposition->root_cell_sfc_index[decomposition->num_local_root_cells+i] / num_sfc_per_proc;
		decomposition->buffer_cell_proc_owner[i] = recv_proc_list[send_sfc_offset[proc]++];
	}

	cart_free( recv_proc_list );
	cart_free( recv_sfc_count );
	cart_free( send_sfc_count );
	cart_free( recv_sfc_offset );
	cart_free( send_sfc_offset );
#endif /* SFC_DOMAIN_DECOMPOSITION */
}

lb_decomposition *lb_domain_decomposition(
		int num_local_root_cells,
		sfc_t *local_sfc_list, float *work,
		int num_constraints, int *constrained_quantities, int *per_proc_constraints,
		int *boundary_level ) {
	lb_decomposition *decomposition = lb_allocate_decomposition();

	/* call plugin decomposition algorithm */
	lb_decompose( num_local_root_cells, local_sfc_list, work, num_constraints,
		constrained_quantities, per_proc_constraints, boundary_level, decomposition );

	/* construct mapping between current and future decomposition */
	lb_finalize_decomposition( decomposition );

	return decomposition;
}
